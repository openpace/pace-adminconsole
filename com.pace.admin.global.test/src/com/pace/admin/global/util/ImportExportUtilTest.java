/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.util.Arrays;

import junit.framework.TestCase;

import com.pace.admin.global.enums.ImportExportFileType;
import com.pace.base.db.membertags.MemberTagType;
import com.pace.server.client.SimpleCellNote;
import com.pace.server.client.SimpleCoordList;
import com.pace.server.client.SimpleMemberTagData;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ImportExportUtilTest extends TestCase {

	private final static String EXPORT_DIR = "C:\\";
	
	private final static String APP_ID_1 = "App1";
	
	private final static String APP_ID_2 = "App2";
	
	private final static String DS_ID_1 = "DS1";
	
	private final static String DS_ID_2 = "DS2";
	
	private final static String CREATOR_1 = "JavaJ";
	
	private final static String CREATOR_2 = "Moosman";
	
	private final static SimpleCoordList simpleCoordList1 = new SimpleCoordList();
	
	private final static SimpleCoordList simpleCoordList2 = new SimpleCoordList();
	
	private final static String TEXT_1 = "Text 1";
	
	private final static String TEXT_2 = "Text 2";
	
	private final static String DATA_1 = "Data 1";
	
	private final static String DATA_2 = "Data 2";

	private final static String MEMBER_TAG_1 = "MT 1";
	
	private final static String MEMBER_TAG_2 = "MT 2";	
	
	private final static String MEMBER_TAG_TYPE_1 = MemberTagType.TEXT.toString();
	
	private final static String MEMBER_TAG_TYPE_2 = MemberTagType.HYPERLINK.toString();	
	
	private final static String[] AXIS_1 = new String[] { "Dim1", "Dim2", "Dim3" };
	
	private final static String[] AXIS_2 = new String[] { "Dim4", "Dim5", "Dim6" };
	
	private final static String[] COORDS_1 = new String[] { "Val1", "Val2", "Val3", "Val4", "Val5", "Val6" } ;
	
	private final static String[] COORDS_2 = new String[] { "Val7", "Val8", "Val9", "Val10", "Val11", "Val13" } ;
	
	private final static int ID_1 = 1;
	
	private final static int ID_2 = 1;
		
	private SimpleCellNote[] simpleCellNotes;
	
	private static String cellNoteFileName;
	
	private static SimpleMemberTagData[] simpleMemberTagDataAr;
	
	private static String simpleMemberTagDataFileName;
	
	static {
		
		simpleCoordList1.getAxis().addAll(Arrays.asList(AXIS_1));
		simpleCoordList1.getCoordinates().addAll(Arrays.asList(COORDS_1));
		
		simpleCoordList2.getAxis().addAll(Arrays.asList(AXIS_2));
		simpleCoordList2.getCoordinates().addAll(Arrays.asList(COORDS_2));
		
	}
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
						
		SimpleCellNote scn1 = new SimpleCellNote();
		scn1.setApplicationName(APP_ID_1);
		scn1.setCreator(CREATOR_1);
		scn1.setDataSourceName(DS_ID_1);
		scn1.setSimpleCoordList(simpleCoordList1);
		scn1.setText(TEXT_1);
		scn1.setId(1);
		scn1.setVisible(true);
		
		SimpleCellNote scn2 = new SimpleCellNote();
		scn2.setApplicationName(APP_ID_2);
		scn2.setCreator(CREATOR_2);
		scn2.setDataSourceName(DS_ID_2);
		scn2.setSimpleCoordList(simpleCoordList2);
		scn2.setText(TEXT_2);
		scn2.setId(ID_2);
		scn2.setVisible(true);
		
		simpleCellNotes = new SimpleCellNote[] { scn1, scn2 };
		
		SimpleMemberTagData smtd1 = new SimpleMemberTagData();
		smtd1.setApplicationName(APP_ID_1);
		smtd1.setCreator(CREATOR_1);
		smtd1.setData(DATA_1);
		smtd1.setId(ID_1);
		smtd1.setMemberTagName(MEMBER_TAG_1);
		smtd1.setMemberTagType(MEMBER_TAG_TYPE_1);
		smtd1.setSimpleCoordList(simpleCoordList1);
		
		SimpleMemberTagData smtd2 = new SimpleMemberTagData();
		smtd2.setApplicationName(APP_ID_2);
		smtd2.setCreator(CREATOR_2);
		smtd2.setData(DATA_2);
		smtd2.setId(ID_2);
		smtd2.setMemberTagName(MEMBER_TAG_2);
		smtd2.setMemberTagType(MEMBER_TAG_TYPE_2);
		smtd2.setSimpleCoordList(simpleCoordList2);
		
		simpleMemberTagDataAr = new SimpleMemberTagData[] { smtd1, smtd2 };
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.ImportExportUtil#exportSimpleCellNotes(com.pace.paf.server.client.SimpleCellNote[], java.lang.String, com.pace.paf.adminconsole.global.enums.ImportExportFileType)}.
	 */
	public void testExportSimpleCellNotes() {

		try {
			
			cellNoteFileName = ImportExportUtil.exportSimpleCellNotes(simpleCellNotes, EXPORT_DIR, ImportExportFileType.XML);
			
			assertNotNull(cellNoteFileName);
			assertEquals(true, new File(cellNoteFileName).exists());				
						
		} catch (Exception e) {
			
			fail(e.getMessage());
			
		}
		
		
	}

	
	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.ImportExportUtil#importSimpleCellNotes(java.lang.String)}.
	 */
	public void testImportSimpleCellNotes() {
		
		SimpleCellNote[] importedSimpleCellNotes = ImportExportUtil.importSimpleCellNotes(cellNoteFileName);
		
		assertNotNull(importedSimpleCellNotes);
		
		assertEquals(importedSimpleCellNotes.length, simpleCellNotes.length);
		
		assertEquals(importedSimpleCellNotes[0].getApplicationName(), simpleCellNotes[0].getApplicationName());
		assertEquals(importedSimpleCellNotes[0].getCreator(), simpleCellNotes[0].getCreator());
		assertEquals(importedSimpleCellNotes[0].getDataSourceName(), simpleCellNotes[0].getDataSourceName());
		assertEquals(importedSimpleCellNotes[0].getId(), simpleCellNotes[0].getId());
		assertNotNull(importedSimpleCellNotes[0].getSimpleCoordList());
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getAxis().size(), simpleCellNotes[0].getSimpleCoordList().getAxis().size());
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getAxis().get(0), simpleCellNotes[0].getSimpleCoordList().getAxis().get(0));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getAxis().get(1), simpleCellNotes[0].getSimpleCoordList().getAxis().get(1));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getAxis().get(2), simpleCellNotes[0].getSimpleCoordList().getAxis().get(2));
		
		assertNotNull(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates());
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().size(), simpleCellNotes[0].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(0), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(0));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(1), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(1));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(2), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(2));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(3), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(3));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(4), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(4));
		assertEquals(importedSimpleCellNotes[0].getSimpleCoordList().getCoordinates().get(5), simpleCellNotes[0].getSimpleCoordList().getCoordinates().get(5));

		
		assertEquals(importedSimpleCellNotes[0].getText(), simpleCellNotes[0].getText());
		
		assertEquals(importedSimpleCellNotes[1].getApplicationName(), simpleCellNotes[1].getApplicationName());
		assertEquals(importedSimpleCellNotes[1].getCreator(), simpleCellNotes[1].getCreator());
		assertEquals(importedSimpleCellNotes[1].getDataSourceName(), simpleCellNotes[1].getDataSourceName());
		assertEquals(importedSimpleCellNotes[1].getId(), simpleCellNotes[1].getId());
		assertNotNull(importedSimpleCellNotes[1].getSimpleCoordList());
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getAxis().size(), simpleCellNotes[1].getSimpleCoordList().getAxis().size());
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getAxis().get(0), simpleCellNotes[1].getSimpleCoordList().getAxis().get(0));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getAxis().get(1), simpleCellNotes[1].getSimpleCoordList().getAxis().get(1));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getAxis().get(2), simpleCellNotes[1].getSimpleCoordList().getAxis().get(2));
		
		assertNotNull(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates());
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().size(), simpleCellNotes[1].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(0), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(0));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(1), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(1));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(2), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(2));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(3), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(3));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(4), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(4));
		assertEquals(importedSimpleCellNotes[1].getSimpleCoordList().getCoordinates().get(5), simpleCellNotes[1].getSimpleCoordList().getCoordinates().get(5));
		
		assertEquals(importedSimpleCellNotes[1].getText(), simpleCellNotes[1].getText());		
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.ImportExportUtil#exportMemberTagData(com.pace.paf.server.client.SimpleMemberTagData[], java.lang.String, com.pace.paf.adminconsole.global.enums.ImportExportFileType)}.
	 */
	public void testExportMemberTagData() {

		
		try {
			
			simpleMemberTagDataFileName = ImportExportUtil.exportMemberTagData(simpleMemberTagDataAr, EXPORT_DIR, ImportExportFileType.XML);
			
			assertNotNull(simpleMemberTagDataFileName);
			
			assertEquals(true, new File(simpleMemberTagDataFileName).exists());				
						
		} catch (Exception e) {
			
			fail(e.getMessage());
			
		}
	}

	
	
	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.ImportExportUtil#importSimpleMemberTagData(java.lang.String)}.
	 */
	public void testImportSimpleMemberTagData() {
		
		SimpleMemberTagData[] importedSimpleMemberTagDataAr = ImportExportUtil.importSimpleMemberTagData(simpleMemberTagDataFileName);
		
		assertNotNull(importedSimpleMemberTagDataAr);
		
		assertEquals(importedSimpleMemberTagDataAr.length, simpleMemberTagDataAr.length);
		
		assertEquals(importedSimpleMemberTagDataAr[0].getApplicationName(), simpleMemberTagDataAr[0].getApplicationName());
		assertEquals(importedSimpleMemberTagDataAr[0].getCreator(), simpleMemberTagDataAr[0].getCreator());
		assertEquals(importedSimpleMemberTagDataAr[0].getData(), simpleMemberTagDataAr[0].getData());
		assertEquals(importedSimpleMemberTagDataAr[0].getId(), simpleMemberTagDataAr[0].getId());
		assertEquals(importedSimpleMemberTagDataAr[0].getMemberTagName(), simpleMemberTagDataAr[0].getMemberTagName());
		assertEquals(importedSimpleMemberTagDataAr[0].getMemberTagType(), simpleMemberTagDataAr[0].getMemberTagType());
		
		assertNotNull(importedSimpleMemberTagDataAr[0].getSimpleCoordList());
		assertNotNull(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getAxis());
		assertNotNull(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates());
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getAxis().size(), simpleMemberTagDataAr[0].getSimpleCoordList().getAxis().size());
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().size(), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().size(), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(0), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(0));
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(1), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(1));
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(2), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(2));
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(3), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(3));
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(4), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(4));
		assertEquals(importedSimpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(5), simpleMemberTagDataAr[0].getSimpleCoordList().getCoordinates().get(5));
		
		assertEquals(importedSimpleMemberTagDataAr[1].getApplicationName(), simpleMemberTagDataAr[1].getApplicationName());
		assertEquals(importedSimpleMemberTagDataAr[1].getCreator(), simpleMemberTagDataAr[1].getCreator());
		assertEquals(importedSimpleMemberTagDataAr[1].getData(), simpleMemberTagDataAr[1].getData());
		assertEquals(importedSimpleMemberTagDataAr[1].getId(), simpleMemberTagDataAr[1].getId());
		assertEquals(importedSimpleMemberTagDataAr[1].getMemberTagName(), simpleMemberTagDataAr[1].getMemberTagName());
		assertEquals(importedSimpleMemberTagDataAr[1].getMemberTagType(), simpleMemberTagDataAr[1].getMemberTagType());
		
		assertNotNull(importedSimpleMemberTagDataAr[1].getSimpleCoordList());
		assertNotNull(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getAxis());
		assertNotNull(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates());
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getAxis().size(), simpleMemberTagDataAr[1].getSimpleCoordList().getAxis().size());
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().size(), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().size(), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().size());
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(0), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(0));
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(1), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(1));
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(2), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(2));
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(3), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(3));
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(4), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(4));
		assertEquals(importedSimpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(5), simpleMemberTagDataAr[1].getSimpleCoordList().getCoordinates().get(5));
		
		
	}


}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;

public class URLUtility {

	private static final Logger logger = Logger.getLogger(URLUtility.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			
			String url = "http://replace_with_home_dir:8080/PafServer/PafService?wsdl";
			
			//create url instance
			URL urlToConnectTo = new URL(url);
			
			//open connection
            URLConnection connection = urlToConnectTo.openConnection();
            
            //set timeout
            connection.setConnectTimeout(1000);
                        
            //if instance of http
            if (connection instanceof HttpURLConnection) {
            
            	//cast to http connection
              HttpURLConnection httpConnection = 
                 (HttpURLConnection)connection;
            
              //try to connect
              httpConnection.connect();
            
              //get code response and log
              logger.debug("HTTP Response Code for " + url + " : " + httpConnection.getResponseCode());
              
              logger.info("Successfull connection to " + url);
              
            }
            
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException: " + e.getMessage());
		} catch (IOException e) {
			logger.warn("URL IOException: " + e.getMessage());
		}

	}

}

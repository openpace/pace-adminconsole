/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.List;

import junit.framework.TestCase;

import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PaceTreeUtilTest extends TestCase {

	private static final String ROOT_CHILD1_CHILD2 = "RootChild1Child2";

	private static final String ROOT_CHILD1_CHILD1 = "RootChild1Child1";
	
	private static final String ROOT_CHILD2_CHILD2 = "RootChild2Child2";

	private static final String ROOT_CHILD2_CHILD1 = "RootChild2Child1";

	private static final String ROOT_CHILD2 = "RootChild2";

	private static final String ROOT_CHILD1 = "RootChild1";

	private PaceTreeNode rootNode1 = new PaceTreeNode(PaceTreeNodeType.Root.toString(), new PaceTreeNodeProperties(PaceTreeNodeType.Root.toString(), 0, 0, null, PaceTreeNodeType.Root), null);

	private PaceTreeNode rootNode2 = new PaceTreeNode(PaceTreeNodeType.Root.toString(), new PaceTreeNodeProperties(PaceTreeNodeType.Root.toString(), 0, 0, null, PaceTreeNodeType.Root), null);
	
	private String DIM1 = "Dimension1";
	
	private String DIM2 = "Dimension2";
	
	private String DIM3 = "Dimension3";
	
	private PaceTreeNode rootChild1NoChildren;
	
	private PaceTreeNode rootChild2NoChildren;
	
	private PaceTreeNode rootChild1;
	
	private PaceTreeNode rootChild2;
	
	private PaceTreeNode rootChild1Child1;
	
	private PaceTreeNode rootChild1Child2;
	
	private PaceTreeNode rootChild2Child1;
	
	private PaceTreeNode rootChild2Child2;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		
		rootChild1NoChildren = new PaceTreeNode(ROOT_CHILD1, new PaceTreeNodeProperties(DIM1, 1, 1, null, PaceTreeNodeType.PaceMemberLevel0, PaceTreeNodeSelectionType.Selection, true), rootNode1);
		
		rootChild2NoChildren = new PaceTreeNode(ROOT_CHILD2, new PaceTreeNodeProperties(DIM1, 1, 1, null, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.IDescendants, true), rootNode1);		
				
		rootChild1 = new PaceTreeNode(ROOT_CHILD1, new PaceTreeNodeProperties(DIM1, 2, 1, null, PaceTreeNodeType.PaceMemberLevel0, PaceTreeNodeSelectionType.Selection, true), rootNode2);
		
		rootChild2 = new PaceTreeNode(ROOT_CHILD2, new PaceTreeNodeProperties(DIM1, 2, 1, null, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.IDescendants, true), rootNode2);
		
		rootChild1Child1 = new PaceTreeNode(ROOT_CHILD1_CHILD1, new PaceTreeNodeProperties(DIM2, 2, 2, null, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.Selection, true), rootChild1);
		
		rootChild1Child2 = new PaceTreeNode(ROOT_CHILD1_CHILD2, new PaceTreeNodeProperties(DIM2, 2, 2, null, PaceTreeNodeType.PaceMemberLevel0, PaceTreeNodeSelectionType.Selection, true), rootChild1);
	
		rootChild2Child1 = new PaceTreeNode(ROOT_CHILD2_CHILD1, new PaceTreeNodeProperties(DIM2, 2, 2, null, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.Selection, true), rootChild2);
		
		rootChild2Child2 = new PaceTreeNode(ROOT_CHILD2_CHILD2, new PaceTreeNodeProperties(DIM2, 2, 2, null, PaceTreeNodeType.PaceMemberLevel0, PaceTreeNodeSelectionType.Selection, true), rootChild2);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getRootNode(com.pace.paf.adminconsole.global.model.PaceTreeNode)}.
	 */
	public void testGetRootNode() {
		
		
		assertEquals(PaceTreeUtil.getRootNode(rootChild1NoChildren), rootNode1);
		assertEquals(PaceTreeUtil.getRootNode(rootChild2NoChildren), rootNode1);
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getPositionNdxForParentNodeAtDimNdx(com.pace.paf.adminconsole.global.model.PaceTreeNode, int)}.
	 */
	public void testGetPositionNdxForParentNodeAtDimNdx() {
		
		assertEquals(PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(rootChild1NoChildren, 1), 0);
		assertEquals(PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(rootChild2NoChildren, 1), 1);
		
		assertEquals(PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(rootChild1NoChildren, 2), -1);
		assertEquals(PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(rootChild2NoChildren, 2), -1);
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getParentNodeAtDimNdx(com.pace.paf.adminconsole.global.model.PaceTreeNode, int)}.
	 */
	public void testGetParentNodeAtDimNdx() {
			
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild1NoChildren, 1), rootNode1);
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild2NoChildren, 1), rootNode1);
		
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild1NoChildren, 0), null);
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild2NoChildren, 0), null);
		
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild1NoChildren, 2), null);
		assertEquals(PaceTreeUtil.getParentNodeAtDimNdx(rootChild2NoChildren, 2), null);
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getFirstBottomLevelPaceTreeNode(com.pace.paf.adminconsole.global.model.PaceTreeNode)}.
	 */
	public void testGetFirstBottomLevelPaceTreeNode() {
		
		assertEquals(PaceTreeUtil.getFirstBottomLevelPaceTreeNode(rootNode1), rootChild1NoChildren);
		assertEquals(PaceTreeUtil.getFirstBottomLevelPaceTreeNode(rootNode2), rootChild1Child1);
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getAllBottomLevelPaceTreeNodes(com.pace.paf.adminconsole.global.model.PaceTreeNode)}.
	 */
	public void testGetAllBottomLevelPaceTreeNodes() {

	
		List<PaceTreeNode> bottomLevelTreeNodesList1 = PaceTreeUtil.getAllBottomLevelPaceTreeNodes(rootNode1);
		assertEquals(bottomLevelTreeNodesList1.size(), 2);
		assertEquals(bottomLevelTreeNodesList1.get(0), rootChild1NoChildren);
		assertEquals(bottomLevelTreeNodesList1.get(1), rootChild2NoChildren);
		
		
		
		List<PaceTreeNode> bottomLevelTreeNodesList2 = PaceTreeUtil.getAllBottomLevelPaceTreeNodes(rootNode2);
		assertEquals(bottomLevelTreeNodesList2.size(), 4);
		assertEquals(bottomLevelTreeNodesList2.get(0), rootChild1Child1);
		assertEquals(bottomLevelTreeNodesList2.get(1), rootChild1Child2);
		assertEquals(bottomLevelTreeNodesList2.get(2), rootChild2Child1);
		assertEquals(bottomLevelTreeNodesList2.get(3), rootChild2Child2);
	
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.PaceTreeUtil#getParentNodesAsList(com.pace.paf.adminconsole.global.model.PaceTreeNode)}.
	 */
	public void testGetParentNodesAsList() {
				
		List<PaceTreeNode> parentNodeList1 = PaceTreeUtil.getParentNodesAsList(rootChild1Child1);		
		assertEquals(parentNodeList1.size(), 1);
		assertEquals(parentNodeList1.get(0), rootChild1);
		
		List<PaceTreeNode> parentNodeList2 = PaceTreeUtil.getParentNodesAsList(rootChild1Child2);		
		assertEquals(parentNodeList2.size(), 1);
		assertEquals(parentNodeList2.get(0), rootChild1);
		
		List<PaceTreeNode> parentNodeList3 = PaceTreeUtil.getParentNodesAsList(rootChild1NoChildren);		
		assertEquals(parentNodeList3.size(), 0);
		
		List<PaceTreeNode> parentNodeList4 = PaceTreeUtil.getParentNodesAsList(rootChild2NoChildren);		
		assertEquals(parentNodeList4.size(), 0);
				
	}

}



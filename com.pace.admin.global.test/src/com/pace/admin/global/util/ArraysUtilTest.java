/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ArraysUtilTest extends TestCase {

	/**
	 *  Method_description_goes_here
	 *
	 * @throws java.lang.Exception
	 */	
	public void setUp() throws Exception {
	}

	/**
	 *  Method_description_goes_here
	 *
	 * @throws java.lang.Exception
	 */
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.util.ArraysUtil#listArrayToArray(T[])}.
	 */
	public void testListArrayToArray() {
		
		List<String> list = new ArrayList<String>();
		
		String[] strAr = ArraysUtil.listArrayToArray(list.toArray(new String[0]));
		
		assertNull(strAr);
		
		list.add("javaj");
		
		strAr = ArraysUtil.listArrayToArray(list.toArray(new String[0]));
		
		assertNotNull(strAr);
		assertEquals(strAr.length, 1);
		assertEquals(strAr[0], "javaj");
		
								
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PaceUserTest extends TestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testWithSet() {
		
		PaceUser pu1 = new PaceUser("javaj", "");
		
		PaceUser pu2 = new PaceUser("javaj", "");
		
		assertEquals(pu1, pu2);
		
		Set<PaceUser> paceUserSet = new HashSet<PaceUser>();
		
		paceUserSet.add(pu1);
		
		paceUserSet.add(pu2);
		
		assertEquals(paceUserSet.size(), 1);
		
		paceUserSet.remove(new PaceUser("javaj"));
		
		assertEquals(paceUserSet.size(), 0);
		
	}
	public void testWithSetList() {
			
			PaceUser pu1 = new PaceUser("javaj", "");
			
			PaceUser pu2 = new PaceUser("javaj", "");
			
			List<PaceUser> paceUserList = new ArrayList<PaceUser>();
			
			paceUserList.add(pu1);
			
			paceUserList.add(pu2);
			
			assertEquals(paceUserList.size(), 2);
						
			Set<PaceUser> paceUserSet = new TreeSet<PaceUser>();
			
			paceUserSet.addAll(paceUserList);
			
			assertEquals(paceUserSet.size(), 1);
					
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.view;

import junit.framework.TestCase;

import com.pace.base.db.membertags.MemberTagCommentEntry;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafViewSectionUITest extends TestCase {

	private static MemberTagDef[] memberTagDefs = null;
	
	private PafViewSectionUI pafViewSectionUI = null;
	
	private static final String DIM1 = "Project";
	
	private static final String DIM2 = "Measures";
	
	private static final String DIM3 = "Versions";
	
	private static final String DIM4 = "TestDim3";
	
	private static final String DIM5 = "TestDim2";
	
	private static final String DIM6 = "TestDim1";
	
	private static final String DIM7 = "Attribute";
	
	private static final String MT1 = "MT1";
	
	private static final String MT2 = "MT2";
	
	private static final String MT3 = "MT3";
	
	private static final String MT4 = "MT4";
	
	private static final String MT5 = "MT5";
	
	private static final String MT6 = "MT6";
	
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		MemberTagDef memberTagDef1 = new MemberTagDef();
		memberTagDef1.setName(MT1);
		memberTagDef1.setDims( new String[] {DIM1, DIM2, DIM3} );
		
		MemberTagDef memberTagDef2 = new MemberTagDef();
		memberTagDef2.setName(MT2);
		memberTagDef2.setDims( new String[] {DIM1, DIM4} );
		
		MemberTagDef memberTagDef3 = new MemberTagDef();
		memberTagDef3.setName(MT3);
		memberTagDef3.setDims( new String[] {DIM2, DIM4} );
		
		MemberTagDef memberTagDef4 = new MemberTagDef();
		memberTagDef4.setName(MT4);
		memberTagDef4.setDims( new String[] {DIM6, DIM4, DIM5} );		
		
		MemberTagDef memberTagDef5 = new MemberTagDef();
		memberTagDef5.setName(MT5);
		memberTagDef5.setDims( new String[] {DIM6, DIM4, DIM5, DIM7} );
		
		MemberTagDef memberTagDef6 = new MemberTagDef();
		memberTagDef6.setName(MT6);
		memberTagDef6.setDims( new String[] { DIM1 } );
		
		memberTagDefs = new MemberTagDef[] { memberTagDef1, memberTagDef2, memberTagDef3, memberTagDef4, memberTagDef5, memberTagDef6 };
	
		PafViewSection pafViewSection = new PafViewSection();
		
		pafViewSection.setPageTuples(createPageTuples(new String[] { DIM1, DIM3 }));
		
		pafViewSection.setRowAxisDims(new String[] { DIM2 });
		
		ViewTuple rowTuple1 = new ViewTuple();
		rowTuple1.setMemberTag(true);
		rowTuple1.setMemberDefs(new String[] { MT1 });
		
		ViewTuple rowTuple2 = new ViewTuple();
		rowTuple2.setMemberTag(true);
		rowTuple2.setMemberDefs(new String[] { MT2 });
		
		ViewTuple rowTuple3 = new ViewTuple();
		rowTuple3.setMemberTag(true);
		rowTuple3.setMemberDefs(new String[] { MT3 });
		
		ViewTuple rowTuple4 = new ViewTuple();
		rowTuple4.setMemberTag(true);
		rowTuple4.setMemberDefs(new String[] { MT4 });
		
		ViewTuple rowTuple5 = new ViewTuple();
		rowTuple5.setMemberTag(false);
		
		ViewTuple rowTuple6 = new ViewTuple();
		rowTuple6.setMemberTag(true);
		rowTuple6.setMemberDefs(new String[] { MT6 });
		
		pafViewSection.setRowTuples(new ViewTuple[] { rowTuple1, rowTuple2, rowTuple3, rowTuple4, rowTuple5, rowTuple6 });		
		
		pafViewSection.setColAxisDims(new String[] { DIM4, DIM5, DIM6 });		

		ViewTuple colTuple1 = new ViewTuple();
		colTuple1.setMemberTag(true);
		colTuple1.setMemberDefs(new String[] { MT1 });
		
		ViewTuple colTuple2 = new ViewTuple();
		colTuple2.setMemberTag(true);
		colTuple2.setMemberDefs(new String[] { MT2 });
		
		ViewTuple colTuple3 = new ViewTuple();
		colTuple3.setMemberTag(true);
		colTuple3.setMemberDefs(new String[] { MT3 });
		
		ViewTuple colTuple4 = new ViewTuple();
		colTuple4.setMemberTag(true);
		colTuple4.setMemberDefs(new String[] { MT4 });
		
		ViewTuple colTuple5 = new ViewTuple();
		colTuple5.setMemberTag(false);
		
		ViewTuple colTuple6 = new ViewTuple();
		colTuple6.setMemberTag(true);
		colTuple6.setMemberDefs(new String[] { MT6 });

		pafViewSection.setColTuples(new ViewTuple[] { colTuple1, colTuple2, colTuple3, colTuple4, colTuple5, colTuple6 });		
		
		MemberTagCommentEntry mtce1 = new MemberTagCommentEntry(MT1, true);
		MemberTagCommentEntry mtce2 = new MemberTagCommentEntry(MT2, false);
		MemberTagCommentEntry mtce3 = new MemberTagCommentEntry(MT3, true);
		MemberTagCommentEntry mtce4 = new MemberTagCommentEntry(MT4, false);
		MemberTagCommentEntry mtce5 = new MemberTagCommentEntry(MT5, true);
		MemberTagCommentEntry mtce6 = new MemberTagCommentEntry(MT6, false);
		
		pafViewSection.setMemberTagCommentEntries(new MemberTagCommentEntry[] { mtce1, mtce2, mtce3, mtce4, mtce5, mtce6 });
		
		pafViewSectionUI = new PafViewSectionUI(pafViewSection);
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.view.PafViewSectionUI#updateMemberTags(com.pace.paf.db.membertags.MemberTagDef[])}.
	 */
	public void testUpdateMemberTagsWithNull() {		
		
		assertNotNull(pafViewSectionUI.getPafViewSection().getRowTuples());
		assertEquals(6, pafViewSectionUI.getPafViewSection().getRowTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getColTuples());
		assertEquals(6, pafViewSectionUI.getPafViewSection().getColTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries());
		
		pafViewSectionUI.updateMemberTags(null);
		
		assertNotNull(pafViewSectionUI.getPafViewSection().getRowTuples());
		assertEquals(1, pafViewSectionUI.getPafViewSection().getRowTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getColTuples());
		assertEquals(1, pafViewSectionUI.getPafViewSection().getColTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries());
		assertEquals(0, pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries().length);
		
	}
	
	public void testUpdateMemberTagsWithMemberTags() {				
		
		assertNotNull(pafViewSectionUI.getPafViewSection().getRowTuples());
		assertEquals(6, pafViewSectionUI.getPafViewSection().getRowTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getColTuples());
		assertEquals(6, pafViewSectionUI.getPafViewSection().getColTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries());
		
		pafViewSectionUI.updateMemberTags(memberTagDefs);
		
		assertNotNull(pafViewSectionUI.getPafViewSection().getRowTuples());
		assertEquals(3, pafViewSectionUI.getPafViewSection().getRowTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getColTuples());
		assertEquals(2, pafViewSectionUI.getPafViewSection().getColTuples().length);
		assertNotNull(pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries());
		assertEquals(3, pafViewSectionUI.getPafViewSection().getMemberTagCommentEntries().length);
		
		
	}
	
	private static PageTuple[] createPageTuples(String[] dims) {
		
		PageTuple[] pageTuples = new PageTuple[dims.length];
		
		int ndx = 0;
		for (String dim : dims ) {
		
			PageTuple pageTuple = new PageTuple();
			pageTuple.setAxis(dim);
			
			pageTuples[ndx++] = pageTuple;			
			
		}		
		
		return pageTuples;
				
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

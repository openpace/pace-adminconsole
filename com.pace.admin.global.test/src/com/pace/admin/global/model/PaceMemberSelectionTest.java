/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import com.pace.admin.global.enums.PaceTreeNodeSelectionType;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PaceMemberSelectionTest extends TestCase {

	
	String existingUserSelDesc = "@DESC(@USER_SEL(CLIMATE1))";
	String existingUserSelDescLevel = "@DESC(@USER_SEL(CLIMATE1), L0)";
	String existingUserSelDescGen = "@DESC(@USER_SEL(CLIMATE1), G1)";
	
	String existingUserSelIDesc = "@IDESC(@USER_SEL(CLIMATE1))";
	String existingUserSelIDescLevel = "@IDESC(@USER_SEL(CLIMATE1), L0)";
	String existingUserSelIDescGen = "@IDESC(@USER_SEL(CLIMATE1), G1)";
	
	String existingUserSelMembers = "@MEMBERS(@USER_SEL(CLIMATE1), ONE, TWO, THREE)";
		
	
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#PaceMemberSelection(java.lang.String[], com.pace.paf.adminconsole.global.enums.PaceTreeNodeSelectionType)}.
	 */
//	public void testPaceMemberSelection() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#getMembers()}.
	 */
//	public void testGetMembers() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#setMembers(java.lang.String[])}.
	 */
//	public void testSetMembers() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#addMember(java.lang.String)}.
	 */
//	public void testAddMember() {
//		fail("Not yet implemented");
//	}
//
//	/**
//	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#getMemberWithUserSelection()}.
//	 */
//	public void testGetMemberWithUserSelection() {
//		fail("Not yet implemented");
//	}
//
//	/**
//	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#getMemberWithSelection()}.
//	 */
//	public void testGetMemberWithSelection() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#createMemberSelectionFromExistingMember(java.lang.String)}.
	 */
	public void testCreateMemberSelectionFromExistingMember() {
		
		
PaceMemberSelection existingUserSelDescMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelDesc);
		
		assertNotNull(existingUserSelDescMemberSelection);
		assertEquals(existingUserSelDescMemberSelection.getMemberWithSelection(), existingUserSelDesc);
		assertEquals(existingUserSelDescMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelDescMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelDescMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelDescMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelDescMemberSelection.getLevelGenNumber(), 0);
		assertEquals(existingUserSelDescMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.Descendants);
		
		
		PaceMemberSelection existingUserSelDescLevelMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelDescLevel);
		
		assertNotNull(existingUserSelDescLevelMemberSelection);
		assertEquals(existingUserSelDescLevelMemberSelection.getMemberWithSelection(), existingUserSelDescLevel);
		assertEquals(existingUserSelDescLevelMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelDescLevelMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelDescLevelMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelDescLevelMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelDescLevelMemberSelection.getLevelGenNumber(), 0);
		assertEquals(existingUserSelDescLevelMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.DescLevel);
		
		PaceMemberSelection existingUserSelDescGenMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelDescGen);
		
		assertNotNull(existingUserSelDescGenMemberSelection);
		assertEquals(existingUserSelDescGenMemberSelection.getMemberWithSelection(), existingUserSelDescGen);
		assertEquals(existingUserSelDescGenMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelDescGenMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelDescGenMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelDescGenMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelDescGenMemberSelection.getLevelGenNumber(), 1);
		assertEquals(existingUserSelDescGenMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.DescGen);
		
		PaceMemberSelection existingUserSelIDescMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelIDesc);
		
		assertNotNull(existingUserSelIDescMemberSelection);
		assertEquals(existingUserSelIDescMemberSelection.getMemberWithSelection(), existingUserSelIDesc);
		assertEquals(existingUserSelIDescMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelIDescMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelIDescMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelIDescMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelIDescMemberSelection.getLevelGenNumber(), 0);
		assertEquals(existingUserSelIDescMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.IDescendants);
		
		PaceMemberSelection existingUserSelIDescLevelMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelIDescLevel);
		
		assertNotNull(existingUserSelIDescLevelMemberSelection);
		assertEquals(existingUserSelIDescLevelMemberSelection.getMemberWithSelection(), existingUserSelIDescLevel);
		assertEquals(existingUserSelIDescLevelMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelIDescLevelMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelIDescLevelMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelIDescLevelMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelIDescLevelMemberSelection.getLevelGenNumber(), 0);
		assertEquals(existingUserSelIDescLevelMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.IDescLevel);
		
		PaceMemberSelection existingUserSelIDescGenMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelIDescGen);
		
		assertNotNull(existingUserSelIDescGenMemberSelection);
		assertEquals(existingUserSelIDescGenMemberSelection.getMemberWithSelection(), existingUserSelIDescGen);
		assertEquals(existingUserSelIDescGenMemberSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");				
		assertEquals(existingUserSelIDescGenMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelIDescGenMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelIDescGenMemberSelection.isUserSelection(), true);
		assertEquals(existingUserSelIDescGenMemberSelection.getLevelGenNumber(), 1);
		assertEquals(existingUserSelIDescGenMemberSelection.getSelectionType(), PaceTreeNodeSelectionType.IDescGen);
		
		PaceMemberSelection existingUserSelMembersSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelMembers);
		
		assertNotNull(existingUserSelMembersSelection);
		assertEquals(existingUserSelMembersSelection.getMemberWithSelection(), existingUserSelMembers);
		assertEquals(existingUserSelMembersSelection.getMemberWithUserSelection(), "@USER_SEL(CLIMATE1)");				
		assertEquals(existingUserSelMembersSelection.getMembers().length, 4);
		assertEquals(existingUserSelMembersSelection.getMembers()[0], "@USER_SEL(CLIMATE1)");
		assertEquals(existingUserSelMembersSelection.getMembers()[1], "ONE");
		assertEquals(existingUserSelMembersSelection.getMembers()[2], "TWO");
		assertEquals(existingUserSelMembersSelection.getMembers()[3], "THREE");
		assertEquals(existingUserSelMembersSelection.isUserSelection(), false);
		assertEquals(existingUserSelMembersSelection.getLevelGenNumber(), 0);
		assertEquals(existingUserSelMembersSelection.getSelectionType(), PaceTreeNodeSelectionType.Members);
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceMemberSelection#isUserSelection()}.
	 */
//	public void testIsUserSelection() {
//		fail("Not yet implemented");
//	}

}

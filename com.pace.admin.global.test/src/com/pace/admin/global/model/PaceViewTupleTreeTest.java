/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.exceptions.InvalidStateException;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.base.PafException;
import com.pace.base.utility.PafImportExportUtility;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.ViewTuple;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PaceViewTupleTreeTest extends TestCase {

	String CONF_DIR = "C:\\adminconsole-test\\test\\eclipse\\junit-workspace\\Test Project\\conf\\";
	
	PafViewSection[] initialViewSections = null;
	
	List<PafViewSectionUI> pafViewSectionUIList = new ArrayList<PafViewSectionUI>();
	
	IProject junitProject = null;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		
		super.setUp();		
						
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		
		assertNotNull(workspace);
		assertEquals(workspace.getRoot().exists(), true);
						
		junitProject = workspace.getRoot().getProject("Test Project");
		
		assertNotNull(junitProject);
		
		if ( ! junitProject.exists() ) {
		
			IProjectDescription junitProjectDesc = ResourcesPlugin.getWorkspace().newProjectDescription(junitProject.getName());
			
			try {
				
				junitProject.create(junitProjectDesc, null);
							
				if ( ! junitProject.isOpen()) {
					try {
						junitProject.open(null);
					} catch (CoreException e) {
						
						e.printStackTrace();						
					}
				}
						
				
			} catch (CoreException e) {
				System.out.println("Problem creating or refreshing new project.  Error: " + e.getMessage());
			}
			
		}
						
		
		assertEquals(junitProject.exists(), true);
		
		File confDir = new File(CONF_DIR);
		confDir.mkdir();
		assertNotNull(confDir);
		assertEquals(confDir.exists(), true);
		
		//TODO FIXME this seems to be broken.
		//initialViewSections = PafImportExportUtility.importMemberTags(CONF_DIR, false);
	
		if ( initialViewSections != null ) {
			
			for (PafViewSection viewSection : initialViewSections) {
				
				pafViewSectionUIList.add(new PafViewSectionUI(viewSection));
				
			}
			
		}
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceViewTupleTree#PaceViewTupleTree(com.pace.paf.adminconsole.global.model.PaceTreeInput, com.pace.paf.view.ViewTuple[])}.
	 */
	public void testPaceViewTupleTreePaceTreeInputViewTupleArray() {
		
		for (PafViewSectionUI pafViewSectionUI : pafViewSectionUIList) {
			
			PaceTreeInput paceTreeInput = new PaceTreeInput(junitProject, pafViewSectionUI, null, PaceTreeAxis.Column, Arrays.asList(pafViewSectionUI.getColAxisDims()), null, null, null, false);
						
			try {
				PaceViewTupleTree paceViewTupleTree = new PaceViewTupleTree(paceTreeInput, pafViewSectionUI.getColTuples());
				
				assertNotNull(paceViewTupleTree);
				
			} catch (PafException e) {
				fail(e.getMessage());
			}
			
			paceTreeInput = new PaceTreeInput(junitProject, pafViewSectionUI, null, PaceTreeAxis.Row, Arrays.asList(pafViewSectionUI.getRowAxisDims()), null, null, null, false);
			
			try {
				PaceViewTupleTree paceViewTupleTree = new PaceViewTupleTree(paceTreeInput, pafViewSectionUI.getRowTuples());
				
				assertNotNull(paceViewTupleTree);
				
			} catch (PafException e) {
				fail(e.getMessage());
			}
		}
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.model.PaceViewTupleTree#getViewTuples()}.
	 */
	public void testGetViewTuples() {
		
		ViewSectionModelManager manager = new ViewSectionModelManager(junitProject);
		
		for (PafViewSectionUI pafViewSectionUI : pafViewSectionUIList) {
			
			PaceTreeInput paceTreeInput = null;
			
			for (PaceTreeAxis currentPaceTreeAxis : Arrays.asList(PaceTreeAxis.Column, PaceTreeAxis.Row)) {
				
				ViewTuple[] beforeViewTuples = null;
				
				List<String> axisDims = null;				
				
				switch(currentPaceTreeAxis) {
				case Column:
					beforeViewTuples = pafViewSectionUI.getColTuples();
					axisDims = Arrays.asList(pafViewSectionUI.getColAxisDims()); 
					break;
				case Row:
					beforeViewTuples = pafViewSectionUI.getRowTuples();
					axisDims = Arrays.asList(pafViewSectionUI.getRowAxisDims());
					break;
				}
				
				paceTreeInput = new PaceTreeInput(junitProject, pafViewSectionUI, null, currentPaceTreeAxis, axisDims, null, null, null, false);
								
				try {					
					
					PaceViewTupleTree paceViewTupleTree = new PaceViewTupleTree(paceTreeInput, beforeViewTuples);
					
					List<ViewTuple> afterViewTupleList = paceViewTupleTree.getViewTuples();
	
					assertEquals(beforeViewTuples.length, afterViewTupleList.size());
					
					for (int i = 0; i < beforeViewTuples.length; i++ ) {
						
						ViewTuple beforeViewTuple = beforeViewTuples[i];
						ViewTuple afterViewTuple = afterViewTupleList.get(i);
						
						assertEquals(beforeViewTuple.getMemberDefs().length, afterViewTuple.getMemberDefs().length);
						
						for (int j = 0; j < beforeViewTuple.getMemberDefs().length; j++) {
							
							assertEquals(beforeViewTuple.getMemberDefs()[j], afterViewTuple.getMemberDefs()[j]);
							
						}
						
						assertEquals(beforeViewTuple.getAxis(), afterViewTuple.getAxis());
						
					}
					
					switch(currentPaceTreeAxis) {
					case Column:
						pafViewSectionUI.setColTuples(afterViewTupleList.toArray(new ViewTuple[0]));
						break;
					case Row:
						pafViewSectionUI.setRowTuples(afterViewTupleList.toArray(new ViewTuple[0]));
						break;
					}
					
				} catch (PafException e) {
					fail(e.getMessage());
				} catch (InvalidStateException e) {
					fail(e.getMessage());
				}				
				
			}
			
			manager.replace(pafViewSectionUI.getName(), pafViewSectionUI);
			
		}
		
		manager.save();
		
	}

}

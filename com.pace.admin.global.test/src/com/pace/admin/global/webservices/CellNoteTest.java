/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.webservices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import junit.framework.TestCase;

import com.pace.server.client.SimpleCellNote;
import com.pace.server.client.SimpleCoordList;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class CellNoteTest extends TestCase {

	private static final int _123 = 123;

	private static final String JAVA_J = "JavaJ";

	private static final String CELL_NOTE_TEST = "Cell Note Test";

	private static final String DS1 = "DS1";

	private static final String APP1 = "App1";

	private CellNote globalCellNote = null;
	
	private SimpleCellNote globalSimpleCellNote = null;
	
	private List<String> axesOrDimensionList = new ArrayList(Arrays.asList(new String[] { "Product", "Location", "Version", "Measures"  }));
	
	private List<String> coordinatesOrMembersList = new ArrayList(Arrays.asList(new String[] { "Div09", "Store1", "WP", "SLS_AUR" }));
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		globalCellNote = createCellNote();
		
		assertNotNull(globalCellNote);
		
		globalSimpleCellNote = createSimpleCellNote();
		
		assertNotNull(globalSimpleCellNote);
	}

	/**
	 * 
	 * Creates a cell note and populates with data.
	 *
	 * @return Instance of a cell note.
	 */
	private CellNote createCellNote() {
		
		CellNote cellNote = new CellNote();
		cellNote.setApplicationName(APP1);
		cellNote.setDataSourceName(DS1);
		cellNote.setText(CELL_NOTE_TEST);
		cellNote.setCreator(JAVA_J);
		cellNote.setId(_123);
		cellNote.setVisible(Boolean.TRUE);
		
		try {
			cellNote.setLastUpdated(DatatypeFactory.newInstance().newXMLGregorianCalendar());
		} catch (DatatypeConfigurationException e) {
			fail(e.getMessage());
		}
		
		com.pace.base.comm.SimpleCoordList simpleCoordList = new com.pace.base.comm.SimpleCoordList();
		
		simpleCoordList.setAxis(axesOrDimensionList.toArray(new String[0]));
		simpleCoordList.setCoordinates(coordinatesOrMembersList.toArray(new String[0]));
		
		cellNote.setSimpleCoordList(simpleCoordList);
		
		return cellNote;
	}
	
	/**
	 * 
	 *  Creates a SimpleCellNote and populates with data
	 *
	 * @return
	 */
	private SimpleCellNote createSimpleCellNote() {
		
		SimpleCellNote simpleCellNote = new SimpleCellNote();
		simpleCellNote.setApplicationName(APP1);
		simpleCellNote.setDataSourceName(DS1);
		simpleCellNote.setText(CELL_NOTE_TEST);
		simpleCellNote.setCreator(JAVA_J);
		simpleCellNote.setId(_123);
		simpleCellNote.setVisible(Boolean.TRUE);
		
		try {
			simpleCellNote.setLastUpdated(DatatypeFactory.newInstance().newXMLGregorianCalendar());
		} catch (DatatypeConfigurationException e) {
			fail(e.getMessage());
		}
		
		SimpleCoordList simpleCoordList = new SimpleCoordList();
		simpleCoordList.getAxis().addAll(axesOrDimensionList);
		simpleCoordList.getCoordinates().addAll(coordinatesOrMembersList);
		simpleCellNote.setSimpleCoordList(simpleCoordList);
		
		return simpleCellNote;
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		globalCellNote = null;
		globalSimpleCellNote = null;
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.webservices.CellNote#CellNote(com.pace.paf.server.client.SimpleCellNote)}.
	 */
	public void testCellNote() {
		
		SimpleCellNote simpleCellNote = createSimpleCellNote();
		
		assertNotNull(simpleCellNote);
		assertEquals(simpleCellNote.getApplicationName(), APP1);
		assertEquals(simpleCellNote.getDataSourceName(), DS1);
		assertEquals(simpleCellNote.getText(), CELL_NOTE_TEST);
		assertEquals(simpleCellNote.getCreator(), JAVA_J);
		assertEquals(simpleCellNote.getId(), _123);
		assertEquals(simpleCellNote.isVisible(), true);
		assertNotNull(simpleCellNote.getSimpleCoordList());
		assertEquals(simpleCellNote.getSimpleCoordList().getAxis().size(), axesOrDimensionList.size());
		
		int ndx = 0;
		for (String axis : simpleCellNote.getSimpleCoordList().getAxis()) {
			
			assertEquals(axis, axesOrDimensionList.get(ndx++));
			
		}
		
		assertEquals(simpleCellNote.getSimpleCoordList().getCoordinates().size(), coordinatesOrMembersList.size());

		ndx = 0;
		for (String coord : simpleCellNote.getSimpleCoordList().getCoordinates()) {
			
			assertEquals(coord, coordinatesOrMembersList.get(ndx++));
			
		}
		
		CellNote cellNote = new CellNote(simpleCellNote);
		
		assertEquals(cellNote.getApplicationName(), APP1);
		assertEquals(cellNote.getDataSourceName(), DS1);
		assertEquals(cellNote.getText(), CELL_NOTE_TEST);
		assertEquals(cellNote.getCreator(), JAVA_J);
		assertEquals(cellNote.getId(), _123);
		assertEquals(cellNote.isVisible(), true);
		assertNotNull(cellNote.getSimpleCoordList());
		assertNotNull(cellNote.getSimpleCoordList().getAxis());
		assertNotNull(cellNote.getSimpleCoordList().getCoordinates());
		assertEquals(cellNote.getSimpleCoordList().getAxis().length, axesOrDimensionList.size());
		
		ndx = 0;
		for (String axis : cellNote.getSimpleCoordList().getAxis()) {
			
			assertEquals(axis, axesOrDimensionList.get(ndx++));
			
		}
		
		assertEquals(cellNote.getSimpleCoordList().getCoordinates().length, coordinatesOrMembersList.size());
		
		ndx = 0;
		for (String coord : cellNote.getSimpleCoordList().getCoordinates()) {
			
			assertEquals(coord, coordinatesOrMembersList.get(ndx++));
			
		}
		
	}

	/**
	 * Test method for {@link com.pace.paf.adminconsole.global.webservices.CellNote#getSimpleCellNote()}.
	 */
	public void testGetSimpleCellNote() {
		
		CellNote cellNote = createCellNote();
		
		assertNotNull(cellNote);
		assertEquals(cellNote.getApplicationName(), APP1);
		assertEquals(cellNote.getDataSourceName(), DS1);
		assertEquals(cellNote.getText(), CELL_NOTE_TEST);
		assertEquals(cellNote.getCreator(), JAVA_J);
		assertEquals(cellNote.getId(), _123);
		assertEquals(cellNote.isVisible(), true);
		assertNotNull(cellNote.getSimpleCoordList());
		assertNotNull(cellNote.getSimpleCoordList().getAxis());
		assertNotNull(cellNote.getSimpleCoordList().getCoordinates());
		assertEquals(cellNote.getSimpleCoordList().getAxis().length, axesOrDimensionList.size());
		
		int ndx = 0;
		for (String axis : cellNote.getSimpleCoordList().getAxis()) {
			
			assertEquals(axis, axesOrDimensionList.get(ndx++));
			
		}
		
		assertEquals(cellNote.getSimpleCoordList().getCoordinates().length, coordinatesOrMembersList.size());
		
		ndx = 0;
		for (String coord : cellNote.getSimpleCoordList().getCoordinates()) {
			
			assertEquals(coord, coordinatesOrMembersList.get(ndx++));
			
		}
		
		SimpleCellNote simpleCellNote = cellNote.getSimpleCellNote();
		
		assertNotNull(simpleCellNote);
		assertEquals(simpleCellNote.getApplicationName(), APP1);
		assertEquals(simpleCellNote.getDataSourceName(), DS1);
		assertEquals(simpleCellNote.getText(), CELL_NOTE_TEST);
		assertEquals(simpleCellNote.getCreator(), JAVA_J);
		assertEquals(simpleCellNote.getId(), _123);
		assertEquals(simpleCellNote.isVisible(), true);
		assertNotNull(simpleCellNote.getSimpleCoordList());
		assertEquals(simpleCellNote.getSimpleCoordList().getAxis().size(), axesOrDimensionList.size());
		
		ndx = 0;
		for (String axis : simpleCellNote.getSimpleCoordList().getAxis()) {
			
			assertEquals(axis, axesOrDimensionList.get(ndx++));
			
		}
		
		assertEquals(simpleCellNote.getSimpleCoordList().getCoordinates().size(), coordinatesOrMembersList.size());

		ndx = 0;
		for (String coord : simpleCellNote.getSimpleCoordList().getCoordinates()) {
			
			assertEquals(coord, coordinatesOrMembersList.get(ndx++));
			
		}
		
	}

}

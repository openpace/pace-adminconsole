/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.springframework.util.StopWatch;

import com.pace.admin.menu.views.MenuView;

/**
 * Checks and performs a background conversion of any workspace projects. 
 * 
 * @author themoosman
 *
 */
public class UpgradeProjectJob extends Job {
	
	private static final Logger logger = Logger.getLogger(UpgradeProjectJob.class);
	private MenuView viewer;
	private IProject project;
	
	public UpgradeProjectJob(MenuView viewer, IProject project, int priority, int scheduleDelay, UpgradeProjectJobChangeAdapter changeAdapter) {
		super("Upgrading Pace Project XML");
		this.viewer = viewer;
		this.project = project;
		this.setPriority(priority);
		this.addJobChangeListener(changeAdapter);
		this.schedule(scheduleDelay);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {

		logger.info("Checking upgrade status of xml projects.");

		StopWatch sw = new StopWatch();
		sw.start();
		
		viewer.autoConvertProject(project);

		sw.stop();
		logger.info(String.format("Took %s (ms) to convert XML project", sw.getTotalTimeMillis()));
		
		return Status.OK_STATUS;
	}

}

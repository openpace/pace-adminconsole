/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.job;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.IProgressConstants;

import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.actions.CompletionAction;
import com.pace.admin.menu.actions.importexport.ImportExportAction;

public class ClientJob extends Job {

	/**	
	 A family identifier for all test jobs
	*/
	public static final Object FAMILY_TEST_JOB = new Object();
	private static Logger logger = Logger.getLogger(ClientJob.class);
	private CompletionAction completedAction;
	private Action actionToRun;
	

	/**
	   * @param name
	   * @param completedAction
	   */
	  public ClientJob(String name, CompletionAction completedAction, Action actionToRun) {
	    super(name);
	    this.completedAction = completedAction;
	    this.actionToRun = actionToRun;
	  }
	  
	  public boolean belongsTo(Object family) {
	         if(family instanceof ClientJob){
	        	 ClientJob cj = (ClientJob) family;
	        	 if(cj.getName().equals(this.getName())){
	        		 return true;
	        	 }
	  		}
	         return family == FAMILY_TEST_JOB;
	      }


	  protected IStatus run(IProgressMonitor monitor) {
	    // activate the progress bar with an unknown amount of task work
	    monitor.beginTask("", IProgressMonitor.UNKNOWN);
	    // perform the job
	    try {
	      // execute task work...
	      actionToRun.run();
	      Thread.sleep(500);
	      // at the end of the successfully ended work, set the completion
	      if(actionToRun instanceof ImportExportAction){
	    	  ImportExportAction action = (ImportExportAction) actionToRun;
	    	  if(action.getThrowable() != null){
	    		  throw new Exception(action.getThrowable());
	    	  }
	      }
	    	  
	      // task to be ok
	      completedAction.setOk(true);
	    } catch (Exception e1) {
	      logger.error(e1, e1);
	      // if the work failed then set the completion
	      // task to be NOT ok and set the exception so it
	      // can be shown to the user
	      completedAction.setThrowable(e1);
	      completedAction.setOk(false);
	    }
	    // stop the monitor
	    monitor.done();
	    // execute the completion task
	    complete();
	    return Status.OK_STATUS;
	  }

	  /**
	   * completes the task by showing the user a dialog about the execution
	   * state of the job
	   */
	  protected void complete() {
	    setProperty(IProgressConstants.ICON_PROPERTY, MenuPlugin.getImageDescriptor("/icons/check.png"));
	    Boolean isModal = (Boolean) this.getProperty(IProgressConstants.PROPERTY_IN_DIALOG);
	    if (isModal != null && isModal.booleanValue() && completedAction != null && completedAction.isShowDialog()) {
	      // The progress dialog is still open so
	      // just open the message
	      showResults(completedAction);
	    } else {
	      //setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
	      setProperty(IProgressConstants.KEEPONE_PROPERTY, Boolean.TRUE);
	      setProperty(IProgressConstants.ACTION_PROPERTY, completedAction);
	    }
	  }

	  /**
	   * Asynchronous execution of an {@link Action}
	   * @param action
	   */
	  protected static void showResults(final Action action) {
	    Display.getDefault().asyncExec(new Runnable() {
	      public void run() {
	        action.run();
	      }
	    });
	  }
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.job;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.swt.widgets.Display;
import org.springframework.util.StopWatch;

import com.pace.admin.menu.views.MenuView;

public class UpgradeProjectJobChangeAdapter extends JobChangeAdapter {
	
	private static final Logger logger = Logger.getLogger(UpgradeProjectJobChangeAdapter.class);
	private MenuView viewer;
	private int threadCount; 
	private int counter;
	private StopWatch sw;
	
	public UpgradeProjectJobChangeAdapter(MenuView viewer, int threadCount) {
		super();
		this.viewer = viewer;
		this.threadCount = threadCount;
		this.counter = 0;
		this.sw = new StopWatch();
		sw.start();
	}

	@Override
	public void done(IJobChangeEvent event) {
		super.done(event);
		synchronizedDone(event);
	}
	
	private synchronized void synchronizedDone(IJobChangeEvent event) {

		counter++;
		
		boolean done = (threadCount <= counter);
		
		if(!done) return;
		
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				viewer.getViewer().refresh();
			}
		});
		
		
		sw.stop();
		
		logger.info(String.format("UpgradeProjectsJob complete, loaded %s projects in %s (ms)", counter, sw.getTotalTimeMillis()));
    }
	
}

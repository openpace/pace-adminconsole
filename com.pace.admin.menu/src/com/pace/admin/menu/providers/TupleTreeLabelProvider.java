/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.providers;

import java.io.File;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.SortOrder;
import com.pace.base.SortPriority;

public class TupleTreeLabelProvider extends LabelProvider {

	public String getText(Object element) {
		
		if ( element instanceof PaceTreeNode ) {
			
			return ((PaceTreeNode) element).getDisplayName();
			
		}
		
		return element.toString();
	}
	
	public Image getImage(Object element) {
		if( element instanceof PaceTreeNode ){
			// TTN-2513 Scenario where both sorting and freeze panes are enabled on a tuple. Display a composite icon.
			if( ((PaceTreeNode)element).getProperties().getSortPriority() != null 
					&& ((PaceTreeNode)element).getProperties().getSortOrder() != null && ((PaceTreeNode)element).getProperties().isFreezePaneSet()) {
				
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Primary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons\\up_one_lock.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Primary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons\\down_one_lock.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Secondary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons\\up_two_lock.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Secondary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons\\down_two_lock.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Tertiary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons\\up_three_lock.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Tertiary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons\\down_three_lock.png").createImage();
				
			}
			
			if( ((PaceTreeNode)element).getProperties().getSortPriority() != null 
					&& ((PaceTreeNode)element).getProperties().getSortOrder() != null ) {
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Primary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "up_one.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Primary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "down_one.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Secondary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "up_two.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Secondary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "down_two.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Tertiary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Ascending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "up_three.png").createImage();
				if( ((PaceTreeNode)element).getProperties().getSortPriority().equals(SortPriority.Tertiary) 
						&& ((PaceTreeNode)element).getProperties().getSortOrder().equals(SortOrder.Descending) )
					return MenuPlugin.getImageDescriptor("icons" + File.separator + "down_three.png").createImage();
				
				
				
			}
			
			

		if( ((PaceTreeNode)element).getProperties().isFreezePaneSet()) 
			return MenuPlugin.getImageDescriptor("icons" + File.separator + "lock-16.png").createImage();
		}
			return null;
		}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.providers;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class MyStyledLabelProvider extends WorkbenchLabelProvider implements IStyledLabelProvider{

	//The MyStyledLabelProvider is simply a custom class that extends the WorkbenchLabelProvider and implements IStyledLabelProvider. You have to override the getStyledText(..) method.
	//#############
	public StyledString getStyledText(final Object element)
	{
		String text = getText(element);
		if (text != null)
		{
		return new StyledString(text, new Styler() {

			/**
			* @see org.eclipse.jface.viewers.StyledString.Styler#applyStyles(or g.eclipse.swt.graphics.TextStyle)
			*/
			@Override
			public void applyStyles(TextStyle textStyle)
			{
				Color foreground = MyStyledLabelProvider.this.getForeground(element);
				if (foreground != null)
					textStyle.foreground = foreground;
				Color background = MyStyledLabelProvider.this.getBackground(element);
				if (background != null)
					textStyle.background = background;
				}
			});
		}
		return null;
	}
		
}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.providers;

import java.io.File;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.menu.MenuPlugin;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PaceTreeLabelProvider extends LabelProvider {
	
	public String getText(Object element) {
		
		if ( element instanceof PaceTreeNode ) {
			
			return ((PaceTreeNode) element).getDisplayName();
			
		}
		
		
		return element.toString();
	}
	
	public Image getImage(Object element) {
		// TTN-2513 Set the image for the freeze panes for the row axis.
		if( element instanceof PaceTreeNode ){
			if( ((PaceTreeNode)element).getProperties().isFreezePaneSet()) 

				return MenuPlugin.getImageDescriptor("icons" + File.separator + "lock-16.png").createImage();

		}
		return null;
	}
}
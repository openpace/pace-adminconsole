/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.providers;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.pace.base.comm.CustomMenuDef;

/**
* Extenstion of LabelProvider for the CustomMenuDef object.
* @author kmoos
* @version	x.xx
*/
public class CustomMenuDefLabelProvider extends LabelProvider {
	
	/**
	 * Override LabelProvider.getText()
	 * return The caption of the CustomMenuDef
	 * @param element
	 * @return String
	 */
	public String getText(Object element) {
		return ((CustomMenuDef) element).getCaption();
	}

	/**
	 * Gets the key of a CustomMenuDef
	 * @param element The CustomMenuDef object.
	 * @return The Key of the CustomMenuDef.
	 */
	public String getKey(Object element) {
		return ((CustomMenuDef) element).getKey();
	}
	
	/**
	 * Override LabelProvider.getImage()
	 * Always returns null.
	 * @param element
	 * @return Image
	 */
	public Image getImage(Object element) {
		return null;
	}
}


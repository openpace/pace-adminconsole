/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.providers;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.pace.admin.global.model.MemberListReorderInput;
import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PageTupleList;
import com.pace.admin.global.model.PageTupleNode;

public class PaceListContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			
			if ( inputElement instanceof PageTupleList) {
				
				List<PageTupleNode> pageTupleNodeList = ((PageTupleList) inputElement).getPageTupleNodes(); 
				
				Object[] objectAr = new Object[pageTupleNodeList.size()];
				
				int i = 0;
				
				for (PageTupleNode pageTupleNode : pageTupleNodeList ) {
					
					if ( pageTupleNode != null) {
					
						objectAr[i] = pageTupleNode;
						
					} else {
						
						objectAr[i] = new PageTupleNode(null, "", null);
						
					}
					
					
					
					i++;
				}
				
				return objectAr;
				
			} else if( inputElement instanceof MemberListReorderInput ){
				
				List<PaceTreeNode> pageTupleNodeList = ((MemberListReorderInput) inputElement).getMembers(); 
				
				Object[] objectAr = new Object[pageTupleNodeList.size()];
				
				int i = 0;
				
				for (PaceTreeNode pageTupleNode : pageTupleNodeList ) {
					
					if ( pageTupleNode != null) {
					
						objectAr[i] = pageTupleNode;
						
					} else {
						
						objectAr[i] = new PageTupleNode(null, "", null);
						
					}
					
					i++;
				}
				
				return objectAr;
				
			}
			
			return new Object[] { "item_0", "item_1", "item_2" };
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
}
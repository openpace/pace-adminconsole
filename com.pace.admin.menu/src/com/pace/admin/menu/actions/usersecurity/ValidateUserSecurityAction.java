/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.usersecurity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.global.security.PaceUser;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.webservices.DomainFilter;
import com.pace.admin.global.widgets.StringSelectionDialog;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.IDBUsersNode;
import com.pace.admin.servers.nodes.LDAPUserNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.DomainNameParser;
import com.pace.server.client.PafNotAbletoGetLDAPContext_Exception;
import com.pace.server.client.PafService;
import com.pace.server.client.SecurityGroup;

/**
* 
* Validates a projects user security.  Compares all the paf_security.xml entries against
* the server's username for both native and Pace Groups.  The Pace Groups are queried using
* the getPaceGroups and then the getUsernameForSecurityGroups to get the user names for 
* the selecte security groups.
* 
* 
* @author jmilliron
* @version x.xx
*/
public class ValidateUserSecurityAction extends Action implements ISelectionListener {

	//const
	private static final String VALIDATE_SECURITY = "Reconcile Users with Server";

	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger(ValidateUserSecurityAction.class);
	
	/**
	 * Unique id for the action.
	 */
	public static final String ID = "com.pace.admin.menu.actions.usersecurity.ValidateUserSecurityAction";
			
	private IWorkbenchWindow window = null;
	
	private IProject project;
	
	private UsersSecurityNode usersSecurityNode;
	
	private String url;
	
	private TreeViewer viewer;

	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public ValidateUserSecurityAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
        this.setText(VALIDATE_SECURITY);
        this.setEnabled(false);
        this.setId(ID);
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	

	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	
	public void run() {
				
//		if project exists 
		if ( project != null ) {
			
			//get server from project util
			PafServer server = null;
			try {
				server = PafProjectUtil.getProjectServer(project);
				this.url = server.getCompleteWSDLService();
			} catch (PafServerNotFound e) {
				// TODO Auto-generated catch block
				logger.warn(e.getMessage());						
				e.printStackTrace();
				MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, e.getMessage());
				return;
			}
			
			boolean authenticated = SecurityManager.isAuthenticated(url);
			//if not authenticated, prompt user to log in
			if ( server != null ) {
				if( ! authenticated ) {
					try {
						ServerView.authWithLoginDialogIfNotAuthed(server,false);
					} catch (ServerNotRunningException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, e.getMessage());
						return;
					}
				}
			}
			//if authenticated
			if ( SecurityManager.isAuthenticated(url) ) {
				
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

				public void run() {
			
					//set to hold invalid user names for domains
					Set<String> invalidSecurityUserDomainNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
					Set<String> invalidSecurityUserNativeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
					//key = domain name; value = set of user names
					Map<String, Set<String>> projectDomainUserNameMap = getProjectDomainUserNameMap();
					
					//key = domain name; value = set of user names
					Map<String, Set<String>> serverDomainUserNameMap = getServerDomainUserNameMap();
					
					//for all project domains, verify the user names for project are valid
					for (String domainName : projectDomainUserNameMap.keySet() ) {
						
						Set<String> projectUserNameSet = projectDomainUserNameMap.get(domainName);
						
						//if server doesn't contain a project domain, invalidate all users for that domain
						if ( ! serverDomainUserNameMap.containsKey(domainName)) {
							
							if ( projectUserNameSet != null ) {
								
								for (String invalidProjectUserName : projectUserNameSet ) {
									
									if ( domainName.equalsIgnoreCase(PafBaseConstants.Native_Domain_Name) ) {
										
										invalidSecurityUserNativeSet.add(invalidProjectUserName + DomainNameParser.AT_TOKEN + domainName);
										
									} else {
									
										invalidSecurityUserDomainNameSet.add(invalidProjectUserName + DomainNameParser.AT_TOKEN + domainName);
										
									}
																											
								}					
							}
							
						} else {
							
							//if set is not null, verify usernames are in server set
							if ( projectUserNameSet != null ) {
						
								Set<String> serverUserNameForDomainSet = serverDomainUserNameMap.get(domainName);
								
								if ( serverUserNameForDomainSet == null ) {
									
									serverUserNameForDomainSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
									
								}
								
								for (String projectUserName : projectUserNameSet ) {
								
									if (! serverUserNameForDomainSet.contains(projectUserName)) {
										
										if ( domainName.equalsIgnoreCase(PafBaseConstants.Native_Domain_Name) ) {
											
											invalidSecurityUserNativeSet.add(projectUserName + DomainNameParser.AT_TOKEN + domainName);
											
										} else {
										
											invalidSecurityUserDomainNameSet.add(projectUserName + DomainNameParser.AT_TOKEN + domainName);
											
										}
																				
									}
									
								}						
								
							}
							
						}
						
					}				
					
					if ( invalidSecurityUserDomainNameSet.size() > 0 ) {
												
						try {
							
							String[] invalidDomainUserNames = SecurityManager.validateUsers(url, invalidSecurityUserDomainNameSet.toArray(new String[0]));
							
							invalidSecurityUserDomainNameSet.clear();
							
							if ( invalidDomainUserNames != null && invalidDomainUserNames.length > 0 ) {

								invalidSecurityUserDomainNameSet.addAll(Arrays.asList(invalidDomainUserNames));
								
							}						
							
						} catch (PafException e) {
							MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "User validation could not be complted because an error occurred on the server: " + e.getMessage());
							return;
						} catch (PafNotAbletoGetLDAPContext_Exception e) {
							MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "User validation could not be complted because a connection could not be made to the LDAP server.");
							return;
						}						
						
					}				
					
					//add invalid native users to invalidSecurityDoman set
					invalidSecurityUserDomainNameSet.addAll(invalidSecurityUserNativeSet);
					
					//if invalid security user names exists
					if ( invalidSecurityUserDomainNameSet.size() > 0 ) {
											
						String[] invalidDomainUsers = invalidSecurityUserDomainNameSet.toArray(new String[0]);
					
						StringSelectionDialog dialog = new StringSelectionDialog(window.getShell(),
								"Invalid User Security", "Invalid User Security", 
								"To remove the invalid user security profiles, check the ones that need to be removed from the security file and then click OK button.", invalidDomainUsers, invalidDomainUsers);						
						
						//open dialog and if user clicks ok...
						if ( dialog.open() == IDialogConstants.OK_ID ) {
							
							String[] selectedUsersToDelete = dialog.getSelectedItems();
							
							if ( selectedUsersToDelete != null ) {
							
								UserSecurityModelManager modelManager = new UserSecurityModelManager(project);
							
								//loop and remove all project users
								for (String domainUserToDelete : selectedUsersToDelete ) {
									
									DomainNameParser dnp = new DomainNameParser(domainUserToDelete);
									
									PafUserSecurity pusy = new PafUserSecurity();
									
									pusy.setUserName(dnp.getUserName());
									pusy.setDomainName(dnp.getDomainName());
									
									modelManager.remove(pusy.getKey());
									
								}	
								
								modelManager.save();
								
								usersSecurityNode.createChildren(null);
								
								if ( viewer != null ) {
									
									viewer.refresh(usersSecurityNode);
									
								}
								
								
							}							
							
						}
					
					//else is valid and then display message to user
					} else {
						
						MessageDialog.openInformation(window.getShell(), Constants.DIALOG_INFO_HEADING, "User Security for project '" + project.getName() + "' is valid.");
						
					}
				}});
			}												
		}
	}
	
	/*
	private void validateUserSecurity() {
		 	
		//if project exists and server is running
		if ( project != null && WebServicesUtil.isServerRunning(url) ) {
			
			try {
				
				//get server from project util
				PafServer server = PafProjectUtil.getProjectServer(project);
				
				//if not authenticated, prompt user to log in
				if ( server != null && ! SecurityManager.isAuthenticated(url) ) {
					
					final Shell shell = window.getShell();
					final String serverName = server.getName();					
					
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

						public void run() {

							LoginDialog loginDialog = null;
							
							try {
								loginDialog = SecurityManager.getLoginDialog(shell, serverName);
								
								loginDialog.setRefreshServerTree(true);
								
								loginDialog.open();
								
							} catch (ServerNotRunningException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
																					
						}});
										
				}
				
				//if authenticated
				if ( SecurityManager.isAuthenticated(url) ) {
					
					//set to hold invalid user names for domains
					Set<String> invalidSecurityUserDomainNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
					Set<String> invalidSecurityUserNativeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
					//key = domain name; value = set of user names
					Map<String, Set<String>> projectDomainUserNameMap = getProjectDomainUserNameMap();
					
					//key = domain name; value = set of user names
					Map<String, Set<String>> serverDomainUserNameMap = getServerDomainUserNameMap();
					
					//for all project domains, verify the user names for project are valid
					for (String domainName : projectDomainUserNameMap.keySet() ) {
						
						Set<String> projectUserNameSet = projectDomainUserNameMap.get(domainName);
						
						//if server doesn't contain a project domain, invalidate all users for that domain
						if ( ! serverDomainUserNameMap.containsKey(domainName)) {
							
							if ( projectUserNameSet != null ) {
								
								for (String invalidProjectUserName : projectUserNameSet ) {
									
									if ( domainName.equalsIgnoreCase(PafBaseConstants.Native_Domain_Name) ) {
										
										invalidSecurityUserNativeSet.add(invalidProjectUserName + DomainNameParser.AT_TOKEN + domainName);
										
									} else {
									
										invalidSecurityUserDomainNameSet.add(invalidProjectUserName + DomainNameParser.AT_TOKEN + domainName);
										
									}
																											
								}					
							}
							
						} else {
							
							//if set is not null, verify usernames are in server set
							if ( projectUserNameSet != null ) {
						
								Set<String> serverUserNameForDomainSet = serverDomainUserNameMap.get(domainName);
								
								if ( serverUserNameForDomainSet == null ) {
									
									serverUserNameForDomainSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
									
								}
								
								for (String projectUserName : projectUserNameSet ) {
								
									if (! serverUserNameForDomainSet.contains(projectUserName)) {
										
										if ( domainName.equalsIgnoreCase(PafBaseConstants.Native_Domain_Name) ) {
											
											invalidSecurityUserNativeSet.add(projectUserName + DomainNameParser.AT_TOKEN + domainName);
											
										} else {
										
											invalidSecurityUserDomainNameSet.add(projectUserName + DomainNameParser.AT_TOKEN + domainName);
											
										}
																				
									}
									
								}						
								
							}
							
						}
						
					}				
					
					if ( invalidSecurityUserDomainNameSet.size() > 0 ) {
												
						try {
							
							String[] invalidDomainUserNames = SecurityManager.validateUsers(url, invalidSecurityUserDomainNameSet.toArray(new String[0]));
							
							invalidSecurityUserDomainNameSet.clear();
							
							if ( invalidDomainUserNames != null && invalidDomainUserNames.length > 0 ) {

								invalidSecurityUserDomainNameSet.addAll(Arrays.asList(invalidDomainUserNames));
								
							}						
							
						} catch (PafException e) {
							MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "User validation could not be complted because an error occurred on the server: " + e.getMessage());
							return;
						} catch (PafNotAbletoGetLDAPContext_Exception e) {
							MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "User validation could not be complted because a connection could not be made to the LDAP server.");
							return;
						}						
						
					}				
					
					//add invalid native users to invalidSecurityDoman set
					invalidSecurityUserDomainNameSet.addAll(invalidSecurityUserNativeSet);
					
					//if invalid security user names exists
					if ( invalidSecurityUserDomainNameSet.size() > 0 ) {
											
						String[] invalidDomainUsers = invalidSecurityUserDomainNameSet.toArray(new String[0]);
					
						StringSelectionDialog dialog = new StringSelectionDialog(window.getShell(),
								"Invalid User Security", "Invalid User Security", 
								"To remove the invalid user security profiles, check the ones that need to be removed from the security file and then click OK button.", invalidDomainUsers, invalidDomainUsers);						
						
						//open dialog and if user clicks ok...
						if ( dialog.open() == IDialogConstants.OK_ID ) {
							
							String[] selectedUsersToDelete = dialog.getSelectedItems();
							
							if ( selectedUsersToDelete != null ) {
							
								UserSecurityModelManager modelManager = new UserSecurityModelManager(project);
							
								//loop and remove all project users
								for (String domainUserToDelete : selectedUsersToDelete ) {
									
									DomainNameParser dnp = new DomainNameParser(domainUserToDelete);
									
									PafUserSecurity pusy = new PafUserSecurity();
									
									pusy.setUserName(dnp.getUserName());
									pusy.setDomainName(dnp.getDomainName());
									
									modelManager.remove(pusy.getKey());
									
								}	
								
								modelManager.save();
								
								usersSecurityNode.createChildren(null);
								
								if ( viewer != null ) {
									
									viewer.refresh(usersSecurityNode);
									
								}
								
								
							}							
							
						}
					
					//else is valid and then display message to user
					} else {
						
						MessageDialog.openInformation(window.getShell(), Constants.DIALOG_INFO_HEADING, "User Security for project '" + project.getName() + "' is valid.");
						
					}
									
					
				}
												
				
			} catch (PafServerNotFound e) {
				
				logger.warn(e.getMessage());						

			}
			
		}	
		
	}
	*/
	
	/**
	 * 
	 * Creates a map of domains/usernames from the server.  The Map key is domain name
	 * and the value is a Set of usernames.  The GetPaceGroups method is called meaning that 
	 * only Pace Groups are used to retrieve the usernames for the domain.  
	 *
	 * @return
	 */
	private Map<String, Set<String>> getServerDomainUserNameMap() {

		Map<String, Set<String>> serverDomainUserNameMap = new HashMap<String, Set<String>>();
		
		if ( url != null ) {
			
			PafService service = WebServicesUtil.getPafService(url);
			
			if ( service != null ) {
										
				try {
					
					SecurityGroup[] paceSecurityGroups = SecurityManager.getPaceGroups(url);
											
					Map<String, Set<String>> domainSecurityGroupMap = WebServicesUtil.createDomainSecurityGroupMap(paceSecurityGroups);
					
					Set<String> nativeDomainEntry = new HashSet<String>();
					
					nativeDomainEntry.add(PafBaseConstants.Native_Domain_Name);
					
					domainSecurityGroupMap.put(PafBaseConstants.Native_Domain_Name, nativeDomainEntry);
					
					DomainFilter[] domainFilters = new DomainFilter[domainSecurityGroupMap.size()];
					
					int domainFltrNdx = 0;
					
					for (String domainName : domainSecurityGroupMap.keySet() ) {
																								
						Set<String> paceSecurityGroupSet = domainSecurityGroupMap.get(domainName);
						
						if ( paceSecurityGroupSet != null ) {
						
							DomainFilter df = new DomainFilter();							
							
							df.setDomainName(domainName);
							df.getSecurityGroupSet().addAll(paceSecurityGroupSet);
							
							domainFilters[domainFltrNdx++] = df;
							
						}						
						
					}	
					
					//Map<String, Map<String, Set<PaceUser>>> domainSecurityGroupUserNameMap = SecurityManager.getUserNamesBySecurityGroups(url, domainFilters);
					
					Map<String, Map<String, Set<PaceUser>>> domainSecurityGroupUserNameMap = SecurityManager.getCachedUserNamesBySecurityGroups(url);
					
					for (String domainName : domainSecurityGroupUserNameMap.keySet()) {
						
						Map<String, Set<PaceUser>> paceUserSecurityGroupMap = domainSecurityGroupUserNameMap.get(domainName);
						
						if ( paceUserSecurityGroupMap != null ) {
							
							Set<String> domainNameUserSet = serverDomainUserNameMap.get(domainName);
							
							if ( domainNameUserSet == null ) {
								
								domainNameUserSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
								
							}
							
							for (Set<PaceUser> paceUserSet : paceUserSecurityGroupMap.values()) {
								
								for (PaceUser paceUser : paceUserSet ) {
									
									domainNameUserSet.add(paceUser.getUserName());
									
								}
								
							}
							
							serverDomainUserNameMap.put(domainName, domainNameUserSet);
							
						}
					}
					
				} catch (PafException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
		return serverDomainUserNameMap;
	}

	/**
	 * 
	 * Creates a map of domain name/users from the project.
	 *
	 * @return
	 */
	private Map<String, Set<String>> getProjectDomainUserNameMap() {
		
		Map<String, Set<String>> projectDomainUserNameMap = new HashMap<String, Set<String>>();
		
		if ( project != null ) {
			
			UserSecurityModelManager modelManager = new UserSecurityModelManager(project);
			
			for (String key : modelManager.getKeySet() ) {
				
				PafUserSecurity pafUserSecurity = (PafUserSecurity) modelManager.getItem(key);
				
				if ( pafUserSecurity != null ) {
					
					String domainName = pafUserSecurity.getDomainName();
					
					if ( domainName == null || domainName.trim().length() == 0 	) {
						
						domainName = PafBaseConstants.Native_Domain_Name;
						
					}
					
					Set<String> userNameSet = null;
					
					if ( projectDomainUserNameMap.containsKey(domainName)) {
						
						userNameSet = projectDomainUserNameMap.get(domainName);
						
					}
					
					if ( userNameSet == null ) {
						
						userNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
						
					}
					
					userNameSet.add(pafUserSecurity.getUserName());
					
					projectDomainUserNameMap.put(domainName, userNameSet);							
					
				}		
				
				
			}
		}
		
		return projectDomainUserNameMap;
	}
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incomming) {
		
		if (incomming instanceof IStructuredSelection) {
			
			IStructuredSelection selection = (IStructuredSelection) incomming;
						
//			get the first selection.
			Object item = selection.getFirstElement();
			
			if ( item instanceof UsersSecurityNode ) {
			
				//convert the node to a PlannerRolesNode.
				usersSecurityNode = (UsersSecurityNode) item;
				
				updateEnabled();
				
			}else if ( item instanceof UserSecurityNode ) { //TTN-2346
				
				if(logger.isDebugEnabled()){
					logger.debug(item.getClass().toString());
					logger.debug(((TreeNode) item).getParent().getClass().toString());
					logger.debug(((TreeNode) item).getParent().getParent().getClass().toString());
				}

				usersSecurityNode = (UsersSecurityNode) ((TreeNode) item).getParent().getParent();
								
				updateEnabled();
				
			} else if ( item instanceof LDAPUserNode ) { //TTN-2346
				
				if(logger.isDebugEnabled()){
					logger.debug(item.getClass().toString());
					logger.debug(((com.pace.admin.servers.nodes.TreeNode) item).getParent().getClass().toString());
					logger.debug(((com.pace.admin.servers.nodes.TreeNode) item).getParent().getParent().getClass().toString());
				}
				//What the hell is this trying to do.
				//This piece of code is attempting to coast a LDAPUsersNode (which extends com.pace.admin.servers.nodes.TreeNode)
				//To a UserSecurityNode (which extends com.pace.admin.menu.nodes.TreeNode)
				//In theory that will never work.
				//TTN-2346 - Removed this cast and just run the code. 
				//usersSecurityNode = (UsersSecurityNode) ((TreeNode) item).getParent().getParent();
								
				updateEnabled();
				
			}
		}

	}
	
	/**
	 * 
	 * Enables the action if the server is running, otherwise action is disabled
	 *
	 */
	private void updateEnabled() {
		
		if ( project != null) {
			
			try {
				
				if ( ServerMonitor.getInstance().isServerRunning(PafProjectUtil.getProjectServer(project)) ) {
					
					setEnabled(true);
					
				} else {
					
					setEnabled(false);
				}
				
			} catch (PafServerNotFound e) {
				
				logger.warn(e.getMessage());						

			}
			
		}
		
	}
	
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
		
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
		updateEnabled();
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

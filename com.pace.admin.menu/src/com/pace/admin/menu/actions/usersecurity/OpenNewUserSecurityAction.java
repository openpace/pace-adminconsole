/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.usersecurity;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.UserSecurityEditor;
import com.pace.admin.menu.editors.input.UserSecurityEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.exceptions.InvalidUserSecurityException;
import com.pace.admin.menu.nodes.UserSecurityDomainNode;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;
import com.pace.base.ui.PafServer;

/**
* Action, opens an existing PlannerRole in a PlannerRoleEditor.
* @author kmoos
* @version x.xx
*/
public class OpenNewUserSecurityAction extends Action implements ISelectionListener {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.usersecurity.OpenNewUserSecurityAction";
	
	private static Logger logger = Logger.getLogger(OpenNewUserSecurityAction.class);
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public OpenNewUserSecurityAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
        this.setToolTipText(ID);
        this.setId(ID);
        this.setText("Create Security for Server User");
		window.getSelectionService().addSelectionListener(this);
	}
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), UserSecurityEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
					return;
			}
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
    	
		//get the first selection.
		//convert the node to a PlannerRolesNode.
		UsersSecurityNode usersSecurityNode = (UsersSecurityNode) selection.getFirstElement();
		
		PafServer pafServer = null;
		
		try {
			
			pafServer = PafProjectUtil.getProjectServer(project);
			
		} catch (PafServerNotFound e) {
			
//			String outMessage = "No default server, nor default project server found.";
			String outMessage = e.getMessage();
			logger.error(outMessage);
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, SWT.ICON_ERROR);
			return;
		}
		

		try {
			
			//using the two model manager, create the input for the PlannerRoleEditor.
			//TTN 1723 - Not able to right-click on User security when no default project server nor default server defined
			//Moved code from UserSecurityEditorInput
			UserSecurityEditorInput input = new UserSecurityEditorInput(usersSecurityNode.getName(), project, pafServer, true);
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), UserSecurityEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't open editor while dependent editor(s) is open");
						return;
				}
				
				//if input has been authenticated
				if ( input.isAuthenticated() ) {
				
					//get user security editor
					UserSecurityEditor userSecurityEditor = (UserSecurityEditor) page.openEditor(input, UserSecurityEditor.ID);
	
					//set the viewer.
					userSecurityEditor.setViewer(viewer);
								
					//update tree node
					userSecurityEditor.setTreeNode(usersSecurityNode);
				}
					
				
			} catch (PartInitException e) {
				// handle error
				logger.error(e.getMessage());
			}
			
		} catch (RuntimeException re) {
			
			logger.error(re.getMessage());
			
			//open error message
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, re.getMessage(), MessageDialog.ERROR);
			
		} catch (InvalidUserSecurityException e) {

			logger.error(e.getMessage());
			
			//open error message
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
			
		}
		
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

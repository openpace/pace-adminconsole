/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.usersecurity;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.menu.editors.UserSecurityEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;

/**
* Action, opens an existing PlannerRole in a PlannerRoleEditor.
* @author jmilliron
* @version x.xx
*/
public class DeleteUserSecurityAction extends Action implements ISelectionListener {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.usersecurity.DeleteUserSecurityAction";
	
	private static Logger logger = Logger.getLogger(DeleteUserSecurityAction.class);
	
	private IWorkbenchWindow window = null;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	private UserSecurityNode userSecurityNode;

	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public DeleteUserSecurityAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.window = window;
		this.viewer = viewer;
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
        this.setToolTipText(ID);
        this.setId(ID);
		window.getSelectionService().addSelectionListener(this);
	}
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incomming) {
		
		if (incomming instanceof IStructuredSelection) {
			
			IStructuredSelection selection = (IStructuredSelection) incomming;
						
//			get the first selection.
			Object item = selection.getFirstElement();
			
			if ( item instanceof UserSecurityNode ) {
			
				//convert the node to a PlannerRolesNode.
				userSecurityNode = (UserSecurityNode) item;
				
				//set text
				this.setText("Remove Security for " + userSecurityNode.getName());
							
			}
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
		try {
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), UserSecurityEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_DELETE_MESSAGE);
					return;
			}
			
			//using the current window, get the active page.
	    	IWorkbenchPage page = window.getActivePage();
			 	
			//confirm with user
			boolean isConfirmed = MessageDialog.openConfirm(window.getShell(), "Confirm", "Remove security for user " + userSecurityNode.getName() + "?");
			
			//if confirmed
			if ( isConfirmed ) {

				//look for any open editors and close them.
				EditorUtils.closeDuplicateEditorType(page, UserSecurityEditor.class);
				
				if ( project != null && userSecurityNode != null)  {
					
					//get user security model manager
					UserSecurityModelManager userSecurityModelManager = new UserSecurityModelManager(project);
					
					//remove user
					userSecurityModelManager.remove(userSecurityNode.getKey());
					
					//save model
					userSecurityModelManager.save();
					
					//get parent node
					UsersSecurityNode parent = (UsersSecurityNode) userSecurityNode.getParent().getParent();
					
					//recrete the children
					parent.createChildren(null);
					
					//refresh viewer
					viewer.refresh(parent);
					
					
				}
			}
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

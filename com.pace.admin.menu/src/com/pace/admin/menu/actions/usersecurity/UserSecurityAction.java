/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.usersecurity;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.UserSecurityEditor;
import com.pace.admin.menu.editors.input.UserSecurityEditorInput;
import com.pace.admin.menu.editors.internal.SecurityMember;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.exceptions.InvalidUserSecurityException;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.nodes.UserSecurityDimensionNode;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UserSecurityRoleNode;
import com.pace.admin.menu.nodes.UserSecuritySpecificationNode;
import com.pace.base.ui.PafServer;

/**
* Opens an existing user's security.
* 
* @author javaj
* @version x.xx
*/
public class UserSecurityAction extends Action implements ISelectionListener {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.usersecurity.UserSecurityAction";
	
	private static Logger logger = Logger.getLogger(UserSecurityAction.class);
	
	private IWorkbenchWindow window;
	
	private IStructuredSelection selection;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	private boolean isClone = false;
	
	public UserSecurityAction(IWorkbenchWindow window, TreeViewer viewer) {
		this(window, viewer, false);
	}
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public UserSecurityAction(IWorkbenchWindow window, TreeViewer viewer, boolean isClone) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
        this.setToolTipText(ID);
        this.setId(ID);
        
        this.isClone = isClone;
        
        if(isClone){
        	setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
        }
        
        
		window.getSelectionService().addSelectionListener(this);
	}
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;		
			
//			get the first selection.
			Object item = this.selection.getFirstElement();
			
			if ( item instanceof UserSecurityNode ) {
			
				//convert the node to a UserSecurityNode.
				TreeNode node = (UserSecurityNode) item;
		
				if ( isClone ) {
					
					this.setText("Copy Security from " + node.getName());
					
				} else {
					this.setText("Modify Security for " + node.getName());
				}
			
			}
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
		//TTN 1723 - Not able to right-click on User security when no default project server nor default server defined
		//Moved code from UserSecurityEditorInput
		PafServer pafServer = null;
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), UserSecurityEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't open editor while dependent editor(s) is open");
					return;
			}
			
			pafServer = PafProjectUtil.getProjectServer(project);
			
			//using the current window, get the active page.
	    	IWorkbenchPage page = window.getActivePage();
	    	
			//get the first selection.
			Object item = selection.getFirstElement();
			
			//convert the node to a userSecurityNode.
			UserSecurityNode userSecurityNode = null;
			
			String selectedRole = null;
			String selectedTab = null;
			String selectedSecurity = null;
			if ( item instanceof UserSecurityNode ) {
				
				userSecurityNode = (UserSecurityNode) item;
				
			} else if ( item instanceof UserSecurityRoleNode) {
				
				userSecurityNode = (UserSecurityNode) ((UserSecurityRoleNode) item).getParent();
				selectedRole = ((UserSecurityRoleNode) item).getName();
				
			} else if ( item instanceof UserSecurityDimensionNode) {
				
				userSecurityNode = (UserSecurityNode) ((UserSecurityDimensionNode) item).getParent().getParent();
				selectedRole = ((UserSecurityDimensionNode) item).getParent().getName();
				selectedTab = ((UserSecurityDimensionNode) item).getName();
				
			} else if ( item instanceof UserSecuritySpecificationNode) {
				
				userSecurityNode = (UserSecurityNode) ((UserSecuritySpecificationNode) item).getParent().getParent().getParent();
				selectedRole = ((UserSecuritySpecificationNode) item).getParent().getParent().getName();
				selectedTab = ((UserSecuritySpecificationNode) item).getParent().getName();
				selectedSecurity = ((UserSecuritySpecificationNode) item).getName();
			}
			
			if ( userSecurityNode !=  null ) {
				 
				//look for any open editors and close them.
				EditorUtils.closeDuplicateEditorType(page, UserSecurityEditor.class);
				
				try {
					
					//using the two model manager, create the input for the PlannerRoleEditor.
					
					UserSecurityEditorInput input = null;
					
					if ( isClone ) {
						
						//TTN 1723 - Not able to right-click on User security when no default project server nor default server defined
						//Moved code from UserSecurityEditorInput
						input = new UserSecurityEditorInput(userSecurityNode.getKey(), project, pafServer, false, isClone);
						
					} else {
					
						//TTN 1723 - Not able to right-click on User security when no default project server nor default server defined
						//Moved code from UserSecurityEditorInput
						input = new UserSecurityEditorInput(userSecurityNode.getKey(), project, pafServer, false);
					
					}
					
					try {			
					
						boolean isInputValid = false;
						
						//if clone, check authenticated, otherwise mark as true
						if ( isClone ) {
							
							isInputValid = input.isAuthenticated();
							
						} else {
							
							isInputValid = true;
						}
						
						
						if ( isInputValid ) {
						
							//open editor
							UserSecurityEditor userSecurityEditor = (UserSecurityEditor) page.openEditor(input, UserSecurityEditor.ID);
				
							//set the viewer.
							userSecurityEditor.setViewer(viewer);
										
							if ( isClone ) {
							
								userSecurityEditor.setTreeNode((TreeNode) userSecurityNode.getParent().getParent() );
								
							} else {
								
								userSecurityEditor.setTreeNode(userSecurityNode);
							}
							
							if(selectedRole != null){
								
								userSecurityEditor.setSelectedRole(selectedRole);
								
							}
							
							if(selectedTab != null){
								
								userSecurityEditor.setSelectedTab(selectedTab);
								userSecurityEditor.setSelectedSecurityDimension(selectedTab, selectedTab);
								userSecurityEditor.setSelectedSecurity(selectedTab, selectedTab);
								userSecurityEditor.setSelectionControls();
							}
							if(selectedSecurity != null){
								
								SecurityMember secMem = new SecurityMember(selectedTab, selectedSecurity );
								userSecurityEditor.setSelectedSecurityDimension(selectedTab, secMem.getMember());
								userSecurityEditor.setSelectedSecurity(selectedTab, selectedSecurity.toString());
								userSecurityEditor.setSelectionControls();
							}
						}
						
					} catch (PartInitException e) {
						// handle error
						logger.error(e.getMessage());
						GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
					}
					
				} catch (RuntimeException re) {
					
					//Jira 1702 - to handle when server is down or application is not running
//					String outMessage = "Server or application may not be running. Or no dimension downloaded.";
					String outMessage = re.getMessage();
					logger.error(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ERROR);
					
				} catch (InvalidUserSecurityException e) {

					logger.error(e.getMessage());
					String outMessage = "Invalid user login.";
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ERROR);
					
				} catch ( Exception e) {
					
					String outMessage = e.getMessage();
					logger.error(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ERROR);
				}
			
			}
			
		} catch (PafServerNotFound e) {
			
			String outMessage = e.getMessage();
			logger.error(outMessage);
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, SWT.ICON_ERROR);
			return;
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

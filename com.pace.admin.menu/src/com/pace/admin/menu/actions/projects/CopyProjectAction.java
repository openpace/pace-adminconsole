/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.CopyProjectOperation;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.base.app.PafApplicationDef;

/**
 * 
 * Copies a project.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class CopyProjectAction extends Action implements IWorkbenchAction {

	private static Logger logger = Logger.getLogger(CopyProjectAction.class);

	private IWorkbenchWindow window = null;
	
//	private IStructuredSelection selection = null;

	private IProject project;
	
	public final static String ID = "com.pace.admin.menu.actions.projects.CopyProjectAction";
	
	/**
	 * 
	 * @param window
	 */
	public CopyProjectAction(IWorkbenchWindow window) {
		
			setId(ID);
			this.window = window;
			setText("Copy Project");
			
		    setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
			//add to selection service
//			window.getSelectionService().addSelectionListener(this);
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {
//		window.getSelectionService().removeSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
//	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
//		
//		if (incoming instanceof IStructuredSelection) {
//			this.selection = (IStructuredSelection) incoming;
//		}
//		
//	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
				
		Shell parentShell = window.getShell();
		
		//get a set of project names before the copy
		Set<String> beforeCopyProjectNameSet = createProjectNameSet();
		
		//copy project operation
		CopyProjectOperation cpo = new CopyProjectOperation(parentShell);
		
		//copy project
		cpo.copyProject(project);

		//get a set of project names after the copy		
		Set<String> afterCopyProjectNameSet = createProjectNameSet();
		
		//remove the server names from before the copy to get the copied project name
		afterCopyProjectNameSet.removeAll(beforeCopyProjectNameSet);
		
		//if only one element remains
		if ( afterCopyProjectNameSet.size() == 1 ) {
			
			//get copied project name
			String copiedProjectName = afterCopyProjectNameSet.toArray(new String[0])[0];
						
			//get ref to iproject
			IProject copiedProject = ResourcesPlugin.getWorkspace().getRoot().getProject(copiedProjectName);
			
			String outMessage = null;
			
			//if project exists
			if ( copiedProject != null && copiedProject.exists()) {
				
				outMessage = "Successfully copied project '" + project.getName() + "' to '" + copiedProject.getName() + "'.";
				
				logger.info(outMessage);
							
				//TTN-710 BEGIN
				PafApplicationDef[] pafApps = PafApplicationUtil.getPafApps(copiedProject);
				
				if ( pafApps != null && pafApps.length > 0) {
					
					PafApplicationDef pafAppDef = pafApps[0];
					
					pafAppDef.setAppId(copiedProject.getName());
					
					PafApplicationUtil.setPafApps(copiedProject, pafAppDef);
					
				}
				//TTN-710 END
				
//				//get menuview and refresh
//				MenuView menuView = MenuPlugin.getDefault().getMenuView();
//				
//				if ( menuView != null ) {
//						
//					menuView.refreshProjectStructure();
//					menuView.selectProjectNode(copiedProject.getName());
//						
//				}
			} else {
				
				outMessage = "Did not successfully copied project '" + project.getName() + "' to '" + copiedProject.getName() + "'.";
				
				logger.error(outMessage);
				
			}
			
			ConsoleWriter.writeMessage(outMessage);
			
		}
	
	}

	/**
	 * 
	 * Creates a set of workspace project names
	 *
	 * @return
	 */
	private Set<String> createProjectNameSet() {

		//hold a set of project names
		Set<String> projectNameSet = new HashSet<String>();
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot rootWorkspace = workspace.getRoot();
		
		if ( rootWorkspace != null && rootWorkspace.getProjects() != null ) {
			for (IProject iProject : rootWorkspace.getProjects() ) {
				projectNameSet.add(iProject.getName());
			}
		}	
		
		return projectNameSet;
		
	}

	/**
	 * 
	 *	gets project
	 *
	 * @return
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * 
	 *	set's iproject
	 *
	 * @param project
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	

}

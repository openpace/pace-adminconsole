/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;

import com.myjavatools.lib.Files;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.views.MenuView;

/**
 * 
 * Deletes a project.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class DeleteProjectAction extends Action implements ISelectionListener,
		ActionFactory.IWorkbenchAction {

	private static String ID = "com.pace.admin.menu.actions.projects.DeleteProjectAction";

	private static Logger logger = Logger.getLogger(DeleteProjectAction.class);

	//workbench window
	private final IWorkbenchWindow window;

	//current selected object
	private IStructuredSelection selection;

	/**
	 * 
	 * @param window
	 */
	public DeleteProjectAction(IWorkbenchWindow window) {

		setId(ID);
		
		setText("&Delete Project");
		//setToolTipText("Delete selected project");
		
		//set window
		this.window = window;
		
		setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\cross.png"));
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {

		//get project node form selection
		ProjectNode projectNode = (ProjectNode) selection.getFirstElement();
		
		//if user wants to delete project
		if (GUIUtil
				.askUserAQuestion("Are you sure you want to delete the project '" + projectNode.getName() + "'?")) {
			
			//cleanup the backup directory by removing it
			cleanupBackDir();
			
			final IProject project = projectNode.getProject();
			
			if ( project.exists() ) {
			
				ProgressMonitorDialog pd = new ProgressMonitorDialog(window.getShell());
				
				//exports the member tag data to disk from server
				try {
					pd.run(true, true, new IRunnableWithProgress() {

						public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
						
							//start task
							monitor.beginTask("Deleting project '" + project.getName() + "' ...", IProgressMonitor.UNKNOWN);
						
//							//set sub task
//							monitor.subTask("Getting Member Tag Data from the server");
							try {
								project.delete(true, null);
							} catch (CoreException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							monitor.done();
						}
						
					});
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

			PafProjectUtil.removeProjectFromPafProjectMap(project);
			
			//refresh menu's viewer
//			MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();
//
//			if ( menuView != null ) {
//			
//				menuView.refreshProjectStructure();
////				menuView.selectTopViewerNode();
//		
//			}
			
		}

	}

	//deletes the backup directory
	private void cleanupBackDir() {

		File backupDir = new File(Constants.ADMIN_CONSOLE_HOME
				+ Constants.BACKUP + File.separator);

		if (backupDir.exists()) {

			Files.deleteFile(backupDir);

		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// Selection containing elements
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
			if (selection.getFirstElement() instanceof ProjectNode) {
				setEnabled(true);
			} else {
				setEnabled(false);
			}

		} else {
			// Other selections, for example containing text or of other kinds.
			setEnabled(false);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);

	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;

import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.nodes.ProjectNode;

/**
 * 
 * To expand the project tree or not to expand the project tree, that is the question.
 *
 * @author fskrgic
 * @version	x.xx
 *
 */
public class ToggleAutoConvertProjectAction extends Action implements ISelectionListener, ActionFactory.IWorkbenchAction {

	private static String ID = "com.pace.admin.menu.actions.projects.ToggleAutoConvertProjectAction";

	private static Logger logger = Logger.getLogger(ToggleAutoConvertProjectAction.class);

	private static int STYLE = AS_CHECK_BOX;
	
	//workbench window
	private final IWorkbenchWindow window;

	//current selected object
	private IStructuredSelection selection;
	/**
	 * 
	 * @param window
	 */
	public ToggleAutoConvertProjectAction(IWorkbenchWindow window) {
		
		super("Auto Upgrade Project", STYLE);
		this.window = window;
		setId(ID);
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
	}
	/**
	 * 
	 */
	public void run(){
		if( selection != null ) {
			//get project node form selection
			ProjectNode projectNode = (ProjectNode) selection.getFirstElement();
			IProject project = projectNode.getProject();
			
			PafProjectUtil.toggleProjectAutoConvert(project);
			
			if(PafProjectUtil.isProjectAutoConvert(project) != null){
				//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\check.png"));
				setChecked(PafProjectUtil.isProjectAutoConvert(project));
			}
		}
	}
	
	/*
	 * 
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);

	}
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		// Selection containing elements
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
			if (selection.getFirstElement() instanceof ProjectNode) {
				ProjectNode projectNode = (ProjectNode) selection.getFirstElement();
				IProject project = projectNode.getProject();
				if(PafProjectUtil.isProjectAutoConvert(project) != null ){
					//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\check.png"));
					setChecked(PafProjectUtil.isProjectAutoConvert(project));
				}
				setEnabled(true);
			} else {
				setEnabled(false);
			}

		} else {
			// Other selections, for example containing text or of other kinds.
			setEnabled(false);
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.actions.ActionFactory;

import com.pace.admin.global.util.PafProjectUtil;

/**
 * 
 * To expand the project tree or not to expand the project tree, that is the question.
 *
 * @author fskrgic
 * @version	x.xx
 *
 */
public class ToggleExpandProjectTreeAction extends Action implements ActionFactory.IWorkbenchAction {

	private static String ID = "com.pace.admin.menu.actions.projects.ToggleExpandProjectTreeAction";

	private static Logger logger = Logger.getLogger(ToggleExpandProjectTreeAction.class);

	private static int STYLE = AS_CHECK_BOX;
	
	/**
	 * 
	 * @param window
	 */
	public ToggleExpandProjectTreeAction() {
		
		super("Auto-Expand Projects", STYLE);
		
		setId(ID);
		
		if(PafProjectUtil.getExpandProjectTree()){
			//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\check.png"));
			setChecked(true);
		}
		else{
			//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\cross.png"));
			setChecked(false);
		}
	}
	/**
	 * 
	 */
	public void run(){
		
		PafProjectUtil.toggleExpandProjectTree();
		
		if(PafProjectUtil.getExpandProjectTree()){
			//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\check.png"));
			setChecked(true);
		}
		else{
			//setImageDescriptor(MenuPlugin.getImageDescriptor("icons\\cross.png"));
			setChecked(false);
		}
	}
	
	/*
	 * 
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		//clearListeners();

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.views.MenuView;
import com.pace.base.ui.PafServer;

/**
 * 
 * Ties a Pace project to a Pace server within the AC.  
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class SelectDefaultServerProjectAction extends Action implements
		IWorkbenchAction, ISelectionListener, Comparable {

	// looger
	private static Logger logger = Logger
			.getLogger(SelectDefaultServerProjectAction.class);

	// holds current workbech window
	private final IWorkbenchWindow window;

	// server to import files from
	private final PafServer server;

	// current selection
	private IStructuredSelection selection = null;
	
	private IProject currentProject = null;

	// ID code for action
	public final static String ID = "com.pace.admin.menu.actions.importexport.SelectDefaultServerProjectAction";

	/**
	 * 
	 * @param window
	 * @param server
	 */
	public SelectDefaultServerProjectAction(IWorkbenchWindow window, PafServer server) {
		super(server.getName());
		this.window = window;
		this.server = server;
		window.getSelectionService().addSelectionListener(this);
	}

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// if incoming is structured selection, set selection
		if (incoming instanceof IStructuredSelection) {

			selection = (IStructuredSelection) incoming;
			
			if ( selection.getFirstElement() instanceof ProjectNode ) {
				
				ProjectNode projectNode = (ProjectNode) selection.getFirstElement();
				
				currentProject = projectNode.getProject();
												
				PafServer defaultProjectServer = null;
				
				try {
					
					defaultProjectServer = PafProjectUtil.getProjectServer(currentProject);
					
				} catch (PafServerNotFound e) {
				}
				
				logger.debug("Project: " + projectNode.getName() + "; IProject: " + currentProject + "; Server Action: " + this.server.getName());
				
				if ( defaultProjectServer == null || ! this.server.getName().equalsIgnoreCase(defaultProjectServer.getName())) {
					
					setImageDescriptor(null);

				} else {
					
					setImageDescriptor(com.pace.admin.menu.MenuPlugin
							.getImageDescriptor("/icons/check.png"));
					
				}
				
				
				
			}

		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		PafServer projectServer = null;
		
		try {
			
			projectServer = PafProjectUtil.getProjectServer(currentProject);
			
		} catch (PafServerNotFound e) {
		}
						
		//if no default server exists and the projec tserver doesn't equal the default server
		if ( projectServer == null || ! this.server.getName().equalsIgnoreCase(projectServer.getName())) {
			
			logger.debug("Server: " + server.getName() + "; IProject: " + currentProject);
			
			PafProjectUtil.setDefaultProjectServer(currentProject, server);
			
			//refresh menu's viewer
			MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

			if ( menuView != null ) {
			
//				menuView.getViewer().setSelection(selection);
				menuView.getViewer().refresh(true);
					
			}	

		}
				
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {
				
		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);
		
	}


	public int compareTo(Object o) {
		
		
		SelectDefaultServerProjectAction otherAction = (SelectDefaultServerProjectAction) o;
		
		
		int outcome = 0;
		
		if ( otherAction.server != null && otherAction.server.getName() != null	) {
			outcome = this.server.getName().compareTo(otherAction.server.getName());
		}
		
		return outcome;
	}

}

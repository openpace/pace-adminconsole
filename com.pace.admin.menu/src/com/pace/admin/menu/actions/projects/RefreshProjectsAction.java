/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.views.MenuView;

/**
 * 
 * Refreshes a project. Removes and creates projects in viewer.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class RefreshProjectsAction extends Action implements
		ISelectionListener, IWorkbenchAction, IRunnableWithProgress {

	private final IWorkbenchWindow window;

	public final static String ID = "com.pace.admin.menu.actions.projects.RefreshProjectsAction";

	/**
	 * 
	 * @param window
	 */
	public RefreshProjectsAction(IWorkbenchWindow window) {
		super(ID);
		
		setId(ID);
		
		setText("Refresh All Project Trees\tCtrl+F5");

		// set window
		this.window = window;
		
		setImageDescriptor(MenuPlugin.getImageDescriptor("/icons/refresh.gif"));

		// add to selection service
		window.getSelectionService().addSelectionListener(this);

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
	}

	public void run() {

		//get menu view
		final MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

		if ( menuView != null ) {
		
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

				public void run() {

			
					menuView.refreshProjectStructure();
//					menuView.selectTopViewerNode();
					
				}});
		
		}
	
	}
	

	public void run(final IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		Display.getDefault().asyncExec(new Runnable() {
		      public void run() {
		    	  
		    	  final MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();
		    	  
		    	  menuView.refreshProjectStructure();
		    	  
//		    	  menuView.selectTopViewerNode();
		      }
		    });
	}
		
}

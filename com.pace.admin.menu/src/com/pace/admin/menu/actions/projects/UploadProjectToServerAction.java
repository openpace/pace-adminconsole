/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.inputs.UploadProjectInput;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.actions.AbstractUploadAction;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PafServer;

public class UploadProjectToServerAction extends AbstractUploadAction {
	
	public final static String ID = "com.pace.admin.menu.actions.projects.UploadProjectToServerAction";

	public UploadProjectToServerAction(IWorkbenchWindow window, PafServer server) {
		super(ID, window, server);	
	}
	
	public void run() {
		
  		//get the first selection.
      	if ( selection instanceof IStructuredSelection && ((IStructuredSelection)selection).getFirstElement() instanceof ProjectNode ) {
			
			PafProjectUtil.uploadProjectToServer(new UploadProjectInput(getProjectNode().getName(), server));
						
      	}
	}

	@Override
	public ProjectNode getProjectNode() {
		return (ProjectNode) ((IStructuredSelection)selection).getFirstElement();
	}

}

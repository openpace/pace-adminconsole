/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.views.MenuView;

/**
 * Collapses the contents of the viewer.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class CollapseAllAction extends Action {
	
	
	public CollapseAllAction() {
		super();

		setToolTipText("Collapse All Projects");
		setImageDescriptor(MenuPlugin.getImageDescriptor("/icons/collapseall.gif"));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		//refresh menu's viewer
		final MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

		if ( menuView != null ) {
		
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getDisplay(), new Runnable() {

				public void run() {
			
					menuView.getViewer().collapseAll();
				}
			});
		}
		
	}

}

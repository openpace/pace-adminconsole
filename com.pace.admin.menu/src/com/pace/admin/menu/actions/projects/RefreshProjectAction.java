/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.actions.AbstractAction;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.views.MenuView;

/**
 * 
 * Refreshes a project. Removes and creates projects in viewer.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class RefreshProjectAction extends AbstractAction  {

	public final static String ID = "com.pace.admin.menu.actions.projects.RefreshProjectAction";

	/**
	 * 
	 * @param window
	 */
	public RefreshProjectAction(IWorkbenchWindow window) {
		super("Refresh Current Project", ID, window);

		setImageDescriptor(MenuPlugin.getImageDescriptor("/icons/refresh.gif"));
	}


	public void run() {

		//get menu view
		final MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

		if ( menuView != null ) {
			
			if ( selection instanceof IStructuredSelection && ((IStructuredSelection)selection).getFirstElement() instanceof ITreeNode ) {
	      		
	      		TreeNode node = (TreeNode) ((IStructuredSelection)selection).getFirstElement();
	      		final ProjectNode projNode = ViewerUtil.getProjectNode(node.getProject().getName());
	      		projNode.setBuilt(false);
	      		
	      		BusyIndicator.showWhile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getDisplay(), new Runnable() {

					public void run() {

						menuView.loadProject(projNode);

					}
				});
	      		
	      		
	      	} else {
	      		return;
	      	}
		}
	}	
}

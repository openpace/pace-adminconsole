/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.dialogs.UploadLocalProjectDialog;
import com.pace.admin.global.inputs.UploadProjectToServerGroupInput;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.actions.AbstractUploadAction;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PafServer;

public class UploadProjectToServerGroupAction extends AbstractUploadAction {

	public final static String ID = "com.pace.admin.menu.actions.projects.UploadProjectToServerGroupAction";
	
	public UploadProjectToServerGroupAction(IWorkbenchWindow window, String groupName, List<PafServer> servers) {
		super(ID, window, groupName, servers);
	}
	
	
	public void run() {
		
  		//get the first selection.
      	if ( selection instanceof IStructuredSelection && ((IStructuredSelection)selection).getFirstElement() instanceof ProjectNode ) {
      								
			//convert the node to a ProjectNode.
			ProjectNode projNode = getProjectNode();

			//Check if all servers are running, if not prompt user.
			if(!ImportExportUtil.okToDeployNonRunningServers(allServersRunning(), servers)){
				return;
			}
			
			//check for non-matching projects.
			if(!ImportExportUtil.okToDeployToNonMatchingServerProjects(PafProjectUtil.getApplicationName(this.getIProject()), servers)){
				return;
			}
			
			UploadLocalProjectDialog dialog = new UploadLocalProjectDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					"Upload Project '" + projNode.getName() + "' to Server Group '" + this.getText() + "'?",
					null);
			int rc = dialog.open();
			
			if ( rc != 0 ) {
				return;
			}
			
			UploadProjectToServerGroupInput input = new UploadProjectToServerGroupInput(projNode.getName(), this.getText(), servers);
			input.setApplyConfigChanges(dialog.isApplyConfig());
			input.setApplyCubeChanges(dialog.isApplyCube());
			
			PafProjectUtil.uploadProjectToServerGroup(input);
			
      	}
	}


	@Override
	public ProjectNode getProjectNode() {
		return  (ProjectNode) ((IStructuredSelection)selection).getFirstElement();
	}
}

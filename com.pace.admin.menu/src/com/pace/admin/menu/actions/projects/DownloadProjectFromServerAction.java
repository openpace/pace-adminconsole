/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.inputs.DownloadProjectInput;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PafServer;

/**
 * Download Project From Server Action
 * 
 * @author JMilliron
 *
 */
public class DownloadProjectFromServerAction extends Action implements IWorkbenchAction, ISelectionListener {

	public final static String ID = "com.pace.admin.menu.actions.projects.DownloadProjectFromServerAction";
	
	// looger
	private static Logger logger = Logger.getLogger(DownloadProjectFromServerAction.class);

	// holds current workbech window
	private final IWorkbenchWindow window;

	// server to import files from
	private final PafServer server;

	// current selection
	private IStructuredSelection selection = null;
	
	/**
	 * Calls download project job.
	 * 
	 * @param window active window used for selection service
	 * @param server server to download project from
	 */
	public DownloadProjectFromServerAction(IWorkbenchWindow window, PafServer server) {
		
		super(server.getName());
		this.window = window;
		this.server = server;
		window.getSelectionService().addSelectionListener(this);
	}
		
	/**
	 * Gets pafserver.
	 * @return
	 */
	public PafServer getServer() {
		return server;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// if incoming is structured selection, set selection
		if (incoming instanceof IStructuredSelection) {

			selection = (IStructuredSelection) incoming;

		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		//get the first selection.
      	if ( selection instanceof IStructuredSelection && ((IStructuredSelection)selection).getFirstElement() instanceof ProjectNode ) {
      								
			//convert the node to a PrintStyleNode.
			ProjectNode projNode = (ProjectNode) ((IStructuredSelection)selection).getFirstElement();
						
			DownloadProjectInput input = new DownloadProjectInput(projNode.getName(), server);
						
			PafProjectUtil.downloadProjectFromServer(input);

      	}
	}
	

}

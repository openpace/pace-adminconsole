/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.projects;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;

import com.pace.admin.menu.MenuPlugin;


public class NewProjectButtonAction extends Action {

	private IWorkbenchWindow window;

	private ActionFactory.IWorkbenchAction action;
	
	public NewProjectButtonAction(IWorkbenchWindow window){
		
		this.window = window;
		
		this.action = ActionFactory.NEW.create(window);
		
		setText("New Project...");
		
        setToolTipText("Creates a new Pace project.");
        
        setId(action.getId());
        
        setActionDefinitionId(action.getActionDefinitionId());
        
        setImageDescriptor(MenuPlugin.getImageDescriptor("icons/ac16.jpg"));
		
	}
	
	 /**
     * Invoke the Import wizards selection Wizard.
     */
    public void run() {
        if (window == null) {
            // action has been disposed
            return;
        }
        
        action.run();
    }
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.security.auth.Refreshable;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.admin.menu.wizards.ProjectErrorsDialog;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;

/**
 * The completion task, which either shows a success dialog, or an error dialog,
 * depending on the ok state set and then notifies the {@link Refreshable} to
 * refresh its contents
 */
public class CompletionAction extends Action {
	private boolean ok;
	private boolean showDialog;
	private String okTitle;
	private String okMsg;
	private String failMsg;
	private String failTitle;
	private Throwable throwable;

	private Shell shell;
	private String pluginId;
//	private List<ProjectDataError> projectDataErrors;
	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrorMap;

	/**
	 * @param pluginId
	 * @param shell
	 * @param refreshable
	 */
	public CompletionAction(String pluginId, Shell shell) {
		this.pluginId = pluginId;
		this.shell = shell;
		this.showDialog = true;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public void setOkTitle(String okTitle) {
		this.okTitle = okTitle;
	}

	public void setOkMsg(String okMsg) {
		this.okMsg = okMsg;
	}

	public void setFailMsg(String failMsg) {
		this.failMsg = failMsg;
	}

	public void setFailTitle(String failTitle) {
		this.failTitle = failTitle;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public void run() {

		// then show the dialog
		if (ok) {
			MessageDialog.openInformation(shell, okTitle, okMsg);
		} else {
			Status status = new Status(IStatus.ERROR, pluginId,
					throwable.getLocalizedMessage(), throwable);
			ErrorDialog.openError(shell, failTitle, failMsg, status);
			ProjectErrorsDialog errorDialog = ProjectWizardUtil.getProjectImportErrorsWizardPage(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
					getProjectDataErrors());
			
			errorDialog.open();
		}
	}

	public boolean isShowDialog() {
		return showDialog;
	}

	public void setShowDialog(boolean showDialog) {
		this.showDialog = showDialog;
	}
	private List<ProjectDataError> getProjectDataErrors(){
		

		List<ProjectDataError> errors = new ArrayList<ProjectDataError>();
		
		if(projectDataErrorMap != null && projectDataErrorMap.keySet() != null){
			
			for(ProjectElementId element : projectDataErrorMap.keySet()){
				
				List<ProjectDataError> error = projectDataErrorMap.get(element);
				errors.addAll(error);
				
			}
			
		}
		
		return errors;
		
	}
}
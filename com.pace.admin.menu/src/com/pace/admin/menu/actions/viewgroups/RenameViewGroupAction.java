/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewgroups;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.RenameViewGroupDialog;
import com.pace.admin.menu.editors.ViewGroupEditor;
import com.pace.admin.menu.editors.input.ViewGroupEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.base.view.PafViewGroup;

/**
 * Renames a view group.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class RenameViewGroupAction extends Action implements IWorkbenchAction,
		ISelectionListener {
	
	private static Logger logger = Logger.getLogger(RenameViewGroupAction.class);
	
	public final static String ID = "com.pace.admin.menu.actions.viewgroups.RenameViewGroupAction";
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
		
	private IProject project;
	
	/**
	 * 
	 * @param window
	 */
	public RenameViewGroupAction(IWorkbenchWindow window) {
		
		setId(ID);
		
		this.window = window;
		
		//set Action text
		setText("Rename View Group...");
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		Object object = selection.getFirstElement();
		ProjectNode projectNode = null;
		
		if ( object instanceof ViewGroupNode ) {
		
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewGroupEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_RENAME_MESSAGE);
						return;
				}
				
				//cast
				ViewGroupNode viewGroupNode = (ViewGroupNode) object;
				
				PafViewGroupModelManager viewGroupModelManager = new PafViewGroupModelManager(project);
				
				PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
				
				ViewModelManager viewModelManager = new ViewModelManager(project);
				
				if ( viewGroupModelManager.contains(viewGroupNode.getName())) {
				
					//get view group
					PafViewGroup pafViewGroup = (PafViewGroup) viewGroupModelManager.getItem(viewGroupNode.getName());
					
					//create unique set of view and view group names. 
					Set<String> uniqueStringSet = new HashSet<String>();
					
					uniqueStringSet.addAll(viewGroupModelManager.getKeySet());
					uniqueStringSet.addAll(viewModelManager.getKeySet());
					
					//get list of open editors
					IEditorReference[] editorRefs = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
					
					//loop over open editors
					for ( IEditorReference editorRef : editorRefs ) {
																	
						try {
							
							//get editor input
							IEditorInput input = editorRef.getEditorInput();
																	
							//see if we get a match for a view group editor, close it
							if ( input instanceof ViewGroupEditorInput) {
								
								EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewGroupEditor.class);
								
							}							
							
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
					RenameViewGroupDialog dialog = new RenameViewGroupDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
							pafViewGroup.getName(), uniqueStringSet.toArray(new String[0]));
									
					int rc = dialog.open();
					
					//if user clicks ok
					if ( rc == 0 ) {
						
						//get new view group name
						String newViewGroupName = dialog.getNewViewGroupName();
						
						logger.info("Old View Group Name: " + pafViewGroup.getName() + "; New View Group Name: " + newViewGroupName);
					
						//load new model data
						viewGroupModelManager.load();
						
						//rename view group name
						viewGroupModelManager.renameViewGroupName(pafViewGroup.getName(), newViewGroupName);
						
						//load new model data
						pafPlannerConfigModelManager.load();
						
						//rename view group name
						pafPlannerConfigModelManager.renameViewOrViewGroupInRoleConfigurations(pafViewGroup.getName(), newViewGroupName);
						
						//get parent node
						Object parentNode = viewGroupNode.getParent();
						
						//set the project node.
						if (parentNode instanceof ViewGroupNode  ) {
							projectNode = (ProjectNode) (((ViewGroupNode) parentNode).getParent()).getParent();
						} else if(parentNode instanceof ViewGroupsNode){
							projectNode = (ProjectNode) (((ViewGroupNode) viewGroupNode).getParent()).getParent();
						}
						
						//have to loop thru the project node until you find the ViewGroupsNode
						//then rebuild it.
						if(projectNode != null){
							for (Object obj : projectNode.getChildren()) {
								if (obj instanceof ViewGroupsNode  ) {
									//get the high level node.
									ViewGroupsNode viewGroupsNode = (ViewGroupsNode) obj;
									
									//re-create children
									viewGroupsNode.createChildren(null);
									
									//update the menu plug in.
									MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
								}		
							}
						}
						
						
						//Old Code
//						//get parent node
//						ViewGroupsNode parentNode = (ViewGroupsNode) viewGroupNode.getParent();
//						
//						//re-create children
//						parentNode.createChildren(null);					
//						
//						//refresh viewer
//						MenuPlugin.getDefault().getMenuView().getViewer().refresh();
					}
				}
			} catch (PartInitException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		//if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {
		
		//remove from selection listener
		window.getSelectionService().removeSelectionListener(this);
		
	}

	public void setProject(IProject project) {

		this.project = project;
		
	}
	
}

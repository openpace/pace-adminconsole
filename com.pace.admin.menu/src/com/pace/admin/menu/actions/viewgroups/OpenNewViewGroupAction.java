/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewgroups;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.menu.editors.ViewGroupEditor;
import com.pace.admin.menu.editors.input.ViewGroupEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ViewGroupsNode;

/**
 * Opens a new view group
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class OpenNewViewGroupAction extends Action implements IWorkbenchAction,
		ISelectionListener {
	
	private static Logger logger = Logger.getLogger(OpenNewViewGroupAction.class);
		
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
	
	public final static String ID = "com.pace.admin.menu.actions.viewgroups."; 
		
	private TreeViewer viewer;
	
	private IProject project;
	
	/**
	 * 
	 * @param window
	 * @param viewer
	 */
	public OpenNewViewGroupAction(IWorkbenchWindow window, TreeViewer viewer) {
		
		setId(ID);
		
		this.window = window;
		
		this.viewer = viewer;
		
		//set Action text
		setText("Create New View Group");
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		Object object = selection.getFirstElement();
		
		if ( object instanceof ViewGroupsNode ) {
			
			ViewGroupsNode viewGroupsNode = (ViewGroupsNode) object;
			
			//using the current window, get the active page.
	    	IWorkbenchPage page = window.getActivePage();
			
	    	//close dup editors
			EditorUtils.closeDuplicateEditorType(page, ViewGroupEditor.class);
			
			//create new input
			ViewGroupEditorInput input = new ViewGroupEditorInput("", project, true);
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewGroupEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't open editor while dependent editor(s) is open");
						return;
				}
				
				//open editor
				ViewGroupEditor viewGroupEditor = (ViewGroupEditor) page.openEditor(input, ViewGroupEditor.ID);
				
				//set viewer
				viewGroupEditor.setViewer(viewer);
				
				//set selected node
				viewGroupEditor.setSelectedNode(viewGroupsNode);
				
			} catch (PartInitException e) {
				logger.error(e.getMessage());
			}      			
			
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		//if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {
		
		//remove from selection listener
		window.getSelectionService().removeSelectionListener(this);
		
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project The project to set.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
}

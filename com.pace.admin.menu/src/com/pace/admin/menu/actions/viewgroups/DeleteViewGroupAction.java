/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewgroups;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.ViewGroupEditor;
import com.pace.admin.menu.editors.input.ViewGroupEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;

/**
 * Deletes one or more view groups for a project.
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class DeleteViewGroupAction extends Action implements IWorkbenchAction,
		ISelectionListener {

	private static Logger logger = Logger
			.getLogger(DeleteViewGroupAction.class);

	public final static String ID = "com.pace.admin.menu.actions.viewgroups.DeleteViewGroupAction";

	private IWorkbenchWindow window = null;

	private IStructuredSelection selection = null;

	private IProject project;

	private TreeViewer viewer;
	
	/**
	 * 
	 * Constructs Action
	 * 
	 * @param window
	 * @param viewer
	 */
	public DeleteViewGroupAction(IWorkbenchWindow window, TreeViewer viewer) {

		setId(ID);

		this.window = window;

		this.viewer = viewer;
		
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
		
		// set Action text
		setText("Delete View Group(s)");

		// add to selection service
		window.getSelectionService().addSelectionListener(this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		Object[] selectionObjectAr = selection.toArray();
		
		if ( selectionObjectAr != null ) {
		
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewGroupEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_DELETE_MESSAGE);
						return;
				}
				
				PafViewGroupModelManager viewGroupModelManager = new PafViewGroupModelManager(project);
				
				List<String> viewGroupNamesToDeleteList = new ArrayList<String>();
				
				ViewGroupsNode viewGroupParentNode = null;
				
				for (Object object : selectionObjectAr ) {
				
					//Object object = selection.getFirstElement();
					
					//ensure view group node
					if ( object instanceof ViewGroupNode ) {
					
						ViewGroupNode viewGroupNode = (ViewGroupNode) object;					
								
						viewGroupNamesToDeleteList.add(viewGroupNode.getName());
						
						if ( viewGroupParentNode == null && viewGroupNode.getParent() instanceof ViewGroupsNode) {
						
							viewGroupParentNode = (ViewGroupsNode) viewGroupNode.getParent();
							
						}
						
					}
					
				}
				
				if ( viewGroupNamesToDeleteList.size() > 0 ) {
					
					//confirm to delete view group with user
					if (GUIUtil.askUserAQuestion(
			    			this.getText(),
							"Are you sure you want to delete the View Group(s): " + viewGroupNamesToDeleteList.toString() + "?")) {
					
						//get list of open editors
						IEditorReference[] editorRefs = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
						
						//loop over open editors
						for ( IEditorReference editorRef : editorRefs ) {
																		
							try {
								
								//get editor input
								IEditorInput input = editorRef.getEditorInput();
								
								//see if we get a match for a view group editor
								if ( input instanceof ViewGroupEditorInput) {
									
									//cast input
									ViewGroupEditorInput vgEInput = (ViewGroupEditorInput) input;
									
									//close editor if view group editor is open
									EditorUtils.closeEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewGroupEditor.class, false);
																	
								}							
								
							} catch (PartInitException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.error(e.getMessage());
							}
							
						}

						for (String viewGroupNameToDelete : viewGroupNamesToDeleteList) {
						
							if ( viewGroupModelManager.contains(viewGroupNameToDelete)) {					
												
								//remove from model manager
								viewGroupModelManager.remove(viewGroupNameToDelete);
							
							}
						
						}
						
						//save model manager					
						viewGroupModelManager.save();
										
						if ( viewGroupParentNode != null ) {
												
							//recreate children
							viewGroupParentNode.createChildren(null);					
								
							//refresh plugin
							MenuPlugin.getDefault().getMenuView().getViewer().refresh();
						
						}
					}		
					
				}
				
			} catch (PartInitException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {

		// if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {

			this.selection = (IStructuredSelection) selection;
			
			//check all selections and make sure are all view group node's and all the nodes have the same parent
			if ( this.selection != null && ! this.selection.isEmpty() && this.selection.getFirstElement() instanceof ViewGroupNode ) {
			
				Object[] selectionObjectAr = this.selection.toArray();
												
				if ( selectionObjectAr != null ) {
				
					ViewGroupsNode parentNode = null;
					
					boolean allViewGroupNodeSameParentNode = false;
					
					for ( Object selectionObject : selectionObjectAr ) {
						
						if ( selectionObject == null || ! (selectionObject instanceof ViewGroupNode) ) {
							
							break;
							
						}
						
						ViewGroupNode vgn = (ViewGroupNode) selectionObject;
						
						//if parent is null, set parent
						if ( parentNode == null) {
							
							parentNode = (ViewGroupsNode) vgn.getParent();
							
						//if parent is not null, verify same parent
						} else if ( parentNode != vgn.getParent()) {
							
							break;
							
						}
						
						//if last selection object, set as success that all selections have same parent
						if ( selectionObject == selectionObjectAr[selectionObjectAr.length-1]) {
							
							allViewGroupNodeSameParentNode = true;
							
						}
						
					}
					
					this.setEnabled(allViewGroupNodeSameParentNode);
					
				}

				
			}
			
			
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		// remove from selection listener
		window.getSelectionService().removeSelectionListener(this);

	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project
	 *            The project to set.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

}

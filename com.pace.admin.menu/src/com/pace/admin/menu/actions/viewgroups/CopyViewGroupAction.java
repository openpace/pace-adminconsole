/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewgroups;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.CopyViewGroupDialog;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.base.view.PafViewGroup;

/**
 * Copies an existing view group.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class CopyViewGroupAction extends Action implements IWorkbenchAction,
		ISelectionListener {
	
	private static Logger logger = Logger.getLogger(CopyViewGroupAction.class);
	
	public final static String ID = "com.pace.admin.menu.actions.viewgroups.CopyViewGroupAction";
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
	
	private IProject project;
		
	public CopyViewGroupAction(IWorkbenchWindow window) {
		
		setId(ID);
		
		this.window = window;
		
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
		
		//set Action text
		setText("Copy View Group");
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		Object object = selection.getFirstElement();
		
		//ensure instance is view group node
		if ( object instanceof ViewGroupNode ) {
		
			ViewGroupNode viewGroupNode = (ViewGroupNode) object;
			
			PafViewGroupModelManager viewGroupModelManager = new PafViewGroupModelManager(project);
			
			ViewModelManager viewModelManager = new ViewModelManager(project); 
			
			//ensure view group's model manger contains the view group to copy
			if ( viewGroupModelManager.contains(viewGroupNode.getName())) {
			
				PafViewGroup pafViewGroup = (PafViewGroup) viewGroupModelManager.getItem(viewGroupNode.getName());
				
				//create unique list of view and view group names.  Used to the new copied view group can't be named the same.
				Set<String> uniqueStringSet = new HashSet<String>();
				
				uniqueStringSet.addAll(viewGroupModelManager.getKeySet());
				uniqueStringSet.addAll(viewModelManager.getKeySet());
				
				//create copoy view group dialog
				CopyViewGroupDialog dialog = new CopyViewGroupDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
						pafViewGroup.getName(), uniqueStringSet.toArray(new String[0]));
				
				//open dialog
				int rc = dialog.open();
				
				//if user clicked ok
				if ( rc == 0 ) {
					
					//get new view group name
					String newViewGroupName = dialog.getNewViewGroupName();
					
					logger.info("Old View Group Name: " + pafViewGroup.getName() + "; New View Group Name: " + newViewGroupName);
				
					PafViewGroup clonedViewGroup = null;
					
					//clone view group
					try {
						clonedViewGroup = (PafViewGroup) pafViewGroup.clone();
						clonedViewGroup.setName(newViewGroupName);
					} catch (CloneNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//add copied view group to to view group model manager 
					viewGroupModelManager.add(newViewGroupName, clonedViewGroup);
					
					//save copied view group
					viewGroupModelManager.save();
					
					//get parent node
					ViewGroupsNode parentNode = (ViewGroupsNode) viewGroupNode.getParent();
					
					//create children for parent
					parentNode.createChildren(null);					
					
					//refresh viewer
					MenuPlugin.getDefault().getMenuView().getViewer().refresh();
		
					
				}
			
			}

		}
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		//if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {
		
		//remove from selection listener
		window.getSelectionService().removeSelectionListener(this);
		
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project The project to set.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
}

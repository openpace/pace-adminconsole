/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.ToolItem;

import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.menu.dialogs.input.ViewSectionTuplesDialogInput;
import com.pace.base.SortOrder;
import com.pace.base.SortPriority;

public class SortTupleAction extends Action {
	private TreeViewer viewer;
	private SortOrder sortOrder;
	private SortPriority sortPriority;
	private IStructuredSelection selection; 
	private ToolItem colClearAllSortsToolItem;
	
	public SortTupleAction( SortPriority sortPriority, SortOrder sortOrder, IStructuredSelection selection, ToolItem colClearAllSortsToolItem ) {
		super(sortOrder.toString());
		this.sortPriority = sortPriority;
		this.sortOrder = sortOrder;
		this.selection = selection;
		this.colClearAllSortsToolItem = colClearAllSortsToolItem;
	}
	/**
	 * @return the viewer
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * @param viewer the viewer to set
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}
	
	public ViewSectionTuplesDialogInput getInput() {
		return input;
	}
	public void setInput(ViewSectionTuplesDialogInput input) {
		this.input = input;
	}

	private ViewSectionTuplesDialogInput input;
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		if ( viewer != null ) {
		
			Object node = selection.getFirstElement();
			
			if( node instanceof PaceTreeNode ) {
				//reset if there is one already
				PaceTreeNode tmpNode =  input.findSortTuple(input.getColumnAxisTupleTree().getRootNode(),sortPriority);
				if( tmpNode != null ) {
					tmpNode.getProperties().setSortPriority(null);
					tmpNode.getProperties().setSortOrder(null);
					input.setSortingForDuplicatedTuples(input.getColumnAxisTupleTree().getRootNode(),(PaceTreeNode)tmpNode, false);
				}
				//set sorting option to node property 
				((PaceTreeNode)node).getProperties().setSortPriority(sortPriority);
				((PaceTreeNode)node).getProperties().setSortOrder(sortOrder);
				
				input.setSortingForDuplicatedTuples(input.getColumnAxisTupleTree().getRootNode(),(PaceTreeNode)node, true);
				
				//refresh tree
				viewer.refresh(true);
				colClearAllSortsToolItem.setEnabled(true);
			}
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.admin.menu.dialogs.UserSelectionsDialog;

public class NewUserSelectionAction extends Action implements
		ISelectionListener, IWorkbenchAction {

	public final static String ID = "com.pace.admin.actions.NewUserSelectionAction";

	private Shell parentShell;
	private IProject project;
	private PaceTreeNodeProperties nodeProps;
	private PaceTreeAxis axis;
	
	public PaceTreeAxis getAxis() {
		return axis;
	}

	public void setAxis(PaceTreeAxis axis) {
		this.axis = axis;
	}

	public PaceTreeNodeProperties getNodeProps() {
		return nodeProps;
	}


	public void setNodeProps(PaceTreeNodeProperties nodeProps) {
		this.nodeProps = nodeProps;
	}


	public Shell getParentShell() {
		return parentShell;
	}


	public void setParentShell(Shell parentShell) {
		this.parentShell = parentShell;
	}


	public IProject getProject() {
		return project;
	}


	public void setProject(IProject project) {
		this.project = project;
	}


	public NewUserSelectionAction() {
	}


	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

	}

	public void run() {

		UserSelectionsDialog dialog = new UserSelectionsDialog(parentShell, project, true);
		dialog.setDimension(nodeProps.getDimensionName());
		dialog.setSingle(axis.equals(PaceTreeAxis.Page));
		dialog.open();

	}


	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}

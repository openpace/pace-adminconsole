/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.List;
import java.util.Map;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.menu.actions.projects.RefreshProjectsAction;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;

public abstract class ImportExportAction extends Action {

	protected ProjectElementId[] elements;

	protected IWorkbenchWindow window;
	
	private boolean error;
	
	private Throwable throwable;
	
	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrors;
		
	protected RefreshProjectsAction refreshAction;
	
	public ImportExportAction(String name, IWorkbenchWindow window, ProjectElementId[] elements){
		super(name);
		
		this.window = window;
		this.elements = elements;
		
		if(window != null){
			refreshAction = new RefreshProjectsAction(this.window);
		}
		
	}
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public Map<ProjectElementId, List<ProjectDataError>> getProjectDataErrors() {
		return projectDataErrors;
	}

	public void setProjectDataErrors(
			Map<ProjectElementId, List<ProjectDataError>> projectDataErrors) {
		this.projectDataErrors = projectDataErrors;
	}
	
}

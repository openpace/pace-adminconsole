/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

/**
 * Exports project to server over webservices
 * 
 * @author JMilliron
 *
 */
public class ExportProjectToServerAction extends ImportExportAction {
	
	private static Logger logger = Logger.getLogger(ExportProjectToServerAction.class);
	
	private final PafServer server;
	
	private IProject project;
	
	private boolean refreshConfiguration; 
	
	private boolean refreshCube;
	
	public final static String ID = "com.pace.admin.menu.actions.importexport.DeployToServerAction";
	
	/**
	 * 
	 * @param paceProject
	 * @param server
	 * @param elements
	 * @param refreshCube 
	 * @param refreshConfiguration 
	 */
	public ExportProjectToServerAction(IWorkbenchWindow window, IProject project, PafServer server, ProjectElementId[] elements, boolean refreshConfiguration, boolean refreshCube) {
		
		//super(server.getName());
		
		super(server.getName(), window, elements);
		
		this.project = project;
		this.server = server;
		this.refreshConfiguration = refreshConfiguration;
		this.refreshCube = refreshCube;

	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		setError(false);
		
		Set<ProjectElementId> filterList = new HashSet<ProjectElementId>();
		
		if ( elements != null ) {
			filterList.addAll(Arrays.asList(elements));
		}
		
		boolean successfullyUploaded = WebServicesUtil.uploadProjectToServer(project, server, refreshConfiguration, refreshCube, filterList);
		
		if ( ! successfullyUploaded ) {
			setError(true);
		}
		
					
		if ( isError() ) {
			
			final String errorMessage = "Project '" + project.getName() + "' was not successfully uploaded to " + server.getName() + " server.  Please check the console for any errors.";
			
			GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, errorMessage, SWT.ICON_WARNING);
			
			Display.getDefault().asyncExec(
			  new Runnable() {
			    public void run(){
			    	GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, errorMessage, SWT.ICON_WARNING);	
			    }
			  });
			
			logger.error(errorMessage);
			
			ConsoleWriter.writeMessage(errorMessage);
			
		} else {
			
			final String outMessage = "Project '" + project.getName() + "' was successfully uploaded to '" + server.getName() + "' server.";
			
			logger.info(outMessage);
			
			ConsoleWriter.writeMessage(outMessage);			
		}
		
	}

}

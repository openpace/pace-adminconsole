/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.menu.actions.AbstractUploadAction;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.base.view.PafView;
import com.pace.base.view.PafViewSection;

public abstract class AbstractUploadViewAction extends AbstractUploadAction {

	private static Logger logger = Logger.getLogger(AbstractUploadViewAction.class);
	
	public AbstractUploadViewAction(String actionId, IWorkbenchWindow window, PafServer server) {
		super(actionId, window, server);
	}
	
	public AbstractUploadViewAction(String actionId, IWorkbenchWindow window, String serverGroupName, List<PafServer> servers) {
		super(actionId, window, serverGroupName, servers);
	}
	
	
	/**
	 * @return Gets the high level project node.
	 */
	public ProjectNode getProjectNode(){
		
		Object objSelection = selection.getFirstElement();
		
		if ( objSelection instanceof ViewsNode ) {

			return (ProjectNode) ((ViewsNode) objSelection).getParent();

		} else if ( objSelection instanceof ViewNode) {

			return ViewerUtil.getProjectNode(((ViewNode) objSelection).getProject().getName());

		}
		
		return null;
	}
	
	
	public Set<ProjectElementId> getProjectElementIdSet(){
		
		Set<ProjectElementId> projectElementIdSet = new HashSet<ProjectElementId>();
		
		projectElementIdSet.add(ProjectElementId.Views);
		projectElementIdSet.add(ProjectElementId.ViewSections);
		
		return projectElementIdSet;
	}
	
	public XMLPaceProject getPaceProject(String[] selectedItems){

		XMLPaceProject xpp = null;
		try {
			
			xpp = new XMLPaceProject(getIConfFolder().getLocation().toString(), getProjectElementIdSet(), false);
			
			Set<String> selectedViewSet = new HashSet<String>(Arrays.asList(selectedItems));
			
			List<PafView> selectedViewList = new ArrayList<PafView>();
			
			List<PafViewSection> selectedViewSectionList = new ArrayList<PafViewSection>();
			
			Map<String, PafViewSection> viewSectionMap = new HashMap<String, PafViewSection>();
			
			if ( xpp.getViewSections() != null ) {
				
				for (PafViewSection pafViewSection : xpp.getViewSections()) {
					
					viewSectionMap.put(pafViewSection.getName(), pafViewSection);
					
				}
				
			}
			
			for (PafView pafView : xpp.getViews()) {
				
				if ( selectedViewSet.contains(pafView.getName())) {
	
					selectedViewList.add(pafView);
					
					if ( pafView.getViewSectionNames() != null & pafView.getViewSectionNames().length == 1 ) {
						
						String viewSectionName = pafView.getViewSectionNames()[0];
						
						if ( viewSectionMap.containsKey(viewSectionName)) {
							
							selectedViewSectionList.add(viewSectionMap.get(viewSectionName));
							
						}
						
					}
					
				}
				
			}
			
			//if number of views differ from number of selected views, only update
			if ( xpp.getViews().size() != selectedViewList.size()) {
				xpp.setUpdateOnly(true);
			}
			
			xpp.setViews(selectedViewList);
			xpp.setViewSections(selectedViewSectionList);

																						
		} catch (InvalidPaceProjectInputException e) {
			logger.error(e.getMessage());
		} catch (PaceProjectCreationException e) {
			logger.error(e.getMessage());
		}
	
		return xpp;																	
	
	}

}

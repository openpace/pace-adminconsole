/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.widgets.StringSelectionDialog;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.views.MenuView;
import com.pace.base.PafBaseConstants;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.base.view.PafView;
import com.pace.base.view.PafViewSection;
import com.pace.server.client.PafSoapException_Exception;

/**
 * 
 * Downloads views from the selected server.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class DownloadViewsFromServerAction extends Action implements
		ISelectionListener, IWorkbenchAction {

	// looger
	private static Logger logger = Logger
			.getLogger(DownloadViewsFromServerAction.class);

	// holds current workbech window
	private final IWorkbenchWindow window;

	// server to import files from
	private final PafServer server;

	// current selection
	private IStructuredSelection selection = null;

	// does nothing
	private TreeViewer viewer = null;

	// ID code for action
	public final static String ID = "com.pace.admin.menu.actions.importexport.DownloadViewsFromServerAction";

	public DownloadViewsFromServerAction(IWorkbenchWindow window,
			PafServer server, TreeViewer viewer) {
		super(server.getName());
		this.window = window;
		this.server = server;
		this.viewer = viewer;
		window.getSelectionService().addSelectionListener(this);

	}

	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// if incoming is structured selection, set selection
		if (incoming instanceof IStructuredSelection) {

			selection = (IStructuredSelection) incoming;

		}

	}

	public void run() {
		
		if ( server != null && ServerMonitor.getInstance().isServerRunning(server) ) {
				
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
				
				public void run() {
			
					Object objSelection = selection.getFirstElement();
			
					//get the selections.
					Object[] objSelections = selection.toArray();
					
					// cast to project node
					ProjectNode project = null;
					
					String[] selectedViewNames = null;
					
					if ( objSelection instanceof ViewsNode ) {
						
						project = (ProjectNode) ((ViewsNode) objSelection).getParent();
						
					} else if ( objSelection instanceof ViewNode ) {
						
						project = ViewerUtil.getProjectNode(((ViewNode) objSelection).getProject().getName());
						
						selectedViewNames = new String[objSelections.length];
						
						int i = 0;
						
						for(Object o : objSelections){
							
							ViewNode vn = (ViewNode) o;
				    		
							selectedViewNames[i] = vn.getName();
							
							i++;
					    	
				    	}
						
						
					}
					
					String projectName = project.getName();
			
					// get workspace and root
					IWorkspaceRoot rootWorkspace = ResourcesPlugin.getWorkspace().getRoot();
			
					// get project and conf folder
					IProject iProject = rootWorkspace.getProject(projectName);
					
					IFolder confFolder = iProject.getFolder(Constants.CONF_DIR);
															
					Set<ProjectElementId> projectElementIdSet = new HashSet<ProjectElementId>();
					
					projectElementIdSet.add(ProjectElementId.Views);
					projectElementIdSet.add(ProjectElementId.ViewSections);
					
					PaceProject pp = WebServicesUtil.downloadProjectFromServer(server, projectElementIdSet);
					
					boolean successful = false;
					boolean cancelled = false;
					if ( pp != null && pp instanceof XMLPaceProject) {
					
						final XMLPaceProject xpp = (XMLPaceProject) pp;
						
						final List<PafView> pafViewsList = pp.getViews();
						
						if (pafViewsList != null) {
				
							String[] pafViewNames = new String[pafViewsList.size()];
																
							final Map<String, PafView> pafViewMap = new HashMap<String, PafView>();
				
							int ndx = 0;
				
							for (PafView pafView : pafViewsList) {
				
								pafViewNames[ndx++] = pafView.getName();
														
								pafViewMap.put(pafView.getName(), pafView);
				
							}
							
							List<PafViewSection> pafViewSectionList = xpp.getViewSections();
							
							final Map<String, PafViewSection> pafViewSectionMap = new HashMap<String, PafViewSection>();
							
							if ( pafViewSectionList != null ) {
								
								for (PafViewSection pafViewSection : pafViewSectionList) {
									
									pafViewSectionMap.put(pafViewSection.getName(), pafViewSection);
									
								}
								
							}
							
							if ( selectedViewNames == null ) {
								
								selectedViewNames = pafViewNames;
								
							}
				
							// create instance of dialog
							final StringSelectionDialog dialog = new StringSelectionDialog(PlatformUI
									.getWorkbench().getActiveWorkbenchWindow().getShell(),
									"Download Views", "Download Views",
									"Select the Views and associated View Sections to download from server '"
											+ server.getName() + "'.", pafViewNames, selectedViewNames);
				
							int rc = dialog.open();
				
							if (rc == 0) {
																
								String[] userSelectedViewNames = dialog.getSelectedItems();
					
								if (userSelectedViewNames != null) {
									
									if ( userSelectedViewNames.length != pafViewsList.size()) {
										
										xpp.setUpdateOnly(true);										
									
										List<PafView> selectedPafViewList = new ArrayList<PafView>();
										List<PafViewSection> selectedPafViewSectionList = new ArrayList<PafViewSection>();
										
										//populate selected view and view section lists
										for (String userSelectedViewName : userSelectedViewNames ) {
																																	
											if ( pafViewMap.containsKey(userSelectedViewName) ) {
											
												PafView pafView = pafViewMap.get(userSelectedViewName);
												
												selectedPafViewList.add(pafView);
												
												if ( pafView.getViewSectionNames() != null && pafView.getViewSectionNames().length > 0 ) {
											
													for (String userSelectedViewSectionName : pafView.getViewSectionNames()) {
														
														if ( pafViewSectionMap.containsKey(userSelectedViewSectionName)) {
															
															PafViewSection pafViewSection = pafViewSectionMap.get(userSelectedViewSectionName);
															
															if (! selectedPafViewSectionList.contains(pafViewSection)) {
																
																selectedPafViewSectionList.add(pafViewSection);
																
															}
															
														}
														
													}
													
												}
												
											}
																																	
										}
										
										xpp.setViews(selectedPafViewList);
										xpp.setViewSections(selectedPafViewSectionList);
									} 
																													
									try {
										
										xpp.saveTo(confFolder.getLocation().toString(), projectElementIdSet);
																				
										// conf folder in project
										IFolder iViewsFolder = iProject.getFolder(
												Constants.CONF_DIR).getFolder(
												PafBaseConstants.DN_ViewsFldr);
										
										// conf folder in project
										IFolder iViewSectionsFolder = iProject.getFolder(
												Constants.CONF_DIR).getFolder(
												PafBaseConstants.DN_ViewSectionsFldr);
										
										try {
											iViewSectionsFolder.refreshLocal(
													IResource.DEPTH_INFINITE, null);
											iViewsFolder.refreshLocal(IResource.DEPTH_INFINITE,
													null);
											confFolder.refreshLocal(IResource.DEPTH_INFINITE, null);
											
										} catch (CoreException e1) {
											logger.error(e1.getMessage());
										}
										
										// get ref to the menu view
										MenuView menuView = MenuPlugin.getDefault().getMenuView();
						
										// if menu view exist
										if (menuView != null) {
						
											//refresh project tree
											menuView.refreshProjectStructure();
											menuView.selectProjectNode(projectName);
																			
										}
										
										successful = true;											
															
									} catch (ProjectSaveException e) {
										logger.error(e.getMessage());
									}
																																																	
								}
							}
							else {
								cancelled = true;
							}
						
						}
						
					}
					
					String outMessage;
					
					if( ! cancelled  ) {
						if ( successful ) {
							
							outMessage = "The selected views were successfully downloaded from server '"
								+ server.getName() + "'.";
							
							logger.info(outMessage);
							
							GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
							
						} else {
							outMessage = "The selected views were not successfully downloaded from server '"
								+ server.getName()
								+ "'.  There was a problem downloading one or more views.";
								
							logger.error(outMessage);
							
							GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
							
						}
						ConsoleWriter.writeMessage(outMessage);
					}
			}});
		} else {
			
			GUIUtil.serverNotRunning(server);
			
		}

	}

	/**
	 * @return the server
	 */
	public PafServer getServer() {
		return server;
	}

}

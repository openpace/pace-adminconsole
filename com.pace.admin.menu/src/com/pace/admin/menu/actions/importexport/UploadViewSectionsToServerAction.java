/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.widgets.StringSelectionDialog;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;

/**
 * 
 * Deploys view sections to a selected server.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class UploadViewSectionsToServerAction extends AbstractUploadViewSectionAction {

	public final static String ID = "com.pace.admin.menu.actions.importexport.UploadViewSectionsToServerAction";
	
	/**
	 * 
	 * @param window
	 * @param server
	 */
	public UploadViewSectionsToServerAction(IWorkbenchWindow window, PafServer server) {
		super(ID, window, server);
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		if ( server != null ) {
			
			String[] selectedViewSectionNames = getSelectedViewSectionItems();
			
			//get iproject from root workspace
			final IProject iProject = getIProject();
			

			//get model manager
			ViewSectionModelManager modelManager = new ViewSectionModelManager(iProject);
	
			//get key neames
			String[] viewSectionNames = modelManager.getKeys();
			
			if ( selectedViewSectionNames == null ) {
				
				 selectedViewSectionNames  = viewSectionNames;
				 
			}
			
			Map<String, Boolean> checkboxMap = new TreeMap<String, Boolean>();
			
			checkboxMap.put(Constants.START_OR_RESTART_APPLICATION, Constants.START_OR_RESTART_APPLICATION_DEFAULT_VALUE);
			checkboxMap.put(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION, Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION_DEFAULT_VALUE);
			
			//create instance of dialog
			final StringSelectionDialog dialog = new StringSelectionDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
					"Upload View Sections", "Upload View Sections", 
					"Select the View Sections to upload to server '" + server.getName() + "'.", viewSectionNames, selectedViewSectionNames, checkboxMap);
			
			//open dialog
			int rc = dialog.open();
			
			//if user clicked ok
			if ( rc == 0 ) {
				
				if ( ServerMonitor.getInstance().isServerRunning(server) ) {
				
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
						
						public void run() {
																	
							//get the selected items
							String[] selectedItems = dialog.getSelectedItems();
								
							//if itmes have been selected
							if ( selectedItems != null && selectedItems.length > 0 ) {
							
								
								XMLPaceProject xpp = getPaceProject(selectedItems);
								boolean refreshConfiguration = dialog.getOptionalCheckOptionsMap().get(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
								boolean refreshCube = dialog.getOptionalCheckOptionsMap().get(Constants.START_OR_RESTART_APPLICATION);
								
								boolean uploadSuccessful = WebServicesUtil.uploadProjectToServer(xpp, server, refreshConfiguration, refreshCube, getProjectElementIdSet());
								
								String outMessage = null;
																				
								if ( ! uploadSuccessful ) {
									
									outMessage = "There were one or more problems uploading the selected view sections to server '" 
										+ server.getName() + "'.  Please check the logs.";
									
									GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
									
								} else {
									
									ServerMonitor.getInstance().refreshAllServerAndApplicationStates();
									
									outMessage = "The selected view sections were successfully uploaded.";
									
									GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
								}
								
								ConsoleWriter.writeMessage(outMessage);
								
							}
					}});
				} else {
					
					GUIUtil.serverNotRunning(server);
										
				}
			}
		}
	}
}

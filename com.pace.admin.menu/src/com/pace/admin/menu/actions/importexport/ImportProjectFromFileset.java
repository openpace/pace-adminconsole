/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.ui.IWorkbenchWindow;
import org.tigris.subversion.subclipse.core.SVNProviderPlugin;
import org.tigris.subversion.subclipse.core.resources.SVNWorkspaceRoot;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.views.MenuView;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.AliasMapping;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureType;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.app.PafPlannerRole;
import com.pace.base.app.PafUserDef;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;
import com.pace.base.app.PlanCycle;
import com.pace.base.app.Season;
import com.pace.base.app.SeasonList;
import com.pace.base.app.VarRptgFlag;
import com.pace.base.app.VersionDef;
import com.pace.base.app.VersionType;
import com.pace.base.comm.PafPlannerConfig;
import com.pace.server.client.PafSimpleBaseTree;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.base.project.ExcelPaceProject;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.ProjectSerializationType;
import com.pace.base.project.ZipPaceProject;
import com.pace.base.view.PafUserSelection;
import com.pace.base.view.PafView;
import com.pace.base.view.PafViewGroup;
import com.pace.base.view.PafViewGroupItem;
import com.pace.base.view.PafViewHeader;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.server.client.PafSimpleBaseMember;
import com.pace.server.client.PafSimpleBaseMemberProps;
import com.pace.server.client.PafSimpleDimMember;
//import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;

public class ImportProjectFromFileset extends ImportExportAction {
	
	private static Logger logger = Logger.getLogger(ImportProjectFromFileset.class);
	
	private final IProject project;
	
	private final String importPathFileName;
	
	private final ProjectSourceType importProjectType;
	
	private final boolean ignoreDependencies;
	
	// properties for New project from essbase outline
	
	private PafApplicationDef pafDefEsb ;
	
	private PafSimpleDimTree simpleDimTree;
	
	private String lastPeriod;
	
	private String currentYear;
	
	private String firstVersion;
	
	private List<String> versionList ;
	
	// hold first plan type for Essbase project 
	private String firstPT;

	
	private List<PafSimpleDimMember> simpleDimMembers = new ArrayList<PafSimpleDimMember>();
	
	public final static String ID = "com.pace.admin.menu.actions.importexport.ImportProjectFromFileset";
	
	public ImportProjectFromFileset(IWorkbenchWindow window, String importPathFileName, 
			IProject project, ProjectElementId[] elements, boolean ignoreDependencies, ProjectSourceType importProjectType) {

		//super(project.getName());
		
		super(ID, window, elements);
		
		this.project = project;
		
		this.importPathFileName = importPathFileName;
		
		this.importProjectType = importProjectType;
		
		this.ignoreDependencies = ignoreDependencies;
	}

	public ImportProjectFromFileset(IWorkbenchWindow window, String importPathFileName, 
			IProject project, ProjectElementId[] elements, boolean ignoreDependencies, ProjectSourceType importProjectType, PafApplicationDef pafAppDef) {

		//super(project.getName());
		
		super(ID, window, elements);
		
		this.project = project;
		
		this.importPathFileName = importPathFileName;
		
		this.importProjectType = importProjectType;
		
		this.ignoreDependencies = ignoreDependencies;
		
		this.pafDefEsb = pafAppDef;
		
		
	}
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		setError(false);
		
		PaceProject pp = null;
			
		Set<ProjectElementId> filter = new HashSet<ProjectElementId>(Arrays.asList(elements));
		
		String outMessage = ""; 
		
		IFolder confFolder = project.getFolder(Constants.CONF_DIR);
		
		PafApplicationDef oldPafAppDef = null;
		
		IFile pafAppDefFile = confFolder.getFile(PafBaseConstants.FN_ApplicationMetaData);
		
		if(pafAppDefFile.exists()){
			oldPafAppDef = PafApplicationUtil.getPafApp(project);
		}
			

		if(elements != null && elements.length > 0){
			try {
				
				switch ( importProjectType ) {
				
				case Excel:
					
					ExcelPaceProject epp = null;
					try{
					
						//create an Excel Pace Project.
						epp = new ExcelPaceProject(
								importPathFileName,
								filter);
						
					
					}catch (PaceProjectCreationException e) {

						setError(true);
						setProjectDataErrors(e.getProjectCreationErrorMap());
						
						String errorMessage = "There was a problem importing from the following input: " + importPathFileName + ". Please select the Show Errors button to see the errors.";
						
						logger.error(errorMessage);
						ConsoleWriter.writeMessage(errorMessage);
						setThrowable(e);
						return;
					}
					
					pp = epp.convertTo(ProjectSerializationType.XML);
					
					break;
					
				case Template:
				case PaceArchiveFile:

					ZipPaceProject zpp = null;
					try{
						
						//create an zip Pace Project.
						zpp = new ZipPaceProject(
								importPathFileName,
								filter,
								true);
					
					}catch (PaceProjectCreationException e) {

						setError(true);
						setProjectDataErrors(e.getProjectCreationErrorMap());
						logger.error("Problem with creating Pace Project: " + e.getMessage());
						ConsoleWriter.writeMessage("Problem with creating Pace Project: " + e.getMessage());
						setThrowable(e);
						return;
					}
					
					pp = zpp.convertTo(ProjectSerializationType.XML);
					
					break;
				case EssbaseOutline:

					ZipPaceProject xpp = null;
					try{
						
						//create an xml Pace Project.
						xpp = new ZipPaceProject(
								importPathFileName,
								filter,
								true);
						
					
					
					
					
				//	pp = xpp.convertTo(ProjectSerializationType.XML);
					List<PafApplicationDef> pafDef = xpp.getApplicationDefinitions();
					
					List<String> axisPriorityList = new ArrayList<String>();
					
					logger.debug("Creating App Def for Essbase Outline Project");
					
					// Set the datasource and dimensions for the Project based on the USer Selections
					pafDef.get(0).getMdbDef().setDataSourceId(pafDefEsb.getMdbDef().getDataSourceId());
					pafDef.get(0).getMdbDef().setMeasureDim(pafDefEsb.getMdbDef().getMeasureDim());
					pafDef.get(0).getMdbDef().setTimeDim(pafDefEsb.getMdbDef().getTimeDim());
					pafDef.get(0).getMdbDef().setYearDim(pafDefEsb.getMdbDef().getYearDim());
					pafDef.get(0).getMdbDef().setPlanTypeDim(pafDefEsb.getMdbDef().getPlanTypeDim());
					pafDef.get(0).getMdbDef().setVersionDim(pafDefEsb.getMdbDef().getVersionDim());
					
					pafDef.get(0).getMdbDef().setMeasureRoot(pafDefEsb.getMdbDef().getMeasureRoot());
					
					// add the dimensions to axis priority list based on the requirements
					axisPriorityList.add(pafDefEsb.getMdbDef().getTimeDim());
					axisPriorityList.add(pafDefEsb.getMdbDef().getMeasureDim());
					axisPriorityList.add(pafDefEsb.getMdbDef().getVersionDim());
					axisPriorityList.add(pafDefEsb.getMdbDef().getPlanTypeDim());
					axisPriorityList.add(pafDefEsb.getMdbDef().getYearDim());
					
					// create array for AxisPriority
					String[] axisPriority  = new String[pafDefEsb.getMdbDef().getHierDims().length];
					List<String> hierDims = new ArrayList<String>();
					axisPriority = pafDefEsb.getMdbDef().getHierDims();
					
					// add the remaining dimensions to the axis priority list 
					for(int i=0;i<axisPriority.length;i++)
					{
						if( ! axisPriorityList.contains(axisPriority[i]))
						{
							axisPriorityList.add(axisPriority[i]);
							hierDims.add(axisPriority[i]);
							
						}
					}
					
					// set axis priority , last period and current year
					pafDef.get(0).getMdbDef().setAxisPriority(axisPriorityList.toArray(new String[0]));
					pafDef.get(0).getMdbDef().setHierDims(hierDims.toArray(new String[0]));
					pafDef.get(0).setLastPeriod(getLastPeriod());
					
					pafDef.get(0).setCurrentYear(getCurrentYear());
					
					// TTN-2609 Remove Invalid Alias Mappings
					AliasMapping[] aliasMappings = new AliasMapping[pafDefEsb.getMdbDef().getHierDims().length];
					
					for (int i=0;i<pafDefEsb.getMdbDef().getHierDims().length;i++){
						aliasMappings[i] = new AliasMapping();
						aliasMappings[i].setDimName(pafDefEsb.getMdbDef().getHierDims()[i]);
						aliasMappings[i].setAliasTableName("Default");
						aliasMappings[i].setPrimaryRowColumnFormat("Alias");
					}
					
					pafDef.get(0).getAppSettings().setGlobalAliasMappings(aliasMappings);
					
					// set the complete app def
					xpp.setApplicationDefinitions(pafDef);
					
					
					logger.debug("Creating versions for Essbase Outline Project");
					
					// set version Def 
					List<VersionDef> versionDefList = new ArrayList<VersionDef>() ;
					
					// Add the Version dimensions first to the versions.xml.
					VersionDef versiond = new VersionDef();
					versiond.setName(pafDefEsb.getMdbDef().getVersionDim());
					versiond.setType(VersionType.NonPlannable);
					versionDefList.add(versiond);
					
					for( int i =0;i< versionList.size();i++)
					{
						VersionDef vd = new VersionDef();
						vd.setName(versionList.get(i));
						vd.setType(VersionType.Plannable);
						versionDefList.add(vd);
					
					}
				//	pafDef.get(0).initVersions(versionDefList);
					xpp.setVersions(versionDefList);
					
					logger.debug("Creating plan cycles, season for Essbase Outline Project");
					
					//set Plan Cycle
					PlanCycle[] pc =  pafDef.get(0).getPlanCycles();
				
					pc[0] = new PlanCycle();
					pc[0].setVersion(firstVersion);
					pc[0].setLabel(firstVersion+" Cycle");
					
					List<PlanCycle> planCycleList = new ArrayList<PlanCycle>();
					planCycleList.add(pc[0]);
					
				
					xpp.setPlanCycles(planCycleList);
					
					// set Season
					List<Season> seasonList  = new ArrayList<Season>();
					Season season = new Season();
					season.setId(firstVersion+" Cycle"+" "+getCurrentYear());
					season.setPlanCycle(pc[0].getLabel());
					season.isOpen();
					
					String[] planYears = new String[1];
					planYears[0] = getCurrentYear();
					season.setPlannableYears(planYears);
					season.setYears(planYears);
					
					season.setTimePeriod("@ICHILDREN("+pafDefEsb.getMdbDef().getTimeDim()+")");
					
					seasonList.add(season);
					
					xpp.setSeasons(seasonList);
					
					
					logger.debug("Creating Role Config, Role for Essbase Outline Project");
					
					
					// Add a Role
					PafPlannerRole roleConfig = new PafPlannerRole();
					roleConfig.setPlanType(firstPT);
					roleConfig.setRoleName("Planner");
					roleConfig.setRoleDesc("A Sample Role");
					
					// set the season id for role config created previously
					String[] seasonIds = new String[1];
					seasonIds[0] = season.getId();
					roleConfig.setSeasonIds(seasonIds);
					
					roleConfig.setReadOnly(false);
					
					List<PafPlannerRole> rolePlannerList = new ArrayList<PafPlannerRole>();
					rolePlannerList.add(roleConfig);
					
					xpp.setRoles(rolePlannerList);
					
					// Add a Role Config 
					PafPlannerConfig plannerConfig = new PafPlannerConfig();
					plannerConfig.setRole(roleConfig.getRoleName());
					plannerConfig.setCycle("");
					
					// add views to the role config
					String[] viewItems = new String[1];
					viewItems[0] = "All Views";		
					plannerConfig.setViewTreeItemNames(viewItems);
					
					plannerConfig.setDefaultEvalEnabledWorkingVersion(false);
					plannerConfig.setMdbSaveWorkingVersionOnUowLoad(false);
					plannerConfig.setCalcElapsedPeriods(false);
					
					String[] rsName = new String[1];
					rsName[0] = "BlankRules";
					plannerConfig.setRuleSetNames(rsName);
					plannerConfig.setDefaultRulesetName("BlankRules");
					
					List<PafPlannerConfig> plannerList = new ArrayList<PafPlannerConfig>();
					plannerList.add(plannerConfig);
					
					xpp.setRoleConfigurations(plannerList);
					
					logger.debug("Setting the AppDef Essbase Outline Project");
					
					
					//set the user security
				
					// Create Dim specs for the hierarchy dimensions to be used with Role Filters
					PafDimSpec[] specs = new PafDimSpec[hierDims.size()];
					
					for(int i=0;i<specs.length;i++)
					{
						specs[i] = new PafDimSpec();
						specs[i].setDimension(hierDims.get(i));
						String[] expList = new String[1];
						expList[0] = "@ICHILD("+hierDims.get(i)+")";
						specs[i].setExpressionList(expList);
					}
					
					// add the dim specs to the work spec 
					PafWorkSpec[] workSpec = new PafWorkSpec[1];
					workSpec[0] = new PafWorkSpec();
					workSpec[0].setName(roleConfig.getRoleName());
					workSpec[0].setDimSpec(specs);

					logger.debug("Creating User Security  for Essbase Outline Project");
					
					PafUserSecurity userSecurity = new PafUserSecurity();
					LinkedHashMap<String, PafWorkSpec[]> roleMap  = new LinkedHashMap<String,PafWorkSpec[]>();
					
					roleMap.put(roleConfig.getRoleName(), workSpec);
					// set the role created previously
					userSecurity.setUserName("admin");
					userSecurity.setRoleFilters(roleMap);
					userSecurity.setAdmin(true);
					
					
					List<PafUserSecurity> securityList = new ArrayList<PafUserSecurity>();
					securityList.add(userSecurity);
						
					xpp.setUserSecurity(securityList);
					
					// Set the measures in measures.xml based on the selections and then set the properties and type of measure accordingly.
					
					logger.debug("Creating Measures for Essbase Outline Project");
					
					List<MeasureDef> measureDef = new ArrayList<MeasureDef>(); 
				
					
					
					if(simpleDimMembers!=null)
					{
					for(int i=0;i<simpleDimMembers.size();i++)
					{
						MeasureDef mDef;
						
						PafSimpleDimMemberProps baseProps = simpleDimMembers.get(i).getPafSimpleDimMemberProps();
						
						
						if(baseProps.isTwoPassCalc())
							mDef = new MeasureDef(simpleDimMembers.get(i).getKey(),MeasureType.Recalc);
						else if(baseProps.getTimeBalanceOption()==1)
							mDef = new MeasureDef(simpleDimMembers.get(i).getKey(),MeasureType.TimeBalFirst);
						else if(baseProps.getTimeBalanceOption()==2)
							mDef = new MeasureDef(simpleDimMembers.get(i).getKey(),MeasureType.TimeBalLast);
						else
							mDef = new MeasureDef(simpleDimMembers.get(i).getKey(),MeasureType.Aggregate);
						
						mDef.setPlannable(true);
						
						if(baseProps.isExpense())
							mDef.setVarRptgFlag(VarRptgFlag.ExpenseReporting);
						else
							mDef.setVarRptgFlag(VarRptgFlag.RevenueReporting);
							
						measureDef.add(mDef);
					}
					
					xpp.setMeasures(measureDef);
					
				
					
					}
					
					String[] dims = pafDefEsb.getMdbDef().getHierDims();
					
					
				logger.debug("Creating view section , view, view group for Essbase Outline Project");
					// Create View Section
					PafViewSection vs = new PafViewSection();
					int dimsOnPage = pafDefEsb.getMdbDef().getHierDims().length - 2 ;
					List<PageTuple> pageList = new ArrayList<PageTuple>();
					PageTuple[] pagetp = new PageTuple[dimsOnPage];
					
					
					
					
					
					
					for( int i =0;i<dims.length;i++)
					{
					    
						// if dimension not time or measures then it goes on page axis.
						if(!dims[i].equals(pafDefEsb.getMdbDef().getTimeDim()) )
						{
						  if(!dims[i].equals(pafDefEsb.getMdbDef().getMeasureDim()))
						  {
							
							
							PageTuple pTuple = new PageTuple();
							
						    pTuple.setAxis(dims[i]);
							if(dims[i].equals(pafDefEsb.getMdbDef().getVersionDim()))
							{
								pTuple.setMember("@PLAN_VERSION");
								
							}
							else if(dims[i].equals(pafDefEsb.getMdbDef().getYearDim()))
							{
								pTuple.setMember("@UOW_ROOT");
							}
							else
								pTuple.setMember("@UOW_ROOT");
							
							
							pageList.add(pTuple);
									
						}
						}
					}
					
					vs.setPageTuples(pageList.toArray(new PageTuple[0]));
					
					// Set Row tuples for View Section
					ViewTuple[] rowtp = new ViewTuple[1];
					rowtp[0] = new ViewTuple();
					rowtp[0].setAxis(0);
					rowtp[0].setMemberDefs(new String[]{"@CHILDREN(@UOW_ROOT)"});
					rowtp[0].setHeaderGlobalStyleName("Header Left Normal");
					
					String[] rowAxisDims = new String[1];
					rowAxisDims[0] = pafDefEsb.getMdbDef().getMeasureDim();
					vs.setRowAxisDims(rowAxisDims);
					vs.setRowTuples(rowtp);
					
					// Set Column Tuples for View Section
					ViewTuple[] coltp = new ViewTuple[1];
					coltp[0] = new ViewTuple();
					coltp[0].setAxis(1);
					coltp[0].setMemberDefs(new String[]{"@ICHILDREN(@UOW_ROOT)"});
					coltp[0].setHeaderGlobalStyleName("Header Left Normal");
					
					String[] colAxisDims = new String[1];
					colAxisDims[0] = pafDefEsb.getMdbDef().getTimeDim();
					vs.setColAxisDims(colAxisDims);
					vs.setColTuples(coltp);
					
					vs.setName("SampleView_vs");
					
					// Create and Set View headers
					PafViewHeader[] viewHeaders = new PafViewHeader[dimsOnPage + 1];
					viewHeaders[0] = new PafViewHeader();
					viewHeaders[0].setLabel("@VIEW_NAME");
					viewHeaders[0].setGlobalStyleName("Bold");
					
					// headers for all page dims
					for(int i=1;i<=dimsOnPage;i++)
					{
					viewHeaders[i] = new PafViewHeader();
					viewHeaders[i].setLabel("@"+pageList.get(i-1).getAxis());
					viewHeaders[i].setGlobalStyleName("Bold");
					}
					
					vs.setPafViewHeaders(viewHeaders);
					
					
					List<PafViewSection> vsList = new ArrayList<PafViewSection>();
					vsList.add(vs);
					
					xpp.setViewSections(vsList);

					
					// create a view to hold the previously created view section
					
					PafView view = new PafView();
					view.setName("Sample View");
					view.setViewSections(new PafViewSection[]{vs});
				    view.setViewSectionNames(new String[]{vs.getName()});
					
					List<PafView> viewList = new ArrayList<PafView>();
					viewList.add(view);
					xpp.setViews(viewList);
					
					
					// Setup the AllViews View Group
					PafViewGroup viewGroup = new PafViewGroup();
					viewGroup.setName("All Views");
					PafViewGroupItem[] groupItems = new PafViewGroupItem[1];
					groupItems[0] = new PafViewGroupItem();
					groupItems[0].setName("Sample View");
					
					viewGroup.setPafViewGroupItems(groupItems);
					HashMap<String, PafViewGroup> groupMap = new HashMap<String, PafViewGroup>();
					groupMap.put("All Views", viewGroup);
					xpp.setViewGroups(groupMap);
					
					logger.debug("Creating user selections for Essbase Outline Project");
					
					// setup the User Selections , one single and one multi for each hierarchy dimension
				//	PafUserSelection[] userSel = new PafUserSelection[dims.length*2];
					List<PafUserSelection> userSelList = new ArrayList<PafUserSelection>();
					for(int i=0;i<dims.length;i++)
					{
						PafUserSelection userSelSingle = new PafUserSelection();
						
						userSelSingle.setId(dims[i]+"_SINGLE");
						userSelSingle.setMultiples(false);
						userSelSingle.setDimension(dims[i]);
						userSelSingle.setPromptString("Select 1 "+dims[i]);
						
						userSelList.add(userSelSingle);
						
						PafUserSelection userSelMulti = new PafUserSelection();
						
						userSelMulti.setId(dims[i]+"_MULTI");
						userSelMulti.setMultiples(true);
						userSelMulti.setDimension(dims[i]);
						userSelMulti.setPromptString("Select "+dims[i]+ "(s)");
						
						userSelList.add(userSelMulti);
					}
					
					xpp.setUserSelections(userSelList);
					
					}catch (PaceProjectCreationException e) {

						setError(true);
						setProjectDataErrors(e.getProjectCreationErrorMap());
						logger.error("Problem with creating Pace Project from Essbase Outline: " + e.getMessage());
						ConsoleWriter.writeMessage("Problem with creating Pace Project from Essbase Outline: " + e.getMessage());
						setThrowable(e);
						return;
					}
					pp = xpp.convertTo(ProjectSerializationType.XML);
					
					break;
				
				}		
			
	
			}  catch (InvalidPaceProjectInputException e) {
				
				setError(true);
				logger.error("Problem refreshing local folder: " + e.getMessage());
				ConsoleWriter.writeMessage("Problem refreshing local folder: " + e.getMessage());
				setThrowable(e);
				return;
				
			}
			
			
		}
		
		
		try {
			
			
			
			boolean isProjectManagedBySubclipse = SVNWorkspaceRoot.isManagedBySubclipse(project);

			//if svn project, unmap from reposity, will remap in a bit				
			if ( isProjectManagedBySubclipse ) {
				
				RepositoryProvider.unmap(project);
			
			}
			
			
			
			//backup the project
			PafProjectUtil.backupProject(project, elements);
			
			
			
			//saveTo the PaceProject to the conf dir.
			if(ignoreDependencies){
				
				pp.saveTo(confFolder.getLocation().toString(), filter);
				
			}
			else{
				
				pp.saveTo(confFolder.getLocation().toString());
				
			}
			
		
					
			confFolder.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
			
			
			//if svn project, remap to repository
			if ( isProjectManagedBySubclipse ) {

				RepositoryProvider.map(project, SVNProviderPlugin.getTypeId());
				
				IFolder backupFolder = project.getFolder(Constants.BACKUP);
				
				//TODO: Add ignore for backup folder and change move project from server to copy each file and dir seperate
				//		it's causing the svn project to become corrupt.
				if ( backupFolder.exists()) {
					
					//ISVNLocalResource svnResource = SVNWorkspaceRoot.getSVNResourceFor(backupFolder);
	                //new AddIgnoredPatternCommand(svnResource.getParent(), "*.").run(new NullProgressMonitor());
					
				}
				
			}
			
			
			
			//TTN-710 BEGIN
			//prompt user about changing app ids if conflicting with existing app id
			GUIUtil.promptUserAboutAppId(oldPafAppDef, project);
			//TTN-710 END			
			
			
			
//			//run the refresh action
//			
//			refreshAction.run();
//			
//			// get ref to the menu view
//			MenuView menuView = MenuPlugin.getDefault().getMenuView();
//
//			// if menu view exist
//			if (menuView != null) {
//			
//				menuView.selectProjectNode(project.getName());
//				
//			}
			
			//tell the user.
			outMessage = "Project '" + project.getName()
				+ "' was successfully imported from: '"
				+ importPathFileName + "'";
			
			
			
			logger.debug("Created the Project");
			
		} catch (ProjectSaveException e) {

			setError(true);
			logger.error("Error saving Pace Project: " + e.getMessage());
			ConsoleWriter.writeMessage("Error saving Pace Project: " + project.getName() + "." + e.getMessage());
			logger.error(e.getStackTrace());
			setThrowable(e);
			
		} catch (Exception e){
			
			setError(true);
			logger.error("Error saving Pace Project: " + e.getMessage());
			ConsoleWriter.writeMessage("Error saving Pace Project: " + project.getName() + "." + e.getMessage());
			setThrowable(e);
			
		} finally{
			
			ConsoleWriter.writeMessage(outMessage);
			
		}
		
	}
	
	public void setSimpleDimTree(PafSimpleDimTree _dimTree)
	{
		simpleDimTree = _dimTree;
	}
	
	public void setSimpleBaseTreeMembers(List<PafSimpleDimMember> _dimTreeMembers)
	{
		this.simpleDimMembers =   _dimTreeMembers;
	}

	public void setLastPeriod(String lastPeriod)
	{
		this.lastPeriod = lastPeriod;
	}
	
	public String getLastPeriod()
	{
		return this.lastPeriod;
	}
	
	public void setCurrentYear(String CY)
	{
		this.currentYear = CY;
	}
	
	public String getCurrentYear()
	{
		return this.currentYear;
	}
	
	public String getFirstVersion() {
		return firstVersion;
	}

	public void setFirstVersion(String firstVersion) {
		this.firstVersion = firstVersion;
	}
	
	public List<String> getAllVersions() {
		return versionList;
	}

	public void setAllVersions(List<String> _versionList) {
		this.versionList = _versionList;
	}
	
	public String getFirstPlanType() {
		return firstPT;
	}

	public void setFirstPlanType(String _firstPT) {
		this.firstPT = _firstPT;
	}
}

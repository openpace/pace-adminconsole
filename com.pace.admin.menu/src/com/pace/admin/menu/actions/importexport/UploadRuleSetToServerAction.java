/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.inputs.UploadProjectInput;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

/**
* Action, deploys rule sets to the server
* @author kb
* @version x.xx
*/
public class UploadRuleSetToServerAction extends Action implements ISelectionListener {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.importexport.UploadRuleSetToServerAction";
	
	private static Logger logger = Logger.getLogger(UploadRuleSetToServerAction.class);
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
	
	private IProject project;
	
	private PafServer server = null;

	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public UploadRuleSetToServerAction(IWorkbenchWindow window, PafServer server, IProject project) {
		
		this.window = window;
		this.server = server;
		this.setId(ID);
        this.setText(server.getName());
        this.project = project;
		window.getSelectionService().addSelectionListener(this);
		
	}
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Deploys user security to server
	 */
	public void run() {
		
		
			if (project != null && project.exists() && server != null) {
				
				logger.info("Uploadin rule sets to server '" + server.getName() + "'");
				
				UploadProjectInput input = new UploadProjectInput(project.getName(), server);
				
				input.setFilterSet(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.RuleSets)));
				
				PafProjectUtil.uploadProjectToServer(input);
				
			}
			
		
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	
	
	/**
	 * @return the server
	 */
	public PafServer getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(PafServer server) {
		this.server = server;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}
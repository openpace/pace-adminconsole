/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.actions.projects.RefreshProjectsAction;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

public class ImportProjectFromServer extends ImportExportAction {
	
	private static Logger logger = Logger.getLogger(ImportProjectFromServer.class);
	
	private IProject project;
	
	private final PafServer pafServer;
	
	public ImportProjectFromServer(IWorkbenchWindow window, ProjectElementId[] elements,
			String projectName, PafServer pafServer){
	
		super(projectName, window, elements);
		
		this.project = null;
		this.pafServer = pafServer;
		
	}
	
	
	public ImportProjectFromServer(IWorkbenchWindow window, ProjectElementId[] elements, 
			IProject project, PafServer pafServer){

		super(project.getName(), window, elements);
		this.project = project;
		this.pafServer = pafServer;
		
		refreshAction = new RefreshProjectsAction(this.window);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		//do something...
		setError(false);
	
		//if running
		if ( pafServer != null && ServerMonitor.getInstance().isServerRunning(pafServer) ) {
		
			IFolder confFolder = project.getFolder(Constants.CONF_DIR);
			
			PafApplicationDef oldPafAppDef = null;
			
			IFile pafAppDefFile = confFolder.getFile(PafBaseConstants.FN_ApplicationMetaData);
			
			if(pafAppDefFile.exists()){
				oldPafAppDef = PafApplicationUtil.getPafApp(project);
			}
			
			Set<ProjectElementId> filterList = new HashSet<ProjectElementId>();
			
			if ( elements != null ) {
				filterList.addAll(Arrays.asList(elements));
			}
			PafApplicationDef newPafAppDef = PafApplicationUtil.getPafApp(project);
			
			
			
			boolean successfulDownload = WebServicesUtil.downloadProjectFromServer(project, pafServer, filterList);
			
			if ( ! successfulDownload ) {
			
				setError(true);
				
			}
			
			// TTN-2731 - Set Default Server on Project Import
			PafProjectUtil.setDefaultProjectServer(project, pafServer);
		
				
			//TTN-710 BEGIN
			//prompt user about changing app ids if conflicting with existing app id
		
			//TTN-710 END			
			GUIUtil.promptUserAboutAppIdForServer(oldPafAppDef, project);
//			//run the refresh action
//			refreshAction.run();
//			
//			// get ref to the menu view
//			MenuView menuView = MenuPlugin.getDefault().getMenuView();
//	
//			// if menu view exist
//			if (menuView != null) {
//			
//				menuView.selectProjectNode(project.getName());
//				
//			}			
		
		} else {
			
			setError(true);
		
			String errorMessage = "Server '" + pafServer.getName() + "' is no longer running.  Please start the server and try again.";
			
			logger.error(errorMessage);
			
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, errorMessage, SWT.ICON_ERROR);
			
		}
		
	}

}

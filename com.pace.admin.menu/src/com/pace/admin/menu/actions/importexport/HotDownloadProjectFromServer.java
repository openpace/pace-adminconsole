/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.inputs.DownloadProjectInput;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PafServer;

/**
 * Hot download project from server.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class HotDownloadProjectFromServer extends Action implements
		ISelectionListener, IWorkbenchAction {
	
	public final static String ID = "com.pace.admin.menu.actions.importexport.HotProjectRefreshAction";
	
	private static final Logger logger = Logger.getLogger(HotDownloadProjectFromServer.class);
	
	private final IWorkbenchWindow window;
	
	private ProjectNode projectNode = null;
	
	private PafServer pafServer = null;

	public HotDownloadProjectFromServer(IWorkbenchWindow window) {
		
		super(ID);
		
		this.window = window;
		
		setToolTipText("Download ALL project xml from the project's default server.");
		
		setImageDescriptor(MenuPlugin.getImageDescriptor("/icons/hot_project_refresh.gif"));
		
		setEnabled(false);
		
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
			
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		if ( projectNode != null && pafServer != null ) {
									
			DownloadProjectInput input = new DownloadProjectInput(projectNode.getName(), pafServer);
			input.setSlientMode(pafServer.isDoesNotPromptOnHotDeploy());
			input.setHotDeploy(true);
			
			PafProjectUtil.downloadProjectFromServer(input);
			
		} else {
			
			logger.error("Project node or paf server was null");
			
		}
		
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {

		if ( selection instanceof IStructuredSelection ) {
			
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					
			if (structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof ProjectNode ) {
				
				projectNode = (ProjectNode) structuredSelection.getFirstElement();
				
				try {
					pafServer = PafProjectUtil.getProjectServer(projectNode.getProject());
					//TTN 1723 - Project tree not updating with new default project server after setting a default server
	    			MenuPlugin.getDefault().getMenuView().getViewer().refresh();
					
					setEnabled(ServerMonitor.getInstance().isServerRunning(pafServer));
					
				} catch (PafServerNotFound e) {
					logger.error(e.getMessage());
					setEnabled(false);
					pafServer = null;
				}
				
			} else {
				
				projectNode = null;
				
				setEnabled(false);
				
			}
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);
		
	}
	
	

}

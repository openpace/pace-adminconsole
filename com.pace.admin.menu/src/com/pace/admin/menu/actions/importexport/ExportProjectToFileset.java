/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.project.ExcelPaceProject;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.ProjectSerializationType;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.project.utils.PafExcelUtil;
import com.pace.base.utility.FileUtils;

public class ExportProjectToFileset extends ImportExportAction {
	
	private static Logger logger = Logger.getLogger(ExportProjectToFileset.class);
	
	private final String sourceFile;
	
	private final String sourceConfDir;
	
	private final String projectName;

	private final ProjectSerializationType exportProjectType;
	
	private final boolean excelCellReferencing;
	
	private final boolean addDependencies;
	
	public final static String ID = "com.pace.admin.menu.actions.importexport.ExportProjectToFileset";

	public ExportProjectToFileset(String projectName, String sourceConfDir, String sourceFile, 
			ProjectElementId[] elements, boolean excelCellReferencing, boolean addDependencies,
			ProjectSerializationType exportProjectType) {
		
		//super(sourceFile);
		
		super(sourceFile, null, elements);
	
		this.projectName = projectName;
		
		this.sourceConfDir = sourceConfDir;
		
		this.sourceFile = sourceFile;
		
		this.exportProjectType = exportProjectType;
		
		this.addDependencies = addDependencies;
		
		this.excelCellReferencing = excelCellReferencing;
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		XMLPaceProject xpp = null;
			
		try {
			
			if(addDependencies){
				
				xpp = new XMLPaceProject(
						sourceConfDir,
						ExcelPaceProject.getProjectIdSetDependencies(new HashSet<ProjectElementId> (Arrays.asList(elements))),
						false);
				
				
			} else {
				
				xpp = new XMLPaceProject(
						sourceConfDir,
						new HashSet<ProjectElementId>(Arrays.asList(elements)),
						false);
				
			}

		} catch (InvalidPaceProjectInputException e1) {
			
			setError(true);
			logger.error("Invalid Pace Project: " + projectName);
			ConsoleWriter.writeMessage("Invalid Pace Project: " + projectName);
			e1.printStackTrace();
			setThrowable(e1);
			return;
			
		} catch (PaceProjectCreationException e1) {
			
			setError(true);
			setProjectDataErrors(e1.getProjectCreationErrorMap());
			logger.error("Problem creating Pace Project: " + e1.getMessage());
			ConsoleWriter.writeMessage("Problem creating Pace Project: " + e1.getMessage());
			setThrowable(e1);
			return;
			
		}
			
		PaceProject pp = null;
		try {
			
			pp = xpp.convertTo(exportProjectType);
		
			switch (exportProjectType){
			
				case XLSX:
					
					if(pp instanceof ExcelPaceProject){
						
						ExcelPaceProject epp = (ExcelPaceProject) pp;
						
						epp.setCellReferencingEnabled(excelCellReferencing);	
						
						epp.setAddDependenciesOnSave(addDependencies);
						
						File excelFile = new File(sourceFile);
						
						boolean isNonExistExcelMacroFile = false;
						
						//if xlsm file and xlsm file doesn't already exists, copy over blank workbook and write to it.
						if ( excelFile.toString().endsWith(PafBaseConstants.XLSM_EXT) && ! excelFile.exists() ) {							
							
							isNonExistExcelMacroFile = true;
							
							URL bundleURL = Platform.getBundle(MenuPlugin.ID).getEntry("resources\\blank.xlsm");
							
							try {
								URL excelFileUrl = FileLocator.resolve(bundleURL); 
								
								File blankExcelWithMacro = new File(excelFileUrl.toURI());
								
								//copy blank file with macros to excel file
								FileUtils.copy(blankExcelWithMacro, excelFile);								
								
							} catch (URISyntaxException e) {
								logger.error(e.getMessage());
								setThrowable(e);
							} catch (IOException e) {
								logger.error(e.getMessage());
								setThrowable(e);
							}
														
						}						
					
						//save to excel
						try {
							pp.saveTo(sourceFile, new HashSet<ProjectElementId>(Arrays.asList(elements)));
						} catch(ProjectSaveException pse) {
							// update display
							if( pse.getProjectSaveErrorMap() != null ) {
								if( ! pse.isFatalError() ) {
									setError(true);
									setProjectDataErrors(pse.getProjectSaveErrorMap());
									setThrowable(pse);
									final String errMsg = "Project '" + projectName + "' was exported to '" + sourceFile + "', but with warnings.";
									ConsoleWriter.writeMessage(errMsg);
//									Map<ProjectElementId, List<ProjectDataError>> errorMap = pse.getProjectSaveErrorMap();
//									final String dispMsg = errMsg + "\n" + errorMap.values().iterator().next().toString();
//									logger.warn(errMsg);
//									ConsoleWriter.writeMessage(dispMsg);
								}
								else {
									final String errMsg = "Error Saving Pace Project: " + projectName;
									logger.error(errMsg);
									ConsoleWriter.writeMessage(errMsg);
									Map<ProjectElementId, List<ProjectDataError>> errorMap = pse.getProjectSaveErrorMap();
									final String dispMsg = errMsg + "\n" + errorMap.values().iterator().next().toString();
									Display.getDefault().asyncExec(new Runnable() {
		
										public void run() {
		
											GUIUtil
													.openMessageWindow("Project Exporting Error",
															dispMsg, MessageDialog.ERROR);
		
										}
		
									});
								}
							}
							else {
								final String errMsg = pse.getMessage();
								logger.error(errMsg);
								ConsoleWriter.writeMessage(errMsg);
								Display.getDefault().asyncExec(new Runnable() {
	
									public void run() {
	
										GUIUtil
												.openMessageWindow("Project Exporting Error",
														errMsg, MessageDialog.ERROR);
	
									}
	
								});
							}
							return;
						}
						
						//if macro file was copied from blank
						if ( isNonExistExcelMacroFile ) {
							
							//if file was successfully copied, remove PaceBlank sheet
							if ( excelFile.exists()) {
								
								String sheetNameToDelete = "PaceBlank";
								Workbook workbook = null;
								try {
									
									//get workbook
									workbook = PafExcelUtil.readWorkbook(excelFile.toString());
									
									//delete blank sheet
									PafExcelUtil.deleteSheet(workbook, sheetNameToDelete);
									
									//save workbook
									PafExcelUtil.writeWorkbook(workbook, excelFile.toString());
									
								} catch (PafException e) {
									logger.warn("Couldn't delete sheet: " + sheetNameToDelete);
									logger.warn(e.getMessage());
//									return;
								}								
								
							}
							
						}
						
					}
					
					break;
				default:
											
					pp.saveTo(sourceFile, new HashSet<ProjectElementId>(Arrays.asList(elements)));
					
					break;
			}
			
			
			ConsoleWriter.writeMessage("Project '" + projectName
					+ "' was successfully exported to: '"
					+ sourceFile + "'");
			
		} catch (ProjectSaveException e) {
			e.printStackTrace();
			if( e.getProjectSaveErrorMap() != null ) {
				if( ! e.isFatalError() ) {
					setError(true);
					setProjectDataErrors(e.getProjectSaveErrorMap());
					setThrowable(e);
				}
				else {
					final String errMsg = "Error Saving Pace Project: " + projectName;
					logger.error(errMsg);
					ConsoleWriter.writeMessage(errMsg);
					Map<ProjectElementId, List<ProjectDataError>> errorMap = e.getProjectSaveErrorMap();
					final String dispMsg = errMsg + "\n" + errorMap.values().iterator().next().toString();
					Display.getDefault().asyncExec(new Runnable() {

						public void run() {

							GUIUtil
									.openMessageWindow("Project Exporting Error",
											dispMsg, MessageDialog.ERROR);

						}

					});
				}
			}
			else {
				final String errorMessage = e.getMessage();
				final String errorHeading = "Error Saving Pace Project: " + errorMessage;
				final String fullErrorMessage = errorHeading + ". " + errorMessage;
				logger.error(fullErrorMessage);
				ConsoleWriter.writeMessage(fullErrorMessage);
						
				// update display
				Display.getDefault().asyncExec(new Runnable() {
	
					public void run() {
	
						GUIUtil
								.openMessageWindow(errorHeading,
										errorMessage, MessageDialog.ERROR);
	
					}
	
				});
			}
			
			//setProjectDataErrors(pp.getProjectCreationErrorMap());
			return;
			
		} 							

	}
	

}

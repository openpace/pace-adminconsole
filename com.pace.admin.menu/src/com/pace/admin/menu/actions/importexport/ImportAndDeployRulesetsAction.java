/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;

import com.pace.admin.menu.dialogs.ImportRulesetDialog;
import com.pace.admin.menu.util.ViewerUtil;

import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;

public class ImportAndDeployRulesetsAction extends Action{
	protected ProjectElementId[] elements;

	protected IWorkbenchWindow window;
	
	private boolean error;
	
	private Throwable throwable;
	
	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrors;
		
	//protected ImportRuleSetWizard importRulesetWizard;
	private Map<String, IProject> currentProjects = null;
	
	public ImportAndDeployRulesetsAction(String name, IWorkbenchWindow window, ProjectElementId[] elements){
		super(name);
		
		this.window = window;
		this.elements = elements;
		
	
		
	}
	
	public void run(){
		
		// get the current projects.
		currentProjects = ViewerUtil.getCurrentProjects();
		//create instance of dialog
		
		final ImportRulesetDialog dialog = new ImportRulesetDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow(),"ImportProject","Import Rule Set and Deploy","Import Desc" , currentProjects.keySet()
				.toArray(new String[0]), 
				
				"Excel workbook:",
				"Select Excel file:",
				Constants.EXCEL_FILE_EXTENSION_LIST);
				
				
		//open dialog
		int rc = dialog.open();
		
	}
	
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public Map<ProjectElementId, List<ProjectDataError>> getProjectDataErrors() {
		return projectDataErrors;
	}

	public void setProjectDataErrors(
			Map<ProjectElementId, List<ProjectDataError>> projectDataErrors) {
		this.projectDataErrors = projectDataErrors;
	}
	
	
}

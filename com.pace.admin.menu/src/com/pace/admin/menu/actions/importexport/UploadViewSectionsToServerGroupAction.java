/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.importexport;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.widgets.StringSelectionDialog;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;

public class UploadViewSectionsToServerGroupAction extends AbstractUploadViewSectionAction {

	public final static String ID = "com.pace.admin.menu.actions.projects.UploadViewSectionsToServerGroupAction";
	
	
	public UploadViewSectionsToServerGroupAction(IWorkbenchWindow window,
			String serverGroup, List<PafServer> servers) {
		super(ID, window, serverGroup, servers);
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		if ( servers != null && servers.size() > 0 ) {
			
			String[] selectedViewSectionNames = getSelectedViewSectionItems();
			
			//get iproject from root workspace
			final IProject iProject = getIProject();
			

			//get model manager
			ViewSectionModelManager modelManager = new ViewSectionModelManager(iProject);
	
			//get key neames
			String[] viewSectionNames = modelManager.getKeys();
			
			if ( selectedViewSectionNames == null ) {
				
				 selectedViewSectionNames  = viewSectionNames;
				 
			}
			
			Map<String, Boolean> checkboxMap = new TreeMap<String, Boolean>();
			
			checkboxMap.put(Constants.START_OR_RESTART_APPLICATION, Constants.START_OR_RESTART_APPLICATION_DEFAULT_VALUE);
			checkboxMap.put(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION, Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION_DEFAULT_VALUE);
			
			//create instance of dialog
			final StringSelectionDialog dialog = new StringSelectionDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
					"Upload View Sections", "Upload View Sections", 
					"Select the View Sections to upload to server group '" + serverGroupName + "'.", viewSectionNames, selectedViewSectionNames, checkboxMap);
			
			//open dialog
			int rc = dialog.open();
			
			//if user clicked ok
			if ( rc == 0 ) {
				
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
					
					public void run() {
											
						//Check if all servers are running, if not prompt user.
						if(!ImportExportUtil.okToDeployNonRunningServers(allServersRunning(), servers)){
							return;
						}
						
						//check for non-matching projects.
						if(!ImportExportUtil.okToDeployToNonMatchingServerProjects(PafProjectUtil.getApplicationName(getIProject()), servers)){
							return;
						}
						
						//get the selected items
						String[] selectedItems = dialog.getSelectedItems();
							
						//if itmes have been selected
						if ( selectedItems != null && selectedItems.length > 0 ) {
						
							
							XMLPaceProject xpp = getPaceProject(selectedItems);
							boolean refreshConfiguration = dialog.getOptionalCheckOptionsMap().get(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
							boolean refreshCube = dialog.getOptionalCheckOptionsMap().get(Constants.START_OR_RESTART_APPLICATION);
							
							Map<String, Boolean> results = WebServicesUtil.uploadProjectToServerGroup(xpp, servers, refreshConfiguration, refreshCube, serverGroupName, getProjectElementIdSet());
							
							boolean uploadSuccessful = ImportExportUtil.getResults(results.values());
							
							
							String outMessage = null;
							String messagePrefix = "There were one or more problems uploading the selected view sections to server group '" 
									+ serverGroupName 
									+ "'.\r\n\r\n";
							String messageEnding = "\r\nPlease check the logs.";
							
							if(uploadSuccessful){
								outMessage = "The selected view sections were successfully uploaded.";
							} else {
								outMessage = messagePrefix +
										ImportExportUtil.parseUploadResults(results) + 
											messageEnding;
							}
						
							ImportExportUtil.showProcessCompleteDialog(uploadSuccessful, outMessage);
							
							
							ConsoleWriter.writeMessage(outMessage);

						}
				}});
			}
		}
	}
}
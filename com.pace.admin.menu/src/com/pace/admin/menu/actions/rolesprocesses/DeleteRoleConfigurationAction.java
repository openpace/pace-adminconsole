/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.RoleConfigurationEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.RoleConfigurationNode;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;

/**
* Deletes a role configuration from the PafPlannerConfigModelManager.
* @author kmoos
* @version	x.xx
*/
public class DeleteRoleConfigurationAction extends Action {
	private final IWorkbenchWindow window;
	private String roleConfigurationName;
	private TreeViewer viewer;
	private IProject project;

	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public DeleteRoleConfigurationAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
        
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.DeleteRoleConfigurationAction");
    }

	/**
	 * Runs the DeleteRoleConfigurationAction.
	 */
	public void run(){
		//if user wants to delete project
		try {
			if ( roleConfigurationName != null ) {
		    	
		    	// TTN-2387 - check for open conflicting editors
		    	if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), RoleConfigurationEditor.class)) {
		    		MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't delete object while dependent editor(s) is open");
		    		return;
		    	}
		    	
		    	
		    	if (GUIUtil.askUserAQuestion(
		    			this.getText(),
						"Are you sure you want to delete the Role Configuration: '" + roleConfigurationName + "'?")) {
		    		
		    		//remove the planner configuration from the pafplannerconfigmodelmanager.
		    		PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
		    		pafPlannerConfigModelManager.remove(roleConfigurationName);
		    		pafPlannerConfigModelManager.save();
		    	}
		    }
		    
		    ISelection selection = viewer.getSelection();
	    	
		    //rebuilds the Roles & Processes tree.
	    	if ( selection instanceof IStructuredSelection) {
	    		Object node = ((IStructuredSelection) selection).getFirstElement();
	    		
	    		if ( node instanceof RoleConfigurationNode ) {
	    			
	    			RoleConfigurationNode roleConfigurationNode = (RoleConfigurationNode) node;
	    			RoleConfigurationsNode parentNode = (RoleConfigurationsNode) roleConfigurationNode.getParent();			
	    			parentNode.createChildren(null);
	    			viewer.refresh();
	    		}
	    	}
			
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Gets the name of the Role Configuration.
	 * @return The name of the Role Configuration.
	 */
	public String getRoleConfigurationName() {
		return roleConfigurationName;
	}

	/**
	 * Sets the name of the Role Configuration.
	 * @param roleConfigurationName The name of the Role Configuration.
	 */
	public void setRoleConfigurationName(String roleConfigurationName) {
		this.roleConfigurationName = roleConfigurationName;
	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

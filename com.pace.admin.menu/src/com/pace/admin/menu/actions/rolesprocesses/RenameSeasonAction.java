/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.*;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.dialogs.RenameSeasonDialog;
import com.pace.admin.menu.editors.SeasonEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.SeasonNode;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * @author Jordan
 *
 */
public class RenameSeasonAction extends Action {
	private static Logger logger = Logger.getLogger(RenamePlannerRoleAction.class);
	private final static int OK_ID = 0;
	private final IWorkbenchWindow window;
	private String seasonName;
	private TreeViewer viewer;
	private IProject project;
	private SeasonModelManager seasonsModelManager;
	
	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public RenameSeasonAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.RenameSeasonProcessAction");
    }
	
	/**
	 * Runs the RenameSeasonAction.
	 */
	public void run(){
		
		
	    if ( seasonName != null ) {
	    	// get user selection and rebuild the Roles & Processes tree.
		    ISelection selection = viewer.getSelection();
	    	
		    
	    	if ( selection instanceof IStructuredSelection) {
	    		Object node = ((IStructuredSelection) selection).getFirstElement();
	    		
	    		
	    		if ( node instanceof SeasonNode ) {
	    			
	    			//if user wants to rename season
	    			try {
	    				// TTN-2387 - check for open conflicting editors
	    				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), SeasonEditor.class)) {
	    					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_RENAME_MESSAGE);
	    						return;
	    				}
	    				
	    				SeasonNode seasonNode = (SeasonNode) node;
	    				RolesProcessesNode rolesProcessesNode = (RolesProcessesNode) seasonNode.getParent().getParent();
	    				
	    				IProject project = ViewerUtil.getProjectNode(rolesProcessesNode);
	    				String oldSeasonProcessName = seasonNode.getName();
	    				seasonsModelManager = new SeasonModelManager(project);
	    				
	    				final RenameSeasonDialog dialog = new RenameSeasonDialog(
	    					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
	    					oldSeasonProcessName, seasonsModelManager, 
	    					"Old Season Process Name:", "New Season Process Name:", "Rename Season Processes");
	    				
	    				int rc = dialog.open();
	    				
	    				//if user clicks ok
	    				if ( rc == OK_ID ) {
	    					
	    					//get new season process name
	    					String newSeasonProcessName = dialog.getNewName();
	    					
	    					logger.info("Old Season Process Name: " + oldSeasonProcessName + "; New Season Process Name: " + newSeasonProcessName);
	    				
	    					//TTN-2466 rename season process name
	    					seasonsModelManager.load();
	    					seasonsModelManager.renameSeason(oldSeasonProcessName, newSeasonProcessName);
	    					
	    					// TTN-2466 update season process in planner role model 
	    					PlannerRoleModelManager plannerRoleModelManager = new PlannerRoleModelManager(project);
	    					plannerRoleModelManager.load();
	    					plannerRoleModelManager.renameSeasonId(oldSeasonProcessName, newSeasonProcessName);
	    					
	    					// rebuilds the Roles & Processes tree.
	    					for(Object child : rolesProcessesNode.getChildren()){
	    						if(child instanceof RoleConfigurationsNode){
	    							RoleConfigurationsNode roleConfigsNode = 
	    								(RoleConfigurationsNode) child;
	    							roleConfigsNode.createChildren(null);
	    							viewer.refresh(roleConfigsNode);
	    						}
	    						if(child instanceof SeasonsNode){
	    							SeasonsNode seasonsNode = 
	    								(SeasonsNode) child;
	    							seasonsNode.createChildren(null);
	    							viewer.refresh(seasonsNode);
	    						}
	    						if(child instanceof PlannerRolesNode){
	    							PlannerRolesNode rolesNode = 
	    								(PlannerRolesNode) child;
	    							rolesNode.createChildren(null);
	    							viewer.refresh(rolesNode);
	    						}
	    						if(child instanceof PlanCyclesNode){
	    							PlanCyclesNode plansNode = 
	    								(PlanCyclesNode) child;
	    							plansNode.createChildren(null);
	    							viewer.refresh(plansNode);
	    						}
	    					}
	    				}
	    			} catch (PartInitException e1) {
	    				// TODO Auto-generated catch block
	    				e1.printStackTrace();
	    			}
				}
			}
	    }
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Gets the season name.
	 * @return The season name.
	 */
	public String getSeasonName() {
		return seasonName;
	}

	/**
	 * Sets the season name.
	 * @param seasonName The name of the season.
	 */
	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

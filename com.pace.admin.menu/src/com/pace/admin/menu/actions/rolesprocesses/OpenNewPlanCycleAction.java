/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.VersionDefModelManager;
import com.pace.admin.menu.editors.PlanCycleEditor;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.input.PlanCycleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.base.app.VersionType;

/**
* Action, opens a new, blank PlanCycleEditor.
* @author kmoos
* @version	x.xx
*/
public class OpenNewPlanCycleAction extends Action implements ISelectionListener,
	IWorkbenchAction {

	private static Logger logger = Logger.getLogger(OpenNewPlanCycleAction.class);
	private final IWorkbenchWindow window;
	private IStructuredSelection selection = null;
	private IProject project;
	private TreeViewer viewer;
	private PlanCyclesNode planCyclesNode;
	
	/**
	 * Constructor.
	 * @param text The text of node.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public OpenNewPlanCycleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;

        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.OpenNewPlanCycleAction");
        window.getSelectionService().addSelectionListener(this);	        
    }
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Runs the OpenNewPlanCycleAction.
	 */
    @SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
    	//get the first selection.
		Object item = selection.getFirstElement();
		//convert the item to a plancyclesnode.
		planCyclesNode = (PlanCyclesNode) item; 
		//look for any open editors and close them.
    	EditorUtils.closeDuplicateEditorType(page, PlanCycleEditor.class);		
		//create a PlanCycleModelManager.
		PlanCycleModelManager model = new PlanCycleModelManager(project);
		//Create a version def model manager.
		VersionDefModelManager versionDefModel = new VersionDefModelManager(project, true);
		//using the two model manager, create the input for the PlanCycleEditor.
		PlanCycleInput input = new PlanCycleInput("", model, versionDefModel, project, true);
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlanCycleEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
					return;
			}
			
			//Open the new editor.
			PlanCycleEditor planCycleEditor = (PlanCycleEditor) 
				page.openEditor(input, PlanCycleEditor.ID);
			//set the viewer.
			planCycleEditor.setViewer(viewer);
			//set the parent plan cycles node (for updating purposes)
			planCycleEditor.setPlanCyclesNode(planCyclesNode);
			
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}    
    }

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}
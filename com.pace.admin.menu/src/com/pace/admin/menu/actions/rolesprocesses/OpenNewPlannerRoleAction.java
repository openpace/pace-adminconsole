/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.input.PlannerRoleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlannerRolesNode;

/**
* Action, opens a new, blank PlannerRoleEditor.
* @author kmoos
* @version x.xx
*/
public class OpenNewPlannerRoleAction extends Action implements ISelectionListener,
	IWorkbenchAction {
	
	private static Logger logger = Logger.getLogger(OpenNewPlannerRoleAction.class);
	private final IWorkbenchWindow window;
	private IStructuredSelection selection = null;
	private IProject project;
	private TreeViewer viewer;
	private PlannerRolesNode plannerRolesNode;
	
	/**
	 * Constructor.
	 * @param text The text of node.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public OpenNewPlannerRoleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.OpenNewPlannerRoleAction");
        window.getSelectionService().addSelectionListener(this);	
    }
	
	/**
	 * Implementation of ISelectionListener.
	 * @param part IWorkbenchPart object.
	 * @param selection ISelection selection.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		}
	}
	
	/**
	 * Occurs when the action is disposed.
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}
	
	/**
	 * Runs the OpenNewPlannerRoleAction.
	 */
    @SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
    	//get the first selection.
    	Object item = selection.getFirstElement();
    	//convert the item to a PlannerRolesNode.
		plannerRolesNode = (PlannerRolesNode) item;
    	//look for any open editors and close them.
    	EditorUtils.closeDuplicateEditorType(page, PlannerRoleEditor.class);
		//Create a PlannerRoleModelManager.
		PlannerRoleModelManager model = new PlannerRoleModelManager(project);
		//Create a SeasonModelManager.
		SeasonModelManager seasonsModel = new SeasonModelManager(project);
		//using the two model manager, create the input for the PlannerRoleEditor.
		PlannerRoleInput input = new PlannerRoleInput("", model, seasonsModel, project, true);
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlannerRoleEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
					return;
			}
			
			//Open the new editor.
			PlannerRoleEditor plannerRoleEditor = (PlannerRoleEditor) 
				page.openEditor(input, PlannerRoleEditor.ID);
			//set the viewer.
			plannerRoleEditor.setViewer(viewer);
			//set the parent plannerRolesNode (for updating purposes)
			plannerRoleEditor.setPlannerRolesNode(plannerRolesNode);
		} catch (PartInitException e) {
			logger.error(e.getMessage());
		}    
    }
    
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

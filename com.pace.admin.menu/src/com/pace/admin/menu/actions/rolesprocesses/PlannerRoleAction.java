/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.input.PlannerRoleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlannerRoleNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;

/**
* Action, opens an existing PlannerRole in a PlannerRoleEditor.
* @author kmoos
* @version x.xx
*/
public class PlannerRoleAction extends Action {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.actions.PlannerRoleAction";
	private static Logger logger = Logger.getLogger(PlannerRoleAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public PlannerRoleAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
        this.setToolTipText(ID);
        this.setId(ID);
	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//get the first selection.
		ISelection selection = viewer.getSelection();
       	if ( selection instanceof IStructuredSelection) {
			Object item = ((IStructuredSelection)selection).getFirstElement();
			//convert the node to a PlannerRolesNode.
			PlannerRoleNode node = (PlannerRoleNode) item; 
			//Create a PlannerRolesNode (parent).
			PlannerRolesNode plannerRolesNode = (PlannerRolesNode) node.getParent(); 
			//look for any open editors and close them.
			EditorUtils.closeDuplicateEditorType(page, PlannerRoleEditor.class);
			//Create a PlannerRoleModelManager.
			PlannerRoleModelManager model = new PlannerRoleModelManager(project);
			//Create a SeasonModelManager.
			SeasonModelManager seasonsModel = new SeasonModelManager(project);
			//using the two model manager, create the input for the PlannerRoleEditor.
			PlannerRoleInput input = new PlannerRoleInput(node.getName(), model, seasonsModel, project, false);
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlannerRoleEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
						return;
				}
				
				PlannerRoleEditor plannerRoleEditor = (PlannerRoleEditor) 
					page.openEditor(input, PlannerRoleEditor.ID);
				//set the viewer.
				plannerRoleEditor.setViewer(viewer);
				//set the parent plannerRolesNode (for updating purposes)
				plannerRoleEditor.setPlannerRolesNode(plannerRolesNode);
			} catch (PartInitException e) {
				// handle error
				logger.error(e.getMessage());
			}
       	}
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

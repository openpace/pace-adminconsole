/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.editors.SeasonEditor;
import com.pace.admin.menu.editors.input.SeasonInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.SeasonNode;
import com.pace.admin.menu.nodes.SeasonsNode;

/**
* Action, opens an existing Season in a SeasonEditor.
* @author kmoos
* @version x.xx
*/
public class SeasonAction extends Action {

	/**
	 * Unique id for the action. 
	 */
	public final static String ID = "com.pace.admin.actions.SeasonAction";
	private static Logger logger = Logger.getLogger(PlanCycleAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public SeasonAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
	}

	/**
	 * Runs the SeasonAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
    	
    	ISelection selection = viewer.getSelection();
       	if ( selection instanceof IStructuredSelection) {
			//get the first selection.
			Object item = ((IStructuredSelection)selection).getFirstElement();
			//convert the item to a SeasonNode.
			SeasonNode node = (SeasonNode) item; 
			//Create a SeasonsNode (parent node)
			SeasonsNode seasonsNode = (SeasonsNode) node.getParent();
			
	//		look for any open editors and close them.
			EditorUtils.closeDuplicateEditorType(page, SeasonEditor.class);
			
			
			//create the season model manager. 
			SeasonModelManager model = new SeasonModelManager(project);
			//using season model manager, create the input for the SeasonEditor.
			SeasonInput input = new SeasonInput(node.getName(), model, project, false);
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), SeasonEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
						return;
				}
				
				
				//Open the new editor.
				SeasonEditor seasonEditor = (SeasonEditor) page.openEditor(input, SeasonEditor.ID);
				//set the viewer.
				seasonEditor.setViewer(viewer);
				//set the parent SeasonsNode (for updating purposes)
				seasonEditor.setSeasonsNode(seasonsNode);
				} catch (PartInitException e) {
				logger.error(e.getMessage());
			}
       	}
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.PlanCycleEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCycleNode;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.RoleConfigurationRemoval;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.SeasonFromRoleRemoval;
import com.pace.admin.menu.nodes.SeasonRemoval;
import com.pace.admin.menu.nodes.SeasonsNode;

/**
 * Deletes a plan cycle from the PlanCycleModelManager, SeasonModelManager,
 * and PafPlannerConfigModelManager.
 * @author kmoos
 * @version	x.xx
 */
public class DeletePlanCycleAction extends Action {
	private final IWorkbenchWindow window;
	private String planCycleName;
	private TreeViewer viewer;
	private IProject project;
	
	
	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public DeletePlanCycleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
        
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.DeletePlanCycleAction");
    }

	/**
	 * Runs the DeletePlanCycleAction.
	 */
	public void run(){
		
		try {
			//if user wants to delete the plan cycle
		    if ( planCycleName != null ) {
		    	
		    	// TTN-2387 - check for open conflicting editors
		    	if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlanCycleEditor.class)) {
		    		MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't delete object while dependent editor(s) is open");
		    		return;
		    	}
		    		
	    		//remove the item from the plan cycle model manager.
	    		PlanCycleModelManager planCycleModelManager = new PlanCycleModelManager(project);
	    		
	    		PlanCycleNode node = null;
	    		ISelection selection = viewer.getSelection();
	        	if ( selection instanceof IStructuredSelection) {
	        		Object objectNode = ((IStructuredSelection) selection).getFirstElement();
	        		
	        		if ( objectNode instanceof PlanCycleNode ) {
	        			node = (PlanCycleNode) objectNode;
	        		}
	        	}
	    		
	        	List<IRemoval> removals = node.getDependants(project);
	        	
	        	String removalText = "Are you sure you want to delete the Plan Cycle: '" + planCycleName + "'?";
	        	
	        	if(!removals.isEmpty()){
	        		removalText += "\n\nThe consequences of this action are the following:\n\n";
	        	}
	        	
	        	for (IRemoval removal : removals) {
					removalText += removal.removalDescription() + '\n'; 
				}
	    		
	        	if (GUIUtil.askUserAQuestion(this.getText(), removalText)){
	        		
		    		PlannerRoleModelManager plannerRoleModelManager = null;
		    		PafPlannerConfigModelManager pafPlannerConfigModelManager = null;
		    		SeasonModelManager seasonModelManager = null;
		    		
		        	for (IRemoval removal : removals) {
						removal.remove();
						if(removal instanceof SeasonRemoval){
							seasonModelManager = (SeasonModelManager) removal.getModelManager();
						}
						if(removal instanceof RoleConfigurationRemoval){
							pafPlannerConfigModelManager = (PafPlannerConfigModelManager) removal.getModelManager();
						}
						if(removal instanceof SeasonFromRoleRemoval){
							plannerRoleModelManager = (PlannerRoleModelManager) removal.getModelManager();
						}
							
					}
		    		planCycleModelManager.remove(planCycleName);
		    		planCycleModelManager.save();
		    		
		    		if(plannerRoleModelManager != null){
		    			plannerRoleModelManager.save();
		    		}
		    		if(pafPlannerConfigModelManager != null){
		    			pafPlannerConfigModelManager.save();
		    		}
		    		if(seasonModelManager != null){
		    			seasonModelManager.save();
		    		}
	        	}
	        	else {
	        		return;
	        	}
		    }
		    
		  //rebuilds the Roles & Processes tree.
		    ISelection selection = viewer.getSelection();
	    	
	    	if ( selection instanceof IStructuredSelection) {
	    		Object node = ((IStructuredSelection) selection).getFirstElement();
	    		
	    		if ( node instanceof PlanCycleNode ) {
	    			
	    			PlanCycleNode planCycleNode = (PlanCycleNode) node;
	    			
	    			RolesProcessesNode rolesProcesses = (RolesProcessesNode) planCycleNode.getParent().getParent();
	    			
	    			for(Object child : rolesProcesses.getChildren()){
	    				if(child instanceof RoleConfigurationsNode){
							RoleConfigurationsNode roleConfigsNode = 
								(RoleConfigurationsNode) child;
							roleConfigsNode.createChildren(null);
							viewer.refresh(roleConfigsNode);
						}
	    				if(child instanceof SeasonsNode){
	    					SeasonsNode seasonsNode = 
								(SeasonsNode) child;
	    					seasonsNode.createChildren(null);
							viewer.refresh(seasonsNode);
						}
	    				if(child instanceof PlannerRolesNode){
	    					PlannerRolesNode rolesNode = 
								(PlannerRolesNode) child;
	    					rolesNode.createChildren(null);
							viewer.refresh(rolesNode);
						}
	    				if(child instanceof PlanCyclesNode){
	    					PlanCyclesNode plansNode = 
								(PlanCyclesNode) child;
	    					plansNode.createChildren(null);
							viewer.refresh(plansNode);
						}
	    			}
	    		}
	    	}
			
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Gets the name of the plan cycle.
	 * @return
	 */
	public String getPlanCycleName() {
		return planCycleName;
	}

	/**
	 * Sets the name of the plan cycle.
	 * @param planCycleName Name of the plan cycle.
	 */
	public void setPlanCycleName(String planCycleName) {
		this.planCycleName = planCycleName;
	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return Returns a string representation of the object.
	 */
	public String toString () {
		return super.toString();
	}
}

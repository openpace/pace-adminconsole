/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRoleNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.RoleConfigurationRemoval;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RoleFromSecurityRemoval;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;

/**
* Deletes a planner role from the PlannerRoleModelManager.
* @author kmoos
* @version	x.xx
*/
public class DeletePlannerRoleAction extends Action {
	private final IWorkbenchWindow window;
	private String roleName;
	private TreeViewer viewer;
	private IProject project;

	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public DeletePlannerRoleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
        
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.DeletePlannerRoleAction");
    }

	/**
	 * Runs the DeletePlannerRoleAction.
	 */
	public void run(){
		//if user wants to delete project
		try {
			
			if ( roleName != null ) {
		    	/*if (GUIUtil.askUserAQuestion(
		    			this.getText(),
						"Are you sure you want to delete the Role: '" + roleName + "'?" + 
						"\nDoing this will delete any " + Constants.MENU_ROLE_CONFIGURATIONS + 
						" containing this role.")) {*/
		    	
		    	// TTN-2387 - check for open conflicting editors
		    	if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlannerRoleEditor.class)) {
		    		MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't delete object while dependent editor(s) is open");
		    		return;
		    	}
		    	
	    		//removes the planner role from the planner role model manager.
	    		PlannerRoleModelManager plannerRoleModel = new PlannerRoleModelManager(project);
	    		
	    		PlannerRoleNode node = null;
	    		ISelection selection = viewer.getSelection();
	        	if ( selection instanceof IStructuredSelection) {
	        		Object objectNode = ((IStructuredSelection) selection).getFirstElement();
	        		
	        		if ( objectNode instanceof PlannerRoleNode ) {
	        			node = (PlannerRoleNode) objectNode;
	        		}
	        	}
	    		
	        	List<IRemoval> removals = node.getDependants(project);
	        	if(removals == null) {
	        		GUIUtil.openErrorDialog("Error", "Cannot delete role.  Please check the error log for more details.");
	        		return;
	        	}
	        	
	        	
	        	String removalText = "Are you sure you want to delete the Role: '" + roleName + "'?";
	        	
	        	if(!removals.isEmpty()){
	        		removalText += "\n\nThe consequences of this action are the following:\n\n";
	        	}
	        	
	        	for (IRemoval removal : removals) {
					removalText += removal.removalDescription() + '\n'; 
				}
	    		
	        	if (GUIUtil.askUserAQuestion(this.getText(), removalText)){
	        		
		    		PafPlannerConfigModelManager pafPlannerConfigModelManager = null;
		    		UserSecurityModelManager userSecurityModelManager = null;
		    		
		        	for (IRemoval removal : removals) {
						removal.remove();
						if(removal instanceof RoleFromSecurityRemoval){
							userSecurityModelManager = (UserSecurityModelManager) removal.getModelManager();
						}
						if(removal instanceof RoleConfigurationRemoval){
							pafPlannerConfigModelManager = (PafPlannerConfigModelManager) removal.getModelManager();
						}
					}
		        	
		    		if(userSecurityModelManager != null){
		    			userSecurityModelManager.save();
		    		}
		    		if(pafPlannerConfigModelManager != null){
		    			pafPlannerConfigModelManager.save();
		    		}

		        	plannerRoleModel.remove(roleName);
		        	plannerRoleModel.save();

	        	}
	        	else {
	        		return;
	        	}
		    }
			
			//rebuilds the Roles & Processes tree.
		    ISelection selection = viewer.getSelection();
	    	
	    	if ( selection instanceof IStructuredSelection) {
	    		Object node = ((IStructuredSelection) selection).getFirstElement();
	    		
	    		if ( node instanceof PlannerRoleNode ) {
	    			
	    			PlannerRoleNode roleNode = (PlannerRoleNode) node;
	    			
	    			RolesProcessesNode rolesProcesses = (RolesProcessesNode) roleNode.getParent().getParent();
	    			
	    			for(Object child : rolesProcesses.getChildren()){
	    				if(child instanceof RoleConfigurationsNode){
							RoleConfigurationsNode roleConfigsNode = 
								(RoleConfigurationsNode) child;
							roleConfigsNode.createChildren(null);
							viewer.refresh(roleConfigsNode);
						}
	    				if(child instanceof SeasonsNode){
	    					SeasonsNode seasonsNode = 
								(SeasonsNode) child;
	    					seasonsNode.createChildren(null);
							viewer.refresh(seasonsNode);
						}
	    				if(child instanceof PlannerRolesNode){
	    					PlannerRolesNode rolesNode = 
								(PlannerRolesNode) child;
	    					rolesNode.createChildren(null);
							viewer.refresh(rolesNode);
						}
	    				if(child instanceof PlanCyclesNode){
	    					PlanCyclesNode plansNode = 
								(PlanCyclesNode) child;
	    					plansNode.createChildren(null);
							viewer.refresh(plansNode);
						}
	    			}
	    			
	    			for(Object child : rolesProcesses.getParent().getChildren()){
	    				
	    				if(child instanceof UsersSecurityNode){
	    					UsersSecurityNode securitiesNode = 
								(UsersSecurityNode) child;
	    					securitiesNode.createChildren(null);
							viewer.refresh(securitiesNode);
						}
	    				
	    			}
	    		}
	    	}
			
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Gets the planner role name.
	 * @return The planner role name.
	 */
	public String getPlannerRoleName() {
		return roleName;
	}

	/**
	 * Sets the name of the Planner Role.
	 * @param roleName THe name of the planner role.
	 */
	public void setPlannerRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

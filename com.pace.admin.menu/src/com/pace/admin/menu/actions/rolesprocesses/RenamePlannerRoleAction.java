/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.actions.projects.RefreshProjectFolderAction;
import com.pace.admin.menu.actions.projects.RefreshProjectsAction;
import com.pace.admin.menu.commands.printstyles.RenamePrintStyleCommand;
import com.pace.admin.menu.dialogs.RenamePlannerRoleDialog;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCycleNode;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRoleNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.RoleConfigurationRemoval;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RoleFromSecurityRemoval;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.nodes.SecurityNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.nodes.UserSecurityDomainNode;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewSectionsNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
* Renames a planner role from the PlannerRoleModelManager.
* @author Jordan
* @version	x.xx
*/
public class RenamePlannerRoleAction extends Action {
	private static Logger logger = Logger.getLogger(RenamePlannerRoleAction.class);
	private final static int OK_ID = 0;
	private final IWorkbenchWindow window;
	private String roleName;
	private TreeViewer viewer;
	private IProject project;
	private PlannerRoleModelManager modelManager;

	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public RenamePlannerRoleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
        super(text);
        this.window = window;
        this.viewer = viewer;
        
        // The id is used to refer to the action in a menu or toolbar
        this.setId("com.pace.admin.menu.actions.RenamePlannerRoleAction");
    }

	/**
	 * Runs the RenamePlannerRoleAction.
	 */
	public void run(){
		
	    // get user selection and rebuild the Roles & Processes tree.
	    ISelection selection = viewer.getSelection();
    	
    	if ( selection instanceof IStructuredSelection) {
    		Object node = ((IStructuredSelection) selection).getFirstElement();
    		
    		if ( node instanceof PlannerRoleNode ) {
    			
    			try {
    				// TTN-2387 - check for open conflicting editors
    				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlannerRoleEditor.class)) {
    					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_RENAME_MESSAGE);
    						return;
    				}
    				
    				PlannerRoleNode plannerRoleNode = (PlannerRoleNode) ((IStructuredSelection) selection).getFirstElement(); 
    				IProject project = ViewerUtil.getProjectNode(plannerRoleNode);
    				PlannerRoleModelManager plannerRoleModel = new PlannerRoleModelManager(project);
    				
    				// Get all planner role names 
    				ArrayList<String> plannerRoleNames = new ArrayList<String>(Arrays.asList(plannerRoleModel.getKeys())); 
    				
    				if (plannerRoleNames.contains(plannerRoleNode.getName())) {
    					
    					String oldPlannerRoleName = plannerRoleNode.getName();
    					
    					final RenamePlannerRoleDialog dialog = new RenamePlannerRoleDialog(
    	    					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
    	    					oldPlannerRoleName, modelManager, 
    	    					"Old Planner Role Name:", "New Planner Role Name:", "Rename Planner Role");
    					
    					int rc = dialog.open();
    					
    					//if user clicks ok
        				if ( rc == OK_ID ) {
        					
        					//get new planner role name
        					String newPlannerRoleName = dialog.getNewName();
        					
        					logger.info("Old Planner Role Name: " + oldPlannerRoleName + "; New Planner Role Name: " + newPlannerRoleName);
        				
        					// load planner role xml
        					plannerRoleModel.load();       					
        					plannerRoleModel.renamePlannerRole(oldPlannerRoleName, newPlannerRoleName);
        					
        					// rename planner role references in paf_planner_conf.xml  
        					PafPlannerConfigModelManager pafPlannerConfigModel = new PafPlannerConfigModelManager(project);       					
        					pafPlannerConfigModel.load(); // load xml 
        					pafPlannerConfigModel.renameRoleName(oldPlannerRoleName, newPlannerRoleName);
        					
        					// rename planner role references in paf_security.xml
        					UserSecurityModelManager userSecurityModelManager = new UserSecurityModelManager(project);
        					userSecurityModelManager.load();
        					userSecurityModelManager.renamePlannerRole(oldPlannerRoleName, newPlannerRoleName);
        					
        					
        					//create a project node
    						ProjectNode projectNode = ViewerUtil.getProjectNode(plannerRoleNode.getProject().getName());

    						if(projectNode != null){
    							for (Object obj : projectNode.getChildren()) {
    								if ( obj instanceof SecurityNode) {
    									//get the high level node.
    									SecurityNode securityNode = (SecurityNode)obj;
    									
    									//re-create children
    									securityNode.createChildren(null);
    									
    									//update the menu plug in.
    									MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
    								}				
    								
    								if ( obj instanceof RolesProcessesNode) {
    									
    									RolesProcessesNode rolesProcessesNode = (RolesProcessesNode)obj;
    									
    									for (Object object : rolesProcessesNode.getChildren()) {
    										if (object instanceof PlannerRolesNode) {
    											PlannerRolesNode plannerRolesNode = (PlannerRolesNode)object;
    											
    											plannerRolesNode.createChildren(null);
    											
    											MenuPlugin.getDefault().getMenuView().getViewer().refresh(object);
    										}
    									}
    								}
    							}
    						}

        					// RolesProcessesNode rolesProcesses = (RolesProcessesNode) plannerRoleNode.getParent().getParent();
        					
        					// RefreshProjectsAction refreshProjectAction = new RefreshProjectsAction(window);
            				// refreshProjectAction.run();
        					
        				}
    				}
    				
    				
    				
    			} catch (PartInitException e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    			}
    		}
    	}
	}
	
	/**
	 * Gets the planner role name.
	 * @return The planner role name.
	 */
	public String getPlannerRoleName() {
		return roleName;
	}

	/**
	 * Sets the name of the Planner Role.
	 * @param roleName The name of the planner role.
	 */
	public void setPlannerRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

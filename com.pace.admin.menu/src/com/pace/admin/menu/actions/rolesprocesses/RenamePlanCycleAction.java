/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.actions.rolesprocesses;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.dialogs.RenamePlanCycleDialog;
import com.pace.admin.menu.editors.PlanCycleEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCycleNode;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.RoleConfigurationRemoval;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.SeasonFromRoleRemoval;
import com.pace.admin.menu.nodes.SeasonRemoval;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * Renames a plan cycle from the PlanCycleModelManager
 * @author Jordan
 * @version	x.xx
 *
 */
public class RenamePlanCycleAction extends Action {
	private static Logger logger = Logger.getLogger(RenamePlanCycleAction.class);
	private final static int OK_ID = 0;
	private final IWorkbenchWindow window;
	private String planCycleName;
	private TreeViewer viewer;
	private IProject project;
		
	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public RenamePlanCycleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		super(text);
	    this.window = window;
	    this.viewer = viewer;
	        
	    // The id is used to refer to the action in a menu or toolbar
	    this.setId("com.pace.admin.menu.actions.RenamePlanCycleAction");
	}

	/**
	 * Runs the RenamePlanCycleAction.
	 */
	public void run(){
		//if user wants to rename the plan cycle
		if ( planCycleName != null ) {
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PlanCycleEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_RENAME_MESSAGE);
						return;
				}
			} catch (PartInitException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    ISelection selection = viewer.getSelection();
	    	
	    	if ( selection instanceof IStructuredSelection) {
	    		
	    		PlanCycleNode planCycleNode = (PlanCycleNode) ((IStructuredSelection) selection).getFirstElement();
	    		
	    		if ( planCycleNode != null ) {
	    			
	    			IProject project = ViewerUtil.getProjectNode(planCycleNode);
					String oldPlanCycleName = planCycleNode.getName();
					PlanCycleModelManager planCycleModelManager = new PlanCycleModelManager(project);
	    			
					final RenamePlanCycleDialog dialog = new RenamePlanCycleDialog(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
							oldPlanCycleName, planCycleModelManager, 
							"Old Plan Cycle Name:", "New Plan Cycle Name:", "Rename Plan Cycle");
						
					int rc = dialog.open();
						
					//if user clicks ok
					if ( rc == OK_ID ) {
							
						//get new season process name
						String newPlanCycleName = dialog.getNewName();
							
						logger.info("Old Plan Cycle Name: " + oldPlanCycleName + "; New Plan Cycle Name: " + newPlanCycleName);
						
						planCycleModelManager.load();
						planCycleModelManager.renamePlanCycle(oldPlanCycleName, newPlanCycleName);
						planCycleModelManager.save();
						
						// TTN-2466 - update plan cycle references in seasons
						SeasonModelManager seasonModelManager = new SeasonModelManager(project);
						seasonModelManager.load();
						seasonModelManager.renamePlanCycle(oldPlanCycleName, newPlanCycleName);
						
						
						// TTN-2466 - update plan cycle references in plan configuration
						PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
						pafPlannerConfigModelManager.load();
						pafPlannerConfigModelManager.renamePlanCycle(oldPlanCycleName, newPlanCycleName);
						
						
						RolesProcessesNode rolesProcesses = (RolesProcessesNode) planCycleNode.getParent().getParent();
					
						//rebuilds the Roles & Processes tree.
						for(Object child : rolesProcesses.getChildren()) {
							if(child instanceof RoleConfigurationsNode){
								RoleConfigurationsNode roleConfigsNode = 
										(RoleConfigurationsNode) child;
								roleConfigsNode.createChildren(null);
								viewer.refresh(roleConfigsNode);
							}
							if(child instanceof SeasonsNode){
								SeasonsNode seasonsNode = 
										(SeasonsNode) child;
								seasonsNode.createChildren(null);
								viewer.refresh(seasonsNode);
							}
							if(child instanceof PlannerRolesNode){
								PlannerRolesNode rolesNode = 
										(PlannerRolesNode) child;
								rolesNode.createChildren(null);
								viewer.refresh(rolesNode);
							}
							if(child instanceof PlanCyclesNode){
								PlanCyclesNode plansNode = 
										(PlanCyclesNode) child;
								plansNode.createChildren(null);
								viewer.refresh(plansNode);
							}
						}
					}
	    		}
	    	}
		}
	}
		
	/**
	 * Gets the name of the plan cycle.
	 * @return
	 */
	public String getPlanCycleName() {
		return planCycleName;
	}

	/**
	 * Sets the name of the plan cycle.
	 * @param planCycleName Name of the plan cycle.
	 */
	public void setPlanCycleName(String planCycleName) {
		this.planCycleName = planCycleName;
	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
		
	/**
	 * Automatically generated method: toString
	 * @return Returns a string representation of the object.
	 */
	public String toString () {
		return super.toString();
	}
}

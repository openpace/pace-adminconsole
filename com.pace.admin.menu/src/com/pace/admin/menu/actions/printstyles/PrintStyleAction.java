/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.base.ui.PrintStyles;

public class PrintStyleAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.printstyles.PrintStyleAction";
	private static Logger logger = Logger.getLogger(PrintStyleAction.class);
	private IWorkbenchWindow window = null;
	private IProject project;
	private TreeViewer viewer;

	public PrintStyleAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
	}
	
	/**
	 * Runs the PrintStyleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//get the first selection.
		ISelection selection = viewer.getSelection();
       	if ( selection instanceof IStructuredSelection) {
			Object item = ((IStructuredSelection)selection).getFirstElement();
			//convert the node to a PrintStyleNode.
			PrintStyleNode node = (PrintStyleNode) item; 
			//look for any open editors and close them.
			EditorUtils.closeDuplicateEditorType(page, PrintStyleEditor.class);
			//Create a PrintStylesManager.
			PrintStyles model = new PrintStylesManager(project);
			//using the two model manager, create the input for the PrintStyleEditor.
			GlobalPrintStyleInput input = new GlobalPrintStyleInput(node.getName(), model, project);
		
			try {
				//open the editor
				PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
				//set the viewer.
				printStyleEditor.setViewer(viewer);
				//set the parent PrintStylesNode (for updating purposes)
				printStyleEditor.setProject(project);
			} catch (PartInitException e) {
				// handle error
				logger.error(e.getMessage());
			}
       	}
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

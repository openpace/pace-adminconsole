/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.inputs.UploadProjectInput;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

public class UploadPrintStylesAction extends AbstractUploadPrintStylesAction {
	
	public final static String ID = "com.pace.admin.menu.actions.printstyles.UploadPrintStylesAction";
	private static final Logger logger = Logger.getLogger(UploadPrintStylesAction.class);
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public UploadPrintStylesAction(IWorkbenchWindow window, PafServer server) {
		super(ID, window, server);
        this.setToolTipText(ID);
        this.setText(server.getName());
	}
	
	/**
	 * Deploys user security to server
	 */
	public void run() {

		if ( selection != null && selection instanceof IStructuredSelection && 
				(((IStructuredSelection) selection).getFirstElement() instanceof PrintStylesNode ||
						((IStructuredSelection) selection).getFirstElement() instanceof PrintStyleNode)) {
		
			if (project != null && project.exists() && server != null) {
				
				logger.info("Uploading Print Styles to server: " + server.getName());
				
				UploadProjectInput input = new UploadProjectInput(project.getName(), server);
				
				input.setSuccessfulMessage("Print styles were successfully uploaded to server '" + server.getName() + "'.");
				
				input.setUnsuccessfulMessage("Print styles were not successfully uploaded to server '" + server.getName() + "'.");
				
				input.setFilterSet(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.PrintStyles)));
				
				PafProjectUtil.uploadProjectToServer(input);
				
			}
			
		}
	}

}

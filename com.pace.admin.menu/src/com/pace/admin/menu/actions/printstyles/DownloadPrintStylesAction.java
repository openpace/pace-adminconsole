/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.views.MenuView;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafSoapException_Exception;

public class DownloadPrintStylesAction extends Action implements
		ISelectionListener, IWorkbenchAction {

	// ID code for action
	public final static String ID = "com.pace.admin.menu.actions.printstyles.DownloadPrintStylesAction";
	// looger
	private static Logger logger = Logger.getLogger(DownloadPrintStylesAction.class);
	// holds current workbech window
	private final IWorkbenchWindow window;
	// server to import files from
	private final PafServer server;
	private IProject project;
	// current selection
	private IStructuredSelection selection = null;

	public DownloadPrintStylesAction(IWorkbenchWindow window,
			PafServer server) {
		super(server.getName());
		this.window = window;
		this.server = server;
		this.setId(ID);
		window.getSelectionService().addSelectionListener(this);
	}

	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// if incoming is structured selection, set selection
		if (incoming instanceof IStructuredSelection) {

			selection = (IStructuredSelection) incoming;

		}

	}

	public void run() {

		if ( selection != null && selection instanceof IStructuredSelection && 
				(((IStructuredSelection) selection).getFirstElement() instanceof PrintStylesNode ||
						((IStructuredSelection) selection).getFirstElement() instanceof PrintStyleNode)) {
							
			// confirm with user to import print styles from server
			if (project != null && project.exists()) {
	
				if (server != null && 
						!GUIUtil.askUserAQuestion("Do you want to download print styles from server '" + server.getName() + "'?")) {
					return;
				}
				
				if ( ServerMonitor.getInstance().isServerRunning(server) ) {
				
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
						
						public void run() {
							
							Set<ProjectElementId> projectElementIdSet = new HashSet<ProjectElementId>();
							
							projectElementIdSet.add(ProjectElementId.PrintStyles);
					
							PaceProject pp = WebServicesUtil.downloadProjectFromServer(server, projectElementIdSet);
							
							boolean successful = false;
							
							if ( pp != null && pp instanceof XMLPaceProject) {
							
								XMLPaceProject xpp = (XMLPaceProject) pp;
								
								try {
									
									IFolder confFolder = project.getFolder(Constants.CONF_DIR);
									
									xpp.saveTo(confFolder.getLocation().toString(), projectElementIdSet);
															
									try {
										
										confFolder.refreshLocal(IResource.DEPTH_INFINITE, null);
										
									} catch (CoreException e1) {
										logger.error(e1.getMessage());
									}
									
									// get ref to the menu view
									MenuView menuView = MenuPlugin.getDefault().getMenuView();
					
									// if menu view exist
									if (menuView != null) {
					
										//refresh project tree
										menuView.refreshProjectStructure();
										menuView.selectProjectNode(project.getName());
																		
									}
									
									successful = true;											
														
								} catch (ProjectSaveException e) {
									logger.error(e.getMessage());
								}
								
								
							}
							
							String outMessage;
							
							if ( successful ) {
								
								outMessage = "Print styles were successfully downloaded from server '"
									+ server.getName() + "'.";
								
								logger.info(outMessage);
								
								GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
								
							} else {
								
								outMessage = "Print styles were not successfully dowloaded from server '"
									+ server.getName() + "'.";
									
								logger.error(outMessage);
								
								GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
								
							}
			
							ConsoleWriter.writeMessage(outMessage);
		
					}});
				}
						
			} 
					
		} else {
			GUIUtil.serverNotRunning(server);
		}
	}

	/**
	 * @return the server
	 */
	public PafServer getServer() {
		return server;
	}

	
	
}

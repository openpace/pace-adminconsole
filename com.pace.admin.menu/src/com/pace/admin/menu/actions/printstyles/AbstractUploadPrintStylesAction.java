/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.menu.actions.AbstractUploadAction;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PafServer;

public abstract class AbstractUploadPrintStylesAction extends AbstractUploadAction {

	protected IProject project;

	
	public AbstractUploadPrintStylesAction(String actionId, IWorkbenchWindow window, PafServer server) {
		super(actionId, window, server);
	}
	
	public AbstractUploadPrintStylesAction(String actionId, IWorkbenchWindow window, String serverGroupName, List<PafServer> servers) {
		super(actionId, window, serverGroupName, servers);
	}
	
	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	@Override
	public ProjectNode getProjectNode() {
		// TODO Auto-generated method stub
		return null;
	}
}
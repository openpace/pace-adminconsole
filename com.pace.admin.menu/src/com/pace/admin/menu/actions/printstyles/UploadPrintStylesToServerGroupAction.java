/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.dialogs.UploadLocalProjectDialog;
import com.pace.admin.global.inputs.UploadProjectToServerGroupInput;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;


public class UploadPrintStylesToServerGroupAction extends AbstractUploadPrintStylesAction {

	public final static String ID = "com.pace.admin.menu.actions.printstyles.UploadPrintStylesToServerGroupAction";
	private static final Logger logger = Logger.getLogger(UploadPrintStylesToServerGroupAction.class);
	
	public UploadPrintStylesToServerGroupAction(IWorkbenchWindow window, String serverGroup, List<PafServer> servers) {
		super(ID, window, serverGroup, servers);
	}
	
	public void run() {

		if ( selection != null && selection instanceof IStructuredSelection && 
				(((IStructuredSelection) selection).getFirstElement() instanceof PrintStylesNode ||
						((IStructuredSelection) selection).getFirstElement() instanceof PrintStyleNode)) {
		
			if (project != null && project.exists() && servers != null) {
				
				//Check if all servers are running, if not prompt user.
				if(!ImportExportUtil.okToDeployNonRunningServers(allServersRunning(), servers)){
					return;
				}
				
				//check for non-matching projects.
				if(!ImportExportUtil.okToDeployToNonMatchingServerProjects(PafProjectUtil.getApplicationName(this.getIProject()), servers)){
					return;
				}
				
				logger.info("Uploading Print Styles to server group: " + serverGroupName);
				
				UploadProjectToServerGroupInput input = new UploadProjectToServerGroupInput(project.getName(), serverGroupName, servers);
				
				input.setSuccessfulMessage("Print styles were successfully uploaded to server group '" + serverGroupName + "'.");
				
				input.setUnsuccessfulMessage("Print styles were not successfully uploaded to one or more servers in the '" + serverGroupName + "' server group.\r\n\r\n");
				
				input.setFilterSet(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.PrintStyles)));
				
				UploadLocalProjectDialog dialog = new UploadLocalProjectDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
						"Upload Project '" + project.getName() + "' to Server Group '" + serverGroupName + "'?",
						input.getFilterSet());

				int rc = dialog.open();
				
				if ( rc != 0 ) {
					return;
				}
				
				PafProjectUtil.uploadProjectToServerGroup(input);
				
			}
			
		}
	}
}
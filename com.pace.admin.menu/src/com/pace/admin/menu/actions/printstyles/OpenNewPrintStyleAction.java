/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.base.ui.PrintStyles;

public class OpenNewPrintStyleAction extends Action {

	public final static String ID = "com.pace.admin.menu.actions.printstyles.OpenNewPrintStyleAction";
	private static Logger logger = Logger.getLogger(OpenNewPrintStyleAction.class);
	private final IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	
	/**
	 * @param window
	 * @param project
	 * @param viewer
	 */
	public OpenNewPrintStyleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		super(text);
	    this.window = window;
	    this.viewer = viewer;
        // The id is used to refer to the action in a menu or toolbar
        this.setId(ID);
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD)));
	}

	/**
	 * @return the project
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(IProject project) {
		this.project = project;
	}


	public void run() {
		// TODO Auto-generated method stub
	   	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//look for any open editors and close them.
    	EditorUtils.closeDuplicateEditorType(page, PrintStyleEditor.class);		
		//create a PlanCycleModelManager.
    	PrintStyles model = new PrintStylesManager(project);
		//using the two model manager, create the input for the PlanCycleEditor.
		GlobalPrintStyleInput input = new GlobalPrintStyleInput("", model, project, true);
		
		try {
			//Open the new editor.
			PrintStyleEditor printStyleEditor = (PrintStyleEditor) 
				page.openEditor(input, PrintStyleEditor.ID);
			//set the viewer.
			printStyleEditor.setViewer(viewer);
			printStyleEditor.setProject(project);
		} catch (PartInitException e) {
			logger.error(e.getMessage());
		}     

	}


	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

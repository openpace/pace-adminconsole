/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.migration;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.funcs.CustomFunctionDef;
import com.pace.base.migration.MigrationAction;
import com.pace.base.migration.MigrationActionStatus;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.FileUtils;

/**
 * 1st. Removes the lib folder from the project.
 * 2nd. Adds new custom functions from global file to existing custom functions xml
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafCustomFunctionsMigrationAction extends MigrationAction {

	private static Logger logger = Logger.getLogger(PafCustomFunctionsMigrationAction.class);
	
	private IProject project = null;
	
	private IFolder iConfDirectory = null;
	
	/**
	 * Creates the custom functions migration action using a project.
	 * 
	 * @param project Project action should run against
	 */
	public PafCustomFunctionsMigrationAction(IProject project, XMLPaceProject xmlProject) {
	
		this.xmlPaceProject = xmlProject;
		
		//if project is not null, set rest of properties
		if ( project != null ) {
		
			iConfDirectory = project.getFolder(Constants.CONF_DIR);
		
//			if ( iConfDirectory != null && iConfDirectory.exists()) {
//				
//				this.confDirectory = iConfDirectory.getLocation().toOSString();
//				
//			}
			
		}
			
		
		this.project = project;
		
	}

	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getActionName()
	 */
	public String getActionName() {
		return "Update Custom Functions";
	}

	/**
	 * 
	 * A check to ensure all attributes are not null
	 *
	 * @return true if all attributes are not null
	 */
	private boolean isValidAttributes() {
		
		return (this.project != null && this.xmlPaceProject != null && this.xmlPaceProject.getProjectInput() != null && this.iConfDirectory != null);
		
	}
	
	/**
	 * 
	 * Checks to see if lib folder is present in project.
	 *
	 * @return True if lib folder still exists in project, fals if not
	 */
	private boolean isLibFolderPresent() {
		
		//get File ref to lib folder
		File libFolder = new File( project.getLocation().toOSString() + File.separator + Constants.LIB_DIR);
		
		//if not null and if exists, returns true
		if ( libFolder != null && libFolder.exists()) {
			return true;
		}
		
		return false;
		
	}
	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getStatus()
	 */
	public MigrationActionStatus getStatus() {		
		
		//if attributes are valid
		if ( isValidAttributes() ) {
		
			//if lib folder exists, return not started
			if ( isLibFolderPresent() ) {
				
				return MigrationActionStatus.NotStarted;
				
			}			
					
			//get missing custom function set
			Set<CustomFunctionDef> missingCustomFunctionsSet =  getMissingCustomFunctionsSet();
			
			//if missing custom function sets exists, then action needs to run
			if ( missingCustomFunctionsSet != null && missingCustomFunctionsSet.size() > 0 ) {
				
				return MigrationActionStatus.NotStarted;
				
			} else {
				
				//make it here and we are complete.
				return MigrationActionStatus.Completed;
			}
			
		}
		
		return MigrationActionStatus.NotStarted;
		
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#run()
	 */
	public void run() {
		
		//if valid attributes and action hasn't started yet
		if ( isValidAttributes() && getStatus().equals(MigrationActionStatus.NotStarted )) {
			
			//remove lib folder if present
			if ( isLibFolderPresent() ) {
				
				IFolder iLibFolder = project.getFolder(Constants.LIB_DIR);
				
				try {
					iLibFolder.delete(true, null);
				} catch (CoreException e) {
					
					logger.error("There was a problem deleting the project's lib folder.");
					
				}
				
			}
			
			//get missing custom function set
			Set<CustomFunctionDef> missingCustomFunctionsSet =  getMissingCustomFunctionsSet();
			
			//if mission custom funcitons exists
			if ( missingCustomFunctionsSet != null && missingCustomFunctionsSet.size() > 0 ) {
				
				//get IFile custom function file
				IFile iCustomFunctions = iConfDirectory.getFile(PafBaseConstants.FN_CustomFunctionMetaData);
				
				//get os path + name of custom function sfile
				String customFunctionFileName = iCustomFunctions.getLocation().toOSString();
				
				File customFunctionFile = new File(customFunctionFileName);		
				
				//if custom function file exists, then back it up
				if ( customFunctionFile.exists() ) {
					
					//backup the file
					try {
						
						FileUtils.copy(customFunctionFile, new File(customFunctionFileName + PafBaseConstants.BAK_EXT));
						
					} catch (IOException e) {
						logger.error("There was a problem backing up the file: " + e.getMessage());
					}
					
				}
				
				//get the projects custom function file map
				Map<String, CustomFunctionDef> projectCustomFunctionMap = PafProjectUtil.importCustomFunctions(iCustomFunctions);
								
				//loop missing functions and add to map
				for ( CustomFunctionDef missingCustomFunction : missingCustomFunctionsSet ) {
					
					projectCustomFunctionMap.put(missingCustomFunction.getFunctionName(), missingCustomFunction);
										
				}
				
				//export values to xml
				ACPafXStream.exportObjectToXml(projectCustomFunctionMap.values().toArray(new CustomFunctionDef[0]), iCustomFunctions);
				
			}
			
		}
		
		
	}

	/**
	 * 
	 * Gets the missing custom functions. Gets a map of project custom functions, then a map of
	 * global custom functions, creates a set of the global custom functions that aren't present
	 * in the project custom functions.
	 *
	 * @return Set of missing project custom functions.
	 */
	private Set<CustomFunctionDef> getMissingCustomFunctionsSet() {
		
		Set<CustomFunctionDef> customFunctionSet = new HashSet<CustomFunctionDef>();
		
		if ( isValidAttributes() ) {
		
			//get map of project custom functions
			IFile iCustomFunctions = iConfDirectory.getFile(PafBaseConstants.FN_CustomFunctionMetaData);
			Map<String, CustomFunctionDef> projectCustomFunctionMap = PafProjectUtil.importCustomFunctions(iCustomFunctions);

			//get map of global custom functions
			File globalCustomFunctionsFile = new File(Constants.ADMIN_CONSOLE_CONF_DIRECTORY + Constants.GLOBAL_XML_FOLDER_NAME + File.separator + PafBaseConstants.FN_CustomFunctionMetaData);
			Map<String, CustomFunctionDef> globalCustomFunctionMap = PafProjectUtil.importCustomFunctions(globalCustomFunctionsFile);

			//if global ones exists
			if ( projectCustomFunctionMap != null && globalCustomFunctionMap != null) {
			
				//popuate set with global custom functions that are missing from the project map
				Set<String> globalCustomFunctionNames = new HashSet<String>(globalCustomFunctionMap.keySet());
				
				for (String globalCustomFunctionName : globalCustomFunctionNames) {
					
					//if not in map, add to set
					if ( ! projectCustomFunctionMap.containsKey(globalCustomFunctionName)) {
						
						customFunctionSet.add(globalCustomFunctionMap.get(globalCustomFunctionName));
					}
					
				}
			
			}
		}
		
		//a set of missing project custom functions
		return customFunctionSet;
		
	}
}

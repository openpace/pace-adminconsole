/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.adminlock;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.AdminLockModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.AdminLockEditor;
import com.pace.admin.menu.editors.input.AdminLockEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.AdminLocksNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
* Action, opens an existing PlannerRole in a PlannerRoleEditor.
* @author kmoos
* @version x.xx
*/
public class CreateNewAdminLockAction extends Action {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.adminlock.CreateNewAdminLockAction";
	
	private static Logger logger = Logger.getLogger(CreateNewAdminLockAction.class);
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public CreateNewAdminLockAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.window = window;
		this.viewer = viewer;
        this.setToolTipText(ID);
        this.setId(ID);
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD)));
		setText("Create Admin Lock");
	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
    	
   		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			AdminLocksNode node = (AdminLocksNode) ((IStructuredSelection) selection).getFirstElement();
			//Close any open editors.
			EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), AdminLockEditor.class);
			//get the first selection.
			//current project
			IProject project = ViewerUtil.getProjectNode(node);
			
			AdminLockModelManager model = new AdminLockModelManager(project);
			
			try {
				AdminLockEditorInput input = new AdminLockEditorInput("", model, project, true);
			
				//get user security editor
				AdminLockEditor adminLockEditor = (AdminLockEditor) page.openEditor(input, AdminLockEditor.ID);

				//set the viewer.
				adminLockEditor.setViewer(viewer);
							
				//update tree node
				adminLockEditor.setTreeNode(node);
					
			} catch (PartInitException e) {
				// handle error
				logger.error(e.getMessage());
			} catch (Exception e) {
				// handle error
				GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, e.getMessage(), SWT.ICON_WARNING);
			}
			
		}
		
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

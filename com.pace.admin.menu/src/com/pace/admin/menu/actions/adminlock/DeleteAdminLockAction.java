/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.adminlock;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.AdminLockModelManager;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.AdminLockEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.AdminLockNode;
import com.pace.admin.menu.nodes.AdminLocksNode;
import com.pace.base.utility.StringUtils;

/**
* Action, opens an existing PlannerRole in a PlannerRoleEditor.
* @author jmilliron
* @version x.xx
*/
public class DeleteAdminLockAction extends Action {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.adminlock.DeleteAdminLockAction";
	
	private static Logger logger = Logger.getLogger(DeleteAdminLockAction.class);
	
	private IWorkbenchWindow window = null;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public DeleteAdminLockAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
        this.setToolTipText(ID);
		setText("Remove Admin Lock");
  	}
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
	  	IWorkbenchPage page = window.getActivePage();
	 	
		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			AdminLockNode adminLockNode = (AdminLockNode) ((IStructuredSelection) selection).getFirstElement();
				
			if ( project != null && adminLockNode != null)  {
				
				//get user security model manager
				AdminLockModelManager adminLockModelManager = new AdminLockModelManager(project);
				PafPlannerConfigModelManager roleConfigModelManager = new PafPlannerConfigModelManager(project);
				List<String> roleConfigs = roleConfigModelManager.findRoleConfigsByAdminLock(adminLockNode.getName());
				boolean isConfirmed = false;
				if( roleConfigs != null && roleConfigs.size() > 0 ) {
					isConfirmed = MessageDialog.openConfirm(window.getShell(), this.getText(), 
							"You are deleting an admin lock that's referenced by role configuration: " + StringUtils.arrayListToString(roleConfigs, ",") + ". The admin lock and assignment will be removed from the role configuration.  Do you want to continue?");
				}
				else {
					isConfirmed = GUIUtil.askUserAQuestion( "Delete Admin Lock",
							"Are you sure you want to delete the Admin Lock: '" + adminLockNode.getName() + "'? ");
				}
				if( isConfirmed ) {
					roleConfigModelManager.removeAdminLockInRoleConfiguraitons(adminLockNode.getName());
					
					//remove style
					adminLockModelManager.remove(adminLockNode.getName());
					
					//save model
					adminLockModelManager.save();
					
					//get parent node
					AdminLocksNode parent = (AdminLocksNode) adminLockNode.getParent();
					
					//recrete the children
					parent.createChildren(null);
					
					//refresh viewer
					viewer.refresh(parent);
					//look for any open editors and close them.
					EditorUtils.closeDuplicateEditorType(page, AdminLockEditor.class);
				}
			}
		}	
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

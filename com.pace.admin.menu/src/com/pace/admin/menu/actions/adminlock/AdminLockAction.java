/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.adminlock;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.AdminLockModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.AdminLockEditor;
import com.pace.admin.menu.editors.RoleConfigurationEditor;
import com.pace.admin.menu.editors.input.AdminLockEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.AdminLockNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
* Opens an existing user's security.
* 
* @author javaj
* @version x.xx
*/
public class AdminLockAction extends Action {

	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.actions.adminlock.AdminLockAction";
	
	private static Logger logger = Logger.getLogger(AdminLockAction.class);
	
	private IWorkbenchWindow window;
	
	private IStructuredSelection selection;
	
	private IProject project;
	
	private TreeViewer viewer;
	
	private boolean isClone = false;
	
	public AdminLockAction(IWorkbenchWindow window, TreeViewer viewer) {
		this(window, viewer, false);
	}
	
	/**
	 * Constructor.
	 * @param window The Workbench window.
	 * @param viewer TreeView object in which the node was clicked.
	 */
	public AdminLockAction(IWorkbenchWindow window, TreeViewer viewer, boolean isClone) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
        this.setToolTipText(ID);
        this.setId(ID);
        
        this.isClone = isClone;
        
        if(isClone){
        	setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
        }
        
 	}
	
	
	/**
	 * Runs the PlannerRoleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		
	   	IWorkbenchPage page = window.getActivePage();
			//get the first selection.
			ISelection selection = viewer.getSelection();
			if ( selection != null && selection instanceof IStructuredSelection ) {
				//refresh menu's viewer
				//current project
				AdminLockNode node = (AdminLockNode) ((IStructuredSelection) selection).getFirstElement();
				EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), 
						AdminLockEditor.class);
				IProject project = ViewerUtil.getProjectNode(node);
				//active page
				//look for any open editors and close them.
				AdminLockModelManager model = new AdminLockModelManager(project);
				try {
					AdminLockEditorInput input = new AdminLockEditorInput(node.getName(), model, project);
					//Open the new editor.
					AdminLockEditor adminLockEditor = (AdminLockEditor) page.openEditor(input, AdminLockEditor.ID);
					//set the viewer.
					adminLockEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
					adminLockEditor.setTreeNode(node);
				} catch (PartInitException e) {
					logger.error(e.getMessage());					
				} catch (Exception e) {
					// handle error
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, e.getMessage(), SWT.ICON_WARNING);
				}
				
			}
	}
	
	/**
	 * Gets the IProject.
	 * @return An IProject associated with this action.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Sets the IProject.
	 * @param project The IProject to associate to this action.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

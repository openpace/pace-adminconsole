/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions;

import java.util.List;
import java.util.Set;

import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewSectionNode;
import com.pace.base.ui.PafServer;

public abstract class AbstractUploadAction extends AbstractMenuAction {
	
	protected final List<PafServer> servers;
	
	protected final String serverGroupName;
	
	protected final PafServer server;
	
	public AbstractUploadAction(String actionId, IWorkbenchWindow window, String serverGroupName, List<PafServer> servers) {
		this(serverGroupName, actionId, window, serverGroupName, null, servers);
	}
	
	public AbstractUploadAction(String actionId, IWorkbenchWindow window, PafServer server) {
		this(server.getName(), actionId, window, null, server, null);
	}
	
	private AbstractUploadAction(String name, String actionId, IWorkbenchWindow window, String serverGroupName, PafServer server, List<PafServer> servers) {
		super(name, actionId, window);
		this.serverGroupName = serverGroupName;
		this.servers = servers;
		this.server = server;
	}
	
	public List<PafServer> getServers() {
		return servers;
	}

	public String getServerGroupName() {
		return serverGroupName;
	}

	public PafServer getServer() {
		return server;
	}
	
	/**
	 * Returns true if all the servers in the group are running.
	 * @return
	 */
	public boolean allServersRunning(){
		Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
		for(PafServer server : servers){
			if(!runningServerSet.contains(server)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns true if all the servers in the group are stopped.
	 * @return
	 */
	public boolean allServersStopped(){
		Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
		for(PafServer server : servers){
			if(runningServerSet.contains(server)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return Gets the user selected views
	 */
	public String[] getSelectedViewItems(){
		
		String[] selectedViewNames = null;
		Object objSelection = selection.getFirstElement();
		Object[] objSelections = selection.toArray();
		
		if ( objSelection instanceof ViewNode) {
			
			selectedViewNames = new String[objSelections.length];
			
			int i = 0;
			
			for(Object o : objSelections){
				
				ViewNode vsn = (ViewNode) o;
	    		
				selectedViewNames[i] = vsn.getName();
				
				i++;
		    	
	    	}
			
			
		}
		
		return selectedViewNames;
	}
	
	/**
	 * @return Gets the user selected views
	 */
	public String[] getSelectedViewSectionItems(){
		
		String[] selectedViewNames = null;
		Object objSelection = selection.getFirstElement();
		Object[] objSelections = selection.toArray();
		
		if ( objSelection instanceof ViewSectionNode) {
			
			selectedViewNames = new String[objSelections.length];
			
			int i = 0;
			
			for(Object o : objSelections){
				
				ViewSectionNode vsn = (ViewSectionNode) o;
	    		
				selectedViewNames[i] = vsn.getName();
				
				i++;
		    	
	    	}
			
			
		}
		
		return selectedViewNames;
	}
}

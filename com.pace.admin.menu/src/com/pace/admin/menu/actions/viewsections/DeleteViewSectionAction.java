/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewsections;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewSectionNode;
import com.pace.admin.menu.nodes.ViewSectionsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * 
 * Delete's the selected view section.
 * 
 * @author jmilliron
 * @version x.xx
 * 
 */
public class DeleteViewSectionAction extends Action {

	private final IWorkbenchWindow window;

	private Object[] viewSectionName;

	private TreeViewer viewer;

	private IProject project;

	/**
	 * 
	 * @param text
	 * @param window
	 * @param viewer
	 */
	public DeleteViewSectionAction(String text, IWorkbenchWindow window,
			TreeViewer viewer) {
		super(text);
		this.window = window;
		this.viewer = viewer;

		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
		
		// The id is used to refer to the action in a menu or toolbar
		setId("com.pace.admin.menu.actions.viewsections.DeleteViewSectionAction");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {

		if (viewSectionName != null) {
			
			String message = "Delete selected view sections?";
			if(viewSectionName != null && viewSectionName.length == 1){
				ViewSectionNode vn = (ViewSectionNode) viewSectionName[0];
				message = "Delete view section: '" + vn.getName() + "'?";
			} 
			
			// confirm with user
			boolean isConfirmed = MessageDialog.openConfirm(window.getShell(),
					"Confirm", message);

			// if confirmed
			if (isConfirmed) {

				ViewSectionModelManager viewSectionManager = new ViewSectionModelManager(project);
				ViewModelManager viewModelManager = new ViewModelManager(project);
				
				//remove view
		    	if(viewSectionName != null){
			    	for(Object o : viewSectionName){
			    		ViewSectionNode vn = (ViewSectionNode) o;
				    	viewSectionManager.remove(vn.getName());
				    	viewModelManager.removeViewSection(vn.getName());
			    	}
		    	}

				ISelection selection = viewer.getSelection();

				if (selection instanceof IStructuredSelection) {

					Object node = ((IStructuredSelection) selection)
							.getFirstElement();

					if (node instanceof ViewSectionNode) {

						ViewSectionNode viewSectionNode = (ViewSectionNode) node;	
						
						//create a project node
						ProjectNode projectNode = ViewerUtil.getProjectNode(viewSectionNode.getProject().getName());
						
						if(projectNode != null){
							for (Object obj : projectNode.getChildren()) {
								if ( obj instanceof ViewSectionsNode) {
									//get the high level node.
									ViewSectionsNode viewsNode = (ViewSectionsNode)obj;
									
									//re-create children
									viewsNode.createChildren(null);
									
									//update the menu plug in.
									viewer.refresh(obj);
								}	

								if (obj instanceof ViewGroupsNode) {
									//get the high level node.
									ViewGroupsNode viewGroupsNode = (ViewGroupsNode) obj;
									
									//re-create children
									viewGroupsNode.createChildren(null);
									
									//update the menu plug in.
									viewer.refresh(obj);
								}	
								
							}
							
						}

					}

				}
				
			}
			
		}

	}

	/**
	 * 
	 * Method_description_goes_here
	 * 
	 * @return
	 */
	public Object[] getViewSectionName() {
		return viewSectionName;
	}

	/**
	 * 
	 * Method_description_goes_here
	 * 
	 * @param viewSectionName
	 */
	public void setViewSectionName(Object[] viewSectionName) {
		this.viewSectionName = viewSectionName;
	}

	/**
	 * 
	 * Method_description_goes_here
	 * 
	 * @return
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * 
	 * Method_description_goes_here
	 * 
	 * @param project
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

}

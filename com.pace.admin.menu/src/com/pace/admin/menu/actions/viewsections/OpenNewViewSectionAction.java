/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewsections;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.ViewSectionDialog;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewSectionsNode;
import com.pace.admin.menu.nodes.ViewsNode;

/**
 * 
 * Opens a new view section using view section model manager.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class OpenNewViewSectionAction extends Action {

	private final IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	
	/**
	 * 
	 * @param text
	 * @param window
	 * @param viewer
	 */
	public OpenNewViewSectionAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
	        super(text);
	        this.window = window;
	        this.viewer = viewer;
	        
	        // The id is used to refer to the action in a menu or toolbar
	        setId("com.pace.admin.menu.actions.viewsections.OpenNewSectionViewAction");
	        	        
	    }

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.action.Action#run()
		 */
	    public void run() {
	        
	    	   	
	    	ViewSectionDialog viewSectionDialog = new ViewSectionDialog(window.getShell(), project, null, true);
	    	
	    	if(viewSectionDialog.open() == Dialog.OK){
		    	ISelection selection = viewer.getSelection();
		    	
		    	if ( selection instanceof IStructuredSelection) {
		    		
		    		Object node = ((IStructuredSelection) selection).getFirstElement();
		    		
		    		if ( node instanceof ViewSectionsNode ) {
		    			
		    			ViewSectionsNode viewSectionsNode = (ViewSectionsNode) node;
						viewSectionsNode.createChildren(null);
		    			viewer.refresh(node);
		    			
			    		if(viewSectionDialog.isCreatedNewView()){
			    			
							ProjectNode projectNode = (ProjectNode)viewSectionsNode.getParent();

			    			if(projectNode != null){
			    				
			    				for (Object obj : projectNode.getChildren()) {
			    					
			    					if ( obj instanceof ViewsNode) {
			    						
			    						//get the high level node.
			    						ViewsNode viewsNode = (ViewsNode)obj;
			    						
			    						//re-create children
			    						viewsNode.createChildren(null);
			    						
			    						//update the menu plug in.
			    						MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
			    					}
			    					
			    				}
			    				
			    			}
			    			
			    		}
		    				
		    		}
		    		
		    	}
		    	
	    	}
	        	        
	    }

	    /**
	     * 
	     *	get's project
	     *
	     * @return
	     */
		public IProject getProject() {
			return project;
		}

		/**
		 * 
		 *	sets project
		 *
		 * @param project
		 */
		public void setProject(IProject project) {
			this.project = project;
		}
	    
	    

	

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.viewsections;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.RenameViewDialog;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewSectionNode;
import com.pace.admin.menu.nodes.ViewSectionsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * Renames a view group.
 *
 * @author themoosman
 * @version	1.00
 *
 */
public class RenameViewSectionAction extends Action implements IWorkbenchAction,
		ISelectionListener {
	
	private static Logger logger = Logger.getLogger(RenameViewSectionAction.class);
	
	public final static String ID = "com.pace.admin.menu.actions.viewsection.RenameViewSectionAction";
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
		
	private IProject project;
	
	/**
	 * 
	 * @param window
	 */
	public RenameViewSectionAction(IWorkbenchWindow window) {
		
		setId(ID);
		
		this.window = window;
		
		//set Action text
		setText("Rename View Section...");
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		Object object = selection.getFirstElement();
		
		
		if ( object instanceof ViewSectionNode ) {
		
			//cast
			ViewSectionNode viewSectionNode = (ViewSectionNode) object;
			
			ViewSectionModelManager viewSectionModel = new ViewSectionModelManager(project);
			
			ViewModelManager viewModelManager = new ViewModelManager(project);
			
		    ArrayList<String> viewSectionNames = new ArrayList<String>(Arrays.asList(viewSectionModel.getKeys())); 
			
			
			if ( viewSectionNames.contains(viewSectionNode.getName())) {
				
				RenameViewDialog dialog = new RenameViewDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
						viewSectionNode.getName(), viewSectionNames.toArray(new String[0]),
						"Old View Section Name:",
						"New View Section Name:",
						"Rename View Section");
								
				int rc = dialog.open();
				
				//if user clicks ok
				if ( rc == 0 ) {
					
					//get new view group name
					String newViewGroupName = dialog.getNewViewGroupName();
					
					logger.info("Old View Section Name: " + viewSectionNode.getName() + "; New View Section Name: " + newViewGroupName);
				
					//load new model data
					viewSectionModel.load();
					
					//rename view section name
					viewSectionModel.renameViewSection(viewSectionNode.getName(), newViewGroupName);
					
					//load new model data
					viewModelManager.load();
					
					//rename view name
					viewModelManager.renameViewSection(viewSectionNode.getName(), newViewGroupName);
	
					//create a project node
					ProjectNode projectNode = ViewerUtil.getProjectNode(viewSectionNode.getProject().getName());
					
					if(projectNode != null){
						for (Object obj : projectNode.getChildren()) {
							if ( obj instanceof ViewSectionsNode) {
								//get the high level node.
								ViewSectionsNode viewsNode = (ViewSectionsNode)obj;
								
								//re-create children
								viewsNode.createChildren(null);
								
								//update the menu plug in.
								MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
							}	

							if (obj instanceof ViewGroupsNode) {
								//get the high level node.
								ViewGroupsNode viewGroupsNode = (ViewGroupsNode) obj;
								
								//re-create children
								viewGroupsNode.createChildren(null);
								
								//update the menu plug in.
								MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
							}							
						}
					}					
				}
			}	
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		//if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {
		
		//remove from selection listener
		window.getSelectionService().removeSelectionListener(this);
		
	}

	public void setProject(IProject project) {

		this.project = project;
		
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.actions.AbstractAction;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.menu.nodes.ProjectNode;

public abstract class AbstractMenuAction extends AbstractAction {

	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractMenuAction(String name, String actionId, IWorkbenchWindow window) {
		super(name, actionId, window);

	}
	
	/**
	 * 
	 * @return the Workspace Root
	 */
	public IWorkspaceRoot getRootWorkspace(){
		return ResourcesPlugin.getWorkspace().getRoot();
	
	}
	
	/**
	 * @return IProject associated to specified project
	 */
	public IProject getIProject(){
		return getRootWorkspace().getProject(getProjectNode().getName());
	
	}
	
	/**
	 * 
	 * @param projectName name of the project
	 * @return the IFolder
	 */
	public IFolder getIConfFolder(){
		return getIProject().getFolder(Constants.CONF_DIR);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public abstract void run();
	
	public abstract ProjectNode getProjectNode();
	
}

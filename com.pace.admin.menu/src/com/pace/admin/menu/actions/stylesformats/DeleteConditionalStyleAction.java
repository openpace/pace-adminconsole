/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.stylesformats;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.model.managers.ConditionalStyleModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ConditionalStyleNode;
import com.pace.admin.menu.nodes.StylesFormatsNode;
import com.pace.base.format.ConditionalFormat;

public class DeleteConditionalStyleAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.commands.stylesformats.DeleteConditionalStyleAction";
	private static Logger logger = Logger.getLogger(DeleteConditionalStyleAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	private ConditionalStyleNode conditionalStyleNode;

	public DeleteConditionalStyleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
		setText(text);
	}
	
	/**
	 * Runs the PrintStyleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
	  	IWorkbenchPage page = window.getActivePage();
	 	
		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			conditionalStyleNode = (ConditionalStyleNode) ((IStructuredSelection) selection).getFirstElement();
				
			if ( project != null && conditionalStyleNode != null)  {
				
				//get user security model manager
				ConditionalStyleModelManager condStyleModelManager = new ConditionalStyleModelManager(project);
				ConditionalFormatModelManager condFormatModelManager = new ConditionalFormatModelManager(project);
				ConditionalFormat condFormat = condFormatModelManager.findConditionalFormatWithStyle(conditionalStyleNode.getName());
				if( condFormat == null ) {
					if( ! GUIUtil.askUserAQuestion( "Delete Conditional Format",
							"Are you sure you want to delete the conditional format: '" + conditionalStyleNode.getName() + "'? ") )
						return;
				}
				else { //if( condFormat != null ) 
					boolean isConfirmed = MessageDialog.openConfirm(window.getShell(), this.getText(), 
							"You are deleting a conditional style that's part of a conditional format. The style and assignment will be removed from the conditional format.  Do you want to continue?");
					
					if( isConfirmed ) {
						int allFormatCounts = condFormatModelManager.findStyleCountInConditionalFormat(condFormat.getName(), null);
						int styleFormatCounts = condFormatModelManager.findStyleCountInConditionalFormat(condFormat.getName(), conditionalStyleNode.getName());
						if( allFormatCounts - styleFormatCounts < 1 ) {
							MessageDialog.openError(window.getShell(), this.getText(), 
									"You cannot delete conditional style: '" + conditionalStyleNode.getName() + "'. Conditional format: '" + condFormat.getName() + "' requires at least 1 style.");
							return;
						}
					}
				}
				
				condFormatModelManager.removeStyleInConditionalFormats(conditionalStyleNode.getName());
				//look for any open editors and close them.
				EditorUtils.closeDuplicateEditorType(page, ConditionalStyleEditor.class);
				//remove style
				condStyleModelManager.remove(condStyleModelManager.getGUIDByName(conditionalStyleNode.getName()));
				
				//save model
				condStyleModelManager.save();
				
				//get parent node
				StylesFormatsNode parent = (StylesFormatsNode) conditionalStyleNode.getParent().getParent().getParent();
				
				//recrete the children
				parent.createChildren(null);
				
				//refresh viewer
				viewer.refresh(parent);
			}
		}	
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

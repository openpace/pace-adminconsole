/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.stylesformats;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.AdminLockEditor;
import com.pace.admin.menu.editors.ConditionalFormatEditor;
import com.pace.admin.menu.editors.input.ConditionalFormatEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.util.ViewerUtil;

public class ConditionalFormatAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.commands.stylesformats.ConditionalFormatAction";
	private static Logger logger = Logger.getLogger(ConditionalFormatAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;

	public ConditionalFormatAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
	}
	
	/**
	 * Runs the ConditionalFormatAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			ConditionalFormatNode node = (ConditionalFormatNode) ((IStructuredSelection) selection).getFirstElement();
			EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), 
					ConditionalFormatEditor.class);
			IProject project = ViewerUtil.getProjectNode(node);
			//active page
			//look for any open editors and close them.
			ConditionalFormatModelManager model = new ConditionalFormatModelManager(project);
			
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ConditionalFormatEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't open editor while dependent editor(s) is open");
						return;
				}				
				
				ConditionalFormatEditorInput input = new ConditionalFormatEditorInput(node.getName(), model, project);
				if( input.getCondStyles() == null || input.getCondStyles().length == 0 ){
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, 
							"At least one conditional style must be created before a conditional format can be created." );
					return;
				}
				//Open the new editor.
				ConditionalFormatEditor condFormatEditor = (ConditionalFormatEditor) page.openEditor(input, ConditionalFormatEditor.ID);
				//set the viewer.
				condFormatEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
				condFormatEditor.setTreeNode(node);
			} catch (PartInitException e) {
				logger.error(e.getMessage());					
			} catch (Exception e) {
				GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, e.getMessage(), SWT.ICON_WARNING);
			}
			
		}
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

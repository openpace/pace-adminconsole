/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.stylesformats;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.ConditionalFormatEditor;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.nodes.ConditionalFormatsNode;

public class DeleteConditionalFormatAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.commands.stylesformats.DeleteConditionalFormatAction";
	private static Logger logger = Logger.getLogger(DeleteConditionalFormatAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;
	private ConditionalFormatNode conditionalFormatNode;

	public DeleteConditionalFormatAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
		setText(text);
		setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
 	}
	
	/**
	 * Runs the PrintStyleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {

		try {
			
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ConditionalFormatEditor.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "Can't delete reference while dependent editor(s) is open");
					return;
			}
			
			//using the current window, get the active page.
		  	IWorkbenchPage page = window.getActivePage();
		 	
			//get the first selection.
			ISelection selection = viewer.getSelection();
			if ( selection != null && selection instanceof IStructuredSelection ) {
				//refresh menu's viewer
				//current project
				conditionalFormatNode = (ConditionalFormatNode) ((IStructuredSelection) selection).getFirstElement();
				//confirm with user
				if (GUIUtil.askUserAQuestion( "Delete Conditional Format",
						"Are you sure you want to delete the conditional format: '" + conditionalFormatNode.getName() + "'? ")) {
		
					//look for any open editors and close them.
					EditorUtils.closeDuplicateEditorType(page, ConditionalFormatEditor.class);
					
					if ( project != null && conditionalFormatNode != null)  {
						
						//get user security model manager
						ConditionalFormatModelManager condFormatModelManager = new ConditionalFormatModelManager(project);
						
						//remove user
						condFormatModelManager.remove(conditionalFormatNode.getName());
						
						//save model
						condFormatModelManager.save();
						
						//get parent node
						ConditionalFormatsNode parent = (ConditionalFormatsNode) conditionalFormatNode.getParent();
						
						//recrete the children
						parent.createChildren(null);
						
						//refresh viewer
						viewer.refresh(parent);
						
					}
				}
			}
			
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

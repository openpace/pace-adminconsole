/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.stylesformats;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.model.managers.ConditionalStyleModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.input.ConditionalStyleEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ConditionalStyleNode;
import com.pace.admin.menu.util.ViewerUtil;

public class CopyConditionalStyleAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.commands.stylesformats.ConditionalStyleAction";
	private static Logger logger = Logger.getLogger(CopyConditionalStyleAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;

	public CopyConditionalStyleAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
		setText(text);
	}
	
	/**
	 * Runs the PrintStyleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			ConditionalStyleNode node = (ConditionalStyleNode) ((IStructuredSelection) selection).getFirstElement();
			//Close any other open editors.
			EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ConditionalStyleEditor.class);
			//current project
			IProject project = ViewerUtil.getProjectNode(node);
			//active page
			//look for any open editors and close them.
			ConditionalStyleModelManager model = new ConditionalStyleModelManager(project);
			ConditionalStyleEditorInput input = new ConditionalStyleEditorInput(node.getName(), model, project, false, true );
			try {
				//Open the new editor.
				ConditionalStyleEditor condStyleEditor = (ConditionalStyleEditor) page.openEditor(input, ConditionalStyleEditor.ID);
				//set the viewer.
				condStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
				condStyleEditor.setTreeNode(node);
			} catch (PartInitException e) {
				logger.error(e.getMessage());					
			}
			
		}
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

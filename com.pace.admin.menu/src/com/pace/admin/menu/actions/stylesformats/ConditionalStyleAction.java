/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.stylesformats;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ConditionalStyleModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.ConditionalFormatEditor;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.input.ConditionalStyleEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.nodes.ConditionalStyleNode;
import com.pace.admin.menu.nodes.ConditionalStylesNode;
import com.pace.admin.menu.util.ViewerUtil;

public class ConditionalStyleAction extends Action {
	/**
	 * Unique id for the action.
	 */
	public final static String ID = "com.pace.admin.menu.commands.stylesformats.ConditionalStyleAction";
	private static Logger logger = Logger.getLogger(ConditionalStyleAction.class);
	private IWorkbenchWindow window;
	private IProject project;
	private TreeViewer viewer;

	public ConditionalStyleAction(IWorkbenchWindow window, TreeViewer viewer) {
		this.setId(ID);
		this.window = window;
		this.viewer = viewer;
		this.setToolTipText(ID);
	}
	
	/**
	 * Runs the PrintStyleAction.
	 */
	@SuppressWarnings("deprecation")
	public void run() {
    	//using the current window, get the active page.
    	IWorkbenchPage page = window.getActivePage();
		//get the first selection.
		ISelection selection = viewer.getSelection();
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			ConditionalStyleNode node = (ConditionalStyleNode) ((IStructuredSelection) selection).getFirstElement();
			EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), 
					ConditionalStyleEditor.class);
			IProject project = ViewerUtil.getProjectNode(node);
			//active page
			ConditionalStyleModelManager model = new ConditionalStyleModelManager(project);
			ConditionalStyleEditorInput input = null;
			if(node.getParent() instanceof ConditionalStylesNode){
				
				input = new ConditionalStyleEditorInput(node.getName(), model, project, false, false );
				
			}else if(node.getParent() instanceof ConditionalFormatNode){
				
				input = new ConditionalStyleEditorInput(getStyleNameFromTreeItem(node.getName()), model, project, false, false );
			}
			try {
				// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ConditionalStyleEditor.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
						return;
				}				
				
				//Open the new editor.
				ConditionalStyleEditor condStyleEditor = (ConditionalStyleEditor) page.openEditor(input, ConditionalStyleEditor.ID);
				//set the viewer.
				condStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
				condStyleEditor.setTreeNode(node);
			} catch (PartInitException e) {
				logger.error(e.getMessage());					
			}
			
		}
	}
	
	private String getStyleNameFromTreeItem(String treeItem) {
		int index1 = treeItem.indexOf("(");
		int index2= treeItem.indexOf(")");
		String member = treeItem.substring(index1+1,index2);
		return member;
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Automatically generated method: toString
	 * @return String representation of object.
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.views;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.RenameViewDialog;
import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.editors.RoleConfigurationEditor;
import com.pace.admin.menu.editors.input.RoleConfigurationInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * Renames a view group.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class RenameViewAction extends Action implements IWorkbenchAction,
		ISelectionListener {
	
	private static Logger logger = Logger.getLogger(RenameViewAction.class);
	
	public final static String ID = "com.pace.admin.menu.actions.views.RenameViewAction";
	
	private IWorkbenchWindow window = null;
	
	private IStructuredSelection selection = null;
		
	private IProject project;
	
	/**
	 * 
	 * @param window
	 */
	public RenameViewAction(IWorkbenchWindow window) {
		
		setId(ID);
		
		this.window = window;
		
		//set Action text
		setText("Rename View...");
		
		//add to selection service
		window.getSelectionService().addSelectionListener(this);
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewDialog.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_RENAME_MESSAGE);
					return;
			}
			
			Object object = selection.getFirstElement();
			
			
			if ( object instanceof ViewNode ) {
			
				//cast
				ViewNode viewNode = (ViewNode) object;
				
				PafViewGroupModelManager viewGroupModelManager = new PafViewGroupModelManager(project);
				
				PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
				
				ViewModelManager viewModelManager = new ViewModelManager(project);
				
				if ( viewModelManager.contains(viewNode.getName())) {
				
					//create unique set of view and view group names. 
					Set<String> uniqueStringSet = new HashSet<String>();
					
					uniqueStringSet.addAll(viewModelManager.getKeySet());
					//uniqueStringSet.addAll(viewModelManager.getKeySet());
					
					//get list of open editors
					IEditorReference[] editorRefs = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
					
					//loop over open editors
					for ( IEditorReference editorRef : editorRefs ) {
																	
						try {
							
							//get editor input
							IEditorInput input = editorRef.getEditorInput();
																	
							//see if we get a match for a view group editor, close it
							if ( input instanceof RoleConfigurationInput) {
								
								EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), RoleConfigurationEditor.class);
								
							}							
							
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
					RenameViewDialog dialog = new RenameViewDialog(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
							viewNode.getName(), 
							uniqueStringSet.toArray(new String[0]),
							"Old View Name:",
							"New View Name:",
							"Rename View");
									
					int rc = dialog.open();
					
					//if user clicks ok
					if ( rc == 0 ) {
						
						//get new view group name
						String newViewGroupName = dialog.getNewViewGroupName();
						
						logger.info("Old View Name: " + viewNode.getName() + "; New View Name: " + newViewGroupName);
					
						//load new model data
						viewModelManager.load();
						
						//rename view name
						viewModelManager.renameView(viewNode.getName(), newViewGroupName);
			
						//load new model data
						viewGroupModelManager.load();
						
						//rename view name
						viewGroupModelManager.renameViewInViewGroups(viewNode.getName(), newViewGroupName);
						
						//load new model data
						pafPlannerConfigModelManager.load();
						
						//rename view group name
						pafPlannerConfigModelManager.renameViewOrViewGroupInRoleConfigurations(
								viewNode.getName(), newViewGroupName);
						
		
						
						//create a project node
						ProjectNode projectNode = ViewerUtil.getProjectNode(viewNode.getProject().getName());

						if(projectNode != null){
							for (Object obj : projectNode.getChildren()) {
								if ( obj instanceof ViewsNode) {
									//get the high level node.
									ViewsNode viewsNode = (ViewsNode)obj;
									
									//re-create children
									viewsNode.createChildren(null);
									
									//update the menu plug in.
									MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
								}	

								if (obj instanceof ViewGroupsNode  ) {
									//get the high level node.
									ViewGroupsNode viewGroupsNode = (ViewGroupsNode) obj;
									
									//re-create children
									viewGroupsNode.createChildren(null);
									
									//update the menu plug in.
									MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
								}							
							}
						}					
					}
				}	
			}
			
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		//if instance of structured selection, save selection
		if (selection instanceof IStructuredSelection) {
			
			this.selection = (IStructuredSelection) selection;
			
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {
		
		//remove from selection listener
		window.getSelectionService().removeSelectionListener(this);
		
	}

	public void setProject(IProject project) {

		this.project = project;
		
	}
	
}

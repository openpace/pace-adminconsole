/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.views;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ViewsNode;

/**
 * 
 * Opens view dialog for a new view.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class OpenNewViewAction extends Action {

	private final IWorkbenchWindow window;
	private TreeViewer viewer;
	private IProject project;

	public OpenNewViewAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
	        super(text);
	        this.window = window;
	        this.viewer = viewer;
	        
	        // The id is used to refer to the action in a menu or toolbar
	        setId("com.pace.admin.menu.actions.views.OpenNewViewAction");
	      	      
	    }

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	    public void run() {
	    	
	    	try {
	    		
	    		// TTN-2387 - check for open conflicting editors
				if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewDialog.class)) {
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
						return;
				}
				
				ViewDialog viewDialog = new ViewDialog(window.getShell(), project, null, true);
		    	viewDialog.open();
		    	
		    	ISelection selection = viewer.getSelection();
		    	
		    	if ( selection instanceof IStructuredSelection) {
		    		
		    		Object node = ((IStructuredSelection) selection).getFirstElement();
		    		
		    		if ( node instanceof ViewsNode ) {
		    			
		    			ViewsNode viewsNode = (ViewsNode) node;
						//viewsNode.clearChildren();
		    			viewsNode.createChildren(null);
	    			
		    			viewer.refresh(node);
	    			
		    		}
	    	
		    	}
		    	
			} catch (PartInitException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    }

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	

}

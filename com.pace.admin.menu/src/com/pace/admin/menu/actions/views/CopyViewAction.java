/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.views;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewsNode;

/**
 * 
 * Opens view dialog for a copied view.
 *
 * @author themoosman
 * @version	x.xx
 *
 */
public class CopyViewAction extends Action {

	private final IWorkbenchWindow window;
	private String viewName;
	private TreeViewer viewer;
	private IProject project;

	public CopyViewAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
	        super(text);
	        this.window = window;
	        this.viewer = viewer;
	        
	        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY)));
	        
	        // The id is used to refer to the action in a menu or toolbar
	        setId("com.pace.admin.menu.actions.views.CopyViewAction");
	    }

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	    public void run() {
 
	    	if ( viewName != null ) {
	  
		    	ISelection selection = viewer.getSelection();
		    	
		    	ViewDialog dialog = new ViewDialog(
		    			window.getShell(), 
						project,
						true,
						viewName);
		    	
		        int rc = dialog.open();
		    	
		    	if(rc == Dialog.OK){
		    	
			    	if ( selection instanceof IStructuredSelection) {
			    		
			    		Object node = ((IStructuredSelection) selection).getFirstElement();
			    		
			    		if ( node instanceof ViewNode ) {
			    			
			    			ViewNode viewNode = (ViewNode) node;
			    			
			    			ViewsNode parentNode = (ViewsNode) viewNode.getParent();    			
			    			
			    			parentNode.createChildren(null);
			    			
			    			//Makes the UI refresh after copying a view
			    			if(parentNode.getParent() != null && parentNode.getParent().getChildren() != null){
			    				for(Object child : parentNode.getParent().getChildren()){
			    					if(child instanceof ViewGroupsNode){
			    						ViewGroupsNode viewGroupsNode = (ViewGroupsNode) child;
			    						viewGroupsNode.createChildren(null);
			    						for(Object objViewGroupNode : viewGroupsNode.getChildren()){
			    							ViewGroupNode viewGroupNode = (ViewGroupNode) objViewGroupNode;
			    							viewGroupNode.createChildren(null);
			    						}
			    						break;
			    					}
			    				}
			    			}
			    		}
			    		
			    	}
			    	viewer.refresh();
		    	}
	    	}       
    }

    public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	    
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}
}

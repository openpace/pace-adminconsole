/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.actions.views;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.editors.RoleConfigurationEditor;
import com.pace.admin.menu.editors.input.RoleConfigurationInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.util.ViewerUtil;

/**
 * 
 * Deletes a view using the ViewModelManager.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class DeleteViewAction extends Action {

	private final IWorkbenchWindow window;
	private Object[] viewName;
	private TreeViewer viewer;
	private IProject project;
	

	/**
	 * 
	 * @param text
	 * @param window
	 * @param viewer
	 */
	public DeleteViewAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		
	        super(text);
	        this.window = window;
	        this.viewer = viewer;
	        
	        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE)));
	        
	        // The id is used to refer to the action in a menu or toolbar
	        setId("com.pace.admin.menu.actions.views.DeleteViewAction");

	        
	    }

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.action.Action#run()
		 */
	    public void run() {	   
	    	
	    	//if new name not null
	    	if ( viewName != null ) {
	    		
	    		try {
	    			
					// TTN-2387 - check for open conflicting editors
					if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewDialog.class)) {
						MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_DELETE_MESSAGE);
							return;
					}
					
					String message = "Delete selected views?";
					if(viewName != null && viewName.length == 1){
						ViewNode vn = (ViewNode) viewName[0];
						message = "Delete view: '" + vn.getName() + "'?";
					} 
		    		
		    		//confirm with user
		    		boolean isConfirmed = MessageDialog.openConfirm(window.getShell(), "Confirm", message);
		    		
		    		//if confirmed
		    		if ( isConfirmed ) {
		    		
		    			//get list of open editors
						IEditorReference[] editorRefs = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
						
						//loop over open editors
						for ( IEditorReference editorRef : editorRefs ) {
																		
							try {
								
								//get editor input
								IEditorInput input = editorRef.getEditorInput();
																		
								//see if we get a match for a view group editor, close it
								if ( input instanceof RoleConfigurationInput) {
									
									EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), RoleConfigurationEditor.class);
									
								}							
								
							} catch (PartInitException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
		    			
		    			
			    		//get view model manager
				    	ViewModelManager viewManager = new ViewModelManager(project);
				    	
				    	//remove view
				    	if(viewName != null){
					    	for(Object o : viewName){
					    		ViewNode vn = (ViewNode) o;
						    	viewManager.remove(vn.getName());
					    	}
				    	}
				    	
				    	ISelection selection = viewer.getSelection();
				    	
				    	if ( selection instanceof IStructuredSelection) {
				    		
				    		Object node = ((IStructuredSelection) selection).getFirstElement();
				    		
				    		if ( node instanceof ViewNode ) {
				    			
				    			ViewNode viewNode = (ViewNode) node;
				    			
//				    			ViewsNode parentNode = (ViewsNode) viewNode.getParent();    			
				    			
				    			
								//create a project node
								ProjectNode projectNode = ViewerUtil.getProjectNode(viewNode.getProject().getName());

								if(projectNode != null){
									for (Object obj : projectNode.getChildren()) {
										if ( obj instanceof ViewsNode) {
											//get the high level node.
											ViewsNode viewsNode = (ViewsNode)obj;
											
											//re-create children
											viewsNode.createChildren(null);
											
											//update the menu plug in.
											MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
										}	

										if (obj instanceof ViewGroupsNode  ) {
											//get the high level node.
											ViewGroupsNode viewGroupsNode = (ViewGroupsNode) obj;
											
											//re-create children
											viewGroupsNode.createChildren(null);
											
											//update the menu plug in.
											MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
										}							
									}
								}	
				    			
				    		}
				    		
				    	}
				    	viewer.refresh();
		    		}
				} catch (PartInitException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
	    }

		public Object[] getViewName() {
			return viewName;
		}

		public void setViewName(Object[] viewName) {
			this.viewName = viewName;
		}

		public IProject getProject() {
			return project;
		}

		public void setProject(IProject project) {
			this.project = project;
		}

	

}

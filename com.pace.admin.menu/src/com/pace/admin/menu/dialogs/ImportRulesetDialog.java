/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;


import java.awt.Checkbox;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.actions.importexport.ImportProjectFromFileset;
import com.pace.admin.menu.actions.importexport.UploadRuleSetToServerAction;
import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.menu.project.ProjectSource;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.wizards.ProjectImportWizard;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

public class ImportRulesetDialog extends Dialog{
	
	private static Logger logger = Logger.getLogger(ImportRulesetDialog.class);
	
	private java.util.List<ProjectSource> projectSourceList = new ArrayList<ProjectSource>();
	private java.util.List<String> fileExtenstionList = new ArrayList<String>();
	private Text text;
	private String fileNameLabel;
	private String fileDialogTitle;
	private String SelectedFileName;
	MenuPreferences preferences;
	private Label lblExistingProjects;
	
	
	private Combo combo;
	String[] itemList;
	String selected;
	private IWorkbenchWindow window;
	boolean isDeployToServer ;
	private final String PREF_NODE = "com.pace.admin.menu.dialogs.ImportRulesetDialog";

	public ImportRulesetDialog(IWorkbenchWindow window,String wizardPageName, String wizardPageTitle,
			String wizardPageDesc, String[] itemList, String fileNameLabel, String fileDialogTitle, java.util.List<String> fileExtenstionList ){
		super(window);
		this.window = window;
		setShellStyle(getShellStyle()|SWT.RESIZE );
		
		preferences = new MenuPreferences(PREF_NODE);
		
		if ( projectSourceList != null ) {
			this.projectSourceList.addAll(projectSourceList);
		}
		
        this.itemList = itemList;
		this.fileNameLabel = fileNameLabel;
		this.fileDialogTitle = fileDialogTitle;
		
		if ( fileExtenstionList != null ) {
			
			this.fileExtenstionList.addAll(fileExtenstionList);
			
		}
		
	}
	protected Point getInitialSize() {
		return new Point(650, 420);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Constants.IMPORT_RULESET_WIZARD_NAME);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.None);
		
		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginWidth = 15;
		gridLayout.marginHeight = 20;
		container.setLayout(gridLayout);
		
		
		
		//setControl(container);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE); 
		{
			// Filename/Project Label
			Label lblProjectName_1 = new Label(container, SWT.NONE);
			
			GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
			lblProjectName_1.setLayoutData(gridData);
			
			if ( fileNameLabel != null ) {				
				lblProjectName_1.setText(fileNameLabel);
			} else {
				lblProjectName_1.setText("File Name:");
			}
		}
		{
			text = new Text(container, SWT.BORDER | SWT.READ_ONLY);
			text.setEnabled(false);
			
			GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
			text.setLayoutData(gridData);
			
		//	text.setRedraw(true);
			
			text.addModifyListener(new ModifyListener() {

				public void modifyText(ModifyEvent e) {

					updateOkButton();

				}

			});
			
			String fileName = preferences.getStringSetting(getPageSettingsPrefix() + "text");
			
			if(fileName != null){
				File file = getFileHandle(fileName);
				if(file.exists()){
					text.setText(fileName);
				}
		
			}
		}
		{
			// Browse Button Functionality
			Button btnBrowse = new Button(container, SWT.NONE);
			btnBrowse.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					// Create a new file dialog
					FileDialog fd =  new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
					
					// Set the title
					fd.setText(fileDialogTitle);
					
					// Set the default directory
					String filterPath = preferences.getStringSetting(getPageSettingsPrefix() + "lastFilterPath");
			//		String filterPath = null;
					
					if(filterPath != null){
						
						fd.setFilterPath(filterPath);
					}else{
						
						fd.setFilterPath("C:/");
						
					}
					
					// Set the filtered file extension
					if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {					
				        
						String[] filterExt = fileExtenstionList.toArray(new String[0]);
				        String[] filterStr = new String[1];
				        filterStr[0] = "";
						for (int i = 0; i < filterExt.length; i++ ) {
							
//							filterExt[i] = "*." + filterExt[i];
							filterStr[0]  += "*." + filterExt[i];
							if( i != filterExt.length -1 ) {
								filterStr[0]  += ";";
							}
						}
						
				        fd.setFilterExtensions(filterStr);
//				        fd.setFilterExtensions(filterExt);
				        
					}
			        
			        // Get the selected path string
			        selected = fd.open();
			        
			        if(FileHelperUtil.doesFileExist(selected)){
			        	
			        }
			        
			        if ( selected != null ) {
			            
			        	preferences.saveStringSetting(getPageSettingsPrefix() + "lastFilterPath", fd.getFilterPath());

			    		if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {
			    		
			    			boolean validFileExtension = false;
			    			
			    			if ( fd.getFilterIndex() >= 0 ) {
			    				
			    				validFileExtension = true;
			    				
//			    				String selectedExtension = fileExtenstionList.get(fd.getFilterIndex());
//			    				
//			    				if ( ! selected.endsWith("." + selectedExtension)) {
//			    					
//			    					text.setText(selected + "." + selectedExtension );
//			    					
//			    				} else {
			    					
			    					text.setText(selected);
			    					
//			    				}			    				
		    						    				
			    			} else {
			    				
			    				for (String fileExtension : fileExtenstionList ) {
				    				
				    				if ( selected.endsWith("." + fileExtension)) {
				    					
				    					validFileExtension = true;
				    					
				    					text.setText(selected);
				    					
				    					break;
				    				}
			    				}
			    				
			    			}			    			
			    			
			    			if ( validFileExtension ) {

			    	//			setPageComplete(pageComplete());
			    				
			    				
			    				
			    			} else {
			    				
			    			//	setPageComplete(false);
			    				
			    			}
			    			
			    		} else {
			    			//i don't think this should ever happen, so I set it 
			    			// to false.  KRM-1/21/2010
			    		//	setPageComplete(false);
			    			
			    		}
			        		
			        			        	
			        }

				}
			});
			btnBrowse.setText("Browse...");
		}

	//	setPageComplete(pageComplete());
		
		
		
		// Existing projects functionality - setting items in list
		
		// update UI
		new Label(container, SWT.NONE); 
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			lblExistingProjects = new Label(container, SWT.LEFT);
			lblExistingProjects.setText("Existing Projects :");
			lblExistingProjects.setEnabled(true);
		}
		{
			combo = new Combo(container, SWT.READ_ONLY);
			combo.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				//	validateSelections();
					
				}
			});
			
			// TTN-2662 - Update OK button status when project combo box is updated
			combo.addModifyListener(new ModifyListener() {

				public void modifyText(ModifyEvent e) {

					updateOkButton();

				}

			});

			combo.setItems(itemList);
			combo.setEnabled(false);
		//	combo.setBounds(151, 50, 203, 23);
			combo.setEnabled(true);
		}
		
	   // update UI
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			final Label lblDeployServer = new Label(container,SWT.None);
		
			lblDeployServer.setText("Deploy Rule Sets to Default Server      	");
		
			final Button btnDeployServer = new Button(container, SWT.CHECK);
			btnDeployServer.setSelection(false);
			btnDeployServer.setEnabled(false);
			btnDeployServer.setBounds(300,400,80,80);
			combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				try {
					
					if(isDefaultServerForSelectedProjectRunning()){
						btnDeployServer.setEnabled(true);
						lblDeployServer.setText("Deploy Rule Sets to ("+getDefaultServerForSelectedProject()+ ") ");
						
					}
					else{
						btnDeployServer.setEnabled(false);
						lblDeployServer.setText("Deploy Rule Sets to Default Server");
						
					}
					
				} catch (PafServerNotFound e1) {
					// TODO Auto-generated catch block
					logger.error(e1.getMessage());
				}
				
			}});
		
		btnDeployServer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				setIsDeployToServer(btnDeployServer.getSelection());
				
			}});
		
		}
		
		
		return container;
		
		
		
	}
	
	
	private boolean pageComplete(){
		
		if(text != null && text.getText().trim().length() > 0 && validateFile()){
			
			setSelectedFileName(text.getText().trim());
			preferences.saveStringSetting(getPageSettingsPrefix() + "text", text.getText().trim());
				return true;
			
		}
		
		return false;
		
	}
	
	private String getPageSettingsPrefix(){
	//	return getName() + ".";
		return ".";
	}
	
	
	   protected boolean validateFile() {
	        //IWorkspace workspace = ResourcesPlugin.getWorkspace();

	        String projectFieldContents = getTemplateNameFieldValue();
	        if (projectFieldContents.equals("")) { //$NON-NLS-1$
	    //        setErrorMessage("Export filename is blank.");
	            return false;
	        }


	        File handle = getFileHandle(projectFieldContents);
	        if (!handle.exists()) {
	        ////    setErrorMessage("Invalid file.  File does not exists.");
	            return false;
	        }
	                

	    //    setErrorMessage(null);
	    //    setMessage(null);
	        return true;
	    }
	   
	   private String getTemplateNameFieldValue() {
	        if (text == null) {
				return ""; //$NON-NLS-1$
			}

	        return text.getText().trim();
	    }
	 private File getFileHandle(String pathFileName) {
	    	
	    	//get file to deploy
	    	File file = new File(pathFileName);
	    	
	        return file;
	    }
	 public String getSelectedFileName() {
			return SelectedFileName;
		}

		public void setSelectedFileName(String selectedFileName) {
			SelectedFileName = selectedFileName;
		}
		
		
		// Method that is called after the OK button is pressed. 
		protected void buttonPressed(int buttonId) {
			if (buttonId == IDialogConstants.OK_ID) {

				// Import the Rule sets from the file
				importFromFileSet();
				
				// deploy to server
				if(isDeployToServer()){
					try {
						deployToServer();
					} catch (PafServerNotFound e) {
						// TODO Auto-generated catch block
						logger.info("Server not found");
					}
				}
				
			}
			super.buttonPressed(buttonId);
		}
		
		
		// Method to import the rule sets from the Excel file
		public void importFromFileSet(){
			
			// Get the project to import into
			IProject project = getProject(false,combo.getItem(combo.getSelectionIndex()));
			 final ImportProjectFromFileset importProjectFromFileset = new ImportProjectFromFileset(window, selected, project, getProjectElementIdSet(), true, ProjectSourceType.Excel);
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
					new Runnable() {
										
				public void run() {
				
					importProjectFromFileset.run();
					
				}
				
			});
			
		}
		
		// Method to deploy/ upload the updated project to the default server.
		public void deployToServer() throws PafServerNotFound{
			
			IProject project = getProject(false,combo.getItem(combo.getSelectionIndex()));
			PafServer server = PafProjectUtil.getProjectServer(project);
			// Check if the server is running
			if(server!=null && server.isTheServerRunning()){
			final UploadRuleSetToServerAction uploadRuleSetToServerAction = new UploadRuleSetToServerAction(window, server, project);
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
					new Runnable() {
							 			
				public void run() {
				
					uploadRuleSetToServerAction.run();
					
				}
				
			});
			}
			
		}


		private IProject getProject(boolean createNewProject, String projectName){
			IProject project = null;
			
			if(!createNewProject){
				
				project = ViewerUtil.getCurrentProjects().get(projectName);
					
			}else{
				
				//a new pace project.
				try {
					
					//create a new pace project.
					project = ViewerUtil.createNewProject(projectName);

				} catch (CoreException e) {
					
					logger.error("Cannot create project: " + projectName  + " : " +  e.getMessage());
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.WARNING);
				}  
				
			}

			
			return project;
		}
		
		// Filter the imported elements to just the rule sets
		public ProjectElementId[] getProjectElementIdSet(){
			
			ProjectElementId[] projectElementIdSet = new ProjectElementId[1];
			
			projectElementIdSet[0] = ProjectElementId.RuleSets;
			
			
			return projectElementIdSet;
		}
		
		public boolean isDeployToServer(){
			return isDeployToServer;
		}
		
		public void setIsDeployToServer(boolean isDeploy){
			isDeployToServer = isDeploy;
		}
		
		// Check if the default server for the selected project in the dropdown is running
		public boolean isDefaultServerForSelectedProjectRunning() throws PafServerNotFound{
			try{
				
			
			IProject project = getProject(false,combo.getItem(combo.getSelectionIndex()));
			PafServer server = PafProjectUtil.getProjectServer(project);
			if(server!=null)
				return server.isTheServerRunning();
			else
				return false;
			}catch(PafServerNotFound e){
				logger.error(e.getMessage());
				return false;
			}
			
		}
		
		
		// Get the default server name for the selected project.
		public String getDefaultServerForSelectedProject() throws PafServerNotFound{
			try{
				
			
			IProject project = getProject(false,combo.getItem(combo.getSelectionIndex()));
			PafServer server = PafProjectUtil.getProjectServer(project);

			if(server!=null)
				return server.getName();
			else
				return "";
			}catch(PafServerNotFound e){
				logger.error(e.getMessage());
				return "";
			}
			
		}
		
		private void updateOkButton(){
			
			// TTN-2662 - Removed infinite loop code and added null check
			// on OK button.
			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (selected == null || selected.isEmpty() || combo.getText().trim().equals("")) {
					okButton.setEnabled(false);
				} else {
					okButton.setEnabled(true);
				}
			}
		}
		
		@Override
		public void create() {
			super.create();
			initializeForm();
		}
		
		private void initializeForm() {
			
			if (combo.getText().trim().equals("") || selected == "" ){
				getButton(IDialogConstants.OK_ID).setEnabled(false);
			}
		
		}
}

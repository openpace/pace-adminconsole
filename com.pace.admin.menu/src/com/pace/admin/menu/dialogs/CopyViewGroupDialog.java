/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CopyViewGroupDialog extends Dialog {

	private Text viewGroupCopyTo;
	private Text viewGroupCopyFromText;
	
	private String viewGroupCopyFromName;
	
	private String newViewGroupName;
	
	private String[] viewGroupNames;
	
	private Button okButton = null;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public CopyViewGroupDialog(Shell parentShell, String viewGroupCopyFromName, String[] viewGroupNames) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.viewGroupCopyFromName = viewGroupCopyFromName;
		this.viewGroupNames = viewGroupNames;

	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);

		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		final Label viewGroupToCopyLabel = new Label(container, SWT.NONE);
		viewGroupToCopyLabel.setText("View Group Copy From:");

		viewGroupCopyFromText = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		viewGroupCopyFromText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( viewGroupCopyFromName != null ) {
			viewGroupCopyFromText.setText(viewGroupCopyFromName);
		}

		final Label viewGroupCopyToLabel = new Label(container, SWT.NONE);
		viewGroupCopyToLabel.setText("View Group Copy To:");

		viewGroupCopyTo = new Text(container, SWT.BORDER);
		viewGroupCopyTo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( viewGroupCopyFromName != null ) {
			viewGroupCopyTo.setText(viewGroupCopyFromName);
		}
		
		viewGroupCopyTo.setFocus();
		viewGroupCopyTo.selectAll();
		
		addListeners();
						
		return container;
	}

	
	private void addListeners() {
		
		viewGroupCopyTo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
			
					boolean validEntry = true;
				
					String newViewGroupName = viewGroupCopyTo.getText().trim();
					
					if ( newViewGroupName.equals("")) {
						validEntry = false;
					}
					
					if ( validEntry &&  viewGroupNames != null ) {
						
						for ( String existingViewGroupName : viewGroupNames ) {
						
							if ( newViewGroupName.equalsIgnoreCase(existingViewGroupName))	{
								
								validEntry = false;
								break;
							}
							
						}
						
					}					
					
					getButton(IDialogConstants.OK_ID).setEnabled(validEntry);
				
			}
		});		
		
	}
	
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(340, 166);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Copy View Group");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		newViewGroupName = viewGroupCopyTo.getText().trim();
		
		super.okPressed();
		
	}

	/**
	 * @return Returns the newViewGroupName.
	 */
	public String getNewViewGroupName() {
		return newViewGroupName;
	}

	
	
}

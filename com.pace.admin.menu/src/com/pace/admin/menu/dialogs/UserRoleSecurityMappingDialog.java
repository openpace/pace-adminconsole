/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class UserRoleSecurityMappingDialog extends Dialog {

	private List list_2;
	private List list;
	private Combo usersCombo;
	public UserRoleSecurityMappingDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		final Label usersLabel = new Label(container, SWT.NONE);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 85);
		formData.right = new FormAttachment(0, 118);
		formData.top = new FormAttachment(0, 65);
		formData.left = new FormAttachment(0, 78);
		usersLabel.setLayoutData(formData);
		usersLabel.setText("Users:");

		usersCombo = new Combo(container, SWT.NONE);
		final FormData formData_1 = new FormData();
		formData_1.top = new FormAttachment(0, 65);
		formData_1.bottom = new FormAttachment(0, 86);
		formData_1.right = new FormAttachment(0, 232);
		formData_1.left = new FormAttachment(0, 117);
		usersCombo.setLayoutData(formData_1);

		list = new List(container, SWT.BORDER);
		final FormData formData_2 = new FormData();
		formData_2.left = new FormAttachment(0, 78);
		formData_2.bottom = new FormAttachment(0, 310);
		formData_2.right = new FormAttachment(0, 243);
		formData_2.top = new FormAttachment(0, 120);
		list.setLayoutData(formData_2);
		list.setItems(new String[] { "Role 1", "Role 3", "Role 4" });

		list_2 = new List(container, SWT.BORDER);
		final FormData formData_2_1 = new FormData();
		formData_2_1.top = new FormAttachment(0, 120);
		formData_2_1.bottom = new FormAttachment(0, 310);
		formData_2_1.right = new FormAttachment(0, 478);
		formData_2_1.left = new FormAttachment(0, 313);
		list_2.setLayoutData(formData_2_1);
		list_2.setItems(new String[] { "Role 2"});

		final Label availableRolesLabel = new Label(container, SWT.NONE);
		final FormData formData_3 = new FormData();
		formData_3.left = new FormAttachment(0, 78);
		formData_3.bottom = new FormAttachment(0, 118);
		formData_3.right = new FormAttachment(0, 153);
		formData_3.top = new FormAttachment(0, 98);
		availableRolesLabel.setLayoutData(formData_3);
		availableRolesLabel.setText("Available Roles:");

		final Label selectedRolesLabel = new Label(container, SWT.NONE);
		final FormData formData_3_1 = new FormData();
		formData_3_1.top = new FormAttachment(0, 98);
		formData_3_1.left = new FormAttachment(0, 313);
		formData_3_1.bottom = new FormAttachment(0, 118);
		formData_3_1.right = new FormAttachment(0, 388);
		selectedRolesLabel.setLayoutData(formData_3_1);
		selectedRolesLabel.setText("Selected Roles:");

		final Button configureSecurityButton = new Button(container, SWT.NONE);
		final FormData formData_4 = new FormData();
		formData_4.bottom = new FormAttachment(0, 345);
		formData_4.right = new FormAttachment(0, 420);
		formData_4.top = new FormAttachment(0, 320);
		formData_4.left = new FormAttachment(list_2, 0, SWT.LEFT);
		configureSecurityButton.setLayoutData(formData_4);
		configureSecurityButton.setText("Configure Security");

		final Button button = new Button(container, SWT.ARROW | SWT.RIGHT);
		final FormData formData_5 = new FormData();
		formData_5.bottom = new FormAttachment(0, 213);
		formData_5.right = new FormAttachment(0, 293);
		formData_5.top = new FormAttachment(0, 198);
		formData_5.left = new FormAttachment(0, 262);
		button.setLayoutData(formData_5);
		button.setText("button");

		final Button button_1 = new Button(container, SWT.ARROW | SWT.LEFT);
		final FormData formData_5_1 = new FormData();
		formData_5_1.left = new FormAttachment(0, 262);
		formData_5_1.bottom = new FormAttachment(0, 236);
		formData_5_1.top = new FormAttachment(0, 221);
		formData_5_1.right = new FormAttachment(0, 293);
		button_1.setLayoutData(formData_5_1);
		button_1.setText("button");
		//
		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(564, 463);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("User Role Mapping");
	}

}

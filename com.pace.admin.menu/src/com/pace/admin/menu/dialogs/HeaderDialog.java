/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.model.managers.GlobalStyleModelManager;
import com.pace.base.view.PafViewHeader;

public class HeaderDialog extends Dialog {

	private Combo globalStyleCombo;

	private Text headerText;

	private PafViewHeader header = null;
	
	private GlobalStyleModelManager globalSytlesManager;

	@Override
	protected void okPressed() {
		
		//create new header
		header = new PafViewHeader();
		header.setLabel(headerText.getText().trim());
		header.setGlobalStyleName(globalStyleCombo.getText());

		super.okPressed();
		
	}

	public HeaderDialog(Shell parentShell, IProject project) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		globalSytlesManager = new GlobalStyleModelManager(project);
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		final Composite composite = new Composite(container, SWT.NONE);
		final GridData gridData = new GridData(SWT.CENTER,
				SWT.CENTER, true, true);
		gridData.heightHint = 97;
		gridData.widthHint = 373;
		composite.setLayoutData(gridData);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		composite.setLayout(gridLayout);

		final Label headerTextLabel = new Label(composite, SWT.NONE);
		headerTextLabel.setLayoutData(new GridData(66, SWT.DEFAULT));
		headerTextLabel.setText("Header Text");

		headerText = new Text(composite, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.WRAP);
		headerText.setTextLimit(300);
		final GridData gridData_1 = new GridData(SWT.FILL,
				SWT.CENTER, true, false);
		gridData_1.heightHint = 47;
		gridData_1.widthHint = 164;
		headerText.setLayoutData(gridData_1);
		headerText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				updateOkButton();

			}

		});

		final Label globalStyleLabel = new Label(composite, SWT.NONE);
		globalStyleLabel.setText("Global Style");

		globalStyleCombo = new Combo(composite, SWT.READ_ONLY);
		globalStyleCombo.setItems(new String[] {"global 1", "global 2", "global 3"});
		globalStyleCombo.setLayoutData(new GridData(96, SWT.DEFAULT));
		globalStyleCombo.setItems(globalSytlesManager.getKeys());
		globalStyleCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				updateOkButton();

			}

		});

		return container;
	}

	private void updateOkButton() {

		if (headerText.getText().trim().equals("")
				|| globalStyleCombo.getText().trim().equals("")) {

			getButton(IDialogConstants.OK_ID).setEnabled(false);

		} else {

			getButton(IDialogConstants.OK_ID).setEnabled(true);

		}

	}

	@Override
	public void create() {
		super.create();
		initializeForm();
	}

	private void initializeForm() {

		if ( header == null ) {
			getButton(IDialogConstants.OK_ID).setEnabled(false);
		} else {
			
			headerText.setText(header.getLabel());
			if ( header.getGlobalStyleName() != null ) {
				globalStyleCombo.setText(header.getGlobalStyleName());
			}
		}
		
	}

	protected Point getInitialSize() {
		return new Point(447, 208);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Header");
	}

	public PafViewHeader getViewHeader() {
		return header;
	}
	
	public void setViewHeader(PafViewHeader header) {
		this.header = header;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ProjectColorsDialog extends TitleAreaDialog {

	private Label label_5;
	private Button resetColorsButton;
	private Text nonPlannableProtectedColorText;
	private Label label_4;
	private Label forwardPlannableProtectedColor;
	private Label label_3;
	private Label nonPlannableProtectedColor;
	private Label label_2;
	private Label systemLockColor;
	private Label label_1;
	private Label protectedLockColor;
	private Label label;
	private Label userLockColorLabel;
	private Text forwardPlannableProtectedColorText;
	private Combo forwardPlannableProtectedColorCombo;
	private Combo nonPlannableProtectedColorCombo;
	private Text systemLockColorText;
	private Combo systemLockColorCombo;
	private Text protectedLockColorText;
	private Combo protectedLockColorCombo;
	private Text userLockColorText;
	private Combo userLockColorCombo;
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public ProjectColorsDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginBottom = 20;
		gridLayout.marginTop = 20;
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		userLockColorLabel = new Label(container, SWT.NONE);
		userLockColorLabel.setText("User Lock Color");
		label = new Label(container, SWT.NONE);

		userLockColorCombo = new Combo(container, SWT.READ_ONLY);
		userLockColorCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		userLockColorText = new Text(container, SWT.BORDER);
		userLockColorText.setDoubleClickEnabled(false);
		userLockColorText.setTextLimit(8);
		userLockColorText.setEditable(false);
		final GridData gridData = new GridData(18, SWT.DEFAULT);
		gridData.minimumWidth = 15;
		userLockColorText.setLayoutData(gridData);

		protectedLockColor = new Label(container, SWT.NONE);
		protectedLockColor.setText("Protected Lock Color");
		label_1 = new Label(container, SWT.NONE);

		protectedLockColorCombo = new Combo(container, SWT.READ_ONLY);
		protectedLockColorCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		protectedLockColorText = new Text(container, SWT.BORDER);
		protectedLockColorText.setDoubleClickEnabled(false);
		protectedLockColorText.setTextLimit(8);
		protectedLockColorText.setEditable(false);
		final GridData gridData_1 = new GridData(18, SWT.DEFAULT);
		gridData_1.minimumWidth = 15;
		protectedLockColorText.setLayoutData(gridData_1);

		systemLockColor = new Label(container, SWT.NONE);
		systemLockColor.setText("System Lock Color");
		label_2 = new Label(container, SWT.NONE);

		systemLockColorCombo = new Combo(container, SWT.READ_ONLY);
		systemLockColorCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		systemLockColorText = new Text(container, SWT.BORDER);
		systemLockColorText.setDoubleClickEnabled(false);
		systemLockColorText.setTextLimit(8);
		systemLockColorText.setEditable(false);
		final GridData gridData_1_1 = new GridData(18, SWT.DEFAULT);
		gridData_1_1.minimumWidth = 15;
		systemLockColorText.setLayoutData(gridData_1_1);

		nonPlannableProtectedColor = new Label(container, SWT.NONE);
		nonPlannableProtectedColor.setText("Non-Plannable Protected Color");
		label_3 = new Label(container, SWT.NONE);

		nonPlannableProtectedColorCombo = new Combo(container, SWT.READ_ONLY);
		nonPlannableProtectedColorCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		nonPlannableProtectedColorText = new Text(container, SWT.BORDER);
		nonPlannableProtectedColorText.setTextLimit(8);
		nonPlannableProtectedColorText.setEditable(false);
		nonPlannableProtectedColorText.setDoubleClickEnabled(false);
		final GridData gridData_1_1_1_1_1 = new GridData(18, SWT.DEFAULT);
		gridData_1_1_1_1_1.minimumWidth = 15;
		nonPlannableProtectedColorText.setLayoutData(gridData_1_1_1_1_1);

		forwardPlannableProtectedColor = new Label(container, SWT.NONE);
		forwardPlannableProtectedColor.setText("Forward Plannable Protected Color");
		label_4 = new Label(container, SWT.NONE);

		forwardPlannableProtectedColorCombo = new Combo(container, SWT.READ_ONLY);
		forwardPlannableProtectedColorCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		forwardPlannableProtectedColorText = new Text(container, SWT.BORDER);
		forwardPlannableProtectedColorText.setTextLimit(8);
		forwardPlannableProtectedColorText.setEditable(false);
		forwardPlannableProtectedColorText.setDoubleClickEnabled(false);
		final GridData gridData_1_1_1_1 = new GridData(18, SWT.DEFAULT);
		gridData_1_1_1_1.minimumWidth = 15;
		forwardPlannableProtectedColorText.setLayoutData(gridData_1_1_1_1);

		resetColorsButton = new Button(container, SWT.NONE);
		resetColorsButton.setText("Reset Colors");
		label_5 = new Label(container, SWT.NONE);
		container.setTabList(new Control[] {userLockColorCombo, protectedLockColorCombo, systemLockColorCombo, nonPlannableProtectedColorCombo, forwardPlannableProtectedColorCombo, resetColorsButton});
		setTitle("Project Colors");
		setMessage("Assign each property a color.");
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(353, 464);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Project Color Settings");
	}

}

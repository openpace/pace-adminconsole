/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs.internal;

import com.pace.admin.global.constants.SecurityMemberConsants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.base.PafBaseConstants;

public class UserSelectionSpecificationMember {
	
	public static final int UNDEFINED_TREE_LEVEL_OR_GEN = -1;
	public final static String AT_TAG = "@";
	public final static String ICHILDREN = "ICHILDREN";
	public final static String CHILDREN = "CHILDREN";
	public final static String IDESCENDANTS = "IDESC";
	public final static String DESCENDANTS = "DESC";
	public final static String LEVEL = "LEVEL";
	public final static String GENERATION = "GEN";
	
	private String dimensionName;
	private String member; 
	private PaceTreeNodeSelectionType selectionType = PaceTreeNodeSelectionType.Selection;
	private LevelGenerationType levelGenType;
	private int levelGenNumber = UNDEFINED_TREE_LEVEL_OR_GEN;
	
	public UserSelectionSpecificationMember(
			String dimensionName, 
			String member, 
			PaceTreeNodeSelectionType selectionType, 
			LevelGenerationType levelGenType,
			int levelGenNumber ) {
		
		this.dimensionName = dimensionName;
		this.member = member;
		this.selectionType = selectionType;
		this.levelGenType = levelGenType;
		this.levelGenNumber = levelGenNumber;
	}
	
	public UserSelectionSpecificationMember(String dimensionName, String fullSpecMember ) {
		this.dimensionName = dimensionName;				
		//if "@"
		if ( fullSpecMember.contains(AT_TAG) && ! fullSpecMember.contains(PafBaseConstants.MEMBERLIST_TOKEN) ) {
			member = stripSelectionType(fullSpecMember);
			
			selectionType = findPaceTreeNodeSelectionType(fullSpecMember);
			
			String[] memberAr = member.split(",");
			
			if ( isKindOfDescendantLevelOrGenSelectionType(selectionType) && memberAr.length > 1) {
				
				member = memberAr[0];
								
				if ( memberAr.length == 1 ) {
				
					setLevelGenNumber(PaceTreeNodeProperties.UNDEFINED_TREE_LEVEL_OR_GEN);
					
				} else if ( memberAr.length > 1 ) {

					String levelGenWithNumStr = memberAr[1].trim();
					String levelGen = levelGenWithNumStr.substring(0, 1);
					
					int numberLevelGenLength = levelGenWithNumStr.length();
					
					if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.LEVEL_IDNT)) {
						
						levelGenType = LevelGenerationType.Level;
						
					} else if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.GENERATION_IDNT)) {
						
						levelGenType = LevelGenerationType.Generation;
					}
					else {
						if ( selectionType.equals(PaceTreeNodeSelectionType.Generation) ) {
							levelGenType = LevelGenerationType.Generation;
						}
						else if( selectionType.equals(PaceTreeNodeSelectionType.Level) ) {
								levelGenType = LevelGenerationType.Level;
						}
					}
					String levelGenNum = levelGenWithNumStr.substring(1, numberLevelGenLength);
					
					if ( levelGen.matches("[0123456789]+")) {
						
						setLevelGenNumber(Integer.parseInt(levelGen));
						
					}
					
					if ( levelGenNum.matches("[0123456789]+")) {
						
						setLevelGenNumber(Integer.parseInt(levelGenNum));
						
					}

				}
				
			}
			else {//only selection
				levelGenNumber = 0;
			}
		}  
		else {
			member = fullSpecMember;
			selectionType = PaceTreeNodeSelectionType.Selection;
			levelGenNumber = 0;
		}
		
	}
	/**
	 * 
	 * Checks if selection type is any type of Level
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is Level
	 */		
	public static boolean isKindOfLevelSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return selectionType.equals(PaceTreeNodeSelectionType.Level) || selectionType.equals(PaceTreeNodeSelectionType.ILevel);
		
	}
	/**
	 * 
	 * Checks if selection type is any type of Level
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is Level
	 */		
	public static boolean isKindOfLevelGenerationSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return selectionType.equals(PaceTreeNodeSelectionType.LevelGen);
		
	}

	/**
	 * 
	 * Checks if selection type is any type of Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is Generation
	 */		
	public static boolean isKindOfGenerationSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return selectionType.equals(PaceTreeNodeSelectionType.Generation);
		
	}
	
	/**
	 * 
	 * Checks if selection type is any type of Descendant or IDescendant
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is any type of Descendant or IDescendant
	 */	
	public static boolean isKindOfDescendantSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return 	selectionType.equals(PaceTreeNodeSelectionType.IDescendants) || 
				selectionType.equals(PaceTreeNodeSelectionType.IDescLevel) ||
				selectionType.equals(PaceTreeNodeSelectionType.IDescGen) ||
				selectionType.equals(PaceTreeNodeSelectionType.Descendants) ||
				selectionType.equals(PaceTreeNodeSelectionType.DescLevel) ||
				selectionType.equals(PaceTreeNodeSelectionType.DescGen) ;
	}

	/**
	 * 
	 * Checks if selection type is IDesc, Desc, Level or Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if is IDesc, Desc, Level or Generation
	 */
	public static boolean isKindOfDescendantLevelOrGenSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return isKindOfDescendantSelectionType(selectionType) || 
				isKindOfLevelSelectionType(selectionType) || 
				isKindOfGenerationSelectionType(selectionType) ||
				isKindOfLevelGenerationSelectionType(selectionType);
		
	}

	/**
	 * 
	 * Checks if selection type is Children, IChildren, IDesc, Desc, ILevel, Level or Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if is Children, IChildren, IDesc, Desc, ILevel, Level or Generation
	 */
	public static boolean isKindOfChildrenDescLevelOrGenSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return 
				selectionType.equals(PaceTreeNodeSelectionType.Children) ||
				selectionType.equals(PaceTreeNodeSelectionType.IChildren) || 
				isKindOfDescendantLevelOrGenSelectionType(selectionType);
		
	}
	
	public static PaceTreeNodeSelectionType findPaceTreeNodeSelectionType(String member) {
		
		PaceTreeNodeSelectionType memberSelectionType = PaceTreeNodeSelectionType.Selection;

		if( member.contains("(")) {

			String memberType = member.substring(member.indexOf(AT_TAG)+1, member.indexOf("("));
		
			if ( memberType.equals(ICHILDREN)) {
				memberSelectionType = PaceTreeNodeSelectionType.IChildren;
			} 
			
			else if ( memberType.equals(CHILDREN)) {
				memberSelectionType = PaceTreeNodeSelectionType.Children;
			} 
			
			else if ( memberType.equals(IDESCENDANTS)) {
				
				if ( member.matches(".*, " + SecurityMemberConsants.GENERATION_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.IDescGen;
					
				} else if (member.matches(".*, " + SecurityMemberConsants.LEVEL_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.IDescLevel;
					
				} else {
				
					memberSelectionType = PaceTreeNodeSelectionType.IDescendants;
				
				}
				
			} 
			
			else if ( memberType.equals(DESCENDANTS)) {
				
				if ( member.matches(".*, " + SecurityMemberConsants.GENERATION_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.DescGen;
					
				} else if (member.matches(".*, " + SecurityMemberConsants.LEVEL_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.DescLevel;
					
				} else {
				
					memberSelectionType = PaceTreeNodeSelectionType.Descendants;
				
				}
				
			} 
			
			else if ( memberType.equals(LEVEL)) {
				if( member.matches(".*, " + "[0123456789]+\\)")) {
					memberSelectionType = PaceTreeNodeSelectionType.Level;
				}
				else {
					memberSelectionType = PaceTreeNodeSelectionType.ILevel;
				}
			} 
			
			else if ( memberType.equals(GENERATION)) {
				memberSelectionType = PaceTreeNodeSelectionType.Generation;			
			} 
			
		}
		
		return memberSelectionType;
	}
	
	/**
	 * 
	 * Removes selection type from member
	 *
	 * @param member member to remove selection type
	 * @return member without selection type
	 */
	private static String stripSelectionType(String member) {
		
		int index1 = member.indexOf("(");
		int index2 = member.lastIndexOf(")");
		
		if ( index1 == -1 || index2 == -1) {
			return member;
		}
		
		return member.substring(++index1, index2);
		
	}
	/**
	 * @return the dimensionName
	 */
	public String getDimensionName() {
		return dimensionName;
	}

	/**
	 * @param dimensionName the dimensionName to set
	 */
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	/**
	 * @return the levelGenNumber
	 */
	public int getLevelGenNumber() {
		return levelGenNumber;
	}

	/**
	 * @param levelGenNumber the levelGenNumber to set
	 */
	public void setLevelGenNumber(int levelGenNumber) {
		this.levelGenNumber = levelGenNumber;
	}

	/**
	 * @return the member
	 */
	public String getMember() {
		return member;
	}

	/**
	 * @param member the member to set
	 */
	public void setMember(String member) {
		this.member = member;
	}

	/**
	 * @return the selectionType
	 */
	public PaceTreeNodeSelectionType getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(PaceTreeNodeSelectionType selectionType) {
		this.selectionType = selectionType;
	}

	public LevelGenerationType getLevelGenType() {
		return levelGenType;
	}

	public void setLevelGenType(LevelGenerationType levelGenType) {
		this.levelGenType = levelGenType;
	}

	public String getMemberWithSelection() {

		String memberWithSelection = null;

		switch (selectionType) {

			//case IChild:
			case IChildren:
				memberWithSelection = AT_TAG + ICHILDREN + "(" + getMemberWithLevelGen() + ")";
				break;
			//case Child:
			case Children:
				memberWithSelection = AT_TAG + CHILDREN + "(" + getMemberWithLevelGen() + ")";
				break;
			//case IDesc:
			case IDescendants:
				memberWithSelection = AT_TAG + IDESCENDANTS + "(" + getMemberWithLevelGen() + ")";
				break;
			//case Desc:
			case Descendants:
				memberWithSelection = AT_TAG + DESCENDANTS + "(" + getMemberWithLevelGen() + ")";
				break;
			case Level:
				memberWithSelection = AT_TAG + LEVEL + "(" + getMemberWithLevelGen() + ")";
				break;
			case Generation:
				memberWithSelection = AT_TAG + GENERATION + "(" + getMemberWithLevelGen() + ")";
				break;
			case LevelGen:
				switch(levelGenType) {
					case Level:
						memberWithSelection = AT_TAG + LEVEL + "(" + getMemberWithLevelGen() + ")";
						break;
					case Generation:
						memberWithSelection = AT_TAG + GENERATION + "(" + getMemberWithLevelGen() + ")";
						break;
					default:
						memberWithSelection = getMemberWithLevelGen();
						break;
				}
				break;		
			default:
				memberWithSelection = getMemberWithLevelGen();
				break;
		}

		return memberWithSelection;

	}


	private String getMemberWithLevelGen() {
		
		String memberWithUserSelection = member;
		
		//handle level differently
		if ( isKindOfDescendantLevelOrGenSelectionType(selectionType) ) {
		
			switch(selectionType) {
				case Level:
				case Generation:
				case LevelGen:
					switch(levelGenType) {
						case Level:
						case Generation:
							memberWithUserSelection = member +", " + levelGenNumber;
							break;
					}
					break;
					
				case IDescendants:
				case Descendants:
					switch(levelGenType) {
						case Level:
							memberWithUserSelection = member +", " + SecurityMemberConsants.LEVEL_IDNT + levelGenNumber;
							break;
						case Generation:
							memberWithUserSelection = member + ", " + SecurityMemberConsants.GENERATION_IDNT + levelGenNumber;
							break;	
						}
					break;
			}
		} 
		return memberWithUserSelection;
	} 	
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getMemberWithSelection();
	}
	
	
	public boolean equals(Object obj) {
		UserSelectionSpecificationMember theOther = (UserSelectionSpecificationMember)obj;
		if( ! this.dimensionName.equals(theOther.dimensionName ))
			return false;
		if( ! this.member.equals(theOther.member) )
				return false;
		return true;
	}
	
	public static void main(String[] args ) {
		
//		String[] ar = {"single_member", "@IDESC(totprod, L0)", "@IDESC(totprod, G2)", "@ICHILD(totprod, L4)", "@ICHILD(totprod, g3)"};
		
		String[] ar = {"@IDESC(totprod, L1)", "@DESC(totprod, G2)", "@LEVEL(totProd, 1)", "@GEN(totProd, 1)"};
		
		for (String s : ar)  {
		
			UserSelectionSpecificationMember securityMember = new UserSelectionSpecificationMember("Product", s);
		
			System.out.println(securityMember);
		}
		
		
		
		
	}

}

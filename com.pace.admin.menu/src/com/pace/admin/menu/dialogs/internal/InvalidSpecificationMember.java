/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs.internal;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class InvalidSpecificationMember {

	private String dimensionName;
	private String memberName;
	
	/**
	 * Default Constructor 
	 */
	public InvalidSpecificationMember() {
		
	}
	
	/**
	 * @param roleName
	 * @param dimensionName
	 * @param memberName
	 */
	public InvalidSpecificationMember(String dimensionName, String memberName) {
	
		this.dimensionName = dimensionName;
		this.memberName = memberName;
		
	}
	
	/**
	 * @return Returns the dimensionName.
	 */
	public String getDimensionName() {
		return dimensionName;
	}
	/**
	 * @param dimensionName The dimensionName to set.
	 */
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}
	/**
	 * @return Returns the memberName.
	 */
	public String getMemberName() {
		return memberName;
	}
	/**
	 * @param memberName The memberName to set.
	 */
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
}

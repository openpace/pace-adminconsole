/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs.internal;

import com.pace.admin.global.constants.SecurityMemberConsants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.util.StringUtil;


public class MemberSelection {

	private String[] members;

	private MemberSelectionType selectionType;

	private LevelGenerationType levelGenType;
	
	private int levelGenNumber;
	
	private boolean userSelection = false;

	public final static String ICHILDREN = "ICHILDREN";

	public final static String CHILDREN = "CHILDREN";

	public final static String IDESCENDANTS = "IDESC";

	public final static String DESCENDANTS = "DESC";

	public final static String LEVEL = "LEVEL";

	public final static String MEMBERS = "MEMBERS";

	public final static String USER_SELECTION = "USER_SEL";

	public final static String GENERATION = "GEN";
	
	private MemberSelection() {
		
	}
	
	public MemberSelection(String[] members, MemberSelectionType selectionType) {

		this.members = members;
	
		this.levelGenType = LevelGenerationType.Bottom;
		
		this.selectionType = selectionType;

	}

	public String[] getMembers() {
		return members;
	}

	public void setMembers(String[] members) {
		this.members = members;
	}

	public void addMember(String newMember) {

		if (members != null) {

			int numberOfMembers = members.length;

			String[] newMemberAr = new String[numberOfMembers + 1];

			int memberNdx = 0;

			for (String member : members) {

				newMemberAr[memberNdx++] = member;

			}

			newMemberAr[memberNdx] = newMember;

			members = newMemberAr;
		}

	}

	public MemberSelectionType getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(MemberSelectionType selectionType) {
		
		if ( this.selectionType != null && this.selectionType.equals(MemberSelectionType.Members) && ! selectionType.equals(MemberSelectionType.Members)) {
			
			String firstMember = members[0];
			
			members = new String[] { firstMember };
			
		}
		
		this.selectionType = selectionType;
	}

	
	private String getMemberListAsString() {
		
		StringBuffer strBuff = new StringBuffer();

		int memberNdx = 1;

		for (String member : members) {

			strBuff.append(member);

			if (memberNdx != members.length) {
				strBuff.append(", ");
			}

			memberNdx++;
		}

		return strBuff.toString();
		
	}
	
	private String getMemberWithUserSelection() {
		
		String memberWithUserSelection = getMemberListAsString();
		
		//if user selction is enabled, format string.  
		if ( userSelection ) {
			
			//handle level differently
			if ( isKindOfDescendantLevelOrGenSelectionType(selectionType) ) {

			
				memberWithUserSelection = "@USER_SEL(" + members[0] + "), " + levelGenNumber;
				
				if ( isKindOfLevelSelectionType(selectionType) || isKindOfGenerationSelectionType(selectionType) ) {
					
					memberWithUserSelection ="@USER_SEL(" + members[0] + "), " + levelGenNumber;
					
				} else if ( levelGenType == null || levelGenType.equals(LevelGenerationType.Bottom) && isKindOfDescendantSelectionType(selectionType)) {

					levelGenType = LevelGenerationType.Bottom;
					
					memberWithUserSelection = "@USER_SEL(" + members[0] + ")";
					
				} else if ( levelGenType.equals(LevelGenerationType.Level) && isKindOfDescendantSelectionType(selectionType)) {
					
					levelGenType = LevelGenerationType.Level;
					
					memberWithUserSelection = "@USER_SEL(" + members[0] + "), " + SecurityMemberConsants.LEVEL_IDNT + levelGenNumber;
					
				} else if ( levelGenType.equals(LevelGenerationType.Generation) && isKindOfDescendantSelectionType(selectionType)) {
					
					memberWithUserSelection ="@USER_SEL(" + members[0] + "), " + SecurityMemberConsants.GENERATION_IDNT + levelGenNumber;
					
				} 
	
				
			} else {
				
				memberWithUserSelection = "@USER_SEL(" + memberWithUserSelection + ")";
			}
								
		} else {
			
			if ( isKindOfDescendantLevelOrGenSelectionType(selectionType) ) {				
				
				if ( isKindOfLevelSelectionType(selectionType) || isKindOfGenerationSelectionType(selectionType) ) {
					
					memberWithUserSelection = members[0] + ", " + levelGenNumber;
					
				} else if ( levelGenType == null || levelGenType.equals(LevelGenerationType.Bottom) && isKindOfDescendantSelectionType(selectionType) ) {
					
					levelGenType = LevelGenerationType.Bottom;
					
					memberWithUserSelection = members[0];
					
				} else if ( levelGenType.equals(LevelGenerationType.Level) && isKindOfDescendantSelectionType(selectionType) ) {
					
					levelGenType = LevelGenerationType.Level;
					
					memberWithUserSelection = members[0] + ", " + SecurityMemberConsants.LEVEL_IDNT + levelGenNumber;
					
				} else if ( levelGenType.equals(LevelGenerationType.Generation) && isKindOfDescendantSelectionType(selectionType)) {
					
					memberWithUserSelection = members[0] + ", " + SecurityMemberConsants.GENERATION_IDNT + levelGenNumber;
					
				
				} 		
				
			}
						
		}
		
		
		return memberWithUserSelection;
	}
	
	public String getMemberWithSelection() {

		String memberWithSelection = null;

		switch (selectionType) {

		case Selection:
			memberWithSelection = getMemberWithUserSelection();
			break;
		case IChild:
		case IChildren:
			memberWithSelection = "@" + ICHILDREN + "(" + getMemberWithUserSelection() + ")";
			break;
		case Child:
		case Children:
			memberWithSelection = "@" + CHILDREN + "(" + getMemberWithUserSelection() + ")";
			break;
		case IDesc:
		case IDescendants:
			memberWithSelection = "@" + IDESCENDANTS + "(" + getMemberWithUserSelection() + ")";
			break;
		case Desc:
		case Descendants:
			memberWithSelection = "@" + DESCENDANTS + "(" + getMemberWithUserSelection() + ")";
			break;
		case Level:
			memberWithSelection = "@" + LEVEL + "(" + getMemberWithUserSelection() + ")";
			break;
		case Generation:
			memberWithSelection = "@" + GENERATION + "(" + getMemberWithUserSelection() + ")";
			break;		
		case Members:
			memberWithSelection = "@" + MEMBERS + "(" + getMemberWithUserSelection() + ")";
			break;
		default:
			memberWithSelection = getMemberWithUserSelection();
			break;
		}

		return memberWithSelection;

	}

	public String toString() {
		return getMemberWithSelection();
	}

	public static MemberSelection createMemberSelectionFromExistingMember(
			String member) {
		
		MemberSelection memberSelection = new MemberSelection();
		
		
		if ( member.contains("@") ) {
			
			if ( member.contains(USER_SELECTION)) {
				memberSelection.setUserSelection(true);
				member = stripUserSelection(member);
			} else {
				memberSelection.setUserSelection(false);			}
			
						
			MemberSelectionType memberSelectionType = findMemberSelectionType(member); 
			
			member = stripSelectionType(member);
			
			String[] memberAr = StringUtil.createStringArFromString(member);
						
			if ( isKindOfDescendantLevelOrGenSelectionType(memberSelectionType) && memberAr.length > 1) {
				
				String descMember = memberAr[0];
								
				if ( memberAr.length == 1 ) {
				
					memberSelection.setLevelGenType(LevelGenerationType.Bottom);
					memberSelection.setLevelGenNumber(0);
					
				} else if ( memberAr.length > 1 ) {

					String levelGenWithNumStr = memberAr[1];
					
					int levelGenLen = levelGenWithNumStr.length();
					
					String levelGen = levelGenWithNumStr.substring(0, 1);
					
					int numberLevelGenLength = levelGenWithNumStr.length();
					
					String levelGenNum = levelGenWithNumStr.substring(1, numberLevelGenLength);
					
					if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.LEVEL_IDNT)) {
						
						memberSelection.setLevelGenType(LevelGenerationType.Level);
						
					} else if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.GENERATION_IDNT)) {
						
						memberSelection.setLevelGenType(LevelGenerationType.Generation);
						
					} else if ( levelGen.matches("[0123456789]+")) {
						
						memberSelection.setLevelGenNumber(Integer.parseInt(levelGen));
						
					}
					
					if ( levelGenNum.matches("[0123456789]+")) {
						
						memberSelection.setLevelGenNumber(Integer.parseInt(levelGenNum));
						
					}
					
					memberAr = new String[] { descMember };
					
				}
							
			}						
			
			memberSelection.setMembers(memberAr);
			
			memberSelection.setSelectionType(memberSelectionType);
			
			
		} else {
			
			memberSelection.setSelectionType(MemberSelectionType.Selection);
			memberSelection.setLevelGenType(LevelGenerationType.Bottom);
			memberSelection.setLevelGenNumber(0);
			memberSelection.setUserSelection(false);
			memberSelection.setMembers(new String[] {member});

		}
		
		return memberSelection;
				
	}
	
	private static String stripSelectionType(String member) {
		
		int index1 = member.lastIndexOf("(");
		int index2 = member.indexOf(")");
		
		if ( index1 == -1 || index2 == -1) {
			return member;
		}
		
		return member.substring(++index1, index2);
		
	}

	private static String stripUserSelection(String member) {
		
		member = member.replace("@" + USER_SELECTION + "(", "");
					
		member = member.replaceFirst("\\)", "");
		
		return member;
		
	}

	public int numberOfMembers() {
		
		if ( members == null ) {
			return 0;
		}
		
		return members.length;
		
	}

	//remove last member from array
	public void removeMember() {

		if (members != null && members.length != 0) {

			int numberOfMembers = members.length;

			String[] newMemberAr = new String[numberOfMembers - 1];

			for (int i = 0; i < newMemberAr.length; i++) {

				newMemberAr[i] = members[i];

			}

			members = newMemberAr;

		}

	}

	public boolean isUserSelection() {
		return userSelection;
	}

	public void setUserSelection(boolean userSelection) {
		this.userSelection = userSelection;
	}
	
	
	public static MemberSelectionType findMemberSelectionType(String member) {
				
		MemberSelectionType memberSelectionType = null;
		
		if ( member.contains(ICHILDREN)) {
			memberSelectionType = MemberSelectionType.IChildren;
		} else if ( member.contains(CHILDREN)) {
			memberSelectionType = MemberSelectionType.Children;
		} else if ( member.contains(IDESCENDANTS)) {
			memberSelectionType = MemberSelectionType.IDesc;
		} else if ( member.contains(DESCENDANTS)) {
			memberSelectionType = MemberSelectionType.Desc;
		} else if ( member.contains(LEVEL)) {
			memberSelectionType = MemberSelectionType.Level;
		} else if ( member.contains(GENERATION)) {
			memberSelectionType = MemberSelectionType.Generation;			
		} else if ( member.contains(MEMBERS)) {
			memberSelectionType = MemberSelectionType.Members;
		} else {
			memberSelectionType = MemberSelectionType.Selection;
		}
		
		return memberSelectionType;
	}
	
	public static boolean isKindOfLevelSelectionType(MemberSelectionType selectionType) {
		
		return selectionType.equals(MemberSelectionType.Level);
		
	}
	
	public static boolean isKindOfGenerationSelectionType(MemberSelectionType selectionType) {
		
		return selectionType.equals(MemberSelectionType.Generation);
		
	}
	
	public static boolean isKindOfDescendantSelectionType(MemberSelectionType selectionType) {
		
		return selectionType.equals(MemberSelectionType.Desc) || 
				selectionType.equals(MemberSelectionType.IDesc) || 
				selectionType.equals(MemberSelectionType.Descendants) || 
				selectionType.equals(MemberSelectionType.IDescendants) ;
	}
	
	public static boolean isKindOfDescendantLevelOrGenSelectionType(MemberSelectionType selectionType) {
		
		return isKindOfDescendantSelectionType(selectionType) || 
				isKindOfLevelSelectionType(selectionType) || 
				isKindOfGenerationSelectionType(selectionType);
		
	}

	public static boolean isKindOfChildrenDescLevelOrGenSelectionType(MemberSelectionType selectionType) {
		
		return 	selectionType.equals(MemberSelectionType.Child) ||
				selectionType.equals(MemberSelectionType.Children) ||
				selectionType.equals(MemberSelectionType.IChild) ||
				selectionType.equals(MemberSelectionType.IChildren) || 
				isKindOfDescendantLevelOrGenSelectionType(selectionType);
		
	}
	
	/**
	 * @return Returns the levelGenNumber.
	 */
	public int getLevelGenNumber() {
		return levelGenNumber;
	}

	/**
	 * @param levelGenNumber The levelGenNumber to set.
	 */
	public void setLevelGenNumber(int levelGenNumber) {
		this.levelGenNumber = levelGenNumber;
	}

	/**
	 * @return Returns the levelGenType.
	 */
	public LevelGenerationType getLevelGenType() {
		return levelGenType;
	}

	/**
	 * @param levelGenType The levelGenType to set.
	 */
	public void setLevelGenType(LevelGenerationType levelGenType) {
		this.levelGenType = levelGenType;
	}
	
	public boolean isLevelMember(String member) {
		
		if ( member != null && member.contains(LEVEL)) {
			return true;
		}
		
		return false;
	}
	
	public boolean isGenerationMember(String member) {
		
		if ( member != null && member.contains(GENERATION)) {
			return true;
		}
		
		return false;
	}
}

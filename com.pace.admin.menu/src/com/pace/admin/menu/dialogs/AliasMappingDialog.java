/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.AliasMapping;
import com.pace.base.app.AliasMemberDisplayType;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.migration.PafAppsMigrationAction;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.server.client.PafMdbProps;
import com.swtdesigner.SWTResourceManager;

public class AliasMappingDialog extends Dialog {

	private static Logger logger = Logger.getLogger(AliasMappingDialog.class);
	
	private Map<String, Button> useGlobalCheckButtonMap = new TreeMap<String, Button>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> aliasTableComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> primaryDisplayFormatComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> additionalDisplayFormatComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	
	private List<String> dimensionNamesInOrder;
	
	private Map<String, AliasMapping> viewSectionAliasMappingsMap = new TreeMap<String, AliasMapping>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, AliasMapping> globalAliasMappingsMap = new TreeMap<String, AliasMapping>(String.CASE_INSENSITIVE_ORDER);
	private String[] aliasTableNames;
	
	private AliasMapping[] aliasMappingAr = null;

	
	private static final String DIM_NAME_KEY = "DimName";
	
	private static final String DEFAULT_DISPLAY_FORMAT = AliasMemberDisplayType.Alias.toString();
	
	private IProject project = null;
	
	private String[] defaultAdditonalItems = new String[] { "", 
				AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString() };
	
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param viewSection 
	 * @throws Exception 
	 */
	public AliasMappingDialog(Shell parentShell, IProject project, PafViewSectionUI viewSection) throws Exception {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.project = project;
		
		dimensionNamesInOrder = new ArrayList<String>();
		
		if ( viewSection != null && viewSection.getPageAxisDims() != null && 
				viewSection.getColAxisDims() != null && viewSection.getRowAxisDims() != null ) {
			
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getPageAxisDims()));
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getColAxisDims()));
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getRowAxisDims()));
			
			Collections.sort(dimensionNamesInOrder);
		}
		
		if ( viewSection != null && viewSection.getAliasMappings() != null) {
			
			AliasMapping[] initialAliasMappings = viewSection.getAliasMappings();
			
			for (AliasMapping aliasMapping : initialAliasMappings) {
				
				viewSectionAliasMappingsMap.put(aliasMapping.getDimName(), aliasMapping);
				
			}
			
		}
		
		PafMdbProps cachedPafMdbProps = null;
		try{
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
					PafProjectUtil.getProjectServer(project), 
					PafProjectUtil.getApplicationName(project));
		} catch(Exception e){
			logger.error(e.getMessage());
			throw e;
		}
		
		if ( cachedPafMdbProps != null ) {
			
			if ( cachedPafMdbProps.getAliasTables().size() > 0 ) {
			
				this.aliasTableNames = cachedPafMdbProps.getAliasTables().toArray(new String[0]);
				
			}
									
			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
			
			XMLPaceProject pp = null;
			try {
				
				pp = new XMLPaceProject(
						iConfFolder.getLocation().toString(), 
						new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.ApplicationDef)),
								false);
				
			} catch (InvalidPaceProjectInputException e1) {
				
				logger.error("Problem refreshing local folder: " + e1.getMessage());
				
			} catch (PaceProjectCreationException e1) {

				logger.error("Invalid Pace Project: " + e1.getMessage());
			}
			
			//run migration action to add any new attribute dims that were cached
			PafAppsMigrationAction migrationAction = new PafAppsMigrationAction(pp);
			
			if ( cachedPafMdbProps.getCachedAttributeDims().size() > 0 ) {
				
				migrationAction.setAttributeDimensionNames(cachedPafMdbProps.getCachedAttributeDims().toArray(new String[0]));
				
			}
			
			migrationAction.run();
			
		} 

		
		PafApplicationDef pafApp = PafApplicationUtil.getPafApp(project);
		
		if ( pafApp != null && pafApp.getAppSettings() != null && 
				pafApp.getAppSettings().getGlobalAliasMappings() != null ) {
			
			AliasMapping[] globalAliasMappings = pafApp.getAppSettings().getGlobalAliasMappings();
			
			for (AliasMapping aliasMapping : globalAliasMappings) {
				
				globalAliasMappingsMap.put(aliasMapping.getDimName(), aliasMapping);
				
			}
			
		}
								
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
	    Composite middle = new Composite(container, SWT.NONE);
	    final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
	    gridData_1.widthHint = 639;
	    middle.setLayoutData(gridData_1);
	    
	    final GridLayout gl = new GridLayout();
	    
	    gl.numColumns = 1;
	    gl.marginTop=20;
	    gl.marginBottom=20;
	    gl.marginLeft=20;
	    gl.marginRight=20;
	    middle.setLayout(gl);
	    
	    
	    // Create the ScrolledComposite to scroll horizontally and vertically
	    ScrolledComposite sc = new ScrolledComposite(middle, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
	    final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
	    gridData.minimumHeight = 200;
	    sc.setLayoutData(gridData);
	    
	    sc.getVerticalBar().setIncrement(sc.getVerticalBar().getIncrement()*3);

	    // Create a child child to hold the controls
	    Composite child = new Composite(sc, SWT.NONE);
	    child.setBounds(0, 0,576, 278);
	    //child.setLayout(new RowLayout());
	    
	    final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 5;
		child.setLayout(gridLayout_1);

		final Label globalEnabledLabel = new Label(child, SWT.WRAP);
		globalEnabledLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//globalEnabledLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		globalEnabledLabel.setLayoutData(new GridData(70, 30));
		globalEnabledLabel.setText("Use Global Setting");

		final Label dimensionNameLabel = new Label(child, SWT.WRAP);
		dimensionNameLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//dimensionNameLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		dimensionNameLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		dimensionNameLabel.setText("Dimension Name");

		final Label aliasTableLabel = new Label(child, SWT.NONE);
		aliasTableLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//aliasTableLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		aliasTableLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		aliasTableLabel.setText("Alias Table");

		final Label displayFormatLabel = new Label(child, SWT.WRAP);
		displayFormatLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		//displayFormatLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel.setText("Primary Row/Col Display Format");

		final Label displayFormatLabel_1 = new Label(child, SWT.WRAP);
		displayFormatLabel_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel_1.setLayoutData(new GridData(100, SWT.DEFAULT));
		//displayFormatLabel_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel_1.setText("Additional Row/Col Display Format");

		final Label label_1 = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
		gridData_2.widthHint = 402;
		label_1.setLayoutData(gridData_2);
		
	    int size = dimensionNamesInOrder.size();
		
	    for (String dimName : dimensionNamesInOrder) {
	    	
	    	createRowInScrolledComposite(child, dimName);
	    	
	    }

	    // Set the child as the scrolled content of the ScrolledComposite
	    sc.setContent(child);

	    // Set the minimum size
	    sc.setMinSize(100, size*45);

	    // Expand both horizontally and vertically
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
		
	    initilizeForm();
	    
	    addListeners();
		//
		return container;
	}
	
	public void createRowInScrolledComposite(Composite child, String dimName) {
		  
		  	final Button useGlobalSettingCheck = new Button(child, SWT.CHECK);

		  	useGlobalSettingCheck.setLayoutData(new GridData());

		  	useGlobalSettingCheck.setData(DIM_NAME_KEY, dimName);
		  	
		  	useGlobalCheckButtonMap.put(dimName, useGlobalSettingCheck);
		  	
			final Label dimNameLabel = new Label(child, SWT.WRAP);
			dimNameLabel.setText(dimName);

			Combo aliasTableCombo = new Combo(child, SWT.READ_ONLY);
			aliasTableCombo.setText("Default");
			final GridData gridData = new GridData(60, SWT.DEFAULT);
			aliasTableCombo.setLayoutData(gridData);

			aliasTableCombo.setData(DIM_NAME_KEY, dimName);
			
			if ( aliasTableNames != null ) {
				
				aliasTableCombo.setItems(aliasTableNames);
				
			}
			
			aliasTableComboMap.put(dimName, aliasTableCombo);

			Combo primaryDisaplyFormatCombo = new Combo(child, SWT.READ_ONLY);
			primaryDisaplyFormatCombo.setText("Alias");
			final GridData gridData_5 = new GridData(40, SWT.DEFAULT);
			primaryDisaplyFormatCombo.setLayoutData(gridData_5);
			
			primaryDisaplyFormatCombo.setData(DIM_NAME_KEY, dimName);

			primaryDisaplyFormatCombo.setItems(new String[] { AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString()});
			
			primaryDisplayFormatComboMap.put(dimName, primaryDisaplyFormatCombo);

			Combo additionalDisplayFormatCombo = new Combo(child, SWT.READ_ONLY);
			additionalDisplayFormatCombo.setText("Member");
			final GridData gridData_6_1 = new GridData(40, SWT.DEFAULT);
			additionalDisplayFormatCombo.setLayoutData(gridData_6_1);

			additionalDisplayFormatCombo.setData(DIM_NAME_KEY, dimName);
			
			//additionalDisplayFormatCombo.setItems(new String[] { "", AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString()});			
			
			additionalDisplayFormatComboMap.put(dimName, additionalDisplayFormatCombo);
			
			final Label sepLabel = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
			final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
			gridData_1.widthHint = 404;
			sepLabel.setLayoutData(gridData_1);
		  
	}

	private void initilizeForm() {

		for (String key : useGlobalCheckButtonMap.keySet()) {
			
			if ( viewSectionAliasMappingsMap.containsKey(key)) {
				
				selectUseGlobalButton(key, false);
				
				enableRow(key, true);
												
				populateRow(key, false);
				
			} else if ( globalAliasMappingsMap.containsKey(key)) {

				selectUseGlobalButton(key, true);

				enableRow(key, false);
				
				populateRow(key, true);
				
			} else {
				//do something
			}
			
		}
			
	}
	
	private void addListeners() {

		for (Button useGlobalCheckButton : useGlobalCheckButtonMap.values()) {
			
			useGlobalCheckButton.addSelectionListener(new SelectionAdapter() {

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					Button useGlobalCheckButton = (Button) selectionEvent.widget;
					
					boolean useGlobal = useGlobalCheckButton.getSelection();
					
					String dimKey = (String) useGlobalCheckButton.getData(DIM_NAME_KEY);
					
					enableRow(dimKey, ! useGlobal);
						
					populateRow(dimKey, useGlobal);


				}
				
			});
			
		}
		
		for (Combo primaryDisplayFormat : primaryDisplayFormatComboMap.values()	) {
			
			primaryDisplayFormat.addSelectionListener(new SelectionAdapter() {

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {
					
					Combo primaryDisplayFormatCombo = (Combo) selectionEvent.widget;
					
					String selectedPrimaryDisplayFormat = primaryDisplayFormatCombo.getText();
					
					String dimKey = (String) primaryDisplayFormatCombo.getData(DIM_NAME_KEY);
					
					Combo additonalDisplayFormatCombo = additionalDisplayFormatComboMap.get(dimKey);
					
					String currentAdditionalDisplayFormat = additonalDisplayFormatCombo.getText();
					
					additonalDisplayFormatCombo.setItems(defaultAdditonalItems);
					
					additonalDisplayFormatCombo.remove(selectedPrimaryDisplayFormat);
					
					additonalDisplayFormatCombo.setText(currentAdditionalDisplayFormat);
					
				}				
				
			});
			
		}
		
	}

	
	private void enableRow(String key, boolean isEnabled) {
		
		aliasTableComboMap.get(key).setEnabled(isEnabled);
		primaryDisplayFormatComboMap.get(key).setEnabled(isEnabled);
		additionalDisplayFormatComboMap.get(key).setEnabled(isEnabled);				
		
	}

	private void selectUseGlobalButton(String key, boolean isSelected) {

		Button useGlobalButton = useGlobalCheckButtonMap.get(key);
		
		if ( useGlobalButton != null ) {
			
			useGlobalButton.setSelection(isSelected);
		}
		
	}
	
	private void populateRow(String key, boolean useGlobal) {
	
		if ( useGlobal ) {
			if ( globalAliasMappingsMap.containsKey(key) ) {
				populateRow(key, globalAliasMappingsMap.get(key));
			} 
		} else {
		
			if ( viewSectionAliasMappingsMap.containsKey(key)) {
				populateRow(key, viewSectionAliasMappingsMap.get(key));				
			}

		}
		
	}

	private void populateRow(String key, AliasMapping mapping) {
		
		aliasTableComboMap.get(key).setText(mapping.getAliasTableName());
		
		if ( aliasTableComboMap.get(key).getText().trim().length() == 0 ) {
			
			aliasTableComboMap.get(key).setText(PafBaseConstants.ALIAS_TABLE_DEFAULT);
			
		}
		
		primaryDisplayFormatComboMap.get(key).setText((mapping.getPrimaryRowColumnFormat() == null) ? DEFAULT_DISPLAY_FORMAT : mapping.getPrimaryRowColumnFormat());
		
		String primaryDisplayFormat = primaryDisplayFormatComboMap.get(key).getText();
		
		if ( primaryDisplayFormat.trim().equals("")) {
			
			primaryDisplayFormatComboMap.get(key).setText(DEFAULT_DISPLAY_FORMAT);
			
			primaryDisplayFormat = DEFAULT_DISPLAY_FORMAT;
			
		}
		
		additionalDisplayFormatComboMap.get(key).setItems(defaultAdditonalItems);
		
		additionalDisplayFormatComboMap.get(key).remove(primaryDisplayFormat);
		
		additionalDisplayFormatComboMap.get(key).setText((mapping.getAdditionalRowColumnFormat() == null) ? "" : mapping.getAdditionalRowColumnFormat());
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(669, 493);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Alias Mapping");
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {

			createAliasMappings();
			
		}
		super.buttonPressed(buttonId);
	}

	private void createAliasMappings() {
	
		for (Button useGlobalCheckButton : useGlobalCheckButtonMap.values() ) {
					
			String dimKey = (String) useGlobalCheckButton.getData(DIM_NAME_KEY);

			//if use global setting
			if ( useGlobalCheckButton.getSelection()) {
				
				if ( viewSectionAliasMappingsMap.containsKey(dimKey ) ) {
					
					viewSectionAliasMappingsMap.remove(dimKey);
				}

			} else {
				
				AliasMapping newAliasMapping = new AliasMapping();
				
				newAliasMapping.setDimName(dimKey);
				newAliasMapping.setAliasTableName(aliasTableComboMap.get(dimKey).getText());
				newAliasMapping.setPrimaryRowColumnFormat(primaryDisplayFormatComboMap.get(dimKey).getText());
				newAliasMapping.setAdditionalRowColumnFormat(additionalDisplayFormatComboMap.get(dimKey).getText());
				
				viewSectionAliasMappingsMap.put(dimKey, newAliasMapping);
				
			}
			
		}
		
		if ( viewSectionAliasMappingsMap.size() > 0 ) {
			aliasMappingAr = viewSectionAliasMappingsMap.values().toArray(new AliasMapping[0]);
		}
		
	}

	/**
	 * @return the aliasMappingAr
	 */
	public AliasMapping[] getAliasMappingAr() {
		return aliasMappingAr;
	}
	
}

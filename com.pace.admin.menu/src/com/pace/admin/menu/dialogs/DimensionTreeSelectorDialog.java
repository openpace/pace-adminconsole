/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.DynamicMembersModelManager;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.DynamicMemberDef;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

/**
 * 
 * Small dialog that displays the dimension tree and allows a user to select a member.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DimensionTreeSelectorDialog extends Dialog {

	private static final String DISPALY_ONLY_KEY = "DISPLAY_ONLY";

	private static Logger logger = Logger.getLogger(DimensionTreeSelectorDialog.class);
	
	private Tree dimensionTree;
	
	private	String dimensionName;
	
	private IProject project;
	
	private String selectedMember;
	
	private String[] additionalNonVersionMembers = new String[] { PafBaseConstants.UOW_ROOT };
	
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public DimensionTreeSelectorDialog(Shell parentShell, IProject project, String dimensionName) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;
		this.dimensionName = dimensionName;
		
		//if arg is null, throw exception
		if ( dimensionName == null ) {
			throw new IllegalArgumentException("Dimension name can not be null.");
		}
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		final Label selectMemberLabel = new Label(container, SWT.NONE);
		selectMemberLabel.setText("Select Member:");

		dimensionTree = new Tree(container, SWT.BORDER);
		dimensionTree.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent arg0) {
				
				TreeItem[] selectedItems = dimensionTree.getSelection();
				
				if ( selectedItems.length == 1 ) {
				
					//if no children, process double click as clicking OK button
					if ( selectedItems[0].getItemCount() == 0 ) {
						
						buttonPressed(IDialogConstants.OK_ID);
						
					}
					
				}
										
				
			}
		});
		dimensionTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		createTreeModel(dimensionTree);
				
		
		return container;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(249, 350);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Dimension Tree");
	}

	
	/**
	 * Build the contents of a tree control and populate it.
	 * @param tree The tree control to build.
	 */
	private void createTreeModel(Tree tree) {
				
		logger.debug("Create Tree Model start");
		
		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();
		
		try {

			String projectAppName = PafProjectUtil.getApplicationName(project);
			
			PafSimpleDimTree pafSimpleTree = DimensionTreeUtility.getDimensionTree(project, projectAppName, dimensionName);
			
			logger.debug("Converting TreeInto hash start");

			HashMap treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(pafSimpleTree);

			logger.debug("Converting TreeInto hash end");

			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(pafSimpleTree.getRootKey());

			logger.info("MAX LEVEL: "
					+ rootMember.getPafSimpleDimMemberProps().getLevelNumber());

			logger.debug("Building Tree start");

			addTreeNode(treeHashMap, tree, null, rootMember, dimensionName);

			cachedTreeMap.put(dimensionName, tree);

			logger.debug("Building Tree end");

		}catch(Exception e){
			logger.error(e.getMessage());
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
			throw new RuntimeException(e.getMessage());
		}
		logger.debug("Create Tree Model end");
		
	}

	
	/**
	 * Adds a tree node to a tree.
	 * @param treeHashMap The hash map containing the tree components.
	 * @param tree The tree to add the nodes to.
	 * @param parent The parent tree item to have the nodes added to.
	 * @param member The paf simple memeber to add to the parent.
	 * @param dimension The dimension of this tree.
	 */
	private void addTreeNode(HashMap treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);
			
		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		if (member.getChildKeys() != null) {

			java.util.List<String> children = member.getChildKeys();

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap.get(child);

				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}
		
		//if root of tree, add additional items
		if ( parent == null ) {
			
			//get version dimension name
			String versionDim = DimensionUtil.getVersionDimensionName(project);
			
			
			//if current dim equals the version dim, add the additional version members
			if ( dimension.equalsIgnoreCase(versionDim)) {
								
				TreeItem dynamicMembersFolderItem = new TreeItem(newItem, SWT.NONE);
				
				dynamicMembersFolderItem.setText(Constants.DYNAMIC_MEMBERS_NODE_NAME);
				dynamicMembersFolderItem.setData(DISPALY_ONLY_KEY, new Boolean(true));				
				
				DynamicMembersModelManager dynamicMembersManager = new DynamicMembersModelManager(project);
				
				DynamicMemberDef versionDynamicMemberDef = (DynamicMemberDef) dynamicMembersManager.getItem(versionDim);
				
				if ( versionDynamicMemberDef != null ) {
					
					String[] memberSpecs = versionDynamicMemberDef.getMemberSpecs();
					
					for (String memberSpec : memberSpecs ) {
						
						TreeItem additionalTreeItem = new TreeItem(dynamicMembersFolderItem, SWT.NONE);
						additionalTreeItem.setText(memberSpec);
						
					}
				
					dynamicMembersFolderItem.setExpanded(true);
				}
				
			//if current dim not equals the version dim, add the additional non version members
			} else {
				
				for (String additionalNonVersionMember : additionalNonVersionMembers ) {
					
					TreeItem additionalTreeItem = new TreeItem(newItem, SWT.NONE);
					additionalTreeItem.setText(additionalNonVersionMember);
					
				}
				
			}
			
			
			newItem.setExpanded(true);
			
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		
		//if ok pressed
		if (buttonId == IDialogConstants.OK_ID) {

			//get selected tree items and set selected member
			TreeItem[] selectedTreeItems = dimensionTree.getSelection();
			
			if ( selectedTreeItems.length > 0 ) {
				
				TreeItem selectedTreeItem = selectedTreeItems[0];
				
				//get data property
				Boolean isDisplayOnly = (Boolean) selectedTreeItem.getData(DISPALY_ONLY_KEY);
				
				//if property is not null, check value
				if ( isDisplayOnly != null ) {
					
					//if property is display only, alert user and return
					if ( isDisplayOnly) {
						
						MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "'" + selectedTreeItem.getText() + "' is not a valid selection.");
						
						return;
					}
					
				}
				
				selectedMember = selectedTreeItem.getText();
				
				logger.debug("User selected: " + selectedMember);
				
			}
			
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * @return Returns the selectedMember.
	 */
	public String getSelectedMember() {
		return selectedMember;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.model.PaceTree;
import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.model.managers.HierarchyFormattingModelManager;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.MemberTagUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.menu.dialogs.input.ViewSectionTuplesDialogInput;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.AliasMapping;
import com.pace.base.app.AliasMemberDisplayType;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.SuppressZeroSettings;
import com.pace.base.db.membertags.MemberTagCommentEntry;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.mdb.PafDimTree.LevelGenType;
import com.pace.base.migration.PafAppsMigrationAction;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.view.GroupingSpec;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.server.client.PafMdbProps;
import com.swtdesigner.SWTResourceManager;

public class ViewSectionOptionsDialog extends Dialog {

	private static final String ROW_TEXT = "Row";

	private static final String COL_TEXT = "Column";
	
	private static Map<String, Boolean> dropdownBooleanMap = new LinkedHashMap<String, Boolean>();
	
	private static Map<String, Boolean> dropdownGlobalBooleanMap = new LinkedHashMap<String, Boolean>();
	
	static {
		
		dropdownBooleanMap.put(Constants.DROPDOWN_BOOL_TRUE, Boolean.TRUE);
		dropdownBooleanMap.put(Constants.DROPDOWN_BOOL_FALSE, Boolean.FALSE);		
		dropdownGlobalBooleanMap.put(Constants.DROPDOWN_GLOBAL_OPTION, null);
		dropdownGlobalBooleanMap.putAll(dropdownBooleanMap);
				
	}
	
	private String[] trueFalseGlobalAr = dropdownGlobalBooleanMap.keySet().toArray(new String[0]);
	
//	private String[] trueFalseAr = dropdownBooleanMap.keySet().toArray(new String[0]);
	
	private HierarchyFormattingModelManager generationFormattingManager = null;
	private ConditionalFormatModelManager condFormatManager = null;
	private MemberTagModelManager memberTagModelManager = null;
	

	private Combo suppressColumnsCombo;
	private Combo suppressRowsCombo;
	private Combo suppressVisibleCombo;
	private Combo suppressEnabledCombo;
	
	private Combo hierarchyFormatCombo;
	private Combo conditionalFormatCombo;
	private Combo readOnlyCombo;
	private Combo primaryFormattingAxisCombo;
	private Combo repeatHeaderRowCombo;
	private Combo repeatHeaderColCombo;
	private static Logger logger = Logger.getLogger(ViewSectionOptionsDialog.class);
	
	private Map<String, Button> aliasMapping_useGlobalCheckButtonMap = new TreeMap<String, Button>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> aliasMapping_aliasTableComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> aliasMapping_primaryDisplayFormatComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> aliasMapping_additionalDisplayFormatComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	
	private Map<String, Button> grouping_enabledCheckButtonMap = new TreeMap<String, Button>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Label> grouping_dimNameLabel = new TreeMap<String, Label>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Label> grouping_axisLabel = new TreeMap<String, Label>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Combo> grouping_levelGenerationComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, Spinner> grouping_levelGeneratioNumberSpinnerMap = new TreeMap<String, Spinner>(String.CASE_INSENSITIVE_ORDER);
	
	//key = member tag name; value = combo with Default/True/False
	private Map<String, Combo> commentMemberTag_VisibleComboMap = new TreeMap<String, Combo>(String.CASE_INSENSITIVE_ORDER);
	
	private List<String> dimensionNamesInOrder;
	private Map<String, PafAxis> dimensionLocation = new TreeMap<String, PafAxis>(String.CASE_INSENSITIVE_ORDER);
	
	private Map<String, AliasMapping> viewSectionAliasMappingsMap = new TreeMap<String, AliasMapping>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, AliasMapping> globalAliasMappingsMap = new TreeMap<String, AliasMapping>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, MemberTagDef> commentMemberTagMap = new TreeMap<String, MemberTagDef>(String.CASE_INSENSITIVE_ORDER);
	private String[] aliasTableNames;
	private Set<String> hierarchyDims = new HashSet<String>(10);
	private String rowAxisGroupDim = "";
	private String colAxisGroupDim = "";
	
	private static final String DIM_NAME_KEY = "DimName";
	
	private static final String DEFAULT_DISPLAY_FORMAT = AliasMemberDisplayType.Alias.toString();
	
	private IProject project = null;
	
	private String[] defaultAdditonalItems = new String[] { "", 
				AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString() };
	
	private PafViewSectionUI viewSection;
	private ViewSectionTuplesDialogInput input;
		
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param viewSection 
	 * @throws Exception 
	 */
	public ViewSectionOptionsDialog(Shell parentShell, ViewSectionTuplesDialogInput input) throws Exception {
		super(parentShell);
		setShellStyle( getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.input = input;
		this.project = this.input.getProject();
		this.viewSection = this.input.getPafViewSectionUI();
		
		generationFormattingManager = new HierarchyFormattingModelManager(project);
		memberTagModelManager = new MemberTagModelManager(project);
		condFormatManager = new ConditionalFormatModelManager(project);
		
		hierarchyDims.addAll(Arrays.asList(DimensionUtil.getHierDimensionNames(project)));
		hierarchyDims.add(DimensionUtil.getTimeDimensionName(project));
		//hierarchyDims.add(DimensionUtil.getYearDimensionName(project));
		
		dimensionNamesInOrder = new ArrayList<String>();
		
		if ( viewSection != null && viewSection.getPageAxisDims() != null && 
				viewSection.getColAxisDims() != null && viewSection.getRowAxisDims() != null ) {
			
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getPageAxisDims()));
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getColAxisDims()));
			dimensionNamesInOrder.addAll(Arrays.asList(viewSection.getRowAxisDims()));
			
			Collections.sort(dimensionNamesInOrder);
		}
		
		if ( viewSection != null && viewSection.getPageAxisDims() != null && 
				viewSection.getColAxisDims() != null && viewSection.getRowAxisDims() != null ) {
			
			PafAxis page = new PafAxis(PafAxis.PAGE);
			for(String s : viewSection.getPageAxisDims()){
				dimensionLocation.put(s, page);
			}
			
			PafAxis col = new PafAxis(PafAxis.COL);
			for(String s : viewSection.getColAxisDims()){
				dimensionLocation.put(s, col);
			}
			
			PafAxis row = new PafAxis(PafAxis.ROW);
			for(String s : viewSection.getRowAxisDims()){
				dimensionLocation.put(s, row);
			}
		}
		
		if ( viewSection != null && viewSection.getAliasMappings() != null) {
			
			AliasMapping[] initialAliasMappings = viewSection.getAliasMappings();
			
			for (AliasMapping aliasMapping : initialAliasMappings) {
				
				viewSectionAliasMappingsMap.put(aliasMapping.getDimName(), aliasMapping);
				
			}
			
		}
		
		//populate comment member tag map
		if ( viewSection != null && dimensionNamesInOrder.size() > 0 ) {
			
			//get member tags
			MemberTagDef[] memberTagDefs = memberTagModelManager.getMemberTags(dimensionNamesInOrder.toArray(new String[0]), false);
						
			if ( memberTagDefs != null ) {
				
				//add member tag to map if member tag is valid for screen
				for ( MemberTagDef memberTagDef : memberTagDefs ) {
					
					if ( MemberTagUtil.isMemberTagValidForViewSection(viewSection, memberTagDef ) ) {
						
						commentMemberTagMap.put(memberTagDef.getName(), memberTagDef);
						
					}
					
				}
				
				/* TOOK OUT BECAUSE OF FARKAS
			
				//get view section member tag names
				String[] viewTupleMemberTagNames = viewSection.getMemberTagNamesFromViewTuples();
				
				//create a set of view section member tag names
				Set<String> viewTupleMemberTagNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
				//populate set if view Tuple member tag names exists
				if ( viewTupleMemberTagNames != null ) {
					
					viewTupleMemberTagNameSet.addAll(Arrays.asList(viewTupleMemberTagNames));
					
				}
				
				//add member tag to map if not on the view section
				for ( MemberTagDef memberTagDef : memberTagDefs ) {
					
					if ( ! viewTupleMemberTagNameSet.contains(memberTagDef.getName())) {
						
						commentMemberTagMap.put(memberTagDef.getName(), memberTagDef);
						
					}
					
				}
			
				*/
			
			}
			
		}
		
		
		PafMdbProps cachedPafMdbProps = null;
		try{
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
					PafProjectUtil.getProjectServer(project), 
					PafProjectUtil.getApplicationName(project));
		} catch(Exception e){
			logger.error(e.getMessage());
			throw e;
		}
		
		if ( cachedPafMdbProps != null ) {
			
			if ( cachedPafMdbProps.getAliasTables().size() > 0 ) {
				this.aliasTableNames = cachedPafMdbProps.getAliasTables().toArray(new String[0]);
			}
			
			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
				
			XMLPaceProject pp = null;
			try {
				
				pp = new XMLPaceProject(
						iConfFolder.getLocation().toString(), 
						new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.ApplicationDef)),
								false);
				
			} catch (InvalidPaceProjectInputException e1) {
				
				logger.error("Problem refreshing local folder: " + e1.getMessage());
				
			} catch (PaceProjectCreationException e1) {

				logger.error("Invalid Pace Project: " + e1.getMessage());
			}
			
			
			//run migration action to add any new attribute dims that were cached
			PafAppsMigrationAction migrationAction = new PafAppsMigrationAction(pp);
			
			List<String> cachedAttDimsList = cachedPafMdbProps.getCachedAttributeDims();
			
			if ( cachedAttDimsList.size() > 0 ) {
				migrationAction.setAttributeDimensionNames(cachedAttDimsList.toArray(new String[0]));
			}
			
			migrationAction.run();
			
		} 

		
		PafApplicationDef pafApp = PafApplicationUtil.getPafApp(project);
		
		if ( pafApp != null && pafApp.getAppSettings() != null && 
				pafApp.getAppSettings().getGlobalAliasMappings() != null ) {
			
			AliasMapping[] globalAliasMappings = pafApp.getAppSettings().getGlobalAliasMappings();
			
			for (AliasMapping aliasMapping : globalAliasMappings) {
				
				globalAliasMappingsMap.put(aliasMapping.getDimName(), aliasMapping);
				
			}
			
		}
								
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
			   
		final TabFolder tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TabItem optionsTabItem = new TabItem(tabFolder, SWT.NONE);
		optionsTabItem.setText("Options");
		
		final Composite composite = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginBottom = 20;
		composite.setLayout(gridLayout);
		optionsTabItem.setControl(composite);

		final Group viewOptionsGroup = new Group(composite, SWT.NONE);
		viewOptionsGroup.setText("View Section Options");
		GridData gd_viewOptionsGroup = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_viewOptionsGroup.heightHint = 191;
		viewOptionsGroup.setLayoutData(gd_viewOptionsGroup);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 2;
		viewOptionsGroup.setLayout(gridLayout_2);

		final Label primaryFormattingAxisLabel = new Label(viewOptionsGroup, SWT.NONE);
		GridData gd_primaryFormattingAxisLabel = new GridData(120, SWT.DEFAULT);
		gd_primaryFormattingAxisLabel.grabExcessVerticalSpace = true;
		primaryFormattingAxisLabel.setLayoutData(gd_primaryFormattingAxisLabel);
		primaryFormattingAxisLabel.setText("Primary Formatting Axis");

		primaryFormattingAxisCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		primaryFormattingAxisCombo.setItems(new String[] { ROW_TEXT, COL_TEXT });
		primaryFormattingAxisCombo.setEnabled(false);
		primaryFormattingAxisCombo.select(0);
		GridData gd_primaryFormattingAxisCombo = new GridData(50, SWT.DEFAULT);
		gd_primaryFormattingAxisCombo.grabExcessVerticalSpace = true;
		primaryFormattingAxisCombo.setLayoutData(gd_primaryFormattingAxisCombo);

		final Label readOnlyLabel = new Label(viewOptionsGroup, SWT.NONE);
		readOnlyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		readOnlyLabel.setText("Read Only");

		readOnlyCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		readOnlyCombo.setItems(new String[] { Constants.DROPDOWN_BOOL_TRUE, Constants.DROPDOWN_BOOL_FALSE });
		GridData gd_readOnlyCombo = new GridData(50, SWT.DEFAULT);
		gd_readOnlyCombo.grabExcessVerticalSpace = true;
		readOnlyCombo.setLayoutData(gd_readOnlyCombo);
		readOnlyCombo.select(1);
		
		final Label hierarchyFormatLabel = new Label(viewOptionsGroup, SWT.NONE);
		hierarchyFormatLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		hierarchyFormatLabel.setText("Hierarchy Format");

		hierarchyFormatCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		hierarchyFormatCombo.setItems(populateGenerationFormats());
		hierarchyFormatCombo.add("", 0);
		GridData gd_hierarchyFormatCombo = new GridData(240, SWT.DEFAULT);
		gd_hierarchyFormatCombo.grabExcessVerticalSpace = true;
		hierarchyFormatCombo.setLayoutData(gd_hierarchyFormatCombo);
		
		final Label conditionalFormatLabel = new Label(viewOptionsGroup, SWT.NONE);
		conditionalFormatLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		conditionalFormatLabel.setText("Conditional Format");
		
		conditionalFormatCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		conditionalFormatCombo.setItems(populateCondtionalFormats());
		GridData gd_conditionalFormatCombo = new GridData(240, SWT.DEFAULT);
		gd_conditionalFormatCombo.grabExcessVerticalSpace = true;
		conditionalFormatCombo.setLayoutData(gd_conditionalFormatCombo);
		conditionalFormatCombo.add("", 0);
		
		final Label lblRepeatRowHeaders = new Label(viewOptionsGroup, SWT.NONE);
		lblRepeatRowHeaders.setText("Repeat Row Headers");
		GridData gd_lblRepeatRowHeaders = new GridData(152, SWT.DEFAULT);
		gd_lblRepeatRowHeaders.grabExcessVerticalSpace = true;
		lblRepeatRowHeaders.setLayoutData(gd_lblRepeatRowHeaders);
		
		repeatHeaderRowCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		repeatHeaderRowCombo.setItems(new String[] {"True", "False"});
		GridData gd_repeatHeaderRowCombo = new GridData(50, SWT.DEFAULT);
		gd_repeatHeaderRowCombo.grabExcessVerticalSpace = true;
		repeatHeaderRowCombo.setLayoutData(gd_repeatHeaderRowCombo);
		repeatHeaderRowCombo.select(1);
		
		final Label lblRepeatColumnHeaders = new Label(viewOptionsGroup, SWT.NONE);
		GridData gd_lblRepeatColumnHeaders = new GridData(152, SWT.DEFAULT);
		gd_lblRepeatColumnHeaders.grabExcessVerticalSpace = true;
		lblRepeatColumnHeaders.setLayoutData(gd_lblRepeatColumnHeaders);
		lblRepeatColumnHeaders.setText("Repeat Column Headers");
		
		repeatHeaderColCombo = new Combo(viewOptionsGroup, SWT.READ_ONLY);
		repeatHeaderColCombo.setItems(new String[] {"True", "False"});
		GridData gd_repeatHeaderColCombo = new GridData(50, SWT.DEFAULT);
		gd_repeatHeaderColCombo.grabExcessVerticalSpace = true;
		repeatHeaderColCombo.setLayoutData(gd_repeatHeaderColCombo);
		repeatHeaderColCombo.select(1);

		
		final Group suppressZeroOptionsGroup = new Group(composite, SWT.NONE);
		suppressZeroOptionsGroup.setText("Suppress Zero Options");
		suppressZeroOptionsGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		suppressZeroOptionsGroup.setLayout(gridLayout_3);

		final Label enabledLabel = new Label(suppressZeroOptionsGroup, SWT.NONE);
		GridData gd_enabledLabel = new GridData(152, SWT.DEFAULT);
		gd_enabledLabel.grabExcessVerticalSpace = true;
		enabledLabel.setLayoutData(gd_enabledLabel);
		enabledLabel.setText("Enabled");

		suppressEnabledCombo = new Combo(suppressZeroOptionsGroup, SWT.READ_ONLY);
		suppressEnabledCombo.setItems(trueFalseGlobalAr);
		suppressEnabledCombo.select(0);
		GridData gd_suppressEnabledCombo = new GridData(50, SWT.DEFAULT);
		gd_suppressEnabledCombo.grabExcessVerticalSpace = true;
		suppressEnabledCombo.setLayoutData(gd_suppressEnabledCombo);

		final Label visibleLabel = new Label(suppressZeroOptionsGroup, SWT.NONE);
		visibleLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		visibleLabel.setText("Visible");

		suppressVisibleCombo = new Combo(suppressZeroOptionsGroup, SWT.READ_ONLY);
		suppressVisibleCombo.setItems(trueFalseGlobalAr);
		suppressVisibleCombo.select(0);
		GridData gd_suppressVisibleCombo = new GridData(50, SWT.DEFAULT);
		gd_suppressVisibleCombo.grabExcessVerticalSpace = true;
		suppressVisibleCombo.setLayoutData(gd_suppressVisibleCombo);

		final Label suppressZeroRowsLabel = new Label(suppressZeroOptionsGroup, SWT.NONE);
		suppressZeroRowsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		suppressZeroRowsLabel.setText("Suppress Rows");

		suppressRowsCombo = new Combo(suppressZeroOptionsGroup, SWT.READ_ONLY);
		suppressRowsCombo.setItems(trueFalseGlobalAr);
		suppressRowsCombo.select(0);
		GridData gd_suppressRowsCombo = new GridData(50, SWT.DEFAULT);
		gd_suppressRowsCombo.grabExcessVerticalSpace = true;
		suppressRowsCombo.setLayoutData(gd_suppressRowsCombo);

		final Label suppressZeroRowsLabel_1 = new Label(suppressZeroOptionsGroup, SWT.NONE);
		
		suppressZeroRowsLabel_1.setLayoutData(new GridData(100, SWT.DEFAULT));
		suppressZeroRowsLabel_1.setText("Suppress Columns");

		suppressColumnsCombo = new Combo(suppressZeroOptionsGroup, SWT.READ_ONLY);
		suppressColumnsCombo.setItems(trueFalseGlobalAr);
		suppressColumnsCombo.select(0);
		GridData gd_suppressColumnsCombo = new GridData(50, SWT.DEFAULT);
		gd_suppressColumnsCombo.grabExcessVerticalSpace = true;
		suppressColumnsCombo.setLayoutData(gd_suppressColumnsCombo);
		

		final TabItem aliasMappingsTabItem = new TabItem(tabFolder, SWT.NONE);
		aliasMappingsTabItem.setText("Alias Mappings");
		
		Composite middle = new Composite(tabFolder, SWT.NONE);
	    
		final GridLayout gl = new GridLayout();
	    
		gl.numColumns = 1;
		gl.marginTop=20;
		gl.marginBottom=20;
		gl.marginLeft=20;
		gl.marginRight=20;
		middle.setLayout(gl);
	    
	    
	    // Create the ScrolledComposite to scroll horizontally and vertically
		ScrolledComposite aliasMappingsScrolledComposite = new ScrolledComposite(middle, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.minimumHeight = 200;
		aliasMappingsScrolledComposite.setLayoutData(gridData);
	    
		aliasMappingsScrolledComposite.getVerticalBar().setIncrement(aliasMappingsScrolledComposite.getVerticalBar().getIncrement()*3);

	    // Create a child child to hold the controls
		Composite child = new Composite(aliasMappingsScrolledComposite, SWT.NONE);
		child.setBounds(0, 0,576, 278);
	    //child.setLayout(new RowLayout());
	    
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 5;
		child.setLayout(gridLayout_1);

		final Label globalEnabledLabel = new Label(child, SWT.WRAP);
		globalEnabledLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//globalEnabledLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		globalEnabledLabel.setLayoutData(new GridData(70, 30));
		globalEnabledLabel.setText("Use Global Setting");

		final Label dimensionNameLabel = new Label(child, SWT.WRAP);
		dimensionNameLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//dimensionNameLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		dimensionNameLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		dimensionNameLabel.setText("Dimension Name");

		final Label aliasTableLabel = new Label(child, SWT.NONE);
		aliasTableLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		//aliasTableLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		aliasTableLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		aliasTableLabel.setText("Alias Table");

		final Label displayFormatLabel = new Label(child, SWT.WRAP);
		displayFormatLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel.setLayoutData(new GridData(100, SWT.DEFAULT));
		//displayFormatLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel.setText("Primary Row/Col Display Format");

		final Label displayFormatLabel_1 = new Label(child, SWT.WRAP);
		displayFormatLabel_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel_1.setLayoutData(new GridData(100, SWT.DEFAULT));
		//displayFormatLabel_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		displayFormatLabel_1.setText("Additional Row/Col Display Format");

		final Label label_1 = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
		gridData_2.widthHint = 402;
		label_1.setLayoutData(gridData_2);
		
		int size = dimensionNamesInOrder.size();
		
		for (String dimName : dimensionNamesInOrder) {
		    	
			createAliasMappingRowInScrolledComposite(child, dimName);
		}

	    // Set the child as the scrolled content of the ScrolledComposite
		aliasMappingsScrolledComposite.setContent(child);

	    // Set the minimum size
		aliasMappingsScrolledComposite.setMinSize(100, size*45);

	    // Expand both horizontally and vertically
		aliasMappingsScrolledComposite.setExpandHorizontal(true);
		aliasMappingsScrolledComposite.setExpandVertical(true);
		aliasMappingsTabItem.setControl(middle);

		
		final TabItem commentMemberTagsTabItem = new TabItem(tabFolder, SWT.NONE);
		commentMemberTagsTabItem.setText("Comment Member Tags");

		final Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginTop = 20;
		gridLayout_4.marginRight = 20;
		gridLayout_4.marginLeft = 20;
		gridLayout_4.marginBottom = 20;
		composite_1.setLayout(gridLayout_4);
		commentMemberTagsTabItem.setControl(composite_1);

		final ScrolledComposite memberTagScrolledComposite = new ScrolledComposite(composite_1, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		memberTagScrolledComposite.setExpandVertical(true);
		memberTagScrolledComposite.setExpandHorizontal(true);
		memberTagScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite child2 = new Composite(memberTagScrolledComposite, SWT.NONE);
		child2.setLocation(0, 0);
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.numColumns = 2;
		child2.setLayout(gridLayout_5);

		final Label globalEnabledLabel_1_1_1 = new Label(child2, SWT.NONE);
		globalEnabledLabel_1_1_1.setLayoutData(new GridData(120, SWT.DEFAULT));
		globalEnabledLabel_1_1_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		globalEnabledLabel_1_1_1.setText("Member Tag Name");

		final Label globalEnabledLabel_1_1 = new Label(child2, SWT.NONE);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_1.widthHint = 115;
		globalEnabledLabel_1_1.setLayoutData(gridData_1);
		globalEnabledLabel_1_1.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		globalEnabledLabel_1_1.setText("Visible On View");

		final Label label = new Label(child2, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		size = commentMemberTagMap.size();
		
		for (String memberTagName : commentMemberTagMap.keySet()) {
		    	
			createMemberTagCommentVisibleRowInScrolledComposite(child2, memberTagName);
		    	
		}
				
		 // Set the child as the scrolled content of the ScrolledComposite
		memberTagScrolledComposite.setContent(child2);

	    // Set the minimum size
		memberTagScrolledComposite.setMinSize(100, size*45);
		
//		 Expand both horizontally and vertically
		memberTagScrolledComposite.setExpandHorizontal(true);
		memberTagScrolledComposite.setExpandVertical(true);
		
		
		
		//Begin TTN-2223
		
		TabItem groupingTabItem = new TabItem(tabFolder, 0);
		groupingTabItem.setText("Groupings");
		
		Composite middle3 = new Composite(tabFolder, SWT.NONE);
		GridLayout gl2 = new GridLayout();
		
		gl2.numColumns = 1;
		gl2.marginTop=20;
		gl2.marginBottom=20;
		gl2.marginLeft=20;
		gl2.marginRight=20;
		middle3.setLayout(gl2);
		
		
		ScrolledComposite groupingScrolledComposite = new ScrolledComposite(middle3, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gd_groupingScrolledComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_groupingScrolledComposite.minimumHeight = 200;
		groupingScrolledComposite.setLayoutData(gd_groupingScrolledComposite);
		groupingScrolledComposite.getVerticalBar().setIncrement(groupingScrolledComposite.getVerticalBar().getIncrement()*3);
		
		Composite child3 = new Composite(groupingScrolledComposite, SWT.NONE);
		child3.setBounds(0, 0,576, 278);

		final GridLayout child3Layout = new GridLayout();
		child3Layout.numColumns = 5;
		child3.setLayout(child3Layout);
		
		Label groupingEnabledLabel = new Label(child3, SWT.WRAP);
		GridData gd_groupingEnabledLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_groupingEnabledLabel.widthHint = 70;
		gd_groupingEnabledLabel.heightHint = -1;
		groupingEnabledLabel.setLayoutData(gd_groupingEnabledLabel);
		groupingEnabledLabel.setText("Enabled");
		groupingEnabledLabel.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		
		Label lblDimName = new Label(child3, SWT.WRAP);
		GridData gd_lblDimName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblDimName.widthHint = 100;
		gd_lblDimName.heightHint = -1;
		lblDimName.setLayoutData(gd_lblDimName);
		lblDimName.setText("Dimension");
		lblDimName.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		
		Label lblAxis = new Label(child3, SWT.NONE);
		GridData gd_lblAxis = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblAxis.widthHint = 100;
		gd_lblAxis.heightHint = -1;
		lblAxis.setLayoutData(gd_lblAxis);
		lblAxis.setText("Axis");
		lblAxis.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		
		Label lblType = new Label(child3, SWT.WRAP);
		GridData gd_lblType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblType.widthHint = 100;
		gd_lblType.heightHint = -1;
		lblType.setLayoutData(gd_lblType);
		lblType.setText("Type");
		lblType.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		
		Label lblLevelgeneration = new Label(child3, SWT.WRAP);
		GridData gd_lblLevelgeneration = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblLevelgeneration.widthHint = 100;
		gd_lblLevelgeneration.heightHint = -1;
		lblLevelgeneration.setLayoutData(gd_lblLevelgeneration);
		lblLevelgeneration.setText("Level/Generation");
		lblLevelgeneration.setFont(SWTResourceManager.getFont("", 8, SWT.BOLD));
		
		Label label_7 = new Label(child3, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gd_label_7 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
		gd_label_7.widthHint = 402;
		label_7.setLayoutData(gd_label_7);
		
	
		Set<Map.Entry<String, PafAxis>> set = dimensionLocation.entrySet();

	    for (Map.Entry<String, PafAxis> dm : set) {
	    	createGroupingsRowInScrolledComposite(child3, dm.getKey(), dm.getValue());
	    }
		

	    // Set the child as the scrolled content of the ScrolledComposite
		groupingScrolledComposite.setContent(child3);

	    // Set the minimum size
		groupingScrolledComposite.setMinSize(100, size*45);

	    // Expand both horizontally and vertically
		groupingScrolledComposite.setExpandHorizontal(true);
		groupingScrolledComposite.setExpandVertical(true);
		groupingTabItem.setControl(middle3);
		
		//End TTN-2223
		
		
	    initilizeForm();
	    
	    addAliasMappingsListeners();
	    
	    addGroupingListeners();
	    
		return container;
	}
	
	/**
	 * TTN-2223
	 * @param child
	 * @param dimName
	 */
	public void createGroupingsRowInScrolledComposite(Composite child, String dimName, PafAxis axis) {
		
		//Don't add hierarchy dimensions
		if(!hierarchyDims.contains(dimName)){
			return;
		}

		boolean enabled = true;
		String axisLabel = PaceTreeAxis.Row.toString(); 	
		switch(axis.getValue()){
			case PafAxis.PAGE:
				enabled = false;
				axisLabel = PaceTreeAxis.Page.toString();
				break;
			case PafAxis.COL:
				axisLabel = PaceTreeAxis.Column.toString();
				break;
		}

		final Button useEnabled = new Button(child, SWT.CHECK);
		useEnabled.setEnabled(enabled);
		useEnabled.setLayoutData(new GridData());
		useEnabled.setData(DIM_NAME_KEY, dimName);
		grouping_enabledCheckButtonMap.put(dimName, useEnabled);
	  	
		final Label dimNameLabel = new Label(child, SWT.WRAP);
		dimNameLabel.setEnabled(enabled);
		dimNameLabel.setText(dimName);
		dimNameLabel.setData(DIM_NAME_KEY, dimName);
		grouping_dimNameLabel.put(dimName, dimNameLabel);
		
		final Label dimAxis = new Label(child, SWT.WRAP);
		dimAxis.setEnabled(enabled);
		dimAxis.setText(axisLabel);
		dimAxis.setData(DIM_NAME_KEY, dimName);
		grouping_axisLabel.put(dimName, dimAxis);
		
		Combo typeCombo = new Combo(child, SWT.READ_ONLY);
		typeCombo.setEnabled(false);
		typeCombo.setText(Constants.DROPDOWN_GLOBAL_OPTION);
		typeCombo.setItems(new String[] { LevelGenerationType.Level.toString(),	LevelGenerationType.Generation.toString(), LevelGenerationType.Bottom.toString() });
		final GridData gridData = new GridData(60, SWT.DEFAULT);
		EditorControlUtil.selectItem(typeCombo, LevelGenerationType.Bottom.toString());
		typeCombo.setLayoutData(gridData);
		typeCombo.setData(DIM_NAME_KEY, dimName);
		grouping_levelGenerationComboMap.put(dimName, typeCombo);
		
		Spinner levelSpinner = new Spinner(child, SWT.BORDER | SWT.READ_ONLY); 
		final GridData gridData2 = new GridData(25, SWT.DEFAULT);
		levelSpinner.setEnabled(false);
		levelSpinner.setLayoutData(gridData2);
		levelSpinner.setData(DIM_NAME_KEY, dimName);
		grouping_levelGeneratioNumberSpinnerMap.put(dimName, levelSpinner);
		
		final Label sepLabel = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
		gridData_1.widthHint = 404;
		sepLabel.setLayoutData(gridData_1);
		
	}
	
	public void createAliasMappingRowInScrolledComposite(Composite child, String dimName) {
		  
		  	final Button useGlobalSettingCheck = new Button(child, SWT.CHECK);

		  	useGlobalSettingCheck.setLayoutData(new GridData());

		  	useGlobalSettingCheck.setData(DIM_NAME_KEY, dimName);
		  	
		  	aliasMapping_useGlobalCheckButtonMap.put(dimName, useGlobalSettingCheck);
		  	
			final Label dimNameLabel = new Label(child, SWT.WRAP);
			dimNameLabel.setText(dimName);

			Combo aliasTableCombo = new Combo(child, SWT.READ_ONLY);
			aliasTableCombo.setText(Constants.DROPDOWN_GLOBAL_OPTION);
			final GridData gridData = new GridData(60, SWT.DEFAULT);
			aliasTableCombo.setLayoutData(gridData);

			aliasTableCombo.setData(DIM_NAME_KEY, dimName);
			
			if ( aliasTableNames != null ) {
				
				aliasTableCombo.setItems(aliasTableNames);
				
			}
			
			aliasMapping_aliasTableComboMap.put(dimName, aliasTableCombo);

			Combo primaryDisaplyFormatCombo = new Combo(child, SWT.READ_ONLY);
			primaryDisaplyFormatCombo.setText("Alias");
			final GridData gridData_5 = new GridData(60, SWT.DEFAULT);
			primaryDisaplyFormatCombo.setLayoutData(gridData_5);
			
			primaryDisaplyFormatCombo.setData(DIM_NAME_KEY, dimName);

			primaryDisaplyFormatCombo.setItems(new String[] { AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString()});
			
			aliasMapping_primaryDisplayFormatComboMap.put(dimName, primaryDisaplyFormatCombo);

			Combo additionalDisplayFormatCombo = new Combo(child, SWT.READ_ONLY);
			additionalDisplayFormatCombo.setText("Member");
			final GridData gridData_6_1 = new GridData(60, SWT.DEFAULT);
			additionalDisplayFormatCombo.setLayoutData(gridData_6_1);

			additionalDisplayFormatCombo.setData(DIM_NAME_KEY, dimName);
			
			//additionalDisplayFormatCombo.setItems(new String[] { "", AliasMemberDisplayType.Alias.toString(), AliasMemberDisplayType.Member.toString()});			
			
			aliasMapping_additionalDisplayFormatComboMap.put(dimName, additionalDisplayFormatCombo);
			
			final Label sepLabel = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
			final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
			gridData_1.widthHint = 404;
			sepLabel.setLayoutData(gridData_1);
		  
	}

	public void createMemberTagCommentVisibleRowInScrolledComposite(Composite child, String commentMemberTagName) {
			  	
		final Label dimNameLabel = new Label(child, SWT.WRAP);
		dimNameLabel.setText(commentMemberTagName);

		Combo visibleComment = new Combo(child, SWT.READ_ONLY);
		//visibleComment.setText(Constants.DROPDOWN_GLOBAL_OPTION);
		final GridData gridData = new GridData(60, SWT.DEFAULT);
		visibleComment.setLayoutData(gridData);
		visibleComment.setItems(this.trueFalseGlobalAr);
		visibleComment.setText(Constants.DROPDOWN_GLOBAL_OPTION);
				
		commentMemberTag_VisibleComboMap.put(commentMemberTagName, visibleComment);

		final Label sepLabel = new Label(child, SWT.SEPARATOR | SWT.HORIZONTAL);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gridData_1.widthHint = 404;
		sepLabel.setLayoutData(gridData_1);
	  
}

	
	private void initilizeForm() {


		initilizeOptionsForm();
		initilizeAliasMappingsForm();
		initilizeCommentMemberTagForm();
		initilizeGroupingForm();
			
	}

	private void initilizeOptionsForm() {
		
		if ( viewSection != null ) {
						
			initPrimaryFormattingAxis();

			//init read only combo
			boolean readOnly = viewSection.isReadOnly();
			if (readOnly) {
				readOnlyCombo.select(0);
			} else {
				readOnlyCombo.select(1);
			}

			//init hierarchy format combo
			if (viewSection.getHierarchyFormatName() != null) {
				hierarchyFormatCombo
						.setText(viewSection.getHierarchyFormatName());
			}
			
			if (viewSection.getCondtionalFormatName() != null) {
				conditionalFormatCombo
						.setText(viewSection.getCondtionalFormatName());
			}
			
			//init rrepeatHeaderRowCombo
			boolean rowHeaderRepeated = viewSection.isRowHeaderRepeated();
			if (rowHeaderRepeated) {
				repeatHeaderRowCombo.select(0);
			} else {
				repeatHeaderRowCombo.select(1);
			}
			
			//init repeatHeaderColCombo
			boolean colHeaderRepeated = viewSection.isColHeaderRepeated();
			if (colHeaderRepeated) {
				repeatHeaderColCombo.select(0);
			} else {
				repeatHeaderColCombo.select(1);
			}
			
			// suppress enabled
			if ( viewSection.getSuppressZeroSettings() != null && viewSection.getSuppressZeroSettings().getEnabled() != null) {
				EditorControlUtil.selectItem(suppressEnabledCombo, StringUtil.initCap(viewSection.getSuppressZeroSettings().getEnabled().toString()));
			} else {
				EditorControlUtil.selectItem(suppressEnabledCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
//			 suppress visible
			if ( viewSection.getSuppressZeroSettings() != null && viewSection.getSuppressZeroSettings().getVisible() != null) {
				EditorControlUtil.selectItem(suppressVisibleCombo, StringUtil.initCap(viewSection.getSuppressZeroSettings().getVisible().toString()));
			} else {
				EditorControlUtil.selectItem(suppressVisibleCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
//			suppress rows
			if ( viewSection.getSuppressZeroSettings() != null && viewSection.getSuppressZeroSettings().getRowsSuppressed() != null) {
				EditorControlUtil.selectItem(suppressRowsCombo, StringUtil.initCap(viewSection.getSuppressZeroSettings().getRowsSuppressed().toString()));
			} else {
				EditorControlUtil.selectItem(suppressRowsCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
//			suppress columns 
			if ( viewSection.getSuppressZeroSettings() != null && viewSection.getSuppressZeroSettings().getColumnsSuppressed() != null) {
				EditorControlUtil.selectItem(suppressColumnsCombo, StringUtil.initCap(viewSection.getSuppressZeroSettings().getColumnsSuppressed().toString()));
			} else {
				EditorControlUtil.selectItem(suppressColumnsCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
			
			
		}	
		
	}
	
	private void initilizeAliasMappingsForm() {
		
		for (String key : aliasMapping_useGlobalCheckButtonMap.keySet()) {
					
					if ( viewSectionAliasMappingsMap.containsKey(key)) {
						
						selectUseGlobalButton(key, false);
						
						enableAliasMappingRow(key, true);
														
						populateAliasMappingRowViaOptionGlobal(key, false);
						
					} else if ( globalAliasMappingsMap.containsKey(key)) {
		
						selectUseGlobalButton(key, true);
		
						enableAliasMappingRow(key, false);
						
						populateAliasMappingRowViaOptionGlobal(key, true);
						
					} else {
						//do something
					}
					
				}
		
	}
	
	/**
	 * Initilizes Comment Member Tag Form
	 *
	 */
	private void initilizeCommentMemberTagForm() {
		
		if ( viewSection.getMemberTagCommentEntries() != null ) {
			
			for (MemberTagCommentEntry commentMemberTagEntry : viewSection.getMemberTagCommentEntries()) {
																			
				if ( commentMemberTagMap.containsKey(commentMemberTagEntry.getName())) {
				
					populateCommentMemberTagRow(commentMemberTagEntry.getName(), commentMemberTagEntry.isVisible());
					
				}
													
			}
		
		}
	}
	
	private void initilizeGroupingForm() {
		if ( viewSection == null ) return;
		if ( viewSection.getGroupingSpecs() == null) return;
		
		for(GroupingSpec spec : viewSection.getGroupingSpecs()){
			if(spec == null) continue;
			//current dim
			String dimension = spec.getDimension();
			//current level
			Integer level = spec.getLevel();
			//type
			LevelGenType type = spec.getType();
			//current axis
			PafAxis axis = new PafAxis(spec.getAxis());
			switch(axis.getValue()){
				case PafAxis.COL:
					this.colAxisGroupDim = dimension;
					break;
				case PafAxis.ROW:
					this.rowAxisGroupDim = dimension;
					break;
			}
			//Get the associated controls.
			Button button = grouping_enabledCheckButtonMap.get(dimension);
			Spinner spin = grouping_levelGeneratioNumberSpinnerMap.get(dimension);
			Combo combo = grouping_levelGenerationComboMap.get(dimension);
	
			//Set the type and level
			if(type == null){
				EditorControlUtil.selectItem(combo, LevelGenerationType.Bottom.toString());
				spin.setSelection(0);
			} else {
				switch(type){
					case LEVEL:
						EditorControlUtil.selectItem(combo, LevelGenerationType.Level.toString());
						break;
					case GEN:
						EditorControlUtil.selectItem(combo, LevelGenerationType.Generation.toString());
						break;
				}
				spin.setSelection(level);
			}
			//Enable the row
			button.setSelection(true);
			//Enable the row items
			enableGroupingRow(dimension, axis, true);
			//Disable all other dimes on same axis
			for (Map.Entry<String, PafAxis> dm : dimensionLocation.entrySet()) {
				String dim = dm.getKey();
				if(!hierarchyDims.contains(dim)) continue;
				if(dim == dimension) continue;
		    	//Only care about dims on the same axis
		    	if(dm.getValue().equals(axis)){
		    		enableGroupingRow(dim, axis, false);
		    	}
			}
		}
		
	}
			
	private void addGroupingListeners() {
		
		for (Button useGlobalCheckButton : grouping_enabledCheckButtonMap.values()) {
			
			useGlobalCheckButton.addSelectionListener(new SelectionAdapter() {

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					Button b = (Button) selectionEvent.widget;
					
					boolean useMe = b.getSelection();
					
					String dimKey = (String) b.getData(DIM_NAME_KEY);
					
					
					if(!dimensionLocation.containsKey(dimKey)){
						return;
					}
					
					PafAxis axis = (PafAxis) dimensionLocation.get(dimKey);
					switch(axis.getValue()){
						case PafAxis.COL:
							if(useMe){
								colAxisGroupDim = dimKey;
							}else{
								colAxisGroupDim = "";
							}
								
							break;
						case PafAxis.ROW:
							if(useMe){
								rowAxisGroupDim = dimKey;
							}else{
								rowAxisGroupDim = "";
							}
							break;
					}
					
					Set<Map.Entry<String, PafAxis>> set = dimensionLocation.entrySet();

				    for (Map.Entry<String, PafAxis> dm : set) {
				    	//Current Dim
			    		String dim = dm.getKey();
			    		//If the dim is not a hier dim, then just keep going.
				    	if(!hierarchyDims.contains(dim)) continue;
				    	//Only care about dims on the same axis
				    	if(dm.getValue().equals(axis)){
				    		//if the user unchecked the box, the enable all on the same axis
				    		if(!useMe){
				    			enableGroupingRow(dim, axis, !useMe);
				    		} else {
				    			if(dim.equals(dimKey)){
				    				enableGroupingRow(dim, axis, useMe);
				    			}else {
				    				enableGroupingRow(dim, axis, !useMe);
				    			}
				    		}
				    	}
				    }
				}
				
			});
		}
		
		for (Combo primaryDisplayFormat : grouping_levelGenerationComboMap.values()	) {
			
			primaryDisplayFormat.addSelectionListener(new SelectionAdapter() {

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {
					
					Combo combo = (Combo) selectionEvent.widget;
					
					String levelGen = combo.getText();
					
					String dimKey = (String) combo.getData(DIM_NAME_KEY);
					
					LevelGenerationType type = LevelGenerationType.valueOf(levelGen);
					
					Spinner spinner = (Spinner) grouping_levelGeneratioNumberSpinnerMap.get(dimKey);	
					
					PaceTree tree = null;
					
					PafAxis axis = (PafAxis) dimensionLocation.get(dimKey);
					
					
					switch(axis.getValue()){
					case PafAxis.COL:
						tree = input.getColumnAxisTree();
						break;
					case PafAxis.ROW:
						tree = input.getRowAxisTree();
						break;
					}
					
					Map<String, Integer> dimTreeHeightMap = tree.getDimensionTreeHeightMap();
					
					Integer dimensionDepth = null;
					
					if ( dimKey != null && dimTreeHeightMap.containsKey(dimKey)) {
						
						dimensionDepth = dimTreeHeightMap.get(dimKey);
					}
					
					int minLevelGenSpinnerLength = 0;
					int maxLevelGenSpinnerLength = 0;
					boolean spinEnabled = true;
					
					switch(type){
						case Generation:
							minLevelGenSpinnerLength = 1;
							maxLevelGenSpinnerLength = dimensionDepth + 1;
							break;
						case Level:
							minLevelGenSpinnerLength = 0;
							maxLevelGenSpinnerLength = dimensionDepth;
							break;
						case Bottom:
							spinEnabled = false;
							break;
					}
					
					//get old max spinner values and log
					int oldMaxSpinnerValue = spinner.getMaximum();
									
					//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
					if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
						
						spinner.setMaximum(maxLevelGenSpinnerLength);
						spinner.setMinimum(minLevelGenSpinnerLength);			
						
					} else {
					
						spinner.setMinimum(minLevelGenSpinnerLength);
						spinner.setMaximum(maxLevelGenSpinnerLength);
						
					}
					spinner.setEnabled(spinEnabled);

				}				
				
			});
			
		}
		
	}
	
    private void addAliasMappingsListeners() {

        for (Button useGlobalCheckButton : aliasMapping_useGlobalCheckButtonMap.values()) {
                
                useGlobalCheckButton.addSelectionListener(new SelectionAdapter() {

                        /* (non-Javadoc)
                         * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
                         */
                        @Override
                        public void widgetSelected(SelectionEvent selectionEvent) {

                                Button useGlobalCheckButton = (Button) selectionEvent.widget;
                                
                                boolean useGlobal = useGlobalCheckButton.getSelection();
                                
                                String dimKey = (String) useGlobalCheckButton.getData(DIM_NAME_KEY);
                                
                                enableAliasMappingRow(dimKey, ! useGlobal);
                                        
                                populateAliasMappingRowViaOptionGlobal(dimKey, useGlobal);


                        }
                        
                });
                
        }
        
        for (Combo primaryDisplayFormat : aliasMapping_primaryDisplayFormatComboMap.values()        ) {
                
                primaryDisplayFormat.addSelectionListener(new SelectionAdapter() {

                        /* (non-Javadoc)
                         * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
                         */
                        @Override
                        public void widgetSelected(SelectionEvent selectionEvent) {
                                
                                Combo primaryDisplayFormatCombo = (Combo) selectionEvent.widget;
                                
                                String selectedPrimaryDisplayFormat = primaryDisplayFormatCombo.getText();
                                
                                String dimKey = (String) primaryDisplayFormatCombo.getData(DIM_NAME_KEY);
                                
                                Combo additonalDisplayFormatCombo = aliasMapping_additionalDisplayFormatComboMap.get(dimKey);
                                
                                String currentAdditionalDisplayFormat = additonalDisplayFormatCombo.getText();
                                
                                additonalDisplayFormatCombo.setItems(defaultAdditonalItems);
                                
                                additonalDisplayFormatCombo.remove(selectedPrimaryDisplayFormat);
                                
                                additonalDisplayFormatCombo.setText(currentAdditionalDisplayFormat);
                                
                        }                                
                        
                });
                
        }
        
}

	
	private void enableAliasMappingRow(String key, boolean isEnabled) {
		
		aliasMapping_aliasTableComboMap.get(key).setEnabled(isEnabled);
		aliasMapping_primaryDisplayFormatComboMap.get(key).setEnabled(isEnabled);
		aliasMapping_additionalDisplayFormatComboMap.get(key).setEnabled(isEnabled);				
		
	}

	
	private void enableGroupingRow(String key, PafAxis axis, boolean isEnabled) {
		
		grouping_enabledCheckButtonMap.get(key).setEnabled(isEnabled);
		
		boolean adjustedStatus = false;
		boolean labelStatus = false;
		switch(axis.getValue()){
		case PafAxis.COL:
			if(colAxisGroupDim == key){
				adjustedStatus = true;
				labelStatus = true;
			}else if(colAxisGroupDim == ""){
				adjustedStatus = false;
				labelStatus = true;
			}else{
				labelStatus = false;
			}
				
			break;
		case PafAxis.ROW:
			if(rowAxisGroupDim == key){
				adjustedStatus = true;
				labelStatus = true;
			}else if(rowAxisGroupDim == ""){
				adjustedStatus = false;
				labelStatus = true;
			}else{
				labelStatus = false;
			}
			break;
		}
		
		grouping_levelGenerationComboMap.get(key).setEnabled(adjustedStatus);
		
		Combo typeCombo = grouping_levelGenerationComboMap.get(key);
		String typeText = typeCombo.getText().trim();
		if(typeText.equals(LevelGenerationType.Bottom.toString())) {
			grouping_levelGeneratioNumberSpinnerMap.get(key).setEnabled(false);	
		} else{
			grouping_levelGeneratioNumberSpinnerMap.get(key).setEnabled(adjustedStatus);	
		}
	
		grouping_dimNameLabel.get(key).setEnabled(labelStatus);	
		grouping_axisLabel.get(key).setEnabled(labelStatus);	
		
	}

	public GroupingSpec getGroupingSpec(String dimension){
		GroupingSpec spec = new GroupingSpec();
		//Set the dim name
		spec.setDimension(dimension);
		//Get set the axis
		PafAxis axis = (PafAxis)dimensionLocation.get(dimension);
		
		switch(axis.getValue()){
			case PafAxis.COL:
				spec.setAxis(PafAxis.COL);
				break;
			case PafAxis.ROW:
				spec.setAxis(PafAxis.ROW);
				break;
		}
		
		//Get/set the type.
		Combo typeCombo = grouping_levelGenerationComboMap.get(dimension);
		Spinner levelSpinner = grouping_levelGeneratioNumberSpinnerMap.get(dimension);
		
		//switch(type.getText()){
		String typeText = typeCombo.getText().trim();
		LevelGenType type = null;
		Integer level = null;
		if(typeText.equals(LevelGenerationType.Level.toString())) {
			type = LevelGenType.LEVEL;
			level = levelSpinner.getSelection();
		} else if(typeText.equals(LevelGenerationType.Generation.toString())) {
			type = LevelGenType.GEN;
			level = levelSpinner.getSelection();
		}

		
		spec.setType(type);
		spec.setLevel(level);
		
		return spec;
	}
	
	private void selectUseGlobalButton(String key, boolean isSelected) {

		Button useGlobalButton = aliasMapping_useGlobalCheckButtonMap.get(key);
		
		if ( useGlobalButton != null ) {
			
			useGlobalButton.setSelection(isSelected);
		}
		
	}
	
	private void populateAliasMappingRowViaOptionGlobal(String key, boolean useGlobal) {
	
		if ( useGlobal ) {
			if ( globalAliasMappingsMap.containsKey(key) ) {
				populateAliasMappingRow(key, globalAliasMappingsMap.get(key));
			} 
		} else {
		
			if ( viewSectionAliasMappingsMap.containsKey(key)) {
				populateAliasMappingRow(key, viewSectionAliasMappingsMap.get(key));				
			}

		}
		
	}

	private void populateCommentMemberTagRow(String memberTagName, boolean isVisible) {
		
		if ( memberTagName != null ) {
		
			if ( commentMemberTag_VisibleComboMap.containsKey(memberTagName)) {
				
				commentMemberTag_VisibleComboMap.get(memberTagName).setText(StringUtil.initCap(Boolean.toString(isVisible)));
				
			}
			
		}
		
	}
	
	private void populateAliasMappingRow(String key, AliasMapping mapping) {
		
		aliasMapping_aliasTableComboMap.get(key).setText(mapping.getAliasTableName());
		
		if ( aliasMapping_aliasTableComboMap.get(key).getText().trim().length() == 0 ) {
			
			aliasMapping_aliasTableComboMap.get(key).setText(PafBaseConstants.ALIAS_TABLE_DEFAULT);
			
		}
		
		aliasMapping_primaryDisplayFormatComboMap.get(key).setText((mapping.getPrimaryRowColumnFormat() == null) ? DEFAULT_DISPLAY_FORMAT : mapping.getPrimaryRowColumnFormat());
		
		String primaryDisplayFormat = aliasMapping_primaryDisplayFormatComboMap.get(key).getText();
		
		if ( primaryDisplayFormat.trim().equals("")) {
			
			aliasMapping_primaryDisplayFormatComboMap.get(key).setText(DEFAULT_DISPLAY_FORMAT);
			
			primaryDisplayFormat = DEFAULT_DISPLAY_FORMAT;
			
		}
		
		aliasMapping_additionalDisplayFormatComboMap.get(key).setItems(defaultAdditonalItems);
		
		aliasMapping_additionalDisplayFormatComboMap.get(key).remove(primaryDisplayFormat);
		
		aliasMapping_additionalDisplayFormatComboMap.get(key).setText((mapping.getAdditionalRowColumnFormat() == null) ? "" : mapping.getAdditionalRowColumnFormat());
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(708, 562);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("View Section Options");
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {

			setViewSectionProperties();
			
		}
		super.buttonPressed(buttonId);
	}

	private void setViewSectionProperties() {
		
		String primaryFormatAxis = primaryFormattingAxisCombo.getText();

		if (primaryFormatAxis.equals(ROW_TEXT)) {
			viewSection.setPrimaryFormattingAxis(PafAxis.ROW);
		} else if (primaryFormatAxis.equals(COL_TEXT)) {
			viewSection.setPrimaryFormattingAxis(PafAxis.COL);
		}

		viewSection.setReadOnly(Boolean.parseBoolean(readOnlyCombo.getText()));

		String hierarchyFormatName = hierarchyFormatCombo.getText();

		if (hierarchyFormatName.equals("")) {
			hierarchyFormatName = null;
		}
		viewSection.setHierarchyFormatName(hierarchyFormatName);
		
		String conditionalFormatName = conditionalFormatCombo.getText();

		if (conditionalFormatName.equals("")) {
			conditionalFormatName = null;
		}
		viewSection.setCondtionalFormatName(conditionalFormatName);
		
		viewSection.setRowHeaderRepeated(Boolean.parseBoolean(repeatHeaderRowCombo.getText()));
		viewSection.setColHeaderRepeated(Boolean.parseBoolean(repeatHeaderColCombo.getText()));

		SuppressZeroSettings suppressZeros = viewSection.getSuppressZeroSettings();
		if ( suppressZeros == null ) {
			
			suppressZeros = new SuppressZeroSettings();
			
		}
		viewSection.setSuppressZeroSettings(suppressZeros);
		
		suppressZeros.setVisible(dropdownGlobalBooleanMap.get(suppressVisibleCombo.getText()));
		suppressZeros.setEnabled(dropdownGlobalBooleanMap.get(suppressEnabledCombo.getText()));
		suppressZeros.setRowsSuppressed(dropdownGlobalBooleanMap.get(suppressRowsCombo.getText()));
		suppressZeros.setColumnsSuppressed(dropdownGlobalBooleanMap.get(suppressColumnsCombo.getText()));
				
		viewSection.setAliasMappings(getAliasMappings());
		
		viewSection.setMemberTagCommentEntries(getSelectedCommentMemberTagEntries());	
		
		//TTN-2223
		List<GroupingSpec> groups = new ArrayList<GroupingSpec>(2);
		if(rowAxisGroupDim != null && rowAxisGroupDim != ""){
			groups.add(getGroupingSpec(rowAxisGroupDim));
		}
		if(colAxisGroupDim != null && colAxisGroupDim != ""){
			groups.add(getGroupingSpec(colAxisGroupDim));
		}
		
		if(groups != null && groups.size() > 0){
			viewSection.setGroupingSpecs(groups.toArray(new GroupingSpec[groups.size()]));
		} else {
			viewSection.setGroupingSpecs(null);
		}
			
	}

	/**
	 * Gets the selected comment member tag entries from the dynamic combo boxes that have
	 * a value of true or false.
	 * 
	 * @return an array of member tag comment entries
	 */
	private MemberTagCommentEntry[] getSelectedCommentMemberTagEntries() {
		
		List<MemberTagCommentEntry> selectedCommentMemberTagEntryList = new ArrayList<MemberTagCommentEntry>();
		
		for (String memberTagKey : commentMemberTag_VisibleComboMap.keySet()) {
			
			Combo memberTagCommentVisibleCombo = commentMemberTag_VisibleComboMap.get(memberTagKey);
			
			//if the member tag comment visible text is not Default
			if ( ! memberTagCommentVisibleCombo.getText().equalsIgnoreCase(Constants.DROPDOWN_GLOBAL_OPTION) ) {
				
				//get boolean value
				boolean memberTagCommentVisible = Boolean.parseBoolean(memberTagCommentVisibleCombo.getText());
				
				//create new member tag comment entry
				MemberTagCommentEntry mtce = new MemberTagCommentEntry(memberTagKey, memberTagCommentVisible);
				
				//add to selected comment member tag entry list
				selectedCommentMemberTagEntryList.add(mtce);
								
			}
			
		}
		
		//if size is 0, return null, otherwise convert list to array
		if ( selectedCommentMemberTagEntryList.size() == 0 ) {
			
			return null;
			
		} else {
			
			return selectedCommentMemberTagEntryList.toArray(new MemberTagCommentEntry[0]);
			
		}
		
		
	}

	private AliasMapping[] getAliasMappings() {
	
		for (Button useGlobalCheckButton : aliasMapping_useGlobalCheckButtonMap.values() ) {
					
			String dimKey = (String) useGlobalCheckButton.getData(DIM_NAME_KEY);

			//if use global setting
			if ( useGlobalCheckButton.getSelection()) {
				
				if ( viewSectionAliasMappingsMap.containsKey(dimKey ) ) {
					
					viewSectionAliasMappingsMap.remove(dimKey);
				}

			} else {
				
				AliasMapping newAliasMapping = new AliasMapping();
				
				newAliasMapping.setDimName(dimKey);
				newAliasMapping.setAliasTableName(aliasMapping_aliasTableComboMap.get(dimKey).getText());
				newAliasMapping.setPrimaryRowColumnFormat(aliasMapping_primaryDisplayFormatComboMap.get(dimKey).getText());
				newAliasMapping.setAdditionalRowColumnFormat(aliasMapping_additionalDisplayFormatComboMap.get(dimKey).getText());
				
				viewSectionAliasMappingsMap.put(dimKey, newAliasMapping);
				
			}
			
		}
		
		if ( viewSectionAliasMappingsMap.size() > 0 ) {
			return viewSectionAliasMappingsMap.values().toArray(new AliasMapping[0]);
		}
		
		return null;
		
	}

	
	/**
	 * @return the viewSection
	 */
	public PafViewSectionUI getViewSection() {
		return viewSection;
	}
	
	private String[] populateGenerationFormats() {

		return generationFormattingManager.getKeys();

	}
	
	private String[] populateCondtionalFormats() {

		return condFormatManager.getKeys();

	}
	
	/**
	 * 
	 * View Section to perform logic on
	 *
	 * @param section
	 * @return
	 */
	private int getMeasureVersionAxis(PafViewSection section) {

		int measureVersionAxis = 0;

		PageTuple[] pageTuples = section.getPageTuples();

		// measure dim via paf apps
		String measureDim = DimensionUtil.getMeasureDimensionName(project);
		String versionDim = DimensionUtil.getVersionDimensionName(project);

		if (pageTuples != null) {

			for (PageTuple pageTuple : pageTuples) {
				if (pageTuple.getAxis().equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_PAGE_AXIS;
				} else if (pageTuple.getAxis().equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_PAGE_AXIS;
				}
			}
		}

		String[] colDims = section.getColAxisDims();

		if (colDims != null) {
			for (String colDim : colDims) {
				if (colDim.equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_COL_AXIS;
				} else if (colDim.equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_COL_AXIS;
				}
			}
		}

		String[] rowDims = section.getRowAxisDims();

		if (rowDims != null) {
			for (String rowDim : rowDims) {
				if (rowDim.equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_ROW_AXIS;
				} else if (rowDim.equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_ROW_AXIS;
				}
			}
		}
	//}
	return measureVersionAxis;

	}
	
	/**
	 * 
	 *  Inits the primary formatting axis.  Populates and sets logic based on 
	 *  page, row and column meansure, version dims.
	 *
	 */
	private void initPrimaryFormattingAxis() {

		if (viewSection != null) {
			
			//init primary formatting axis
			int priFormatAxis = viewSection.getPrimaryFormattingAxis();

			if (priFormatAxis == PafAxis.ROW) {
				primaryFormattingAxisCombo.select(0);
			} else if (priFormatAxis == PafAxis.COL) {
				primaryFormattingAxisCombo.select(1);
			}
			
			if (viewSection.getPageAxisDims() == null 
					&& viewSection.getColAxisDims() == null
					&& viewSection.getRowAxisDims() == null) {
				primaryFormattingAxisCombo.setEnabled(false);
				
			} else {
				primaryFormattingAxisCombo.setEnabled(true);
			}
			
			int measureVersionAxis = getMeasureVersionAxis(viewSection
					.getPafViewSection());

			switch (measureVersionAxis) {

			case (PafViewSection.MEASURE_PAGE_AXIS | PafViewSection.VERSION_PAGE_AXIS):

				primaryFormattingAxisCombo.setEnabled(true);

				break;

			case (PafViewSection.MEASURE_COL_AXIS | PafViewSection.VERSION_COL_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.COL);
				break;

			case (PafViewSection.MEASURE_ROW_AXIS | PafViewSection.VERSION_ROW_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.ROW);
				break;

			case (PafViewSection.MEASURE_COL_AXIS | PafViewSection.VERSION_ROW_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.COL);
				break;

			case (PafViewSection.MEASURE_ROW_AXIS | PafViewSection.VERSION_COL_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.ROW);
				break;

			case (PafViewSection.MEASURE_PAGE_AXIS | PafViewSection.VERSION_COL_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.COL);
				break;

			case (PafViewSection.MEASURE_PAGE_AXIS | PafViewSection.VERSION_ROW_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.ROW);
				break;

			case (PafViewSection.VERSION_PAGE_AXIS | PafViewSection.MEASURE_COL_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.COL);
				break;

			case (PafViewSection.VERSION_PAGE_AXIS | PafViewSection.MEASURE_ROW_AXIS):

				primaryFormattingAxisCombo.setEnabled(false);
				primaryFormattingAxisCombo.select(PafAxis.ROW);

				break;

			}

		}
	}
}

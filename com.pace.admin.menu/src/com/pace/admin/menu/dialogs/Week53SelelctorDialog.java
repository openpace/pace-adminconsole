/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.composite.FindTreeModule;
import com.pace.admin.menu.editors.input.ProjectSettingsEditorInput;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.utility.StringUtils;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.swtdesigner.ResourceManager;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Label;

public class Week53SelelctorDialog extends Dialog {
	private static Logger logger = Logger.getLogger(Week53SelelctorDialog.class);
	private ProjectSettingsEditorInput projectSettingsInput;
	private Tree periods;
	private List<String> week53MembersSelected = new ArrayList<String>();
	private HashMap<String, PafSimpleDimMember>  treeHashMap;
	private PafSimpleDimTree pafSimpleTree;
	
	public Week53SelelctorDialog(Shell parentShell, ProjectSettingsEditorInput projectSettingsInput) {
		super(parentShell);
		this.projectSettingsInput = projectSettingsInput;
		if( projectSettingsInput.getWeek53MembersSelected() != null ) {
			week53MembersSelected.addAll(projectSettingsInput.getWeek53MembersSelected());
		}
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Week 53 Selector");
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));
		
		final FindTreeModule ftm = new FindTreeModule(container, SWT.NONE);
		ftm.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		Button btnClear = new Button(container, SWT.CENTER);
		btnClear.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearSelectedTreeItem(periods.getItem(0));
				week53MembersSelected.clear();
            	projectSettingsInput.setSelectorModified(true);
			}
		});
		btnClear.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/remove_all.gif"));
		btnClear.setToolTipText("Clear All Selections");

		final Composite composite_1 = new Composite(container, SWT.NONE);
		final GridData gd_composite_1 = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_composite_1.widthHint = 120;
		composite_1.setLayoutData(gd_composite_1);
		composite_1.setLayout(new GridLayout());
		
		periods = new Tree(composite_1, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		periods.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		ftm.setTree(periods);
		
		periods.addListener(SWT.Selection, new Listener() {
	        public void handleEvent(Event event) {
	            if (event.detail == SWT.CHECK) {
	                TreeItem item = (TreeItem) event.item;
	                boolean checked = item.getChecked();
                	item.setChecked(checked);
                	if( checked ) {
    					week53MembersSelected.add(item.getText());
                	}
                	else {
                		week53MembersSelected.remove(item.getText());
                	}
                	projectSettingsInput.setSelectorModified(true);
	            }
	        }
	    });
		
		//load time dimension
		createTreeModel(periods);
		new Label(container, SWT.NONE);
		EditorControlUtil.expandTree(periods.getItems());
		//set week 53 members
		if( week53MembersSelected != null && week53MembersSelected.size() != 0 
				&& periods.getItems().length > 0 ) {
			for( String member : week53MembersSelected ){
				setTreeItem(member, periods.getItem(0));
			}
		}

		return container;
	}

	protected boolean isCheckable(String item) {
		if( pafSimpleTree != null ) {
			PafSimpleDimMember member = (PafSimpleDimMember) treeHashMap.get(item);

			int level = member.getPafSimpleDimMemberProps().getLevelNumber();
			if( level == 0 || level == 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Build the contents of a tree control and populate it.
	 * @param tree The tree control to build.
	 */
	private void createTreeModel(Tree tree) {
		logger.debug("Create Tree Model start");
		
		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();
		
		try {

			pafSimpleTree = projectSettingsInput.getPafSimpleTreeTime();
			
			if( pafSimpleTree == null ) {
				String outMessage = "Couldn't get cached simple tree. Login into server and cache dimensions";
				GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
			}
			
			logger.debug("Converting TreeInto hash start");

			treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(pafSimpleTree);

			logger.debug("Converting TreeInto hash end");

			String timeDimName = projectSettingsInput.getTimeDimName();
			if( timeDimName != null ) {
				PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(timeDimName);
				logger.info("MAX LEVEL: "
						+ rootMember.getPafSimpleDimMemberProps().getLevelNumber());
	
				logger.debug("Building Tree start");
	
				addTreeNode(treeHashMap, tree, null, rootMember, timeDimName);
	
				cachedTreeMap.put(timeDimName, tree);
				logger.debug("Building Tree end");
			}

		}catch(Exception e){
			logger.error(e.getMessage());
		}
		logger.debug("Create Tree Model end");
	}
	
	/**
	 * Adds a tree node to a tree.
	 * @param treeHashMap The hash map containing the tree components.
	 * @param tree The tree to add the nodes to.
	 * @param parent The parent tree item to have the nodes added to.
	 * @param member The paf simple memeber to add to the parent.
	 * @param dimension The dimension of this tree.
	 */
	private void addTreeNode(HashMap<String, PafSimpleDimMember>  treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		if (member.getChildKeys().size() > 0 ) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap.get(child);

				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}
	}

	/**
	 * Set the tree items in a tree checked.
	 * @param textToSelect The text to select.
	 * @param item The tree item to search.
	 */
	private void setTreeItem(String week53Member, TreeItem treeItem) {
		if (treeItem != null) {
			if( treeItem.getText().equalsIgnoreCase(week53Member)) {
				treeItem.setChecked(true);
				periods.setSelection(treeItem);
				return;
			}
		    else {
		        TreeItem[] items = treeItem.getItems();
				if (items != null && items.length != 0) {
					for (TreeItem item : items) {
						if( item.getText().equalsIgnoreCase(week53Member)) {
							if( ! isItemFoundChecked(week53Member, periods.getItem(0)) ) {
								item.setChecked(true);
								periods.setSelection(item);
								return;
							}
						}
						else {
							setTreeItem(week53Member, item);
						}
		            }
		        }	
	    	}
	    }
	}
	
	private boolean isItemFoundChecked(String member, TreeItem treeItem) {
		TreeItem[] items = treeItem.getItems();
		for (TreeItem item : items) {
			if( item.getText().equalsIgnoreCase(member)) {
				boolean checked = item.getChecked();
				if( checked ) {
					 return true;
				}
			}
			if( isItemFoundChecked(member, item) == true )
				return true;
		}
		return false;
	}

	private void clearSelectedTreeItem(TreeItem treeItem) {
		if (treeItem != null) {
            boolean checked = treeItem.getChecked();
            if( checked ) {
            	treeItem.setChecked(false);
            }
        	TreeItem[] items = treeItem.getItems();
			if (items != null && items.length != 0) {
				for (TreeItem item : items) {
	                checked = item.getChecked();
	                if( checked ) {
	                	item.setChecked(false);
	                }
					clearSelectedTreeItem(item);
	            }
	        }
 		}
	}
	
	//getCheck() not working	
//	private void getSelectedTreeItem(TreeItem treeItem) {
//        TreeItem[] items = treeItem.getItems();
//		if (items != null && items.length != 0) {
//			for (TreeItem item : items) {
//				if( item.getChecked()) {
//					week53MembersSelected.add(item.getText());
//				}
//				getSelectedTreeItem(item);
//            }
//        }	
//	}
	
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID && projectSettingsInput.isSelectorModified() ) {
			
			String outMessage = null;
			
			if( week53MembersSelected.size() > 1  ) {
				//validate week 53 members
				//If More than one same Week 53 member has been selected
				List<String> wk53List = new ArrayList<String>(week53MembersSelected);
				Collections.sort(wk53List);
				for( int i=1; i<wk53List.size(); i++ ) {
					if( wk53List.get(i-1) == wk53List.get(i)) {
						outMessage = "Can not save changes, the following 'Week 53' members are selected more than once: " + wk53List.get(i) + ".";
						GUIUtil.openMessageWindow("Validation Error(s)", outMessage, SWT.ERROR);				
						return;
					}
				}

				//IF they are at different level 
				List<Integer> levels = new ArrayList<Integer>();
				for( String item : week53MembersSelected ) {
					PafSimpleDimMember member = (PafSimpleDimMember) treeHashMap.get(item);
					if( member != null ) {
						int level = member.getPafSimpleDimMemberProps().getLevelNumber();
						levels.add(level);
					}
				}
				Collections.sort(levels);
				boolean diffLevelFound = false, sameLevelFound = false;
				for( int i=1; i<levels.size(); i++ ) {
					if( levels.get(i-1) != levels.get(i)) {
						diffLevelFound = true;
						break;
					}
				}
				for( int i=1; i<levels.size(); i++ ) {
					if( levels.get(i-1) == levels.get(i)) {
						sameLevelFound = true;
						break;
					}
				}
				
				String strMem = StringUtils.arrayListToString(week53MembersSelected, ",");
				//they are at different level and same level as well
				if( diffLevelFound && sameLevelFound ) {
					outMessage = "More than one level and more than one 'Week 53' member have been selected. Please verify that all these selections are valid: " + strMem;
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
				//they are at the different level
				else if( diffLevelFound && ! sameLevelFound ) {
					outMessage = "More than one level 'Week 53' members have been selected. Please verify that all these selections are valid: " + strMem;
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
				//they are at the same level
				else if( ! diffLevelFound && sameLevelFound ) {
					outMessage = "More than one 'Week 53' members have been selected. Please verify that all these selections are valid: " + strMem ;
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
			}
			
			//saving the selections
			projectSettingsInput.setWeek53MembersSelected(new HashSet<String>(week53MembersSelected));
			
			// Close the dialog
			this.close();
			return;
		}
		
		super.buttonPressed(buttonId);
		
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.List;

import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.base.view.ViewTuple;

public class CreatingPaceTreeFromViewTuplesUtility {

	/**
	 *  Method_description_goes_here
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		ViewTuple vt1 = new ViewTuple();
		vt1.setMemberDefs(new String[] { "a", "b", "c"});
		
		ViewTuple vt2 = new ViewTuple();
		vt2.setMemberDefs(new String[] { "a", "b", "d"});
		
		ViewTuple vt3 = new ViewTuple();
		vt3.setMemberDefs(new String[] { "a", "c", "c"});
		
		ViewTuple vt4 = new ViewTuple();
		vt4.setMemberDefs(new String[] { "b", "a", "b"});
		
		
		List<ViewTuple> viewTupleList = new ArrayList<ViewTuple>();
		
		viewTupleList.add(vt1);
		viewTupleList.add(vt2);
		viewTupleList.add(vt3);
		viewTupleList.add(vt4);
		
		PaceTreeNode root = new PaceTreeNode("Root", new PaceTreeNodeProperties(), null);
		
		List<PaceTreeNode> nodeList = new ArrayList<PaceTreeNode>();
		
		for (ViewTuple vt : viewTupleList) {					
								
			PaceTreeNode newNode = createAdditionalNode(root, vt, 0);
			
			//System.out.println(newNode);
			
			if ( nodeList.size() == 0 || (! nodeList.get(nodeList.size() -1).getName().equals(newNode.getName()))) {
			
				nodeList.add(newNode);
				
			}
								
		}
		
		System.out.println(nodeList);

	}

	private static PaceTreeNode createAdditionalNode(PaceTreeNode parent, ViewTuple vt,
			int ndx) {

		PaceTreeNode gen1 = null;
		
		if ( parent.hasChildren() && parent.getChildren().get(parent.getChildren().size()-1).getName().equals(vt.getMemberDefs()[ndx]) ) {
							
			gen1 = parent.getChildren().get(parent.getChildren().size()-1);
			
		} else {
			
			gen1 = new PaceTreeNode(vt.getMemberDefs()[ndx], new PaceTreeNodeProperties(), parent);
			
		}
		
		if ( ++ndx != vt.getMemberDefs().length ) {
			
			createAdditionalNode(gen1, vt, ndx);
			
		}
		
		return gen1;
		
	}

}


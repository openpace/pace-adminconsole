/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.constants.ErrorConstants;
import com.pace.admin.global.exceptions.InvalidDimensionsException;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.model.managers.HierarchyFormattingModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.input.ViewSectionTuplesDialogInput;
import com.pace.base.view.PafViewHeader;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;
import com.swtdesigner.ResourceManager;

/**
 * 
 * Allows a user to manage view sections.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ViewSectionDialog extends Dialog {

	private Button dialogOkButton;
	
	private Button viewSectionOptionsButton;
	
	private String initialViewName = null;
	
	private static Logger logger = Logger.getLogger(ViewSectionDialog.class);

	private PafViewSectionUI clonedViewSection = null;

	private ArrayList<PafViewHeader> localHeaders;

	private Table headerTable;

	private Text viewSectionDesc;

	private Text viewSectionName;

	private ArrayList<String> viewSectionList;

	private Button assignDimensionsButton;

	private Button assignMembersButton;

	private Button createViewButton;

	private Button newHeaderButton;

	private Button editHeaderButton;

	private Button deleteHeaderButton;

	private Group viewSectionGroup;

	private Button upHeaderButton;

	private Button downHeaderButton;
	
	private Text pageAxisText;

	private Text columnAxisText;

	private Text rowAxisText;

	private Label errorLabel;

	private static final String DELETE_BUTTON = "Delete";

	private static final int NEW_VIEW_SECTION_BUTTON_ID = 10;

	private static final int COPY_VIEW_SECTION_BUTTON_ID = 11;

	private static final int DELETE_VIEW_SECTION_BUTTON_ID = 12;

	private static final int NEW_HEADER_BUTTON_ID = 13;

	private static final int EDIT_HEADER_BUTTON_ID = 14;

	private static final int DELETE_HEADER_BUTTON_ID = 15;

	private static final int ADD_VIEW_SECTION_BUTTON_ID = 16;

	private static final int CANCEL_VIEW_SECTION_BUTTON_ID = 17;

	private static final int EXISTING_VIEW_STATE = 0;

	private static final int NEW_VIEW_STATE = 1;

	private static final int CLONED_VIEW_STATE = 2;

	private static final int UNKNOWN_VIEW_STATE = 3;

	private static final int HEADER_UP_ID = -1;

	private static final int HEADER_DOWN_ID = 1;

	private static final String ROW_TEXT = "Row";

	private static final String COL_TEXT = "Column";

	private static final String TRUE = "True";

	private static final String FALSE = "False";

	private ViewModelManager viewManager = null;

	private ViewSectionModelManager viewSectionManager = null;

	private HierarchyFormattingModelManager generationFormattingManager = null;

	private Shell parentShell = null;

	boolean openWithNewView = false;
	
	private boolean copyToNewView = false;

	private IProject project;

	private boolean createdNewView = false;
	
	/**
	 * 
	 * @param parentShell
	 * @param project
	 * @param initialViewName
	 * @wbp.parser.constructor
	 */
	public ViewSectionDialog(Shell parentShell, IProject project,
			String initialViewName) {

		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.project = project;
		this.parentShell = parentShell;
		this.initialViewName = initialViewName;

		viewSectionList = new ArrayList<String>();
		
		// Create view section Manager
		viewSectionManager = new ViewSectionModelManager(project);

		viewManager = new ViewModelManager(project);

		// broke for now
		generationFormattingManager = new HierarchyFormattingModelManager(project);
	}
	
	/**
	 * 
	 * @param parentShell
	 * @param project
	 * @param initialViewName
	 * @param openWithNewView - true when no view section exists and user wants to create a new view section
	 */
	public ViewSectionDialog(Shell parentShell, IProject project,
			String initialViewName, boolean openWithNewView) {
		this(parentShell, project, initialViewName);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.openWithNewView = openWithNewView;
	}

	/**
	 * 
	 * @param parentShell Parents shell
	 * @param project Project
	 * @param copyToNewView Copy the view to a new view.
	 * @param copyFromView The source view to copy.
	 */
	public ViewSectionDialog(Shell parentShell, IProject project, 
			boolean copyToNewView,
			String copyFromView) {
		this(parentShell, project, copyFromView);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.initialViewName = copyFromView;
		this.openWithNewView = false;
		this.copyToNewView = copyToNewView;
	}
	

	protected Control createDialogArea(Composite parent) {

		Composite container = new Composite(parent, SWT.NONE);
		final GridData containerGridArea = new GridData(SWT.FILL,SWT.FILL, true, true);
		containerGridArea.heightHint = 800;
		container.setLayoutData(containerGridArea);
		// container.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_CYAN));
		GridLayout containerLayout = new GridLayout();
		containerLayout.numColumns = 2;
		container.setLayout(containerLayout);

		final Composite viewFormArea = new Composite(container, SWT.NONE);

		final GridData viewFormGridArea = new GridData(SWT.FILL,
				SWT.TOP, true, true, 2, 1);
		viewFormGridArea.heightHint = 800;
		// gridData.heightHint = 585;
		// viewFormArea.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GREEN));

		// viewFormGridArea.heightHint = 555;
		viewFormGridArea.widthHint = 562;
		final GridLayout viewFormLayout = new GridLayout();
		viewFormLayout.marginTop = 12;
		viewFormArea.setLayout(viewFormLayout);
		viewFormArea.setLayoutData(viewFormGridArea);

		/*
		 * TableColumn alignmentColumnHeader = new TableColumn(headerTable,
		 * SWT.NONE); alignmentColumnHeader.setText("Alignment");
		 * alignmentColumnHeader.setWidth(80);
		 */
		/*
		 * final GridData gridData_11 = new GridData(GridData.CENTER,
		 * GridData.CENTER, false, false, 2, 1); gridData_11.heightHint = 33;
		 * gridData_11.widthHint = 209;
		 * composite_2.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		 * composite_2.setLayoutData(gridData_11);
		 */

		viewSectionGroup = new Group(viewFormArea, SWT.NONE);
		viewSectionGroup.setText("View Section");
		// viewSectionGroup.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_YELLOW));

		final GridData gridData_2 = new GridData(SWT.FILL,
				SWT.FILL, true, true);
		gridData_2.heightHint = 700;
		viewSectionGroup.setLayoutData(gridData_2);
		final GridLayout viewGroupLayout = new GridLayout();
		viewGroupLayout.makeColumnsEqualWidth = true;
		viewSectionGroup.setLayout(viewGroupLayout);

		final Composite infoComposite = new Composite(viewSectionGroup, SWT.NONE);
		final GridData gd_infoComposite = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_infoComposite.heightHint = 77;
		infoComposite.setLayoutData(gd_infoComposite);
		final GridLayout gridLayout_8 = new GridLayout();
		gridLayout_8.numColumns = 3;
		infoComposite.setLayout(gridLayout_8);

		final Label nameLabel = new Label(infoComposite, SWT.NONE);
		nameLabel.setAlignment(SWT.RIGHT);
		nameLabel.setText("Name");

		errorLabel = new Label(infoComposite, SWT.NONE);
		errorLabel.setVisible(false);
		errorLabel.setToolTipText("A View Section with that name already exists.");
		errorLabel.setText("_");
		ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor(Constants.SYMBOL_ERROR_ICON_PATH);
		if (imageDesc != null ) {
			errorLabel.setImage(imageDesc.createImage());
		}
		
		viewSectionName = new Text(infoComposite, SWT.READ_ONLY | SWT.BORDER);
		viewSectionName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		viewSectionName.setTextLimit(150);
		viewSectionName.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				String key = viewSectionName.getText().trim();
				if(viewSectionName.getEditable()){
					if (viewSectionManager.contains(key) || key.length() == 0 || key.startsWith(".")) {
						if(dialogOkButton != null){
							dialogOkButton.setEnabled(false);
						}
//						if(dialogApplyButton != null){
//							dialogApplyButton.setEnabled(false);
//						}
						errorLabel.setVisible(true);
						assignDimensionsButton.setEnabled(false);
						//addButton.setEnabled(false);
	
					} else {
						if(dialogOkButton != null){
							dialogOkButton.setEnabled(true);
						}
//						if(dialogApplyButton != null){
//							dialogApplyButton.setEnabled(true);
//						}
						errorLabel.setVisible(false);
						assignDimensionsButton.setEnabled(true);
						//addButton.setEnabled(true);
					}
				}
			}
		});

		final Label descriptionLabel = new Label(infoComposite, SWT.NONE);
		descriptionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		descriptionLabel.setText("Description");
		new Label(infoComposite, SWT.NONE);

		viewSectionDesc = new Text(infoComposite, SWT.V_SCROLL | SWT.BORDER
				| SWT.WRAP);
		viewSectionDesc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		viewSectionDesc.setTextLimit(500);
		viewSectionDesc.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				// do nothing
			}

			public void focusLost(FocusEvent e) {
				saveCurrentViewState();
			}
		});

		final Group headersGroup = new Group(viewSectionGroup, SWT.NONE);
		headersGroup.setText("Headers");
		final GridData gridData_5 = new GridData(SWT.FILL, SWT.FILL, false, true);
		gridData_5.heightHint = 168;
		gridData_5.widthHint = 528;
		headersGroup.setLayoutData(gridData_5);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 2;
		headersGroup.setLayout(gridLayout_2);

		headerTable = new Table(headersGroup, SWT.FULL_SELECTION | SWT.BORDER);
		headerTable.setHeaderVisible(true);
		headerTable.setLinesVisible(true);

		headerTable.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {

				editHeader();

			}

			public void mouseDown(MouseEvent e) {
				// do nothing
			}

			public void mouseUp(MouseEvent e) {
				// do nothing

			}

		});

		headerTable.addPaintListener(new PaintListener() {

			public void paintControl(PaintEvent e) {

				updateHeaderButtons();

			}

		});

		headerTable.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				updateHeaderButtons();

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// do nothing
			}

		});

		final GridData headerTableData = new GridData(SWT.FILL,
				SWT.FILL, true, true);
		headerTableData.widthHint = 430;
		headerTableData.heightHint = 107;
		headerTableData.minimumHeight = 75;
		headerTable.setLayoutData(headerTableData);

		// table.setFont(new Font(parent.getDisplay(), "Arial", 12, SWT.NONE));

		TableColumn headerTextColumnHeader = new TableColumn(headerTable,
				SWT.NONE);
		headerTextColumnHeader.setText("Header Text");
		headerTextColumnHeader.setWidth(290);

		TableColumn globalStyleColumnHeader = new TableColumn(headerTable,
				SWT.NONE);
		globalStyleColumnHeader.setText("Global Style");
		globalStyleColumnHeader.setWidth(150);

		final Composite composite_3 = new Composite(headersGroup, SWT.NONE);
		final GridData gridData_9 = new GridData(SWT.LEFT,
				SWT.TOP, false, false);
		gridData_9.heightHint = 141;
		gridData_9.widthHint = 74;
		composite_3.setLayoutData(gridData_9);
		final GridLayout gridLayout_1 = new GridLayout();
		composite_3.setLayout(gridLayout_1);

		newHeaderButton = new Button(composite_3, SWT.NONE);
		newHeaderButton.setLayoutData(new GridData(60, SWT.DEFAULT));
		newHeaderButton.setText("New...");
		newHeaderButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				buttonPressed(NEW_HEADER_BUTTON_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		editHeaderButton = new Button(composite_3, SWT.NONE);
		editHeaderButton.setLayoutData(new GridData(60, SWT.DEFAULT));
		editHeaderButton.setText("Edit...");
		editHeaderButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				buttonPressed(EDIT_HEADER_BUTTON_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		deleteHeaderButton = new Button(composite_3, SWT.NONE);
		deleteHeaderButton.setLayoutData(new GridData(60, SWT.DEFAULT));
		deleteHeaderButton.setText(DELETE_BUTTON);
		deleteHeaderButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				buttonPressed(DELETE_HEADER_BUTTON_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		final Composite composite = new Composite(composite_3, SWT.NONE);
		composite.setLayoutData(new GridData(64, 35));
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.makeColumnsEqualWidth = true;
		gridLayout_5.numColumns = 2;
		composite.setLayout(gridLayout_5);

		upHeaderButton = new Button(composite, SWT.NONE);
		upHeaderButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/up_arrow.gif"));
		upHeaderButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, true));
		upHeaderButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				headerDirectionalButtonPressed(HEADER_UP_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		downHeaderButton = new Button(composite, SWT.DOWN);
		downHeaderButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/down_arrow.gif"));
		downHeaderButton.setLayoutData(new GridData(GridData.FILL,
				GridData.FILL, true, true));
		downHeaderButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				headerDirectionalButtonPressed(HEADER_DOWN_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		final Composite axisDimComposite = new Composite(viewSectionGroup, SWT.NONE);
		// composite_1.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		final GridData gd_axisDimComposite = new GridData(SWT.FILL, SWT.TOP, true, true);
		gd_axisDimComposite.heightHint = 180;
		gd_axisDimComposite.widthHint = 534;
		// gridData_6.heightHint = 253;
		axisDimComposite.setLayoutData(gd_axisDimComposite);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.marginHeight = 0;
		gridLayout_3.marginWidth = 0;
		gridLayout_3.makeColumnsEqualWidth = true;
		axisDimComposite.setLayout(gridLayout_3);

		final Group dimensionsGroup = new Group(axisDimComposite, SWT.NONE);
		final GridData gridData_12 = new GridData(SWT.FILL,	SWT.TOP, true, true);
		gridData_12.heightHint = 180;
		// gridData_12.heightHint = 86;
		gridData_12.widthHint = 428;
		dimensionsGroup.setLayoutData(gridData_12);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.numColumns = 2;
		dimensionsGroup.setLayout(gridLayout_6);
		dimensionsGroup.setText("Axis Dimensions");

		final Label pageLabel = new Label(dimensionsGroup, SWT.NONE);
		pageLabel.setText("Page");

		pageAxisText = new Text(dimensionsGroup, SWT.READ_ONLY | SWT.BORDER);
		GridData gd_pageAxisText = new GridData(SWT.FILL, GridData.CENTER,
				true, true);
		gd_pageAxisText.widthHint = 475;
		pageAxisText.setLayoutData(gd_pageAxisText);

		final Label columnLabel = new Label(dimensionsGroup, SWT.NONE);
		columnLabel.setText("Column");

		columnAxisText = new Text(dimensionsGroup, SWT.READ_ONLY | SWT.BORDER);
//		gd_columnAxisText.heightHint = 29;
		GridData gd_columnAxisText = new GridData(SWT.FILL,
				GridData.CENTER, true, true);
		gd_columnAxisText.widthHint = 475;
		columnAxisText.setLayoutData(gd_columnAxisText);

		final Label rowLabel = new Label(dimensionsGroup, SWT.NONE);
		rowLabel.setText("Row");

		rowAxisText = new Text(dimensionsGroup, SWT.READ_ONLY | SWT.BORDER);
		GridData gd_rowAxisText = new GridData(SWT.FILL, GridData.CENTER,
				true, true);
		gd_rowAxisText.widthHint = 475;
		rowAxisText.setLayoutData(gd_rowAxisText);

		final Composite composite_2 = new Composite(axisDimComposite, SWT.NONE);

		// composite_2.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		final GridData gridData_11 = new GridData(SWT.CENTER,
				SWT.TOP, true, true);
		gridData_11.heightHint = 60;
		// gridData_11.heightHint = 22;
		gridData_11.widthHint = 475;
		composite_2.setLayoutData(gridData_11);
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.numColumns = 4;
		gridLayout_7.makeColumnsEqualWidth = true;

		final GridData composite_2GD = new GridData(SWT.FILL,
				GridData.CENTER, true, false, 2, 1);
		composite_2GD.widthHint = 550;

		// composite_2GD.heightHint = 40;
		composite_2GD.heightHint = 40;
		// composite_2GD.widthHint = 150;
		composite_2.setLayout(gridLayout_7);
		composite_2.setLayoutData(composite_2GD);

		assignDimensionsButton = new Button(composite_2, SWT.NONE);
		assignDimensionsButton.setLayoutData(new GridData(GridData.FILL,
				GridData.FILL, true, true));
		assignDimensionsButton.setText("Assign Dimensions...");
		assignDimensionsButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				buttonPressed(Constants.APPLY_ID);
				
				String viewSectionNameStr = viewSectionName.getText();

				ViewSectionConfigureDimDialog vsConfigDimDialog = new ViewSectionConfigureDimDialog(
						parentShell, project, viewSectionNameStr,
						viewSectionManager);
				vsConfigDimDialog.open();

				loadViewSection((PafViewSectionUI) viewSectionManager
						.getItem(viewSectionNameStr));

				//openWithNewView = false;
				
			}

		});

		assignMembersButton = new Button(composite_2, SWT.NONE);
		assignMembersButton.setLayoutData(new GridData(GridData.FILL,
				GridData.FILL, true, true));
		assignMembersButton.setText("Assign Members...");
		assignMembersButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				final String viewSectionNameStr = viewSectionName.getText();
				ArrayList<String> viewsSectionToBeSaved = new ArrayList<String>();
				viewsSectionToBeSaved.add(viewSectionNameStr);
				
				viewSectionManager.save(viewsSectionToBeSaved);

				
				// use an hourglass and start up the tuples dialog
				BusyIndicator.showWhile(parentShell.getDisplay(),
						new Runnable() {
							public void run() {
								try {

									PafViewSectionUI pafViewSection = (PafViewSectionUI) viewSectionManager.getItem(viewSectionNameStr);
									
									ViewSectionTuplesDialogInput input = new ViewSectionTuplesDialogInput(project, pafViewSection);
									
									ViewSectionTuplesDialog dialog = new ViewSectionTuplesDialog(parentShell, input);
																		
									if ( dialog.open() == IDialogConstants.OK_ID) {
										
										pafViewSection.setPageTuples(input.getPageAxisTupleList().getPageTuples().toArray(new PageTuple[0]));
										/*
										if ( input.getPageDimensions().size() > 0 && input.getPageAxisTupleList().getPageTuples().size() > 0 ) {
											
											pafViewSection.setPageTuples(input.getPageAxisTupleList().getPageTuples().toArray(new PageTuple[0]));
											
										} else {
											
											pafViewSection.setPageTuples(null);
											
										}
										*/
										
										pafViewSection.setColTuples(input.getColumnAxisTupleTree().getViewTuples().toArray(new ViewTuple[0]));
										pafViewSection.setRowTuples(input.getRowAxisTupleTree().getViewTuples().toArray(new ViewTuple[0]));
										viewSectionManager.replace(pafViewSection.getName(), pafViewSection);
										
									}
									
								} catch (Exception e1) {

									if (e1 instanceof RuntimeException || e1 instanceof PafServerNotFound ) {
										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
												.getMessage(), MessageDialog.ERROR);
									} else if ( e1 instanceof InvalidDimensionsException ) {
										
										InvalidDimensionsException idExc = (InvalidDimensionsException) e1;
										
										String missingDims = "";
										for(String dim : idExc.getInvalidDimensionList()){
											missingDims += dim + "\n";
										}
										
										MessageDialog.openError(parentShell,
												ErrorConstants.MISSING_DIMENSIONS,
												ErrorConstants.THE_FOLLOWING_DIMENSIONS_ARE_NO_LONGER_VALID + missingDims);
										
									} else if ( e1.getMessage() != null ) {
										
										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
												.getMessage(), MessageDialog.ERROR);
										
									} else {
										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
														"There was an unexpected error.  The xml could be corrupt.", MessageDialog.ERROR);
									}

									logger.error(e1.getMessage());
									
								}
							}
						});
				
				
//				// use an hourglass and start up the tuples dialog
//				BusyIndicator.showWhile(parentShell.getDisplay(),
//						new Runnable() {
//							public void run() {
//								try {
//
//									ViewSectionTuplesDialog2 vstd = new ViewSectionTuplesDialog2(
//											parentShell, project,
//											viewSectionNameStr,
//											viewSectionManager);
//
//									int rc = vstd.open();
//
//									logger.info("View Section Tuple Dialog RC: " + rc);
//
//								} catch (Exception e1) {
//
//									if (e1 instanceof RuntimeException || e1 instanceof PafServerNotFound ) {
//										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
//												.getMessage(), MessageDialog.ERROR);
//									} else {
//										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
//														"There was an unexpected error.  The xml might be corrupt.", MessageDialog.ERROR);
//									}
//
//									logger.error(e1.getMessage());
//									
//								}
//							}
//						});

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// do nothing

			}

		});

		viewSectionOptionsButton = new Button(composite_2, SWT.NONE);
		viewSectionOptionsButton.setEnabled(false);
		viewSectionOptionsButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		viewSectionOptionsButton.setText("Options...");
		
		viewSectionOptionsButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

								
//				 use an hourglass and start up the tuples dialog
				BusyIndicator.showWhile(parentShell.getDisplay(),
						new Runnable() {
							public void run() {								

						ArrayList<String> viewsSectionToBeSaved = new ArrayList<String>();
						viewsSectionToBeSaved.add(viewSectionName.getText());
								
						PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
							.getItem(viewSectionName.getText());
						
						ViewSectionOptionsDialog viewSectionsOptionsDialog = null;
						
						try {
							
							ViewSectionTuplesDialogInput input = new ViewSectionTuplesDialogInput(project, viewSection);
							
							viewSectionsOptionsDialog = new ViewSectionOptionsDialog(getShell(), input);
							
							int rc = viewSectionsOptionsDialog.open();
							
							if ( rc == Dialog.OK ) {
								
								viewSection = viewSectionsOptionsDialog.getViewSection();
																								
								viewSectionManager.replace(viewSection.getName(), viewSection);
								
								viewSectionManager.save(viewsSectionToBeSaved);
								
								loadViewSection((PafViewSectionUI) viewSectionManager
										.getItem(viewSection.getName()));
								
							}
							
						} catch (Exception e1) {
							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
									.getMessage(), MessageDialog.ERROR);
						}
					}
				});
			}
		});

		createViewButton = new Button(composite_2, SWT.NONE);
		createViewButton.setEnabled(false);
		createViewButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				try {
					ViewDialog dialog = new ViewDialog(
						parentShell, 
						project,
						viewSectionName.getText(),
						true);
					
					if(dialog.open() == Dialog.OK){
						createdNewView = true;
					}
						
				}catch (Exception e1) {
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
							.getMessage(), MessageDialog.ERROR);
				}
			}
		});
		final GridData gd_createViewButton = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd_createViewButton.widthHint = 109;
		createViewButton.setLayoutData(gd_createViewButton);
		createViewButton.setText("Create View...");

//		final Button newAssignMembersButton = new Button(tempComposite, SWT.NONE);
//		newAssignMembersButton.setToolTipText("Temporary Button...");
//		newAssignMembersButton.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, false, false));
//		newAssignMembersButton.addSelectionListener(new SelectionAdapter() {
//			public void widgetSelected(final SelectionEvent e) {
//				
//				final String viewSectionNameStr = viewSectionName.getText();
//
//				//viewSectionManager.save();
//
////				// use an hourglass and start up the tuples dialog
////				BusyIndicator.showWhile(parentShell.getDisplay(),
////						new Runnable() {
////							public void run() {
////								try {
////
////									PafViewSectionUI pafViewSection = (PafViewSectionUI) viewSectionManager.getItem(viewSectionNameStr);
////									
////									ViewSectionTuplesDialogInput input = new ViewSectionTuplesDialogInput(project, pafViewSection);
////									
////									ViewSectionTuplesDialog dialog = new ViewSectionTuplesDialog(parentShell, input);
////																		
////									if ( dialog.open() == IDialogConstants.OK_ID) {
////										
////										pafViewSection.setPageTuples(input.getPageAxisTupleList().getPageTuples().toArray(new PageTuple[0]));
////										/*
////										if ( input.getPageDimensions().size() > 0 && input.getPageAxisTupleList().getPageTuples().size() > 0 ) {
////											
////											pafViewSection.setPageTuples(input.getPageAxisTupleList().getPageTuples().toArray(new PageTuple[0]));
////											
////										} else {
////											
////											pafViewSection.setPageTuples(null);
////											
////										}
////										*/
////										
////										pafViewSection.setColTuples(input.getColumnAxisTupleTree().getViewTuples().toArray(new ViewTuple[0]));
////										pafViewSection.setRowTuples(input.getRowAxisTupleTree().getViewTuples().toArray(new ViewTuple[0]));
////										
////										viewSectionManager.replace(pafViewSection.getName(), pafViewSection);
////										
////									}
////									
////								} catch (Exception e1) {
////
////									if (e1 instanceof RuntimeException || e1 instanceof PafServerNotFound ) {
////										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
////												.getMessage(), MessageDialog.ERROR);
////									} else if ( e1.getMessage() != null ) {
////										
////										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1
////												.getMessage(), MessageDialog.ERROR);
////										
////									} else {
////										GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
////														"There was an unexpected error.  The xml might be corrupt.", MessageDialog.ERROR);
////									}
////
////									logger.error(e1.getMessage());
////									
////								}
////							}
////						});
//			}
//		});

		// new Label(composite_1, SWT.NONE);
		// new Label(viewSectionGroup, SWT.NONE);

		// populate dialog window
		initializeDialog();

		if (openWithNewView) {
			buttonPressed(NEW_VIEW_SECTION_BUTTON_ID);
		}else if(copyToNewView){
			buttonPressed(COPY_VIEW_SECTION_BUTTON_ID);
		}

		return container;
	}

	private String[] populateGenerationFormats() {

		return generationFormattingManager.getKeys();

	}

	private void initializeDialog() {

		refreshViewSection();
		editHeaderButton.setEnabled(false);
		deleteHeaderButton.setEnabled(false);
		upHeaderButton.setEnabled(false);
		downHeaderButton.setEnabled(false);
		
	}

	private void refreshViewSection() {

		// if views exists
		if (viewSectionManager.size() > 0) {

			String[] keys = viewSectionManager.getKeys();

			viewSectionList.addAll(viewSectionManager.getKeySet());
			
			PafViewSectionUI currentViewSection = (PafViewSectionUI) viewSectionManager
					.getItem(keys[0]);

			loadViewSection(currentViewSection);

			if (initialViewName == null) {
			} else {
				int initialNdx = viewSectionManager.getIndex(initialViewName);
				//viewSectionList.select(initialNdx);
				PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
						.getItem(initialViewName);
				loadViewSection(viewSection);
				
				//viewSectionList.showSelection();
				
				initialViewName = null;
			}

		} else {

			clearForm();
			
			viewSectionList.clear();
			viewSectionList.addAll(viewSectionManager.getKeySet());

			// diable group
			viewSectionGroup.setEnabled(false);

			//copyButton.setEnabled(false);
			//deleteButton.setEnabled(false);
			assignDimensionsButton.setEnabled(false);
			assignMembersButton.setEnabled(false);
			viewSectionOptionsButton.setEnabled(false);

		}

	}

	private void loadViewSection(PafViewSectionUI viewSection) {

		clearForm();

		setTextItem(viewSectionName, viewSection.getName());
		setTextItem(viewSectionDesc, viewSection.getDescription());

		// load headers
		PafViewHeader[] headers = viewSection.getHeaders();

		if (headers != null) {

			headerTable.clearAll();
			headerTable.setItemCount(0);

			for (PafViewHeader header : headers) {

				addHeaderTableItem(header);

			}

			// editHeaderButton.setEnabled(true);
			// deleteHeaderButton.setEnabled(true);

		}

		// setTextItem(dataCacheText, viewSection.getDataAlias());

		setTextItem(pageAxisText, viewSection
				.convertArToCommaDelimitedStr(viewSection.getPageAxisDims()));
		setTextItem(columnAxisText, viewSection
				.convertArToCommaDelimitedStr(viewSection.getColAxisDims()));
		setTextItem(rowAxisText, viewSection
				.convertArToCommaDelimitedStr(viewSection.getRowAxisDims()));

		if (pageAxisText.getText().trim().length() == 0
				&& columnAxisText.getText().trim().length() == 0
				&& rowAxisText.getText().trim().length() == 0) {
			assignDimensionsButton.setEnabled(true);
			assignMembersButton.setEnabled(false);
			viewSectionOptionsButton.setEnabled(false);
		} else {
			assignDimensionsButton.setEnabled(true);
			assignMembersButton.setEnabled(true);
			viewSectionOptionsButton.setEnabled(true);
			createViewButton.setEnabled(true);
			
		}

	}

	private void setTextItem(Text textItem, String value) {

		if (value == null) {
			textItem.setText("");
		} else {
			textItem.setText(value);
		}

	}

	private void addHeaderTableItem(PafViewHeader header) {

		TableItem item = new TableItem(headerTable, SWT.NONE);
		item.setText(0, header.getLabel());

		if (header.getGlobalStyleName() != null) {

			// TODO: fix this to use name instead of actual style
			if (header.getGlobalStyleName() != null) {
				item.setText(1, header.getGlobalStyleName());
			}

		}

	}

	@Override
	protected void buttonPressed(int buttonId) {

		// ok button
		if (buttonId == IDialogConstants.OK_ID || buttonId == Constants.APPLY_ID) {
			//if the view section is not in the list then save add the view section.
			String newViewName = viewSectionName.getText().trim();
			ArrayList<String> viewsToBeSaved = new ArrayList<String>();
			viewsToBeSaved.add(newViewName);
			
			if(!searchListIgnoreCase(newViewName)){
				
				// if cloned view, save differently
				if (clonedViewSection != null) {

					clonedViewSection.setName(newViewName);
					clonedViewSection.setDescription(viewSectionDesc.getText().trim());

					viewSectionManager.add(clonedViewSection.getName(), clonedViewSection);

					// clear cloned View
					clonedViewSection = null;

				} else {

					PafViewSectionUI viewSection = new PafViewSectionUI(new PafViewSection());
					viewSection.setName(newViewName);
					viewSection.setDescription(viewSectionDesc.getText().trim());

					// add new headers
					viewSection.setHeaders((PafViewHeader[]) localHeaders
							.toArray(new PafViewHeader[localHeaders.size()]));

					viewSectionManager.add(viewSection.getName(), viewSection);

					// kill local headers and view sections
					localHeaders = null;
				}

//				// hide buttons
//				dialogApplyButton.setEnabled(false);

				// disable view name
				viewSectionName.setEditable(false);

				viewSectionList.add(newViewName);
				
				assignDimensionsButton.setEnabled(true);

				if (pageAxisText.getText().trim().length() == 0
						&& columnAxisText.getText().trim().length() == 0
						&& rowAxisText.getText().trim().length() == 0) {
					assignMembersButton.setEnabled(false);
					viewSectionOptionsButton.setEnabled(false);
					createViewButton.setEnabled(false);
				} else {
					assignMembersButton.setEnabled(true);
					viewSectionOptionsButton.setEnabled(true);
					createViewButton.setEnabled(true);
				}
			}

			viewSectionManager.save(viewsToBeSaved);
			
			if(buttonId == IDialogConstants.OK_ID){
				super.okPressed();
			} 

		} else if (buttonId == IDialogConstants.CANCEL_ID) {
			// viewSectionManager = null;
			
			if ( openWithNewView ) {
			
				String currentViewSectionName = viewSectionName.getText().trim();
				
				if ( viewSectionManager != null && viewSectionManager.contains(currentViewSectionName)) {
					
					viewSectionManager.remove(currentViewSectionName);
										
				}
			
			}
			
			cancelPressed();
		}else if (buttonId == NEW_VIEW_SECTION_BUTTON_ID) {

			clearForm();

			localHeaders = new ArrayList<PafViewHeader>(4);

//			// disable buttons
//			copyButton.setEnabled(false);
//			deleteButton.setEnabled(false);
//			addButton.setEnabled(false);
//			newButton.setEnabled(false);
//
//			// enable buttons
//			cancelButton.setEnabled(true);

//			// make buttons visible
//			addButton.setVisible(true);
//			cancelButton.setVisible(true);

			// enable group
			viewSectionGroup.setEnabled(true);

			// disable list
//			viewSectionList.setEnabled(false);

			// make view name editable
			viewSectionName.setEditable(true);

			assignDimensionsButton.setEnabled(false);
			assignMembersButton.setEnabled(false);
			viewSectionOptionsButton.setEnabled(false);
			createViewButton.setEnabled(false);

			viewSectionName.setFocus();

		} else if (buttonId == COPY_VIEW_SECTION_BUTTON_ID) {

			PafViewSection clonedSection = null;

			try {
				clonedSection = (PafViewSection)((PafViewSectionUI)viewSectionManager.getItem(viewSectionName.getText())).getPafViewSection().clone();

			} catch (CloneNotSupportedException e) {
				logger.error("The PafViewSection was not cloneable.");
				return;
			}

			clonedViewSection = new PafViewSectionUI(clonedSection);

			// disable buttons
//			newButton.setEnabled(false);
//			copyButton.setEnabled(false);
//			deleteButton.setEnabled(false);
//			addButton.setEnabled(false);

			// enable buttons
//			cancelButton.setEnabled(true);

			// make buttons visible
//			addButton.setVisible(true);
//			cancelButton.setVisible(true);

			// enable group
			viewSectionGroup.setEnabled(true);

			// disable list
//			viewSectionList.setEnabled(false);

			clonedViewSection.setName(Constants.COPY_TO + clonedViewSection.getName());
			
			viewSectionName.setEditable(true);

			loadViewSection(clonedViewSection);

			assignMembersButton.setEnabled(false);
			viewSectionOptionsButton.setEnabled(false);

			assignDimensionsButton.setEnabled(false);
			assignMembersButton.setEnabled(false);
			viewSectionOptionsButton.setEnabled(false);
			

		}  else if (buttonId == NEW_HEADER_BUTTON_ID) {

			HeaderDialog headerDialog = new HeaderDialog(this.getShell(),
					project);
			headerDialog.open();

			PafViewHeader header = headerDialog.getViewHeader();

			// if user added a header via header dialog
			if (header != null) {

				int currentState = getCurrentViewState();

				if (currentState == NEW_VIEW_STATE) {

					if (localHeaders == null) {
						localHeaders = new ArrayList<PafViewHeader>();
					}

					localHeaders.add(header);

					// if already existing one and not clone
				} else if (currentState == EXISTING_VIEW_STATE) {

					PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
							.getItem(viewSectionName.getText());
					viewSection.addHeader(header);

					viewSectionManager.add(viewSection.getName(), viewSection);

				} else if (currentState == CLONED_VIEW_STATE) {

					clonedViewSection.addHeader(header);

				}

				addHeaderTableItem(header);

			}
		} else if (buttonId == EDIT_HEADER_BUTTON_ID) {

			editHeader();

		} else if (buttonId == DELETE_HEADER_BUTTON_ID) {

			//prompt user
			if ( MessageDialog.openConfirm(this.getShell(), Constants.DIALOG_QUESTION_HEADING, "Delete Selected Header?") ) {
			
				int selectedIndex = headerTable.getSelectionIndex();
				int currentState = getCurrentViewState();
	
				if (currentState == NEW_VIEW_STATE) {
	
					localHeaders.remove(selectedIndex);
	
					populateHeaderTable(localHeaders);
	
				} else if (currentState == EXISTING_VIEW_STATE) {
	
					PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
							.getItem(viewSectionName.getText());
	
					ArrayList<PafViewHeader> headers = new ArrayList<PafViewHeader>();
					PafViewHeader[] headersAr = viewSection.getHeaders();
	
					if (headersAr != null) {
						for (PafViewHeader header : headersAr) {
							headers.add(header);
						}
					}
	
					headers.remove(selectedIndex);
	
					populateHeaderTable(headers);
	
					// create headerAr
					PafViewHeader[] headerAr = (PafViewHeader[]) headers
							.toArray(new PafViewHeader[headers.size()]);
	
					viewSection.setHeaders(headerAr);
	
					viewSectionManager.replace(viewSection.getName(), viewSection);
	
				} else if (currentState == CLONED_VIEW_STATE) {
	
					ArrayList<PafViewHeader> headers = (ArrayList<PafViewHeader>) clonedViewSection
							.getHeaderList();
	
					headers.remove(selectedIndex);
	
					populateHeaderTable(headers);
	
					// create headerAr
					PafViewHeader[] headerAr = (PafViewHeader[]) headers
							.toArray(new PafViewHeader[headers.size()]);
	
					clonedViewSection.setHeaders(headerAr);
	
				}
	
				if (headerTable.getItemCount() != -1) {
	
					headerTable.select(0);
	
				}
			}
		}
	}

	private boolean searchListIgnoreCase(String newViewName) {
		String[] listAr = viewSectionList.toArray(new String[0]);
		
		if (listAr != null) {
			for (String name : listAr) {
				if (newViewName.equalsIgnoreCase(name)) {
					return true;
				}
			}
		}

		return false;
	}

	private void clearForm() {

		String blank = "";

		viewSectionName.setText(blank);
		viewSectionDesc.setText(blank);
		headerTable.clearAll();
		headerTable.setItemCount(0);

		// dataCacheText.setText(blank);

		pageAxisText.setText(blank);
		columnAxisText.setText(blank);
		rowAxisText.setText(blank);

	}

	protected void createButtonsForButtonBar(Composite parent) {
		//dialogApplyButton = createButton(parent, Constants.APPLY_ID, Constants.APPLY_LABEL, false);
		dialogOkButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		if(copyToNewView && errorLabel.getVisible()){
//			dialogApplyButton.setEnabled(false);
			dialogOkButton.setEnabled(false);
//		}else if(openWithNewView){
//			dialogApplyButton.setEnabled(false);
//			dialogOkButton.setEnabled(false);
//		} 
		}else{
//			dialogApplyButton.setEnabled(false);
			dialogOkButton.setEnabled(true);
		}
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, true);
	}

	protected Point getInitialSize() {
		return new Point(599, 568);

	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("View Section");
	}

	// returns the current view state, three options: new, existing, or clone
	private int getCurrentViewState() {

		int state = UNKNOWN_VIEW_STATE;

		// if not clone and on new
		if (clonedViewSection == null && viewSectionName.getEditable()) {

			state = NEW_VIEW_STATE;

			// if already exisiting one and not clone
		} else if (clonedViewSection == null
				&& (!viewSectionName.getEditable())) {

			state = EXISTING_VIEW_STATE;

		} else if (clonedViewSection != null) {

			state = CLONED_VIEW_STATE;

		}

		return state;

	}

	// update the header buttons depending on the state of the header table.
	private void updateHeaderButtons() {

		if (headerTable.getSelectionIndex() != -1) {

			editHeaderButton.setEnabled(true);
			deleteHeaderButton.setEnabled(true);

			if (headerTable.getItemCount() == 1) {
				upHeaderButton.setEnabled(false);
				downHeaderButton.setEnabled(false);
			} else if (headerTable.getSelectionIndex() == 0) {
				upHeaderButton.setEnabled(false);
				downHeaderButton.setEnabled(true);
			} else if (headerTable.getSelectionIndex() == headerTable
					.getItemCount() - 1) {
				upHeaderButton.setEnabled(true);
				downHeaderButton.setEnabled(false);
			} else {
				upHeaderButton.setEnabled(true);
				downHeaderButton.setEnabled(true);
			}

		} else {

			upHeaderButton.setEnabled(false);
			downHeaderButton.setEnabled(false);
			editHeaderButton.setEnabled(false);
			deleteHeaderButton.setEnabled(false);

		}

	}

	private void swapHeaderTableElements(ArrayList<PafViewHeader> list,
			int ndx1, int ndx2) {

		PafViewHeader header1 = list.get(ndx1);
		PafViewHeader header2 = list.get(ndx2);

		list.remove(header1);
		list.remove(header2);

		if (ndx1 < ndx2) {
			list.add(ndx1, header2);
			list.add(ndx2, header1);
		} else {
			list.add(ndx2, header1);
			list.add(ndx1, header2);
		}

	}

	private void populateHeaderTable(ArrayList<PafViewHeader> list) {

		headerTable.clearAll();
		headerTable.setItemCount(-1);

		for (PafViewHeader header : list) {

			addHeaderTableItem(header);

		}
	}

	private void headerDirectionalButtonPressed(int direction) {

		if (headerTable.getSelectionIndex() >= 0) {

			int ndx1 = headerTable.getSelectionIndex();

			int currentState = getCurrentViewState();

			if (currentState == NEW_VIEW_STATE) {

				swapHeaderTableElements(localHeaders, ndx1, (ndx1 + direction));

				populateHeaderTable(localHeaders);

			} else if (currentState == EXISTING_VIEW_STATE) {

				PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
						.getItem(viewSectionName.getText());

				ArrayList<PafViewHeader> headers = (ArrayList<PafViewHeader>) viewSection
						.getHeaderList();

				swapHeaderTableElements(headers, ndx1, (ndx1 + direction));

				populateHeaderTable(headers);

				// create headerAr
				PafViewHeader[] headerAr = (PafViewHeader[]) headers
						.toArray(new PafViewHeader[headers.size()]);

				viewSection.setHeaders(headerAr);

				viewSectionManager.replace(viewSection.getName(), viewSection);

			} else if (currentState == CLONED_VIEW_STATE) {

				ArrayList<PafViewHeader> headers = (ArrayList<PafViewHeader>) clonedViewSection
						.getHeaderList();

				swapHeaderTableElements(headers, ndx1, (ndx1 + direction));

				populateHeaderTable(headers);

				// create headerAr
				PafViewHeader[] headerAr = (PafViewHeader[]) headers
						.toArray(new PafViewHeader[headers.size()]);

				clonedViewSection.setHeaders(headerAr);

			}

			headerTable.select(ndx1 + direction);
		}

	}

	private void editHeader() {

		int selectedIndex = headerTable.getSelectionIndex();

		PafViewHeader localHeader = null;

		HeaderDialog headerDialog = new HeaderDialog(this.getShell(), project);

		int currentState = getCurrentViewState();

		if (currentState == NEW_VIEW_STATE) {

			localHeader = localHeaders.get(selectedIndex);

			headerDialog.setViewHeader(localHeader);
			headerDialog.open();

			localHeader = headerDialog.getViewHeader();

			localHeaders.remove(selectedIndex);
			localHeaders.add(selectedIndex, localHeader);

			populateHeaderTable(localHeaders);

		} else if (currentState == EXISTING_VIEW_STATE) {

			PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
					.getItem(viewSectionName.getText());

			ArrayList<PafViewHeader> headers = (ArrayList<PafViewHeader>) viewSection
					.getHeaderList();

			localHeader = headers.get(selectedIndex);

			headerDialog.setViewHeader(localHeader);
			headerDialog.open();

			localHeader = headerDialog.getViewHeader();

			headers.remove(selectedIndex);
			headers.add(selectedIndex, localHeader);

			populateHeaderTable(headers);

			// create headerAr
			PafViewHeader[] headerAr = (PafViewHeader[]) headers
					.toArray(new PafViewHeader[headers.size()]);

			viewSection.setHeaders(headerAr);

			viewSectionManager.replace(viewSection.getName(), viewSection);

		} else if (getCurrentViewState() == CLONED_VIEW_STATE) {

			ArrayList<PafViewHeader> headers = (ArrayList<PafViewHeader>) clonedViewSection
					.getHeaderList();

			localHeader = headers.get(selectedIndex);

			headerDialog.setViewHeader(localHeader);
			headerDialog.open();

			localHeader = headerDialog.getViewHeader();

			headers.remove(selectedIndex);
			headers.add(selectedIndex, localHeader);

			populateHeaderTable(headers);

			// create headerAr
			PafViewHeader[] headerAr = (PafViewHeader[]) headers
					.toArray(new PafViewHeader[headers.size()]);

			clonedViewSection.setHeaders(headerAr);

		}

		headerTable.select(selectedIndex);

	}

	private void saveCurrentViewState() {

		int currentState = getCurrentViewState();

		if (currentState == NEW_VIEW_STATE) {

		} else if (currentState == EXISTING_VIEW_STATE) {

			PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionManager
					.getItem(viewSectionName.getText());

			viewSection.setDescription(viewSectionDesc.getText().trim());


			viewSectionManager.replace(viewSection.getName(), viewSection);

		} else if (currentState == CLONED_VIEW_STATE) {

		}

	}

	

	private int getMeasureVersionAxis(PafViewSection section) {

		int measureVersionAxis = 0;

		PageTuple[] pageTuples = section.getPageTuples();

		String measureDim = DimensionUtil.getMeasureDimensionName(project);
		String versionDim = DimensionUtil.getVersionDimensionName(project);

		if (pageTuples != null) {

			for (PageTuple pageTuple : pageTuples) {
				if (pageTuple.getAxis().equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_PAGE_AXIS;
				} else if (pageTuple.getAxis().equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_PAGE_AXIS;
				}
			}
		}

		String[] colDims = section.getColAxisDims();

		if (colDims != null) {
			for (String colDim : colDims) {
				if (colDim.equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_COL_AXIS;
				} else if (colDim.equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_COL_AXIS;
				}
			}
		}

		String[] rowDims = section.getRowAxisDims();

		if (rowDims != null) {
			for (String rowDim : rowDims) {
				if (rowDim.equals(measureDim)) {
					measureVersionAxis += PafViewSection.MEASURE_ROW_AXIS;
				} else if (rowDim.equals(versionDim)) {
					measureVersionAxis += PafViewSection.VERSION_ROW_AXIS;
				}
			}
		}
			
		return measureVersionAxis;

	}
	

	public boolean isCreatedNewView() {
		return createdNewView;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

public class UserSecurityConfigurationDialog extends Dialog {

	private Text text_2;
	private Combo combo;
	private Text text;
	private Button leftButton_3;
	private Button leftButton_2;
	private List list;
	private Button leftButton_1;
	private Button leftButton;
	private Tree tree;
	private Combo memberLevel;
	private Button justDescendantsOfSelectionButton;
	private Button selectionAndDecendantsButton;
	private Button justChildrenOfSelectionButton;
	private Button selectionAndChildrenButton;
	private Button selectionButton;
	public UserSecurityConfigurationDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE  );
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		final Group memberPropertiesGroup = new Group(container, SWT.NONE);
		final FormData formData_6 = new FormData();
		formData_6.bottom = new FormAttachment(0, 498);
		formData_6.top = new FormAttachment(0, 348);
		formData_6.right = new FormAttachment(0, 285);
		formData_6.left = new FormAttachment(0, 75);
		memberPropertiesGroup.setLayoutData(formData_6);
		memberPropertiesGroup.setLayout(new FormLayout());
		memberPropertiesGroup.setText("Member Properties");

		selectionButton = new Button(memberPropertiesGroup, SWT.RADIO);
		final FormData formData_12 = new FormData();
		formData_12.bottom = new FormAttachment(0, 25);
		formData_12.top = new FormAttachment(0, 10);
		formData_12.right = new FormAttachment(0, 70);
		formData_12.left = new FormAttachment(0, 6);
		selectionButton.setLayoutData(formData_12);
		selectionButton.setText("Selection");

		selectionAndChildrenButton = new Button(memberPropertiesGroup, SWT.RADIO);
		final FormData formData_11 = new FormData();
		formData_11.bottom = new FormAttachment(0, 40);
		formData_11.top = new FormAttachment(0, 28);
		formData_11.right = new FormAttachment(0, 133);
		formData_11.left = new FormAttachment(0, 6);
		selectionAndChildrenButton.setLayoutData(formData_11);
		selectionAndChildrenButton.setText("Selection and Children");

		justChildrenOfSelectionButton = new Button(memberPropertiesGroup, SWT.RADIO);
		final FormData formData_10 = new FormData();
		formData_10.bottom = new FormAttachment(0, 59);
		formData_10.top = new FormAttachment(0, 45);
		formData_10.right = new FormAttachment(0, 148);
		formData_10.left = new FormAttachment(0, 6);
		justChildrenOfSelectionButton.setLayoutData(formData_10);
		justChildrenOfSelectionButton.setText("Just Children of Selection");

		selectionAndDecendantsButton = new Button(memberPropertiesGroup, SWT.RADIO);
		final FormData formData_9 = new FormData();
		formData_9.bottom = new FormAttachment(0, 78);
		formData_9.top = new FormAttachment(0, 59);
		formData_9.right = new FormAttachment(0, 151);
		formData_9.left = new FormAttachment(0, 6);
		selectionAndDecendantsButton.setLayoutData(formData_9);
		selectionAndDecendantsButton.setText("Selection and Decendants");

		justDescendantsOfSelectionButton = new Button(memberPropertiesGroup, SWT.RADIO);
		final FormData formData_8 = new FormData();
		formData_8.bottom = new FormAttachment(0, 94);
		formData_8.top = new FormAttachment(0, 77);
		formData_8.right = new FormAttachment(0, 171);
		formData_8.left = new FormAttachment(0, 6);
		justDescendantsOfSelectionButton.setLayoutData(formData_8);
		justDescendantsOfSelectionButton.setText("Just Descendants of Selection");

		final Label levelLabel = new Label(memberPropertiesGroup, SWT.NONE);
		final FormData formData_21 = new FormData();
		formData_21.bottom = new FormAttachment(0, 120);
		formData_21.top = new FormAttachment(0, 105);
		formData_21.right = new FormAttachment(justDescendantsOfSelectionButton, 29, SWT.LEFT);
		formData_21.left = new FormAttachment(justDescendantsOfSelectionButton, 0, SWT.LEFT);
		levelLabel.setLayoutData(formData_21);
		levelLabel.setText("Level:");

		memberLevel = new Combo(memberPropertiesGroup, SWT.READ_ONLY);
		final FormData formData_25 = new FormData();
		formData_25.bottom = new FormAttachment(levelLabel, 19, SWT.TOP);
		formData_25.top = new FormAttachment(levelLabel, -2, SWT.TOP);
		formData_25.right = new FormAttachment(levelLabel, 74, SWT.LEFT);
		formData_25.left = new FormAttachment(levelLabel, 35, SWT.LEFT);
		memberLevel.setLayoutData(formData_25);
		memberLevel.select(0);
		memberLevel.setEnabled(false);

		tree = new Tree(container, SWT.BORDER);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 343);
		formData.right = new FormAttachment(0, 285);
		formData.top = new FormAttachment(0, 103);
		formData.left = new FormAttachment(0, 75);
		tree.setLayoutData(formData);

		leftButton = new Button(container, SWT.ARROW | SWT.LEFT);
		final FormData formData_2 = new FormData();
		formData_2.top = new FormAttachment(0, 228);
		formData_2.bottom = new FormAttachment(0, 246);
		formData_2.right = new FormAttachment(0, 345);
		formData_2.left = new FormAttachment(0, 300);
		leftButton.setLayoutData(formData_2);
		leftButton.setEnabled(false);
		leftButton.setText("button");

		leftButton_1 = new Button(container, SWT.ARROW | SWT.RIGHT);
		final FormData formData_2_1 = new FormData();
		formData_2_1.left = new FormAttachment(0, 300);
		formData_2_1.bottom = new FormAttachment(0, 218);
		formData_2_1.top = new FormAttachment(0, 200);
		formData_2_1.right = new FormAttachment(0, 345);
		leftButton_1.setLayoutData(formData_2_1);
		leftButton_1.setEnabled(false);
		leftButton_1.setText("button");

		list = new List(container, SWT.BORDER);
		final FormData formData_1 = new FormData();
		formData_1.bottom = new FormAttachment(0, 343);
		formData_1.right = new FormAttachment(0, 555);
		formData_1.top = new FormAttachment(0, 103);
		formData_1.left = new FormAttachment(0, 360);
		list.setLayoutData(formData_1);

		leftButton_2 = new Button(container, SWT.ARROW);
		final FormData formData_2_1_1 = new FormData();
		formData_2_1_1.top = new FormAttachment(0, 200);
		formData_2_1_1.bottom = new FormAttachment(0, 218);
		formData_2_1_1.right = new FormAttachment(0, 601);
		formData_2_1_1.left = new FormAttachment(0, 581);
		leftButton_2.setLayoutData(formData_2_1_1);
		leftButton_2.setEnabled(false);
		leftButton_2.setText("button");

		leftButton_3 = new Button(container, SWT.ARROW | SWT.DOWN);
		final FormData formData_2_1_1_1 = new FormData();
		formData_2_1_1_1.top = new FormAttachment(0, 223);
		formData_2_1_1_1.left = new FormAttachment(0, 581);
		formData_2_1_1_1.bottom = new FormAttachment(0, 241);
		formData_2_1_1_1.right = new FormAttachment(0, 601);
		leftButton_3.setLayoutData(formData_2_1_1_1);
		leftButton_3.setEnabled(false);
		leftButton_3.setText("button");

		final Label userNameLabel = new Label(container, SWT.NONE);
		final FormData formData_3 = new FormData();
		formData_3.left = new FormAttachment(0, 75);
		formData_3.bottom = new FormAttachment(0, 35);
		formData_3.right = new FormAttachment(0, 135);
		formData_3.top = new FormAttachment(0, 20);
		userNameLabel.setLayoutData(formData_3);
		userNameLabel.setText("User Name:");

		final Label userNameLabel_1 = new Label(container, SWT.NONE);
		final FormData formData_3_1 = new FormData();
		formData_3_1.top = new FormAttachment(0, 44);
		formData_3_1.left = new FormAttachment(0, 75);
		formData_3_1.bottom = new FormAttachment(0, 59);
		formData_3_1.right = new FormAttachment(0, 135);
		userNameLabel_1.setLayoutData(formData_3_1);
		userNameLabel_1.setText("Role Name:");

		final Label userNameLabel_2 = new Label(container, SWT.NONE);
		final FormData formData_3_2 = new FormData();
		formData_3_2.left = new FormAttachment(0, 75);
		formData_3_2.bottom = new FormAttachment(0, 85);
		formData_3_2.top = new FormAttachment(0, 70);
		formData_3_2.right = new FormAttachment(0, 125);
		userNameLabel_2.setLayoutData(formData_3_2);
		userNameLabel_2.setText("Dimension:");

		text = new Text(container, SWT.BORDER);
		text.setEditable(false);
		final FormData formData_4 = new FormData();
		formData_4.top = new FormAttachment(0, 21);
		formData_4.left = new FormAttachment(0, 136);
		formData_4.bottom = new FormAttachment(0, 41);
		formData_4.right = new FormAttachment(0, 251);
		text.setLayoutData(formData_4);

		combo = new Combo(container, SWT.NONE);
		final FormData formData_7 = new FormData();
		formData_7.left = new FormAttachment(0, 136);
		formData_7.bottom = new FormAttachment(0, 91);
		formData_7.right = new FormAttachment(0, 251);
		formData_7.top = new FormAttachment(0, 70);
		combo.setLayoutData(formData_7);

		text_2 = new Text(container, SWT.BORDER);
		text_2.setEditable(false);
		final FormData formData_4_1 = new FormData();
		formData_4_1.top = new FormAttachment(0, 46);
		formData_4_1.left = new FormAttachment(0, 136);
		formData_4_1.bottom = new FormAttachment(0, 66);
		formData_4_1.right = new FormAttachment(0, 251);
		text_2.setLayoutData(formData_4_1);

		final Label selectedMembersLabel = new Label(container, SWT.NONE);
		final FormData formData_5 = new FormData();
		formData_5.left = new FormAttachment(0, 360);
		formData_5.bottom = new FormAttachment(0, 96);
		formData_5.right = new FormAttachment(0, 455);
		formData_5.top = new FormAttachment(0, 80);
		selectedMembersLabel.setLayoutData(formData_5);
		selectedMembersLabel.setText("Selected Members:");
		//
		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(638, 594);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("User Security Configuration Mapping");
	}

}

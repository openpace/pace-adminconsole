/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.constants.ErrorConstants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.exceptions.InvalidStateException;
import com.pace.admin.global.model.PaceTree;
import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.admin.global.model.PaceViewTupleTree;
import com.pace.admin.global.model.PageTupleList;
import com.pace.admin.global.model.PageTupleNode;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PaceTreeNodeUtil;
import com.pace.admin.global.util.PaceTreeUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.menu.MenuPlugin;

import com.pace.admin.menu.actions.NewUserSelectionAction;
import com.pace.admin.menu.actions.SortTupleAction;
import com.pace.admin.menu.actions.TreeCollapseAction;
import com.pace.admin.menu.actions.TreeExpandAction;
import com.pace.admin.menu.composite.FindTreeModule;
import com.pace.admin.menu.dialogs.input.ViewSectionTuplesDialogInput;
import com.pace.admin.menu.providers.PaceListContentProvider;
import com.pace.admin.menu.providers.PaceListLabelProvider;
import com.pace.admin.menu.providers.PaceTreeContentProvider;
import com.pace.admin.menu.providers.PaceTreeLabelProvider;
import com.pace.admin.menu.providers.TupleTreeLabelProvider;
import com.pace.base.SortOrder;
import com.pace.base.SortPriority;
import com.pace.base.view.PafBorder;
import com.pace.base.view.ViewTuple;
import com.swtdesigner.ResourceManager;

public class ViewSectionTuplesDialog extends Dialog  {
		
	//private static final String COLLAPSE_ALL = "Collapse All";
	//private static final String EXPAND_ALL = "Expand All";
	private static final String CLEAR_ALL_SORTS = "Clear All Sorts";
//	private Tree tree;
	private static final String COLLAPSE_SELECTION = "Collapse Selection";
	private static final String EXPAND_SELECTION = "Expand Selection";
	private static final String USER_SELECTIONS = "User Selections";
	private static final String NEW_USER_SELECTION = "Add New User Selection";
	private static final int PASTE_BEFORE = -1;
	private static final int PASTE_AFTER = 1;
	private Tree rowTree;
	private Tree rowTupleTree;
	private List pageTupleList;
	private TreeViewer colTupleTreeViewer;
	private Tree colTupleTree;
	private TreeViewer colTreeViewer;
	private Tree colTree;
	private Button dataBorderBottomCheck;
	private Button dataBorderLeftCheck;
	private Button dataBorderTopCheck;
	private Button dataBorderRightCheck;
	private Button dataBorderAllCheck;
	private Button headerBorderBottomCheck;
	private Button headerBorderLeftCheck;
	private Button headerBorderTopCheck;
	private Button headerBorderRightCheck;
	private Button headerBorderAllCheck;
	private Button cancelButton;
	private Button applyButton;
	private Combo dataOverrideNumeric;
	private Combo dataGlobalStyle;
	private Combo dataMemberTagEditableCombo;
	private Button dataOverrideProtectionCheck;
	private Combo headerGlobalStyle;
	private Text headerRowHeight;
	private Text headerColumnWidth;
	private Button headerParentFirstButton;

	private Combo levelGenDropDown;
	private Combo selectionTypeCombo;
	private static final String BOTTOM = "Bottom";
	private PaceTreeNode transferNode = null;
	
	// TTN-2513 Added for keeping track of freezepanes.
	private PaceTreeNode fPNode = null;
	
	private static final String[] levelGenBottomDropdownValues = new String[] { BOTTOM, LevelGenerationType.Level.toString(),
			LevelGenerationType.Generation.toString() };
	
//	private static final String[] levelGenDropdownValues = new String[] { LevelGenerationType.Level.toString(),
//			LevelGenerationType.Generation.toString() };
	
	private static final String[] EMPTY_STRING_ARRAY = new String[0];
	
	private Combo aliasTableNameCombo;
	private Tree pageTree;
	
	PaceTreeNodeProperties pageBuiltTreeNodeProps = new PaceTreeNodeProperties();
	
	PaceTreeNode pageBuiltTreeRootNode = new PaceTreeNode("Root", pageBuiltTreeNodeProps, null, null);
	private TreeViewer pageTreeViewer;
	private Button removeButton;
	private Button addButton;
	private Button upButton;
	private Button downButton;
	private TreeViewer activeTupleTreeViewer;
		
	//key is selection type and value is radio button
//	private Map<PaceTreeNodeSelectionType, Button> memberSelectionButtonMap = new HashMap<PaceTreeNodeSelectionType, Button>();
	private Spinner levelGenSpinner;
	private Spinner offsetMemSpinner;
	private Button replaceButton;
	
	//private PageTupleList paceList;
	private ListViewer pageTupleListViewer;
	private TabFolder availTabFolder;
	private TabFolder selTabFolder;
	private ViewSectionTuplesDialogInput input;

	private boolean dialogAreaBuilt = false;
	private TreeViewer rowTupleTreeViewer;
	private TreeViewer rowTreeViewer;
	protected ModifyListener headerDataModifyTextListener;
	//private ToolItem pageExpandAllToolItem;
	//private ToolItem pageCollapseAllToolItem;	
	private ToolItem colClearAllSortsToolItem; //clear sort


	private Shell parentShell;	
	
	/**
	 * @return the dialogAreaBuilt
	 */
	private boolean isDialogAreaBuilt() {
		
		return dialogAreaBuilt;
		
	}

	public ViewSectionTuplesDialog(Shell parentShell, ViewSectionTuplesDialogInput input) throws Exception {
		
		super(parentShell);
		this.parentShell = parentShell;
		
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);

		//TODO: VALIDATE INPUT
		this.input = input;
		
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
	
		Composite container = (Composite) super.createDialogArea(parent);
//		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		
		if ( input.getPageAxisTree().getAliasTableList() != null ) {
		}

		final Composite treeComposite = new Composite(container, SWT.NONE);
		final GridData gd_treeComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_treeComposite.heightHint = 275;
		gd_treeComposite.minimumHeight = -1;
		treeComposite.setLayoutData(gd_treeComposite);
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.numColumns = 4;
		treeComposite.setLayout(gridLayout_7);
	
		final TreeExpandAction expandTreeAction = new TreeExpandAction();
		final TreeCollapseAction collapseTreeAction = new TreeCollapseAction();
		final NewUserSelectionAction newUserSelectionAction = new NewUserSelectionAction();
		
		final SortTupleAction[] primarySortActions = new SortTupleAction[2];
		final SortTupleAction[] secondarySortActions = new SortTupleAction[2];
		final SortTupleAction[] tertiarySortActions = new SortTupleAction[2];
		
		final MenuManager availableMemberTreesMenu = new MenuManager();
		availableMemberTreesMenu.setRemoveAllWhenShown(true);
		availableMemberTreesMenu.addMenuListener(new IMenuListener() {

			/* (non-Javadoc)
			 * @see org.eclipse.jface.action.IMenuListener#menuAboutToShow(org.eclipse.jface.action.IMenuManager)
			 */
			public void menuAboutToShow(IMenuManager manager) {
				IStructuredSelection selection = (IStructuredSelection) getActivePaceTreeViewer()
						.getSelection();
				
				if (!selection.isEmpty()) {
					expandTreeAction.setText(EXPAND_SELECTION);
					expandTreeAction.setViewer(getActivePaceTreeViewer());					
					collapseTreeAction.setText(COLLAPSE_SELECTION);
					collapseTreeAction.setViewer(getActivePaceTreeViewer());
					
									
					availableMemberTreesMenu.add(expandTreeAction);
					availableMemberTreesMenu.add(collapseTreeAction);

					availableMemberTreesMenu.add(new Separator());

					Object object = selection.getFirstElement();
					if (object instanceof PaceTreeNode) {
						PaceTreeNode userSelectionNode = (PaceTreeNode)object;
						PaceTreeNodeProperties props = userSelectionNode.getProperties();
						if( userSelectionNode.getName().equals(USER_SELECTIONS)
								|| props.getType().equals(PaceTreeNodeType.UserSelSingle) 
								|| props.getType().equals(PaceTreeNodeType.UserSelMulti) ) {
							newUserSelectionAction.setText(NEW_USER_SELECTION);
							newUserSelectionAction.setParentShell(parentShell);
							newUserSelectionAction.setProject(input.getProject());
							newUserSelectionAction.setNodeProps(props);
							newUserSelectionAction.setAxis(getSelectedPaceTreeAxis());
							availableMemberTreesMenu.add(newUserSelectionAction);
						}
					}
				}
			}
		});

		final Composite composite_6 = new Composite(treeComposite, SWT.NONE);
		composite_6.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		final GridLayout gridLayout_12 = new GridLayout();
		gridLayout_12.marginWidth = 0;
		gridLayout_12.numColumns = 2;
		composite_6.setLayout(gridLayout_12);

		final Label aliasTableLabel = new Label(composite_6, SWT.NONE);
		
		aliasTableLabel.setText("Alias Table: ");

		aliasTableNameCombo = new Combo(composite_6, SWT.READ_ONLY);
		aliasTableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		aliasTableNameCombo.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(final SelectionEvent e) {
				
				
				((PaceTree) pageTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
				pageTreeViewer.refresh(true);
											
				PageTupleList paceList = (PageTupleList) pageTupleListViewer.getInput();
				
				paceList.setAliasTableToDisplay(aliasTableNameCombo.getText());
				
				pageTupleListViewer.refresh(true);				
				
				((PaceTree) colTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
				colTreeViewer.refresh(true);
				
				((PaceTree) colTupleTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
				colTupleTreeViewer.refresh(true);				
				
				((PaceTree) rowTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
				rowTreeViewer.refresh(true);
				
				((PaceTree) rowTupleTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
				rowTupleTreeViewer.refresh(true);				
				
				
			}
			
		});
			
		EditorControlUtil.addItems(aliasTableNameCombo, input.getPageAxisTree().getAliasTableList().toArray(new String[0]));
		aliasTableNameCombo.select(0);
		new Label(treeComposite, SWT.NONE);
		new Label(treeComposite, SWT.NONE);
		new Label(treeComposite, SWT.NONE);

		final Label availableMembersLabel = new Label(treeComposite, SWT.NONE);
		availableMembersLabel.setText("Available Members:");
		new Label(treeComposite, SWT.NONE);

		final Label selectedMembersLabel = new Label(treeComposite, SWT.NONE);
		selectedMembersLabel.setText("Selected Members:");
		new Label(treeComposite, SWT.NONE);

		availTabFolder = new TabFolder(treeComposite, SWT.NONE);
		final GridData gd_availTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_availTabFolder.widthHint = 180;
		availTabFolder.setLayoutData(gd_availTabFolder);

		availTabFolder.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				
					updateTabs(availTabFolder.getSelectionIndex());
					
			}
			
		});
					
		final TabItem pageTabItem = new TabItem(availTabFolder, SWT.NONE);
		pageTabItem.setText("Page");

		final Composite availPageComposite = new Composite(availTabFolder, SWT.NONE);
		final GridLayout gridLayout_13 = new GridLayout();
		gridLayout_13.marginBottom = 5;
		gridLayout_13.marginHeight = 0;
		gridLayout_13.verticalSpacing = 0;
		availPageComposite.setLayout(gridLayout_13);
		pageTabItem.setControl(availPageComposite);		
/*
		final Composite pageAvailMemberToolBarComp = new Composite(availPageComposite, SWT.NONE);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginWidth = 2;
		gridLayout_2.marginHeight = 0;
		pageAvailMemberToolBarComp.setLayout(gridLayout_2);

		final ToolBar pageAvailMembersToolBar = new ToolBar(pageAvailMemberToolBarComp, SWT.NONE);

		pageExpandAllToolItem = new ToolItem(pageAvailMembersToolBar, SWT.PUSH);
		pageExpandAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				if ( isDialogAreaBuilt() ) {
					
					pageTreeViewer.expandAll();
					
				}
			}
		});
		pageExpandAllToolItem.setToolTipText(EXPAND_ALL);
		pageExpandAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/expandall.gif"));

		pageCollapseAllToolItem = new ToolItem(pageAvailMembersToolBar, SWT.PUSH);
		pageCollapseAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					pageTreeViewer.collapseAll();
					
				}
				
			}
		});
		pageCollapseAllToolItem.setToolTipText(COLLAPSE_ALL);
		pageCollapseAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/collapseall.gif"));
*/
		final FindTreeModule ftm_page = new FindTreeModule(availPageComposite, SWT.NONE);
		final GridData gridData_ftm_page = new GridData(SWT.FILL, SWT.TOP, true, false);
		ftm_page.setLayoutData(gridData_ftm_page);
		
		pageTreeViewer = new TreeViewer(availPageComposite, SWT.BORDER | SWT.MULTI);
		pageTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		pageTreeViewer.setContentProvider(new PaceTreeContentProvider());
		pageTreeViewer.setInput(input.getPageAxisTree());
		pageTree = pageTreeViewer.getTree();
		ftm_page.setTree(pageTree);
		ftm_page.setTreeViewer(pageTreeViewer);
		pageTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			
		//don't remove, use
		pageTreeViewer.getControl().setMenu(availableMemberTreesMenu.createContextMenu(pageTreeViewer.getControl()));
		
		pageTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent arg0) {
				
				activeTupleTreeViewer = null;
				updateSelectionTypeCombo();
				updateButtons();				
				enableHeaderDataContent(false);	
			}
		});
		pageTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent arg0) {
								
				addPageNodeToPageTupleList();
				updateButtons();
												
			}
		});
		
		final TabItem colTabItem = new TabItem(availTabFolder, SWT.NONE);
		colTabItem.setText("Column");
		
		final Composite availColComposite = new Composite(availTabFolder, SWT.NONE);
		final GridLayout gridLayout_15 = new GridLayout();
		gridLayout_15.verticalSpacing = 0;
		gridLayout_15.marginBottom = 5;
		gridLayout_15.marginHeight = 0;
		availColComposite.setLayout(gridLayout_15);
		colTabItem.setControl(availColComposite);

		final Composite composite_1 = new Composite(availColComposite, SWT.NONE);
		final GridLayout gridLayout_14 = new GridLayout();
		gridLayout_14.marginWidth = 2;
		gridLayout_14.marginHeight = 0;
		composite_1.setLayout(gridLayout_14);
/*
		final ToolBar toolBar = new ToolBar(composite_1, SWT.NONE);

		final ToolItem colExpandAllToolItem = new ToolItem(toolBar, SWT.PUSH);
		colExpandAllToolItem.setToolTipText(EXPAND_ALL);
		colExpandAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					colTreeViewer.expandAll();
					
				}
			}
		});
		colExpandAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/expandall.gif"));

		final ToolItem colCollapseAllToolItem = new ToolItem(toolBar, SWT.PUSH);
		colCollapseAllToolItem.setToolTipText(COLLAPSE_ALL);
		colCollapseAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					colTreeViewer.collapseAll();
					
				}
				
			}
		});
		colCollapseAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/collapseall.gif"));
*/
		final FindTreeModule ftm_col = new FindTreeModule(availColComposite, SWT.NONE);
		final GridData gridData_ftm_col = new GridData(SWT.FILL, SWT.TOP, true, false);
		ftm_col.setLayoutData(gridData_ftm_col);
		
		colTreeViewer = new TreeViewer(availColComposite, SWT.BORDER | SWT.MULTI);
		colTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		colTreeViewer.setContentProvider(new PaceTreeContentProvider());
		colTreeViewer.setInput(input.getColumnAxisTree());
		colTree = colTreeViewer.getTree();
		ftm_col.setTree(colTree);
		ftm_col.setTreeViewer(colTreeViewer);
		colTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		colTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent arg0) {
				
				//activeTreeViewer = colTupleTreeViewer; 
				//activeTupleTreeViewer = colTreeViewer;
				activeTupleTreeViewer = null;
				updateSelectionTypeCombo();
				updateButtons();
				enableHeaderDataContent(false);
		
			}
		});
		colTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent arg0) {
				
				addPaceNodeToTupleTree((TreeViewer) arg0.getViewer(), colTupleTreeViewer);
				updateButtons();		
				
			}
		});
		
		colTreeViewer.getControl().setMenu(availableMemberTreesMenu.createContextMenu(colTreeViewer.getControl()));
		colTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		colTreeViewer.setContentProvider(new PaceTreeContentProvider());
		
		final TabItem rowTabItem = new TabItem(availTabFolder, SWT.NONE);
		rowTabItem.setText("Row");
		
		final Composite availRowComposite = new Composite(availTabFolder, SWT.NONE);
		final GridLayout gridLayout_17 = new GridLayout();
		gridLayout_17.verticalSpacing = 0;
		gridLayout_17.marginHeight = 0;
		gridLayout_17.marginBottom = 5;
		availRowComposite.setLayout(gridLayout_17);
		rowTabItem.setControl(availRowComposite);		

		final Composite composite_7 = new Composite(availRowComposite, SWT.NONE);
		final GridLayout gridLayout_16 = new GridLayout();
		gridLayout_16.marginWidth = 2;
		gridLayout_16.marginHeight = 0;
		composite_7.setLayout(gridLayout_16);
/*
		final ToolBar toolBar_1 = new ToolBar(composite_7, SWT.NONE);

		final ToolItem rowExpandAllToolItem = new ToolItem(toolBar_1, SWT.PUSH);
		rowExpandAllToolItem.setToolTipText(EXPAND_ALL);
		rowExpandAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					rowTreeViewer.expandAll();
					
				}
				
			}
		});
		rowExpandAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/expandall.gif"));

		final ToolItem rowCollapseAllToolItem = new ToolItem(toolBar_1, SWT.PUSH);
		rowCollapseAllToolItem.setToolTipText(COLLAPSE_ALL);
		rowCollapseAllToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					rowTreeViewer.collapseAll();
					
				}
				
			}
		});
		
		rowCollapseAllToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/collapseall.gif"));
*/
		final FindTreeModule ftm = new FindTreeModule(availRowComposite, SWT.NONE);
		final GridData gridData_ftm = new GridData(SWT.FILL, SWT.TOP, true, false);
		ftm.setLayoutData(gridData_ftm);
		
		rowTreeViewer = new TreeViewer(availRowComposite, SWT.BORDER | SWT.MULTI);
		rowTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		rowTreeViewer.setContentProvider(new PaceTreeContentProvider());
		rowTreeViewer.setInput(input.getRowAxisTree());
		rowTree = rowTreeViewer.getTree();
		ftm.setTree(rowTree);
		ftm.setTreeViewer(rowTreeViewer);
		rowTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		rowTreeViewer.getControl().setMenu(availableMemberTreesMenu.createContextMenu(rowTreeViewer.getControl()));
		
		rowTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent arg0) {
				
				//activeTreeViewer = rowTupleTreeViewer; 
				//activeTupleTreeViewer = rowTreeViewer;
				activeTupleTreeViewer = null;
				updateSelectionTypeCombo();
				updateButtons();
				enableHeaderDataContent(false);
				
				
			}
		});
		rowTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent arg0) {
				
				addPaceNodeToTupleTree((TreeViewer) arg0.getViewer(), rowTupleTreeViewer);
				updateButtons();	
								
			}
		});
						
		final MenuManager tupleTreeMenuManager = new MenuManager();
		tupleTreeMenuManager.setRemoveAllWhenShown(true);
		tupleTreeMenuManager.addMenuListener(new IMenuListener() {

			/* (non-Javadoc)
			 * @see org.eclipse.jface.action.IMenuListener#menuAboutToShow(org.eclipse.jface.action.IMenuManager)
			 */
			public void menuAboutToShow(IMenuManager manager) {
				
				IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer()
						.getSelection();
				if (!selection.isEmpty()) {
					
					expandTreeAction.setText(EXPAND_SELECTION);
					expandTreeAction.setViewer(getActivePaceTupleTreeViewer());					
					collapseTreeAction.setText(COLLAPSE_SELECTION);
					collapseTreeAction.setViewer(getActivePaceTupleTreeViewer());
										
					tupleTreeMenuManager.add(expandTreeAction);
					tupleTreeMenuManager.add(collapseTreeAction);
					tupleTreeMenuManager.add(new Separator());
				}


				final java.util.List<PaceTreeNode> bottomLevelNodeList = PaceTreeUtil.getAllBottomLevelPaceTreeNodes(((PaceViewTupleTree) getActivePaceTupleTreeViewer().getInput()).getRootNode());
								
				Action selectAllAction = new Action("Select All") {
										
					@Override
					public void run() {
											
						java.util.List<PaceTreeNode> tupleList = new ArrayList<PaceTreeNode>();
						
						for (PaceTreeNode bottomLevelNode : bottomLevelNodeList ) {
							
							if ( bottomLevelNode.isViewTuple()) {
								tupleList.add(bottomLevelNode);
							}
							
						}
						
						if ( tupleList.size() > 0 ) {
							getActivePaceTupleTreeViewer().expandAll();
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection(tupleList.toArray(new PaceTreeNode[0])));
						} else {
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection());
						}
					}
				};
				
				Action selectAllBlankAction = new Action("Select All Blanks") {
										
					@Override
					public void run() {
						
						java.util.List<PaceTreeNode> pafBlankList = new ArrayList<PaceTreeNode>();
						
						for (PaceTreeNode bottomLevelNode : bottomLevelNodeList ) {
							
							if ( bottomLevelNode.isType(PaceTreeNodeType.Blank)) {
								pafBlankList.add(bottomLevelNode);
							}
							
						}
						
						if ( pafBlankList.size() > 0 ) {
							getActivePaceTupleTreeViewer().expandAll();
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection(pafBlankList.toArray(new PaceTreeNode[0])));
						} else {
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection());
						}
					}
				};
				
				Action selectAllNonBlankAction = new Action("Select All Non-Blanks") {
										
					@Override
					public void run() {
						
						java.util.List<PaceTreeNode> nonPafBlankList = new ArrayList<PaceTreeNode>();
						
						for (PaceTreeNode bottomLevelNode : bottomLevelNodeList ) {
							
							if ( ! bottomLevelNode.isType(PaceTreeNodeType.Blank)) {
								nonPafBlankList.add(bottomLevelNode);
							}
							
						}
						
						if ( nonPafBlankList.size() > 0 ) {
							getActivePaceTupleTreeViewer().expandAll();
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection(nonPafBlankList.toArray(new PaceTreeNode[0])));
						} else {
							getActivePaceTupleTreeViewer().setSelection(new StructuredSelection());
						}
						
					}
				};
				
				tupleTreeMenuManager.add(selectAllAction);
				tupleTreeMenuManager.add(selectAllBlankAction);
				tupleTreeMenuManager.add(selectAllNonBlankAction);
				
				tupleTreeMenuManager.add(new Separator());

		
        		 TreeSelection ts = (TreeSelection) selection;
                 PaceTreeNode node = (PaceTreeNode) ts.getFirstElement();
        		
        		// TTN-2513 Create remove freeze pane action . Needs to be ceated before the set action.
        		 Action removePaneAction = new Action("Remove Freeze Pane") {
 					@Override
 					public void run() {
 						IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
 						TreeSelection ts = (TreeSelection) selection;
 						if( ts.getFirstElement() !=null && ts.getFirstElement() instanceof PaceTreeNode ) {
 							PaceTreeNode node = (PaceTreeNode)ts.getFirstElement();
 							
 							input.removeTupleFreezePane(node);
 							getActivePaceTupleTreeViewer().refresh();
 				
 						}
 					}						
 				};
                 
 				
 				// TTN-2513 Create the Freeze Pane Action
        		Action addPaneAction = new Action("Set Freeze Pane After Member") {
        		@Override
        	    public void run() {
        	        
        			IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
        			
        	        if ( getActivePaceTupleTreeViewer() != null ) {
        	        
        	            Object nodeS = selection.getFirstElement();
        	            
        	            if( nodeS instanceof PaceTreeNode ) {
        	                //reset if there is one already, but reset only for the selected axis.
        	            	if(input.getColumnAxisTupleTree().findTreeNode(((PaceTreeNode) nodeS).getName())!=null && getSelectedPaceTreeAxis().equals(PaceTreeAxis.Column)){
        	            		input.removeCascadeFreezePanes(input.getColumnAxisTupleTree().getRootNode());
        	            	}
        	            	if(input.getRowAxisTupleTree().findTreeNode(((PaceTreeNode) nodeS).getName())!=null && getSelectedPaceTreeAxis().equals(PaceTreeAxis.Row)){
        	            		input.removeCascadeFreezePanes(input.getRowAxisTupleTree().getRootNode());
        	            	}
        	            	getActivePaceTupleTreeViewer().refresh(true);
        	            	// Consider the case where the freeze pane is applied to a parent node in the view tuple
        	            	// Assign it to the last child in the view tuple as per the requirements.
        	            	if(((PaceTreeNode) nodeS).hasChildren()){
        	            		PaceTreeNode lastNode = getLastNode((PaceTreeNode)nodeS);
        	            		lastNode.getProperties().setFreezePaneSet(true);
        	            	}
        	            	else
        	            		((PaceTreeNode)nodeS).getProperties().setFreezePaneSet(true);
        	                
        	                // TTN-2513 set the fpnode so it can be used to determine which node to enable the remove pane action for
        	                fPNode = (PaceTreeNode) nodeS;
        	       
        	                //refresh tree
        	                getActivePaceTupleTreeViewer().refresh(true);
        	             
        	            }
        	        } 
        	    }// run closes
        		};
        		
            
               
        		tupleTreeMenuManager.add(addPaneAction);
                tupleTreeMenuManager.add(removePaneAction);
                removePaneAction.setEnabled(false);
              
                // Enable the remove pane action if the node has been set as a freeze pane
                if(node!=null && fPNode !=null &&  fPNode.getName().equals(((PaceTreeNode)node).getName())){
                	if(node.getProperties().isFreezePaneSet()) // TTN-2616 Remove option only available for node on which freeze pane is set.
                		removePaneAction.setEnabled(true);
                }
               
                // Enable the remove pane action if the node has been saved as a freeze pane
                if(input.isRowFreezePaneExists() ){
                	for( PaceTreeNode pNode : input.getRowFreezePaneList()){
                		if(node.getName().equals(pNode.getName())){
                			if(node.getProperties().isFreezePaneSet()) // TTN-2616 Remove option only available for node on which freeze pane is set.
                				removePaneAction.setEnabled(true);
                		}
                	}
                }
                
               // Enable the remove pane action if the node has been saved as a freeze pane
                if(input.isColumnFreezePaneExists() ){
                	for( PaceTreeNode pNode : input.getColumnFreezePaneList()){
                		if(node.getName().equals(pNode.getName())){
                			if(node.getProperties().isFreezePaneSet()) // TTN-2616 Remove option only available for node on which freeze pane is set.
                				removePaneAction.setEnabled(true);
                		}
                	}
                }
              
               
        		
                tupleTreeMenuManager.add(new Separator());

                
				final Action copyMemberSet = new Action() {
					
					@Override
					public String getText() {
						return "Copy Member Selection";
					}
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
					
					@Override
					public void run() {
						
						copyMemberSet(selection);
					}

				};
				copyMemberSet.setAccelerator(SWT.CTRL | 'c');
				if( selection.size() == 1 ) {
					tupleTreeMenuManager.add(copyMemberSet);
				}
				
				if(node == null || ! node.getParentNode().isType(PaceTreeNodeType.Root)){
					copyMemberSet.setEnabled(false);
				} 
				
				final Action pasteMemberSetBefore = new Action() {
					
					@Override
					public String getText() {
						return "Paste Before";
					}
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
					
					@Override
					public void run() {
						
						pasteMemberSet(selection, PASTE_BEFORE);
					}

				};
				if( selection.size() == 1 ) {
					tupleTreeMenuManager.add(pasteMemberSetBefore);
				}
				final Action pasteMemberSetAfter = new Action() {
					
					@Override
					public String getText() {
						return "Paste After";
					}
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
					
					@Override
					public void run() {
						
						pasteMemberSet(selection, PASTE_AFTER);
					}

				};
				pasteMemberSetAfter.setAccelerator(SWT.CTRL | 'v');
				if( selection.size() == 1 ) {
					tupleTreeMenuManager.add(pasteMemberSetAfter);
				}				
				if(node == null || ! node.getParentNode().isType(PaceTreeNodeType.Root) ||  transferNode == null ){
					pasteMemberSetBefore.setEnabled(false);
					pasteMemberSetAfter.setEnabled(false);
				} 
				
				final Action deleteMemberSet = new Action() {
					
					@Override
					public String getText() {
						return "Delete Member Selection";
					}
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
					
					@Override
					public void run() {
						
						deleteMemberSet(selection);
					}

				};
				deleteMemberSet.setAccelerator(SWT.DEL);
				tupleTreeMenuManager.add(deleteMemberSet);
				tupleTreeMenuManager.add(new Separator());
				
				Action organizeMember = new Action() {
					
					@Override
					public String getText() {
						return "Organize Members";
					}
					
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
					
					
					
					@Override
					public void run() {
						
						
						openMemberReorder(selection);
						
//						TreeSelection ts = (TreeSelection) selection;
//						
//						PaceTreeNode node = (PaceTreeNode) ts.getFirstElement();
//						
//						MemberReorder mr = new MemberReorder(parentShell, node, input);
//
//						int rc = mr.open();
//						
//						if ( rc == Dialog.OK ) {
//							PaceTreeNode newNode = mr.getNewMemberReorder().reorderNode();
//							node.replace(newNode);
//							//getActivePaceTupleTreeViewer().refresh(newNode.getParentNode(), true);
//							//getActivePaceTupleTreeViewer().refresh(newNode, true);
//							getActivePaceTupleTreeViewer().refresh(true);
//						}
						
					}
				};
				
				//disable the "Organize Member" right click option if the user, 
				//did not click on a members node type.
				if(node == null || ! node.isSelectionType(PaceTreeNodeSelectionType.Members) ){
					organizeMember.setEnabled(false);
				} 
				
				tupleTreeMenuManager.add(organizeMember);
					
				tupleTreeMenuManager.add(new Separator());
				
				//TTN 906 - Presorted Ranking View
				PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
				if ( paceTree.isColumnTree() && !selection.isEmpty() && selection.size() == 1 ) {

					MenuManager primSortMenu = new MenuManager("Primary Sort", "idInsert");
					primarySortActions[0] = new SortTupleAction(SortPriority.Primary, SortOrder.Ascending, selection, colClearAllSortsToolItem );
					primarySortActions[1] = new SortTupleAction(SortPriority.Primary,SortOrder.Descending, selection, colClearAllSortsToolItem );
					if ( primarySortActions != null ) {
						for (SortTupleAction primSortAction : primarySortActions ) {
							primSortAction.setViewer(colTupleTreeViewer);
							primSortAction.setInput(input);
							primSortMenu.add(primSortAction);
						}
					}
					tupleTreeMenuManager.add(primSortMenu);
					
					MenuManager secSortMenu = new MenuManager("Secondary Sort", "idInsert");
					secondarySortActions[0] = new SortTupleAction(SortPriority.Secondary, SortOrder.Ascending, selection, colClearAllSortsToolItem );
					secondarySortActions[1] = new SortTupleAction(SortPriority.Secondary, SortOrder.Descending, selection, colClearAllSortsToolItem );
					if ( secondarySortActions != null ) {
						for (SortTupleAction sencSortAction : secondarySortActions ) {
							sencSortAction.setViewer(colTupleTreeViewer);
							sencSortAction.setInput(input);
							secSortMenu.add(sencSortAction);
						}
					}
					tupleTreeMenuManager.add(secSortMenu);
					
					MenuManager tertSortMenu = new MenuManager("Tertiary Sort", "idInsert");
					tertiarySortActions[0] = new SortTupleAction(SortPriority.Tertiary, SortOrder.Ascending, selection, colClearAllSortsToolItem );
					tertiarySortActions[1] = new SortTupleAction(SortPriority.Tertiary, SortOrder.Descending, selection, colClearAllSortsToolItem );
					if ( tertiarySortActions != null ) {
						for (SortTupleAction tertSortAction : tertiarySortActions ) {
							tertSortAction.setViewer(colTupleTreeViewer);
							tertSortAction.setInput(input);
							tertSortMenu.add(tertSortAction);
						}
					}
					tupleTreeMenuManager.add(tertSortMenu);
					
					if( node == null 
							|| ! node.isViewTuple() || ! node.isNodeTypeValidForSorting() || ! node.isSelectionTypeValidForSorting() ) {
						primSortMenu.setVisible(false);
						secSortMenu.setVisible(false);
						tertSortMenu.setVisible(false);
					}
					
					Action removeSortAction = new Action("Remove Sort") {
						@Override
						public void run() {
							IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
							TreeSelection ts = (TreeSelection) selection;
							if( ts.getFirstElement() !=null && ts.getFirstElement() instanceof PaceTreeNode ) {
								PaceTreeNode node = (PaceTreeNode)ts.getFirstElement();
								input.removeTupleSorts(node, false);
								getActivePaceTupleTreeViewer().refresh();
								if(input.findSortTuple(((PaceViewTupleTree) getActivePaceTupleTreeViewer().getInput()).getRootNode()) != null ) {
									colClearAllSortsToolItem.setEnabled(true);
								}
								else { 
									colClearAllSortsToolItem.setEnabled(false);
								}
							}
						}						
					};
					if( node.getProperties().getSortOrder() != null && node.getProperties().getSortPriority() != null ) {
						tupleTreeMenuManager.add(removeSortAction);
					}
					
					//TODO
					Action removeCascadeSortAction = new Action("Remove Sort (Cascade)") {
						@Override
						public void run() {
							IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
							TreeSelection ts = (TreeSelection) selection;
							if( ts.getFirstElement() !=null && ts.getFirstElement() instanceof PaceTreeNode ) {
								PaceTreeNode node = (PaceTreeNode) ts.getFirstElement();
								input.removeTupleSorts(node, true);
								getActivePaceTupleTreeViewer().refresh();
								if(input.findSortTuple(((PaceViewTupleTree) getActivePaceTupleTreeViewer().getInput()).getRootNode()) != null ) {
									colClearAllSortsToolItem.setEnabled(true);
								}
								else { 
									colClearAllSortsToolItem.setEnabled(false);
								}
							}
						}						
					};
					if( node.getProperties().getSortOrder() != null && node.getProperties().getSortPriority() != null ) {
						tupleTreeMenuManager.add(removeCascadeSortAction);
					}
				}
			}
		});

		
		final Composite buttonComposite1 = new Composite(treeComposite, SWT.NONE);
		final GridData gd_buttonComposite1 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_buttonComposite1.minimumWidth = -1;
		buttonComposite1.setLayoutData(gd_buttonComposite1);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.makeColumnsEqualWidth = true;
		buttonComposite1.setLayout(gridLayout_1);

		addButton = new Button(buttonComposite1, SWT.NONE);
		addButton.setEnabled(false);
		addButton.setToolTipText("Add");
		addButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/right_arrow.gif"));
		addButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
												
				PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
							
				if ( paceTree.isPageTree()) {
				
					addPageNodeToPageTupleList();
					
				} else if ( paceTree.isColumnTree()) {
					
					addPaceNodeToTupleTree(colTreeViewer, colTupleTreeViewer);
					
				} else if ( paceTree.isRowTree()) {
					
					addPaceNodeToTupleTree(rowTreeViewer, rowTupleTreeViewer);
					
				}
												
				updateButtons();	
				
			}
		});
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		removeButton = new Button(buttonComposite1, SWT.NONE);
		removeButton.setEnabled(false);
		removeButton.setToolTipText("Remove");
		//removeButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/left_arrow.gif"));
		removeButton.setImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE));
		removeButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
				
				if ( paceTree.isPageTree() ) {					
															
					removePageNodeToPageTupleList();
										
				} else {
					
					ISelection selection = getActivePaceTupleTreeViewer().getSelection();
					
					if ( selection instanceof IStructuredSelection) {
					
						IStructuredSelection structuredSelection = (IStructuredSelection) selection;
						
						Iterator<PaceTreeNode> iterator = structuredSelection.iterator();
						
						while (iterator.hasNext()) {
							
							PaceTreeNode nodeToRemove = (PaceTreeNode) iterator.next();
								
							//if members selection and has members to remove
							if ( nodeToRemove.getProperties().getSelectionType().equals(PaceTreeNodeSelectionType.Members) &&
									nodeToRemove.hasAdditionalMembers()	) {
								
								nodeToRemove.removeMember();
								
							} else {
								
								nodeToRemove.remove();
																
							}
					
						}
						
						getActivePaceTupleTreeViewer().refresh();
											
					}	
					
					
				}
						
				updateButtons();
				
			}
		});
		removeButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		replaceButton = new Button(buttonComposite1, SWT.NONE);
		replaceButton.setEnabled(false);
		replaceButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
				
				if ( paceTree.isPageTree()) {
				
					addPageNodeToPageTupleList();
					
				} else if ( paceTree.isColumnTree()) {
					
					replacePaceNodes(colTreeViewer, colTupleTreeViewer);
					
				} else if ( paceTree.isRowTree()) {
					
					replacePaceNodes(rowTreeViewer, rowTupleTreeViewer);					
				}
				
				updateButtons();	
								
			}
		});
		replaceButton.setToolTipText("Replace");
		final GridData gd_button = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_button.minimumWidth = 30;
		replaceButton.setLayoutData(gd_button);
		replaceButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/synced.gif"));

		selTabFolder = new TabFolder(treeComposite, SWT.NONE);
		final GridData gd_selTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_selTabFolder.widthHint = 180;
		selTabFolder.setLayoutData(gd_selTabFolder);

		selTabFolder.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				
					updateTabs(selTabFolder.getSelectionIndex());
					
			}
			
		});
		
		final TabItem selPageTabItem = new TabItem(selTabFolder, SWT.NONE);
		selPageTabItem.setText("Page");

		final Composite selPageComposite = new Composite(selTabFolder, SWT.NONE);
		selPageComposite.setLayout(new GridLayout());
		selPageTabItem.setControl(selPageComposite);

		pageTupleListViewer = new ListViewer(selPageComposite, SWT.V_SCROLL | SWT.MULTI | SWT.H_SCROLL | SWT.BORDER);
		pageTupleListViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent arg0) {
				
				activeTupleTreeViewer = null;
				updateSelectionTypeCombo();
				updateButtons();
				enableHeaderDataContent(false);
				
			}
		});
		pageTupleListViewer.setContentProvider(new PaceListContentProvider());
		pageTupleListViewer.setLabelProvider(new PaceListLabelProvider());
		pageTupleListViewer.setInput(input.getPageAxisTupleList());
		pageTupleList = pageTupleListViewer.getList();
		pageTupleList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		final TabItem selColumnTabItem = new TabItem(selTabFolder, SWT.NONE);
		selColumnTabItem.setText("Column");

		final Composite selColumnComposite = new Composite(selTabFolder, SWT.NONE);
		final GridLayout gridLayout_18 = new GridLayout();
		gridLayout_18.verticalSpacing = 0;
		gridLayout_18.marginHeight = 0;
		gridLayout_18.marginBottom = 5;
		selColumnComposite.setLayout(gridLayout_18);
		selColumnTabItem.setControl(selColumnComposite);
		
		final FindTreeModule ftm_col_tuple = new FindTreeModule(selColumnComposite, SWT.NONE);
		final GridData grid_ftm_col_tuple = new GridData(SWT.FILL, SWT.TOP, true, false);
		ftm_col_tuple.setLayoutData(grid_ftm_col_tuple);
	
		final ToolBar toolBar_2 = new ToolBar(selColumnComposite, SWT.NONE);
		GridData toolbargrid = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		toolBar_2.setLayoutData(toolbargrid);
/*
		final ToolItem colExpandAllToolItem2 = new ToolItem(toolBar_2, SWT.PUSH);
		//colExpandAllToolItem2.setToolTipText(EXPAND_ALL);
		colExpandAllToolItem2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/expandall.gif"));
		colExpandAllToolItem2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					colTupleTreeViewer.expandAll();
										
				}
				
			}
		});

		final ToolItem colCollapseAllToolItem2 = new ToolItem(toolBar_2, SWT.PUSH);
		colCollapseAllToolItem2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					colTupleTreeViewer.collapseAll();
										
				}

			}
		});
		//colCollapseAllToolItem2.setToolTipText(COLLAPSE_ALL);
		colCollapseAllToolItem2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/collapseall.gif"));
*/
		
		colClearAllSortsToolItem = new ToolItem(toolBar_2, SWT.PUSH);
		colClearAllSortsToolItem.setWidth(15);
		colClearAllSortsToolItem.setToolTipText(CLEAR_ALL_SORTS);
		colClearAllSortsToolItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {

					input.removeTupleSorts(null, null);
					getActivePaceTupleTreeViewer().refresh(true);
					colClearAllSortsToolItem.setEnabled(false);
				
			}
		});
		
		colClearAllSortsToolItem.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/clear_sort.gif"));
		if(input.getPafViewSectionUI() != null 
				&& input.getPafViewSectionUI().getSortingTuples() != null 
				&& input.getPafViewSectionUI().getSortingTuples().getSortingTupleList() != null 
				&& input.getPafViewSectionUI().getSortingTuples().getSortingTupleList().size() > 0 ) {
			colClearAllSortsToolItem.setEnabled(true);
		}
		else { 
			colClearAllSortsToolItem.setEnabled(false);
		}
		
		colTupleTreeViewer = new TreeViewer(selColumnComposite, SWT.MULTI | SWT.BORDER);
//		tree = colTupleTreeViewer.getTree();
//		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		colTupleTreeViewer.setLabelProvider(new TupleTreeLabelProvider());
		colTupleTreeViewer.setContentProvider(new PaceTreeContentProvider());
		colTupleTreeViewer.setInput(input.getColumnAxisTupleTree());
				
		colTupleTreeViewer.getControl().setMenu(tupleTreeMenuManager.createContextMenu(colTupleTreeViewer.getControl()));
		
		colTupleTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent arg0) {

				activeTupleTreeViewer = colTupleTreeViewer; 
				updateSelectionTypeCombo();			
				updateButtons();	
				updateHeaderDataForm(colTupleTreeViewer);
			}
		});
					
		colTupleTree = colTupleTreeViewer.getTree();
		ftm_col_tuple.setTree(colTupleTree);
		ftm_col_tuple.setTreeViewer(colTupleTreeViewer);
		colTupleTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		colTupleTree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				IStructuredSelection selection = (IStructuredSelection) colTupleTreeViewer
					.getSelection();
				if ( selection.size() > 0 ) {
					PaceTreeNode node = (PaceTreeNode) selection.getFirstElement();
					if(node != null && node.getParentNode().isType(PaceTreeNodeType.Root)){
						if ((e.stateMask & SWT.CTRL) != 0
								&& (e.keyCode == 67 || e.keyCode == 99)) {
							copyMemberSet(selection);
						} else if ((e.stateMask & SWT.CTRL) != 0
								&& (e.keyCode == 86 || e.keyCode == 118)) {
							if (transferNode != null ) {
								pasteMemberSet(selection, PASTE_AFTER);
							}
						}
					}
					if (e.keyCode == SWT.DEL) {
						deleteMemberSet(selection);
					}
				}
			}
		});

		final TabItem selRowTabItem = new TabItem(selTabFolder, SWT.NONE);
		selRowTabItem.setText("Row");

		final Composite selRowComposite = new Composite(selTabFolder, SWT.NONE);
		final GridLayout gridLayout_19 = new GridLayout();
		gridLayout_19.verticalSpacing = 0;
		gridLayout_19.marginHeight = 0;
		gridLayout_19.marginBottom = 5;
		selRowComposite.setLayout(gridLayout_19);
		selRowTabItem.setControl(selRowComposite);
/*
		final ToolBar toolBar_3 = new ToolBar(selRowComposite, SWT.NONE);

		final ToolItem rowExpandAllToolItem2 = new ToolItem(toolBar_3, SWT.PUSH);
		rowExpandAllToolItem2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				if ( isDialogAreaBuilt() ) {
					
					rowTupleTreeViewer.expandAll();
										
				}
			}
		});
		rowExpandAllToolItem2.setToolTipText(EXPAND_ALL);
		rowExpandAllToolItem2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/expandall.gif"));

		final ToolItem rowCollapseAllToolItem2 = new ToolItem(toolBar_3, SWT.PUSH);
		rowCollapseAllToolItem2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( isDialogAreaBuilt() ) {
					
					rowTupleTreeViewer.collapseAll();
										
				}
				
			}
		});
		rowCollapseAllToolItem2.setToolTipText(COLLAPSE_ALL);
		rowCollapseAllToolItem2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/collapseall.gif"));
*/
		final FindTreeModule ftm_row_tuple = new FindTreeModule(selRowComposite, SWT.NONE);
		final GridData grid_ftm_row_tuple = new GridData(SWT.FILL, SWT.TOP, true, false);
		ftm_row_tuple.setLayoutData(grid_ftm_row_tuple);
		
		rowTupleTreeViewer = new TreeViewer(selRowComposite, SWT.MULTI | SWT.BORDER);
		rowTupleTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();	
				PaceTreeNode node = (PaceTreeNode) selection.getFirstElement();
				if(node == null || node.isSelectionType(PaceTreeNodeSelectionType.Members) ){
					openMemberReorder(selection);
				} 
			}
		});
		rowTupleTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		rowTupleTreeViewer.setContentProvider(new PaceTreeContentProvider());
		rowTupleTreeViewer.setInput(input.getRowAxisTupleTree());		
		rowTupleTree = rowTupleTreeViewer.getTree();
		ftm_row_tuple.setTree(rowTupleTree);
		ftm_row_tuple.setTreeViewer(rowTupleTreeViewer);
		rowTupleTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));	
		rowTupleTree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				IStructuredSelection selection = (IStructuredSelection) rowTupleTreeViewer
					.getSelection();
				if ( selection.size() > 0 ) {
					PaceTreeNode node = (PaceTreeNode) selection.getFirstElement();
					if(node != null && node.getParentNode().isType(PaceTreeNodeType.Root)){
						if ((e.stateMask & SWT.CTRL) != 0
								&& (e.keyCode == 67 || e.keyCode == 99)) {
							copyMemberSet(selection);
						}else if ((e.stateMask & SWT.CTRL) != 0
								&& (e.keyCode == 86 || e.keyCode == 118)) {
							if (transferNode != null ) {
								pasteMemberSet(selection, PASTE_AFTER);
							}
						}
					}
					if (e.keyCode == SWT.DEL) {
						deleteMemberSet(selection);
					}
				}
			}
		});
		rowTupleTreeViewer.getControl().setMenu(tupleTreeMenuManager.createContextMenu(rowTupleTreeViewer.getControl()));
				
		final Composite buttonComposite2 = new Composite(treeComposite, SWT.NONE);
		final GridData gd_buttonComposite2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_buttonComposite2.minimumWidth = -1;
		buttonComposite2.setLayoutData(gd_buttonComposite2);
		buttonComposite2.setLayout(new GridLayout());

		upButton = new Button(buttonComposite2, SWT.NONE);
		upButton.setEnabled(false);
		upButton.setToolTipText("Move Up");
		upButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/up_arrow.gif"));
		final GridData gd_upButton = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_upButton.minimumWidth = 30;
		upButton.setLayoutData(gd_upButton);
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
									
				if ( getActivePaceTupleTreeViewer() != null ) {
								
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
	
					if ( ! selection.isEmpty() && selection.size() == 1 ) {
					
						PaceTreeNode paceNode = (PaceTreeNode) selection.getFirstElement();
						
						PaceTreeNode parentPaceNode = paceNode.getParentNode();
						
						parentPaceNode.moveChild(paceNode, -1);
												
						Object[] expandedObjects = getActivePaceTupleTreeViewer().getExpandedElements();
						
						getActivePaceTupleTreeViewer().refresh(true);
						
						getActivePaceTupleTreeViewer().setExpandedElements(expandedObjects);
						
						updateButtons();
					}
					
				}
				
			}
		});

		downButton = new Button(buttonComposite2, SWT.DOWN);
		downButton.setEnabled(false);
		downButton.setToolTipText("Move Down");
		downButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/down_arrow.gif"));
		final GridData gd_downButton = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_downButton.minimumWidth = 30;
		downButton.setLayoutData(gd_downButton);
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
												
				if ( getActivePaceTupleTreeViewer() != null ) {
					
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();
	
					if ( ! selection.isEmpty() && selection.size() == 1 ) {
					
						PaceTreeNode paceNode = (PaceTreeNode) selection.getFirstElement();
						
						PaceTreeNode parentPaceNode = paceNode.getParentNode();
						
						parentPaceNode.moveChild(paceNode, 1);
						
						Object[] expandedObjects = getActivePaceTupleTreeViewer().getExpandedElements();
						
						getActivePaceTupleTreeViewer().refresh(true);
						
						getActivePaceTupleTreeViewer().setExpandedElements(expandedObjects);
										
						updateButtons();
					}
				}
			}
		});

		final Composite composite_3 = new Composite(container, SWT.NONE);
		composite_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_5 = new GridLayout();
		composite_3.setLayout(gridLayout_5);

		final Group selectionTypeGroup = new Group(composite_3, SWT.NONE);
		selectionTypeGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		selectionTypeGroup.setText("Selection Type");
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.numColumns = 4;
		selectionTypeGroup.setLayout(gridLayout_6);

		selectionTypeCombo = new Combo(selectionTypeGroup, SWT.READ_ONLY);
		selectionTypeCombo.setEnabled(false);
		selectionTypeCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				String selectedText = selectionTypeCombo.getText();
				
				PaceTreeNodeSelectionType currentSelectedType = PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectedText);
					
				if ( currentSelectedType != null) {
					updatePaceTupleTreeNodeWithSelection(currentSelectedType);
					updateSortingTuplesWithNewSelectionType(currentSelectedType);
				}				
							
			}
		});
		final GridData gd_selectionTypeCombo = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_selectionTypeCombo.widthHint = 204;
		selectionTypeCombo.setLayoutData(gd_selectionTypeCombo);

		levelGenDropDown = new Combo(selectionTypeGroup, SWT.READ_ONLY);
		levelGenDropDown.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				PaceTreeNode activeNode = getActivePaceNode();
								
				PaceTreeNodeSelectionType selectionTypeInCombo = PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectionTypeCombo.getText());
				
				if ( selectionTypeInCombo != null && activeNode != null && activeNode.getProperties() != null &&
						activeNode.getProperties().getSelectionType() != null) {
					
					
					String levelGenDropDownValue = levelGenDropDown.getText();
					
					switch(activeNode.getProperties().getSelectionType()) {
					
					case IDescendants:
					case Descendants:
					case IDescLevel:
					case DescLevel:
					case IDescGen:
					case DescGen:
						
						if ( levelGenDropDownValue.equals(BOTTOM) ) {			

							if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.IDescendants)) {
								
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.IDescendants);
								
							} else if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.Descendants)) {
								
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Descendants);
							}
							
						} else if ( levelGenDropDownValue.equals(PaceTreeNodeSelectionType.Level.toString())) {
													
							if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.Descendants) ) {
							
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.DescLevel);
								
							} else if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.IDescendants) ) {
								
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.IDescLevel);
								
							} 											
														
						} else if ( levelGenDropDownValue.equals(PaceTreeNodeSelectionType.Generation.toString())) {
							
							if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.Descendants) ) {
								
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.DescGen);
								
							} else if ( selectionTypeInCombo.equals(PaceTreeNodeSelectionType.IDescendants)) {
								
								activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.IDescGen);
								
							} 		
							
						}
						
						break;
					case ILevel:		
					case Level:		
					case Generation:
																		
						if ( levelGenDropDownValue.equals(BOTTOM) ) {	

							activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.ILevel);

						}
						else if ( levelGenDropDownValue.equals(PaceTreeNodeSelectionType.Level.toString())) {
							
							activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Level);

						} else if ( levelGenDropDownValue.equals(PaceTreeNodeSelectionType.Generation.toString())) {
							
							activeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Generation);
							
						}
						
						break;
						
					}
					
					updateSelectionTypeCombo();
					
					getActivePaceTupleTreeViewer().refresh();
					
				}
				
			}
		});
		levelGenDropDown.setEnabled(false);
		final GridData gd_levelGenDropDown = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_levelGenDropDown.widthHint = 71;
		levelGenDropDown.setLayoutData(gd_levelGenDropDown);

		levelGenSpinner = new Spinner(selectionTypeGroup, SWT.READ_ONLY | SWT.BORDER);
		levelGenSpinner.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				PaceTreeNode activeNode = getActivePaceNode();
				
				int levelGenNumber = levelGenSpinner.getSelection();
				if ( activeNode != null && activeNode.getProperties() != null &&
						activeNode.getProperties().getSelectionType() != null) {					
					
					switch(activeNode.getProperties().getSelectionType()) {
				
					case Level:						
					case Generation:
					case IDescGen:
					case DescGen:
					case IDescLevel:
					case DescLevel:

						activeNode.getProperties().setLevelGenNumber(levelGenNumber);
						break;	
						
					case OffsetMember:
						activeNode.getProperties().setOffsetMinNumber(levelGenNumber);
						break;
					case OffsetMemberRange:
						if( offsetMemSpinner.isEnabled() 
								&& levelGenSpinner.getSelection() > offsetMemSpinner.getSelection()  ) {
							levelGenSpinner.setSelection(offsetMemSpinner.getSelection());
						}
						activeNode.getProperties().setOffsetMinNumber(levelGenSpinner.getSelection());
						break;
					
					}
					
					getActivePaceTupleTreeViewer().refresh();
					
				}
				
			}
		});
		levelGenSpinner.setEnabled(false);
		
		offsetMemSpinner = new Spinner(selectionTypeGroup, SWT.BORDER | SWT.READ_ONLY);
		offsetMemSpinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PaceTreeNode activeNode = getActivePaceNode();
				
				if ( activeNode != null && activeNode.getProperties() != null &&
						activeNode.getProperties().getSelectionType() != null) {					
					
					switch(activeNode.getProperties().getSelectionType()) {
					
					case OffsetMemberRange:
						if( levelGenSpinner.isEnabled() 
								&& levelGenSpinner.getSelection() > offsetMemSpinner.getSelection()  ) {
							offsetMemSpinner.setSelection(levelGenSpinner.getSelection());
						}
						activeNode.getProperties().setOffsetMaxNumber(offsetMemSpinner.getSelection());
						break;
					}
					
					getActivePaceTupleTreeViewer().refresh();
				}
			}
		});
		GridData gd_offsetMemSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_offsetMemSpinner.widthHint = 18;
		offsetMemSpinner.setLayoutData(gd_offsetMemSpinner);
		offsetMemSpinner.setEnabled(false);
		
		//for both level and generation
		new Label(container, SWT.NONE);

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		composite.setLayout(gridLayout_3);

		final Group headerPropertiesGroup = new Group(composite, SWT.NONE);
		headerPropertiesGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		headerPropertiesGroup.setText("Header Properties");
		final GridLayout gridLayout_9 = new GridLayout();
		gridLayout_9.numColumns = 2;
		headerPropertiesGroup.setLayout(gridLayout_9);

		headerParentFirstButton = new Button(headerPropertiesGroup, SWT.CHECK);
		headerParentFirstButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		headerParentFirstButton.setEnabled(false);
		headerParentFirstButton.setText("Parent First");
		new Label(headerPropertiesGroup, SWT.NONE);

		final Label columnWidthLabel = new Label(headerPropertiesGroup, SWT.NONE);
		columnWidthLabel.setText("Column Width:");

		headerColumnWidth = new Text(headerPropertiesGroup, SWT.BORDER);
		headerColumnWidth.setTextLimit(8);
		headerColumnWidth.addFocusListener(new FocusAdapter() {
			public void focusGained(final FocusEvent e) {
				
				headerColumnWidth.addModifyListener(headerDataModifyTextListener);
				
			}
			public void focusLost(final FocusEvent e) {
				
				headerColumnWidth.removeModifyListener(headerDataModifyTextListener);
				
			}
		});
		
		headerColumnWidth.addVerifyListener(new VerifyListener() {

			public void verifyText(VerifyEvent e) {

				verifyOnlyNumericFloat(e, headerColumnWidth.getText());

			}

		});
		
		headerColumnWidth.setEnabled(false);
		headerColumnWidth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label rowHeightLabel = new Label(headerPropertiesGroup, SWT.NONE);
		rowHeightLabel.setText("Row Height:");

		headerRowHeight = new Text(headerPropertiesGroup, SWT.BORDER);
		headerRowHeight.setTextLimit(8);
		headerRowHeight.addFocusListener(new FocusAdapter() {
			public void focusGained(final FocusEvent e) {
				
				headerRowHeight.addModifyListener(headerDataModifyTextListener);
				
			}
			public void focusLost(final FocusEvent e) {
				
				headerRowHeight.removeModifyListener(headerDataModifyTextListener);
				
			}
		});
		
		headerRowHeight.addVerifyListener(new VerifyListener() {

			public void verifyText(VerifyEvent e) {

				verifyOnlyNumericFloat(e, headerRowHeight.getText());

			}

		});
		
		headerRowHeight.setEnabled(false);
		headerRowHeight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label borderLabel = new Label(headerPropertiesGroup, SWT.NONE);
		borderLabel.setText("Border: ");

		final Composite composite_4 = new Composite(headerPropertiesGroup, SWT.BORDER);
		composite_4.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 5;
		composite_4.setLayout(gridLayout_4);

		headerBorderAllCheck = new Button(composite_4, SWT.CHECK);
		headerBorderAllCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);
				
				headerDataPropertyChanged();				
			}
		});
		headerBorderAllCheck.setEnabled(false);
		headerBorderAllCheck.setText("All");

		headerBorderRightCheck = new Button(composite_4, SWT.CHECK);
		headerBorderRightCheck.setEnabled(false);
		headerBorderRightCheck.setText("Right");
		headerBorderRightCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});
		
		
		headerBorderTopCheck = new Button(composite_4, SWT.CHECK);
		headerBorderTopCheck.setEnabled(false);
		headerBorderTopCheck.setText("Top");
		headerBorderTopCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});

		headerBorderLeftCheck = new Button(composite_4, SWT.CHECK);
		headerBorderLeftCheck.setEnabled(false);
		headerBorderLeftCheck.setText("Left");
		headerBorderLeftCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);	
				
				headerDataPropertyChanged();
			}
		});

		headerBorderBottomCheck = new Button(composite_4, SWT.CHECK);
		headerBorderBottomCheck.setEnabled(false);
		headerBorderBottomCheck.setText("Bottom");
		headerBorderBottomCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);	
				
				headerDataPropertyChanged();
			}
		});

		final Label globalStyleLabel = new Label(headerPropertiesGroup, SWT.NONE);
		globalStyleLabel.setText("Global Style:");

		headerGlobalStyle = new Combo(headerPropertiesGroup, SWT.READ_ONLY);
		headerGlobalStyle.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		headerGlobalStyle.setEnabled(false);
		headerGlobalStyle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		final Group dataPropertiesGroup = new Group(composite, SWT.NONE);
		dataPropertiesGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		dataPropertiesGroup.setText("Data Properties");
		final GridLayout gridLayout_8 = new GridLayout();
		gridLayout_8.numColumns = 2;
		dataPropertiesGroup.setLayout(gridLayout_8);

		dataOverrideProtectionCheck = new Button(dataPropertiesGroup, SWT.CHECK);
		dataOverrideProtectionCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		dataOverrideProtectionCheck.setEnabled(false);
		dataOverrideProtectionCheck.setText("Override Protection");
		new Label(dataPropertiesGroup, SWT.NONE);

		final Label memberTagEditableLabel = new Label(dataPropertiesGroup, SWT.NONE);
		memberTagEditableLabel.setText("Member Tag Editable:");

		dataMemberTagEditableCombo = new Combo(dataPropertiesGroup, SWT.READ_ONLY);
		dataMemberTagEditableCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		dataMemberTagEditableCombo.setEnabled(false);
		dataMemberTagEditableCombo.select(0);
		dataMemberTagEditableCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label borderLabel_1 = new Label(dataPropertiesGroup, SWT.NONE);
		borderLabel_1.setText("Border:");

		final Composite composite_5 = new Composite(dataPropertiesGroup, SWT.BORDER);
		composite_5.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout_10 = new GridLayout();
		gridLayout_10.numColumns = 5;
		composite_5.setLayout(gridLayout_10);

		dataBorderAllCheck = new Button(composite_5, SWT.CHECK);
		dataBorderAllCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});
		dataBorderAllCheck.setEnabled(false);
		dataBorderAllCheck.setText("All");

		dataBorderRightCheck = new Button(composite_5, SWT.CHECK);
		dataBorderRightCheck.setEnabled(false);
		dataBorderRightCheck.setText("Right");
		dataBorderRightCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});

		dataBorderTopCheck = new Button(composite_5, SWT.CHECK);
		dataBorderTopCheck.setEnabled(false);
		dataBorderTopCheck.setText("Top");
		dataBorderTopCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});
		
		dataBorderLeftCheck = new Button(composite_5, SWT.CHECK);
		dataBorderLeftCheck.setEnabled(false);
		dataBorderLeftCheck.setText("Left");
		dataBorderLeftCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});

		dataBorderBottomCheck = new Button(composite_5, SWT.CHECK);
		dataBorderBottomCheck.setEnabled(false);
		dataBorderBottomCheck.setText("Bottom");
		dataBorderBottomCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				headerDataPropertyChanged();
			}
		});

		final Label globalStyleLabel_1 = new Label(dataPropertiesGroup, SWT.NONE);
		globalStyleLabel_1.setText("Global Style:");

		dataGlobalStyle = new Combo(dataPropertiesGroup, SWT.READ_ONLY);
		dataGlobalStyle.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		dataGlobalStyle.setEnabled(false);
		dataGlobalStyle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label globalStyleLabel_1_1 = new Label(dataPropertiesGroup, SWT.NONE);
		globalStyleLabel_1_1.setToolTipText("Override Numeric Format");
		globalStyleLabel_1_1.setText("Override Num Format:");

		dataOverrideNumeric = new Combo(dataPropertiesGroup, SWT.READ_ONLY);
		dataOverrideNumeric.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				headerDataPropertyChanged();
			}
		});
		dataOverrideNumeric.setEnabled(false);
		dataOverrideNumeric.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE);

		final Composite composite_2 = new Composite(composite, SWT.NONE);
		final GridData gd_composite_2 = new GridData(SWT.RIGHT, SWT.TOP, false, false);
		gd_composite_2.widthHint = 120;
		composite_2.setLayoutData(gd_composite_2);
		final GridLayout gridLayout_11 = new GridLayout();
		gridLayout_11.numColumns = 2;
		gridLayout_11.marginWidth = 0;
		gridLayout_11.makeColumnsEqualWidth = true;
		composite_2.setLayout(gridLayout_11);

		applyButton = new Button(composite_2, SWT.NONE);
		applyButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				applyTuplePropertyChanges();
				
			}
		});
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		applyButton.setEnabled(false);
		applyButton.setText("Apply");

		cancelButton = new Button(composite_2, SWT.NONE);
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				if ( getActivePaceTupleTreeViewer() != null ) {
					
					getActivePaceTupleTreeViewer().setSelection(new StructuredSelection());
					updateSelectionTypeCombo();
					updateButtons();	
					updateHeaderDataForm(getActivePaceTupleTreeViewer());
					
				}
				
			}
		});
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		cancelButton.setEnabled(false);
		cancelButton.setText("Cancel");

		enableHeaderDataContent(false);
		
		//expand user selections, dynamic members and member tags.
		for (TreeViewer treeViewer : Arrays.asList(pageTreeViewer, colTreeViewer, rowTreeViewer)) {
		
			PaceTree treeViewerInput =(PaceTree) treeViewer.getInput();
			
			PaceTreeNode dynamicMembers = treeViewerInput.findTreeNode(Constants.DYNAMIC_MEMBERS_NODE_NAME);
			
			if ( dynamicMembers != null ) {
				
				treeViewer.expandToLevel(dynamicMembers.getParentNode(), 1);
				treeViewer.expandToLevel(dynamicMembers, 1);
				treeViewer.collapseToLevel(dynamicMembers.getParentNode(), 1);
			}
			
			
			//loop through root's children nodes, which is head node for each dim.  expand user selection if found.
			for (PaceTreeNode rootChildNode : treeViewerInput.getRootNode().getChildren()) {
			
				//user selections expand code.  If this is needed, change findTreeNode to return all found nodes, not just 1st
				PaceTreeNode userSelectionsNode = treeViewerInput.findTreeNode(rootChildNode, Constants.USER_SELECTIONS_NODE_NAME);
				
				if ( userSelectionsNode != null ) {
					
					treeViewer.expandToLevel(userSelectionsNode.getParentNode(), 1);
					treeViewer.expandToLevel(userSelectionsNode, 1);
					treeViewer.collapseToLevel(userSelectionsNode.getParentNode(), 1);
					//add User Selection
				}				
				
			}
						
			
			PaceTreeNode memberTagsNode = treeViewerInput.findTreeNode(Constants.MEMBER_TAGS_NODE_NAME);
			
			if ( memberTagsNode != null ) {
				
				treeViewer.expandToLevel(memberTagsNode, 1);
				
			}
			
		}
				
		colTupleTreeViewer.expandAll();
		rowTupleTreeViewer.expandAll();
		
		dialogAreaBuilt = true;
		
		initFormItems();
		
		//
		return container;
		
	}
		
	protected void applyTuplePropertyChanges() {

		TreeViewer treeViewer = getActivePaceTupleTreeViewer();
		
		if ( treeViewer != null ) {
		
			IStructuredSelection structuredSelection = (IStructuredSelection) treeViewer.getSelection();			
								
//			boolean allSelectionsMemberTags = true;
			
			java.util.List<PaceTreeNode> paceTreeNodesList = new ArrayList<PaceTreeNode>();
			
			for (Object selection : structuredSelection.toArray() ) {
				
				if ( selection instanceof PaceTreeNode) {
				
					PaceTreeNode selectedTreeNode = (PaceTreeNode) selection;
												
					//if node is not member tag, set flag to false so control will be 
					//disabled.
//					if ( ! selectedTreeNode.isType(PaceTreeNodeType.MemberTag)) {
//						allSelectionsMemberTags = false;
//					}
					
					paceTreeNodesList.add(selectedTreeNode);
					
				} 
				
			}
			
			for (PaceTreeNode paceTreeNode : paceTreeNodesList) {

				ViewTuple vt = createViewTupleFromForm(paceTreeNode.getViewTuple());
				
				ViewTuple clonedViewTuple = vt.clone();
				
				paceTreeNode.setViewTuple(clonedViewTuple);						
				
			}
			
			updateSelectionTypeCombo();
			updateButtons();	
			updateHeaderDataForm(getActivePaceTupleTreeViewer());
		
		}				
		
	}

	private void initFormItems() {

		if ( isDialogAreaBuilt() ) {
			
			EditorControlUtil.addItems(headerGlobalStyle, input.getGlobalSytleNames().toArray(new String[0]));
			EditorControlUtil.addItems(dataGlobalStyle, input.getGlobalSytleNames().toArray(new String[0]));
			EditorControlUtil.addItems(dataOverrideNumeric, input.getNumericFormatNames().toArray(new String[0]));
			EditorControlUtil.addItems(dataMemberTagEditableCombo, new String[] { Constants.DROPDOWN_GLOBAL_OPTION, 
					StringUtil.initCap(Boolean.TRUE.toString()), StringUtil.initCap(Boolean.FALSE.toString()) });
			
			
			headerDataModifyTextListener = new ModifyListener() {
				public void modifyText(final ModifyEvent arg0) {
					headerDataPropertyChanged();
				}
			};
			
			colTupleTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
				public void doubleClick(final DoubleClickEvent arg0) {
					IStructuredSelection selection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();	
					TreeSelection ts = (TreeSelection) selection;
					PaceTreeNode node = (PaceTreeNode) ts.getFirstElement();
					if(node == null || node.isSelectionType(PaceTreeNodeSelectionType.Members) ){
						openMemberReorder(selection);
					} 
				}
			});
			
			rowTupleTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
				public void selectionChanged(final SelectionChangedEvent arg0) {

					activeTupleTreeViewer = rowTupleTreeViewer; 
					updateSelectionTypeCombo();
					updateButtons();	
					updateHeaderDataForm(rowTupleTreeViewer);
					
				}
			});
			
		}
		
	}

	protected void updateButtons() {
		
		if ( isDialogAreaBuilt() ) {
		
			PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
			
			if ( paceTree.isPageTree()) {
			
				updateSwapAddButtons(pageTreeViewer, pageTupleListViewer);
				updateRemoveButton(pageTupleListViewer);
				upButton.setEnabled(false);
				downButton.setEnabled(false);
				
			} else if ( paceTree.isColumnTree()) {
				
				updateSwapAddButtons(colTreeViewer, colTupleTreeViewer);
				updateRemoveButton(colTupleTreeViewer);
				updateUpDownButtons(colTupleTreeViewer);
				
			} else if ( paceTree.isRowTree()) {
				
				updateSwapAddButtons(rowTreeViewer, rowTupleTreeViewer);
				updateRemoveButton(rowTupleTreeViewer);
				updateUpDownButtons(rowTupleTreeViewer);
				
			}

		}
	}

	private void updateUpDownButtons(TreeViewer treeViewer) {

	
		IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
		
		if ( ! selection.isEmpty() && selection.size() == 1 ) {
			
			PaceTreeNode paceNode = (PaceTreeNode) selection.getFirstElement();
			
			PaceTreeNode parentPaceNode = paceNode.getParentNode();
			
			int paceNodeNdx = parentPaceNode.getChildIndex(paceNode);
			
			upButton.setEnabled(paceNodeNdx != 0);
			downButton.setEnabled(paceNodeNdx != parentPaceNode.getChildren().size()-1);			
			
		} else {
			
			upButton.setEnabled(false);
			downButton.setEnabled(false);
			
		}
			
	}

	
	private void updateRemoveButton(TreeViewer toViewer) {
		
		IStructuredSelection listViewerSelection =  (IStructuredSelection) toViewer.getSelection();
		
		if ( listViewerSelection.isEmpty()) {
			
			removeButton.setEnabled(false);
			
		} else {
			
			removeButton.setEnabled(true);
			
		}		
	}
	
	private void updateRemoveButton(ListViewer toViewer) {
				
		IStructuredSelection listViewerSelection =  (IStructuredSelection) toViewer.getSelection();
		
		PageTupleList pageTupleList = (PageTupleList) toViewer.getInput();
		
		if ( listViewerSelection.isEmpty()) {
			
			removeButton.setEnabled(false);
			
		} else {
			
			boolean status = true;
			
			//TTN-1334
			Object[] items = listViewerSelection.toArray();
			
			for(Object node : items){
				
				PageTupleNode paceNode = (PageTupleNode) node;
				
				if(!pageTupleList.getPageTupleNodes().contains(paceNode)){
					
					status = false;
					
					break;
					
				}
			}
			
			removeButton.setEnabled(status);

		}		
		
	}
	
	protected void updateHeaderDataForm(TreeViewer treeViewer) {
				
		if ( treeViewer == null || ((IStructuredSelection) treeViewer.getSelection()).isEmpty() ) {

			enableHeaderDataContent(false);
			clearHeaderDataContent();
			
		} else {
		
			IStructuredSelection structuredSelection = (IStructuredSelection) treeViewer.getSelection();			
								
			Object[] selections = structuredSelection.toArray();
			
			int numberOfSelections = selections.length;
			
			boolean allSelectionsMemberTags = true;
			
			for (Object selection : selections ) {
				
				if ( selection instanceof PaceTreeNode) {
				
					PaceTreeNode selectedTreeNode = (PaceTreeNode) selection;
					
					if ( ! selectedTreeNode.isViewTuple() ) {
						enableHeaderDataContent(false);
						clearHeaderDataContent();
						return;
					}
					
					//if node is not member tag, set flag to false so control will be 
					//disabled.
					if ( ! selectedTreeNode.isType(PaceTreeNodeType.MemberTag)) {
						allSelectionsMemberTags = false;
					}
					
				} else {
					enableHeaderDataContent(false);
					clearHeaderDataContent();
					return;
					
				}
				
			}
			
			enableHeaderDataContent(true);
			
			dataOverrideProtectionCheck.setEnabled(! allSelectionsMemberTags);
			
			dataMemberTagEditableCombo.setEnabled(allSelectionsMemberTags);
			
			//if only one view tuple is selected, populate form with value
			if ( numberOfSelections == 1 ) {
				
				clearHeaderDataContent();
				
				PaceTreeNode selectedTreeNode = (PaceTreeNode) selections[0];
				
				ViewTuple vt = selectedTreeNode.getViewTuple();
					
				if ( vt == null ) {
					vt = new ViewTuple();
				}
				
				//vt.setMemberTag(allSelectionsMemberTags);
				
				populateHeaderDataContent(vt);
				
			} else {
				
				//enable/disable  border info
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);
				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
				
			}
						
		}
		
		updateApplyCancelButtons();
		
	}

	private void populateHeaderDataContent(ViewTuple vt) {
		
		if ( vt != null)  {
			
			//set header values
			
			if ( vt.getParentFirst() != null ) {				
				headerParentFirstButton.setSelection(vt.getParentFirst());
			} 
								
			if ( vt.getColumnWidth() != null && vt.getColumnWidth() > -1) {
				headerColumnWidth.setText(vt.getColumnWidth().toString());
			}
			
			if ( vt.getRowHeight() != null && vt.getRowHeight() > -1 ) {
				headerRowHeight.setText(vt.getRowHeight().toString());
			}
			
			if ( vt.getHeaderBorder() != null ) {
			
				PafBorder border = vt.getHeaderBorder();
				
				headerBorderAllCheck.setSelection(border.isAll());
				headerBorderLeftCheck.setSelection(border.isLeft());
				headerBorderTopCheck.setSelection(border.isTop());
				headerBorderBottomCheck.setSelection(border.isBottom());
				headerBorderRightCheck.setSelection(border.isRight());
				
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);
			}
			
			if ( vt.getHeaderGlobalStyleName() != null ) {
				
				EditorControlUtil.selectItem(headerGlobalStyle, vt.getHeaderGlobalStyleName());
				
			}
			
			//set data values
			
			if ( vt.getPlannable() != null ) {
				
				dataOverrideProtectionCheck.setSelection(! vt.getPlannable());
			}
			
			if ( vt.getDataBorder() != null ) {
				
				PafBorder border = vt.getDataBorder();
				
				dataBorderAllCheck.setSelection(border.isAll());
				dataBorderLeftCheck.setSelection(border.isLeft());
				dataBorderTopCheck.setSelection(border.isTop());
				dataBorderBottomCheck.setSelection(border.isBottom());
				dataBorderRightCheck.setSelection(border.isRight());
				
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);
				
			}
			
			if ( vt.getDataGlobalStyleName() != null ) {
				
				EditorControlUtil.selectItem(dataGlobalStyle, vt.getDataGlobalStyleName());
				
			}
			
			if ( vt.getNumberFormatOverrideLabel() != null ) {
				
				EditorControlUtil.selectItem(dataOverrideNumeric, vt.getNumberFormatOverrideLabel());
				
			}
			
			if ( vt.isMemberTag() ) {
				
				if ( vt.getIsMemberTagEditable() == null ) {
					
					EditorControlUtil.selectItem(dataMemberTagEditableCombo, ViewSectionTuplesDialog.BOTTOM);
					
				} else {
				
					EditorControlUtil.selectItem(dataMemberTagEditableCombo, StringUtil.initCap(((Boolean) vt.getIsMemberTagEditable()).toString()));
					
				}
				dataOverrideProtectionCheck.setEnabled(false);								
			}
			
		}	
		
	}

	protected void headerDataPropertyChanged() {
		
		PaceTreeNode paceTreeNode = getActivePaceNode();
				
		if ( paceTreeNode != null ) {
			
			if ( paceTreeNode.isViewTuple()) {
			
				ViewTuple vt = paceTreeNode.getViewTuple();
				
				vt = createViewTupleFromForm(vt);
				
				paceTreeNode.setViewTuple(vt);
			}			
			
		} 
				
	}
	
	private ViewTuple createViewTupleFromForm(ViewTuple vt) {

		//if view tuple is null
		if ( vt == null ) {
			vt = new ViewTuple();
		}
		
		vt.setParentFirst(headerParentFirstButton.getSelection());
		
		PafBorder headerBorder = createBorder(headerBorderAllCheck, headerBorderLeftCheck, headerBorderTopCheck,headerBorderRightCheck, headerBorderBottomCheck);
				
		vt.setHeaderBorder(headerBorder);
		
		if ( headerGlobalStyle.getText().trim().equals("") ) {
			
			vt.setHeaderGlobalStyleName(null);
			
		} else {
			
			vt.setHeaderGlobalStyleName(headerGlobalStyle.getText().trim());
			
		}
		
		switch(getSelectedPaceTreeAxis()) {
		
		case Column:
													
			if (headerColumnWidth.getText().trim().length() == 0 || headerColumnWidth.getText().equals(".")) {
				vt.setColumnWidth(null);
			} else {
				vt.setColumnWidth(new Float(headerColumnWidth.getText()));
			}
			
			break;
		case Row:
			
			if (headerRowHeight.getText().trim().length() == 0 || headerRowHeight.getText().equals(".")) {
				vt.setRowHeight(null);
			} else {
				vt.setRowHeight(new Float(headerRowHeight.getText()));
			}
								
			break;
		
		}
		
		if( vt.isMemberTag() ){
			vt.setPlannable(null);
		}
		else {
			if ( dataOverrideProtectionCheck.getSelection() ) {
				vt.setPlannable(false);
			} else {
				vt.setPlannable(true);
			}
		}
		
		PafBorder dataBorder = createBorder(dataBorderAllCheck, dataBorderLeftCheck, dataBorderTopCheck,dataBorderRightCheck, dataBorderBottomCheck);
		
		vt.setDataBorder(dataBorder);
		
		if (dataGlobalStyle.getText().length() != 0) {
			vt.setDataGlobalStyleName(dataGlobalStyle.getText());
		} else {
			vt.setDataGlobalStyleName(null);
		}

		if (dataOverrideNumeric.getText().trim().length() != 0) {
			vt.setNumberFormatOverrideLabel(dataOverrideNumeric
					.getText().trim());
		} else {
			vt.setNumberFormatOverrideLabel(null);
		}
		
		//if the dropdown has default, set value to null, otherwise convert value to a boolean
		if ( ! dataMemberTagEditableCombo.getText().equals(Constants.DROPDOWN_GLOBAL_OPTION)) {
			
			vt.setIsMemberTagEditable(Boolean.valueOf(dataMemberTagEditableCombo.getText()));
			
		} else {
			
			vt.setIsMemberTagEditable(null);
		}		
		
		
		return vt;
	}
	
	private void verifyOnlyNumericFloat(VerifyEvent event, String currentText) {

		if ( ! event.text.matches("[0123456789.]*") ) {
			event.doit = false;
		} 
		
		String newCurrentText = null;
		
		if(event.start == event.end){
			
			StringBuilder sb = new StringBuilder(currentText);
			sb.insert(event.start, event.text);			
			newCurrentText = sb.toString();
					
		} else {
			
			newCurrentText =  currentText.substring(event.start, event.end);
			newCurrentText = newCurrentText.replace(newCurrentText, "");
			
		}
		
		
		if (newCurrentText != null && newCurrentText.length() > 0 && ! newCurrentText.matches("^([0-9]*)(\\.([0-9]{0,2}))?$")) {
			
			event.doit = false;
			
		}
				
	}	
	
	private void updateApplyCancelButtons() {
	
		boolean allViewTuples = false;
		
		if ( getActivePaceTupleTreeViewer() != null ) {
			
			IStructuredSelection structuredSelection = (IStructuredSelection) getActivePaceTupleTreeViewer().getSelection();

			if ( ! structuredSelection.isEmpty() && structuredSelection.size() > 1 ) {
			
				Object[] selections = structuredSelection.toArray();
												
				for (Object selection : selections ) {
					
					if ( selection instanceof PaceTreeNode) {
					
						PaceTreeNode selectedTreeNode = (PaceTreeNode) selection;
						
						//if not view tuple, break and disable buttons
						if ( ! selectedTreeNode.isViewTuple() ) {
							allViewTuples = false;
							break;
						}
						
						//set true, should stay true if all other conditions are found
						allViewTuples = true;	
						
					} else {
						
						allViewTuples = false;
						//break and disable buttons if not
						break;
						
					}
					
				}				
				
			} 
			
		}
		
		applyButton.setEnabled(allViewTuples);
		cancelButton.setEnabled(allViewTuples);
		
	}
		
	private void clearHeaderDataContent() {

		if ( isDialogAreaBuilt() ) {
		
			//clear header content
			headerParentFirstButton.setSelection(false);
			headerBorderAllCheck.setSelection(false);
			headerBorderLeftCheck.setSelection(false);
			headerBorderRightCheck.setSelection(false);
			headerBorderTopCheck.setSelection(false);
			headerBorderBottomCheck.setSelection(false);
			headerGlobalStyle.deselectAll();
			headerGlobalStyle.clearSelection();
			headerRowHeight.setText("");
			headerColumnWidth.setText("");
			
			//clear data content
			dataOverrideProtectionCheck.setSelection(false);
			dataBorderAllCheck.setSelection(false);
			dataBorderLeftCheck.setSelection(false);
			dataBorderRightCheck.setSelection(false);
			dataBorderTopCheck.setSelection(false);
			dataBorderBottomCheck.setSelection(false);
			dataGlobalStyle.deselectAll();
			dataGlobalStyle.clearSelection();
			dataOverrideNumeric.deselectAll();
			dataOverrideNumeric.clearSelection();		
			dataMemberTagEditableCombo.select(0);
			
		}
	}

	private PafBorder createBorder(Button all, 	Button left, Button top, Button right, Button bottom) {
		
		PafBorder border = new PafBorder();
		
		if (all.getSelection()) {
			border.setBorder(PafBorder.BORDER_ALL);
		} else {

			int borderState = 0;

			if (left.getSelection()) {
				borderState = PafBorder.BORDER_LEFT;
			}

			if (top.getSelection()) {
				borderState = borderState | PafBorder.BORDER_TOP;
			}

			if (right.getSelection()) {
				borderState = borderState | PafBorder.BORDER_RIGHT;
			}

			if (bottom.getSelection()) {
				borderState = borderState | PafBorder.BORDER_BOTTOM;
			}
			border.setBorder(borderState);			
		}	
		
		return border;
	}
	
	/**	  
	 *	
	 *	Performs border logic based on which check is selected.
	 *
	 * @param all
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 */
	private void performBorderLogic(Button all, Button left, Button right,
			Button top, Button bottom) {

		if ( isDialogAreaBuilt()) {
		
			if (all.getSelection()) {
		
				left.setSelection(false);
				right.setSelection(false);
				top.setSelection(false);
				bottom.setSelection(false);
		
				left.setEnabled(false);
				right.setEnabled(false);
				top.setEnabled(false);
				bottom.setEnabled(false);
		
			} else {
				left.setEnabled(true);
				right.setEnabled(true);
				top.setEnabled(true);
				bottom.setEnabled(true);
			}
		}
	}

	
	/**
	 * 
	 *  Replaces a pace node on the toViewer with the node on the fromViewer
	 *
	 * @param fromViewer
	 * @param toViewer
	 */
	protected void replacePaceNodes(TreeViewer fromViewer,
			TreeViewer toViewer) {
		
		IStructuredSelection fromViewerSelection = (IStructuredSelection) fromViewer.getSelection();
		
		IStructuredSelection toViewerSelection = (IStructuredSelection) toViewer.getSelection();
		
		if ( fromViewerSelection.size() == 1 && toViewerSelection.size() > 0 ) {
			
			PaceTreeNode selectedFromNodeToSwap = (PaceTreeNode) fromViewerSelection.getFirstElement();
			
			java.util.List<PaceTreeNode> swappedNodeList = new ArrayList<PaceTreeNode>();
			
			for (Object selectedNode : toViewerSelection.toArray()) {
			
				PaceTreeNode selectedToNodeToSwap = (PaceTreeNode) selectedNode;
										
				boolean expandedState = toViewer.getExpandedState(selectedToNodeToSwap);
				
				Object[] expandedObjects = toViewer.getExpandedElements();
				
				PaceTreeNode swappedNode = selectedToNodeToSwap.swap(selectedFromNodeToSwap);

				//added swapping for sorting
				if( swappedNode != null ){
					if(  swappedNode.isViewTuple() ) { //just swap children and keep the sorting (C1=C2)
						if( swappedNode.isNodeTypeValidForSorting() 
								&& swappedNode.isSelectionTypeValidForSorting() ){
							if( selectedToNodeToSwap.getProperties().getSortOrder() != null //(T2)
									&& selectedToNodeToSwap.getProperties().getSortPriority() != null ) {
								//if no other same sorting tuple found, then swap sorting, otherwise don't
								PaceTreeNode tmpNode = input.findSortTuple(((PaceTree) toViewer.getInput()).getRootNode(),selectedToNodeToSwap);
								if( tmpNode == null ) {
									swappedNode.getProperties().setSortOrder(selectedToNodeToSwap.getProperties().getSortOrder());
									swappedNode.getProperties().setSortPriority(selectedToNodeToSwap.getProperties().getSortPriority());
								}
							} else {
								//No sorting, but find if sorting exists on any other existing same tuple, if yes, set the same sorting as others
								PaceTreeNode tmpNode = input.findSortTuple(((PaceTree) toViewer.getInput()).getRootNode(),swappedNode);
								if( tmpNode != null ) {
									swappedNode.getProperties().setSortOrder(tmpNode.getProperties().getSortOrder());
									swappedNode.getProperties().setSortPriority(tmpNode.getProperties().getSortPriority());
								}
							}
						}
					} else { //swapping column header, not view tuple
						//find sorting tuple under the column first
						PaceTreeNode sortTupleToSwap = input.findSortTuple(selectedToNodeToSwap);
						//The Column HAS Sorting Tuple
						if( sortTupleToSwap != null ) {//there is sorting under the selectedToNodeToSwap
							PaceTreeNode tmpNode = input.findSortTuple(((PaceTree) toViewer.getInput()).getRootNode(),sortTupleToSwap);
							//found same sorting
							if( tmpNode != null ) {
								sortTupleToSwap.getProperties().setSortOrder(tmpNode.getProperties().getSortOrder());
								sortTupleToSwap.getProperties().setSortPriority(tmpNode.getProperties().getSortPriority());
							} else { //no same sorting found
								sortTupleToSwap.getProperties().setSortOrder(null);
								sortTupleToSwap.getProperties().setSortPriority(null);
							}
						} else {//The Column has NO Sorting Tuple
							//find if there is any sortable child 
							PaceTreeNode sortTupleSwapped = input.findSortableTuple(swappedNode);
							//find if the swapped node is an existing sorting tuple 
							PaceTreeNode tmpNode = input.findSortTuple(((PaceTree) toViewer.getInput()).getRootNode(),sortTupleSwapped);
							if( tmpNode != null ) {
								sortTupleSwapped.getProperties().setSortOrder(tmpNode.getProperties().getSortOrder());
								sortTupleSwapped.getProperties().setSortPriority(tmpNode.getProperties().getSortPriority());
							}
						}
					}
				}
				
				swappedNodeList.add(swappedNode);
				
				if ( swappedNode != null ) {
												
					toViewer.refresh(true);			
					
					//if blank, expand all children
					if ( swappedNode.getProperties().getType().equals(PaceTreeNodeType.Blank)) {
						
						toViewer.expandToLevel(swappedNode, TreeViewer.ALL_LEVELS);
						
						//uncomment if you want the bottom Blank selected instead of swapped node
						//swappedNode = PaceTreeUtil.getFirstBottomLevelPaceTreeNode(swappedNode);
						
					} else if ( expandedState ) {
																							
						toViewer.setExpandedState(swappedNode, expandedState);
						
						toViewer.setExpandedElements(expandedObjects);
											
						toViewer.setExpandedState(swappedNode, expandedState);
					}
					
					//toViewer.setSelection(new StructuredSelection(swappedNode));				
					
				}
						
			}
			
			//select all swapped nodes
			toViewer.setSelection(new StructuredSelection(swappedNodeList.toArray()));
			
		}

		
	}

	protected void addPageNodeToPageTupleList() {

		IStructuredSelection selection = (IStructuredSelection)pageTreeViewer.getSelection();
		
		if ( selection.size() > 0 ) {
			for( Iterator<PaceTreeNode> it = selection.iterator(); it.hasNext();) {
				PaceTreeNode paceTreeNode = it.next();
				if ( paceTreeNode.canAdd(paceTreeNode)) {
				
					PageTupleNode listNode = new PageTupleNode(paceTreeNode.getProperties().getDimensionName(), paceTreeNode.getName(), paceTreeNode.getProperties().getAliasTableMap());
										
					listNode.setAliasTableName(aliasTableNameCombo.getText());
					
					PageTupleList pageTupleList = (PageTupleList) pageTupleListViewer.getInput();
					
					pageTupleList.addItem(listNode);
					
					pageTupleListViewer.refresh(true);
					
					pageTupleListViewer.setSelection(new StructuredSelection(listNode));
					
				}
				
			}
			
		}
		
	}
	
	protected void removePageNodeToPageTupleList() {

		ISelection selection = pageTupleListViewer.getSelection();
		
		if ( selection instanceof IStructuredSelection) {
			
			PageTupleList pageTupleList = (PageTupleList) pageTupleListViewer.getInput();
			
			for (Object selectionObject : ((IStructuredSelection) selection).toArray()) {
			
				if ( selectionObject instanceof PageTupleNode ) {
					
					PageTupleNode nodeToRemove = (PageTupleNode) selectionObject;
																								
					pageTupleList.removeItem(nodeToRemove);
												
				}
				
			}
			
			pageTupleListViewer.refresh(true);
									
		}
		
	}
	
	/**
	 * 
	 *  Gets the last pace tree node selected in either the row or column tree viewers or
	 *  in the row or column tuple tree viewer.
	 *
	 * @return
	 */
	private PaceTreeNode getActivePaceNode() {
		
		PaceTreeNode activePaceNode = null;
		
		if ( activeTupleTreeViewer != null ) {
			
			ISelection selection = activeTupleTreeViewer.getSelection();
			
			if ( selection instanceof IStructuredSelection ) {
				
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				
				if ( structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof PaceTreeNode ) {
				
					activePaceNode =  (PaceTreeNode) structuredSelection.getFirstElement();									
					
				}				
				
			}	
			
		}
		
		return activePaceNode;
		
	}
	
	private PaceTreeAxis getSelectedPaceTreeAxis() {
						
		switch (availTabFolder.getSelectionIndex()) {
		
		case 0:
			return PaceTreeAxis.Page;
			
		case 1:
			return PaceTreeAxis.Column;
			
		case 2:
			
			return PaceTreeAxis.Row;
		
		}		
		
		return null;
		
	}
	
	private TreeViewer getActivePaceTreeViewer() {
		
		switch (getSelectedPaceTreeAxis()) {
		
		case Page:
			return pageTreeViewer;
			
		case Column:
			return colTreeViewer;
			
		case Row:
			
			return rowTreeViewer;
		
		}
			
		return null;
		
	}
	
	private TreeViewer getActivePaceTupleTreeViewer() {
		
		PaceTree paceTree = (PaceTree) getActivePaceTreeViewer().getInput();
		
		if ( ! paceTree.isPageTree() ) {
									
			if ( paceTree.isColumnTree()) {
				
				return colTupleTreeViewer;
				
			} else if ( paceTree.isRowTree()) {
				
				return rowTupleTreeViewer;
				
			}
		}
		
		return null;
	}
	
	protected void updatePaceTupleTreeNodeWithSelection(PaceTreeNodeSelectionType selectionType) {
		
		if ( selectionType != null) {
				
			PaceTreeNode selectedNode = getActivePaceNode();
			
			if ( selectedNode != null ) {
					
				PaceTreeNodeSelectionType currentSelectionType = selectedNode.getProperties().getSelectionType();
				
				switch(currentSelectionType) {
				
				case IDescGen:
				case DescGen:
				case Generation:
					
					switch(selectionType) {
					
					case IDescendants:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.IDescGen);
						break;
					case Descendants:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.DescGen);
						break;			
					case LevelGen:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Generation);
						break;						
					default:
						selectedNode.getProperties().setSelectionType(selectionType);
						break;
					} 
					
					break;
				case IDescLevel:
				case DescLevel:
				case Level:
					
					switch(selectionType) {
					
					case IDescendants:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.IDescLevel);
						break;
					case Descendants:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.DescLevel);
						break;				
					case LevelGen:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Level);
						break;						
					default:
						selectedNode.getProperties().setSelectionType(selectionType);
						break;
					} 
					
					break;
					
				case LevelGen:
					
					switch(selectionType) {
					
					case IDescGen:
					case DescGen:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Generation);
						break;
					case IDescLevel:
					case DescLevel:
						selectedNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Level);
						break;						
					default:
						selectedNode.getProperties().setSelectionType(selectionType);
						break;
					} 
					
					break;
					
				default:
					selectedNode.getProperties().setSelectionType(selectionType);	
					break;
				
				}
				
				//TODO:
				updateLevelGenControls(selectedNode);
//				updateSelectionTypeCombo();
				
				activeTupleTreeViewer.refresh(selectedNode, true);
				
			}
								
		}
		
		
	}

	protected void updateSortingTuplesWithNewSelectionType(PaceTreeNodeSelectionType selectionType) {
		if( selectionType != null && ! selectionType.equals(PaceTreeNodeSelectionType.Selection )) {	
			PaceTreeNode selectedNode = getActivePaceNode();
			if ( selectedNode != null ) {
				if( selectedNode.isViewTuple() ) {
					if( selectedNode.getProperties() != null && selectedNode.getProperties().getSortOrder() != null 
							&& selectedNode.getProperties().getSortPriority() != null ) {
						selectedNode.getProperties().setSortPriority(null);
						selectedNode.getProperties().setSortOrder(null);
						activeTupleTreeViewer.refresh(selectedNode, true);
					}
				}
				else {
					PaceTreeNode viewTupleNode = input.findSortTuple(selectedNode);
					if( viewTupleNode != null ) {
						viewTupleNode.getProperties().setSortPriority(null);
						viewTupleNode.getProperties().setSortOrder(null);
						activeTupleTreeViewer.refresh(viewTupleNode, true);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 *  Updates/enables the member selection type combo
	 * 
	 * @param selection
	 */
	protected void updateSelectionTypeCombo() {		
		//only update selection type combo if row or column tuple tree viewer
		if ( activeTupleTreeViewer != null && (activeTupleTreeViewer.equals(rowTupleTreeViewer) 
				|| activeTupleTreeViewer.equals(colTupleTreeViewer))) {
			
			ISelection selection = activeTupleTreeViewer.getSelection();
			
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			
			String[] selectionTypeItems = null;
			
			PaceTreeNode selectedPaceTreeNode = null;
			
			if ( structuredSelection.size() == 1 ) {
								
				selectedPaceTreeNode = (PaceTreeNode) structuredSelection.getFirstElement();
				java.util.List<PaceTreeNodeSelectionType> paceTreeNodeSelectionTypeList = PaceTreeNodeUtil.getPaceTreeNodeSelectionTypes(selectedPaceTreeNode.getProperties().getType());			
				
				java.util.List<String> paceTreeNodeSelectionTypeDisplayNames =	new ArrayList<String>();
				
				for (PaceTreeNodeSelectionType paceTreeNodeSelectionType : paceTreeNodeSelectionTypeList ) {
					
					paceTreeNodeSelectionTypeDisplayNames.add(PaceTreeNodeUtil.getDisplayNameFromSelectionType(paceTreeNodeSelectionType));
					
				}
				
				if ( paceTreeNodeSelectionTypeDisplayNames.size() > 0 ) {
					
					selectionTypeItems = paceTreeNodeSelectionTypeDisplayNames.toArray(EMPTY_STRING_ARRAY);
					
				}
				
				updateLevelGenControls(selectedPaceTreeNode);
				
			}			
			
			if ( selectionTypeItems == null ) {
				
				selectionTypeCombo.setEnabled(false);								
				selectionTypeCombo.setItems(EMPTY_STRING_ARRAY);
				updateLevelGenControls(null);
				
			} else {
				
				selectionTypeCombo.setEnabled(true);
				selectionTypeCombo.setItems(selectionTypeItems);
								
				if ( selectedPaceTreeNode == null ) {
				
					selectionTypeCombo.select(0);
					
				} else {
				
					String selectionTypeComboText = PaceTreeNodeUtil.getDisplayNameFromSelectionType(selectedPaceTreeNode.getProperties().getSelectionType()); 
					
					if ( selectionTypeComboText != null ) {
						selectionTypeCombo.setText(selectionTypeComboText);
					} else {
						selectionTypeCombo.select(0);
					}
										
				}			
				
			}			
		} else {
			
			selectionTypeCombo.setEnabled(false);								
			selectionTypeCombo.setItems(EMPTY_STRING_ARRAY);
			updateLevelGenControls(null);
		}
		
		
	}	
	
	private void updateLevelGenControls(PaceTreeNode paceTreeNode) {
						
		if ( paceTreeNode != null && paceTreeNode.getProperties() != null &&
				paceTreeNode.getProperties().getSelectionType() != null ) {
		
			PaceTreeNodeSelectionType selectionType = paceTreeNode.getProperties().getSelectionType();
			
			PaceTree activePaceTree = (PaceTree) getActivePaceTreeViewer().getInput();
			
			Map<String, Integer> dimTreeHeightMap = activePaceTree.getDimensionTreeHeightMap();
			
			String dimensionName = paceTreeNode.getProperties().getDimensionName();
			
			Integer dimensionDepth = null;
			
			if ( dimensionName != null && dimTreeHeightMap.containsKey(dimensionName)) {
				
				dimensionDepth = dimTreeHeightMap.get(dimensionName);
				
			}
			
			switch (selectionType) {
		
			case Descendants:
			case IDescendants:
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);						
				levelGenDropDown.setText(BOTTOM);
				levelGenSpinner.setEnabled(false);
				levelGenSpinner.setMinimum(0);
				levelGenSpinner.setSelection(0);
				levelGenSpinner.setMaximum(1);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				break;
			case IDescLevel:
			case DescLevel:
				
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);						
				levelGenDropDown.setText(PaceTreeNodeSelectionType.Level.toString());
				levelGenSpinner.setEnabled(true);
				
				//reset spinner selection to 0 so in range of 0 to dim depth + 1
				//levelGenSpinner.setSelection(0);
				
				int minLevelGenSpinnerLength = 0;
				int maxLevelGenSpinnerLength = dimensionDepth;
				
				//get old max spinner values and log
				int oldMaxSpinnerValue = levelGenSpinner.getMaximum();
			
				//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
				if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
					
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
					
				} else {
				
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					
				}
				
				int currentLevelGenValue = paceTreeNode.getProperties().getLevelGenNumber();
				
				if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
					
					currentLevelGenValue = levelGenSpinner.getMinimum();
					
				} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
					
					currentLevelGenValue = levelGenSpinner.getMaximum();
					
				}
				
				 paceTreeNode.getProperties().setLevelGenNumber(currentLevelGenValue);
				
				levelGenSpinner.setSelection(currentLevelGenValue);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				break;
				
			case IDescGen:
			case DescGen:
				
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);						
				levelGenDropDown.setText(PaceTreeNodeSelectionType.Generation.toString());
				levelGenSpinner.setEnabled(true);														
				
				minLevelGenSpinnerLength = 1;
				maxLevelGenSpinnerLength = dimensionDepth + 1;
				
				//get old max spinner values and log
				oldMaxSpinnerValue = levelGenSpinner.getMaximum();				
								
				//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
				if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
					
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
					
				} else {
				
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					
				}
				
				currentLevelGenValue = paceTreeNode.getProperties().getLevelGenNumber();
				
				if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
					
					currentLevelGenValue = levelGenSpinner.getMinimum();
					
				} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
					
					currentLevelGenValue = levelGenSpinner.getMaximum();
					
				}
				
				paceTreeNode.getProperties().setLevelGenNumber(currentLevelGenValue);
				
				levelGenSpinner.setSelection(currentLevelGenValue);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				
				break;
			case ILevel:
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);
				levelGenDropDown.setText(BOTTOM);
				levelGenSpinner.setEnabled(false);
				levelGenSpinner.setMinimum(0);
				levelGenSpinner.setSelection(0);
				levelGenSpinner.setMaximum(1);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				
				break;
			case LevelGen:
			case Level:
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);
				levelGenDropDown.select((new ArrayList<String>(Arrays.asList(levelGenBottomDropdownValues))).indexOf(PaceTreeNodeSelectionType.Level.toString()));							
				levelGenSpinner.setEnabled(true);				
				
				minLevelGenSpinnerLength = 0;
				maxLevelGenSpinnerLength = dimensionDepth;
				
				//get old max spinner values and log
				oldMaxSpinnerValue = levelGenSpinner.getMaximum();				
								
				//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
				if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
					
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
					
				} else {
				
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					
				}
				
				currentLevelGenValue = paceTreeNode.getProperties().getLevelGenNumber();
				
				if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
					
					currentLevelGenValue = levelGenSpinner.getMinimum();
					
				} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
					
					currentLevelGenValue = levelGenSpinner.getMaximum();
					
				}
				
				paceTreeNode.getProperties().setLevelGenNumber(currentLevelGenValue);
				
				paceTreeNode.getProperties().setSelectionType(PaceTreeNodeSelectionType.Level);
				
				levelGenSpinner.setSelection(currentLevelGenValue);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				
				break;
			case Generation:
				
				levelGenDropDown.setEnabled(true);
				levelGenDropDown.setItems(levelGenBottomDropdownValues);
				levelGenDropDown.select((new ArrayList<String>(Arrays.asList(levelGenBottomDropdownValues))).indexOf(PaceTreeNodeSelectionType.Generation.toString()));							
				levelGenSpinner.setEnabled(true);													
				
				minLevelGenSpinnerLength = 1;
				maxLevelGenSpinnerLength = dimensionDepth + 1;
				
				//get old max spinner values and log
				oldMaxSpinnerValue = levelGenSpinner.getMaximum();
								
				//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
				if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
					
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
					
				} else {
				
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					
				}
				
				currentLevelGenValue = paceTreeNode.getProperties().getLevelGenNumber();
				
				if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
					
					currentLevelGenValue = levelGenSpinner.getMinimum();
					
				} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
					
					currentLevelGenValue = levelGenSpinner.getMaximum();
					
				}
				
				paceTreeNode.getProperties().setLevelGenNumber(currentLevelGenValue);
				
				levelGenSpinner.setSelection(currentLevelGenValue);
				
				break;		
				
			case OffsetMember:
					
				levelGenDropDown.setEnabled(false);
				levelGenSpinner.setEnabled(true);	
				levelGenSpinner.setMaximum(99);
				levelGenSpinner.setMinimum(-99);
				levelGenSpinner.setSelection(paceTreeNode.getProperties().getOffsetMinNumber());
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				break;
				
			case OffsetMemberRange:
				
				levelGenDropDown.setEnabled(false);
				levelGenSpinner.setEnabled(true);													
				levelGenSpinner.setMaximum(99);
				levelGenSpinner.setMinimum(-99);
				levelGenSpinner.setSelection(paceTreeNode.getProperties().getOffsetMinNumber());
				offsetMemSpinner.setEnabled(true);
				offsetMemSpinner.setMaximum(99);
				offsetMemSpinner.setMinimum(-99);
				offsetMemSpinner.setSelection(paceTreeNode.getProperties().getOffsetMaxNumber());
				if( levelGenSpinner.getSelection() > offsetMemSpinner.getSelection() ) {
					levelGenSpinner.setSelection(offsetMemSpinner.getSelection());
					paceTreeNode.getProperties().setOffsetMinNumber(levelGenSpinner.getSelection());		
				}
				break;				
				
			default:
				levelGenDropDown.setEnabled(false);
				levelGenDropDown.setItems(EMPTY_STRING_ARRAY);
				levelGenSpinner.setEnabled(false);
				levelGenSpinner.setMinimum(0);
				levelGenSpinner.setSelection(0);
				levelGenSpinner.setMaximum(1);
				offsetMemSpinner.setEnabled(false);
				offsetMemSpinner.setSelection(0);
				break;
			
			}
		//if null, disable form items
		} else {
			levelGenDropDown.setEnabled(false);
			levelGenDropDown.setItems(EMPTY_STRING_ARRAY);
			levelGenSpinner.setEnabled(false);
			levelGenSpinner.setMinimum(0);
			levelGenSpinner.setSelection(0);
			levelGenSpinner.setMaximum(1);
			offsetMemSpinner.setEnabled(false);
			offsetMemSpinner.setSelection(0);
			
		}
		
	}
	
	
	/**
	 * 
	 *  Clears the member selection map by looping through
	 *  buttons and setting all selections to false.
	 *
	 */
//	private void clearMemberSelections() {
//
//		for (Button memberSelectionButton : memberSelectionButtonMap.values()) {
//			
//			memberSelectionButton.setSelection(false);
//			
//		}
//		
//	}
//	
	protected void addPaceNodeToTupleTree(TreeViewer fromViewer, TreeViewer toViewer) {
		
		IStructuredSelection selectionToAdd = (IStructuredSelection) fromViewer.getSelection();
		
		if ( selectionToAdd.size() > 0 ) {
			
			for( Iterator<PaceTreeNode> it = selectionToAdd.iterator(); it.hasNext();) {
				PaceTreeNode selectedFromNodeToAdd = it.next();
			
				PaceTreeNode selectedToNode = null;
				
				if ( toViewer.getSelection().isEmpty() ) {
					
					selectedToNode = ((PaceTree) toViewer.getInput()).getRootNode();
					
				//} else if ( ((IStructuredSelection) toViewer.getSelection()).size() == 1 ) {
				} else if ( ((IStructuredSelection) toViewer.getSelection()).size() > 0 ) {
					
					selectedToNode = (PaceTreeNode) ((IStructuredSelection) toViewer.getSelection()).getFirstElement();
					
				}
										
				if ( selectedToNode != null ) {
					
					PaceTreeNode newNode = selectedToNode.add(selectedFromNodeToAdd, true);
					
					PaceTreeNode tmpNode = input.findSortTuple(((PaceTree) toViewer.getInput()).getRootNode(), newNode );
					if( tmpNode != null ) {
						newNode.getProperties().setSortOrder(tmpNode.getProperties().getSortOrder());
						newNode.getProperties().setSortPriority(tmpNode.getProperties().getSortPriority());
					}
					
					System.out.println("KB,,,,,,,,,,,,,,,,,,,,,Setting properties");
					PaceTreeNode tmpFNode = input.findFreezePaneTuple(((PaceTree) toViewer.getInput()).getRootNode());
					if( tmpNode != null ) {
						newNode.getProperties().setFreezePaneSet(tmpFNode.getProperties().isFreezePaneSet());
					}
					
					if ( newNode != null ) {
	
						if ( ! toViewer.getExpandedState(newNode.getParentNode()) ) {
							
							toViewer.setExpandedState(newNode.getParentNode(), true);
							
						}
						
						toViewer.refresh(true);
						
						if ( newNode.hasChildren()) {
							
							toViewer.expandToLevel(newNode, TreeViewer.ALL_LEVELS);
							
							newNode = PaceTreeUtil.getFirstBottomLevelPaceTreeNode(newNode);
							
						} 
			
						if ( selectedToNode.isSelectionType(PaceTreeNodeSelectionType.Members) ) {
							
							newNode = selectedToNode;
							
						}
						
						ISelection toSelection = new StructuredSelection(newNode);
						
						toViewer.setSelection(toSelection);
						
					}
				}
			
			}
						
		}
		
	}

	private void updateSwapAddButtons(TreeViewer fromViewer, TreeViewer toViewer) {
		
		IStructuredSelection selectionToAdd = (IStructuredSelection) fromViewer.getSelection();
		
		if ( selectionToAdd.size() > 0 ) {
			
			PaceTreeNode selectedFromNodeToAdd = (PaceTreeNode) selectionToAdd.getFirstElement();	
			
			PaceTreeNode selectedToNode = null;
			
			//if empty, use the root node as node to add to
			if ( toViewer.getSelection().isEmpty() ) {
				
				selectedToNode = ((PaceTree) toViewer.getInput()).getRootNode();
				
			//} else if ( ((IStructuredSelection) toViewer.getSelection()).size() == 1 ) {
			} else if ( ((IStructuredSelection) toViewer.getSelection()).size() > 0 ) {
				
				selectedToNode = (PaceTreeNode) ((IStructuredSelection) toViewer.getSelection()).getFirstElement();
				
			}
			
			
			if ( selectedToNode == null ) {
			
				addButton.setEnabled(false);
				replaceButton.setEnabled(false);
				
			} else {
				
				addButton.setEnabled(selectedToNode.canAdd(selectedFromNodeToAdd));
				replaceButton.setEnabled(selectedToNode.canSwap(selectedFromNodeToAdd));
				
			}
			
			
		}
		
	}
	
	/**
	 * 
	 *  This is used for enabling/disabling the add and swap buttons for the Page Tree Viewer ONLY.
	 *
	 * @param fromViewer
	 */
	private void updateSwapAddButtons(TreeViewer fromViewer, ListViewer toViewer) {
		
		IStructuredSelection selectionToAdd = (IStructuredSelection) fromViewer.getSelection();
		
		if ( selectionToAdd.size() > 0 ) {
			
			//the item to move from left to right.
			PaceTreeNode selectedFromNodeToAdd = (PaceTreeNode) selectionToAdd.getFirstElement();	
			
			//the right hand list to accept the new item.
			PageTupleList pageTupleList = (PageTupleList) toViewer.getInput();
		

			//=Begin TTN-1334
			
			boolean hasNull = false;
			
			Object[] items = ((IStructuredSelection) toViewer.getSelection()).toArray();
			
			if(items == null){
				hasNull = true;
			}
			
			for(Object node : items){
				
				PageTupleNode paceNode = (PageTupleNode) node;
				
				if(paceNode.getDisplayName() == null || paceNode.getDisplayName() == ""){
					
					hasNull = true;
					
					break;
					
				}
			}
			
			//End TTN-1334==		
			
		
			if(hasNull){
				
				addButton.setEnabled(selectedFromNodeToAdd.canAdd(selectedFromNodeToAdd));
				replaceButton.setEnabled(false);
				
			} else if ( pageTupleList.doesItemExist(selectedFromNodeToAdd.getProperties().getDimensionName())) {
				
				addButton.setEnabled(false);
				
				try {
					pageTupleList.validate();
				} catch (InvalidStateException e) {
					addButton.setEnabled(true);
				}
				
				replaceButton.setEnabled(selectedFromNodeToAdd.canAdd(selectedFromNodeToAdd));

				
			//if page tuple list doesn't have item for dimension name, disable swap button but do add button logic				
			} else {
			
				addButton.setEnabled(selectedFromNodeToAdd.canAdd(selectedFromNodeToAdd));
				replaceButton.setEnabled(false);
			}
						
		} else {
			
			addButton.setEnabled(false);
			replaceButton.setEnabled(false);
			
		}
		
		
	}
	
	
//	private void enableLevelGenerationControls(boolean enable) {
//		
//		enableLevelGenDropdownControl(enable);
//		enableLevelGenSpinnerControl(enable);
//		
//	}
//	
//	private void enableLevelGenDropdownControl(boolean enable) {
//		
//		levelGenDropDown.setEnabled(enable);
//		
//	}
//	
//	private void enableLevelGenSpinnerControl(boolean enable) {
//		
//		levelGenSpinner.setEnabled(enable);
//	}	
	
	private void enableHeaderDataContent(boolean enabled) {
	
		if ( isDialogAreaBuilt()) {
		
			//header
			headerBorderAllCheck.setEnabled(enabled);
			headerBorderLeftCheck.setEnabled(enabled);
			headerBorderRightCheck.setEnabled(enabled);
			headerBorderTopCheck.setEnabled(enabled);
			headerBorderBottomCheck.setEnabled(enabled);
			headerGlobalStyle.setEnabled(enabled);
			headerParentFirstButton.setEnabled(enabled);
		
			switch ( getSelectedPaceTreeAxis()) {
			
			case Page:
				headerColumnWidth.setEnabled(false);
				headerRowHeight.setEnabled(false);
				break;
			case Column:
				headerColumnWidth.setEnabled(enabled);
				headerRowHeight.setEnabled(false);
				break;
			case Row:
				headerRowHeight.setEnabled(enabled);
				headerColumnWidth.setEnabled(false);				
				break;
			
			}
			
			//data
			dataBorderAllCheck.setEnabled(enabled);
			dataBorderLeftCheck.setEnabled(enabled);
			dataBorderRightCheck.setEnabled(enabled);
			dataBorderTopCheck.setEnabled(enabled);
			dataBorderBottomCheck.setEnabled(enabled);
			dataGlobalStyle.setEnabled(enabled);
			dataOverrideNumeric.setEnabled(enabled);
			
			//TTN-1988 - Updating a tuple automatically sets the tuple to read only 
//			dataOverrideProtectionCheck.setEnabled(enabled);
			//always set to false, it will enable 
//			dataMemberTagEditableCombo.setEnabled(false);
				
		}
		
	}
	
	
//	/**
//	 * Create the actions
//	 */
//	private void createActions() {
//		// Create the actions
//	}

	/**
	 * Create the menu manager
	 * @return the menu manager
	 */
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		return menuManager;
	}

	//syncs the tabs
	private void updateTabs(int index) {

		if (isDialogAreaBuilt()) {
			
			availTabFolder.setSelection(index);
			selTabFolder.setSelection(index);
			
			switch (index) {
			
			case 0:
				updateHeaderDataForm(null);
				activeTupleTreeViewer = null;
				break;
				
			case 1:
				
				updateHeaderDataForm(colTupleTreeViewer);
				
				if ( ((IStructuredSelection) colTupleTreeViewer.getSelection()).size() > 0 ) {
					
					activeTupleTreeViewer = colTupleTreeViewer;
					
				} else {
					
					//activeTupleTreeViewer = colTreeViewer;
					activeTupleTreeViewer = null;
					
				}
				
				break;
			case 2:
				updateHeaderDataForm(rowTupleTreeViewer);
				
				if ( ((IStructuredSelection) rowTupleTreeViewer.getSelection()).size() > 0 ) {
					
					activeTupleTreeViewer = rowTupleTreeViewer;
					
				} else {
					
					//activeTupleTreeViewer = rowTreeViewer;
					activeTupleTreeViewer = null;
					
				}
				
				break;
						
			}
			
			updateSelectionTypeCombo();			
			updateButtons();
			
		}

	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		if ( input.getViewSectionName() != null ) {
			newShell.setText("Assign Members: " + input.getViewSectionName());
		} 		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	protected Point getInitialSize() {
		return new Point(800, 700);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {

		if ( buttonId == IDialogConstants.OK_ID) {

			applyTuplePropertyChanges();
			
			try {
			
				PageTupleList pageTupleList = (PageTupleList) pageTupleListViewer.getInput();
				
				pageTupleList.validate();
			
				input.setPageAxisTupleList(pageTupleList);
				
			} catch (InvalidStateException e) {
				
				e.printStackTrace();
				updateTabs(0);
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage());
				return;
			}
			
			try {
				
				PaceViewTupleTree columnTupleTree = (PaceViewTupleTree) colTupleTreeViewer.getInput();
				
				columnTupleTree.validate();
				
				input.setColumnAxisTupleTree(columnTupleTree);
				//columnTupleTree.getViewTuples();
				
				//TTN 609 - Presort Ranking View
				if( ! input.validateSortingTuples() ) {
					MessageDialog.openError(parentShell, "Error", ErrorConstants.INVALID_SORTING_COMBINATION);
					return;
				}
				else
					input.saveSortingTuples();
				
				
				
			} catch (InvalidStateException e) {
				
				e.printStackTrace();
				updateTabs(1);		
				if ( e.getErroredObject() != null ) {
					colTupleTreeViewer.setSelection(new StructuredSelection(e.getErroredObject()));
				}
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage());
				return;				
			}
			
			try {
				
				PaceViewTupleTree rowTupleTree = (PaceViewTupleTree) rowTupleTreeViewer.getInput();
				
				rowTupleTree.validate();				

				//rowTupleTree.getViewTuples();
				
				input.setRowAxisTupleTree(rowTupleTree);
				
				//TTN-2513 Added to save freeze pane tuples to the view section.
				input.saveFreezePaneViewTuples();
				
			} catch (InvalidStateException e) {
				
				e.printStackTrace();
				updateTabs(2);
				if ( e.getErroredObject() != null ) {
					rowTupleTreeViewer.setSelection(new StructuredSelection(e.getErroredObject()));
				}				
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage());
				return;				
			}
						
		}
		
		super.buttonPressed(buttonId);
	}
	
	private void openMemberReorder(IStructuredSelection currentNodeSelection){

		TreeSelection ts = (TreeSelection) currentNodeSelection;
		
		PaceTreeNode node = (PaceTreeNode) ts.getFirstElement();
		
		MemberReorder mr = new MemberReorder(parentShell, node, input);

		int rc = mr.open();
		
		if ( rc == Dialog.OK ) {
			PaceTreeNode newNode = mr.getNewMemberReorder().reorderNode();
			node.replace(newNode);
			getActivePaceTupleTreeViewer().refresh(true);
		}

	}

	private void copyMemberSet(IStructuredSelection currentNodeSelection) {
		Object object = currentNodeSelection.getFirstElement();
		if (object instanceof PaceTreeNode) {
			transferNode = (PaceTreeNode) object;
		}
	}
	
	private void pasteMemberSet(IStructuredSelection currentNodeSelection, int pasteWhere)  {
		Object object = currentNodeSelection.getFirstElement();
		if (object instanceof PaceTreeNode) {
		PaceTreeNode targetNode = (PaceTreeNode) object;
			//get the existing node index, if PASTE_BEFORE, the new index will be the existing index
			Integer toTreeSelectedNodeNdx = targetNode.getParentNode().getChildren().indexOf(targetNode);
			//if PASTE_AFTER, the new index will be the existing index + 1
			if( toTreeSelectedNodeNdx >= 0 ) {
				if( pasteWhere == PASTE_AFTER){
					toTreeSelectedNodeNdx++;
				}
			}
			// TTN-2616 Set the freeze pane to false for the new node copied on the tree.
			PaceTreeNodeProperties newProps = new PaceTreeNodeProperties();
			try {
				newProps = transferNode.getProperties().clone();
			} catch (CloneNotSupportedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			PaceTreeNode newNode = new PaceTreeNode(transferNode.getName(), newProps, transferNode.getParentNode(), toTreeSelectedNodeNdx);
			newNode.getProperties().setFreezePaneSet(false);
			if	( transferNode.isSelectionType(PaceTreeNodeSelectionType.Members) && transferNode.hasAdditionalMembers() ) {
				newNode.addMembers(transferNode.getAdditionalPaceTreeNodeList());
			}
			java.util.List<PaceTreeNode> newChildren = new ArrayList<PaceTreeNode>();
			for	( PaceTreeNode child : transferNode.getChildren() ) {
				try {
					PaceTreeNode childCopy = child.clone();
					childCopy.setParentNode(newNode);
					// Set freeze pane property to false for the copied child.
					childCopy.getProperties().setFreezePaneSet(false);
					// Set freeze pane property to false for the copied children.
					if	(childCopy.hasChildren())
					{
						setChildrenFreezePane(childCopy);
						
					}
					newChildren.add(childCopy);
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			newNode.setChildren(newChildren);
			// TTN-2616 Set the freeze pane to false for the new node copied on the tree.
			newNode.getProperties().setFreezePaneSet(false);
			getActivePaceTupleTreeViewer().refresh(true);
			transferNode = null;
		
		}
	}
	
	// Set freeze pane property to false for the children.
	public void setChildrenFreezePane(PaceTreeNode node){
		if	(node.hasChildren()) {
			for	( PaceTreeNode nodeChild : node.getChildren()){
				nodeChild.getProperties().setFreezePaneSet(false);
			}
			for	( PaceTreeNode nodeChild : node.getChildren()){
				if (nodeChild.hasChildren()){
					setChildrenFreezePane(nodeChild);
				}
			}
			 
		}
	}
	private void deleteMemberSet(IStructuredSelection selection) {
		if ( selection instanceof IStructuredSelection) {
		
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			
			Iterator<PaceTreeNode> iterator = structuredSelection.iterator();
			
			while (iterator.hasNext()) {
				
				PaceTreeNode nodeToRemove = (PaceTreeNode) iterator.next();
					
				//if members selection and has members to remove
				if ( nodeToRemove.getProperties().getSelectionType().equals(PaceTreeNodeSelectionType.Members) &&
						nodeToRemove.hasAdditionalMembers()	) {
					
					nodeToRemove.removeMember();
					
				} else {
					
					nodeToRemove.remove();
													
				}
		
			}
			
			getActivePaceTupleTreeViewer().refresh();
								
		}	
		updateButtons();
		
	}
	
	private void freezePaneMemberSet(IStructuredSelection currentNodeSelection) {
		Object object = currentNodeSelection.getFirstElement();
		if (object instanceof PaceTreeNode) {
			fPNode = (PaceTreeNode) object;
		}
	}
	
	// TTN-2513 Method to get the last member in a node , recursive.
			public PaceTreeNode getLastNode( PaceTreeNode node){
				if(node.hasChildren()){
					java.util.List<PaceTreeNode> children = node.getChildren();
					int i=0;
					for(PaceTreeNode child: children){
						i++;
						if(child.hasChildren()){
							return getLastNode(child);
						
						}
						else if (children.size() == i){ // if last member reached .
							return child;
						}
					}
				}
				return null;
			}

}

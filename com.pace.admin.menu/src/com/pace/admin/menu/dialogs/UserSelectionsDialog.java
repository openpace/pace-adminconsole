/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.Arrays;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ButtonId;
import com.pace.admin.global.model.managers.UserSelectionModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.view.PafUserSelection;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimTree;

/**
 * TODO: replace with javadoc
 * 
 * @version x.xx
 * @author Jason Milliron
 * 
 */
public class UserSelectionsDialog extends Dialog {

	private Text promptStringText;

	private Combo multipleCombo;

	private PafUserSelection currentModelObject = null;

	private TreeMap<String, PafUserSelection> modelMap = null;

	static final Logger logger = Logger.getLogger(UserSelectionsDialog.class);
	private static final String VERSION="Version";
	private static final String IDESC_UOW_ROOT="@IDESC(@UOW_ROOT)";

	// form widgets
	private Text idText;

	private Combo dimensionCombo;

	// left list widget
	private List list;

	// Buttons
	private Button newButton;

	private Button deleteButton;

	private Button addButton;

	private Button cancelButton;

	// used to see if form should be changed when form item changes
	private boolean cachingEnabled = false;

	private UserSelectionModelManager modelManager;
	
	private Shell shell;
	private IProject project;
	private Text displayStringText;
	
	private Button parentFirst;
	private Button genRightClick;
	private Button levelRightClick;
	private Label lblFilterSpecification;
	private Button btnFilterSpec;
	private List specificationList;

	private boolean isNew;
	private String dimension;
	private boolean isSingle;
	private Label lblDefaultMember;
	private List defaultMbrList;
	private Button btnDefaultMbr;
	private String lastDimension;
	/**
	 * @param parentShell
	 *            parentShell
	 */
	public UserSelectionsDialog(Shell parentShell, IProject project, boolean isNew) {
		super(parentShell);

		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.shell = parentShell;
		this.project = project;		
		this.isNew = isNew;
		
		modelManager = new UserSelectionModelManager(project);

		modelMap = (TreeMap) modelManager.getModel();

	}

	/**
	 * @param parent
	 *            Parent Composite used to add the widgets to
	 * @returns Control used to paint the scrren
	 */
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.marginTop = 10;
		gridLayout.marginRight = 10;
		gridLayout.marginLeft = 10;
		gridLayout.marginBottom = 10;
		container.setLayout(gridLayout);

		Label multipleLabel_1;

		final Composite composite_4 = new Composite(container, SWT.NONE);
		final GridData gd_composite_4 = new GridData(SWT.FILL, SWT.FILL, true, true);
		//TTN-2280 Increase the width to show the scroll bar with window scaling.
		gd_composite_4.widthHint = 90;
		composite_4.setLayoutData(gd_composite_4);
		composite_4.setLayout(new GridLayout());

		final Label listLabel = new Label(composite_4, SWT.NONE);
		listLabel.setText("User Selections List:");

		list = new List(composite_4, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite composite_1 = new Composite(composite_4, SWT.NONE);
		composite_1.setLayoutData(new GridData(110, SWT.DEFAULT));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginWidth = 0;
		gridLayout_1.makeColumnsEqualWidth = true;
		gridLayout_1.numColumns = 2;
		composite_1.setLayout(gridLayout_1);

		newButton = new Button(composite_1, SWT.NONE);
		newButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		newButton.setText("New");

		deleteButton = new Button(composite_1, SWT.NONE);
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		deleteButton.setText("Delete");

		final Composite composite_2 = new Composite(container, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.marginRight = 20;
		gridLayout_3.marginLeft = 40;
		gridLayout_3.numColumns = 3;
		composite_2.setLayout(gridLayout_3);

		final Label nameLabel = new Label(composite_2, SWT.NONE);
		nameLabel.setAlignment(SWT.RIGHT);
		nameLabel.setText("Id:");

		idText = new Text(composite_2, SWT.BORDER);
		idText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		idText.setTextLimit(20);
		new Label(composite_2, SWT.NONE);

		final Label ageLabel = new Label(composite_2, SWT.NONE);
		ageLabel.setAlignment(SWT.RIGHT);
		ageLabel.setText("Dimension:");

		dimensionCombo = new Combo(composite_2, SWT.READ_ONLY);
		dimensionCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		//dimensionCombo.setItems(populateDimensions());
		dimensionCombo.setVisibleItemCount(10);
		new Label(composite_2, SWT.NONE);

		final Label multipleLabel = new Label(composite_2, SWT.NONE);
		multipleLabel.setAlignment(SWT.RIGHT);
		multipleLabel.setText("Multiple:");

		multipleCombo = new Combo(composite_2, SWT.READ_ONLY);
		multipleCombo.setItems(new String[] { "False", "True" });
		new Label(composite_2, SWT.NONE);
		
		Label displayStringLabel = new Label(composite_2, SWT.NONE);
		displayStringLabel.setText("Display String:");
		
		displayStringText = new Text(composite_2, SWT.BORDER);
		GridData gd_displayStringText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_displayStringText.heightHint = 16;
		displayStringText.setLayoutData(gd_displayStringText);
		displayStringText.setTextLimit(50);
		new Label(composite_2, SWT.NONE);
		multipleLabel_1 = new Label(composite_2, SWT.NONE);
		multipleLabel_1.setAlignment(SWT.RIGHT);
		multipleLabel_1.setText("Prompt String:");

		promptStringText = new Text(composite_2, SWT.BORDER | SWT.WRAP);
		promptStringText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite_2, SWT.NONE);
		
		lblFilterSpecification = new Label(composite_2, SWT.NONE);
		lblFilterSpecification.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		lblFilterSpecification.setText("Selection\r\nSpecifications:");
		
		specificationList = new List(composite_2, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		specificationList.setEnabled(true);
		GridData gd_specificationList = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_specificationList.widthHint = 113;
		gd_specificationList.heightHint = 84;
		specificationList.setLayoutData(gd_specificationList);
		
		btnFilterSpec = new Button(composite_2, SWT.NONE);
		btnFilterSpec.setSelection(true);
		btnFilterSpec.setEnabled(true);
		btnFilterSpec.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnFilterSpec.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				UserSelectionDimSpecWizardDialog dialog = new UserSelectionDimSpecWizardDialog(shell, project,dimensionCombo.getText(),specificationList.getItems() );
				if( dialog.getPafDimTree() == null ) {
			    	GUIUtil.openMessageWindow("Error", "Unable to open User Selection Specification dialog if the Pace Server isn't running and there are no cached dimensions.");
			    	return;
				}
				dialog.open();
				if( dialog.getUserSelectionSpecs() != null && dialog.getUserSelectionSpecs().length > 0 ) {
					specificationList.setItems(dialog.getUserSelectionSpecs());
				}
				else {
					specificationList.removeAll();
				}
				formDataChanged();
			}
		});
		btnFilterSpec.setToolTipText("Modify Current Selection...");
		btnFilterSpec.setText("...");
		
		lblDefaultMember = new Label(composite_2, SWT.NONE);
		lblDefaultMember.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		lblDefaultMember.setText("_____");
		
		defaultMbrList = new List(composite_2, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		defaultMbrList.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		defaultMbrList.setEnabled(true);
		
		btnDefaultMbr = new Button(composite_2, SWT.NONE);
		btnDefaultMbr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				PafApplicationDef[] pafAppDefAr = PafApplicationUtil.getPafApps(project);
				PafSimpleDimTree filteredTree = null;
				String dimName = dimensionCombo.getText();
				boolean hasUowRoot = false;
				try {
					java.util.List<String> members = new java.util.ArrayList<String>();
					for(Object j : specificationList.getItems()){
						String member = j.toString();
						if(!member.contains(PafBaseConstants.UOW_ROOT)){
							members.add(member);
						} else {
							hasUowRoot = true;
						}
					}
					
					
					filteredTree = DimensionTreeUtility.getFilteredDimensionTree(project, pafAppDefAr[0].getAppId(), dimensionCombo.getText(), members);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					//e1.printStackTrace();
					filteredTree = null;
				}
				
				UserSelectionDimSpecWizardDialog dialog = new UserSelectionDimSpecWizardDialog(shell, 
						project,
						dimName, 
						defaultMbrList.getItems(), 
						new Boolean(multipleCombo.getText()), 
						filteredTree,
						"User Selector Default Member Selector",
						hasUowRoot,
						String.format("The entire [%s] tree is being displayed since the selector specification contains one or more runtime tokens", dimName));
				if( dialog.getPafDimTree() == null ) {
			    	GUIUtil.openMessageWindow("Error", "Unable to open User Selection Specification dialog if the Pace Server isn't running and there are no cached dimensions.");
			    	return;
				}
				dialog.open();
				if( dialog.getUserSelectionSpecs() != null && dialog.getUserSelectionSpecs().length > 0 ) {
					defaultMbrList.setItems(dialog.getUserSelectionSpecs());
				}
				else {
					defaultMbrList.removeAll();
				}
				formDataChanged();
			}
		});
		btnDefaultMbr.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnDefaultMbr.setToolTipText("Modify Current Selection...");
		btnDefaultMbr.setText("...");
		btnDefaultMbr.setSelection(true);
		btnDefaultMbr.setEnabled(true);
		new Label(composite_2, SWT.NONE);
		
		parentFirst = new Button(composite_2, SWT.CHECK);
		parentFirst.setText("Parent First");
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		
		genRightClick = new Button(composite_2, SWT.CHECK);
		genRightClick.setText("Show Generation Right-Click");
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		
		levelRightClick = new Button(composite_2, SWT.CHECK);
		levelRightClick.setText("Show Level Right-Click");
		new Label(composite_2, SWT.NONE);

		final Composite composite_3 = new Composite(composite_2, SWT.NONE);
		final GridData gd_composite_3 = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
		gd_composite_3.widthHint = 110;
		composite_3.setLayoutData(gd_composite_3);
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginWidth = 0;
		gridLayout_4.makeColumnsEqualWidth = true;
		gridLayout_4.numColumns = 2;
		composite_3.setLayout(gridLayout_4);

		
		addButton = new Button(composite_3, SWT.NONE);
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		addButton.setVisible(false);
		addButton.setText("Add");

		cancelButton = new Button(composite_3, SWT.NONE);
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		cancelButton.setVisible(false);
		cancelButton.setText("Cancel");
		new Label(composite_2, SWT.NONE);
		
		//build the dimension list
		populateDimensions();
		
		// initially populate the list items.
		refreshList();

		// set up dialog
		initializeDialog();

		// set up listeners
		setupListeners();
		
		if( isNew ) {
			newButtonAction();
		}
		return container;
	}

	private void populateDimensions()  {
		//Load the attribute dimensions into an array list.
		PafMdbProps cachedPafMdbProps = null;
		try{
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
					PafProjectUtil.getProjectServer(project), 
					PafProjectUtil.getApplicationName(project));
		} catch(Exception e){
	    	GUIUtil.openMessageWindow("Warning",  e.getMessage());
		}
				
		if( cachedPafMdbProps != null && cachedPafMdbProps.getCachedAttributeDims().size() > 0 ) {
			for(String item : cachedPafMdbProps.getCachedAttributeDims()){
				dimensionCombo.add(item);
			}
			for(String item : getDimensions()){
				dimensionCombo.add(item);
			}
		}
		//get the list in an array
		String[] items = dimensionCombo.getItems();
		//clear an selection
		dimensionCombo.clearSelection();
		//sort the list
		Arrays.sort(items);
		//reset the items.
		dimensionCombo.setItems(items);
	}
	

	private String[] getDimensions() {
		
		return PafProjectUtil.getProjectDimensions(project);
	}

	
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(695, 523);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Constants.USER_SELECTION_WIZARD_NAME);
	}

	/**
	 * Initials the form.
	 */
	private void initializeDialog() {

		// if model map contains items
		if (modelMap.size() > 0) {
			list.setSelection(0);
			list.showSelection();
			populateForm(list.getSelection()[0]);
			enableForm(true, false);
			deleteButton.setEnabled(true);
		} else {
			list.deselectAll();
			list.setEnabled(true);
			clearForm();
			enableForm(false, true);
			deleteButton.setEnabled(false);
		}

	}

	/**
	 * Populates the form from the model map using the key param
	 * 
	 * @param key
	 *            used to get object from the model map
	 */
	private void populateForm(String key) {

		// if the model map has the key, populate the form with it
		if (modelMap.containsKey(key)) {

			cachingEnabled = false;

			idText.setEnabled(false);

			PafUserSelection model = modelMap.get(key);

			idText.setText(model.getId());

			if (model.getDimension() != null) {
				dimensionCombo.setText(model.getDimension());
				lastDimension = model.getDimension();
			}

			if (model.isMultiples()) {
				multipleCombo.select(1);
				lblDefaultMember.setText("Default\r\nMembers:");
			} else {
				multipleCombo.select(0);
				lblDefaultMember.setText("Default\r\nMember:");
			}

			if (model.getPromptString() != null) {
				promptStringText.setText(model.getPromptString());
			}

			if (model.getDisplayString() != null) {
				displayStringText.setText(model.getDisplayString());
			}
			
			parentFirst.setSelection(!model.isParentLast());
			genRightClick.setSelection(model.isGenerationRightClick());
			levelRightClick.setSelection(model.isLevelRightClick());
			
			if(model.getSpecification() != null 
					&& model.getSpecification().getExpressionList() != null 
					&& model.getSpecification().getExpressionList().length > 0 ){
				specificationList.setItems(model.getSpecification().getExpressionList());
				
			}
			
			if(model.getDefaultMembers() != null 
					&& model.getDefaultMembers().getExpressionList() != null 
					&& model.getDefaultMembers().getExpressionList().length > 0 ){
				defaultMbrList.setItems(model.getDefaultMembers().getExpressionList());
				
			}
			
			cachingEnabled = true;

		}

	}

	/**
	 * Sets up all the listeners
	 * 
	 */
	private void setupListeners() {

		// setup the list listener
		setupListListener();

		// setup all form listeners
		setupFormListeners();

		// setup all button listeners
		setupButtonListeners();

	}

	/**
	 * Sets up the listener for the list box
	 * 
	 */
	private void setupListListener() {

		list.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				// if a selection exist
				if (list.getSelectionCount() > 0) {
					logger.debug(list.getSelection()[0]);

					// save cached model if one exist
					saveCachedModel();

					// clear and popualte form from selection
					clearForm();
					populateForm(list.getSelection()[0]);

				}

			}

		});

	}

	/**
	 * Sets up the listeners for the form widgets
	 * 
	 */
	private void setupFormListeners() {

		idText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				
				checkRequiredFields();

			}

		});


		dimensionCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String origDimension = lastDimension;
				String newDimension = dimensionCombo.getText();
				if ( ! origDimension.equals(newDimension) ) {
					String key = idText.getText().trim().toUpperCase();
					PafUserSelection model = modelMap.get(key);
					boolean dataChanged = false;
					if( !GUIUtil.askUserAQuestion("The dimension has changed. As a result, the selection and default member specifications tied to the dimension will be reset.  Continue?")) {
						dimensionCombo.setText(origDimension);
						return;
					}

					if(  specificationList.getItemCount() > 0 ) {
						dataChanged = true;
						specificationList.removeAll();
					} else {
						if( !newDimension.equals(VERSION)) {
							specificationList.setItems(new String[]{IDESC_UOW_ROOT});
						}
						dataChanged = true;
					}
					
					if(  defaultMbrList.getItemCount() > 0 ) {
						dataChanged = true;
						defaultMbrList.removeAll();
					}
					
					if(dataChanged){
						formDataChanged();
					}
					
					
//					if(  specificationList.getItemCount() > 0 ) {
//						//if( ( specificationList.getItemCount() != 1 || ! specificationList.getItem(0).equals(IDESC_UOW_ROOT))) {
//							if( GUIUtil.askUserAQuestion("The dimension has changed. As a result, the selection and default member specifications tied to the dimension will be reset.  Continue?")) {
//								specificationList.removeAll();
//								//if( ! model.getDimension().equals(VERSION)) {
//								if(!newDimension.equals(VERSION)){
//									specificationList.setItems(new String[]{IDESC_UOW_ROOT});
//								}
//								formDataChanged();
//							}
//							else {
//								dimensionCombo.setText(origDimension);
//							}
//						//}
//					}
//					else {
//						if( ! model.getDimension().equals(VERSION)) {
//							specificationList.setItems(new String[]{IDESC_UOW_ROOT});
//						}
//						formDataChanged();
//					}
				}
			}
		});

		dimensionCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				formDataChanged();
			}
		});

		multipleCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				
				Boolean multiples = new Boolean(multipleCombo.getText());
				parentFirst.setEnabled(multiples);
				genRightClick.setEnabled(multiples);
				levelRightClick.setEnabled(multiples);
				
				formDataChanged();

			}

		});
		

		promptStringText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				formDataChanged();

			}

		});

		displayStringText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				
				formDataChanged();

			}

		});
		
		parentFirst.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				formDataChanged();
			}

		});
		
		genRightClick.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				formDataChanged();
			}

		});
		
		levelRightClick.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				formDataChanged();
			}

		});
		
	}

	protected void checkRequiredFields() {

		boolean enableAddButton = false;

		// get and trim key
		String key = idText.getText().trim().toUpperCase();

		// if key already in model map, disable add button
		if (!modelMap.containsKey(key)
				&& !dimensionCombo.getText().trim().equals("")) {
			enableAddButton = true;
		}

		addButton.setEnabled(enableAddButton);

	}

	/**
	 * Called when any the form data changes
	 * 
	 */
	private void formDataChanged() {

		// if caching is enabled, cach the form
		if (cachingEnabled) {
			cacheForm();
		}

	}

	/**
	 * Caches the form to a temp object model
	 * 
	 */
	private void cacheForm() {

		logger.debug("Caching form");

		// if current model object is null, create one
		if (currentModelObject == null) {

			currentModelObject = new PafUserSelection();

		}

		currentModelObject.setId(idText.getText().trim());

		if (!dimensionCombo.getText().trim().equals("")) {
			currentModelObject.setDimension(dimensionCombo.getText());
		}

		Boolean multiples = new Boolean(multipleCombo.getText());

		if (multiples != null) {
			currentModelObject.setMultiples(multiples.booleanValue());
		}

		if (!displayStringText.getText().trim().equals("")) {

			currentModelObject.setDisplayString(displayStringText.getText()
					.trim());

		}
		
		if (!promptStringText.getText().trim().equals("")) {

			currentModelObject.setPromptString(promptStringText.getText()
					.trim());

		}
		
		PafDimSpec pafDimSpec = new PafDimSpec();
		pafDimSpec.setDimension(dimension);
		pafDimSpec.setExpressionList(specificationList.getItems());

		currentModelObject.setSpecification(pafDimSpec);
		
		
		PafDimSpec pafDimSpec2 = new PafDimSpec();
		pafDimSpec2.setDimension(dimension);
		pafDimSpec2.setExpressionList(defaultMbrList.getItems());
		currentModelObject.setDefaultMembers(pafDimSpec2);
		
		currentModelObject.setParentLast(!parentFirst.getSelection());
		currentModelObject.setGenerationRightClick(genRightClick.getSelection());
		currentModelObject.setLevelRightClick(levelRightClick.getSelection());
		
	}

	/**
	 * Sets up the listeners for the buttons
	 * 
	 */
	private void setupButtonListeners() {

		// new button listeners
		setupButtonSelectionListener(newButton, ButtonId.New);

		// delete button listeners
		setupButtonSelectionListener(deleteButton, ButtonId.Delete);

		// add button listeners
		setupButtonSelectionListener(addButton, ButtonId.Add);

		// cancel button listeners
		setupButtonSelectionListener(cancelButton, ButtonId.Cancel);

	}

	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}

	/**
	 * Used to perfrom an action when a button is pressed
	 * 
	 * @param buttonId
	 *            enum for which button was selected
	 */
	private void buttonSelected(ButtonId buttonId) {

		switch (buttonId) {

		case Add:

			logger.debug(buttonId + " button clicked");
			addButtonAction();
			cachingEnabled = true;
			break;

		case Delete:

			logger.debug(buttonId + " button clicked");
			deleteButtonAction();

			break;

		case New:

			logger.debug(buttonId + " button clicked");
			newButtonAction();
			cachingEnabled = false;
			break;

		case Cancel:

			logger.debug(buttonId + " button clicked");
			cancelButtonAction();
			cachingEnabled = true;
			break;

		}

	}

	/**
	 * Action called when new button was pressed
	 */
	private void newButtonAction() {

		list.setEnabled(false);
		list.deselectAll();

		enableForm(true, true);

		clearForm();

		newButton.setEnabled(false);
		deleteButton.setEnabled(false);

		addButton.setVisible(true);
		addButton.setEnabled(false);

		cancelButton.setVisible(true);
		cancelButton.setEnabled(true);
		
		idText.setFocus();

	}

	/**
	 * Action called when delete button was pressed
	 */
	private void deleteButtonAction() {

		// if selection count is greater than 0, delete item
		if (list.getSelectionCount() > 0) {

			String key = list.getSelection()[0];

			if (modelMap.containsKey(key)) {

				modelMap.remove(key);
				list.removeAll();
				refreshList();
				initializeDialog();

			}

		} else {

			list.deselectAll();

		}

	}

	/**
	 * Clears the current form.
	 */
	private void clearForm() {

		cachingEnabled = false;

		idText.setText("");
		if( isNew ) {
			if( dimensionCombo.isEnabled() ) {
				dimensionCombo.setText(dimension);
				dimensionCombo.setEnabled(!isSingle);
			}
			if( multipleCombo.isEnabled() ) {
				multipleCombo.setText(isSingle==true?"False":"True");
				multipleCombo.setEnabled(!isSingle);
			}
		}
		else{
			dimensionCombo.select(0);
			multipleCombo.select(0);
		}
		displayStringText.setText("");
		promptStringText.setText("");
		parentFirst.setSelection(true);
		genRightClick.setSelection(false);
		levelRightClick.setSelection(false);
		specificationList.removeAll();
		defaultMbrList.removeAll();
		cachingEnabled = true;
	}

	/**
	 * Action called when cancel button was pressed
	 */
	private void cancelButtonAction() {

		list.setEnabled(true);

		enableForm(false, true);

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);

		addButton.setVisible(false);
		cancelButton.setVisible(false);

		initializeDialog();

	}

	/**
	 * Action called when add button was pressed
	 */
	private void addButtonAction() {

		PafUserSelection model = new PafUserSelection();

		String key = idText.getText().trim().toUpperCase();

		model.setId(key);

//		String dimension = null;

		if (!dimensionCombo.getText().equals("")) {
			dimension = dimensionCombo.getText();
			model.setDimension(dimension);
		}

		Boolean multiples = new Boolean(multipleCombo.getText());

		if (multiples != null) {
			model.setMultiples(multiples.booleanValue());

		}

		if (!displayStringText.getText().trim().equals("")) {

			model.setDisplayString(displayStringText.getText().trim());

		}
		
		if (!promptStringText.getText().trim().equals("")) {

			model.setPromptString(promptStringText.getText().trim());

		}
		
		if ( specificationList.getItemCount() > 0 ) {
			PafDimSpec pafDimSpec = new PafDimSpec();
			pafDimSpec.setDimension(dimension);
			pafDimSpec.setExpressionList(specificationList.getItems());

			model.setSpecification(pafDimSpec);
		}
		
		if( defaultMbrList != null && defaultMbrList.getItemCount() > 0 ) {
			PafDimSpec pafDimSpec = new PafDimSpec();
			pafDimSpec.setDimension(dimension);
			pafDimSpec.setExpressionList(defaultMbrList.getItems());

			model.setDefaultMembers(pafDimSpec);
		}
		
		model.setParentLast(!parentFirst.getSelection());
		model.setGenerationRightClick(genRightClick.getSelection());
		model.setLevelRightClick(levelRightClick.getSelection());

		modelMap.put(key, model);

		refreshList();

		list.setEnabled(true);
		list.select(getListItemIndex(key));
		populateForm(key);

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);
		addButton.setVisible(false);
		cancelButton.setVisible(false);

	}

	/**
	 * Removes all entries in the list and repopulates with the keyset from the
	 * model map
	 */
	private void refreshList() {

		list.removeAll();
		list.setItems(modelMap.keySet().toArray(new String[0]));

	}

	/**
	 * Enables the form fields
	 * 
	 * @param enable
	 *            enables/disables the form fields
	 * @param includePrimaryKey
	 *            include the primary key field when enabling/disabling form
	 */
	private void enableForm(boolean enable, boolean includePrimaryKey) {

		if (includePrimaryKey) {
			idText.setEnabled(enable);
		}

		dimensionCombo.setEnabled(enable);
		multipleCombo.setEnabled(enable);
		promptStringText.setEnabled(enable);
		displayStringText.setEnabled(enable);
		Boolean multiples = new Boolean(multipleCombo.getText());
		parentFirst.setEnabled(multiples);
		genRightClick.setEnabled(multiples);
		levelRightClick.setEnabled(multiples);
		specificationList.setEnabled(enable);
		defaultMbrList.setEnabled(enable);
		btnFilterSpec.setEnabled(enable);
	}

	/**
	 * Used to get the index of the key in the list
	 * 
	 * @returns int index of key in list
	 */
	private int getListItemIndex(String key) {

		int index = 0;

		for (String item : list.getItems()) {

			if (item.equals(key)) {
				break;
			}

			index++;
		}

		return index;
	}

	@Override
	protected void okPressed() {

		saveCachedModel();

		modelManager.setModel(modelMap);
		modelManager.save();

		super.okPressed();

	}

	/**
	 * Saves the cached model objects.
	 */
	private void saveCachedModel() {

		if (currentModelObject != null) {

			if (modelMap.containsKey(currentModelObject.getId())) {

				logger.debug("Updating model map for key "
						+ currentModelObject.getId());
				modelMap.put(currentModelObject.getId(), currentModelObject);
				currentModelObject = null;

			}

		}

	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public boolean isSingle() {
		return isSingle;
	}

	public void setSingle(boolean isSingle) {
		this.isSingle = isSingle;
	}

}

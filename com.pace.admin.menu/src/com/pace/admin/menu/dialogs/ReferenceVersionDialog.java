/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.VersionDefModelManager;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

public class ReferenceVersionDialog extends Dialog {
	private Tree tree;
	
	private PafSimpleDimTree versionTree;
	
	private Map<String, PafSimpleDimMember> memberMap;
	
	private ArrayList<String> refVersionList;
	
	private PafSimpleDimMember rootSimpleDimMember = null;
	
	private CheckboxTreeViewer checkboxTreeViewer;
	
	private Set<PafSimpleDimMember>	disabledVersionMembers = new HashSet<PafSimpleDimMember>();
	

	public ReferenceVersionDialog(Shell parentShell, IProject project, ArrayList<String> refVersionList) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		try {
			
			//get version dim name
//			String versionDimName =  DimensionUtil.getVersionDimensionName(project);
			
			//get version tree
			versionTree = DimensionTreeUtility.getDimensionTree(project, PafProjectUtil.getApplicationName(project), DimensionUtil.getVersionDimensionName(project));
			
			//conver tree into member map
			memberMap = DimensionTreeUtility.convertTreeIntoHashMap(versionTree);

			
			String rootMemberKey = versionTree.getRootKey();
					

			//if member map isn't null
			if ( memberMap != null ) {
			
				//if version dim name is in member map, set root member
				if ( memberMap.containsKey(rootMemberKey)) {
					rootSimpleDimMember = memberMap.get(rootMemberKey);
				}
				
				//all all members to the disabled version members
				disabledVersionMembers.addAll(memberMap.values());
				
				//get version model manager
				VersionDefModelManager versionModelManager = new VersionDefModelManager(project);
				
				//get versions listed in the version xml
				String[] validVersionKeys = versionModelManager.getKeys();
				
				//if not null
				if ( validVersionKeys != null ) {
					
					//loop over valid versions and remove them from the disabled list
					for (String validVersionKey : validVersionKeys ) {
						
						if ( memberMap.containsKey(validVersionKey) ) {
							disabledVersionMembers.remove(memberMap.get(validVersionKey));
						}
						
					}
					
				}
				
//				//create dynamic members model manager
//				DynamicMembersModelManager dynamicMembersModelManager = new DynamicMembersModelManager(project);
//				
//				//get version dynamic member
//				DynamicMemberDef versionDynamicMember = (DynamicMemberDef) dynamicMembersModelManager.getItem(versionDimName);
//				
//				String[] dynamicMembers = null;
//				
//				//if not null
//				if ( versionDynamicMember != null ) {
//					
//					//loop over member spects, create a paf simple dim member and then add to member map
//					for ( String memberSpec : versionDynamicMember.getMemberSpecs() ) {
//						
//						//TODO: FIX THIS ASAP { FIGURE IT OUT }
//						PafSimpleDimMember dynamicMemberObject = new PafSimpleDimMember();//(null, memberSpec, null, Constants.DYNAMIC_MEMBERS_FOLDER_NAME);
//						dynamicMemberObject.setKey(memberSpec);
//						dynamicMemberObject.setParentKey(Constants.DYNAMIC_MEMBERS_NODE_NAME);
//						
//						memberMap.put(memberSpec, dynamicMemberObject);
//																	
//					}
//					
//					//get member spec array
//					dynamicMembers = versionDynamicMember.getMemberSpecs();
//					
//				}
//				
//				//create dynamic members folder object
//				PafSimpleDimMember dynamicMembersObject = new PafSimpleDimMember();//dynamicMembers, Constants.DYNAMIC_MEMBERS_FOLDER_NAME, null, versionDimName);
//				dynamicMembersObject.setKey(Constants.DYNAMIC_MEMBERS_NODE_NAME);
//				dynamicMembersObject.setParentKey(versionDimName);
//				dynamicMembersObject.getChildKeys().addAll(Arrays.asList(dynamicMembers));
//				
//				
//				//add folder object to disabled version members so user can't select
//				disabledVersionMembers.add(dynamicMembersObject);
//				
//				//push into map
//				memberMap.put(Constants.DYNAMIC_MEMBERS_NODE_NAME, dynamicMembersObject);
				
				//get the root's child keys
				String[] rootChildKeys = rootSimpleDimMember.getChildKeys().toArray(new String[0]);
				
				//if keys is null, create new string array with only dynamic member folder
				if ( rootChildKeys == null || rootChildKeys[0] == null) {
					
					rootChildKeys = new String[] { Constants.DYNAMIC_MEMBERS_NODE_NAME };
					
				} else {
					
					//create a root child list form root node
					List<String> rootChildList = new ArrayList<String>(Arrays.asList(rootChildKeys));
					
					//add dynamic members folder to begining of list
					rootChildList.add(0, Constants.DYNAMIC_MEMBERS_NODE_NAME);
					
					//convert back to string array
					rootChildKeys = rootChildList.toArray(new String[0]);
					
				}
				
				//set root child keys
				rootSimpleDimMember.getChildKeys().clear();
				rootSimpleDimMember.getChildKeys().addAll(Arrays.asList(rootChildKeys));
				
				//replace root simple member in map
				memberMap.put(rootSimpleDimMember.getKey(), rootSimpleDimMember);

				
			}
									
		} catch (Exception e) {
			
			//throw runtime exc if exc is thrown. 
			throw new RuntimeException(e.getLocalizedMessage());
		}
		
		//set reference version list
		this.refVersionList = refVersionList;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 15;
		gridLayout.marginRight = 15;
		gridLayout.marginLeft = 15;
		container.setLayout(gridLayout);

		final Label selectVersionsToLabel = new Label(container, SWT.WRAP);
		selectVersionsToLabel.setLayoutData(new GridData(270, SWT.DEFAULT));
		selectVersionsToLabel.setText("Select version members to be used as the default evaluation Reference Version(s).  Invalid version selection(s) will result in solid filling of the checkbox and will not be saved.");

		checkboxTreeViewer = new CheckboxTreeViewer(container, SWT.BORDER);
		checkboxTreeViewer.setContentProvider(new TreeContentProvider());
		checkboxTreeViewer.setLabelProvider(new TreeLabelProvider());
		checkboxTreeViewer.setInput(versionTree);
		tree = checkboxTreeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		//if version list not null, set checked elements and then expand them
		if ( refVersionList != null ) {
			
			checkboxTreeViewer.setCheckedElements(getPafSimpleDimMembers(refVersionList.toArray(new String[0])));
			checkboxTreeViewer.setExpandedElements(getParentsOfPafSimpleDimMembers(refVersionList.toArray(new String[0])));
			
		}
		
		//if root member exists, expand root element if not already expanded
		if ( rootSimpleDimMember != null ) {
			
			if ( ! checkboxTreeViewer.getExpandedState(rootSimpleDimMember) ) {
				
				checkboxTreeViewer.setExpandedState(rootSimpleDimMember, true);
				
			}
		
			//set selection to root member
			checkboxTreeViewer.setSelection(new StructuredSelection(rootSimpleDimMember));
			
		}	
		
		//if diabled versions members exists, set the grayed elements
		if ( disabledVersionMembers.size() > 0 ) {
		
			checkboxTreeViewer.setGrayedElements(disabledVersionMembers.toArray(new PafSimpleDimMember[0]));
		
		}
				
		final Composite composite = new Composite(container, SWT.NONE);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.makeColumnsEqualWidth = true;
		gridLayout_1.numColumns = 2;
		composite.setLayout(gridLayout_1);

		final Button selectAllButton = new Button(composite, SWT.NONE);
		selectAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				//if member map is not null, check all elements in map
				if ( memberMap != null ) {
					checkboxTreeViewer.setCheckedElements(memberMap.values().toArray(new PafSimpleDimMember[0]));
				}
				
			}
		});
		selectAllButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		selectAllButton.setText("Check All");

		final Button deselectAllButton = new Button(composite, SWT.NONE);
		deselectAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				//uncheck all tree items
				checkboxTreeViewer.setAllChecked(false);
				
			}
		});
		deselectAllButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		deselectAllButton.setText("Uncheck All");


		return container;
	}

	/**
	 * 
	 *  A method called recursivly that will take a set of member names, and return 
	 *  the parents until the root member.
	 *
	 * @param strMemberNames A list of member names.
	 * @return Array of parent simple dim members.
	 */
	private PafSimpleDimMember[] getParentsOfPafSimpleDimMembers(String[] strMemberNames) {

		//get the paf simple dim members from member names
		PafSimpleDimMember[] pafSimpleDimMembers = getPafSimpleDimMembers(strMemberNames);
		
		//create new hash set
		Set<PafSimpleDimMember> pafSimpleDimMemberParentSet = new HashSet<PafSimpleDimMember>();
		
		//if simple dim members exists
		if (pafSimpleDimMembers != null && memberMap != null) {

			//loop over dim members
			for (PafSimpleDimMember pafSimpleDimMember : pafSimpleDimMembers) {

				//get parent key
				String parentKey = pafSimpleDimMember.getParentKey();

				//if member map contains key, get parent member then recursivly call this method
				if (memberMap.containsKey(parentKey)) {

					//get parent member
					PafSimpleDimMember parentMember = memberMap.get(parentKey);

					//add parent to parent set
					pafSimpleDimMemberParentSet.add(parentMember);

					//if the parent's parent key is valid
					if (parentMember.getParentKey() != null) {

						//call this mehtod
						PafSimpleDimMember[] parentMemberAr = getParentsOfPafSimpleDimMembers(new String[] { parentMember
								.getKey() });

						//if not null, add to list.
						if (parentMemberAr != null) {

							pafSimpleDimMemberParentSet.addAll(Arrays
									.asList(parentMemberAr));

						}

					}

				}

			}

		}			
		
		//convert to an array
		return pafSimpleDimMemberParentSet.toArray(new PafSimpleDimMember[0]);
	}

	/**
	 * 
	 *  Creates an array of paf simple dim members from member names
	 *
	 * @param strMemberNames Paf simple dim member names
	 * @return Array of paf simple dim members.
	 */
	private PafSimpleDimMember[] getPafSimpleDimMembers(String[] strMemberNames) {

		Set<PafSimpleDimMember> pafSimpleDimMemberSet = new HashSet<PafSimpleDimMember>(); 
		
		if ( strMemberNames != null && memberMap != null	 ) {
			
			for (String strMemberKey : strMemberNames ) {
				
				if ( memberMap.containsKey(strMemberKey) ) {
					
					pafSimpleDimMemberSet.add(memberMap.get(strMemberKey));
					
				}
				
			}			
			
		}
		
		return pafSimpleDimMemberSet.toArray(new PafSimpleDimMember[0]);
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(324, 502);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Reference Versions");
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
			
			//get checked elements
			Object[] checkedObjects = checkboxTreeViewer.getCheckedElements();
			
			//create list
			List<String> checkedRefVersionList = new ArrayList<String>();
			
			//for each checked item, if in map, add to list
			for (Object checkedObject : checkedObjects ) {
				
				if ( checkedObject instanceof PafSimpleDimMember) {
					
					PafSimpleDimMember simpleDimMember = (PafSimpleDimMember) checkedObject;
					
					//don't add to list if version is not in version xml
					if ( ! disabledVersionMembers.contains(simpleDimMember) ) {
					
						checkedRefVersionList.add(simpleDimMember.getKey());
					
					}
					
				}
				
			}
			
			//if reference version list is null, create new array list, else clear list
			if ( refVersionList == null ) {
				refVersionList = new ArrayList<String>();
			} else {
				refVersionList.clear();
			}
			
			//add all checked items to refVersion list
			refVersionList.addAll(checkedRefVersionList);
			
		}
		
		super.buttonPressed(buttonId);
	}

	/**
	 * @return the refVersionList
	 */
	public ArrayList<String> getRefVersionList() {
		return refVersionList;
	}
	/**
	 * 
	 * Content Provider for the version tree
	 *
	 * @version	x.xx
	 * @author jmilliron
	 *
	 */
	class TreeContentProvider implements IStructuredContentProvider, ITreeContentProvider {
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement) {
			
			return getChildren(inputElement);
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		public Object[] getChildren(Object parentElement) {
			
			//holds refs to child keys
			String[] childKeys = null;
			
			//if parenet element is a tree, return root of tree as child
			if ( parentElement instanceof PafSimpleDimTree) {
				
				PafSimpleDimTree tree = (PafSimpleDimTree) parentElement;
										
				childKeys = new String[] { tree.getRootKey() };
																
			//else if parent is type of simple dim member, return child keys
			} else if ( parentElement instanceof PafSimpleDimMember) {
				
				PafSimpleDimMember member = (PafSimpleDimMember) parentElement;
				
				if ( member.getChildKeys().size() > 0 ) {
				
					childKeys = member.getChildKeys().toArray(new String[0]);
					
				}
				
				
			}
			
			//create empty list
			List<PafSimpleDimMember> children = new ArrayList<PafSimpleDimMember>();
			
			//if child keys exists, loop through them and add the to the children map
			if ( childKeys != null ) {
				
				for (String childKey : childKeys) {
					
					if ( memberMap.containsKey(childKey)) {
						
						children.add(memberMap.get(childKey));
						
					}
					
				}
				
			}
			
			//convert to array and return
			return children.toArray(new PafSimpleDimMember[0]);
				
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		public Object getParent(Object element) {

			//if paf simple dim member
			if ( element instanceof PafSimpleDimMember) {
				
				//cast into simple dim member
				PafSimpleDimMember member = (PafSimpleDimMember) element;
				
				//get parent key
				String parentKey = member.getParentKey();
				
				//if member map contains the parent, return parent member
				if ( memberMap.containsKey(parentKey)) {
					return memberMap.get(parentKey);
				}
				
				
			}
			
			return null;
			
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		public boolean hasChildren(Object element) {
			
			//true of childen members exists
			return getChildren(element).length > 0;
		}
	}
	
	/**
	 * 
	 * Label Provider for Version Tree
	 *
	 * @version	x.xx
	 * @author jmilliron
	 *
	 */	 
	class TreeLabelProvider extends LabelProvider {
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		public String getText(Object element) {
			
			//if element is a paf simple dim member, cast and get key
			if ( element instanceof PafSimpleDimMember) {
				
				PafSimpleDimMember member = (PafSimpleDimMember) element;
				
				return member.getKey();
				
			//if string, cast and return
			} else if ( element instanceof String ) {
				
				return (String) element;
				
			}
			
			return null;
		}
		
		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		public Image getImage(Object element) {
			return null;
		}
	}
}

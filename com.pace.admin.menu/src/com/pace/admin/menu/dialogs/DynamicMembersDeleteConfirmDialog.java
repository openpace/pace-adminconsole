/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.util.EditorControlUtil;
import com.swtdesigner.SWTResourceManager;

/**
 * 
 * Prompts users to delete the dynamic members and displays a list of 
 * view section and role configuraiton references.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DynamicMembersDeleteConfirmDialog extends Dialog {

	private List roleConfigsList;
	private List dynamicMembersList;
	private List viewSectionList;
	
	private String[] dynamicMembers; 
	private String[] viewSectionNames; 
	private String[] roleConfigNames;
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public DynamicMembersDeleteConfirmDialog(Shell parentShell, String[] dynamicMembers, String[] viewSectionNames, String[] roleConfigNames) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.dynamicMembers = dynamicMembers;
		this.viewSectionNames = viewSectionNames;
		this.roleConfigNames = roleConfigNames;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginTop = 20;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);

		final Label deleteSelectedDynamicLabel = new Label(container, SWT.NONE);
		deleteSelectedDynamicLabel.setText("Delete the listed dynamic members?*");

		dynamicMembersList = new List(container, SWT.V_SCROLL | SWT.BORDER);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.minimumHeight = 75;
		dynamicMembersList.setLayoutData(gridData);

		EditorControlUtil.addItemsToList(dynamicMembersList, dynamicMembers);
		
		final Label label = new Label(container, SWT.NONE);

		final Label theFollowingViewLabel = new Label(container, SWT.NONE);
		theFollowingViewLabel.setText("The following View Sections contain a reference to one or more of these members:");

		viewSectionList = new List(container, SWT.V_SCROLL | SWT.BORDER);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_1.minimumHeight = 75;
		viewSectionList.setLayoutData(gridData_1);

		EditorControlUtil.addItemsToList(viewSectionList, viewSectionNames);
		
		final Label label_1 = new Label(container, SWT.NONE);

		final Label theFollowingRoleLabel = new Label(container, SWT.NONE);
		theFollowingRoleLabel.setText("The following Role Configurations contain a reference to one or more of these members:");

		roleConfigsList = new List(container, SWT.V_SCROLL | SWT.BORDER);
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_2.minimumHeight = 75;
		roleConfigsList.setLayoutData(gridData_2);

		EditorControlUtil.addItemsToList(roleConfigsList, roleConfigNames);

		final Label noteTheDynamicLabel = new Label(container, SWT.NONE);
		noteTheDynamicLabel.setForeground(SWTResourceManager.getColor(255, 0, 0));
		noteTheDynamicLabel.setText("*Note: The dynamic member references do not get deleted, just the dynamic member entries.");
		//
		return container;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(598, 491);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Confirm?");
	}

}

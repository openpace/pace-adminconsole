/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.exceptions.InvalidDimensionsException;
import com.pace.admin.global.exceptions.InvalidStateException;
import com.pace.admin.global.model.PaceTree;
import com.pace.admin.global.model.PaceTreeInput;
import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PaceViewTupleTree;
import com.pace.admin.global.model.PageTupleList;
import com.pace.admin.global.model.managers.GlobalStyleModelManager;
import com.pace.admin.global.model.managers.NumericFormatModelManager;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafException;
import com.pace.base.SortPriority;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.data.Intersection;
import com.pace.base.mdb.SortingTuple;
import com.pace.base.mdb.SortingTuples;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimTree;


/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ViewSectionTuplesDialogInput {
	
	private static final Logger logger = Logger.getLogger(ViewSectionTuplesDialogInput.class);
	
	private IProject project; 
	
	private PafViewSectionUI pafViewSectionUI;
	
	private PafApplicationDef pafAppDef;
	
	private List<String> aliasTables;
	
	private List<String> attributeDimensions;
	
	private List<String> pageDimensions;
	
	private List<String> colDimensions;
	
	private List<String> rowDimensions;
	
	private PaceTree pageAxisTree;
	
	private PaceTree columnAxisTree;
	
	private PaceTree rowAxisTree;
	
	private PageTupleList pageAxisTupleList;
	
	private PaceViewTupleTree columnAxisTupleTree;
	
	private PaceViewTupleTree rowAxisTupleTree;
	
	private boolean isRowPaneExists;
	
	private boolean isColumnPaneExists;

	private List<PaceTreeNode> rowFreezePaneList;
	
	private List<PaceTreeNode> columnFreezePaneList ;
	
	private int freezePaneNodeNo = 0 ;
	public ViewSectionTuplesDialogInput(IProject project, PafViewSectionUI pafViewSectionUI) throws Exception {
		
		//TODO: validate parms
		
		this.project = project;
		this.pafViewSectionUI = pafViewSectionUI;
		this.pafAppDef = PafApplicationUtil.getPafApp(project);
		
		if ( pafViewSectionUI != null ) {
			
			try {
			
				PafMdbProps mdbProps = DimensionTreeUtility.getMdbProps(PafProjectUtil.getProjectServer(project), PafProjectUtil.getApplicationName(project));
				
				aliasTables = new ArrayList(mdbProps.getAliasTables());
				
				attributeDimensions = new ArrayList(mdbProps.getCachedAttributeDims());
				
				//TODO: verify all dimensions are valid
				
				HashMap<String, PafSimpleDimTree> pafDimTreeMap = new LinkedHashMap<String, PafSimpleDimTree>();
					
				List<String> invalidDimList = new ArrayList<String>();
				
				for ( String dimName : pafViewSectionUI.getDimensionSet() ) {
						
					try {
					
						PafSimpleDimTree cachedTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(project, PafProjectUtil.getApplicationName(project), dimName);
						
						pafDimTreeMap.put(dimName, cachedTree);
						
					} catch ( Exception ex )	 {
																		
						logger.error("Dimension " + dimName + " is no longer valid.");
						
						invalidDimList.add(dimName);
						
					}
									
				}		
				
				if ( invalidDimList.size() > 0 ) {
					
					throw new InvalidDimensionsException(invalidDimList);
					
				}
				
				//TODO: Refactor next three sections into one
				if ( pafViewSectionUI.getPageAxisDims() != null ) {
					
					pageDimensions = Arrays.asList(pafViewSectionUI.getPageAxisDims());
										
					PaceTreeInput input = new PaceTreeInput(project, pafViewSectionUI, aliasTables, PaceTreeAxis.Page, pageDimensions, new ArrayList<String>(this.pafViewSectionUI.getDimensionSet()), attributeDimensions, pafDimTreeMap, true);
					
					pageAxisTree = new PaceTree(input);
					
					List<PageTuple> pageTupleList = new ArrayList<PageTuple>();
					
					if ( pafViewSectionUI.getPageTuples() != null ) {
						
						pageTupleList.addAll(Arrays.asList(pafViewSectionUI.getPageTuples().clone()));
						
					}
					
					//fix for TTN-1333
/*					List<PageTuple> remove = new ArrayList<PageTuple>();
					if(pageTupleList != null && pageTupleList.size() > 0){
						for(PageTuple tuple : pageTupleList){
							if(tuple.getMember() == null){
								remove.add(tuple);
							}
						}
						if(remove.size() > 0){
							pageTupleList.removeAll(remove);
						}
					}
	*/				
					//if page tuples exist, create the list from existing tuples, if not, then just init tuple list
					if ( pageTupleList.size() > 0 ) {
						
						pageAxisTupleList = new PageTupleList(pageTupleList, pafDimTreeMap);
					
					} else {
						
						pageAxisTupleList = new PageTupleList(pageDimensions);
						
					}
					
				} 
				
				if ( pafViewSectionUI.getColAxisDims() != null ) {
					
					colDimensions = Arrays.asList(pafViewSectionUI.getColAxisDims());
					
					PaceTreeInput input = new PaceTreeInput(project, pafViewSectionUI, aliasTables, PaceTreeAxis.Column, colDimensions, new ArrayList<String>(this.pafViewSectionUI.getDimensionSet()), attributeDimensions, pafDimTreeMap, true);
					
					columnAxisTree = new PaceTree(input);
					
					input.setBuildDimensionTrees(false);
					
					columnAxisTupleTree = new PaceViewTupleTree(input, pafViewSectionUI.getColTuples());

					//TTN 609 - Presort Ranking View
					loadSortingTuples();

					// TTN-2513 Add FreezePanes
				    loadFreezePaneTuples();
				
				} 
	
				if ( pafViewSectionUI.getRowAxisDims() != null ) {
					
					rowDimensions = Arrays.asList(pafViewSectionUI.getRowAxisDims());
					
					PaceTreeInput input = new PaceTreeInput(project, pafViewSectionUI, aliasTables, PaceTreeAxis.Row, rowDimensions, new ArrayList<String>(this.pafViewSectionUI.getDimensionSet()), attributeDimensions, pafDimTreeMap, true);
					
					rowAxisTree = new PaceTree(input);
					
					input.setBuildDimensionTrees(false);
					
					rowAxisTupleTree = new PaceViewTupleTree(input, pafViewSectionUI.getRowTuples());
					
				    // TTN-2513 Add FreezePanes
					loadFreezePaneTuples();
					
				} 
			}catch (PafException pEx) {
				
				logger.error(pEx.getMessage());
				throw pEx;
				
			}
		}
		
	}

	/**
	 * @return the pafAppDef
	 */
	public PafApplicationDef getPafAppDef() {
		return pafAppDef;
	}

	/**
	 * @return the pageDimensions
	 */
	public List<String> getPageDimensions() {
		return pageDimensions;
	}

	/**
	 * @return the colDimensions
	 */
	public List<String> getColDimensions() {
		return colDimensions;
	}

	/**
	 * @return the rowDimensions
	 */
	public List<String> getRowDimensions() {
		return rowDimensions;
	}

	/**
	 * @return the project
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @return the pageAxisTree
	 */
	public PaceTree getPageAxisTree() {
		return pageAxisTree;
	}

	/**
	 * @return the columnAxisTree
	 */
	public PaceTree getColumnAxisTree() {
		return columnAxisTree;
	}

	/**
	 * @return the rowAxisTree
	 */
	public PaceTree getRowAxisTree() {
		return rowAxisTree;
	}
	
	public List<String> getGlobalSytleNames() {
				
		List<String> globalStyleNames = new ArrayList<String>(); 
		
		GlobalStyleModelManager modelManager = new GlobalStyleModelManager(project);

		if ( modelManager.size() > 0 ) {
			
			//TODO: ensure this is in alphabetical order
			globalStyleNames.addAll(Arrays.asList(modelManager.getKeys()));
			
		} 
		
		//add blank entry as 1st item
		globalStyleNames.add(0, "");
		
		return globalStyleNames;
		
	}
	
	public List<String> getNumericFormatNames() {
						
		List<String> numericFormatNames = new ArrayList<String>(); 
		
		 NumericFormatModelManager modelManager = new NumericFormatModelManager(project);

		if ( modelManager.size() > 0 ) {
			
			//TODO: ensure this is in alphabetical order
			numericFormatNames.addAll(Arrays.asList(modelManager.getKeys()));
			
		} 
		
		//add blank entry as 1st item
		numericFormatNames.add(0, "");
		
		return numericFormatNames;

	}

	

	/**
	 * @return the pageAxisTupleList
	 */
	public PageTupleList getPageAxisTupleList() {
		return pageAxisTupleList;
	}

	/**
	 * @param pageAxisTupleList the pageAxisTupleList to set
	 */
	public void setPageAxisTupleList(PageTupleList pageAxisTupleList) {
		this.pageAxisTupleList = pageAxisTupleList;
	}

	/**
	 * @return the columnAxisTupleTree
	 */
	public PaceViewTupleTree getColumnAxisTupleTree() {
		return columnAxisTupleTree;
	}

	/**
	 * @param columnAxisTupleTree the columnAxisTupleTree to set
	 */
	public void setColumnAxisTupleTree(PaceViewTupleTree columnAxisTupleTree) {
		this.columnAxisTupleTree = columnAxisTupleTree;
	}

	/**
	 * @return the rowAxisTupleTree
	 */
	public PaceViewTupleTree getRowAxisTupleTree() {
		return rowAxisTupleTree;
	}

	/**
	 * @param rowAxisTupleTree the rowAxisTupleTree to set
	 */
	public void setRowAxisTupleTree(PaceViewTupleTree rowAxisTupleTree) {
		this.rowAxisTupleTree = rowAxisTupleTree;
	}
	
	public void validateTuples() throws InvalidStateException {
		
	}

	/**
	 * @return the aliasTables
	 */
	public List<String> getAliasTables() {
		return aliasTables;
	}

	/**
	 * @return the attributeDimensions
	 */
	public List<String> getAttributeDimensions() {
		return attributeDimensions;
	}

	public String getViewSectionName() {
		
		String viewSectionName = null;
		
		if ( pafViewSectionUI != null ) {
			viewSectionName = pafViewSectionUI.getName();
		}
		
		return viewSectionName;
	}

	public PafViewSectionUI getPafViewSectionUI() {
		return pafViewSectionUI;
	}
	
	//load sorting information from ViewSection
	public void loadSortingTuples() {
		SortingTuples sortingTuples = pafViewSectionUI.getSortingTuples();
		if ( sortingTuples != null ) {
			SortingTuple primSortTuple = sortingTuples.getPrimarySortingTuple();
			if( primSortTuple != null ) {
				loadSortingTupe(columnAxisTupleTree.getRootNode(), primSortTuple, SortPriority.Primary );
			}
			SortingTuple secSortTuple = sortingTuples.getSecondarySortingTuple();
			if( secSortTuple != null ) {
				loadSortingTupe(columnAxisTupleTree.getRootNode(), secSortTuple, SortPriority.Secondary);
			}
			SortingTuple tertSortTuple = sortingTuples.getTertiarySortingTuple();
			if( tertSortTuple != null ) {
				loadSortingTupe(columnAxisTupleTree.getRootNode(), tertSortTuple, SortPriority.Tertiary);
			}
		}
	}
	
	private void loadSortingTupe(PaceTreeNode parent, SortingTuple sortingTuple, SortPriority sortPriority) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.isViewTuple() && node.isNodeTypeValidForSorting() && node.isSelectionTypeValidForSorting() ) {
				if(  Arrays.equals( getColDimensions().toArray(), sortingTuple.getIntersection().getDimensions())  &&
						Arrays.equals(node.getViewTuple().getMemberDefs(), sortingTuple.getIntersection().getCoordinates()) ){
					node.getProperties().setSortPriority(sortPriority);
					node.getProperties().setSortOrder(sortingTuple.getSortOrder());
					setSortingForDuplicatedTuples(parent, node,true);
					return;
				}
			}
			loadSortingTupe(node, sortingTuple, sortPriority);
		}
	}

	public boolean validateSortingTuples() {
		if( ( findSortTuple(columnAxisTupleTree.getRootNode(), SortPriority.Tertiary) != null 
			&& ( findSortTuple(columnAxisTupleTree.getRootNode(), SortPriority.Secondary) == null
			|| findSortTuple(columnAxisTupleTree.getRootNode(), SortPriority.Primary) == null ) ) 
			||
			( findSortTuple(columnAxisTupleTree.getRootNode(), SortPriority.Secondary ) != null
			&& findSortTuple(columnAxisTupleTree.getRootNode(),SortPriority.Primary) == null ) ) {
		
			return false;
		}
		return true;
	}
	
	
	public void saveSortingTuples() {
		PaceTreeNode node = null;
		Intersection intersection = null;
		SortingTuple sortingTuple = null;
		SortingTuples sortingTuples = new SortingTuples();
		java.util.List<SortingTuple> sortingTupleList = new ArrayList<SortingTuple>();
		if( (node = findSortTuple(columnAxisTupleTree.getRootNode(),SortPriority.Primary)) != null )
		{
			intersection = new Intersection();
			intersection.setDimensions(getColDimensions().toArray(new String[0]));
			intersection.setCoordinates(node.getViewTuple().getMemberDefs());
			sortingTuple = new SortingTuple();
			sortingTuple.setIntersection(intersection);
			sortingTuple.setSortOrder(node.getProperties().getSortOrder() );
			sortingTupleList.add(sortingTuple);
		}
		if( (node = findSortTuple(columnAxisTupleTree.getRootNode(),SortPriority.Secondary)) != null )
		{
			intersection = new Intersection();
			intersection.setDimensions(getColDimensions().toArray(new String[0]));
			intersection.setCoordinates(node.getViewTuple().getMemberDefs());
			sortingTuple = new SortingTuple();
			sortingTuple.setIntersection(intersection);
			sortingTuple.setSortOrder(node.getProperties().getSortOrder() );
			sortingTupleList.add(sortingTuple);
		}
		if( (node = findSortTuple(columnAxisTupleTree.getRootNode(),SortPriority.Tertiary)) != null )
		{
			intersection = new Intersection();
			intersection.setDimensions(getColDimensions().toArray(new String[0]));
			intersection.setCoordinates(node.getViewTuple().getMemberDefs());
			sortingTuple = new SortingTuple();
			sortingTuple.setIntersection(intersection);
			sortingTuple.setSortOrder(node.getProperties().getSortOrder() );
			sortingTupleList.add(sortingTuple);
		}
		
		if(sortingTupleList.size() > 0 ) {
			sortingTuples.setSortingTupleList(sortingTupleList);
			getPafViewSectionUI().setSortingTuples(sortingTuples);
		}
		else {
			getPafViewSectionUI().setSortingTuples(null);
		}
	}
	
	public PaceTreeNode findSortTuple(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		PaceTreeNode retNode = null;
		for (PaceTreeNode node : nodes ) {
			if( node.getProperties().getSortOrder() != null && node.getProperties().getSortPriority() != null ){
				return node;
			}
			if( ( retNode = findSortTuple(node)) != null )
				return retNode;
		}
		return null;
	}
	
	//only need to find 1 sort node
	public PaceTreeNode findSortTuple(PaceTreeNode parent, SortPriority sortPriority ) {
		List<PaceTreeNode> nodes = parent.getChildren();
		PaceTreeNode retNode = null;
		for (PaceTreeNode node : nodes ) {
			if( node.getProperties().getSortPriority() != null && node.getProperties().getSortPriority().equals(sortPriority) ){
				return node;
			}
			if( ( retNode = findSortTuple(node, sortPriority)) != null )
				return retNode;
		}
		return null;
	}

	public PaceTreeNode findSortTuple(PaceTreeNode parent, PaceTreeNode theNode ) {
		List<PaceTreeNode> nodes = parent.getChildren();
		PaceTreeNode retNode = null;
		for (PaceTreeNode node : nodes ) {
			if( node != theNode && theNode != null
					&& node.getName().equals(theNode.getName()) 
					&& node.getParentNode().getName().equals(theNode.getParentNode().getName())
					&& node.getProperties().getSortPriority() != null 
					&& node.getProperties().getSortOrder() != null ){
				return node;
			}
			if( ( retNode = findSortTuple(node, theNode)) != null )
				return retNode;
		}
		return null;
	}

	public PaceTreeNode findSortableTuple(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		PaceTreeNode retNode = null;
		for (PaceTreeNode node : nodes ) {
			if( node.isViewTuple() 
					&& node.isNodeTypeValidForSorting() 
					&& node.isSelectionTypeValidForSorting()){
				return node;
			}
			if( ( retNode = findSortableTuple(node)) != null )
				return retNode;
		}
		return null;
	}

	public void setSortingForDuplicatedTuples(PaceTreeNode parent, PaceTreeNode theNode, boolean setFlag) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.isViewTuple() 
					&& node.isNodeTypeValidForSorting() 
					&& node.isSelectionTypeValidForSorting()
					&& node != theNode && theNode != null
					&& node.getName().equals(theNode.getName())
					&& node.getParentNode().getName().equals(theNode.getParentNode().getName())
					&& node.getProperties().getSelectionType().equals(theNode.getProperties().getSelectionType())
					){
				if( setFlag ) {
					((PaceTreeNode)node).getProperties().setSortPriority(theNode.getProperties().getSortPriority());
					((PaceTreeNode)node).getProperties().setSortOrder(theNode.getProperties().getSortOrder());
				}
				else {
					((PaceTreeNode)node).getProperties().setSortPriority(null);
					((PaceTreeNode)node).getProperties().setSortOrder(null);
				}
			}
			setSortingForDuplicatedTuples(node, theNode, setFlag);
		}
	}
	
	public void removeTupleSorts(PaceTreeNode theNode, Boolean cascade) {
		if( theNode != null ) {
			if( cascade ) { //remove all subsequent sorts
				removeCascadeTupleSorts(columnAxisTupleTree.getRootNode(),theNode.getProperties().getSortPriority());
			}
			else { //remove sort on the node
				//unset the property
				((PaceTreeNode)theNode).getProperties().setSortPriority(null);
				((PaceTreeNode)theNode).getProperties().setSortOrder(null);
				setSortingForDuplicatedTuples(columnAxisTupleTree.getRootNode(),theNode,false);
			}
		}
		else {
			removeTupleSorts(columnAxisTupleTree.getRootNode());
		}
	}

	public void removeTupleSorts(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.getProperties().getSortPriority() != null && node.getProperties().getSortOrder() != null ){
				node.getProperties().setSortOrder(null);
				node.getProperties().setSortPriority(null);
			}
			removeTupleSorts(node);
		}
	}
	
	public void removeCascadeTupleSorts(PaceTreeNode parent, SortPriority sortPriority) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.getProperties().getSortPriority() != null && node.getProperties().getSortOrder() != null 
					&& node.getProperties().getSortPriority().getIndex() >= sortPriority.getIndex()){
				node.getProperties().setSortOrder(null);
				node.getProperties().setSortPriority(null);
			}
			removeCascadeTupleSorts(node, sortPriority);
		}
	}

	// TTN-2513 Remove freeze panes on all nodes from the parent onwards
	public void removeCascadeFreezePanes(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.getProperties().isFreezePaneSet() ){
				node.getProperties().setFreezePaneSet(false);
				node.getViewTuple().setFreezePaneSet(false);
			}
			removeCascadeFreezePanes(node);
		}
	}

//	public boolean findSortTuple(PaceTreeNode parent, SortPriority sortPriority) {
//		List<PaceTreeNode> nodes = parent.getChildren();
//		boolean found = false;
//		for (PaceTreeNode node : nodes ) {
//			if( ! node.isSelectionType(PaceTreeNodeSelectionType.Members) && node.isViewTuple() 
//					&& node.getProperties().getSortPriority() != null && node.getProperties().getSortPriority().equals(sortPriority) ){
//				return true;
//			}
//			found = findSortTuple(node, sortPriority);
//		}
//		return found;
//	}
	
//	public boolean findSortTuple(PaceTreeNode parent, SortPriority sortPriority ) {
//		List<PaceTreeNode> nodes = parent.getChildren();
//		for (PaceTreeNode node : nodes ) {
//			System.out.println("Node Name: " + node.getName());
//			if( node.getProperties().getSortPriority() != null && node.getProperties().getSortPriority().equals(sortPriority) )
//				return true;
//			if( findSortTuple(node, sortPriority) == true )
//				return true;
//		}
//		return false;
//	}
	
	// TTN-2513 Method to find if the freeze pane has been set on any view tuple from the root node onwards , recursive .
	public PaceTreeNode findFreezePaneTuple(PaceTreeNode parent) {
		boolean tupleFound = false;
		PaceTreeNode nodeToReturn = null ;
		if(parent.hasChildren()){
		List<PaceTreeNode> nodes = parent.getChildren();
		
		// check the nodes to see if freeze pane found. 
		for (PaceTreeNode node : nodes ) {
			if(!node.hasChildren())
				freezePaneNodeNo ++ ;
			// code to check for single level nodes.
			if( node.getProperties().isFreezePaneSet()){
				return node;
			}
			// If node has children,check them to see if freeze panes was set on them 
			if(node.hasChildren()){  
				List<PaceTreeNode> children = node.getChildren();
				for( PaceTreeNode child: children){
					if(!child.hasChildren())
						freezePaneNodeNo ++ ;
					
					if(!tupleFound)
					nodeToReturn = findFreezePaneTuple(child);
					if(nodeToReturn !=null )
						return nodeToReturn;
				}
			}
			
		}
		}
		else {
			if(parent.getProperties().isFreezePaneSet()){
				tupleFound = true;
				return parent;
			}
		}
		
		return null;
	}
	

	// TTN-2513 Method to remove freeze pane from a node , recursive.
	public void removeTupleFreezePane(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		if(parent!=null){
			if( parent.getProperties().isFreezePaneSet() ){
				parent.getViewTuple().setFreezePaneSet(false);
				parent.getProperties().setFreezePaneSet(false);
				
			}
			//removeTupleFreezePane(node);
		}
	}
	
	// TTN-2513 Method to save the freeze pane tuples to the view section
/*	public void saveFreezePaneTuples() {
		PaceTreeNode node = null;
		Intersection intersection = null;
		FreezePaneTuple freezePaneTuple = null;
		FreezePaneTuples freezePaneTuples = new FreezePaneTuples();
		java.util.List<FreezePaneTuple> freezePaneTupleList = new ArrayList<FreezePaneTuple>();
		
		// fetch the existing freeze panes from the view section 
		FreezePaneTuples freezePaneOldTuples = getPafViewSectionUI().getFreezeTuples();
		if ( freezePaneOldTuples != null){
			for (FreezePaneTuple paneTuple :freezePaneOldTuples.getFreezePaneTupleList()){
			freezePaneTupleList.add(paneTuple);
			}
			
		}
		if ( (node = findFreezePaneTuple(columnAxisTupleTree.getRootNode())) != null )
		{
			intersection = new Intersection();
			List<Intersection> intersections = new ArrayList<Intersection>();
			
			intersection.setDimensions(getColDimensions().toArray(new String[0]));
			// if the topmost node of the tuple if selected, set the freeze pane on the last member of the tuple.
			if(node.getViewTuple() == null)
				intersection.setCoordinates(node.getChildNodeOfIndex(node.getChildren().size()-1).getViewTuple().getMemberDefs());
			else
				intersection.setCoordinates(node.getViewTuple().getMemberDefs());
			freezePaneTuple = new FreezePaneTuple();
			
			intersections.add(intersection);
			SimpleCoordList list = IntersectionUtil.convertIntersectionsToSimpleCoordList(intersections);
			freezePaneTuple.setCoordList(list);;
			freezePaneTuple.setAxis(Axis.Column);
		
			if ( freezePaneOldTuples != null && freezePaneOldTuples.getFreezePaneTupleList().size()>0) {
				for( FreezePaneTuple tuple : freezePaneOldTuples.getFreezePaneTupleList() ){
					if(tuple.getAxis().equals(Axis.Column)){
						freezePaneTupleList.remove(tuple);
						freezePaneTupleList.add(freezePaneTuple);
					}
					else if (freezePaneOldTuples.getFreezePaneTupleList().size() == 1){
						freezePaneTupleList.add(freezePaneTuple);
					}
				}
			}
			else
				freezePaneTupleList.add(freezePaneTuple);
		}
		if (  (node = findFreezePaneTuple(rowAxisTupleTree.getRootNode())) != null )
		{
			intersection = new Intersection();
			List<Intersection> intersections = new ArrayList<Intersection>();
			intersection.setDimensions(getRowDimensions().toArray(new String[0]));
		
			// if the topmost node of the tuple if selected, set the freeze pane on the last member of the tuple.
			if(node.getViewTuple() == null)
				intersection.setCoordinates(node.getChildNodeOfIndex(node.getChildren().size()-1).getViewTuple().getMemberDefs());
			else
				intersection.setCoordinates(node.getViewTuple().getMemberDefs());
			freezePaneTuple = new FreezePaneTuple();
			intersections.add(intersection);
			SimpleCoordList list = IntersectionUtil.convertIntersectionsToSimpleCoordList(intersections);
			freezePaneTuple.setCoordList(list);;
			freezePaneTuple.setAxis(Axis.Row);
			
			if ( freezePaneOldTuples != null && freezePaneOldTuples.getFreezePaneTupleList().size()>0) {
				for( FreezePaneTuple tuple : freezePaneOldTuples.getFreezePaneTupleList() ){
					if(tuple.getAxis().equals(Axis.Row)){
						freezePaneTupleList.remove(tuple);
						freezePaneTupleList.add(freezePaneTuple);
					}else if (freezePaneOldTuples.getFreezePaneTupleList().size() == 1){
						freezePaneTupleList.add(freezePaneTuple);
					}
				}// for closes
			}// if closes
			else{   
					freezePaneTupleList.add(freezePaneTuple);
				
			}
				
		}
		
		if(freezePaneTupleList.size() > 0 ) {
			freezePaneTuples.setFreezePaneTupleList(freezePaneTupleList);
			getPafViewSectionUI().setFreezePaneTuples(freezePaneTuples);
		}
		else {
			getPafViewSectionUI().setFreezePaneTuples(null);
		}
	}
	*/
	// TTN-2513 Method to load the freeze panes saved in the view section
	public void loadFreezePaneTuples() {
		
		rowFreezePaneList = new ArrayList<PaceTreeNode>();
		columnFreezePaneList = new ArrayList<PaceTreeNode>();
		if(columnAxisTupleTree!=null)
					loadFreezePaneTuple(columnAxisTupleTree.getRootNode());
		if(rowAxisTupleTree!=null)
					loadFreezePaneTuple(rowAxisTupleTree.getRootNode());
		
		
	}
	
	// TTN-2513 Method to load the freeze panes saved in the view section
	private void loadFreezePaneTuple(PaceTreeNode parent) {
		List<PaceTreeNode> nodes = parent.getChildren();
		for (PaceTreeNode node : nodes ) {
			if( node.isViewTuple() && node.getViewTuple().getAxis()==1) {
				if(  node.getViewTuple().isFreezePaneSet() ){
					
					node.getProperties().setFreezePaneSet(true);
					// Add the node to the list so the remove action can be enabled on those nodes
					setColumnFreezePaneExists(true);
					columnFreezePaneList.add(node);
					return;
				}
			}
			if( node.isViewTuple() && node.getViewTuple().getAxis()==0) {
				if(  node.getViewTuple().isFreezePaneSet() ){
					node.getProperties().setFreezePaneSet(true);
					// Add the node to the list so the remove action can be enabled on those nodes
					setRowFreezePaneExists(true);
					rowFreezePaneList.add(node);
					return;
				}
			}
			loadFreezePaneTuple(node);
		}
	}


	public boolean isRowFreezePaneExists(){
		return isRowPaneExists ;
	}

	public void setRowFreezePaneExists(boolean rowPane){
		this.isRowPaneExists = rowPane;
	}
	
	public boolean isColumnFreezePaneExists(){
		return isColumnPaneExists ;
	}

	public void setColumnFreezePaneExists(boolean columnPane){
		this.isColumnPaneExists = columnPane;
	}
	
	public List<PaceTreeNode> getRowFreezePaneList() {
		return rowFreezePaneList;
	}

	public void setRowFreezePaneList(List<PaceTreeNode> rowFreezePaneList) {
		this.rowFreezePaneList = rowFreezePaneList;
	}

	public List<PaceTreeNode> getColumnFreezePaneList() {
		return columnFreezePaneList;
	}

	public void setColumnFreezePaneList(List<PaceTreeNode> columnFreezePaneList) {
		this.columnFreezePaneList = columnFreezePaneList;
	}

	
	
	// TTN-2513 Method to save the freeze pane property to the view tuples in the view section
		public void saveFreezePaneViewTuples() {
			PaceTreeNode node = null;
			// variable to keep track of the tuple number
			int i=0;
			// code block to iterate through the node where the freeze pane is set , and apply it to the innermost member within the node.
			freezePaneNodeNo = 0;
			if ((node = findFreezePaneTuple(columnAxisTupleTree.getRootNode())) != null )
			{
				
				ViewTuple[] colTuples = getPafViewSectionUI().getColTuples();
				// Get column tuples from the tree so that new tuples from view modification will be pulled as well.
				try {
					colTuples =  columnAxisTupleTree.getViewTuples().toArray(new ViewTuple[0]);
				} catch (InvalidStateException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ViewTuple[] updatedColTuples = new ViewTuple[colTuples.length];
				
				if (colTuples!=null && colTuples.length>0){
					for (ViewTuple colTuple: colTuples){
						colTuple.setFreezePaneSet(false);
						if (node.getViewTuple()!=null){
							if (node.getViewTuple().getMemberDefs().length == colTuple.getMemberDefs().length){
								String[] nodeDef = node.getViewTuple().getMemberDefs();
								String[] colDef = colTuple.getMemberDefs();

								if (Arrays.equals(nodeDef, colDef) ){
									 // Set freeze pane to true after checking both member defs and the order of the tuple.
									if (freezePaneNodeNo -1 == i){
										colTuple.setFreezePaneSet(true);
									}
									 // Comment so that new tuples can be saved with freeze panes
								    // pafViewSectionUI.getColTuples()[i].setFreezePaneSet(true);
								}
							}
						}
						else if (node.hasChildren()){ // If node had children, get set the freeze pane on the last innermost member
							ViewTuple nodeViewTuple = getLastNodeViewTuple(node);
							if(node.getProperties().isFreezePaneSet() && Arrays.equals(nodeViewTuple.getMemberDefs(),colTuple.getMemberDefs())){
								colTuple.setFreezePaneSet(true);
							}
						}
						updatedColTuples[i] = colTuple;
						i++;
						
					}
					
					pafViewSectionUI.setColTuples(updatedColTuples);
					try {
						columnAxisTupleTree = new PaceViewTupleTree(columnAxisTree.getInput(), pafViewSectionUI.getColTuples());
					} catch (PafException e) {
						logger.warn(e.getMessage());
					}
				}
				
				
			} // code block to handle the case where no freeze panes are set on any tuples within the column axis.
			else if ((node = findFreezePaneTuple(columnAxisTupleTree.getRootNode())) == null ){
				
				ViewTuple[] colTuples = getPafViewSectionUI().getColTuples();
				// Get column tuples from the tree so that new tuples from view modification will be pulled as well.
				try {
					colTuples =  columnAxisTupleTree.getViewTuples().toArray(new ViewTuple[0]);
				} catch (InvalidStateException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if (colTuples!=null && colTuples.length>0){
					for (ViewTuple colTuple: colTuples){
						colTuple.setFreezePaneSet(false);
					}
					
					pafViewSectionUI.setColTuples(colTuples);
					try {
						columnAxisTupleTree = new PaceViewTupleTree(columnAxisTree.getInput(), pafViewSectionUI.getColTuples());
					} catch (PafException e) {
						logger.warn(e.getMessage());
					}
				}
			}
			 i=0;
			 freezePaneNodeNo = 0;
				// code block to iterate through the node where the freeze pane is set , and apply it to the innermost member within the node.
			 if ( (node = findFreezePaneTuple(rowAxisTupleTree.getRootNode())) != null )
			 {
				
				 ViewTuple[] rowTuples = pafViewSectionUI.getRowTuples();
				// Get row tuples from the tree so that new tuples from view modification will be pulled as well.
				 try {
						rowTuples =  rowAxisTupleTree.getViewTuples().toArray(new ViewTuple[0]);
					} catch (InvalidStateException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				 ViewTuple[] updatedRowTuples = new ViewTuple[rowTuples.length];
				 if (rowTuples!=null && rowTuples.length>0){
					 for (ViewTuple rowTuple: rowTuples){
						 rowTuple.setFreezePaneSet(false);
						if (node.getViewTuple()!=null){   // If node does not have children, then set the freeze pane on the selected tuple.
						 if (node.getViewTuple().getMemberDefs().length == rowTuple.getMemberDefs().length){
							 String[] nodeDef = node.getViewTuple().getMemberDefs();
							 String[] rowDef = rowTuple.getMemberDefs();

							 if (Arrays.equals(nodeDef, rowDef)){
								 // Set freeze pane to true after checking both member defs and the order of the tuple.
								 if (freezePaneNodeNo -1 == i){
									 rowTuple.setFreezePaneSet(true);
								 }
								 // Comment so that new tuples can be saved with freeze panes
								// pafViewSectionUI.getRowTuples()[i].setFreezePaneSet(true);
								
							 }
						 }
						} // If node had children, get set the freeze pane on the last innermost member
						else if	(node.hasChildren()){
							
							
							ViewTuple nodeViewTuple = getLastNodeViewTuple(node);
							if (nodeViewTuple!=null)
							if (node.getProperties().isFreezePaneSet() && Arrays.equals(nodeViewTuple.getMemberDefs(),rowTuple.getMemberDefs())){
								rowTuple.setFreezePaneSet(true);
							}
						}

						 updatedRowTuples[i] = rowTuple;
						 
						 i++;
					 }
					 
					 pafViewSectionUI.setRowTuples(updatedRowTuples);
					
					try {
						rowAxisTupleTree = new PaceViewTupleTree(rowAxisTree.getInput(), pafViewSectionUI.getRowTuples());
					} catch (PafException e) {
						logger.warn(e.getMessage());
					}
					 
					
				 }
	 
			 } // code block to handle the case where no freeze panes are set on any tuples within the Row axis.
			 else if ((node = findFreezePaneTuple(rowAxisTupleTree.getRootNode())) == null ){
				
					ViewTuple[] rowTuples = getPafViewSectionUI().getRowTuples();
					// Get row tuples from the tree so that new tuples from view modification will be pulled as well.
					try {
						rowTuples =  rowAxisTupleTree.getViewTuples().toArray(new ViewTuple[0]);
					} catch (InvalidStateException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if(rowTuples!=null && rowTuples.length>0){
						for(ViewTuple rowTuple: rowTuples){
							rowTuple.setFreezePaneSet(false);
						}
						
						pafViewSectionUI.setRowTuples(rowTuples);
						try {
							rowAxisTupleTree = new PaceViewTupleTree(rowAxisTree.getInput(), pafViewSectionUI.getRowTuples());
						} catch (PafException e) {
							logger.warn(e.getMessage());
						}
					}
				}
			
		}
		
		
		// TTN-2513 Method to get the view tuple of the last member in a node , recursive.
		public ViewTuple getLastNodeViewTuple( PaceTreeNode node){
			if(node.hasChildren()){
				List<PaceTreeNode> children = node.getChildren();
				int i=0;
				for(PaceTreeNode child: children){
					i++;
					if(child.hasChildren()){
						return getLastNodeViewTuple(child);
					
					}
					else if (children.size() == i){ // if last member reached .
						return child.getViewTuple();
					}
				}
			}
			return null;
		}
	}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.model.MemberListReorderInput;
import com.pace.admin.global.model.PaceTree;
import com.pace.admin.global.model.PaceTreeNode;
import com.pace.admin.global.model.PaceTreeNodeProperties;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.PaceTreeNodeUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.input.ViewSectionTuplesDialogInput;
import com.pace.admin.menu.providers.PaceListContentProvider;
import com.pace.admin.menu.providers.PaceListLabelProvider;
import com.pace.admin.menu.providers.PaceTreeContentProvider;
import com.pace.admin.menu.providers.PaceTreeLabelProvider;
import com.swtdesigner.ResourceManager;

public class MemberReorder extends Dialog {

	private Combo aliasTableNameCombo;
	private Tree sourceTree;
	private TreeViewer sourceTreeViewer;
	private ListViewer tupleViewer;
	private org.eclipse.swt.widgets.List tupleViewerList;
	private Label errorLabel;
	private Button dialogOkButton;
	private Button rightButton;
	private Button deleteButton;
	private Button upButton;
	private Button downButton;
	private PaceTreeNode paceTreeNodes;
	private ViewSectionTuplesDialogInput input;
	private PaceTree paceTree = null;
	private String[] newMemberOrder = null;
	private String dimension;
	private MemberListReorderInput memberListInput = null;

	/**
	 * Up button id
	 */
	public final static int UP_BUTTON_ID = -1;

	/**
	 * Down button id
	 */
	public final static int DOWN_BUTTON_ID = 1;
	
	/**
	 * 
	 */
	public final static String PAF_BLANK = PaceTreeNodeType.Blank.toString();
	
	public MemberReorder(Shell parentShell, PaceTreeNode paceTreeNodes, ViewSectionTuplesDialogInput input){
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.paceTreeNodes = paceTreeNodes;
		this.input = input;
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 4;
		container.setLayout(gridLayout_1);

		final Composite composite = new Composite(container, SWT.NONE);
		final GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd_composite.widthHint = 294;
		composite.setLayoutData(gd_composite);
		composite.setLayout(new GridLayout());

		final Composite aliasComposite = new Composite(composite, SWT.NONE);
		final GridData gd_aliasComposite = new GridData(SWT.FILL, SWT.BOTTOM, false, false);
		gd_aliasComposite.widthHint = 285;
		aliasComposite.setLayoutData(gd_aliasComposite);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		aliasComposite.setLayout(gridLayout);

		final Label aliasTableLabel = new Label(aliasComposite, SWT.NONE);
		aliasTableLabel.setText("Alias Table:");

		aliasTableNameCombo = new Combo(aliasComposite, SWT.READ_ONLY);
		EditorControlUtil.addItems(aliasTableNameCombo, input.getPageAxisTree().getAliasTableList().toArray(new String[0]));
		aliasTableNameCombo.select(0);
		aliasTableNameCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {			
				refresh();
			}
		});

		final GridData gd_aliasTableNameCombo = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_aliasTableNameCombo.widthHint = 26;
		aliasTableNameCombo.setLayoutData(gd_aliasTableNameCombo);

		
		final Label availableSelectionsLabel = new Label(composite, SWT.NONE);
		final GridData gd_availableSelectionsLabel = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd_availableSelectionsLabel.widthHint = 243;
		availableSelectionsLabel.setLayoutData(gd_availableSelectionsLabel);
		availableSelectionsLabel.setText("Available Selections");
		new Label(container, SWT.NONE);

		final Composite memberLabelComposite = new Composite(container, SWT.NONE);
		final GridData gd_memberLabelComposite = new GridData(SWT.LEFT, SWT.BOTTOM, false, false);
		gd_memberLabelComposite.heightHint = 24;
		memberLabelComposite.setLayoutData(gd_memberLabelComposite);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 2;
		memberLabelComposite.setLayout(gridLayout_2);

		final Label reorderMemberSelectionsLabel = new Label(memberLabelComposite, SWT.NONE);
		final GridData gd_reorderMemberSelectionsLabel = new GridData(SWT.FILL, SWT.BOTTOM, false, false);
		gd_reorderMemberSelectionsLabel.widthHint = 108;
		reorderMemberSelectionsLabel.setLayoutData(gd_reorderMemberSelectionsLabel);
		reorderMemberSelectionsLabel.setText("Member Selections:");

		errorLabel = new Label(memberLabelComposite, SWT.NONE);
		errorLabel.setVisible(false);
		errorLabel.setToolTipText("Invalid root member node selection.");
		errorLabel.setText("_");
		ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor(Constants.SYMBOL_ERROR_ICON_PATH);
		if (imageDesc != null ) {
			errorLabel.setImage(imageDesc.createImage());
		}
		new Label(container, SWT.NONE);

		//set the current source dimension.
		dimension = paceTreeNodes.getProperties().getDimensionName();
		
		PaceTreeNode reorderInput = null;
		
		reorderInput = input.getRowAxisTree().findTreeNode(dimension);
		
		try {
			if(reorderInput != null){
				
				ViewSectionTuplesDialogInput vstdi = new ViewSectionTuplesDialogInput(
						input.getProject(),
						input.getPafViewSectionUI());
				
				paceTree = vstdi.getRowAxisTree();
			} else {
				
				ViewSectionTuplesDialogInput vstdi = new ViewSectionTuplesDialogInput(
						input.getProject(),
						input.getPafViewSectionUI());
				
				paceTree = vstdi.getColumnAxisTree();
			}
		} catch (Exception e) {
			e.printStackTrace(); 
		}
		
		
		
		for(PaceTreeNode child : paceTree.getRootNode().getChildren()){
			if(child.getProperties().getDimensionName() == null || 
					!child.getProperties().getDimensionName().equalsIgnoreCase(dimension)){
				paceTree.getRootNode().removeChild(child);
			}
		}
		
		sourceTreeViewer = new TreeViewer(container, SWT.BORDER | SWT.MULTI);
		sourceTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent arg0) {
				IStructuredSelection selectionToAdd = (IStructuredSelection) sourceTreeViewer.getSelection();
				if ( selectionToAdd.size() > 0 ) {
					
					moveNodeToRight();
					
				}
				
			}
		});
		sourceTreeViewer.setLabelProvider(new PaceTreeLabelProvider());
		sourceTreeViewer.setContentProvider(new PaceTreeContentProvider());

		sourceTreeViewer.setInput(paceTree);
		sourceTree = sourceTreeViewer.getTree();
		sourceTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				updateAddButtion();
			}
		});
		
		final GridData gd_tree_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_tree_1.widthHint = 285;
		sourceTree.setLayoutData(gd_tree_1);
		final GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_tree.widthHint = 234;
		sourceTree.setLayoutData(gd_tree);
		

		final Composite buttonComposite_1 = new Composite(container, SWT.NONE);
		final GridData gd_buttonComposite_1 = new GridData(SWT.FILL, SWT.CENTER, false, true);
		gd_buttonComposite_1.heightHint = 69;
		buttonComposite_1.setLayoutData(gd_buttonComposite_1);
		buttonComposite_1.setLayout(new GridLayout());

		rightButton = new Button(buttonComposite_1, SWT.NONE);
		rightButton.setEnabled(false);
		rightButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				moveNodeToRight();
			}
		});
		rightButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/right_arrow.gif"));
		rightButton.setToolTipText("Add selection");

		deleteButton = new Button(buttonComposite_1, SWT.NONE);
		deleteButton.setEnabled(false);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				removeMembers();
			}
		});
		deleteButton.setToolTipText("Remove member(s)");
		
		
		Image deleteImg = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
		
		deleteButton.setImage(deleteImg);

		tupleViewer = new ListViewer(container, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		tupleViewerList = tupleViewer.getList();
		tupleViewerList.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				validator();
			}
		});
		final GridData gd_list = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_list.heightHint = 225;
		gd_list.widthHint = 253;
		tupleViewerList.setLayoutData(gd_list);
		
		final Composite buttonComposite = new Composite(container, SWT.NONE);
		buttonComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		buttonComposite.setLayout(new GridLayout());

		upButton = new Button(buttonComposite, SWT.NONE);
		upButton.setEnabled(false);
		upButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/up_arrow.gif"));
		upButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				directionalButtonPressed(UP_BUTTON_ID);
				validator();
			}
			
		});
		
		upButton.setToolTipText("Move selection up");

		downButton = new Button(buttonComposite, SWT.DOWN);
		downButton.setEnabled(false);
		downButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/down_arrow.gif"));
		downButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				directionalButtonPressed(DOWN_BUTTON_ID);
				validator();
			}
		});
		downButton.setToolTipText("Move selection down");
		
		final MenuManager tupleTreeMenuManager = new MenuManager();
		tupleTreeMenuManager.setRemoveAllWhenShown(true);
		tupleTreeMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				Image removeImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
				ImageDescriptor remove = ImageDescriptor.createFromImage(removeImage);
				
				Action deleteMember = new Action() {
					
					@Override
					public String getText() {
						return "Remove Members";
					}

					@Override
					public void run() {
						removeMembers();
					}

				};
			
				deleteMember.setImageDescriptor(remove);

				if(tupleViewerList.getSelectionIndices() != null && tupleViewerList.getSelectionIndices().length > 0){
					deleteMember.setEnabled(true);
				} else{
					deleteMember.setEnabled(false);
				}
				
				tupleTreeMenuManager.add(deleteMember);
			}
		});
		tupleViewer.getControl().setMenu(tupleTreeMenuManager.createContextMenu(tupleViewer.getControl()));
		
		
		final MenuManager sourceTupleTreeMenuManager = new MenuManager();
		sourceTupleTreeMenuManager.setRemoveAllWhenShown(true);
		sourceTupleTreeMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				ImageDescriptor right = MenuPlugin.getImageDescriptor("icons/right_arrow.gif");
				Action addMember = new Action() {
					
					@Override
					public String getText() {
						return "Add Member";
					}
					
					@Override
					public void run() {
						moveNodeToRight();
					}

				};
			
				addMember.setImageDescriptor(right);
				addMember.setEnabled(updateAddButtion());
				sourceTupleTreeMenuManager.add(addMember);
			}
		});
		sourceTreeViewer.getControl().setMenu(sourceTupleTreeMenuManager.createContextMenu(sourceTreeViewer.getControl()));
		
		loadData();
		
		tupleViewer.setContentProvider(new PaceListContentProvider());
		tupleViewer.setLabelProvider(new PaceListLabelProvider());
		tupleViewer.setInput(memberListInput);
		
		return container;
	}

	
	private void refresh(){
		((MemberListReorderInput) tupleViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
		tupleViewer.refresh(true);	
			
		
		((PaceTree) sourceTreeViewer.getInput()).setAliasTableToDisplay(aliasTableNameCombo.getText());
		sourceTreeViewer.refresh(true);
	}
	
	
	private void moveNodeToRight(){
		IStructuredSelection selectionToAdd = (IStructuredSelection) sourceTreeViewer.getSelection();
		if ( selectionToAdd.size() > 0 ) {
			for( Iterator<PaceTreeNode> it = selectionToAdd.iterator(); it.hasNext();) {
				PaceTreeNode selectedNode = it.next();
				
				PaceTreeNodeProperties nodeProp = null;
				PaceTreeNode selectedToNode = paceTreeNodes;
				
				if(selectedToNode.canAdd(selectedNode)){
				
					try{
						nodeProp = selectedNode.getProperties().clone();
						//nodeProp.setSelectionType(PaceTreeNodeSelectionType.Members);
						nodeProp.setMemberOfTuple(true);
					} catch (CloneNotSupportedException ex) {
						ex.printStackTrace(); 
					}
					
					PaceTreeNode newNode = new PaceTreeNode(
							selectedNode.getName(),
							nodeProp,
							null);
					
					newNode.setAliasTable(aliasTableNameCombo.getText());
					
					addItemToList(newNode, true);
				}
			}
		}
		validator();
	}
	
	public void addItemToList(PaceTreeNode item, boolean ignoreDuplicates){

		int idx = tupleViewerList.getSelectionIndex();
		if(idx == -1){
			memberListInput.addMember(item);
		}else{
			memberListInput.addMember(idx+1, item);
		}
		refresh();
	}
	
	public void directionalButtonPressed(int buttonId) {
		
		//get selected items
		String[] selItems = tupleViewerList.getSelection();
		
		//get selcted items indexs
		int[] selItemIdx = tupleViewerList.getSelectionIndices();
		
		//initialize vars
		int topSelIdx = 0;
		int bottomSelIdx = 0;
		int newIdx = 0;
		
		//if something is actually selected
		if(selItems != null && selItems.length > 0){
			//index of the item at the top of the selection.
			topSelIdx = selItemIdx[0];
			//index of the item at the bottom of the selection.
			bottomSelIdx = selItemIdx[selItemIdx.length - 1];
			//check top/bottm and the buttons to see if the items can be moved.
			if(buttonId == UP_BUTTON_ID){
				//Selection is at the top, so exit.
				if(topSelIdx == 0){
					return;
				}
			} else{
				//check to see if the bottom item is selected, if so we can't move down.
				if(bottomSelIdx == (tupleViewerList.getItemCount() - 1)){
					return;
				}
			}
			//set the new index.
			newIdx = topSelIdx + buttonId;
			//remove all the items to be removed.
			List<PaceTreeNode> selections = new ArrayList<PaceTreeNode>();
			for(int i : selItemIdx){
				selections.add((PaceTreeNode)tupleViewer.getElementAt(i));
			}
			memberListInput.removeMembers(selItemIdx);
			//Indexes to reselect.
			int[] idxToSel = new int[selItems.length];
			//counter
			int i = 0;
			//now reinsert the items at the new position.
			for(PaceTreeNode node : selections){	
				memberListInput.addMember(newIdx, node);
				idxToSel[i] = newIdx;
				newIdx++;
				i++;
			}
			refresh();
			tupleViewerList.setSelection(idxToSel);
		}
	}
	
	private boolean updateAddButtion(){
		IStructuredSelection selectionToAdd = (IStructuredSelection) sourceTreeViewer.getSelection();
		boolean ret = false;
		if ( selectionToAdd.size() > 0 ) {
			
			PaceTreeNode selectedFromNodeToAdd = (PaceTreeNode) selectionToAdd.getFirstElement();	
			
			PaceTreeNode selectedToNode = paceTreeNodes;
			
			if(rightButton != null){
				
				if ( selectedToNode == null ) {
					
					rightButton.setEnabled(false);
					return false;
					
				} else {
					
					ret = selectedToNode.canAdd(selectedFromNodeToAdd);
					rightButton.setEnabled(ret);
					return ret;
				}
			}
		}
		return ret;
	}
	
	
	
	protected void createButtonsForButtonBar(Composite parent) {
		dialogOkButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, true);
	}
	
	private void loadData(){
		
		java.util.List<String> strItems = new ArrayList<String>();
		memberListInput = new MemberListReorderInput();
		
		memberListInput.addMember(paceTreeNodes);
		
		List<PaceTreeNode> pageTupleNodeList = paceTreeNodes.getAdditionalPaceTreeNodeList();
		
		strItems.add(paceTreeNodes.getAliasDisplayName());
		memberListInput.addMembers(pageTupleNodeList);
		
		for(PaceTreeNode node : pageTupleNodeList){
			strItems.add(node.getAliasDisplayName());
		}

	}
	
	public void removeMembers(){
		int[] idxToSel = tupleViewerList.getSelectionIndices();
		if(idxToSel != null && idxToSel.length > 0){
			
			memberListInput.removeMembers(tupleViewerList.getSelectionIndices());
		
			refresh();
			
			tupleViewerList.setSelection(idxToSel);
			
		}
		validator();
	}
	
	private void validator(){
		
		//get selcted items indexs
		int[] selItemIdx = tupleViewerList.getSelectionIndices();
		
		if(tupleViewerList.getItemCount() == 0){
			errorLabel.setToolTipText("Must have at least one selection in your members selection.");
			errorLabel.setVisible(true);
			dialogOkButton.setEnabled(false);
			deleteButton.setEnabled(false);
			upButton.setEnabled(false);
			downButton.setEnabled(false);
		} else if(tupleViewerList.getItem(0).equalsIgnoreCase(PAF_BLANK)){	
			errorLabel.setToolTipText("Invalid root member node selection.");
			errorLabel.setVisible(true);
			dialogOkButton.setEnabled(false);
			deleteButton.setEnabled(true);
			upButton.setEnabled(true);
			downButton.setEnabled(true);
		} else if(selItemIdx != null && selItemIdx.length > 0 && selItemIdx[0] == 1 && !canMoveToRootSelection()){ 
			errorLabel.setToolTipText("Cannot move selection to top position.");
			errorLabel.setVisible(true);
			dialogOkButton.setEnabled(true);
			upButton.setEnabled(false);
			downButton.setEnabled(true);
		} else{
			errorLabel.setVisible(false);
			dialogOkButton.setEnabled(true);
			if(selItemIdx != null && selItemIdx.length > 0){
				deleteButton.setEnabled(true);
				upButton.setEnabled(true);
				downButton.setEnabled(true);
			}else{
				deleteButton.setEnabled(false);
				upButton.setEnabled(false);
				downButton.setEnabled(false);
			}
		}
		
		if(selItemIdx != null && selItemIdx.length > 0){
			//index of the item at the top of the selection.
			int topSelIdx = selItemIdx[0];
			//index of the item at the bottom of the selection.
			int bottomSelIdx = selItemIdx[selItemIdx.length - 1];
			
			//Selection is at the top, so exit.
			if(topSelIdx == 0){
				upButton.setEnabled(false);
			}
			//check to see if the bottom item is selected, if so we can't move down.
			if(bottomSelIdx == (tupleViewerList.getItemCount() - 1)){
				downButton.setEnabled(false);
			}
		}
		
	}
	
	public boolean canMoveToRootSelection(){
		
		int[] selectionToAdd = tupleViewerList.getSelectionIndices();
		
		if ( selectionToAdd.length > 0 ) {
			
			PaceTreeNode selNode = (PaceTreeNode) tupleViewer.getElementAt(selectionToAdd[0]);
			
			List<PaceTreeNodeSelectionType> treeNodeSelType = 
				PaceTreeNodeUtil.getPaceTreeNodeSelectionTypes(selNode.getProperties().getType());
			
			if(treeNodeSelType.contains(PaceTreeNodeSelectionType.Members)){
				return true;
			}
				
		}
		
		return false;
	}
	
	@Override
	protected void okPressed() {
		
		newMemberOrder = tupleViewerList.getItems();
		
		super.okPressed();

	}

	public String[] getNewMemberOrder() {
		return newMemberOrder;
	}
	
	public MemberListReorderInput getNewMemberReorder(){
		return memberListInput;
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Reorder Member Selections");
	}
	
	protected Point getInitialSize() {
		return new Point(676, 517);
	}
	
}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.NumericFormatModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.base.view.PafNumberFormat;

public class NumericFormatsDialog extends Dialog {

	private static Logger logger = Logger
			.getLogger(NumericFormatsDialog.class);

	private NumericFormatModelManager props;

	private List propertyList;

	private Text nameText;

	private Combo formatCombo;

	private Button newButton;

	private Button deleteButton;

	private Button okButton;

	private Button addButton;

	private Button formCancelButton;
	
	private Button defaultCheck;

	private String currentDefaultNumericFormatName = null;

	private IProject project;
	
	private static Font labelFont = new Font(PlatformUI.getWorkbench()
			.getActiveWorkbenchWindow().getShell().getDisplay(),
			"Arial", 8, SWT.BOLD);

	public NumericFormatsDialog(Shell parentShell, IProject project) {
		super(parentShell);
		
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.project = project;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Constants.NUMERIC_FORMATTING_WIZARD_NAME);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite area = (Composite) super.createDialogArea(parent);
		
		logger.debug("Creating Numeric Format Properties Object");
		
		props = new NumericFormatModelManager(project);
		
		logger.debug("Setting up initial layout for dialog box");

		GridLayout layout = new GridLayout();
		layout.marginBottom = 10;
		layout.marginTop = 10;
		layout.marginRight = 10;
		layout.marginLeft = 10;
		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = true;
		area.setLayout(layout);

		logger.debug("Creating List area");
		createListArea(area);

		logger.debug("Creating Form area");
		createFormArea(area);
		
		logger.debug("Setting Up Listeners");
		setupListeners();

		logger.debug("Initializing List and Form");
		selectFirstElementInList();

		return area;
	}

	private void createListArea(Composite listArea) {
		
		logger.debug("BEGIN createListArea()");

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.widthHint = 280;
		Composite listComp = new Composite(listArea, SWT.NONE);
		GridLayout listCompLayout = new GridLayout();
		listCompLayout.numColumns = 2;
		listComp.setLayout(listCompLayout);
		listComp.setLayoutData(data);

		Label label1 = new Label(listComp, SWT.NONE);
		label1.setText("Numeric Formats:");

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		data.widthHint = 200;

		propertyList = new List(listComp, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER
				| SWT.H_SCROLL);
		propertyList.setItems(props.getKeys());
		propertyList.setLayoutData(data);

		data = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		data.widthHint = 110;
		Composite buttonComp = new Composite(listComp, SWT.NONE);
		GridLayout buttonLayout = new GridLayout();
		buttonLayout.marginWidth = 0;
		buttonLayout.numColumns = 2;
		buttonLayout.makeColumnsEqualWidth = true;
		buttonComp.setLayout(buttonLayout);
		buttonComp.setLayoutData(data);

		data = new GridData(GridData.FILL_BOTH);
		newButton = new Button(buttonComp, SWT.PUSH);
		newButton.setText("New");
		newButton.setLayoutData(data);

		data = new GridData(GridData.FILL_BOTH);
		deleteButton = new Button(buttonComp, SWT.PUSH);
		deleteButton.setText("Delete");
		deleteButton.setLayoutData(data);
		
		logger.debug("END createListArea()");
	}

	private void createFormArea(Composite formArea) {
		
		logger.debug("BEGIN createFormArea()");

		GridData data = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);

		Composite formComp = new Composite(formArea, SWT.RIGHT);
		GridLayout formCompLayout = new GridLayout();
		formCompLayout.marginRight = 20;
		formCompLayout.marginLeft = 35;
		formCompLayout.numColumns = 3;
		formComp.setLayout(formCompLayout);
		formComp.setLayoutData(data);

		data = new GridData();
		data.widthHint = 50;
		Label nameLabel = new Label(formComp, SWT.NONE);
		nameLabel.setText("Name");
		nameLabel.setFont(labelFont);
		nameLabel.setLayoutData(data);

		// format name text
		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		nameText = new Text(formComp, SWT.BORDER | SWT.READ_ONLY);
		nameText.setLayoutData(data);
		nameText.setTextLimit(50);

		data = new GridData();
		data.widthHint = 50;
		Label formatLabel = new Label(formComp, SWT.NONE);
		formatLabel.setText("Format");
		formatLabel.setFont(labelFont);
		formatLabel.setLayoutData(data);

		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		formatCombo = new Combo(formComp, SWT.NONE);
		formatCombo.setItems(props.defaultNumericFormats());
		formatCombo.setLayoutData(data);
		formatCombo.setTextLimit(250);

		data = new GridData();
		data.widthHint = 50;
		Label defaultLabel = new Label(formComp, SWT.NONE);
		defaultLabel.setText("Default");
		defaultLabel.setFont(labelFont);
		defaultLabel.setLayoutData(data);
		
		defaultCheck = new Button(formComp, SWT.CHECK);

		// local area button
		data = new GridData(SWT.CENTER, SWT.CENTER, false, false, 3, 1);
		Composite localButtonArea = new Composite(formComp, SWT.NONE);
		GridLayout localButtonLayout = new GridLayout();
		localButtonLayout.numColumns = 4;
		localButtonLayout.makeColumnsEqualWidth = true;
		localButtonArea.setLayout(localButtonLayout);
		localButtonArea.setLayoutData(data);

		// put blank widget
		new Label(localButtonArea, SWT.NONE);

		data = new GridData(GridData.FILL_BOTH);
		addButton = new Button(localButtonArea, SWT.PUSH);
		addButton.setText("Add");
		addButton.setLayoutData(data);
		addButton.setVisible(false);

		data = new GridData(GridData.FILL_BOTH);
		formCancelButton = new Button(localButtonArea, SWT.PUSH);
		formCancelButton.setText("Cancel");
		formCancelButton.setLayoutData(data);
		formCancelButton.setVisible(false);
		
		logger.debug("END createFormArea()");

	}
	
	private void setupListeners() {

		// add listeners
		propertyList.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				
				String key = StringUtil.removeDefaultMarker(propertyList.getItem(propertyList
						.getSelectionIndex()));;
				
				logger.debug("User selected: " + key + " from the list");
				
				nameText.setText(key);
				
				PafNumberFormat currentFormat = (PafNumberFormat) props.getItem(key); 
				formatCombo.setText(currentFormat.getPattern());
				defaultCheck.setSelection(currentFormat.isDefaultFormat());
				 
			}

		});

		newButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				currentDefaultNumericFormatName = props
						.getNextDefaultFormatName();
				nameText.setText(currentDefaultNumericFormatName);
				formatCombo.setText("");

				nameText.setEditable(true);
				newButton.setEnabled(false);
				okButton.setEnabled(false);
				addButton.setVisible(true);
				addButton.setEnabled(false);
				formCancelButton.setVisible(true);
				deleteButton.setEnabled(false);
				formatCombo.setEnabled(true);
				defaultCheck.setEnabled(true);
				defaultCheck.setSelection(false);

			}

		});

		deleteButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				if (propertyList.getItemCount() > 0) {
					
					String key = StringUtil.removeDefaultMarker(propertyList
							.getItem(propertyList.getSelectionIndex()));
					
					if (props.contains(key)) {
						
						props.remove(key);

						// set initial selection
						if (props.getKeys().length > 0) {
							
							propertyList.setItems(props.getKeys());
							propertyList.select(0);							
							key = StringUtil.removeDefaultMarker(propertyList.getItem(0));
							nameText.setText(key);
							formatCombo.setText(((PafNumberFormat) props.getItem(propertyList
									.getItem(0))).getPattern());
							defaultCheck.setSelection(((PafNumberFormat) props.getItem(propertyList
									.getItem(0))).isDefaultFormat());
						} else {
							propertyList.removeAll();
							nameText.setText("");
							formatCombo.setText("");
							deleteButton.setEnabled(false);
							newButton.setEnabled(true);
							formatCombo.setEnabled(false);
							defaultCheck.setEnabled(false);
							defaultCheck.setSelection(false);
							okButton.setEnabled(true);
						}

					}

				}
			}
			
		});
		
		nameText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				if (nameText.getText().trim().length() == 0
						|| formatCombo.getText().trim().length() == 0) {
					addButton.setEnabled(false);
					propertyList.setEnabled(false);
				} else {
					addButton.setEnabled(true);
				}

			}

		});
		
		addButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				String name = nameText.getText().trim();
				
				if ( props.contains(name) ) {
					
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "'" + name + "' already exists.", MessageDialog.ERROR);
				
					return;
				}

				//create new format
				PafNumberFormat newFormat = new PafNumberFormat();
				newFormat.setName(name);
				newFormat.setPattern(formatCombo.getText());
				newFormat.setDefaultFormat(defaultCheck.getSelection());

				props.add(name, newFormat);
				
				nameText.setText(name);
				propertyList.setItems(props.getKeys());
				formatCombo.setText(newFormat.getPattern());
				propertyList.select(props.getIndex(name));
				defaultCheck.setSelection(newFormat.isDefaultFormat());
				addButton.setVisible(false);
				nameText.setEditable(false);
				newButton.setEnabled(true);
				
				formCancelButton.setVisible(false);
				deleteButton.setEnabled(true);
				propertyList.setEnabled(true);
				
				if ( okButton != null ) {
					okButton.setEnabled(true);
				}


			}

		});

		formCancelButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				addButton.setVisible(false);
				nameText.setEditable(false);
				newButton.setEnabled(true);
				okButton.setEnabled(true);
				formCancelButton.setVisible(false);
				deleteButton.setEnabled(true);
				propertyList.setEnabled(true);

				selectFirstElementInList();
				
			}


		});
		
		formatCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				if (nameText.getText().trim().length() == 0
						|| formatCombo.getText().trim().length() == 0) {
					addButton.setEnabled(false);
					
					propertyList.setEnabled(false);
					newButton.setEnabled(false);
					deleteButton.setEnabled(false);

				} else {

					addButton.setEnabled(true);

					if (okButton != null && (!addButton.isVisible())) {
						okButton.setEnabled(true);
					}

					if (addButton.isVisible()) {
						propertyList.setEnabled(false);
					} else {
						newButton.setEnabled(true);
						deleteButton.setEnabled(true);
						propertyList.setEnabled(true);
					}
				}

			}

		});

		formatCombo.addFocusListener(new FocusListener() {

			public void focusGained(FocusEvent e) {
				// do nothing

			}

			//when focus is lost, save in buffer
			public void focusLost(FocusEvent e) {

				//if this is not a new format
				if (!nameText.getEditable()) {
					
					PafNumberFormat format = (PafNumberFormat) props.getItem(nameText.getText());
					String pattern = format.getPattern();
					String newFormat = formatCombo.getText().trim();
					
					//if pattern is not the same as new format
					if (!pattern.equals(newFormat)) {
						
						format.setDefaultFormat(defaultCheck.getSelection());
						format.setPattern(newFormat);

						props.replace(nameText.getText(), format);
						propertyList.setItems(props.getKeys());
						propertyList.select(props.getIndex(nameText.getText()));
						
					}

					if (propertyList.getEnabled()) {

						newButton.setEnabled(true);
						deleteButton.setEnabled(true);

					}
				}

			}

		});
		
		defaultCheck.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				
				//if this is not a new format
				if (!nameText.getEditable()) {
					//if user clicks as default, update props and refresh tree to display default
					if ( defaultCheck.getSelection() ) {
						props.setDefaultFormat(nameText.getText(), true);
					} else {
						props.setDefaultFormat(nameText.getText(), false);
					}
					
					propertyList.setItems(props.getKeys());
					propertyList.select(props.getIndex(nameText.getText()));
					
				}
				
			}

		});
		
	}

	private void selectFirstElementInList() {

		logger.debug("BEGIN selectFirstElementInList()");
		
		// set initial selection
		if (propertyList.getItemCount() > 0) {
			
			propertyList.select(0);
			
			String key = StringUtil.removeDefaultMarker(propertyList.getItem(0));
			nameText.setText(key);
			
			PafNumberFormat currentFormat = (PafNumberFormat) props.getItem(key); 
			formatCombo.setText(currentFormat.getPattern());
			defaultCheck.setSelection(currentFormat.isDefaultFormat());
			
		} else {
		
			deleteButton.setEnabled(false);
			nameText.setText("");
			formatCombo.setEnabled(false);
			formatCombo.setText("");
			defaultCheck.setEnabled(false);
			defaultCheck.setSelection(false);	
			newButton.setEnabled(true);
			if ( okButton != null ) {
				okButton.setEnabled(true);
			}
			
		}

		logger.debug("END selectFirstElementInList()");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(600, 500);
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		return new Point(150, 150);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		okButton = createButton(parent, 1, IDialogConstants.OK_LABEL, true);
		createButton(parent, 2, IDialogConstants.CANCEL_LABEL, false);

	}

	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == 1) {
			okPressed();
		} else if (buttonId == 2) {
			cancelPressed();
		}
		
	}

	@Override
	protected void okPressed() {
		super.okPressed();
		logger.info("Saving Numeric Format changes");
		props.save();
		close();
	}
	
}

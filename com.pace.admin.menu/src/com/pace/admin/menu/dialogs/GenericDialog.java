/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


/**
 * TODO: replace with javadoc
 *
 * @version x.xx
 * @author Jason Milliron
 *
 */
public class GenericDialog extends Dialog {

	//TODO: remove this
	private class GenericModelObject {

		GenericModelObject() {

		}

		GenericModelObject(String name, Integer age) {
			this.name = name;
			this.age = age;
		}

		public String name;

		public Integer age;

	}

	//TODO: replace GenericModelObjec with real model
	private GenericModelObject currentModelObject = null;

	//TODO: replace GenericModelObjec with real model
	private TreeMap<String, GenericModelObject> modelMap = new TreeMap<String, GenericModelObject>();

	//TODO: replace .class with real class
	static final Logger logger = Logger.getLogger(GenericDialog.class);

	// used when buttons are clicked
	private enum ButtonId {
		New, Add, Delete, Cancel;
	}

	// form widgets
	private Text nameText;
	private Combo ageCombo;

	// left list widget
	private List list;

	// Buttons
	private Button newButton;

	private Button deleteButton;

	private Button addButton;

	private Button cancelButton;

	// used to see if form should be changed when form item changes
	private boolean cachingEnabled = false;

	/**
	 * @param parentShell parentShell
     */
	public GenericDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );

		//TODO: remove this dummy data
		modelMap.put("Jason", new GenericModelObject("Jason", 25));
		modelMap.put("Jim", new GenericModelObject("Jim", 38));
		modelMap.put("Paul", new GenericModelObject("Paul", 39));
		modelMap.put("Karl", new GenericModelObject("Karl", 30));

	}

	/**
     * @param parent Parent Composite used to add the widgets to
     * @returns Control used to paint the scrren 
     */
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		list = new List(container, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 289);
		formData.right = new FormAttachment(0, 160);
		formData.top = new FormAttachment(0, 44);
		formData.left = new FormAttachment(0, 25);
		list.setLayoutData(formData);

		final Label listLabel = new Label(container, SWT.NONE);
		final FormData formData_1 = new FormData();
		formData_1.left = new FormAttachment(0, 25);
		formData_1.bottom = new FormAttachment(0, 39);
		formData_1.right = new FormAttachment(0, 150);
		formData_1.top = new FormAttachment(0, 24);
		listLabel.setLayoutData(formData_1);
		listLabel.setText("replace with label");

		newButton = new Button(container, SWT.NONE);
		final FormData formData_2 = new FormData();
		formData_2.bottom = new FormAttachment(0, 315);
		formData_2.right = new FormAttachment(0, 70);
		formData_2.top = new FormAttachment(list, 5, SWT.BOTTOM);
		formData_2.left = new FormAttachment(0, 25);
		newButton.setLayoutData(formData_2);
		newButton.setText("New");

		deleteButton = new Button(container, SWT.NONE);
		final FormData formData_2_1 = new FormData();
		formData_2_1.left = new FormAttachment(0, 76);
		formData_2_1.bottom = new FormAttachment(0, 315);
		formData_2_1.top = new FormAttachment(0, 294);
		formData_2_1.right = new FormAttachment(0, 120);
		deleteButton.setLayoutData(formData_2_1);
		deleteButton.setText("Delete");

		final Label nameLabel = new Label(container, SWT.NONE);
		nameLabel.setAlignment(SWT.RIGHT);
		final FormData formData_3 = new FormData();
		formData_3.bottom = new FormAttachment(0, 85);
		formData_3.right = new FormAttachment(0, 275);
		formData_3.top = new FormAttachment(0, 70);
		formData_3.left = new FormAttachment(0, 220);
		nameLabel.setLayoutData(formData_3);
		nameLabel.setText("Name:");

		nameText = new Text(container, SWT.BORDER);
		final FormData formData_4 = new FormData();
		formData_4.right = new FormAttachment(0, 405);
		formData_4.left = new FormAttachment(0, 285);
		formData_4.top = new FormAttachment(0, 67);
		nameText.setLayoutData(formData_4);

		ageCombo = new Combo(container, SWT.READ_ONLY);
		final FormData formData_6 = new FormData();
		formData_6.right = new FormAttachment(0, 335);
		formData_6.top = new FormAttachment(0, 92);
		formData_6.left = new FormAttachment(0, 285);
		ageCombo.setLayoutData(formData_6);
		ageCombo.setItems(createTestDropdownData());

		final Label ageLabel = new Label(container, SWT.NONE);
		final FormData formData_3_1 = new FormData();
		formData_3_1.bottom = new FormAttachment(0, 110);
		formData_3_1.top = new FormAttachment(0, 95);
		formData_3_1.right = new FormAttachment(0, 275);
		formData_3_1.left = new FormAttachment(0, 220);
		ageLabel.setLayoutData(formData_3_1);
		ageLabel.setAlignment(SWT.RIGHT);
		ageLabel.setText("Age:");

		addButton = new Button(container, SWT.NONE);
		addButton.setVisible(false);
		final FormData formData_2_2 = new FormData();
		formData_2_2.right = new FormAttachment(0, 315);
		formData_2_2.bottom = new FormAttachment(0, 241);
		formData_2_2.top = new FormAttachment(0, 220);
		formData_2_2.left = new FormAttachment(0, 271);
		addButton.setLayoutData(formData_2_2);
		addButton.setText("Add");

		cancelButton = new Button(container, SWT.NONE);
		cancelButton.setVisible(false);
		final FormData formData_2_2_1 = new FormData();
		formData_2_2_1.top = new FormAttachment(0, 220);
		formData_2_2_1.left = new FormAttachment(0, 322);
		formData_2_2_1.bottom = new FormAttachment(0, 241);
		formData_2_2_1.right = new FormAttachment(0, 368);
		cancelButton.setLayoutData(formData_2_2_1);
		cancelButton.setText("Cancel");

		//initially populate the list items.
		refreshList();

		// set up dialog
		initializeDialog();

		// set up listeners
		setupListeners();

		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(501, 430);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("replace with title");
	}

	/**
     * Initials the form.
     */
	private void initializeDialog() {

		//if model map contains items
		if (modelMap.size() > 0) {
			list.setSelection(0);
			populateForm(list.getSelection()[0]);
			enableForm(true, false);
			deleteButton.setEnabled(true);
		} else {
			list.deselectAll();
			list.setEnabled(true);
			clearForm();
			enableForm(false, true);
			deleteButton.setEnabled(false);
		}

	}

	/**
     * Populates the form from the model map using the key param
     * 
     * @param key used to get object from the model map
     */
	private void populateForm(String key) {

		//if the model map has the key, populate the form with it
		if (modelMap.containsKey(key)) {

			cachingEnabled = false;

			nameText.setEnabled(false);

			GenericModelObject model = modelMap.get(key);

			nameText.setText(model.name);

			if (model.age != null) {
				ageCombo.setText(model.age.toString());
			}

			cachingEnabled = true;

		}

	}

	//TODO: Remove this
	public String[] createTestDropdownData() {

		ArrayList<String> numbers = new ArrayList<String>();

		numbers.add("");

		for (int i = 1; i < 100; i++) {
			numbers.add(new Integer(i).toString());
		}

		return numbers.toArray(new String[0]);

	}

	/**
     * Sets up all the listeners
     *  
     */
	private void setupListeners() {

		//setup the list listener
		setupListListener();
		
		//setup all form listeners
		setupFormListeners();
		
		//setup all button listeners
		setupButtonListeners();

	}

	/**
     * Sets up the listener for the list box
     * 
     */
	private void setupListListener() {

		list.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				//if a selection exist
				if (list.getSelectionCount() > 0) {
					logger.debug(list.getSelection()[0]);

					//save cached model if one exist
					saveCachedModel();
					
					//clear and popualte form from selection
					clearForm();
					populateForm(list.getSelection()[0]);

				}

			}

		});

	}

	/**
     * Sets up the listeners for the form widgets
     * 
     */
	private void setupFormListeners() {

		nameText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				//get and trim key
				String key = nameText.getText().trim();

				// if key already in model map, disable add button
				if (modelMap.containsKey(key)) {

					addButton.setEnabled(false);

				} else {

					addButton.setEnabled(true);

				}

			}

		});

		ageCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				//data has been changed
				formDataChanged();

			}

		});

	}

	/**
     * Called when any the form data changes
     * 
     */
	private void formDataChanged() {

		//if caching is enabled, cach the form
		if (cachingEnabled) {
			cacheForm();
		}

	}

	/**
     * Caches the form to a temp object model
     * 
     */
	private void cacheForm() {

		logger.debug("Caching form");

		//if current model object is null, create one
		if (currentModelObject == null) {

			currentModelObject = new GenericModelObject();

		}

		currentModelObject.name = nameText.getText().trim();

		if (!ageCombo.getText().trim().equals("")) {
			currentModelObject.age = new Integer(ageCombo.getText());
		}
	}

	/**
     * Sets up the listeners for the buttons
     * 
     */
	private void setupButtonListeners() {

		// new button listeners
		setupButtonSelectionListener(newButton, ButtonId.New);

		// delete button listeners
		setupButtonSelectionListener(deleteButton, ButtonId.Delete);

		// add button listeners
		setupButtonSelectionListener(addButton, ButtonId.Add);

		// cancel button listeners
		setupButtonSelectionListener(cancelButton, ButtonId.Cancel);

	}

	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}

	/**
     * Used to perfrom an action when a button is pressed
     * 
     * @param buttonId enum for which button was selected
     */
	private void buttonSelected(ButtonId buttonId) {

		switch (buttonId) {

		case Add:

			logger.debug(buttonId + " button clicked");
			addButtonAction();
			cachingEnabled = true;
			break;

		case Delete:

			logger.debug(buttonId + " button clicked");
			deleteButtonAction();

			break;

		case New:
			
			logger.debug(buttonId + " button clicked");
			newButtonAction();
			cachingEnabled = false;
			break;

		case Cancel:

			logger.debug(buttonId + " button clicked");
			cancelButtonAction();
			cachingEnabled = true;
			break;

		}

	}

	/**
     * Action called when new button was pressed   
     */
	private void newButtonAction() {

		list.setEnabled(false);
		list.deselectAll();

		enableForm(true, true);

		clearForm();

		newButton.setEnabled(false);
		deleteButton.setEnabled(false);

		addButton.setVisible(true);
		addButton.setEnabled(false);

		cancelButton.setVisible(true);
		cancelButton.setEnabled(true);

	}

	/**
     * Action called when delete button was pressed   
     */
	private void deleteButtonAction() {

		// if selection count is greater than 0, delete item
		if (list.getSelectionCount() > 0) {

			String key = list.getSelection()[0];

			if (modelMap.containsKey(key)) {

				modelMap.remove(key);
				list.removeAll();
				refreshList();
				initializeDialog();

			}

		} else {
			
			list.deselectAll();
			
		}

	}

	/**
     * Clears the current form.   
     */
	private void clearForm() {

		cachingEnabled = false;

		nameText.setText("");
		ageCombo.setText("");

		cachingEnabled = true;
	}

	/**
     * Action called when cancel button was pressed   
     */
	private void cancelButtonAction() {

		list.setEnabled(true);

		enableForm(false, true);

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);

		addButton.setVisible(false);
		cancelButton.setVisible(false);

		initializeDialog();

	}

	/**
     * Action called when add button was pressed   
     */
	private void addButtonAction() {

		String key = nameText.getText().trim();

		Integer age = null;

		if (!ageCombo.getText().equals("")) {
			age = new Integer(ageCombo.getText());
		}

		GenericModelObject model = new GenericModelObject(key, age);

		modelMap.put(key, model);

		refreshList();

		list.setEnabled(true);
		list.select(getListItemIndex(key));
		populateForm(key);

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);
		addButton.setVisible(false);
		cancelButton.setVisible(false);

	}

	/**
     * Removes all entries in the list and repopulates with the keyset from the model map   
     */
	private void refreshList() {

		list.removeAll();
		list.setItems(modelMap.keySet().toArray(new String[0]));

	}

	/**
     * Enables the form fields
     * 
     * @param enable enables/disables the form fields
     * @param includePrimaryKey include the primary key field when enabling/disabling form
     */
	private void enableForm(boolean enable, boolean includePrimaryKey) {

		if (includePrimaryKey) {
			nameText.setEnabled(enable);
		}

		ageCombo.setEnabled(enable);

	}

	/**
     * Used to get the index of the key in the list
     * 
     *    @returns int index of key in list
     */
	private int getListItemIndex(String key) {

		int index = 0;

		for (String item : list.getItems()) {

			if (item.equals(key)) {
				break;
			}

			index++;
		}

		return index;
	}

	@Override
	protected void okPressed() {

		saveCachedModel();

		super.okPressed();

	}
	
	/**
     * Saves the cached model objects.   
     */
	private void saveCachedModel() {

		if (currentModelObject != null) {

			if (modelMap.containsKey(currentModelObject.name)) {

				logger.debug("Updating model map for key "
						+ currentModelObject.name);
				modelMap.put(currentModelObject.name, currentModelObject);
				currentModelObject = null;

			}

		}

	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class CustomMenuSelectionDialog extends TitleAreaDialog {

	private List list_2;
	private List list;
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public CustomMenuSelectionDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 20;
		gridLayout.marginHeight = 30;
		gridLayout.numColumns = 4;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(new GridLayout());

		final Label availableCustomMenusLabel = new Label(composite, SWT.NONE);
		availableCustomMenusLabel.setText("Available Custom Menus");

		final ListViewer listViewer = new ListViewer(composite, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		list = listViewer.getList();
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 137;
		list.setLayoutData(gridData);
		listViewer.setInput(new Object());

		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
		composite_1.setLayout(new GridLayout());

		final Composite composite_3 = new Composite(composite_1, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.CENTER, SWT.CENTER, false, true);
		gridData_2.widthHint = 36;
		composite_3.setLayoutData(gridData_2);
		composite_3.setLayout(new GridLayout());

		final Button button = new Button(composite_3, SWT.ARROW | SWT.RIGHT);
		button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		button.setText("button");

		final Button button_1 = new Button(composite_3, SWT.ARROW | SWT.LEFT);
		button_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		button_1.setText("button");

		final Composite composite_2 = new Composite(container, SWT.NONE);
		final GridData gd_composite_2 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_composite_2.widthHint = 167;
		composite_2.setLayoutData(gd_composite_2);
		composite_2.setLayout(new GridLayout());

		final Label availableCustomMenusLabel_1 = new Label(composite_2, SWT.WRAP);
		final GridData gd_availableCustomMenusLabel_1 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_availableCustomMenusLabel_1.widthHint = 182;
		gd_availableCustomMenusLabel_1.heightHint = 33;
		availableCustomMenusLabel_1.setLayoutData(gd_availableCustomMenusLabel_1);
		availableCustomMenusLabel_1.setText("Selected Auto Run On Save Custom Menus");

		final ListViewer listViewer_1 = new ListViewer(composite_2, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		list_2 = listViewer_1.getList();
		final GridData gridData_1 = new GridData(SWT.LEFT, SWT.FILL, true, true);
		gridData_1.widthHint = 165;
		list_2.setLayoutData(gridData_1);
		listViewer_1.setInput(new Object());

		final Composite composite_1_1 = new Composite(container, SWT.NONE);
		composite_1_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
		composite_1_1.setLayout(new GridLayout());

		final Composite composite_3_1 = new Composite(composite_1_1, SWT.NONE);
		composite_3_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true));
		composite_3_1.setLayout(new GridLayout());

		final Button button_2 = new Button(composite_3_1, SWT.ARROW);
		button_2.setText("button");

		final Button button_1_1 = new Button(composite_3_1, SWT.ARROW | SWT.DOWN);
		button_1_1.setText("button");
		setTitle("Auto Run On Save Custom Menu Selector");
		setMessage("Please select from the Available Custom Menus and then order the Selected Auto Run On Save Custom Menu items.");
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(559, 375);
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.composite.FindTreeModule;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.ListErrorMessagesDialog;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PaceTreeNodeUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.internal.InvalidSpecificationMember;
import com.pace.admin.menu.dialogs.internal.UserSelectionSpecificationMember;
import com.pace.admin.menu.views.InvalidSpecificationMemberView;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.data.PafMemberList;
import com.pace.base.data.UserMemberLists;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafNotAuthenticatedSoapException_Exception;
import com.pace.server.client.PafNotAuthorizedSoapException_Exception;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.ValidateUserSecurityRequest;
import com.pace.server.client.ValidationResponse;
import com.swtdesigner.ResourceManager;


public class UserSelectionDimSpecWizardDialog extends Dialog {
	private static Logger logger = Logger.getLogger(UserSelectionDimSpecWizardDialog.class);
	private static final String EMPTY_STRING_ARRAY="";
	private static final String VERSION="Version";
	
	java.util.List<String> selectionTypeList = PaceTreeNodeUtil.getAllUserSelectionTypes();			
	java.util.List<String> levelGenTypeList = PaceTreeNodeUtil.getAllLevelGenerationTypes();			
	private IProject project;
	private String dimension;
	private String[] userSelectionSpecs;
	private UserMemberLists userMemberList;
	private boolean displayWarning;
	private String warningText;
	private Map<String, Map<String, TreeItem>> dimensionMapOfTreeItemMap = new HashMap<String, Map<String, TreeItem>>();
	private Map<String, Tree> dimensionControlTreeMap = new HashMap<String, Tree>();
	private Integer maxTreeDepth;
	private Map<String, List> specificationMemberControlMap = new HashMap<String, List>();
	private Map<String, java.util.List<UserSelectionSpecificationMember>> specMemberMap = new HashMap<String, java.util.List<UserSelectionSpecificationMember>>();
	private Map<String, Map<String, String>> dimSubMemberListTextMap = new HashMap<String, Map<String, String>>();
	private Map<String, Button> applyAliasTableMap = new HashMap<String, Button>();
	private Map<String, Combo> aliasTableComboMap = new HashMap<String, Combo>();
	private Button addButton, removeButton, clearButton, validateButton;
	private Combo selectionTypeCombo, levelGenDropDown;
	private Spinner levelGenSpinner;
	// map to hold the max number of levels
	private MouseListener treeMouseListener;
	private Font boldFont, normalFont;
	private PafSimpleDimTree pafDimTree;
	private boolean multipleSelections;
	private Group selectionTypeGroup;
	private String childDialogTitle = "User Selection Filter Wizard";
	private Label lblWarning;
	
	/**
	 * @wbp.parser.constructor
	 */
	public UserSelectionDimSpecWizardDialog(Shell parentShell, IProject project, String dimension, String[] userSelectionSpecs) {
		this(parentShell, project, dimension, userSelectionSpecs, true, null, null, false, null);
	}
	
	public UserSelectionDimSpecWizardDialog(Shell parentShell, IProject project, String dimension, String[] userSelectionSpecs,
			boolean allowMulSelections, PafSimpleDimTree filteredTree, String dialogText, boolean displayWarning, String warningText) {
		super(parentShell);
		this.dimension = dimension;
		this.project = project;
		this.userSelectionSpecs = userSelectionSpecs;
		this.multipleSelections = allowMulSelections;
		this.displayWarning = displayWarning;
		this.warningText = warningText;
		if(filteredTree == null){
			setPafDimTree();
		} else {
			this.pafDimTree = filteredTree;
		}
		if(dialogText != null && dialogText.length() > 0){
			childDialogTitle = dialogText;
		}
		
	}

	public PafSimpleDimTree getPafDimTree() {
		return pafDimTree;
	}
	
	private void setPafDimTree() {
		PafApplicationDef[] pafAppDefAr = PafApplicationUtil.getPafApps(project);
		try {
			
			pafDimTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
					project, pafAppDefAr[0].getAppId(), dimension);
			
		} catch (Exception e1) {
	    	logger.error(e1.getMessage());
		}
	}
	
	public IFile getIFileFromProject(String fileName) {
		
		IFolder confFolder = project.getFolder(Constants.CONF_DIR);

		return confFolder.getFile(fileName);
		
	}
	
	/**
	 * @param parent
	 *            Parent Composite used to add the widgets to
	 * @returns Control used to paint the screen
	 */
	protected Control createDialogArea(Composite parent) {
		

		try {
			userMemberList = (UserMemberLists) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_UserMemberLists));
		} catch (Exception ex) { 
			logger.error(ex.getMessage()); 
		}
		
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, true));
		GridData containerData = new GridData(SWT.FILL, SWT.FILL, true, true);
		containerData.heightHint = 350;
		container.setLayoutData(containerData);

		if(dimSubMemberListTextMap.get(dimension) == null){
			dimSubMemberListTextMap.put(dimension, new HashMap<String, String>());
		}
		/*******************************
		 * Dimension Group	****
		 *******************************/
		Group grpSecDim = new Group(container, SWT.NONE);
		grpSecDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecDim.setLayout(new GridLayout(3, false));
		grpSecDim.setText("Dimension");

		final Button aliasTableCheck = new Button(grpSecDim, SWT.CHECK);
		aliasTableCheck.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		aliasTableCheck.setText("View Alias Table");

		applyAliasTableMap.put(dimension, aliasTableCheck);

		final Combo aliasTableDropDown = new Combo(grpSecDim,SWT.READ_ONLY);
		aliasTableDropDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		if (pafDimTree != null && pafDimTree.getAliasTableNames().size() > 0 ) {

			String[] aliasTableNames = pafDimTree.getAliasTableNames().toArray(new String[0]);

			if (aliasTableNames != null) {

				aliasTableDropDown.setItems(aliasTableNames);

			}

			// select first item
			aliasTableDropDown.select(0);

		}

		aliasTableDropDown.setEnabled(false);

		aliasTableComboMap.put(dimension, aliasTableDropDown);
		
		new Label(grpSecDim, SWT.NONE);
		

		final FindTreeModule ftm = new FindTreeModule(grpSecDim, SWT.NONE);
		ftm.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		addButton = new Button(grpSecDim, SWT.PUSH );
		addButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		addButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/add.gif"));
		addButton.setToolTipText("Add Tree Item to Specification");
		addButton.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				addNewSpecMember();
				enableControls();
			}
		});

		final Tree dimensionTree = new Tree(grpSecDim, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		dimensionTree.addListener(SWT.EraseItem, new Listener() { 
			public void handleEvent(Event event) { 
				final boolean multiSelect = multipleSelections;
				final Button addIt = addButton;
				//final java.util.List<String> sel = selectable;
				//if(sel == null || sel.size() == 0) return;
				if((event.detail & SWT.SELECTED) != 0){
					//dimensionTree.deselectAll();
					if(!multiSelect){
						final List specMemberList = specificationMemberControlMap.get(dimension);
						if(specMemberList.getItemCount() > 0){
							dimensionTree.deselectAll();
							addIt.setEnabled(false);
						}else {
							addIt.setEnabled(true);
						}
					} else {
						addIt.setEnabled(true);
					}
				} 
			} 
		}); 
		dimensionTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		
		ftm.setTree(dimensionTree);
		
		dimensionTree.addMouseListener(getTreeMouseListener());
		
		dimensionControlTreeMap.put(dimension, dimensionTree);
		Tree treeControl = dimensionControlTreeMap.get(dimension);

		// create swt tree model from paf tree
		createTreeModel(treeControl, pafDimTree, pafDimTree.getRootKey());

		// expand top item
		if(treeControl != null && treeControl.getTopItem() != null){
			treeControl.getTopItem().setExpanded(true);
		}
		/***********************************
		 * Security Specification Group
		 **********************************/
		Group grpSecSpec = new Group(container, SWT.NONE);
		grpSecSpec.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecSpec.setLayout(new GridLayout(3, false));
		grpSecSpec.setText("Specification");

		validateButton = new Button(grpSecSpec, SWT.CENTER);
		validateButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/validator.gif"));
		validateButton.setToolTipText("Validate (edited) Specification");
		validateButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				validateAllSpecifications();
			}
		});

		removeButton = new Button(grpSecSpec, SWT.CENTER);
		removeButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/delete.png"));
		removeButton.setToolTipText("Delete selected item from Specification");
		removeButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeSelectedSpecification();
				enableControls();
			}
		});

		clearButton = new Button(grpSecSpec, SWT.CENTER);
		clearButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/clear.png"));
		clearButton.setToolTipText("Clear Specification");
		clearButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeAllSpecification();
				enableControls();
			}
		});

		final List specListControl = new List(grpSecSpec, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		specListControl.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		specListControl.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				onSelectedSpecListControlChange();
			}
		
		});

		specificationMemberControlMap.put(dimension, specListControl);

		aliasTableCheck.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				// get selection
				boolean isSelected = aliasTableCheck.getSelection();

				// if apply alias is checked, enable drop down, select first
				// element and update tree with aliases
				if (isSelected) {

					aliasTableDropDown.setEnabled(true);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimensionTree, specListControl, aliasTableDropDown.getText());

				} else {

					aliasTableDropDown.setEnabled(false);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimensionTree, specListControl, null);
				}

			}

		});

		aliasTableDropDown.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				updateTreeWithAliases(dimensionTree, specListControl, aliasTableDropDown.getText());

			}

		});
		
		/*******************************
		 * Selection Type Group	****
		 *******************************/
		selectionTypeGroup = new Group(container, SWT.NONE);
		selectionTypeGroup.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		GridLayout gl_selectionTypeGroup = new GridLayout(3, false);
		gl_selectionTypeGroup.verticalSpacing = 1;
		selectionTypeGroup.setLayout(gl_selectionTypeGroup);
		selectionTypeGroup.setText("Selection Type");
		
		selectionTypeCombo = new Combo(selectionTypeGroup, SWT.READ_ONLY);
		selectionTypeCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				PaceTreeNodeSelectionType selectionType = PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectionTypeCombo.getText());
				if( selectionType != null ) {
					int minLevelGenSpinnerLength = 0;
					int maxLevelGenSpinnerLength = maxTreeDepth;
					//get old max spinner values and log
					int oldMaxSpinnerValue = levelGenSpinner.getMaximum();
					int currentLevelGenValue = 0;
					switch (selectionType) {
						case Descendants:
						case IDescendants:
							if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
									levelGenDropDown.setText(LevelGenerationType.Bottom.toString());
							}
							levelGenDropDown.setEnabled(true);
							break;
							
						case DescLevel:
						case Level:
						case LevelGen:
							if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
								levelGenDropDown.setText(LevelGenerationType.Level.toString());
							}
							levelGenDropDown.setEnabled(true);
							minLevelGenSpinnerLength = 0;
							maxLevelGenSpinnerLength = maxTreeDepth;
							//get old max spinner values and log
							oldMaxSpinnerValue = levelGenSpinner.getMaximum();
						
							//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
							if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
								
								levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
								levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
								
							} else {
							
								levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
								levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
								
							}
							
							if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
								
								currentLevelGenValue = levelGenSpinner.getMinimum();
								
							} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
								
								currentLevelGenValue = levelGenSpinner.getMaximum();
								
							}
							levelGenSpinner.setSelection(currentLevelGenValue);
						break;
						
						case DescGen:
						case Generation:
							if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
								levelGenDropDown.setText(LevelGenerationType.Generation.toString());
							}
							levelGenDropDown.setEnabled(true);
							minLevelGenSpinnerLength = 1;
							maxLevelGenSpinnerLength = maxTreeDepth + 1;
							
							//get old max spinner values and log
							oldMaxSpinnerValue = levelGenSpinner.getMaximum();
						
							//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
							if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
								
								levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
								levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
								
							} else {
							
								levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
								levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
								
							}
							
							if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
								
								currentLevelGenValue = levelGenSpinner.getMinimum();
								
							} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
								
								currentLevelGenValue = levelGenSpinner.getMaximum();
								
							}
							
							levelGenSpinner.setSelection(currentLevelGenValue);
							break;
							
						default:
							levelGenDropDown.setText(EMPTY_STRING_ARRAY);
							levelGenDropDown.setEnabled(false);
							break;
					}
				}
				if( levelGenDropDown.isEnabled() ) {
					LevelGenerationType levelGenType = LevelGenerationType.valueOf(levelGenDropDown.getText());
					switch (levelGenType) {
						case Bottom:
							levelGenSpinner.setSelection(0);
							levelGenSpinner.setEnabled(false);
							break;
						case Level:
						case Generation:
							levelGenSpinner.setEnabled(true);
							break;
					}
				}
				else {
					levelGenSpinner.setSelection(0);
					levelGenSpinner.setEnabled(false);
				}
				
				updateSpecMember();

			}
		});
		selectionTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		levelGenDropDown = new Combo(selectionTypeGroup, SWT.NONE);
		levelGenDropDown.setEnabled(false);
		levelGenDropDown.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {

				int minLevelGenSpinnerLength = 0;
				int maxLevelGenSpinnerLength = maxTreeDepth;
				//get old max spinner values and log
				int oldMaxSpinnerValue = levelGenSpinner.getMaximum();
				int currentLevelGenValue = 0;
				LevelGenerationType levelGenType = LevelGenerationType.valueOf(levelGenDropDown.getText());
				switch (levelGenType) {
					case Bottom:
						levelGenSpinner.setSelection(0);
						levelGenSpinner.setEnabled(false);
						break;
					case Level:
						minLevelGenSpinnerLength = 0;
						maxLevelGenSpinnerLength = maxTreeDepth;
						//get old max spinner values and log
						oldMaxSpinnerValue = levelGenSpinner.getMaximum();
					
						//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
						if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
							
							levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
							levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
							
						} else {
						
							levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
							levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
							
						}
						
						if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
							
							currentLevelGenValue = levelGenSpinner.getMinimum();
							
						} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
							
							currentLevelGenValue = levelGenSpinner.getMaximum();
							
						}
						levelGenSpinner.setSelection(currentLevelGenValue);
						levelGenSpinner.setEnabled(true);
						break;
					case Generation:
						minLevelGenSpinnerLength = 1;
						maxLevelGenSpinnerLength = maxTreeDepth + 1;
						
						//get old max spinner values and log
						oldMaxSpinnerValue = levelGenSpinner.getMaximum();
					
						//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
						if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
							
							levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
							levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
							
						} else {
						
							levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
							levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
							
						}
						
						if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
							
							currentLevelGenValue = levelGenSpinner.getMinimum();
							
						} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
							
							currentLevelGenValue = levelGenSpinner.getMaximum();
							
						}
						
						levelGenSpinner.setSelection(currentLevelGenValue);
						levelGenSpinner.setEnabled(true);
						break;
				}
				
				updateSpecMember();
				
			}
		});
		final GridData gd_levelGenDropDown = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_levelGenDropDown.widthHint = 71;
		levelGenDropDown.setLayoutData(gd_levelGenDropDown);
				
						
		levelGenSpinner = new Spinner(selectionTypeGroup, SWT.BORDER);
		GridData gd_levelGenSpinner = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_levelGenSpinner.widthHint = 44;
		levelGenSpinner.setLayoutData(gd_levelGenSpinner);
		levelGenSpinner.setEnabled(false);
		
		new Label(selectionTypeGroup, SWT.NONE);
		new Label(selectionTypeGroup, SWT.NONE);
		new Label(selectionTypeGroup, SWT.NONE);
		
		lblWarning = new Label(container, SWT.WRAP);
		Display display = Display.getCurrent();
		if(display != null){
			lblWarning.setForeground(display.getSystemColor(SWT.COLOR_RED));
		}
		lblWarning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		if(this.displayWarning && this.warningText != null){
			this.lblWarning.setText(this.warningText);
		}
		
		levelGenSpinner.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				updateSpecMember();
			}
		});
		
		
		
		
		populateSpecForm();
		updateTreeSelectionsForSpecMember();
		updateSelectionTypeLevelGenCombos();
		enableControls();
		
		return container;
	
	}
	
	protected Point getInitialSize() {
		return new Point(640, 480);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		newShell.setText(childDialogTitle);
	}
	
	private void createTreeModel(Tree tree, PafSimpleDimTree simpleBaseTree,
			String root) {

		logger.debug("Create Tree Model start: " + root);

		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();

		try {

			HashMap<String, PafSimpleDimMember> treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(simpleBaseTree);
			//Why-?, The Root node should be the root that's passed into the sub!
			//PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(dimension);
			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(root);
			maxTreeDepth = rootMember.getPafSimpleDimMemberProps().getLevelNumber();	

			addRootTreeNode(tree, null, rootMember, root);
			
			if( ! rootMember.getKey().equals(VERSION) ) {
				if(rootMember.getKey().equals(dimension))
				{
				PafSimpleDimMember uowRootMember = new PafSimpleDimMember();
				uowRootMember.setKey(PafBaseConstants.UOW_ROOT);
				PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
				prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
				prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
				uowRootMember.setPafSimpleDimMemberProps(prop);
				addRootTreeNode(tree, dimensionMapOfTreeItemMap.get(root).get(rootMember.getKey()), uowRootMember, root);
			}
			}
			
			if( userMemberList != null 
					&& userMemberList.getMemberLists() != null 
					&& userMemberList.getMemberLists().size() > 0 ) {
				for ( Map.Entry<String, PafMemberList> entry : userMemberList.getMemberLists().entrySet() )
				{
					if( entry.getValue().getDimName().equalsIgnoreCase(dimension) ) {
						PafSimpleDimMember memberLists = new PafSimpleDimMember();
						memberLists.setKey(Constants.MEMBER_LISTS);
						PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
						prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
						prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
						memberLists.setPafSimpleDimMemberProps(prop);
						if( dimensionMapOfTreeItemMap.get(root).get(memberLists.getKey()) == null )
							addRootTreeNode(tree, dimensionMapOfTreeItemMap.get(root).get(rootMember.getKey()), memberLists, root);
						
						PafSimpleDimMember memberList = new PafSimpleDimMember();
						memberList.setKey(PafBaseConstants.MEMBERLIST_TOKEN + "(" + entry.getKey() + ")");
						prop = new PafSimpleDimMemberProps();
						prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
						prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
						memberList.setPafSimpleDimMemberProps(prop);
						addRootTreeNode(tree, dimensionMapOfTreeItemMap.get(root).get(Constants.MEMBER_LISTS), memberList, root);
					}
				}
			}
			
			addChildrenTreeNode(treeHashMap, tree, dimensionMapOfTreeItemMap.get(root).get(rootMember.getKey()), rootMember, root);

			cachedTreeMap.put(root, tree);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Create Tree Model end");
	}

	/**
	 * Adds a tree node to a tree.
	 * 
	 * @param treeHashMap
	 *            The hash map containing the tree components.
	 * @param tree
	 *            The tree to add the nodes to.
	 * @param parent
	 *            The parent tree item to have the nodes added to.
	 * @param member
	 *            The paf simple memeber to add to the parent.
	 * @param dimension
	 *            The dimension of this tree.
	 */
	private void addRootTreeNode(Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		newItem.setData(member);

		Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimension);

		if (treeItemMap == null) {

			treeItemMap = new HashMap<String, TreeItem>();

		}

		treeItemMap.put(newItem.getText(), newItem);

		dimensionMapOfTreeItemMap.put(dimension, treeItemMap);

	}

	private void addChildrenTreeNode(HashMap<String, PafSimpleDimMember> treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		if (member.getChildKeys() != null && member.getChildKeys().size() > 0 ) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				TreeItem newItem = new TreeItem(parent, SWT.NONE);
				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap
						.get(child);

				newItem.setText(childMember.getKey());
				
				newItem.setData(childMember);
		
				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimension);
				
				if (treeItemMap == null) {
		
					treeItemMap = new HashMap<String, TreeItem>();
		
				}
		
				treeItemMap.put(newItem.getText(), newItem);
		
				dimensionMapOfTreeItemMap.put(dimension, treeItemMap);

				addChildrenTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}
	}

	protected void updateSelectionTypeLevelGenCombos() {		
		List secMemList = specificationMemberControlMap.get(dimension);
		
		if ( secMemList.getSelectionCount() == 1 ) {
			
			UserSelectionSpecificationMember specMember = 
				new UserSelectionSpecificationMember(dimension, secMemList.getSelection()[0]);
			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimension);
			int levelNumber = 0;
			
			if (treeItemMap != null) {
				TreeItem selectedTreeItem = treeItemMap.get(specMember.getMember());
				
				if( selectedTreeItem != null ) {
					PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
					levelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
				}
			}
			
			if( levelNumber != 0 ) {
				selectionTypeCombo.setItems(selectionTypeList.toArray(new String[0]));
				levelGenDropDown.setItems(levelGenTypeList.toArray(new String[0]));
			}
			else {
				selectionTypeCombo.setItems(new String[]{PaceTreeNodeUtil.SELECTION_DISPLAY});
				levelGenDropDown.setItems(new String[]{EMPTY_STRING_ARRAY});
			}
			
			PaceTreeNodeSelectionType selectionType = specMember.getSelectionType();
			LevelGenerationType levelGenType = specMember.getLevelGenType();
			if( selectionType == null || levelNumber == 0 ) {
				selectionTypeCombo.setText(PaceTreeNodeUtil.SELECTION_DISPLAY);
			}
			else {
				selectionTypeCombo.setText(PaceTreeNodeUtil.getDisplayNameFromSelectionType(selectionType));
			}
			levelGenDropDown.setText(levelGenType==null?EMPTY_STRING_ARRAY:levelGenType.toString());
			levelGenSpinner.setSelection(specMember.getLevelGenNumber());
			
			updateLevelGenControls(specMember);
			
		}
		else {
			selectionTypeCombo.setItems(new String[]{EMPTY_STRING_ARRAY});
			levelGenDropDown.setText(EMPTY_STRING_ARRAY);
			levelGenDropDown.setItems(new String[]{EMPTY_STRING_ARRAY});
			levelGenDropDown.setText(EMPTY_STRING_ARRAY);
			levelGenSpinner.setSelection(0);
		}
	}	

	protected void addNewSpecMember() {

		unboldDimensionTreeItems(dimension);

		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(dimension).isEnabled() ) {

			if( dimensionControlTreeMap.get(dimension).getSelection().length == 1 ) { // get selected tree item name
			
				TreeItem selectedTreeItem = dimensionControlTreeMap.get(dimension).getSelection()[0];
	
				PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
				if( ! selectedMember.getKey().equals(Constants.MEMBER_LISTS) ) {
					int levelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
					
					// get selected tree item name
					String selectedTreeItemName = selectedMember.getKey();
		
					// bold newly selected tree item
					boldTreeItem(dimension, selectedTreeItemName, true);
		
					if( levelNumber != 0 ) {
						selectionTypeCombo.setItems(selectionTypeList.toArray(new String[0]));
						levelGenDropDown.setItems(levelGenTypeList.toArray(new String[0]));
					}
					else {
						selectionTypeCombo.setItems(new String[]{PaceTreeNodeUtil.SELECTION_DISPLAY});
						levelGenDropDown.setItems(new String[]{EMPTY_STRING_ARRAY});
					}
					
					UserSelectionSpecificationMember specMember = 
						new UserSelectionSpecificationMember(
								dimension,
								selectedTreeItemName,
								selectionTypeCombo.getText().isEmpty()?PaceTreeNodeSelectionType.Selection:PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectionTypeCombo.getText()),
								levelGenDropDown.getText().isEmpty()?null:LevelGenerationType.valueOf(levelGenDropDown.getText()),
								levelGenSpinner.getSelection());
		
					PaceTreeNodeSelectionType selectionType = specMember.getSelectionType();
					LevelGenerationType levelGenType = specMember.getLevelGenType();
					if( selectionType == null || levelNumber == 0 ) {
						selectionTypeCombo.setText(PaceTreeNodeUtil.SELECTION_DISPLAY);
					}
					else {
						selectionTypeCombo.setText(PaceTreeNodeUtil.getDisplayNameFromSelectionType(selectionType));
					}
					levelGenDropDown.setText(levelGenType==null?EMPTY_STRING_ARRAY:levelGenType.toString());
					levelGenSpinner.setSelection(specMember.getLevelGenNumber());
					
					specMember.setSelectionType(PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectionTypeCombo.getText()));
					specMember.setLevelGenType(levelGenDropDown.getText().isEmpty()?null:LevelGenerationType.valueOf(levelGenDropDown.getText()));
					specMember.setLevelGenNumber(levelGenSpinner.getSelection());
					
					List securityMemberList = specificationMemberControlMap.get(dimension);
					
					// get security member map
					if (specMemberMap == null ) {
						specMemberMap = new HashMap<String,  java.util.List<UserSelectionSpecificationMember>>();
					}
				    // try to get security member
					java.util.List<UserSelectionSpecificationMember> specMemberArray = specMemberMap.get(dimension);
					Map<String,String> memberAliasListMap = this.dimSubMemberListTextMap.get(dimension);
				    //if the dimension has security members already, then add more into it
				    if( specMemberArray == null ) {
				    	specMemberArray = new ArrayList<UserSelectionSpecificationMember>();
				    }
				    if( ! specMemberArray.contains(specMember) ) {
				    	specMemberArray.add(specMember);
				    	specMemberMap.put(dimension, specMemberArray);
					    
				    	securityMemberList.add(selectedTreeItem.getText());
				    	memberAliasListMap.put(selectedTreeItem.getText(), specMember.toString());
				    }
				    
			    	securityMemberList.setSelection(new String[] {selectedTreeItem.getText()});
				    
					updateLevelGenControls(specMember);
				}

			}
		}	
	}

	/**
	 * Applies an alias table to the existing dimension tree.
	 * 
	 * @param dimensionTree
	 *            Dimension tree to have alias applied to
	 * @param aliasTableName
	 *            Alias table name to apply to tree
	 */
	protected void updateTreeWithAliases(Tree dimensionTree, List dimList, String aliasTableName) {

		logger.debug("Going to update tree " + dimensionTree
				+ " with alias table " + aliasTableName);
		
		Map<String, String> memberListTextMap = this.dimSubMemberListTextMap.get(dimension);
		String[] listStrs = dimList.getItems();

		for (TreeItem childItem : dimensionTree.getItems()) {

			applyAliasToTree(childItem, memberListTextMap, listStrs, aliasTableName);

		}
		
		for(int i = 0; i < listStrs.length; i++){
			if(memberListTextMap.containsKey(listStrs[i])){
				String temp = listStrs[i];
				listStrs[i] = memberListTextMap.get(listStrs[i]);
				memberListTextMap.remove(temp);
				memberListTextMap.put(listStrs[i], temp);
			} else if(listStrs[i].indexOf('(') != -1){
				int endNdx = listStrs[i].indexOf(')');
				if(listStrs[i].indexOf(',') != -1){
					endNdx = listStrs[i].indexOf(',');
				}
				String theText = listStrs[i].substring(listStrs[i].lastIndexOf('(') + 1, endNdx);
				String newText = memberListTextMap.get(theText);
				listStrs[i] = listStrs[i].substring(0, listStrs[i].lastIndexOf('(') + 1) +
								newText +
								listStrs[i].substring(endNdx, listStrs[i].length());
				memberListTextMap.remove(theText);
				memberListTextMap.put(newText, theText);				
			}
		}
		
		dimList.setItems(listStrs);
	}
	
	private void applyAliasToTree(TreeItem treeItem, Map<String, String> memListTextMap, String[] listStrs, String aliasTableName) {

		// if the tree item has children
		if (treeItem.getItems().length > 0) {

			// apply aliases to the children
			for (TreeItem childItem : treeItem.getItems()) {

				applyAliasToTree(childItem, memListTextMap, listStrs, aliasTableName);

			}

		}

		// get data member
		PafSimpleDimMember member = (PafSimpleDimMember) treeItem.getData();

		// if the alias table name is null, use key from member, else use alias
		if (aliasTableName == null) {

			treeItem.setText(member.getKey());

		} else {
			
			if (member.getPafSimpleDimMemberProps().getAliasKeys().size() > 0) {

				String[] aliasKeys = member.getPafSimpleDimMemberProps().getAliasKeys().toArray(new String[0]);
				
				String[] aliasValues = member.getPafSimpleDimMemberProps()
						.getAliasValues().toArray(new String[0]);

				for (int i = 0; i < aliasKeys.length; i++) {
					if (aliasKeys[i].equals(aliasTableName)) {

						treeItem.setText(aliasValues[i]);
					}
				}
			}
		}
		
		if(memListTextMap.get(member.getKey()) != null){
			memListTextMap.remove(member.getKey());
			memListTextMap.put(member.getKey(), treeItem.getText());
		}
	}

	public void enableControls() {
		
		List specMemberList = specificationMemberControlMap.get(dimension);
		
		selectionTypeGroup.setEnabled(this.multipleSelections);
		if(!this.multipleSelections){
			selectionTypeCombo.select(0);
		}
		
		
		if( specMemberList.getItemCount() > 0 ) {
			removeButton.setEnabled(true);
			clearButton.setEnabled(true);
			validateButton.setEnabled(true);
			selectionTypeCombo.setEnabled(true);
		}
		else {
			removeButton.setEnabled(false);
			clearButton.setEnabled(false);
			validateButton.setEnabled(false);
			selectionTypeCombo.setEnabled(false);								
			levelGenDropDown.setEnabled(false);
			levelGenSpinner.setEnabled(false);
		}
		
		if( specMemberList.getSelectionCount() > 0 ) {
			selectionTypeCombo.setEnabled(true);
		}
		else {
			selectionTypeCombo.setEnabled(false);								
			levelGenDropDown.setEnabled(false);
			levelGenSpinner.setEnabled(false);
		}
	}

	protected void onSelectedSpecListControlChange() {

		updateTreeSelectionsForSpecMember();
		updateSelectionTypeLevelGenCombos();
		enableControls();

	}
	
	private void updateLevelGenControls(UserSelectionSpecificationMember selectedSpecMem) {
		if( selectedSpecMem != null ) {
			int minLevelGenSpinnerLength = 0;
			int maxLevelGenSpinnerLength = maxTreeDepth;
			//get old max spinner values and log
			int oldMaxSpinnerValue = levelGenSpinner.getMaximum();
			int currentLevelGenValue = 0;
			PaceTreeNodeSelectionType selectionType = selectedSpecMem.getSelectionType();
			LevelGenerationType levelGenType = selectedSpecMem.getLevelGenType();
			levelGenDropDown.setText(levelGenType==null?EMPTY_STRING_ARRAY:levelGenType.toString());
			levelGenSpinner.setSelection(selectedSpecMem.getLevelGenNumber());
			
			switch (selectionType) {
				case Descendants:
				case IDescendants:
					if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
							levelGenDropDown.setText(LevelGenerationType.Bottom.toString());
					}
					levelGenDropDown.setEnabled(true);
					break;
					
				case DescLevel:
				case Level:
				case LevelGen:
					if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
						levelGenDropDown.setText(LevelGenerationType.Level.toString());
					}
					levelGenDropDown.setEnabled(true);
					
					minLevelGenSpinnerLength = 0;
					maxLevelGenSpinnerLength = maxTreeDepth;
					//get old max spinner values and log
					oldMaxSpinnerValue = levelGenSpinner.getMaximum();
				
					//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
					if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
						
						levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
						levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
						
					} else {
					
						levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
						levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
						
					}
					
					currentLevelGenValue = selectedSpecMem.getLevelGenNumber();
					
					if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
						
						currentLevelGenValue = levelGenSpinner.getMinimum();
						
					} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
						
						currentLevelGenValue = levelGenSpinner.getMaximum();
						
					}
					
					selectedSpecMem.setLevelGenNumber(currentLevelGenValue);
					levelGenSpinner.setSelection(currentLevelGenValue);
				break;
				
				case DescGen:
				case Generation:
					if( levelGenDropDown.getText() == EMPTY_STRING_ARRAY ) {
						levelGenDropDown.setText(LevelGenerationType.Generation.toString());
					}
					levelGenDropDown.setEnabled(true);
					
					minLevelGenSpinnerLength = 1;
					maxLevelGenSpinnerLength = maxTreeDepth + 1;
					
					//get old max spinner values and log
					oldMaxSpinnerValue = levelGenSpinner.getMaximum();
				
					//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
					if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
						
						levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
						levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
						
					} else {
					
						levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
						levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
						
					}
					
					currentLevelGenValue = selectedSpecMem.getLevelGenNumber();
					
					if (currentLevelGenValue < levelGenSpinner.getMinimum()	) {
						
						currentLevelGenValue = levelGenSpinner.getMinimum();
						
					} else if ( currentLevelGenValue > levelGenSpinner.getMaximum()) {
						
						currentLevelGenValue = levelGenSpinner.getMaximum();
						
					}
					
					selectedSpecMem.setLevelGenNumber(currentLevelGenValue);
					levelGenSpinner.setSelection(currentLevelGenValue);
					break;
					
				default:
					levelGenDropDown.setText(EMPTY_STRING_ARRAY);
					levelGenDropDown.setEnabled(false);
					break;
			}
			
		}
		else {
			levelGenDropDown.setEnabled(false);
			levelGenDropDown.setText(EMPTY_STRING_ARRAY);
		}
		
		if( levelGenDropDown.isEnabled() && levelGenDropDown.getText() != null && levelGenDropDown.getText().length() > 0) {
			LevelGenerationType levelGenType = LevelGenerationType.valueOf(levelGenDropDown.getText());
			switch (levelGenType) {
				case Bottom:
					levelGenSpinner.setSelection(0);
					levelGenSpinner.setEnabled(false);
					break;
				case Level:
				case Generation:
					levelGenSpinner.setEnabled(true);
					break;
			}
		}
		else {
			levelGenSpinner.setSelection(0);
			levelGenSpinner.setEnabled(false);
		}
	}
	
	protected void updateSpecMember() {
		List specMemberList = specificationMemberControlMap.get(dimension);
		if( specMemberList.getSelectionCount() == 1 ) {
			String selectedSpecMember = specMemberList.getSelection()[0];
			int index = specMemberList.getSelectionIndex();
			UserSelectionSpecificationMember specMember = new UserSelectionSpecificationMember(dimension,selectedSpecMember);
			
			java.util.List<UserSelectionSpecificationMember> specMemberArray = specMemberMap.get(dimension);
			
			if( specMemberArray.contains(specMember) )
				specMemberArray.remove(specMember);
			
			updateSelectionTypeForSpecMember(specMember, dimension );
	
			if( !specMemberArray.contains(specMember) ) {
				specMemberArray.add(specMember);
				specMemberMap.put(dimension, specMemberArray);
			}
				
			specMemberList.setItem(index, specMember.toString());
			
			setSelectedSpecDimension(dimension, specMember.getMember());
			
			boldTreeItem(dimension, specMember.getMember(), true);
		}
	}
	
	//update selection type options
	protected void updateTreeSelectionsForSpecMember() {
		
		List secMemList = specificationMemberControlMap.get(dimension);
		
		if ( secMemList.getSelectionCount() == 1 ) {
			
			UserSelectionSpecificationMember selectedSpecMem = 
				new UserSelectionSpecificationMember(dimension, secMemList.getSelection()[0]);
			
			setSelectedSpecDimension(dimension, selectedSpecMem.getMember());
			
			boldTreeItem(dimension, selectedSpecMem.getMember(), true);
		}
		
	}

	protected void updateSelectionTypeForSpecMember(UserSelectionSpecificationMember specMember, String dimension) {
		PaceTreeNodeSelectionType selectionType = PaceTreeNodeUtil.getSelectionTypeFromDisplayName(selectionTypeCombo.getText());
		specMember.setSelectionType(selectionType);
		specMember.setLevelGenType(levelGenDropDown.getText().isEmpty()?null:LevelGenerationType.valueOf(levelGenDropDown.getText()));
		if( selectionType.equals(PaceTreeNodeSelectionType.LevelGen) ) {
			if (levelGenDropDown.getText().equals(LevelGenerationType.Level.toString())) {
	
				specMember.setSelectionType(PaceTreeNodeSelectionType.Level);
	
			} else if (levelGenDropDown.getText().equals(LevelGenerationType.Generation.toString())) {
	
				specMember.setSelectionType(PaceTreeNodeSelectionType.Generation);
				specMember.setLevelGenType(LevelGenerationType.Generation);
			} else if (levelGenDropDown.getText().equals(LevelGenerationType.Bottom.toString())) {
	
				specMember.setSelectionType(PaceTreeNodeSelectionType.Level);
				specMember.setLevelGenType(LevelGenerationType.Bottom);
			}
		}
		specMember.setLevelGenNumber(levelGenSpinner.getSelection());
	}
	
	/**
	 * Populates the security form based ont the role
	 * 
	 * @param roleName
	 *            Role Name
	 */
	protected void populateSpecForm() {

		if( userSelectionSpecs != null && userSelectionSpecs.length > 0 ) {
			specMemberMap = new HashMap<String,  java.util.List<UserSelectionSpecificationMember>>();
		    // try to get security member
			java.util.List<UserSelectionSpecificationMember> specMemberArray = new ArrayList<UserSelectionSpecificationMember>();
			List securityMemberList = specificationMemberControlMap.get(dimension);
			Map<String,String> memberAliasListMap = this.dimSubMemberListTextMap.get(dimension);

		    for( String spec : userSelectionSpecs ) {
				UserSelectionSpecificationMember specMember = new UserSelectionSpecificationMember(dimension,spec);
			    if( ! specMemberArray.contains(specMember) ) {
			    	specMemberArray.add(specMember);
					if( ! Arrays.asList(securityMemberList.getItems()).contains(spec) ){
						securityMemberList.add(spec);
						if(spec.indexOf(',') != -1){
							String temp = spec.substring(spec.lastIndexOf('(') + 1, spec.indexOf(','));
							memberAliasListMap.put(temp, temp);
						} else if (spec.indexOf('(') != -1){
							String temp = spec.substring(spec.lastIndexOf('(') + 1, spec.indexOf(')'));
							memberAliasListMap.put(temp, temp);
						} else {
							memberAliasListMap.put(spec, spec);
						}
					}
			    }
		    }
		    
			specMemberMap.put(dimension, specMemberArray);
			
			securityMemberList.setSelection(0);
			

		}
	}
	
	/**
	 * Selects the tab with the specified string as text
	 * TTN-749
	 * @param tab
	 */
	public void setSelectedSpecDimension(String dim, String selectedSpec) {

		unboldAllDimensionTreeItems();
		
		if (selectedSpec == null ) {
			return;
		}
		Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dim);
		if (treeItemMap != null) {
			TreeItem securityMemberTreeItem = treeItemMap.get(selectedSpec);
			if (securityMemberTreeItem != null) {
				dimensionControlTreeMap.get(dim).setSelection(securityMemberTreeItem);
				boldTreeItem(securityMemberTreeItem, true);
			}
		}
	}
	
	private void unboldAllDimensionTreeItems() {

		if (dimensionMapOfTreeItemMap != null) {

			for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);

				if (treeItemMap != null) {

					for (TreeItem treeItem : treeItemMap.values()) {

						boldTreeItem(treeItem, false);

					}

				}

			}

		}

	}

	private MouseListener getTreeMouseListener() {

		if (treeMouseListener == null) {

			treeMouseListener = new MouseListener() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void mouseDoubleClick(MouseEvent e) {

					addNewSpecMember();
					enableControls();

				}

				@Override
				public void mouseDown(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseUp(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

			};

		}

		return treeMouseListener;
	}
	/**
	 * Unbolds tree item.
	 * 
	 * @param dimensionName
	 */
	private void unboldDimensionTreeItems(String dimensionName) {

		if (dimensionMapOfTreeItemMap != null) {

			// for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
					.get(dimensionName);

			if (treeItemMap != null) {

				for (TreeItem treeItem : treeItemMap.values()) {

					boldTreeItem(treeItem, false);

				}

			}

			// }

		}

	}
	
	/**
	 * The tree item should be bolded / unbolded based on bold flag. Then the
	 * parent item should be bolded / unbolded based on if any of it's children
	 * are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded
	 */

	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		Font font = treeItem.getFont();

		// get font data ar for tree item
		FontData[] fontDataAr = font.getFontData();

		if (fontDataAr.length > 0) {

			// get font data
			FontData fontData = fontDataAr[0];

			// if bold is true, set style to bold, else to none
			if (bold) {

				fontData.setStyle(SWT.BOLD);

				if (boldFont == null) {

					boldFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(boldFont);

			} else {

				fontData.setStyle(SWT.NONE);

				if (normalFont == null) {

					normalFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(normalFont);
			}

		}

	}

	/**
	 * Bolds tree item.
	 * 
	 * @param dimensionName
	 * @param treeItemName
	 * @param bold
	 */
	private void boldTreeItem(String dimensionName, String treeItemName,
			boolean bold) {

		if (dimensionMapOfTreeItemMap != null) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
					.get(dimensionName);

			if (treeItemMap != null) {

				TreeItem treeItem = treeItemMap.get(treeItemName);

				if (treeItem != null) {

					boldTreeItem(treeItem, bold);

				}

			}

		}

	}

	@Override
	protected void buttonPressed(int buttonId) {

		if ( buttonId == IDialogConstants.OK_ID) {

			List specMemberList = specificationMemberControlMap.get(dimension);
			// if the dimension tab is enabled
			if ( specMemberList.getItemCount() > 0 ) {

				if(	! validateAllSpecifications() ) {	
					return;
				}
				
			}
			userSelectionSpecs = specMemberList.getItems();
		}
		
		super.buttonPressed(buttonId);
	}

	public String[] getUserSelectionSpecs() {
		
		return userSelectionSpecs;
	
	}
	
	public void setUserSelectionSpecs(String[] specs) {
		
		userSelectionSpecs = specs;
	
	}
	
	protected boolean validateAllSpecifications() {
		
		unboldDimensionTreeItems(dimension);
		List specMemberList = specificationMemberControlMap.get(dimension);
		// if the dimension tab is enabled
		if ( specMemberList.getItemCount() > 0 ) {
			
			//exclude UOW_ROOT from validation
			java.util.List<String> specList = new ArrayList<String>(Arrays.asList(specMemberList.getItems()));
			specList = removeUOWTreeItemFromList(specList);
			
			if( specList.size() > 0 ) {
				//validate against dim cache first
				Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimension);
				if (dimTreeItemMap != null) {
					Set<InvalidSpecificationMember> invalidMemberSet = new LinkedHashSet<InvalidSpecificationMember>();
					// verify security member is still valid member in outline
					for( String specMem : specMemberList.getItems() ) {
						//TODO
						UserSelectionSpecificationMember securityMember = new UserSelectionSpecificationMember(dimension, specMem);
						if (!dimTreeItemMap.containsKey(securityMember.getMember())) {
							InvalidSpecificationMember invalidSpecMember = new InvalidSpecificationMember();
							invalidSpecMember.setDimensionName(securityMember.getDimensionName());
							invalidSpecMember.setMemberName(securityMember.getMember());
							invalidMemberSet.add(invalidSpecMember);
						}
					}
					if( invalidMemberSet.size() > 0 ) {
		
						Shell shell = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell();
		
						InvalidSpecMemberErrorDialog invalidSpecMemberErrorDialog = new InvalidSpecMemberErrorDialog(
								shell, invalidMemberSet);
						invalidSpecMemberErrorDialog.open();
		
						InvalidSpecificationMemberView invalidSpecMemberView;
						try {
							invalidSpecMemberView = (InvalidSpecificationMemberView) PlatformUI
									.getWorkbench().getActiveWorkbenchWindow()
									.getActivePage().showView(
											InvalidSpecificationMemberView.ID);
							invalidSpecMemberView.setInvalidSpecMemberSet(invalidMemberSet);
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return false;
					}
				}
				
				//validate against server then
				ValidationResponse valResp  = validateAllSpecifications( dimension,specList);
				if( valResp != null ) {
					if( ! valResp.isSuccess() ) { //validation failed
						java.util.List<String> errorList = valResp.getValidationErrors();
						if( errorList.size() > 0 ) {
							Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
							ListErrorMessagesDialog userSecurityValidationDialog = new ListErrorMessagesDialog(
									shell, 
									errorList,
									"Invalid User Security Specification",
									"The followings are invalid specifications. Please re-configure.");
							userSecurityValidationDialog.open();
							return false;
						}
					}
					else { //validation succeeded
						GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "The specifications for '" + dimension + "' have been validated.", MessageDialog.INFORMATION);
						return true;
					}
				}
				else { //service call failed
					GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "There is an issue with server. The specifications for '" +  dimension + "' have NOT been validated.", MessageDialog.INFORMATION);
					return false;
				}
			}
		}
		return true;
	}

	private java.util.List<String> removeUOWTreeItemFromList(
			java.util.List<String> specList) {
		for( Iterator<String> it = specList.iterator(); it.hasNext(); ) {
			String spec = it.next();
			if( spec.contains(PafBaseConstants.UOW_ROOT) || spec.contains(PafBaseConstants.MEMBERLIST_TOKEN) ) {
				it.remove();
			}
		}
		return specList;
	}

	protected ValidationResponse validateAllSpecifications(String dimension,java.util.List<String> expList) {
		String serverName = null;
		try {
			PafServer server = PafProjectUtil.getProjectServer(project);
			serverName = server.getName();
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName),false);
			String url = server.getCompleteWSDLService();
			
			if( SecurityManager.isAuthenticated(url) ) {
				ValidateUserSecurityRequest valUsrSecReq = new ValidateUserSecurityRequest();
				ServerSession serverSession = SecurityManager.getSession(url);
				valUsrSecReq.setClientId(serverSession.getClientId());
				valUsrSecReq.setSessionToken(serverSession.getSecurityToken());
				com.pace.server.client.PafDimSpec clientDimSpec = new com.pace.server.client.PafDimSpec();
				clientDimSpec.setDimension(dimension);
				clientDimSpec.getExpressionList().addAll(expList);
				valUsrSecReq.getSecuritySpecs().add(clientDimSpec);
				PafService service = WebServicesUtil.getPafService(url);
				if( service != null ) {
					return service.validateUserSecurity(valUsrSecReq);
				}
			}
		} catch (PafNotAuthorizedSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafServerNotFound e) {
			logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
			e.printStackTrace();
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch( RuntimeException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	protected void removeAllSpecification() {
		unboldDimensionTreeItems(dimension);
		List specMemberList = specificationMemberControlMap.get(dimension);
		
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(dimension).isEnabled() && specMemberList.getItemCount() > 0 ) {
			specMemberList.removeAll();
			specMemberMap.remove(dimension);
		}
		
		updateSelectionTypeLevelGenCombos();
	}
	
	protected void removeSelectedSpecification() {
		unboldDimensionTreeItems(dimension);
		List specMemberList = specificationMemberControlMap.get(dimension);
		
	    // try to get security member
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(dimension).isEnabled() && specMemberList.getSelectionCount() > 0 ) {
			String[] secList = specMemberList.getSelection();
			for( String sec : secList ) {
				specMemberList.remove(sec);
				removeSelectedSpecification(dimension, sec);
			}
		}
		
		updateSelectionTypeLevelGenCombos();
	}

	protected void removeSelectedSpecification( String dimension, String speicfication ) {
		if (specMemberMap != null && specMemberMap.size() > 0 ) {
			java.util.List<UserSelectionSpecificationMember> specArray = specMemberMap.get(dimension);
			if( specArray != null  ) {
				UserSelectionSpecificationMember specMem = new UserSelectionSpecificationMember(dimension, speicfication);
				  for( Iterator< UserSelectionSpecificationMember > it = specArray.iterator(); it.hasNext() ; ) {
					  UserSelectionSpecificationMember spec = it.next(); 
			           if( spec.equals(specMem) )
			        	   it.remove();
				  }
			}
		}
	}
}

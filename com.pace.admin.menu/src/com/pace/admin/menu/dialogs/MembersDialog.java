/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.menu.model.PafMember;

public class MembersDialog extends Dialog {

	private Button leftButton;
	private Button rightButton;
	private Button upButton_1;
	private Button upButton;
	private List list;
	private Tree tree;
	private Tree dimensionTree;
	private PafMember currentMember;
		
	public MembersDialog(Shell parentShell, PafMember currentMember, Tree dimensionTree) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.currentMember = currentMember;
		this.dimensionTree = dimensionTree;
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		tree = new Tree(container, SWT.BORDER);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 250);
		formData.right = new FormAttachment(0, 225);
		formData.top = new FormAttachment(0, 50);
		formData.left = new FormAttachment(0, 60);
		tree.setLayoutData(formData);
		
		createTreeItems();

		list = new List(container, SWT.BORDER);
		final FormData formData_2 = new FormData();
		formData_2.top = new FormAttachment(0, 50);
		formData_2.bottom = new FormAttachment(0, 250);
		formData_2.right = new FormAttachment(0, 428);
		formData_2.left = new FormAttachment(0, 308);
		list.setLayoutData(formData_2);

		upButton = new Button(container, SWT.ARROW);
		final FormData formData_22_1 = new FormData();
		formData_22_1.bottom = new FormAttachment(0, 146);
		formData_22_1.top = new FormAttachment(0, 128);
		formData_22_1.right = new FormAttachment(0, 470);
		formData_22_1.left = new FormAttachment(0, 450);
		upButton.setLayoutData(formData_22_1);
		upButton.setEnabled(false);
		upButton.setText("button");

		upButton_1 = new Button(container, SWT.ARROW | SWT.DOWN);
		final FormData formData_22_1_1 = new FormData();
		formData_22_1_1.top = new FormAttachment(0, 152);
		formData_22_1_1.left = new FormAttachment(0, 450);
		formData_22_1_1.bottom = new FormAttachment(0, 170);
		formData_22_1_1.right = new FormAttachment(0, 470);
		upButton_1.setLayoutData(formData_22_1_1);
		upButton_1.setEnabled(false);
		upButton_1.setText("button");

		rightButton = new Button(container, SWT.ARROW | SWT.RIGHT);
		final FormData formData_22 = new FormData();
		formData_22.bottom = new FormAttachment(0, 147);
		formData_22.top = new FormAttachment(0, 129);
		formData_22.right = new FormAttachment(0, 281);
		formData_22.left = new FormAttachment(0, 255);
		rightButton.setLayoutData(formData_22);
		rightButton.setEnabled(false);
		rightButton.setText("button");

		leftButton = new Button(container, SWT.ARROW | SWT.LEFT);
		final FormData formData_2_1 = new FormData();
		formData_2_1.top = new FormAttachment(0, 152);
		formData_2_1.left = new FormAttachment(0, 255);
		formData_2_1.bottom = new FormAttachment(0, 170);
		formData_2_1.right = new FormAttachment(0, 281);
		leftButton.setLayoutData(formData_2_1);
		leftButton.setEnabled(false);
		leftButton.setText("button");
		//
		return container;
	}

	private void createTreeItems() {
		
		TreeItem[] children = dimensionTree.getItems();
				
		for (TreeItem child : children) {
		
			addTreeNode(null, child);
			
		}
		
	}
	
	private void addTreeNode(TreeItem parent, TreeItem currentItem) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(currentItem.getText());

		if (currentItem.getItems().length > 0 ) {

			TreeItem[] treeItems = currentItem.getItems();

			for (TreeItem treeItem : treeItems) {
				
				addTreeNode(newItem, treeItem);

			}
		}
		
		if (parent == null) {
			
			newItem.setExpanded(true);
			
		}
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(500, 375);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Member Function Selection");
	}

	/*private void addTreeNode(TreeItem parent,
			PafSimpleMember member) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		if (member.getChildKeys() != null) {

			String[] children = member.getChildKeys();

			for (String child : children) {

				PafSimpleMember childMember = (PafSimpleMember) treeHashMap
						.get(child);

				addTreeNode(newItem, childMember);

			}
		}

		if (newItem.getParentItem() == null) {

			for (String additionalItem : additionalDimItems) {

				TreeItem additionalTreeItem = new TreeItem(newItem, SWT.NONE);
				additionalTreeItem.setText(additionalItem);

			}
		}

		// if version dim, add additional members. eventually make this more
		// dynamic
		if (newItem.getText().equals(versionName)) {
			for (String additionalItem : additionalVersionMembers) {

				TreeItem additionalTreeItem = new TreeItem(newItem, SWT.NONE);
				additionalTreeItem.setText(additionalItem);

			}
		}

	}
	
	private void createTreeModel(Tree tree) {
		
		addTreeNode(null, rootNode);		
		
	}
	*/
}

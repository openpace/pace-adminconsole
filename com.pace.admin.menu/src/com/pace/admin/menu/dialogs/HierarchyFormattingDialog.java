/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.model.managers.GlobalStyleModelManager;
import com.pace.admin.global.model.managers.HierarchyFormattingModelManager;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.util.TreeUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.format.GenFormat;
import com.pace.base.format.LevelFormat;
import com.pace.base.view.Dimension;
import com.pace.base.view.HierarchyFormat;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

/**
 * TODO: replace with javadoc
 * 
 * @version x.xx
 * @author Jason Milliron
 * 
 */
public class HierarchyFormattingDialog extends Dialog {

	private static final Logger logger = Logger.getLogger(HierarchyFormattingDialog.class);
	
	private Combo globalStyleCombo;

	private Text levelGenText;

	private Text dimensionText;

	private Combo selectDimensionCombo;

	private Tree dimensionTree;

	private Text nameText;

	private HierarchyFormat currentHiearchyFormat = null;

	private TreeMap<String, HierarchyFormat> modelMap = null;
		
	// used when buttons are clicked
	private enum ButtonId {
		New, Add, Delete, Cancel;
	}

	// form widgets

	// left list widget
	private List list;

	// Buttons
	private Button newButton;

	private Button deleteButton;

	private Button addButton;

	private Button cancelButton;

	private HierarchyFormattingModelManager modelManager = null;

	private GlobalStyleModelManager globalStyleModelManager = null;

	private Map<String, ArrayList<String>> treeCache = null;

	private IProject project;

	// used to see if form should be changed when form item changes
	private boolean cachingEnabled = false;

	private String[] cachedDimensions = null;

	private String[] cachedAttributeDimensions = null;

	private PafMdbProps cachedPafMdbProps = null;

	/**
	 * @param parentShell
	 *            parentShell
	 * @throws ServerNotRunningException 
	 */
	public HierarchyFormattingDialog(Shell parentShell, IProject project) throws ServerNotRunningException, Exception {

		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;

		modelManager = new HierarchyFormattingModelManager(project);

		// convert into tree map

		if (modelManager.size() > 0) {

			modelMap = new TreeMap<String, HierarchyFormat>(
					String.CASE_INSENSITIVE_ORDER);
			for (String key : modelManager.getKeys()) {
				modelMap.put(key, (HierarchyFormat) modelManager.getItem(key));
			}

		} else {
			modelMap = new TreeMap<String, HierarchyFormat>(
					String.CASE_INSENSITIVE_ORDER);
		}

		globalStyleModelManager = new GlobalStyleModelManager(project);

		try {
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(PafProjectUtil
					.getProjectServer(project), PafProjectUtil
					.getApplicationName(project));
		} catch (ServerNotRunningException sne ) {
			
			throw sne;
			
		} catch (Exception e) {
			logger
					.error("Cannot load cached dimensions, please reload your dimension cache.");
			throw e;
		}

	}

	/**
	 * @param parent
	 *            Parent Composite used to add the widgets to
	 * @returns Control used to paint the scrren
	 */
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		list = new List(container, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 289);
		formData.right = new FormAttachment(0, 180);
		formData.top = new FormAttachment(0, 44);
		formData.left = new FormAttachment(0, 25);
		list.setLayoutData(formData);

		final Label listLabel = new Label(container, SWT.NONE);
		final FormData formData_1 = new FormData();
		formData_1.right = new FormAttachment(list, 0, SWT.RIGHT);
		formData_1.left = new FormAttachment(0, 25);
		formData_1.bottom = new FormAttachment(0, 39);
		formData_1.top = new FormAttachment(0, 24);
		listLabel.setLayoutData(formData_1);
		listLabel.setText("Hierarchy Formatting List:");

		newButton = new Button(container, SWT.NONE);
		final FormData formData_2 = new FormData();
		formData_2.bottom = new FormAttachment(0, 315);
		formData_2.right = new FormAttachment(0, 75);
		formData_2.top = new FormAttachment(list, 5, SWT.BOTTOM);
		formData_2.left = new FormAttachment(0, 25);
		newButton.setLayoutData(formData_2);
		newButton.setText("New");

		deleteButton = new Button(container, SWT.NONE);
		final FormData formData_2_1 = new FormData();
		formData_2_1.left = new FormAttachment(0, 80);
		formData_2_1.bottom = new FormAttachment(0, 315);
		formData_2_1.top = new FormAttachment(0, 294);
		formData_2_1.right = new FormAttachment(0, 129);
		deleteButton.setLayoutData(formData_2_1);
		deleteButton.setText("Delete");

		addButton = new Button(container, SWT.NONE);
		addButton.setVisible(false);
		final FormData formData_2_2 = new FormData();
		formData_2_2.right = new FormAttachment(0, 417);
		formData_2_2.bottom = new FormAttachment(0, 390);
		formData_2_2.top = new FormAttachment(0, 369);
		formData_2_2.left = new FormAttachment(0, 367);
		addButton.setLayoutData(formData_2_2);
		addButton.setText("Add");

		cancelButton = new Button(container, SWT.NONE);
		cancelButton.setVisible(false);
		final FormData formData_2_2_1 = new FormData();
		formData_2_2_1.top = new FormAttachment(0, 369);
		formData_2_2_1.left = new FormAttachment(0, 422);
		formData_2_2_1.bottom = new FormAttachment(0, 390);
		formData_2_2_1.right = new FormAttachment(0, 468);
		cancelButton.setLayoutData(formData_2_2_1);
		cancelButton.setText("Cancel");

		final Group generationFormatGroup = new Group(container, SWT.NONE);
		generationFormatGroup.setText("Hierarchy Format");
		final FormData formData_5 = new FormData();
		formData_5.bottom = new FormAttachment(0, 359);
		formData_5.right = new FormAttachment(0, 615);
		formData_5.top = new FormAttachment(0, 38);
		formData_5.left = new FormAttachment(0, 210);
		generationFormatGroup.setLayoutData(formData_5);
		generationFormatGroup.setLayout(new FormLayout());

		final Label nameLabel = new Label(generationFormatGroup, SWT.NONE);
		final FormData formData_3 = new FormData();
		formData_3.bottom = new FormAttachment(0, 20);
		formData_3.top = new FormAttachment(0, 5);
		formData_3.right = new FormAttachment(0, 160);
		formData_3.left = new FormAttachment(0, 120);
		nameLabel.setLayoutData(formData_3);
		nameLabel.setAlignment(SWT.RIGHT);
		nameLabel.setText("Name:");

		nameText = new Text(generationFormatGroup, SWT.BORDER);
		final FormData formData_4 = new FormData();
		formData_4.left = new FormAttachment(0, 170);
		formData_4.bottom = new FormAttachment(nameLabel, 19, SWT.TOP);
		formData_4.top = new FormAttachment(nameLabel, 0, SWT.TOP);
		formData_4.right = new FormAttachment(0, 290);
		nameText.setLayoutData(formData_4);
		nameText.setEnabled(false);

		final Label dimensionsLabel = new Label(generationFormatGroup, SWT.NONE);
		final FormData formData_8 = new FormData();
		formData_8.left = new FormAttachment(0, 25);
		formData_8.right = new FormAttachment(0, 130);
		formData_8.top = new FormAttachment(0, 45);
		dimensionsLabel.setLayoutData(formData_8);
		dimensionsLabel.setText("Select Dimension:");

		dimensionTree = new Tree(generationFormatGroup, SWT.BORDER);

		final FormData formData_9 = new FormData();
		formData_9.right = new FormAttachment(0, 200);
		formData_9.left = new FormAttachment(nameText, -145, SWT.LEFT);
		formData_9.bottom = new FormAttachment(0, 290);
		formData_9.top = new FormAttachment(0, 90);
		dimensionTree.setLayoutData(formData_9);

		selectDimensionCombo = new Combo(generationFormatGroup, SWT.READ_ONLY);
		selectDimensionCombo.setVisibleItemCount(7);
		final FormData formData_7 = new FormData();
		formData_7.right = new FormAttachment(0, 140);
		formData_7.left = new FormAttachment(0, 25);
		formData_7.top = new FormAttachment(0, 61);
		selectDimensionCombo.setLayoutData(formData_7);

		final Group dimensionLevelMappingiGroup = new Group(
				generationFormatGroup, SWT.NONE);
		dimensionLevelMappingiGroup.setText("Dimension Mapping");
		final FormData formData_10 = new FormData();
		formData_10.top = new FormAttachment(0, 105);
		formData_10.bottom = new FormAttachment(0, 270);
		formData_10.right = new FormAttachment(0, 381);
		formData_10.left = new FormAttachment(0, 216);
		dimensionLevelMappingiGroup.setLayoutData(formData_10);
		dimensionLevelMappingiGroup.setLayout(new FormLayout());

		final Label dimensionLabel = new Label(dimensionLevelMappingiGroup,
				SWT.NONE);
		final FormData formData_10_1 = new FormData();
		formData_10_1.bottom = new FormAttachment(0, 27);
		formData_10_1.top = new FormAttachment(0, 12);
		formData_10_1.right = new FormAttachment(0, 72);
		formData_10_1.left = new FormAttachment(0, 10);
		dimensionLabel.setLayoutData(formData_10_1);
		dimensionLabel.setText("Dimension:");

		dimensionText = new Text(dimensionLevelMappingiGroup, SWT.READ_ONLY
				| SWT.BORDER);
		final FormData formData_13 = new FormData();
		formData_13.top = new FormAttachment(0, 30);
		formData_13.bottom = new FormAttachment(0, 49);
		formData_13.right = new FormAttachment(0, 108);
		formData_13.left = new FormAttachment(0, 10);
		dimensionText.setLayoutData(formData_13);
		dimensionText.setEditable(false);

		final Label dimensionLabel_1 = new Label(dimensionLevelMappingiGroup,
				SWT.NONE);
		final FormData formData_10_1_1 = new FormData();
		formData_10_1_1.top = new FormAttachment(0, 52);
		formData_10_1_1.bottom = new FormAttachment(0, 67);
		formData_10_1_1.left = new FormAttachment(0, 10);
		dimensionLabel_1.setLayoutData(formData_10_1_1);
		dimensionLabel_1.setText("Level (L) / Generation (G):");

		levelGenText = new Text(dimensionLevelMappingiGroup, SWT.READ_ONLY
				| SWT.BORDER);
		final FormData formData_14 = new FormData();
		formData_14.top = new FormAttachment(0, 70);
		formData_14.bottom = new FormAttachment(0, 89);
		formData_14.right = new FormAttachment(0, 54);
		formData_14.left = new FormAttachment(0, 10);
		levelGenText.setLayoutData(formData_14);
		levelGenText.setEditable(false);

		final Label globalStyleLabel = new Label(dimensionLevelMappingiGroup,
				SWT.WRAP);
		final FormData formData_12 = new FormData();
		formData_12.top = new FormAttachment(0, 98);
		formData_12.bottom = new FormAttachment(0, 113);
		formData_12.right = new FormAttachment(0, 104);
		formData_12.left = new FormAttachment(0, 10);
		globalStyleLabel.setLayoutData(formData_12);
		globalStyleLabel.setText("Global Style:");

		globalStyleCombo = new Combo(dimensionLevelMappingiGroup, SWT.READ_ONLY);
		formData_10_1_1.right = new FormAttachment(globalStyleCombo, 0,
				SWT.RIGHT);
		final FormData formData_11 = new FormData();
		formData_11.top = new FormAttachment(0, 115);
		formData_11.bottom = new FormAttachment(0, 136);
		formData_11.right = new FormAttachment(0, 142);
		formData_11.left = new FormAttachment(0, 10);
		globalStyleCombo.setLayoutData(formData_11);

		final Composite blankComposite = new Composite(container, SWT.NONE);
		blankComposite.setVisible(false);
		final FormData formData_6 = new FormData();
		formData_6.top = new FormAttachment(0, 340);
		formData_6.left = new FormAttachment(0, 30);
		blankComposite.setLayoutData(formData_6);
		blankComposite.setLayout(new FormLayout());

		// initially populate the list items.
		refreshList();

		try {

			initializeMetaData(blankComposite);

		} catch (Exception e) {

			throw new RuntimeException(e);

		}

		// set up dialog
		initializeDialog();

		// set up listeners
		setupListeners();

		// select initial dimension selection
		if (selectDimensionCombo.getItemCount() > 0) {
			selectDimensionCombo.select(0);
		}

		return container;
	}

	protected void dimensionTreeItemSelected(TreeItem[] selectedTreeItems) {

		// if number of selected tree itmes is greater than 0
		if (selectedTreeItems.length > 0) {

			// get selected item
			TreeItem selectedTreeItem = selectedTreeItems[0];

			// get the tree item level
			int treeItemLevel = TreeUtil.getTreeItemLevel(selectedTreeItem);

			// if not 0, populate the Dimension Level map form with tree item
			// and enable
			if (treeItemLevel != 0) {

				populateDimensionLevelGenMapping(selectedTreeItem);
				enableDimensionLevelGenForm(true);

				// else clear dim form and disable form
			} else {

				clearDimensionForm();
				enableDimensionLevelGenForm(false);

			}

		}

	}

	private void clearDimensionForm() {

		cachingEnabled = false;

		dimensionText.setText("");
		levelGenText.setText("");
		globalStyleCombo.setText("");

		cachingEnabled = true;

	}

	private void enableDimensionLevelGenForm(boolean enable) {

		globalStyleCombo.setEnabled(enable);

	}

	private void populateDimensionLevelGenMapping(TreeItem selectedTreeItem) {

		if (selectedTreeItem != null) {

			clearDimensionForm();

			// get the dimension name from tree item. gets parent of tree
			String dimensionName = TreeUtil.getParentOfTree(selectedTreeItem)
					.getText();

			// get the level name
			String levelGenName = selectedTreeItem.getText();

			// populate fields with values
			dimensionText.setText(dimensionName);
			levelGenText.setText(levelGenName);

			// get the dimension object from current model.
			Dimension dimension = currentHiearchyFormat
					.getDimension(dimensionName);

			// if dimension exist
			if (dimension != null) {

				String ident = parseIdentFromString(levelGenName);

				// try to get the level format name
				String levelGenFormatName = null;

				if (ident.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)) {

					levelGenFormatName = dimension
							.getLevelFormatName(parseIntFromString(levelGenName));

				} else if (ident
						.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {
					levelGenFormatName = dimension
							.getGenFormatName(parseIntFromString(levelGenName));
				}

				// if level format name exist
				if (levelGenFormatName != null) {

					// select the current global style
					selectGlobalStyle(levelGenFormatName);

				}

			}

		}

	}

	private boolean selectGlobalStyle(String levelFormatName) {

		boolean found = false;

		int indexOfLevelFormatName = globalStyleCombo.indexOf(levelFormatName);

		if (indexOfLevelFormatName >= 0) {

			globalStyleCombo.setText(levelFormatName);
			found = true;

		} else {

			logger.error("Global Style: " + levelFormatName
					+ " not found.  Selecting blank.");
			globalStyleCombo.select(0);

		}

		return found;

	}

	private void initializeMetaData(Composite composite) throws Exception {

		//TODO: MAYBE pull this from the cachedPafMdbProps instead of project file?
		
		// read in the dimensions from file
		String[] dimensions = PafProjectUtil.getProjectDimensions(project);
		
		//TODO: MAYBE just use attribute dims and not just the cached ones.
		
		// add attributes to generation formatting.
		java.util.List<String> attributeDimensionsList = cachedPafMdbProps.getCachedAttributeDims();
		
		// build an array list of all the dimensions(base and attributes)
		ArrayList<String> allDims = new ArrayList<String>();

		// if dimensions and attributes don't exists
		if (dimensions == null && attributeDimensionsList.size() == 0) {
			throw new Exception("No dimensions exist.");
		}

		cachedDimensions = dimensions;
		cachedAttributeDimensions = attributeDimensionsList.toArray(new String[0]);

		ArrayList<String> dimensionComboItems = new ArrayList<String>();

		// Add the ALL to top dimensions array list
		dimensionComboItems.add("ALL");

		// add dimensions from file to array list
		for (String comboItem : dimensions) {
			dimensionComboItems.add(comboItem);
			allDims.add(comboItem);
		}
		// add attribute dimensions from file to array list
		for (String comboItem : attributeDimensionsList) {
			dimensionComboItems.add(comboItem);
			allDims.add(comboItem);
		}

		// convert into an arraylist and set items of combo box
		selectDimensionCombo.setItems(dimensionComboItems
				.toArray(new String[0]));

		// create a new tree cache, <Dimension> ArrayList<Level names>
		treeCache = new HashMap<String, ArrayList<String>>();

		// for each dimension in list of dimensions
		for (String dimension : allDims) {// dimensions) {

			// used to hold level numbers per dimension
			ArrayList<String> levelGenNumbers = new ArrayList<String>();

			int dimensionMaxLevel = 0;
			int dimensionMaxGeneration = 1;

			PafSimpleDimTree simpleTree = null;
			try {
				simpleTree = (PafSimpleDimTree) DimensionTreeUtility
					.getDimensionTree(project, PafProjectUtil
							.getApplicationName(project), dimension);
			} catch (Exception e1) {
														
				throw new Exception("Server or application may not be running. Or no dimension cache downloaded.");
			}
			// get the member objects from simple tree
			java.util.List<PafSimpleDimMember> memberObjectsList = simpleTree.getMemberObjects();

			PafApplicationDef pafAppDef = PafApplicationUtil.getPafApp(this.project);
			
			//by default, search for dimension name member.
			String dimensionMemberToSearch = dimension;
			
			//if measure dim and measure root is different than measure dim name, set dimension member to measure root
			if ( dimension.equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureDim()) &&
					pafAppDef.getMdbDef().getMeasureRoot() != null && ! pafAppDef.getMdbDef().getMeasureDim().equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureRoot())) {
				
				dimensionMemberToSearch = pafAppDef.getMdbDef().getMeasureRoot();
				
			}
			
			// for each member object, find the dimension max level
			for (PafSimpleDimMember member : memberObjectsList) {					
				
				if (member.getKey().equalsIgnoreCase(dimensionMemberToSearch)) {

					dimensionMaxLevel = member.getPafSimpleDimMemberProps()
							.getLevelNumber();
					dimensionMaxGeneration = dimensionMaxLevel + 1;
					
					break;
				}
			}
				
			
			
			// loop throught all levels and add level with tag to level numbers
			// array list
			for (int i = 0; i <= dimensionMaxLevel; i++) {

				levelGenNumbers.add(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT + i);

			}

			for (int i = 1; i <= dimensionMaxGeneration; i++) {

				levelGenNumbers
						.add(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT + i);

			}

			// add to the tree cache
			treeCache.put(dimension, levelGenNumbers);
		}

		// populate global styles

		String[] globalStyleNames = globalStyleModelManager.getKeys();

		if (globalStyleNames != null) {

			globalStyleCombo.setItems(globalStyleNames);
			globalStyleCombo.add("", 0);

		}

	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(638, 506);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Hierarchy Formatting Wizard");
	}

	/**
	 * Initials the form.
	 */
	private void initializeDialog() {

		// if model map contains items
		if (modelMap.size() > 0) {
			list.setSelection(0);
			populateGenerationFormatForm(list.getSelection()[0]);
			deleteButton.setEnabled(true);
		} else {
			list.deselectAll();
			list.setEnabled(true);
			clearForm();
			enableDimensionForm(false);
			deleteButton.setEnabled(false);
		}

		enableDimensionLevelGenForm(false);

	}

	/**
	 * Populates the form from the model map using the key param
	 * 
	 * @param key
	 *            used to get object from the model map
	 */
	private void populateGenerationFormatForm(String key) {

		// if the model map has the key, populate the form with it
		if (modelMap.containsKey(key)) {

			enablePrimaryKey(false);

			enableDimensionForm(true);

			enableDimensionLevelGenForm(false);

			cachingEnabled = false;

			HierarchyFormat model = modelMap.get(key);

			nameText.setText(model.getName());

			currentHiearchyFormat = model;

			cachingEnabled = true;

		}

	}

	/**
	 * Sets up all the listeners
	 * 
	 */
	private void setupListeners() {

		// setup the list listener
		setupListListener();

		// setup all form listeners
		setupFormListeners();

		// setup all button listeners
		setupButtonListeners();

	}

	/**
	 * Sets up the listener for the list box
	 * 
	 */
	private void setupListListener() {

		list.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				// if a selection exist
				if (list.getSelectionCount() > 0) {

					logger.debug("List Selection: " + list.getSelection()[0]);

					// save cached model if one exist
					saveCachedModel();

					// clear and popualte form from selection
					clearForm();
					populateGenerationFormatForm(list.getSelection()[0]);

					resetDimensionTree();

				}

			}

		});

	}

	protected void resetDimensionTree() {

		selectDimensionCombo.deselectAll();
		selectDimensionCombo.setText("ALL");

	}

	/**
	 * Sets up the listeners for the form widgets
	 * 
	 */
	private void setupFormListeners() {

		nameText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				// get and trim key
				String key = nameText.getText().trim();

				// if key already in model map, disable add button
				if (modelMap.containsKey(key)) {

					addButton.setEnabled(false);

				} else {

					addButton.setEnabled(true);

				}

			}

		});

		dimensionTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				dimensionTreeItemSelected(dimensionTree.getSelection());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

				TreeUtil.doubleClickSelectionAction(dimensionTree);

			}
		});

		selectDimensionCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				logger.info("Loading Dimension Tree with Dimension: "
						+ selectDimensionCombo.getText());
				newDimensionSelectedAction(selectDimensionCombo.getText());

			}

		});

		globalStyleCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				globalStyleChanged();

			}

		});

	}

	private TreeItem getSelectedTreeItem() {

		if (dimensionTree.getSelectionCount() > 0) {

			return dimensionTree.getSelection()[0];

		}

		return null;

	}

	protected void globalStyleChanged() {

		// if caching is enabled
		if (cachingEnabled) {

			// get selected tree item
			TreeItem selectedLevelGen = getSelectedTreeItem();

			// get dimension name, level name and global style from text box
			String dimensionName = dimensionText.getText();
			String levelGenMember = levelGenText.getText();
			String globalStyleName = globalStyleCombo.getText();

			// get dimension from current model
			Dimension dimension = currentHiearchyFormat
					.getDimension(dimensionName);

			String ident = parseIdentFromString(levelGenMember);

			if (ident.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)) {

				// get level from level name
				int level = parseIntFromString(levelGenMember);

				// if dimension is not null
				if (dimension != null) {

					// get level format from dimension
					LevelFormat levelFormat = dimension.getLevelFormat(level);

					// if level format is null and global style is blank, create
					// a
					// new level format
					if (levelFormat == null && !globalStyleName.equals("")) {

						levelFormat = new LevelFormat(level, globalStyleName);

						// if level format is not null and global style name is
						// blank, remove from dimension and set to null
					} else if (globalStyleName.equals("")) {

						// remove level format from dimension
						dimension.removeLevelFormat(levelFormat);

						// unbold tree item
						boldTreeItem(selectedLevelGen, false);

						levelFormat = null;

						
						if (dimension.getNumberOfHierFormats() == 0) {
							currentHiearchyFormat.removeDimension(dimension);
							dimension = null;
						}

						// all else fails, set formatName to new format name
					} else {

						levelFormat.setFormatName(globalStyleName);

					}

					// if level format is not null, add to dimension (replaces
					// existing one)
					if (levelFormat != null) {

						boldTreeItem(selectedLevelGen, true);

						dimension.addLevelFormat(levelFormat);
					}

					// if dimension is null
				} else {

					// if global style name is not blank
					if (!globalStyleName.equals("")) {

						// create a new dimension
						dimension = new Dimension();

						// set dimension name
						dimension.setName(dimensionName);

						// create new level format
						LevelFormat levelFormat = new LevelFormat(level,
								globalStyleName);

						// create temp map to hold key of integer and value of
						// LevelFormat
						Map<Integer, LevelFormat> tempLevelFormatMap = new HashMap<Integer, LevelFormat>();

						// insert key and value into map
						tempLevelFormatMap.put(levelFormat.getLevel(),
								levelFormat);

						// set the level formats
						dimension.setLevelFormats(tempLevelFormatMap);

						// bold tree item
						boldTreeItem(selectedLevelGen, true);

					}

				}

			} else if (ident
					.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {

				// get generation from generation name
				int generation = parseIntFromString(levelGenMember);

				// if dimension is not null
				if (dimension != null) {

					// get generation format from dimension
					GenFormat genFormat = dimension.getGenFormat(generation);

					// if generation format is null and global style is blank,
					// create a
					// new generation format
					if (genFormat == null && !globalStyleName.equals("")) {

						genFormat = new GenFormat(generation, globalStyleName);

						// if generation format is not null and global style
						// name is
						// blank, remove from dimension and set to null
					} else if (globalStyleName.equals("")) {

						// remove generation format from dimension
						dimension.removeGenFormat(genFormat);

						// unbold tree item
						boldTreeItem(selectedLevelGen, false);

						genFormat = null;

						// if no more generation formats exist, remove dimension
						// from
						// current model object and then null
						if (dimension.getNumberOfHierFormats() == 0) {
							currentHiearchyFormat.removeDimension(dimension);
							dimension = null;
						}

						// all else fails, set formatName to new format name
					} else {

						genFormat.setFormatName(globalStyleName);

					}

					// if generation format is not null, add to dimension
					// (replaces
					// existing one)
					if (genFormat != null) {

						boldTreeItem(selectedLevelGen, true);

						dimension.addGenFormat(genFormat);
					}

					// if dimension is null
				} else {

					// if global style name is not blank
					if (!globalStyleName.equals("")) {

						// create a new dimension
						dimension = new Dimension();

						// set dimension name
						dimension.setName(dimensionName);

						// create new generation format
						GenFormat genFormat = new GenFormat(generation,
								globalStyleName);

						// create temp map to hold key of integer and value of
						// GenFormat
						Map<Integer, GenFormat> tempGenFormatMap = new HashMap<Integer, GenFormat>();

						// insert key and value into map
						tempGenFormatMap.put(genFormat.getGeneration(),
								genFormat);

						// set the generation formats
						dimension.setGenFormats(tempGenFormatMap);

						// bold tree item
						boldTreeItem(selectedLevelGen, true);

					}

				}

			}

			// if dimension is not null, add dimension to current model
			if (dimension != null) {
				currentHiearchyFormat.addDimension(dimension);
			}
		}
	}

	/**
	 * The tree item should be bolded / unbolded based on bold flag. Then the
	 * parent item should be bolded / unbolded based on if any of it's children
	 * are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded
	 */
	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		// get font data ar for tree item
		FontData[] fontDataAr = treeItem.getFont().getFontData();

		if (fontDataAr.length > 0) {

			// get font data
			FontData fontData = fontDataAr[0];

			// if bold is true, set style to bold, else to none
			if (bold) {
				fontData.setStyle(SWT.BOLD);
			} else {
				fontData.setStyle(SWT.NONE);
			}

			// set tree item font
			treeItem.setFont(new Font(this.getShell().getDisplay(), fontData));

			// get parent item for tree item
			TreeItem parentItem = TreeUtil.getParentOfTree(treeItem);

			// get font data for parent data
			FontData parentFontData = parentItem.getFont().getFontData()[0];

			// if a parent has at least one child with bold, then bold parent
			if (parentsChildrenHaveBolding(parentItem)) {

				parentFontData.setStyle(SWT.BOLD);

				parentItem.setFont(new Font(this.getShell().getDisplay(),
						parentFontData));

				// else unbold parent
			} else {

				parentFontData.setStyle(SWT.NONE);

				parentItem.setFont(new Font(this.getShell().getDisplay(),
						parentFontData));

			}

		}

	}

	/**
	 * Checks to see if a parent item has any children has the style of
	 * SWT.BOLD. If they do, then the parent should be bolded, otherwise the
	 * parent should have the style set to NONE.
	 * 
	 * @param parentItem
	 *            parent item containing the child items
	 */
	private boolean parentsChildrenHaveBolding(TreeItem parentItem) {

		boolean exist = false;

		// if parent item is null, return false;
		if (parentItem == null) {
			return false;
		}

		// for each child item, see if child has style of bold
		for (TreeItem childItem : parentItem.getItems()) {

			// if a child item is bolded, then set to true and break
			if ((childItem.getFont().getFontData()[0].getStyle() & SWT.BOLD) > 0) {

				exist = true;
				break;

			}

		}

		return exist;

	}

	/**
	 * When a new dimension is selected, the dimension tree should be cleared
	 * and repopulated with new dimension(s).
	 * 
	 * @param dimension
	 *            dimension name
	 */
	protected void newDimensionSelectedAction(String dimension) {

		// remove all items
		dimensionTree.removeAll();

		// if ALL, populate the tree with all dimensions
		if (dimension.equals("ALL")) {

			// for each dimension, add to dimension tree
			for (String dim : cachedDimensions) {
				// load dimension tree
				loadDimensionTree(dim, false);
			}
			// for each dimension, add to dimension tree
			for (String dim : cachedAttributeDimensions) {
				// load dimension tree
				loadDimensionTree(dim, false);
			}

		} else {

			// load dimension tree
			loadDimensionTree(dimension, true);

		}

		// turn caching off
		cachingEnabled = false;

		// clear the dimension form
		clearDimensionForm();

		// turn caching on
		cachingEnabled = true;

	}

	/**
	 * Checks to see if a global style is defined via a dimension name and
	 * level.
	 * 
	 * @param dimensionName
	 *            dimension name
	 * @param level
	 *            current level
	 */
	private boolean isGlobalStyleDefined(String dimensionName,
			String levelGenMember) {

		boolean globalStyleFound = false;

		if (currentHiearchyFormat != null) {

			Dimension dimension = currentHiearchyFormat
					.getDimension(dimensionName);

			// if dimension is not null
			if (dimension != null) {

				String globalStyleName = null;

				String ident = parseIdentFromString(levelGenMember);

				if (ident.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)) {

					globalStyleName = dimension
							.getLevelFormatName(parseIntFromString(levelGenMember));

				} else if (ident
						.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {

					globalStyleName = dimension
							.getGenFormatName(parseIntFromString(levelGenMember));

				}

				// if level format name exist
				if (globalStyleName != null) {

					// if global style name exist in global style model manager,
					// the set flag to true
					if (globalStyleModelManager.contains(globalStyleName)) {

						globalStyleFound = true;

					} else {

						logger.error("Global Style: " + globalStyleName
								+ " defined in "
								+ PafBaseConstants.FN_HierarchyFormats
								+ " file but not in "
								+ PafBaseConstants.FN_GlobalStyleMetaData + " file");

					}
				}

			}
		}

		return globalStyleFound;
	}

	private void loadDimensionTree(String dimensionName, boolean expand) {

		// if dimension exist in the generation format
		if (treeCache.containsKey(dimensionName)) {

			// get the level numbers. Number of levels per dimension
			ArrayList<String> levelGenMembers = treeCache.get(dimensionName);

			// create the tree root item
			TreeItem rootItem = new TreeItem(dimensionTree, SWT.NONE);
			// set the root's name
			rootItem.setText(dimensionName);

			// loop through all lever numbers to populate the root item with
			// levels
			for (String levelGenMember : levelGenMembers) {

				// create level tree item off of root item
				TreeItem newTreeItem = new TreeItem(rootItem, SWT.NONE);

				// set the new item to the level number (for example L1 or G2)
				newTreeItem.setText(levelGenMember);

				// is a global style defined for this dimension and level
				boolean globalStyleDefined = isGlobalStyleDefined(
						dimensionName, levelGenMember);

				// if a global sytle is defined, bold item
				if (globalStyleDefined) {
					boldTreeItem(newTreeItem, true);
				}

			}

			// expand root based on expand boolean
			rootItem.setExpanded(expand);

		}

	}

	/**
	 * Sets up the listeners for the buttons
	 * 
	 */
	private void setupButtonListeners() {

		// new button listeners
		setupButtonSelectionListener(newButton, ButtonId.New);

		// delete button listeners
		setupButtonSelectionListener(deleteButton, ButtonId.Delete);

		// add button listeners
		setupButtonSelectionListener(addButton, ButtonId.Add);

		// cancel button listeners
		setupButtonSelectionListener(cancelButton, ButtonId.Cancel);

	}

	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}

	/**
	 * Used to perfrom an action when a button is pressed
	 * 
	 * @param buttonId
	 *            enum for which button was selected
	 */
	private void buttonSelected(ButtonId buttonId) {

		switch (buttonId) {

		case Add:

			logger.debug(buttonId + " button clicked");
			addButtonAction();
			cachingEnabled = true;
			break;

		case Delete:

			logger.debug(buttonId + " button clicked");
			deleteButtonAction();

			break;

		case New:

			logger.debug(buttonId + " button clicked");
			newButtonAction();
			cachingEnabled = false;
			break;

		case Cancel:

			logger.debug(buttonId + " button clicked");
			cancelButtonAction();
			cachingEnabled = true;
			break;

		}

		// reset dimension tree to clear the bolding
		resetDimensionTree();

	}

	/**
	 * Action called when new button was pressed
	 */
	private void newButtonAction() {

		list.setEnabled(false);
		list.deselectAll();

		enablePrimaryKey(true);

		enableDimensionForm(false);

		clearForm();

		// newDimensionSelectedAction("ALL");

		// clearDimensionTreeBolding();

		// resetDimensionTree();

		newButton.setEnabled(false);
		deleteButton.setEnabled(false);

		addButton.setVisible(true);
		addButton.setEnabled(false);

		cancelButton.setVisible(true);
		cancelButton.setEnabled(true);

	}

	/**
	 * Enables teh dimension combo box, dimension tree and global style combo
	 * 
	 * @param enable
	 *            enable/disable form based on value
	 */
	private void enableDimensionForm(boolean enable) {

		selectDimensionCombo.setEnabled(enable);
		dimensionTree.setEnabled(enable);
		globalStyleCombo.setEnabled(enable);

	}

	/**
	 * Action called when delete button was pressed
	 */
	private void deleteButtonAction() {

		// if selection count is greater than 0, delete item
		if (list.getSelectionCount() > 0) {

			// get generation format name
			String key = list.getSelection()[0];

			// if generation format exist in map
			if (modelMap.containsKey(key)) {

				// remove from map
				modelMap.remove(key);
				refreshList();
				initializeDialog();

			}

		} else {

			// deselect all from the tree
			list.deselectAll();

		}

		// reset dimension combo
		// resetDimensionTree();

	}

	/**
	 * Clears the current form.
	 */
	private void clearForm() {

		// turn caching off
		cachingEnabled = false;

		nameText.setText("");
		selectDimensionCombo.setText("");
		globalStyleCombo.setText("");
		dimensionText.setText("");
		levelGenText.setText("");

		// turn caching on
		cachingEnabled = true;
	}

	private void clearDimensionTreeBolding() {

		TreeItem[] dimensionTreeItems = dimensionTree.getItems();

		for (TreeItem dimensionItem : dimensionTreeItems) {

			for (TreeItem dimensionLevelItem : dimensionItem.getItems()) {
				boldTreeItem(dimensionLevelItem, false);
			}

		}

	}

	/**
	 * Action called when cancel button was pressed
	 */
	private void cancelButtonAction() {

		list.setEnabled(true);

		enablePrimaryKey(false);

		enableDimensionForm(false);

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);

		addButton.setVisible(false);
		cancelButton.setVisible(false);

		initializeDialog();

	}

	/**
	 * Action called when add button was pressed
	 */
	private void addButtonAction() {

		HierarchyFormat model = new HierarchyFormat();

		String key = nameText.getText().trim();

		// set generation format name
		model.setName(key);

		// push model into model map
		modelMap.put(key, model);

		refreshList();

		list.setEnabled(true);
		list.select(getListItemIndex(key));

		populateGenerationFormatForm(key);

		// resetDimensionTree();

		newButton.setEnabled(true);
		deleteButton.setEnabled(true);
		addButton.setVisible(false);
		cancelButton.setVisible(false);

	}

	/**
	 * Removes all entries in the list and repopulates with the keyset from the
	 * model map
	 */
	private void refreshList() {

		list.removeAll();
		list.deselectAll();
		list.setItems(modelMap.keySet().toArray(new String[0]));

	}

	/**
	 * Enables the form fields
	 * 
	 * @param enable
	 *            enables/disables the form fields
	 */
	private void enablePrimaryKey(boolean enable) {

		nameText.setEnabled(enable);

	}

	/**
	 * Used to get the index of the key in the list
	 * 
	 * @returns int index of key in list
	 */
	private int getListItemIndex(String key) {

		int index = 0;

		// find the generation format index from listbox
		for (String item : list.getItems()) {

			if (item.equals(key)) {
				break;
			}

			index++;
		}

		return index;
	}

	@Override
	protected void okPressed() {

		// save the cached model to model
		saveCachedModel();

		modelManager.setModel(modelMap);

		// write to file
		modelManager.save();

		// calls parent method
		super.okPressed();

	}

	/**
	 * Saves the cached model objects.
	 */
	private void saveCachedModel() {

		// if current model exist
		if (currentHiearchyFormat != null) {

			// if generation format model map contains current model, update
			if (modelMap.containsKey(currentHiearchyFormat.getName())) {

				logger.debug("Updating model map for key "
						+ currentHiearchyFormat.getName());
				modelMap.put(currentHiearchyFormat.getName(),
						currentHiearchyFormat);
				currentHiearchyFormat = null;

			}

		}

	}

	/**
	 * 
	 * @param strLevel
	 * @return
	 */
	private int parseIntFromString(String strLevel) {

		return Integer.parseInt(strLevel.substring(1, strLevel.length()));

	}

	/**
	 * 
	 * @param strLevel
	 * @return
	 */
	private String parseIdentFromString(String strMember) {

		return strMember.substring(0, 1);

	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.pace.base.app.PafDimSpec;


/**
 * The Measure Flag Intersection Dialog.  Used to setup all non security dimensions with a member.
 *
 * @version	1.00
 * @author jmilliron
 *
 */
public class MeasureFlagIntersectionDialog extends TitleAreaDialog {

	private Table table;
	
	private PafDimSpec[] pafDimSpecAr;
	
	private IProject project;
	
	//map to key dim name and value a paf dim spec
	private Map<String, PafDimSpec> pafDimSpecMap = new LinkedHashMap<String, PafDimSpec>();
	
	//map used to hold the table editors per dimension
	private Map<String, HashSet<TableEditor>> dimensionTableEditorMap = new HashMap<String, HashSet<TableEditor>>();
		
	private static final String BUTTON_INDEX_KEY = "ButtonKey";
	
	private static final String DIMENSION_NAME_KEY = "DimKey";

	/**
	 * 
	 * Constructor
	 * 
	 * @param parentShell	Parent Shell
	 * @param project		Current Project
	 * @param pafDimSpecAr	
	 */
	public MeasureFlagIntersectionDialog(Shell parentShell, IProject project, PafDimSpec[] pafDimSpecAr) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;
		
		//populate the paf dim spec by cloning all the options
		if ( pafDimSpecAr != null ) {
			
			this.pafDimSpecAr = new PafDimSpec[pafDimSpecAr.length];
			
			for ( int i = 0; i < pafDimSpecAr.length; i++ ) {

				this.pafDimSpecAr[i] = (PafDimSpec) pafDimSpecAr[i].clone();
				
			}
			
		}
						

	}
		
	private boolean getOkButtonStatus() {
		
		if ( this.pafDimSpecAr == null ) {
			return false;
		}		
					
		for ( PafDimSpec pafDimSpec : pafDimSpecAr ) {
			
			if ( pafDimSpec.getExpressionList() == null || pafDimSpec.getExpressionList().length != 1 ) {
				return false;
			}
			
		}
		
		return true;
						
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		final Composite composite = new Composite(area, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 25;
		gridLayout.marginRight = 60;
		gridLayout.marginLeft = 60;
		composite.setLayout(gridLayout);

		table = new Table(composite, SWT.BORDER);
		table.getHorizontalBar().setEnabled(false);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn tableColumn = new TableColumn(table, SWT.NONE);
		tableColumn.setWidth(100);
		tableColumn.setText("Dimension");

		final TableColumn tableColumn_1 = new TableColumn(table, SWT.NONE);
		tableColumn_1.setWidth(199);
		tableColumn_1.setText("Selected Member");
		setTitle("Measure Flag Intersection");
		setMessage("Configure each non-security dimension by selecting the ellipsis button and then selecting a member from the tree.");

		populateTableWithDimensionSpec(composite);
		
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(446, 370);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Measure Flag Intersection");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();
		getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
	}

	/**
	 * @return Returns the pafDimSpecAr.
	 */
	public PafDimSpec[] getPafDimSpecAr() {
		return pafDimSpecAr;
	}

	/**
	 * 
	 * Populates the table's 2nd column from dimension spec
	 *
	 * @param composite
	 */
	private void populateTableWithDimensionSpec(final Composite composite) {
		
		if ( pafDimSpecAr != null ) {
			
			//populate the table 1st column as dimenion name
			for (int i=0; i< pafDimSpecAr.length; i++) {
				TableItem item = new TableItem (table, SWT.NONE);
				item.setText(pafDimSpecAr[i].getDimension());
			}
			
			//get table items
			TableItem [] items = table.getItems ();

			//loop over paf dim spec ar
			for (int i=0; i<pafDimSpecAr.length; i++) {
				
				//get table editors for dimension
				HashSet<TableEditor> tableEditorHashSet = dimensionTableEditorMap.get(pafDimSpecAr[i].getDimension());
				
				//if null, create new hash set
				if ( tableEditorHashSet == null ) {
					tableEditorHashSet = new HashSet<TableEditor>();
				}
				
				//create new table editor
				TableEditor editor = new TableEditor (table);				
								
				String member = null;
				
				//if member exists on dim spec, get it
				if ( pafDimSpecAr[i].getExpressionList() != null) {
				
					member = pafDimSpecAr[i].getExpressionList()[0];
					
				}
				
				//push value into paf dim spec map <dimension name, paf dim spec>
				pafDimSpecMap.put(pafDimSpecAr[i].getDimension(), pafDimSpecAr[i]);
				
				//new label
				Label label = new Label(table, SWT.NONE);
				label.setText((member == null) ? "" : member);
				label.pack();
				editor.horizontalAlignment = SWT.LEFT;
				editor.minimumWidth = label.getSize ().x;
				editor.setEditor(label , items[i], 1);
				tableEditorHashSet.add(editor);
				
				//new editor
				editor = new TableEditor (table);
				
				//new button
				Button button = new Button (table, SWT.PUSH);
				button.setText("...");
				button.setData(DIMENSION_NAME_KEY, pafDimSpecAr[i].getDimension());
				button.setData(BUTTON_INDEX_KEY, new Integer(i));
				
				//add button listener
				button.addSelectionListener(new SelectionAdapter() {

					public void widgetSelected(final SelectionEvent selectionEvent) {
						
						Object source = selectionEvent.getSource();
						
						if ( source instanceof Button && ((Button) source).getData(DIMENSION_NAME_KEY) instanceof String) {
							
							final Button buttonSource = (Button) source;
							
							final String dimensionName = (String) buttonSource.getData(DIMENSION_NAME_KEY);

							// Identify the selected row
							final int tableIndex = (Integer) buttonSource.getData(BUTTON_INDEX_KEY);
												
							final SelectionAdapter currentSelectionAdapter = this;
							
							// use an hourglass and start up the tuples dialog
							BusyIndicator.showWhile(composite.getDisplay(),
									new Runnable() {
										public void run() {
											
											DimensionTreeSelectorDialog dialog = new DimensionTreeSelectorDialog(composite.getShell(), project, dimensionName);
											int rc = dialog.open();
											
											if ( rc == Dialog.OK) {
												
												String selectedMember = dialog.getSelectedMember();
												
												if ( selectedMember != null) {
												
													if ( dimensionTableEditorMap.containsKey( dimensionName)) {

														HashSet<TableEditor> editorHashSet = dimensionTableEditorMap.get(dimensionName);
																																									
														if ( editorHashSet != null ) {
														
															TableEditor[] tableEditorAr = editorHashSet.toArray(new TableEditor[0]);
															
															for (TableEditor oldEditor : tableEditorAr) {
														
																Control oldEditorControl = oldEditor.getEditor();
																
																if ( oldEditorControl != null ) {
																	oldEditorControl.dispose();
																}		
																
																editorHashSet.remove(oldEditor);
																
															}
															
														}
														
														TableEditor editor = new TableEditor(table);									
							
														TableItem item = table.getItem (tableIndex);
														
														if ( item == null ) { 
															return;
														}
														
														Label label = new Label(table, SWT.NONE);
														label.setText(selectedMember);
														label.pack();
														
														editor.horizontalAlignment = SWT.LEFT;
														editor.minimumWidth = label.getSize ().x;
														editor.setEditor(label , item, 1);
														
														editorHashSet.add(editor);
														
														editor = new TableEditor (table);
														
														Button button = new Button (table, SWT.PUSH);
														button.setText("...");
														button.setData(DIMENSION_NAME_KEY, dimensionName);
														button.setData(BUTTON_INDEX_KEY, new Integer(tableIndex));
														button.addSelectionListener(currentSelectionAdapter);
														button.pack();
														
														editor.minimumWidth = button.getSize ().x;
														editor.horizontalAlignment = SWT.RIGHT;
														editor.setEditor(button , item, 1);
														editorHashSet.add(editor);
														
														dimensionTableEditorMap.put(dimensionName, editorHashSet);
														
														PafDimSpec pafDimSpec = pafDimSpecMap.get(dimensionName);
														
														if ( pafDimSpec != null ) {
														
															pafDimSpec.setExpressionList(new String[] { selectedMember });
															
														}
														
														pafDimSpecMap.put(dimensionName, pafDimSpec);
														
														pafDimSpecAr = pafDimSpecMap.values().toArray(new PafDimSpec[0]);
														
														getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
														
													}
													
												
												}
											}
											
										}
									});
							
						}
					
					}
					
				});

				button.pack();
				editor.minimumWidth = button.getSize ().x;
				editor.horizontalAlignment = SWT.RIGHT;
				editor.setEditor (button, items[i], 1);

				//add editor to hash set
				tableEditorHashSet.add(editor);
				
				//replace old hash set with new one
				dimensionTableEditorMap.put(pafDimSpecAr[i].getDimension(), tableEditorHashSet);
				
			}
		}
		
	}
}

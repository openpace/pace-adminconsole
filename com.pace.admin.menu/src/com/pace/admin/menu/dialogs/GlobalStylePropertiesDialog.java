/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.GlobalStyleModelManager;
import com.pace.admin.global.util.ColorsUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.widgets.ColorButton;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.misc.PafRGB;
import com.pace.base.view.PafAlignment;
import com.pace.base.view.PafStyle;

/**
 * The Global Styles Properties Dialog allows a user to set up different 
 * Global Styles per project.  Global Styles are used by the client to display
 * certain properties such as bold, font name, fill color, etc. 
 *
 * @version	x.xx
 * @author Jason Milliron
 *
 */

public class GlobalStylePropertiesDialog extends Dialog {

	/** Used to populate the font size drop down */
	private String[] fontSizes = new String[] { "", "8", "9", "10", "11", "12",
			"14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72" };
	
	private PafStyle currentStyle;

	private GlobalStyleModelManager styles;

	private List styleList;

	private Button newButton;

	private Button copyButton;

	private Button deleteButton;

	private Text styleName;

	private Combo fontNames;

	private Button fontColorSwatchButton;
	
	private ColorButton fontColorSwatchColorButton;
	
	private Button fillColorSwatchButton;
	
	private ColorButton fillColorSwatchColorButton;

	private Combo fontSize;

	private Combo alignment;
	
	private Combo boldCombo;

	private Combo italicCombo;

	private Combo underlineCombo;

	private Combo strikeoutCombo;

	private Combo doubleUnderlineCombo;

	private Button addButton;

	private Button cancelButton;

	private Button okButton;

	private Display display;

	@SuppressWarnings("unused")
	private Composite composite;

	private static final String ADD_BUTTON = "Add";

	private static final String CANCEL_BUTTON = "Cancel";

	private static final String NEW_BUTTON = "New";

	private static final String DELETE_BUTTON = "Delete";

	private static final String COPY_BUTTON = "Copy";

	private static final String OK_BUTTON = "Ok";

	private static final int NEW_BUTTON_ID = 0;

	private static final int COPY_BUTTON_ID = 1;

	private static final int DELETE_BUTTON_ID = 2;

	private static final int ADD_BUTTON_ID = 3;

	private static final int CANCEL_BUTTON_ID = 4;

	private TreeMap<String, String> excelColors = null;

	private String[] styleOptions = { "None", "True", "False" };
	
	private static Font labelFont = new Font(PlatformUI.getWorkbench()
			.getActiveWorkbenchWindow().getShell().getDisplay(), "Arial", 8,
			SWT.BOLD);

	private Button removeFontColorButton;
	
	private Button removeFillColorButton;

	private Set<Color> colorSet = new HashSet<Color>();

	/** Initialized the GlobalStyleModelManger and loads the Excel Colors 
	 * 
	 * @param parentShell	Fed from the calling Shell
	 * @param parentDir		The string representation of the parent directory
	 *  
	 */

	public GlobalStylePropertiesDialog(Shell parentShell, IProject project) {

		super(parentShell);
		
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		styles = new GlobalStyleModelManager(project);


	}

	/** Loads the excel colors into a Map.  If file is not found a dialog window
	 * will alert the user.
	 */
//	private void loadExcelColors() {
//
//		try {
//			excelColors = (TreeMap<String, String>) PafXStream
//					.importObjectFromXml(Constants.ADMIN_CONSOLE_CONF_DIRECTORY + "\\"
//							+ Constants.EXCEL_COLORS);
//		} catch (PafConfigFileNotFoundException e) {
//			
//			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "File: '" + Constants.ADMIN_CONSOLE_CONF_DIRECTORY + "\\"
//					+ Constants.EXCEL_COLORS + "' could not be found", MessageDialog.ERROR);
//			e.printStackTrace();
//		}
//		
//	}

	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		
		newShell.setText(Constants.GLOBAL_SYTLES_WIZARD_NAME);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite area = (Composite) super.createDialogArea(parent);

		this.composite = area;

		display = area.getDisplay();

		GridLayout layout = new GridLayout();
		layout.marginLeft = 10;
		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = true;
		area.setLayout(layout);

		createLeftSide(area);

		createFormArea(area);

		initilizeForm();

		setUpListeners();

		return area;
	}

	/** Adds a SelectionListener to the button passed in.
	 * 
	 * @param button			The button the listener will be added to
	 * @param buttonId			The id for the button being passed in
	 */
	private void setUpButtonListener(Button button, final int buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				buttonClicked(buttonId);

			}

		});

	}
	
	/** Adds a SelectionListener to the button passed in.
	 * 
	 * @param button			The button the listener will be added to
	 */
	private void setUpStyleListener(Combo combo) {

		combo.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				applyStyleChanges();
				updateAddButton();

			}

		});

	}

	/** Sets up all the Listeners for the widgets.
	 * 
	 */
	private void setUpListeners() {

		// Setup button listeners
		setUpButtonListener(newButton, NEW_BUTTON_ID);
		setUpButtonListener(copyButton, COPY_BUTTON_ID);
		setUpButtonListener(deleteButton, DELETE_BUTTON_ID);
		setUpButtonListener(cancelButton, CANCEL_BUTTON_ID);
		setUpButtonListener(addButton, ADD_BUTTON_ID);

		styleList.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				saveExistingStyleState();

				loadStyle(styleList.getItem(styleList.getSelectionIndex()));

			}

		});

		styleName.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				updateAddButton();
			}

		});

		fontNames.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				applyStyleChanges();
				updateAddButton();

			}

		});

		fontSize.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				applyStyleChanges();
				updateAddButton();

			}

		});

		alignment.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				applyStyleChanges();
				updateAddButton();

			}

		});
		
		
		setUpStyleListener(boldCombo);
		setUpStyleListener(italicCombo);
		setUpStyleListener(strikeoutCombo);
		setUpStyleListener(underlineCombo);
		setUpStyleListener(doubleUnderlineCombo);

	}

	/** Enables/Disables the Add Button
	 * 
	 */
	public void updateAddButton() {

		//if sytleName is not blank and at least 1 property exist.
		addButton.setEnabled(styleName.getText().trim().length() != 0
				&& stylePropertyDataExists());	

	}

	/** Determines if one of hte properties is set
	 * 
	 * @return			true/false if a property exist.
	 */
	private boolean stylePropertyDataExists() {

		if (fontNames.getText().equals("") 
				&& fillColorSwatchColorButton.getColor() == null
				&& fontColorSwatchColorButton.getColor() == null
				&& fontSize.getText().equals("")
				&& alignment.getText().equals("") 
				&& boldCombo.getText().equals(styleOptions[0])
				&& italicCombo.getText().equals(styleOptions[0])
				&& strikeoutCombo.getText().equals(styleOptions[0])
				&& underlineCombo.getText().equals(styleOptions[0])
				&& doubleUnderlineCombo.getText().equals(styleOptions[0])) {
			return false;
		} else {
			return true;
		}

	}

	/** Creates the List, New button, Copy button, and Delete button
	 * 
	 * @param compoiste			Parent composite
	 */
	public void createLeftSide(final Composite composite) {

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		Composite leftComp = new Composite(composite, SWT.NONE);
		GridLayout leftCompLayout = new GridLayout();
		leftCompLayout.numColumns = 2;
		leftComp.setLayout(leftCompLayout);
		leftComp.setLayoutData(data);

		Label lListBox = new Label(leftComp, SWT.NONE);
		lListBox.setText("Select an item:");

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);

		styleList = new List(leftComp, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		styleList.setItems(styles.getKeys());
		styleList.setLayoutData(data);

		data = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		Composite buttonComposite = new Composite(leftComp, SWT.NONE);
		GridLayout buttonLayout = new GridLayout();
		buttonLayout.marginWidth = 0;
		buttonLayout.numColumns = 3;
		buttonLayout.makeColumnsEqualWidth = true;
		buttonComposite.setLayout(buttonLayout);
		buttonComposite.setLayoutData(data);

		data = new GridData(GridData.FILL_BOTH);
		newButton = new Button(buttonComposite, SWT.PUSH);
		newButton.setText(NEW_BUTTON);
		newButton.setLayoutData(data);

		data = new GridData(GridData.FILL_BOTH);
		copyButton = new Button(buttonComposite, SWT.PUSH);
		copyButton.setText(COPY_BUTTON);
		copyButton.setLayoutData(data);

		data = new GridData(GridData.FILL_BOTH);
		deleteButton = new Button(buttonComposite, SWT.PUSH);
		deleteButton.setText(DELETE_BUTTON);
		deleteButton.setLayoutData(data);

	}

	/** Creates the form
	 * 
	 * @param composite		The parent composite
	 */
	public void createFormArea(final Composite composite) {

		// parent composite for this area
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		data.heightHint = 183;
		final Composite rightComp = new Composite(composite, SWT.RIGHT);
		GridLayout rightCompLayout = new GridLayout();
		rightCompLayout.marginRight = 15;
		rightCompLayout.marginLeft = 15;
		rightCompLayout.numColumns = 3;
		rightComp.setLayout(rightCompLayout);
		rightComp.setLayoutData(data);

		// style name label
		data = getLabelGridData();
		final Label lStyleName = new Label(rightComp, SWT.NONE);
		lStyleName.setText("Style Name");
		lStyleName.setFont(labelFont);
		lStyleName.setLayoutData(data);

		// style name text
		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		data.widthHint = 150;
		styleName = new Text(rightComp, SWT.BORDER);
		styleName.setLayoutData(data);
		styleName.setTextLimit(50);

		// style property group
		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		Group sytlePropertyGroup = new Group(rightComp, SWT.SHADOW_ETCHED_IN);
		GridLayout sytlePropertyLayout = new GridLayout();
		sytlePropertyLayout.numColumns = 3;
		sytlePropertyGroup.setText("Style Property");
		sytlePropertyGroup.setLayout(sytlePropertyLayout);
		sytlePropertyGroup.setLayoutData(data);

		// style name text
		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		data.horizontalSpan = 3;
		data.widthHint = 270;
		
		Group group = new Group(sytlePropertyGroup, SWT.SHADOW_ETCHED_IN);
		GridLayout groupLayout = new GridLayout();
		groupLayout.numColumns = 3;
		//groupLayout.marginRight = 10;
		//groupLayout.marginLeft = 15;
		
		group.setText("Font Style");
		group.setLayout(groupLayout);
		group.setLayoutData(data);

		// font name label
		data = getLabelGridData();
		final Label lFontName = new Label(group, SWT.NONE);
		lFontName.setText("Font Name");
		lFontName.setFont(labelFont);
		lFontName.setLayoutData(data);

		// font name combo box
		data = new GridData();
		data.widthHint = 100;
		data.horizontalSpan = 2;
		fontNames = new Combo(group, SWT.BORDER | SWT.READ_ONLY);
		fontNames.setLayoutData(data);
		String[] fonts = getFontNames();
		if(fonts != null){	
			fontNames.setItems(fonts);
		}

		// font color label
		data = getLabelGridData();
		final Label lFontColor = new Label(group, SWT.NONE | SWT.READ_ONLY);
		lFontColor.setText("Font Color");
		lFontColor.setFont(labelFont);
		lFontColor.setLayoutData(data);

		//font color swatch 
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		data.grabExcessHorizontalSpace = true;
		 
		fontColorSwatchButton = new Button(group, SWT.None);
		fontColorSwatchButton.setLayoutData(data);
		fontColorSwatchButton.setToolTipText("Choose color");
		fontColorSwatchColorButton = new ColorButton(fontColorSwatchButton, null);
		
		fontColorSwatchButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Color origColor = fontColorSwatchColorButton.getColor();
				ColorDialog dlg = new ColorDialog(getShell());
				if( origColor != null ) {
					RGB origRGB = origColor.getRGB();
					dlg.setRGBs(new RGB[]{origRGB});
					dlg.setRGB(origRGB);
				}
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(getShell().getDisplay(), rgb);
					fontColorSwatchColorButton.setColor(selectedColor);			
					colorSet.add(selectedColor);
					
					removeFontColorButton.setEnabled(true);
				
					applyStyleChanges();
					
					updateAddButton();
		        }
				
			}
			
		});
		
		data = new GridData(23, 23);
		
		removeFontColorButton = new Button(group, SWT.None);
		Image image1 = getRemoveImage();
		if(image1 != null){
			removeFontColorButton.setImage(image1);
		}
		removeFontColorButton.setLayoutData(data);
		removeFontColorButton.setEnabled(false);
		removeFontColorButton.setToolTipText("Remove color");
		removeFontColorButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				fontColorSwatchColorButton.setColor(null);
				removeFontColorButton.setEnabled(false);
				applyStyleChanges();
				updateAddButton();
			}
			
		});
					

		// font Size label
		data = getLabelGridData();
		final Label lFontSize = new Label(group, SWT.NONE | SWT.READ_ONLY);
		lFontSize.setText("Font Size");
		lFontSize.setFont(labelFont);
		lFontSize.setLayoutData(data);

		// font Size text
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 12;
		fontSize = new Combo(group, SWT.BORDER | SWT.READ_ONLY);
		fontSize.setLayoutData(data);
		fontSize.setItems(fontSizes);

		// font Size label
		data = getLabelGridData();
		final Label lFontAlignment = new Label(group, SWT.NONE | SWT.READ_ONLY);
		lFontAlignment.setText("Alignment");
		lFontAlignment.setFont(labelFont);
		lFontAlignment.setLayoutData(data);

		// font Size text
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 35;
		alignment = new Combo(group, SWT.BORDER | SWT.READ_ONLY);
		alignment.setLayoutData(data);
		alignment.setItems(new String[] { "", "Left", "Center", "Right" });

		// button panel
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 200;
		Composite buttonPanel1 = new Composite(group, SWT.NONE);
		//buttonPanel1.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		
		//GridLayout buttonLayout = new GridLayout();
		//buttonLayout.numColumns = 4;
		
		RowLayout buttonLayout = new RowLayout();
		buttonLayout.spacing = 6;
		
		buttonPanel1.setLayout(buttonLayout);
		buttonPanel1.setLayoutData(data);

		// bold info
		Label boldLabel = new Label(buttonPanel1, SWT.NONE);
		boldLabel.setFont(labelFont);
		boldLabel.setText("Bold");

		//boldCheck = new Button(buttonPanel, SWT.CHECK);
		boldCombo = new Combo(buttonPanel1, SWT.READ_ONLY);
		boldCombo.setItems(styleOptions);

		// italic info
		Label italicLabel = new Label(buttonPanel1, SWT.NONE);
		italicLabel.setFont(labelFont);
		italicLabel.setText("Italic");

		italicCombo = new Combo(buttonPanel1, SWT.READ_ONLY);
		italicCombo.setItems(styleOptions);

//		 button panel
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 260;
		Composite buttonPanel2 = new Composite(group, SWT.NONE);
		buttonLayout = new RowLayout();
		buttonLayout.spacing = 6;
		
		buttonPanel2.setLayout(buttonLayout);
		buttonPanel2.setLayoutData(data);
		
		// strike out info
		Label strikeoutLabel = new Label(buttonPanel2, SWT.NONE);
		strikeoutLabel.setFont(labelFont);
		strikeoutLabel.setText("Strikeout");

		strikeoutCombo = new Combo(buttonPanel2, SWT.READ_ONLY);
		strikeoutCombo.setItems(styleOptions);

		// underline info
		Label underlineLabel = new Label(buttonPanel2, SWT.NONE);
		underlineLabel.setFont(labelFont);
		underlineLabel.setText("Underline");

		underlineCombo = new Combo(buttonPanel2, SWT.READ_ONLY);
		underlineCombo.setItems(styleOptions);

//		 button panel
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 200;
		Composite buttonPanel3 = new Composite(group, SWT.NONE);
		buttonLayout = new RowLayout();
		buttonLayout.spacing = 6;
		
		buttonPanel3.setLayout(buttonLayout);
		buttonPanel3.setLayoutData(data);

		
		// double undrline info
		Label doubleUnderlineLabel = new Label(buttonPanel3, SWT.NONE);
		doubleUnderlineLabel.setFont(labelFont);
		doubleUnderlineLabel.setText("Double Underline");

		doubleUnderlineCombo = new Combo(buttonPanel3, SWT.READ_ONLY);
		doubleUnderlineCombo.setItems(styleOptions);

		// back ground group
		data = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		data.widthHint = 265;
		data.horizontalSpan = 3;
		Group backgroudStyleGroup = new Group(sytlePropertyGroup,
				SWT.SHADOW_ETCHED_IN);
		GridLayout backgroudStyleLayout = new GridLayout();
		backgroudStyleLayout.numColumns = 3;
		backgroudStyleGroup.setText("Background Style");
		backgroudStyleGroup.setLayout(sytlePropertyLayout);
		backgroudStyleGroup.setLayoutData(data);

		// fill color label
		data = getLabelGridData();
		Label lFillColor = new Label(backgroudStyleGroup, SWT.NONE
				| SWT.READ_ONLY);
		lFillColor.setText("Fill Color");
		lFillColor.setFont(labelFont);
		lFillColor.setLayoutData(data);
		
//		font color swatch 
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		data.grabExcessHorizontalSpace = true;
		 
		fillColorSwatchButton = new Button(backgroudStyleGroup, SWT.None);
		fillColorSwatchButton.setLayoutData(data);
		fillColorSwatchButton.setToolTipText("Choose color");
		fillColorSwatchColorButton = new ColorButton(fillColorSwatchButton, null);
		
		fillColorSwatchButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Color origColor = fillColorSwatchColorButton.getColor();
				ColorDialog dlg = new ColorDialog(getShell());
				if( origColor != null ) {
					RGB origRGB = origColor.getRGB();
					dlg.setRGBs(new RGB[] {origRGB});
					dlg.setRGB(origRGB);
				}
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(getShell().getDisplay(), rgb);
					fillColorSwatchColorButton.setColor(selectedColor);			
					colorSet.add(selectedColor);
					
					removeFillColorButton.setEnabled(true);
				
					applyStyleChanges();
					
					updateAddButton();
				}
			}
			
		});
		
		data = new GridData(23, 23);
		
		removeFillColorButton = new Button(backgroudStyleGroup, SWT.None);
		Image image2 = getRemoveImage();
		if(image2 != null){
			removeFillColorButton.setImage(image2);
		}
		removeFillColorButton.setLayoutData(data);
		removeFillColorButton.setEnabled(false);
		removeFillColorButton.setToolTipText("Remove color");
		removeFillColorButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				fillColorSwatchColorButton.setColor(null);
				removeFillColorButton.setEnabled(false);
				applyStyleChanges();
				updateAddButton();
			}
			
		});
					
		
		// local area button
		data = new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 3, 1);
		Composite localButtonArea = new Composite(rightComp, SWT.NONE);
		GridLayout localButtonLayout = new GridLayout();
		localButtonLayout.numColumns = 4;
		localButtonLayout.makeColumnsEqualWidth = true;
		localButtonArea.setLayout(localButtonLayout);
		localButtonArea.setLayoutData(data);

		// put blank widget
		new Label(localButtonArea, SWT.NONE);

		// add button
		data = new GridData(GridData.FILL_BOTH);
		addButton = new Button(localButtonArea, SWT.PUSH);
		addButton.setText(ADD_BUTTON);
		addButton.setLayoutData(data);
		addButton.setVisible(false);

		// cancel button
		data = new GridData(GridData.FILL_BOTH);
		cancelButton = new Button(localButtonArea, SWT.PUSH);
		cancelButton.setText(CANCEL_BUTTON);
		cancelButton.setLayoutData(data);
		cancelButton.setVisible(false);

		data = new GridData(SWT.LEFT, SWT.TOP, false, true, 3, 1);
		data.heightHint = 23;
		final Label messageLine1 = new Label(rightComp, SWT.NONE);
		messageLine1
				.setText("Style Properties are used to overwrite View default values.");
		messageLine1.setLayoutData(data);

	}

	/** Uses the os to get a list of font names
	 *
	 * @return			String[] of available font names
	 */
	private String[] getFontNames() {

		FontData[] fontDataAr = Display.getDefault().getFontList(null, false);

		Set<String> fontNames = new TreeSet<String>();

		fontNames.add("");
		if(fontDataAr != null && fontDataAr.length > 0){
			for (FontData fontData : fontDataAr) {
	
				fontNames.add(fontData.getName());
	
			}
		}
		
		fontDataAr = Display.getDefault().getFontList(null, true);

		for (FontData fontData : fontDataAr) {

			fontNames.add(fontData.getName());

		}

		return fontNames.toArray(new String[0]);

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		okButton = createButton(parent, 1, OK_BUTTON, true);
		createButton(parent, 2, IDialogConstants.CANCEL_LABEL, false);

	}

	@Override
	protected Point getInitialSize() {
		return new Point(750, 550);
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		return new Point(150, 150);
	}

	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == 1) {
			saveExistingStyleState();
			saveChangesPressed();
		} else if (buttonId == 2) {
			cancelPressed();
		}
	}

	/** When a user is ready to save, the syles are serialized and the 
	 * window is closed.
	 */
	private void saveChangesPressed() {

		//save all the changes
		styles.save();
		
		//close the window
		close();

	}

	/** Sets up the grid data object for all labels
	 * 
	 * @return			The grid data object for labels
	 */
	private GridData getLabelGridData() {

		GridData data = new GridData();
		data.widthHint = 65;

		return data;

	}

	/** Initializes the form by loading the first style if a style exist.
	 * 
	 */
	private void initilizeForm() {

		styleName.setEnabled(false);

		//if at least one style exist
		if (styles != null && styles.size() > 0) {

			loadStyle(styleList.getItem(0));
			
			//select first style
			styleList.select(0);

		} else {
			clearForm();
		}

	}

	/** Clears the form
	 * 
	 */
	private void clearForm() {

		styleList.select(-1);
		styleName.setText("");
		fontNames.setText("");
		fontColorSwatchColorButton.setColor(null);
		fontSize.setText("");
		alignment.setText("");
		boldCombo.select(getStyleIndex(null));
		italicCombo.select(getStyleIndex(null));
		underlineCombo.select(getStyleIndex(null));
		doubleUnderlineCombo.select(getStyleIndex(null));
		strikeoutCombo.select(getStyleIndex(null));
		fillColorSwatchColorButton.setColor(null);
		addButton.setEnabled(true);
		if (okButton != null) {
			okButton.setEnabled(true);
		}
		deleteButton.setEnabled(false);
		copyButton.setEnabled(false);

	}

	/** Called whenever a user changes a property.  All changes are kept in
	 * the currentStyle object until a users adds a new style, selects a 
	 * different style, or exits the dialog.
	 * 
	 */
	private void applyStyleChanges() {

		//if current style is null then create a new style to hold changes
		if (currentStyle == null) {
			currentStyle = new PafStyle();
		}

		currentStyle.setName(styleName.getText());

		if (fontNames.getText().trim().length() != 0) {
			currentStyle.setFontName(fontNames.getText());
		} else {
			currentStyle.setFontName(null);
		}
	
		if ( fillColorSwatchColorButton.getColor() != null ) {
			
			Color color = fillColorSwatchColorButton.getColor();
			
			currentStyle.setBgHexFillColor(ColorsUtil.colorToHexString(color));
			
		} else {
			currentStyle.setBgHexFillColor(null);
		}
		
		
		if ( fontColorSwatchColorButton.getColor() != null ) {
			
			Color color = fontColorSwatchColorButton.getColor();
			
			currentStyle.setFontHexColor(ColorsUtil.colorToHexString(color));
			
		} else {
			
			currentStyle.setFontHexColor(null);
			
		}

		if (fontSize.getText().trim().length() != 0) {
			currentStyle.setSize(new Integer(fontSize.getText()));
		} else {
			currentStyle.setSize(null);
		}

		if (alignment.getText().length() != 0) {
			PafAlignment pafAlignment = new PafAlignment();
			pafAlignment.setValue(alignment.getText());
			currentStyle.setAlignment(pafAlignment);
		} else {
			currentStyle.setAlignment(null);
		}
		
		
		if ( boldCombo.getText().equals(styleOptions[0] ) ) {
			currentStyle.setBold(null);
		} else {
			currentStyle.setBold( new Boolean(boldCombo.getText()));
		}
		
		if ( italicCombo.getText().equals(styleOptions[0] ) ) {
			currentStyle.setItalics(null);
		} else {
			currentStyle.setItalics( new Boolean(italicCombo.getText()));
		}
		
		if ( underlineCombo.getText().equals(styleOptions[0] ) ) {
			currentStyle.setUnderline(null);
		} else {
			currentStyle.setUnderline( new Boolean(underlineCombo.getText()));
		}
		
		if ( doubleUnderlineCombo.getText().equals(styleOptions[0] ) ) {
			currentStyle.setDoubleUnderline(null);
		} else {
			currentStyle.setDoubleUnderline( new Boolean(doubleUnderlineCombo.getText()));
		}		
		
		if ( strikeoutCombo.getText().equals(styleOptions[0] ) ) {
			currentStyle.setStrikeOut(null);
		} else {
			currentStyle.setStrikeOut( new Boolean(strikeoutCombo.getText()));
		}

	}

	
	/** Clears existing form, populates form and enables copy and delete buttons.
	 * 
	 * @param styleName			Name of style to load
	 */
	private void loadStyle(String styleName) {

		clearForm();
		populateForm((PafStyle) styles.getItem(styleName));
		copyButton.setEnabled(true);
		deleteButton.setEnabled(true);

	}

	/** Populates the form from the global style passed in.
	 * 
	 * @param style			The global style to be used to populate the form
	 */
	private void populateForm(PafStyle style) {

		styleName.setText(style.getName());

		if (style.getFontName() != null) {
			fontNames.setText(style.getFontName());
		} else {
			fontNames.clearSelection();
		}
		
		
		if ( style.getFontHexColor() != null ) {
			
			Color color = ColorsUtil.hexStringToColor(this.display, style.getFontHexColor());
			
			if ( color != null)  {
				
				colorSet.add(color);
				
				fontColorSwatchColorButton.setColor(color);
				removeFontColorButton.setEnabled(true);
								
			} else {
				
				fontColorSwatchColorButton.setColor(null);
				removeFontColorButton.setEnabled(false);
								
			}
			
			
		} else {
			
			fontColorSwatchColorButton.setColor(null);
			removeFontColorButton.setEnabled(false);
		}
		
		if ( style.getBgHexFillColor() != null ) {
			
			Color color = ColorsUtil.hexStringToColor(this.display, style.getBgHexFillColor());
			
			if ( color != null ) {
			
				colorSet.add(color);
				
				fillColorSwatchColorButton.setColor(color);
				removeFillColorButton.setEnabled(true);
				
			} else {
				
				fillColorSwatchColorButton.setColor(null);
				removeFillColorButton.setEnabled(false);
				
			}
			
			
		} else {
			
			fillColorSwatchColorButton.setColor(null);
			removeFillColorButton.setEnabled(false);
			
		}

		if (style.getSize() != null) {
			fontSize.setText(style.getSize().toString());
		}

		if (style.getAlignment() != null) {
			if ( style.getAlignment().getValue() != null ) {
				alignment.setText( style.getAlignment().getValue() );
			}
			
		}

		Boolean tmpBool = style.getBold();
				
		if (tmpBool != null) {
			boldCombo.select(getStyleIndex(tmpBool));
			
		}

		tmpBool = style.getItalics();
		if (tmpBool != null) {
			italicCombo.select(getStyleIndex(tmpBool));
		}

		tmpBool = style.getUnderline();
		if (tmpBool != null) {
			underlineCombo.select(getStyleIndex(tmpBool));
		}

		tmpBool = style.getDoubleUnderline();
		if (tmpBool != null) {
			doubleUnderlineCombo.select(getStyleIndex(tmpBool));
		}

		tmpBool = style.getStrikeOut();
		if (tmpBool != null) {
			strikeoutCombo.select(getStyleIndex(tmpBool));
		}

		// highlight style in list
		styleList.select(styles.getIndex(style.getName()));

	}

	private int getStyleIndex(Boolean booleanValue) {
		
		if ( booleanValue == null ) {
			return 0;
		} else if ( booleanValue ) {
			return 1;
		} else {
			return 2;
		}
		
	}
	
	/** Based on the button id, the approiate action will occur.
	 * 
	 * @param buttonId		Id for button being clicked
	 */
	public void buttonClicked(int buttonId) {

		switch (buttonId) {
		case NEW_BUTTON_ID:

			initializeNewStyle();
			
			break;

		case CANCEL_BUTTON_ID:

			cancelStyle();
			
			break;

		case ADD_BUTTON_ID:

			addStyle();
			
			break;

		case COPY_BUTTON_ID:

			saveExistingStyleState();

			copyStyle();
			
			break;

		case DELETE_BUTTON_ID:

			deleteStyle();

			break;
		}

	}

	/** When user clicks the new button, the form is cleared and
	 * the form is initialized for a new style
	 * 
	 */
	private void initializeNewStyle() {
		
		clearForm();

		styleName.setEnabled(true);
		styleName.setText(styles.getNextGlobalStyleName());
		styleList.setEnabled(false);
		newButton.setEnabled(false);
		deleteButton.setEnabled(false);
		addButton.setVisible(true);
		cancelButton.setVisible(true);
		copyButton.setEnabled(false);

		if (okButton != null) {
			okButton.setEnabled(false);
		}

		
	}

	/** When cancel button is selected, the changes will be backed out.
	 * 
	 */
	private void cancelStyle() {
		
		clearForm();

		newButton.setEnabled(true);
		copyButton.setEnabled(true);
		addButton.setVisible(false);
		cancelButton.setVisible(false);

		if (styles.size() > 0) {
			deleteButton.setEnabled(true);
		} else {
			deleteButton.setEnabled(false);
		}

		styleList.setEnabled(true);

		initilizeForm();

		if (okButton != null) {
			okButton.setEnabled(true);
		}
		
		currentStyle = null;
		
	}

	/** When the Add button is selected a new style will be created and added
	 * the the model manager.
	 * 
	 */
	private void addStyle() {

		String trimmedStyleName = styleName.getText().trim();
		
		PafStyle style = (PafStyle) styles.getItem(trimmedStyleName);
		
		if (style != null) {
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "'" + trimmedStyleName
					+ "' already exists.", MessageDialog.ERROR);
			return;
		}

		applyStyleChanges();

		// add new style and repopulate style
		styles.add(currentStyle.getName(), currentStyle);
		styleList.setItems(styles.getKeys());

		loadStyle(currentStyle.getName());

		currentStyle = null;

		// list
		styleList.setEnabled(true);

		styleName.setEnabled(false);

		// buttons
		newButton.setEnabled(true);
		copyButton.setEnabled(true);
		deleteButton.setEnabled(true);
		addButton.setVisible(false);
		cancelButton.setVisible(false);

		if (okButton != null) {
			okButton.setEnabled(true);
		}
		
	}

	/** When the copy button is selected, the current style will be 
	 * cloned and the form will be populated with new values.
	 */

	private void copyStyle() {

		String copyFromName = styleList.getItem(styleList
				.getSelectionIndex());
		PafStyle copyFromStyle = (PafStyle) styles.getItem(copyFromName);

		if (copyFromStyle != null) {

			try {

				PafStyle copyToStyle = (PafStyle) copyFromStyle.clone();
				copyToStyle.setName(Constants.COPY_TO + copyFromStyle.getName());

				styleList.setEnabled(false);
				styleName.setEnabled(true);

				// buttons
				newButton.setEnabled(false);
				copyButton.setEnabled(false);
				deleteButton.setEnabled(false);
				addButton.setVisible(true);
				cancelButton.setVisible(true);

				if (okButton != null) {
					okButton.setEnabled(false);
				}

				populateForm(copyToStyle);

			} catch (CloneNotSupportedException e1) {
				
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "There was a problem copying style.", MessageDialog.ERROR);
				
				e1.printStackTrace();
			}
		}
		
	}

	/** When the Delete button is selected, the selected style is deleted and if 
	 * more styles exist, the first one will be selected.
	 *  
	 */
	private void deleteStyle() {

		if (styles.size() > 0) {
			styles.remove(styleList.getItem(styleList.getSelectionIndex()));
			styleList.setItems(styles.getKeys());

			// if styles still exist after removing current style, then load
			// first element
			if (styles.size() > 0) {

				loadStyle(styleList.getItem(0));
				styleList.select(0);

			} else {

				clearForm();
				deleteButton.setEnabled(false);

			}

		}
		
	}

	/** Replaces an existing style with the current style.
	 * 
	 */
	private void saveExistingStyleState() {

		if (currentStyle != null) {

			if (currentStyle.getName() != null) {

				if (styles.contains(currentStyle.getName())) {

					styles.replace(currentStyle.getName(), currentStyle);

					currentStyle = null;

				}

			}

		}

	}


	/** Used to create a Color object from an RGB object that gets create via hex.
	 * 
	 * @param colorName		Name of color in excel
	 * @return				new Color object
	 */
	private Color createColor(String colorName) {

		Color color = null;

		String hexValue = excelColors.get(colorName);

		if (hexValue != null) {

			try {

				PafRGB pafRGBFillColor = PafRGB.hexStringToRGB(hexValue);
				RGB rgbFillColor = new RGB(pafRGBFillColor.getRed(),
						pafRGBFillColor.getGreen(), pafRGBFillColor.getBlue());
				color = new Color(display, rgbFillColor);

			} catch (Exception e) {
				
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
						"There was a problem converting hex number: '"
								+ hexValue + "' to a color.", MessageDialog.ERROR);
				e.printStackTrace();
			}

		}

		return color;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#close()
	 */
	@Override
	public boolean close() {

		for (Color colorToDispose : colorSet ) {
			
			colorToDispose.dispose();
			
		}
		
		return super.close();
	}
	

	/**
	 * 
	 * @return Image
	 */
	public Image getRemoveImage() {
		
		Image image = null;
		
		ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "remove.gif");
	
		if (imageDesc != null ) {
			image = imageDesc.createImage();
		}

		/*
		if ( image != null)  {

			  ImageData imdata = image.getImageData();
			  
			  //create the new scaled image
			  Image tempimage= new Image(Display.getCurrent(),imdata.scaledTo(14, 14));
			  
			  image = tempimage;
		}
		*/
		
		return image;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.ui.IPafMapModelManager;


/**
 * @author Jordan
 *
 */
public class RenamePlanCycleDialog extends Dialog {
	
	private String dialogTitle;
	private String oldLabel;
	private String newLabel;
	private Text oldText;
	private Text newText;
	
	private String oldName;
	private String newName;
	
	private IPafMapModelManager modelManager;

	private Label labelError;

	/**
	 * @wbp.parser.constructor
	 */
	public RenamePlanCycleDialog(Shell parentShell, String oldPlanCycleName, IPafMapModelManager modelManager,
			String oldPlanCycleLabel, String newPlanCycleLabel, String dialogTitle) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.oldName = oldPlanCycleName;
		this.modelManager = modelManager;
		this.oldLabel = oldPlanCycleLabel;
		this.newLabel = newPlanCycleLabel;
		this.dialogTitle = dialogTitle;
	}
	
	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		final Label oldPlanCycleProcessLabel = new Label(container, SWT.NONE);
		oldPlanCycleProcessLabel.setText(oldLabel);
		new Label(container, SWT.NONE);
		
		oldText = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		oldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( oldName != null ) {
			oldText.setText(oldName);
		}

		final Label newPlanCycleProcessLabel = new Label(container, SWT.NONE);
		newPlanCycleProcessLabel.setText(newLabel);
		
		labelError = new Label(container, SWT.NONE);
		ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor(Constants.SYMBOL_ERROR_ICON_PATH);
		if (imageDesc != null ) {
			labelError.setImage(imageDesc.createImage());
		}
		GridData gd_labelError = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_labelError.widthHint = 21;
		labelError.setLayoutData(gd_labelError);
		
		newText = new Text(container, SWT.BORDER);
		newText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( oldName != null ) {
			newText.setText(oldName);
		}
		
		newText.setFocus();
		newText.selectAll();
		
		addListeners();
						
		return container;
	}
	
	private void addListeners() {
		
		newText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				boolean validEntry = true;
				String newPlanCycleName = newText.getText().trim();
				if ( newPlanCycleName.equals("")) {
					validEntry = false;
					labelError.setToolTipText("");
				}
				if ( validEntry &&  modelManager != null ) {
					if( modelManager instanceof PlanCycleModelManager) {
						if (!((PlanCycleModelManager)modelManager).findDulicatePlanCycleName(newPlanCycleName)) {
							validEntry = false;
							labelError.setToolTipText("A plan cycle with that name already exists.");
						}
					}
				}	
				if( ! validEntry ) {
					labelError.setVisible(true);
				}
				else {
					labelError.setVisible(false);
					labelError.setToolTipText("");
				}
				getButton(IDialogConstants.OK_ID).setEnabled(validEntry);
			}
		});		
	}
	
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		newName = newText.getText().trim();
		super.okPressed();
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(400, 200);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(dialogTitle);
	}
	/**
	 * @return Returns the new PrintStyles name.
	 */
	public String getNewName() {
		return newName;
	}
}


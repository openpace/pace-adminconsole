/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.input.ViewPrintSettingsInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.base.PafBaseConstants;
import com.pace.base.ViewPrintState;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.ui.PrintStyle;
import com.pace.base.ui.PrintStyles;
import com.swtdesigner.ResourceManager;

/**
 * TODO: replace with javadoc
 * 
 * @version x.xx
 * @author Jason Milliron
 * 
 */
public class ViewDialog extends Dialog {
	
	//TTN 900 - Print Preference - Iris
//	private static final String LANDSCAPE = "Landscape";
//
//	private static final String PORTRAIT = "Portrait";

	//private Button addButton;
	private List selectedViewSectionsList;

	private List availableViewSectionsList;
	
	private ArrayList<String> viewList;
	
	private PafAdminConsoleView currentModelObject = null;

	private TreeMap<String, PafAdminConsoleView> modelMap = new TreeMap<String, PafAdminConsoleView>();

	static final Logger logger = Logger.getLogger(ViewDialog.class);

	// used when buttons are clicked
	private enum ButtonId {
		New, Add, Delete, Cancel, Copy, AddViewSectionName, RemoveViewSectionName, SetupViewSections, ModifyPrintStyle;
	}

	// form widgets
	private Text viewNameText;

	private Text viewDescriptionText;

	//TTN 900
//	private Text pagesWideText;
//	
//	private Text pagesTallText;
//	
//	private Combo pageOrientationCombo;
	
	// left list widget

	// Buttons

	private Label errorLabel;

	//private Button cancelButton;
	
	private Button addViewSectionButton;
	
	private Button removeViewSectionButton;
	
	private Button setupButton;
		
	private Button dialogOkButton;	

	private IProject project;

	private String initialViewName;

	private ViewModelManager viewManager;

	private ViewSectionModelManager viewSectionManager;
	
	private PafViewGroupModelManager viewGroupManager;
	
	private boolean openWithNewView = false;
	
	private boolean copyToNewView = false;

	// used to see if form should be changed when form item changes
	private boolean cachingEnabled = false;
	
	//TTN 900 - Print Preference - Iris
//	private String[] pageOrientationItems = { "", PORTRAIT, LANDSCAPE };
	
	//TTN 900 - Print Preference - Iris
	private Combo printStyleCombo;
	private Button btnModify; 
	private PrintStyles printStylesManager;
	/**
	 * @param parentShell 	Parents shell
	 * @param project	 Project
	 * @param initialViewName If not null, the dialog will open on this view
	 * @wbp.parser.constructor
	 */
	@SuppressWarnings("unchecked")
	public ViewDialog(Shell parentShell, IProject iProject, String initialViewName) {
	
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.project = iProject;
	
		this.initialViewName = initialViewName;
		
		viewList = new ArrayList<String>();
		
		// Create view Manager
		viewManager = new ViewModelManager(iProject);
	
		//get model map from view manager and cast to a tree map
		modelMap = (TreeMap<String, PafAdminConsoleView>) viewManager.getModel();
	
		// create view section Manager
		viewSectionManager = new ViewSectionModelManager(iProject);
		
		viewGroupManager = new PafViewGroupModelManager(iProject);

		//TTN 900 - Print Preference - Iris
		printStylesManager= new PrintStylesManager(iProject);
	}
	
	/**
	 * @param parentShell 	Parents shell
	 * @param project	 Project
	 * @param initialViewName If not null, the dialog will open on this view
	 * @param openWithNewView	If true, the dialog opens on a new record
	 */
	public ViewDialog(Shell parentShell,IProject project,
			String initialViewName, boolean openWithNewView) {
		this(parentShell, project, initialViewName);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.initialViewName = initialViewName;
		this.openWithNewView = openWithNewView;
	}
	
	/**
	 * 
	 * @param parentShell Parents shell
	 * @param project Project
	 * @param copyToNewView Copy the view to a new view.
	 * @param copyFromView The source view to copy.
	 */
	public ViewDialog(Shell parentShell, IProject project, 
			boolean copyToNewView,
			String copyFromView) {
		this(parentShell, project, copyFromView);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.initialViewName = copyFromView;
		this.openWithNewView = false;
		this.copyToNewView = copyToNewView;
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout_1 = new GridLayout();
		container.setLayout(gridLayout_1);

		final Composite compositeView = new Composite(container, SWT.NONE);
		final GridData gd_compositeView = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_compositeView.widthHint = 623;
		compositeView.setLayoutData(gd_compositeView);
		compositeView.setLayout(new GridLayout());

		final Group viewsGroup = new Group(compositeView, SWT.NONE);
		viewsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		viewsGroup.setText("Views");
		viewsGroup.setLayout(new GridLayout());

		final Composite infoComposite = new Composite(viewsGroup, SWT.NONE);
		final GridData gd_infoComposite = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd_infoComposite.widthHint = 163;
		gd_infoComposite.heightHint = 90;
		infoComposite.setLayoutData(gd_infoComposite);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.numColumns = 3;
		infoComposite.setLayout(gridLayout_6);

		final Label nameLabel = new Label(infoComposite, SWT.NONE);
		nameLabel.setText("Name");

		errorLabel = new Label(infoComposite, SWT.NONE);
		errorLabel.setVisible(false);
		errorLabel.setToolTipText("A View with that name already exists.");
		errorLabel.setText("_");
		ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor(Constants.SYMBOL_ERROR_ICON_PATH);
		if (imageDesc != null ) {
			errorLabel.setImage(imageDesc.createImage());
		}

		viewNameText = new Text(infoComposite, SWT.BORDER);
		final GridData gd_viewNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_viewNameText.widthHint = 40;
		viewNameText.setLayoutData(gd_viewNameText);
		viewNameText.setTextLimit(150);
		viewNameText.setTabs(1);
		viewNameText.setEnabled(false);

		final Label descriptionLabel = new Label(infoComposite, SWT.NONE);
		final GridData gd_descriptionLabel = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gd_descriptionLabel.widthHint = 83;
		descriptionLabel.setLayoutData(gd_descriptionLabel);
		descriptionLabel.setText("Description");

		final Label placeHolder = new Label(infoComposite, SWT.NONE);
		placeHolder.setVisible(false);
		placeHolder.setText("_");

		viewDescriptionText = new Text(infoComposite, SWT.MULTI | SWT.BORDER);
		final GridData gd_viewDescriptionText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_viewDescriptionText.heightHint = 49;
		gd_viewDescriptionText.widthHint = 43;
		viewDescriptionText.setLayoutData(gd_viewDescriptionText);
		viewDescriptionText.setTextLimit(500);
		viewDescriptionText.setTabs(0);

		final Composite compositePage = new Composite(viewsGroup, SWT.NONE);
		GridData gd_compositePage = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_compositePage.widthHint = 374;
		compositePage.setLayoutData(gd_compositePage);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 3;
		compositePage.setLayout(gridLayout_3);

		final Label lblPrintStyle = new Label(compositePage, SWT.NONE);
		lblPrintStyle.setText("Print Style");
		
		printStyleCombo = new Combo(compositePage, SWT.NONE);
		GridData gd_printStyleCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_printStyleCombo.widthHint = 253;
		printStyleCombo.setLayoutData(gd_printStyleCombo);
		
		btnModify = new Button(compositePage, SWT.NONE);
		btnModify.setText("Modify");

		final Composite compositeViewSections = new Composite(viewsGroup, SWT.NONE);
		final GridData gd_compositeViewSections = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_compositeViewSections.heightHint = 352;
		compositeViewSections.setLayoutData(gd_compositeViewSections);
		compositeViewSections.setLayout(new GridLayout());

		final Group viewSectionsGroup = new Group(compositeViewSections, SWT.NONE);
		final GridData gd_viewSectionsGroup = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_viewSectionsGroup.heightHint = 282;
		gd_viewSectionsGroup.widthHint = 310;
		viewSectionsGroup.setLayoutData(gd_viewSectionsGroup);
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 3;
		viewSectionsGroup.setLayout(gridLayout_4);
		viewSectionsGroup.setText("View Sections");

		final Label availableViewSectionsLabel = new Label(viewSectionsGroup, SWT.NONE);
		availableViewSectionsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		availableViewSectionsLabel.setText("Available View Sections");
		new Label(viewSectionsGroup, SWT.NONE);

		final Label availableViewSectionsLabel_1 = new Label(viewSectionsGroup, SWT.NONE);
		availableViewSectionsLabel_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		availableViewSectionsLabel_1.setText("Selected View Sections");

		availableViewSectionsList = new List(viewSectionsGroup, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		final GridData gd_availableViewSectionsList = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_availableViewSectionsList.heightHint = 102;
		gd_availableViewSectionsList.widthHint = 130;
		availableViewSectionsList.setLayoutData(gd_availableViewSectionsList);

		final Composite compositeViewSectionButtons = new Composite(viewSectionsGroup, SWT.NONE);
		final GridData gd_compositeViewSectionButtons = new GridData(SWT.RIGHT, SWT.CENTER, false, true);
		gd_compositeViewSectionButtons.heightHint = 69;
		compositeViewSectionButtons.setLayoutData(gd_compositeViewSectionButtons);
		compositeViewSectionButtons.setLayout(new GridLayout());

		addViewSectionButton = new Button(compositeViewSectionButtons, SWT.NONE);
		addViewSectionButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/right_arrow.gif"));
		addViewSectionButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		addViewSectionButton.setEnabled(false);

		removeViewSectionButton = new Button(compositeViewSectionButtons, SWT.NONE);
		removeViewSectionButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/left_arrow.gif"));
		removeViewSectionButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		removeViewSectionButton.setEnabled(false);

		selectedViewSectionsList = new List(viewSectionsGroup, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		final GridData gd_selectedViewSectionsList = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_selectedViewSectionsList.widthHint = 142;
		selectedViewSectionsList.setLayoutData(gd_selectedViewSectionsList);

		final Composite compositeButtons = new Composite(compositeViewSections, SWT.NONE);
		final GridData gd_compositeButtons = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		gd_compositeButtons.widthHint = 160;
		compositeButtons.setLayoutData(gd_compositeButtons);
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.marginRight = 0;
		compositeButtons.setLayout(gridLayout_5);
		
		//		addButton = new Button(compositeButtons, SWT.NONE);
		//		addButton.setEnabled(false);
		//		addButton.setVisible(false);
		//		final GridData gd_addButton = new GridData(SWT.RIGHT, SWT.TOP, false, false);
		//		gd_addButton.widthHint = 90;
		//		addButton.setLayoutData(gd_addButton);
		//		addButton.setText("Add");
		
		setupButton = new Button(compositeButtons, SWT.NONE);
		final GridData gd_setupButton = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gd_setupButton.widthHint = 155;
		setupButton.setLayoutData(gd_setupButton);
		setupButton.setEnabled(false);
		setupButton.setText("Create View Section...");

		// initially populate the list items.
		refreshList();

		// set up dialog
		initializeDialog();

		// set up listeners
		setupListeners();
		
		//if initial view is selected, populate form and select in list
		if ( initialViewName != null && !openWithNewView) {
			populateForm(initialViewName);
		} 
		
		//if open with new view, run new button action to open a blank view
		if (openWithNewView) {
			
			newButtonAction();
			
			viewNameText.setText(viewNameText.getText());
			
		}else if(copyToNewView){
			copyButtonAction();
		}
		
		//TTN 900 - Print Preference - Iris
		populatePrintStyles();
		
		return container;
	}
	
	protected void createButtonsForButtonBar(Composite parent) {
		dialogOkButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		if(copyToNewView && errorLabel.getVisible()){
			dialogOkButton.setEnabled(false);
		}else{
			dialogOkButton.setEnabled(true);
		}
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		if (openWithNewView) {
			
			viewNameText.setText(viewNameText.getText());
		
		}
	}


	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("View");
	}

	/**
	 * Initials the form.
	 */
	private void initializeDialog() {

		// if model map contains items
		if (modelMap.size() > 0) {
			enableForm(true, false);
		} else {
			//list.deselectAll();
			//list.setEnabled(true);
			clearForm();
			enableForm(false, true);
			//copyButton.setEnabled(false);
			//deleteButton.setEnabled(false);
			removeViewSectionsNamesFromLists();
		}

	}

	/**
	 * Populates the form from the model map using the key param
	 * 
	 * @param key
	 *            used to get object from the model map
	 */
	private void populateForm(String key) {

		// if the model map has the key, populate the form with it
		if (modelMap.containsKey(key)) {

			//turn off caching
			cachingEnabled = false;

			//disable view name text box
			viewNameText.setEnabled(false);

			//get model from map
			PafAdminConsoleView model = modelMap.get(key);

			//set new view name from model's name
			viewNameText.setText(model.getName());

			//set the view description from model
			if(model.getDesc() != null){
				
				viewDescriptionText.setText(model.getDesc());
			
			}
			
			//TTN 900 - Iris
//			if ( model.getPageOrientation() != null ) {
//				
//				pageOrientationCombo.setText(model.getPageOrientation());
//				
//			} else {
//				
//				pageOrientationCombo.setText("");
//				
//			}
//
//			if ( model.getPagesTall() != null ) {
//			
//				pagesTallText.setText(model.getPagesTall().toString());
//			
//			} else {
//				
//				pagesTallText.setText("");
//				
//			}
//			
//			if ( model.getPagesWide() != null ) {
//				
//				pagesWideText.setText(model.getPagesWide().toString());
//				
//			} else {
//				
//				pagesWideText.setText("");
//				
//			}
			
			//turn caching back on
			cachingEnabled = true;

			//set current model object to local object
			currentModelObject = model;
			
			//populate the view sections list boxes
			populateViewSections();

		}

	}

	/**
	 * Sets up all the listeners
	 * 
	 */
	private void setupListeners() {

		// setup the list listener
		setupListListener();

		// setup all form listeners
		setupFormListeners();

		// setup all button listeners
		setupButtonListeners();

	}

	/**
	 * Sets up the listener for the list box
	 * 
	 */
	private void setupListListener() {

	}

	/**
	 * Sets up the listeners for the form widgets
	 * 
	 */
	private void setupFormListeners() {

		viewNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				// get and trim key
				String key = viewNameText.getText().trim();
				if(viewNameText.getEnabled()){
					// if key already in model map or blank or in view group, disable add button
					if (modelMap.containsKey(key) || key.length() == 0 || viewGroupManager.containsIgnoreCase(key) || key.startsWith(".")) {
						if(dialogOkButton != null){
							dialogOkButton.setEnabled(false);
						}
						errorLabel.setVisible(true);
						//addButton.setEnabled(false);
					} else {
						if(dialogOkButton != null){
							dialogOkButton.setEnabled(true);
						}
						errorLabel.setVisible(false);
						//addButton.setEnabled(true);
					}
				}
			}
		});

		viewDescriptionText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				// data has been changed
				formDataChanged();

			}

		});
		
		//TTN 900 - Iris
//		pageOrientationCombo.addModifyListener(new ModifyListener() {
//
//			public void modifyText(ModifyEvent e) {
//
//				// data has been changed
//				formDataChanged();
//
//			}
//
//		});
//
//		pagesWideText.addModifyListener(new ModifyListener() {
//
//			public void modifyText(ModifyEvent e) {
//
//				// data has been changed
//				formDataChanged();
//
//			}
//
//		});
//		
//		pagesWideText.addVerifyListener(new VerifyListener() {
//
//			public void verifyText(VerifyEvent e) {
//								
//				
//				verifyOnlyNumeric(e);
//								
//				
//			}
//			
//		});
//		
//		pagesTallText.addModifyListener(new ModifyListener() {
//
//			public void modifyText(ModifyEvent e) {
//
//				// data has been changed
//				formDataChanged();
//
//			}
//
//		});
//		
//		pagesTallText.addVerifyListener(new VerifyListener() {
//
//			public void verifyText(VerifyEvent e) {
//				
//								
//				verifyOnlyNumeric(e);
//							
//				
//			}
//			
//		});
		
		availableViewSectionsList.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				
				//deselect all selections from selected view section list
				selectedViewSectionsList.deselectAll();
				
				//update the view section right and left arrow buttons
				updateViewSectionButtons();
				
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				
				//get selected item from available view sections list
				//String selectedItem = getSelectedItemFromList(availableViewSectionsList);
				
				if(addViewSectionButton.getEnabled()){
					buttonSelected(ButtonId.AddViewSectionName);
				}
				
				//open view section dialog
				//openViewSectionDialog(selectedItem);
				
				//get index from list for selected item
				//int selectedItemIndex = getIndexFromList(availableViewSectionsList, selectedItem);

				//reselect the previously selected item
				//availableViewSectionsList.select(selectedItemIndex);
								
			}
			
		});
		
		selectedViewSectionsList.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				
				availableViewSectionsList.deselectAll();
				updateViewSectionButtons();
				
				
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				
				String selectedItem = getSelectedItemFromList(selectedViewSectionsList);
				
				openViewSectionDialog(selectedItem);
				
				int selectedItemIndex = getIndexFromList(selectedViewSectionsList, selectedItem);
				
				selectedViewSectionsList.select(selectedItemIndex);
				
			}
			
		});

	}
	

	protected void openViewSectionDialog(String viewSectionName) {
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewSectionDialog.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
					return;
			}
			
			ViewSectionDialog vsDialog = null;
			
			//if view section name is null and the size of view sections is 0, open on true
			if ( viewSectionName == null ) {
				if( viewSectionManager.size() == 0 ) {
					vsDialog = new ViewSectionDialog(
							setupButton.getShell(), project, viewSectionName, true);
				}
				else {
			    	vsDialog = new ViewSectionDialog(
			    			setupButton.getShell(), project, null, true);
				}
			} else {
				String selectedItem = getSelectedItemFromList(selectedViewSectionsList);
				if( selectedItem == null ) {
			    	vsDialog = new ViewSectionDialog(
			    			setupButton.getShell(), project, null, true);
				}
				//else open on first elemenet in view section list
				else {
					vsDialog = new ViewSectionDialog(
							setupButton.getShell(), project, viewSectionName);
				}
			}		
			
			//open view section dialog
			vsDialog.open();
			
			//get new instance of view section model manager
			viewSectionManager = new ViewSectionModelManager(project);
			
			//re-populate view sections
			populateViewSections();
		
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Called when any the form data changes
	 * 
	 */
	private void formDataChanged() {

		// if caching is enabled, cach the form
		if (cachingEnabled) {
			cacheForm();
		}

	}

	/**
	 * Caches the form to a temp object model
	 * 
	 */
	private void cacheForm() {

		logger.debug("Caching form");

		// if current model object is null, create one
		if (currentModelObject == null) {

			currentModelObject = new PafAdminConsoleView();

		}

		currentModelObject.setName(viewNameText.getText().trim());

		currentModelObject.setDesc(viewDescriptionText.getText().trim());
		
		//TTN 900 - Iris
//		if ( ! pageOrientationCombo.getText().trim().equals("") ) {
//			
//			currentModelObject.setPageOrientation(pageOrientationCombo.getText().trim());
//			
//		} else {
//			
//			currentModelObject.setPageOrientation(null);
//			
//		}
//		
//		
//		
//		if ( pagesTallText.getText().trim().length() > 0 ) {
//			currentModelObject.setPagesTall(new Integer(pagesTallText.getText().trim()));
//		} else {
//			currentModelObject.setPagesTall(null);
//		}
//		
//		if ( pagesWideText.getText().trim().length() > 0 ) {
//			currentModelObject.setPagesWide(new Integer(pagesWideText.getText().trim()));
//		} else {
//			currentModelObject.setPagesWide(null);
//		}

	}

	/**
	 * Sets up the listeners for the buttons
	 * 
	 */
	private void setupButtonListeners() {

		// new button listener

		// copy button listener
		
		// delete button listener

		// add button listener

		// cancel button listener
		//setupButtonSelectionListener(cancelButton, ButtonId.Cancel);
		
		// add view section name button listener
		setupButtonSelectionListener(addViewSectionButton, ButtonId.AddViewSectionName);
		
		// remove view section name button listener
		setupButtonSelectionListener(removeViewSectionButton, ButtonId.RemoveViewSectionName);

		// setup view section name button listener
		setupButtonSelectionListener(setupButton, ButtonId.SetupViewSections);
		
		setupButtonSelectionListener(btnModify, ButtonId.ModifyPrintStyle);
	}

	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}

	/**
	 * Used to perfrom an action when a button is pressed
	 * 
	 * @param buttonId
	 *            enum for which button was selected
	 */
	private void buttonSelected(ButtonId buttonId) {

		logger.debug(buttonId + " button clicked");
		
		switch (buttonId) {

		case Add:
			
			addButtonAction();
			cachingEnabled = true;
			break;

//		case Copy:
//			
//			cachingEnabled = false;
//			copyButtonAction();
//			cachingEnabled = true;
//			break;
//			
//		case Delete:
//			
//			deleteButtonAction();
//
//			break;
//
//		case New:
//			
//			newButtonAction();
//			cachingEnabled = false;
//			break;

//		case Cancel:
//			
//			cancelButtonAction();
//			cachingEnabled = true;
//			break;

		case AddViewSectionName:
			
			addViewSectionButtonAction();
			break;
			
		case RemoveViewSectionName:
			
			removeViewSectionButtonAction();
			break;
		
		case SetupViewSections:
			
			String selectedItem = getSelectedItemFromList(selectedViewSectionsList);
			String availableItem = getSelectedItemFromList(availableViewSectionsList);
			
			if(selectedItem != null){
				openViewSectionDialog(selectedItem);
			} else if(availableItem != null){
				openViewSectionDialog(availableItem);
			} else{
				openViewSectionDialog(null);
			}
			
			//openViewSectionDialog(null);
			break;
		
			//TTN 900 - Print Preferences - Iris
		case ModifyPrintStyle:
			
			modifyPrintStyleButtonAction();
			break;
		
}

	}

//	private void copyButtonAction() {
//
//		//save last changes
//		saveCachedModel();
//				
//		String key = list.getSelection()[0];
//		
//		if ( key != null ) {
//						
//			try {
//				
//				currentModelObject = (PafAdminConsoleView) modelMap.get(key).clone();
//				
//				String newKey = "Copy of " + key;
//				
//				currentModelObject.setName(newKey);
//
//				viewNameText.setText(newKey);
//				
//				list.setEnabled(false);
//				list.deselectAll();
//
//				enableForm(true, true);
//
//				populateViewSections();
//
//				viewNameText.setFocus();
//				
//				newButton.setEnabled(false);
//				copyButton.setEnabled(false);
//				deleteButton.setEnabled(false);
//
//				addButton.setVisible(true);
//				
//				//if model map already contains new key, then disable add button
//				if ( modelMap.containsKey(newKey)) {
//					addButton.setEnabled(false);
//				} else {
//					addButton.setEnabled(true);
//				}					
//
//				cancelButton.setVisible(true);
//				cancelButton.setEnabled(true);
//				
//				
//			} catch (CloneNotSupportedException e) {
//				logger.error("Could not clone view: " + key);
//				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "There was a problem trying to copy view: " + key, MessageDialog.ERROR);
//				e.printStackTrace();
//			}
//			
//			
//		}	
//		
//		
//	}

	
	private void copyButtonAction() {
				
			String key = null;
			if(viewNameText != null){
				key = viewNameText.getText().trim();
			}else{
				logger.error("Could not clone view.");
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "There was a problem trying to copy view.", MessageDialog.ERROR);;
				return;
			}
			
			if ( key != null ) {
							
				try {
					
					currentModelObject = (PafAdminConsoleView) modelMap.get(key).clone();
					
					String newKey = Constants.COPY_TO + key;
					
					currentModelObject.setName(newKey);
	
					viewNameText.setText(newKey);
	
					enableForm(true, true);
	
					populateViewSections();
	
					viewNameText.setFocus();
					
					// if key already in model map or blank or in view group, disable add button
					if(viewNameText.getEnabled()){
						if (modelMap.containsKey(newKey) || newKey.length() == 0 || newKey.startsWith(".")) {
							if(dialogOkButton != null){
								dialogOkButton.setEnabled(false);
							}
							errorLabel.setVisible(true);
							//addButton.setEnabled(false);
						} else {
							if(dialogOkButton != null){
								dialogOkButton.setEnabled(true);
							}
							errorLabel.setVisible(false);
							//addButton.setEnabled(true);
						}
					}
	
				} catch (Exception e) {
					logger.error("Could not clone view: " + key);
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "There was a problem trying to copy view: " + key, MessageDialog.ERROR);
					e.printStackTrace();
				}
			}	
		}
	
	
	
	/**
	 * Action called when new button was pressed
	 */
	private void newButtonAction() {

		//set current model object to null
		currentModelObject = new PafAdminConsoleView();
		
		enableForm(true, true);

		clearForm();

		setFormDefaults();
		
		populateViewSections();
		
		viewNameText.setFocus();
	}

	/**
	 * Sets the form defaults.
	 */
	private void setFormDefaults(){
		if(this.initialViewName != null && this.initialViewName.trim().length() > 0){
			viewNameText.setText(this.initialViewName.trim());
			//create and add the new view section.
			Set<String> currentViewSectionNames = new HashSet<String>();
			currentViewSectionNames.add(this.initialViewName.trim());
			currentModelObject.setViewSectionNames(currentViewSectionNames);
		}
		//TTN 900
//		pagesTallText.setText("1");
//		pagesWideText.setText("1");
//		pageOrientationCombo.setText(LANDSCAPE);
		viewNameText.setFocus();
		if(viewNameText != null && viewNameText.getText().trim().length() > 0){
			viewNameText.selectAll();
		}
	}
	
	
//	/**
//	 * Action called when delete button was pressed
//	 */
//	private void deleteButtonAction() {
//
//		// if selection count is greater than 0, delete item
//		if (list.getSelectionCount() > 0) {
//
//			String key = list.getSelection()[0];
//
//			if (modelMap.containsKey(key)) {
//
//				modelMap.remove(key);
//				list.removeAll();
//				refreshList();
//				initializeDialog();
//
//			}
//
//		} else {
//
//			list.deselectAll();
//
//		}
//
//	}

	/**
	 * Clears the current form.
	 */
	private void clearForm() {

		cachingEnabled = false;

		viewNameText.setText("");
		viewDescriptionText.setText("");
		//TTN 900
//		pageOrientationCombo.setText("");
//		pagesTallText.setText("");
//		pagesWideText.setText("");

		cachingEnabled = true;
	}

//	/**
//	 * Action called when cancel button was pressed
//	 */
//	private void cancelButtonAction() {
//
//		//list.setEnabled(true);
//
//		enableForm(false, true);
//
//		//newButton.setEnabled(true);
//		//copyButton.setEnabled(true);
//		//deleteButton.setEnabled(true);
//
//		addButton.setVisible(false);
//		//cancelButton.setVisible(false);
//
//		initializeDialog();
//
//	}

	/**
	 * Action called when add button was pressed
	 */
	private void addButtonAction() {

		//get key from text box
		String key = viewNameText.getText().trim();
		
		viewNameText.setText(key);
		
		//create an empty model
		PafAdminConsoleView model = new PafAdminConsoleView();

		//set name and desc
		model.setName(key);
		model.setDesc(viewDescriptionText.getText().trim());
		
		//TTN 900 - Print Style - Iris
//		if ( ! pageOrientationCombo.getText().trim().equals("") ) {
//			
//			model.setPageOrientation(pageOrientationCombo.getText().trim());
//			
//		} else {
//			
//			model.setPageOrientation(null);
//		}
//		
//		if ( pagesTallText.getText().trim().length() > 0 ) {
//			model.setPagesTall(new Integer(pagesTallText.getText().trim()));
//		} else {
//			model.setPagesTall(null);
//		}
//		
//		if ( pagesWideText.getText().trim().length() > 0 ) {
//			model.setPagesWide(new Integer(pagesWideText.getText().trim()));
//		} else {
//			model.setPagesWide(null);
//		}
					
		//if there are view sections in the selected view section list, add to model
		if ( selectedViewSectionsList.getItemCount() > 0 ) {
						
			//create an empty set
			Set<String> viewSectionNames = new TreeSet<String>();
			
			//add the first item from the selected view section list to set
			viewSectionNames.add(selectedViewSectionsList.getItem(0));
			
			//update model
			model.setViewSectionNames(viewSectionNames);
		}
		
		if ( currentModelObject != null && currentModelObject.getHeaders() != null) {
			
			model.setHeaders(currentModelObject.getHeaders());
			
		}
		
		//add to model map
		modelMap.put(key, model);		
	
		//refresh list to include new model
		refreshList();
		
		//populate form with new model
		populateForm(key);		
		//cancelButton.setVisible(false);

	}

	/**
	 * Removes all entries in the list and repopulates with the keyset from the
	 * model map
	 */
	private void refreshList() {
		viewList.clear();
		viewList.addAll(modelMap.keySet());
	}

	/**
	 * Enables the form fields
	 * 
	 * @param enable
	 *            enables/disables the form fields
	 * @param includePrimaryKey
	 *            include the primary key field when enabling/disabling form
	 */
	private void enableForm(boolean enable, boolean includePrimaryKey) {

		if (includePrimaryKey) {
			viewNameText.setEnabled(enable);
		}

		viewDescriptionText.setEnabled(enable);
		
		//TTN 900 - Print Style - Iris
//		pageOrientationCombo.setEnabled(enable);
//		
//		pagesTallText.setEnabled(enable);
//		
//		pagesWideText.setEnabled(enable);
		
		availableViewSectionsList.setEnabled(enable);
		
		selectedViewSectionsList.setEnabled(enable);
		
		setupButton.setEnabled(enable);		

	}

//	/**
//	 * Used to get the index of the key in the list
//	 * 
//	 * @returns int index of key in list
//	 */
//	private int getListItemIndex(String key) {
//
//		int index = 0;
//
//		for (String item : list.getItems()) {
//
//			if (item.equals(key)) {
//				break;
//			}
//
//			index++;
//		}
//
//		return index;
//	}

	@Override
	protected void okPressed() {
		
		saveCachedModel();
		ArrayList<String> viewsToBeSaved = new ArrayList<String>();
		viewsToBeSaved.add(viewNameText.getText());
		viewManager.save(viewsToBeSaved);

		super.okPressed();

	}
	
	private void savePrintStyle() {
		//TTN 900 - save print style changes 
		String selectedPrintStyleName = printStyleCombo.getText();
		if( selectedPrintStyleName != null && ! selectedPrintStyleName.isEmpty() && selectedPrintStyleName.length() != 0 ) {
			PafAdminConsoleView view = (PafAdminConsoleView)viewManager.getItem(viewNameText.getText());
			if( selectedPrintStyleName.equalsIgnoreCase(PafBaseConstants.EMBEDED_PRINT_SETTINGS )) {
				view.setViewPrintState(ViewPrintState.LOCAL);
				view.setGlobalPrintStyleGUID(null);
			}
			else if( selectedPrintStyleName.endsWith(Constants.DEFAULT_MARKER )) {
				if( openWithNewView )
					view.setPrintStyle(printStylesManager.getDefaultPrintStyle());
				view.setViewPrintState(ViewPrintState.DEFAULT);
				view.setGlobalPrintStyleGUID(null);
			}
			else {
				view.setViewPrintState(ViewPrintState.GLOBAL);
				PrintStyle printStyle = printStylesManager.getPrintStyleByName(selectedPrintStyleName);
				if( printStyle != null ) {
					view.setGlobalPrintStyleGUID(printStyle.getGUID());
				}
			}
		}
	}
	/**
	 * Saves the cached model objects.
	 */
	private void saveCachedModel() {

		if (currentModelObject != null) {

			String key1 = currentModelObject.getName();
			String key = viewNameText.getText();
			
			if (key != null && modelMap.containsKey(key)) {//if not new
				logger.debug("Updating model map for key " + key);
				modelMap.put(key, currentModelObject);
				currentModelObject = null;
			} else {//if new or copy
				if( copyToNewView ) { //copy
					if( ! key1.equals(key) ) { //view name is changed in copy view windown
						currentModelObject.setName(key);
					}
					modelMap.put(key, currentModelObject);
				}
				if( openWithNewView ) {//new
					addButtonAction();
				}
				//if ( addButton.isEnabled() && addButton.isVisible()) {
										
					//modelMap.put(key, currentModelObject);
					
				//}
			}
			savePrintStyle();
		} 

	}

	private void populateViewSections() {

		removeViewSectionsNamesFromLists();

		// get view section names
		String[] keys = viewSectionManager.getKeys();

		ArrayList<String> availableViewSectionNames = new ArrayList<String>();

		// if keys not null, populate view section names
		if (keys != null) {

			for (String key : keys) {

				availableViewSectionNames.add(key);

			}

		}
		
		//populate the available view section list box
		populateListBox(availableViewSectionsList,
				availableViewSectionNames.toArray(new String[0]));

		//get the set of view section names
		Set<String> currentViewSectionNames = currentModelObject
				.getViewSectionNames();				

		//see if the view has a view section tied to it, if so, apply
		if (currentViewSectionNames != null
				&& currentViewSectionNames.size() > 0) {
			
			//currently, just get the first one
			String currentModelViewSectionName = currentViewSectionNames
					.toArray(new String[0])[0];
			
			//if current model has a view section name
			if ( currentModelViewSectionName != null ) {
				
				//get view section name index from availaible list 
				int viewSectionNameIndex = getIndexFromList(availableViewSectionsList, currentModelViewSectionName);
				
				//if a view section exist, populate selected list box
				if ( viewSectionNameIndex >= 0 ) {
					
					//remove from the available view sections
					availableViewSectionsList.remove(viewSectionNameIndex);
					
					//populate the selected view sections with new view section name
					populateListBox(selectedViewSectionsList,
							new String[] { currentModelViewSectionName });
					
					selectedViewSectionsList.setSelection(new String[] { currentModelViewSectionName });
					
					updateViewSectionButtons();
				//if view section name is on view, but view section doesn't exist
				} else {
					
					//remove view section name from the set
					currentViewSectionNames.remove(currentModelViewSectionName);
					
					//set updated view section set to current model
					currentModelObject.setViewSectionNames(currentViewSectionNames);
					
					
				}
				
			}
			
		}	

	}

	//TTN 900 - Iris
	private void populatePrintStyles() {
		
		String[] names = printStylesManager.getNames(true);
		ArrayList<String> availablePrintStyles = new ArrayList<String>();
		availablePrintStyles.add(PafBaseConstants.EMBEDED_PRINT_SETTINGS);
		// if keys not null, populate view section names
		if (names != null) {
			for (String name : names) {
				availablePrintStyles.add(name);
			}
		}
		printStyleCombo.setItems(availablePrintStyles.toArray(new String[0]));
		
		//if this is a new View, assign the default print style to the view 
		if( openWithNewView ) {
			
			PrintStyle defaultStyle = printStylesManager.getDefaultPrintStyle();
			if( defaultStyle != null )
				printStyleCombo.setText(defaultStyle.getName()+Constants.DEFAULT_MARKER);
		}
		else {
			//using embeded print style
			if( currentModelObject.getViewPrintState() == ViewPrintState.LOCAL ) {
				//populate the available print styles in combo section list box
				printStyleCombo.setText(PafBaseConstants.EMBEDED_PRINT_SETTINGS);
			}
			//using a global print style
			else if(currentModelObject.getViewPrintState() == ViewPrintState.GLOBAL) {
				PrintStyle globalStyle = printStylesManager.getPrintStyleByGUID(currentModelObject.getGlobalPrintStyleGUID());
				if( globalStyle != null ) {
					printStyleCombo.setText(globalStyle.getName());
				}
			}
			//using a default print style
			else if(currentModelObject.getViewPrintState() == ViewPrintState.DEFAULT) {
				PrintStyle defaultStyle = printStylesManager.getDefaultPrintStyle();
				if( defaultStyle != null ) {
					printStyleCombo.setText(defaultStyle.getName() + Constants.DEFAULT_MARKER);
				}
			}
		}
	}

	private int getIndexFromList(List list, String searchString) {
		
		//set not found index to -1
		int index = -1;
		
		//get items from list
		String[] items = list.getItems();
		
		//loop through items and see if one matches search string
		for (int i = 0; i < items.length; i++) {
			
			if ( items[i].equals(searchString)) {
				index = i;
				break;
			}
			
		}
		
		//return index of either found or not found item.
		return index;
	}

	private int getIndexFromCombo(Combo combo, String searchString) {
		
		//set not found index to -1
		int index = -1;
		
		//get items from list
		String[] items = combo.getItems();
		
		//loop through items and see if one matches search string
		for (int i = 0; i < items.length; i++) {
			
			if ( items[i].equals(searchString)) {
				index = i;
				break;
			}
			
		}
		
		//return index of either found or not found item.
		return index;
	}

	private void populateListBox(List listBox, String[] values) {

		//set items in list
		listBox.setItems(values);

	}

	private void removeViewSectionsNamesFromLists() {

		//remove all items form the view section lists
		availableViewSectionsList.removeAll();
		selectedViewSectionsList.removeAll();

	}
	
	
	private void addViewSectionButtonAction() {
		
		//get selected item from available list
		String selectedItem = getSelectedItemFromList(availableViewSectionsList);
		
		//get view section name set from current model object
		Set<String> currentViewSectionNames = currentModelObject.getViewSectionNames();
		
		//add new view section name to set
		currentViewSectionNames.add(selectedItem);
		
		//set the new view section name set
		currentModelObject.setViewSectionNames(currentViewSectionNames);
		
		//re-populate view sections
		populateViewSections();
		
		//get selected item index
		int itemIndex = getIndexFromList(selectedViewSectionsList, selectedItem);
		
		//select itm in selected views sections list
		selectedViewSectionsList.select(itemIndex);
		
		updateViewSectionButtons();
		
		
	}
	
	private String getSelectedItemFromList(List list) {
		
		//get selected items from list box
		String[] selectedItems = list.getSelection();
		
		//set up a selected item string of null
		String selectedItem = null;
		
		//if selected items exist, set teh selected item to the first item in the selected items ar
		if ( selectedItems.length > 0 ) {
		
			selectedItem = selectedItems[0];
			
		}
		
		//return selected items
		return selectedItem;
	}

	private void removeViewSectionButtonAction() {		
		
		//get selected item from list
		String selectedItem = getSelectedItemFromList(selectedViewSectionsList);
				
		//get current view section name set from current model
		Set<String> currentViewSectionNames = currentModelObject.getViewSectionNames();
		
		//remove view section name from set
		currentViewSectionNames.remove(selectedItem);
		
		//set the new view section name set to the current model
		currentModelObject.setViewSectionNames(currentViewSectionNames);
		
		//populate the view sections based on model
		populateViewSections();
		
		//get the item index for the newly added item
		int itemIndex = getIndexFromList(availableViewSectionsList, selectedItem);
		
		//select the newly added item
		availableViewSectionsList.select(itemIndex);
		
		//update the View section buttons
		updateViewSectionButtons();
		
	}

	protected void updateViewSectionButtons() {
		
		if ( selectedViewSectionsList.getItemCount() == 0 ) { //view section list is empty
			setupButton.setText("Create View Section...");
			addViewSectionButton.setEnabled(true);
			removeViewSectionButton.setEnabled(false);
		}else {
			setupButton.setText("Open View Section...");
			addViewSectionButton.setEnabled(false);
			if ( selectedViewSectionsList.getSelectionCount() > 0 ) {
				removeViewSectionButton.setEnabled(true);
			} else {
				removeViewSectionButton.setEnabled(false);
			}
		}
		
	}
	
	private void verifyOnlyNumeric(VerifyEvent event) {
		
		// only allow positive numbers
		if (! event.text.matches("[ 0123456789]*")) {
			event.doit = false;
		}

	}
	
	private void modifyPrintStyleButtonAction() {
		//save the state 
		saveCachedModel();
		ArrayList<String> viewsToBeSaved = new ArrayList<String>();
		viewsToBeSaved.add(viewNameText.getText());
		viewManager.save(viewsToBeSaved);

		String selectedPrintStyleName = printStyleCombo.getText();
		if( selectedPrintStyleName != null && ! selectedPrintStyleName.isEmpty() && selectedPrintStyleName.length() != 0 ) {
			PafAdminConsoleView view = (PafAdminConsoleView)viewManager.getItem(viewNameText.getText());
			
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			EditorUtils.closeDuplicateEditorType(page,PrintStyleEditor.class);
			
			if( selectedPrintStyleName.equalsIgnoreCase(PafBaseConstants.EMBEDED_PRINT_SETTINGS )) {
				//load view specific print style edit screen
				 ViewPrintSettingsInput input = new ViewPrintSettingsInput(view.getName(), viewManager, project);
				try {
					//open the editor
					PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
					//set the viewer.
					printStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
					printStyleEditor.setProject(project);
					
				} catch (PartInitException e) {
					// handle error
					logger.error(e.getMessage());
				}
			}
			else {
				selectedPrintStyleName = StringUtil.removeDefaultMarker(selectedPrintStyleName);
				if( printStylesManager.getPrintStyleByName(selectedPrintStyleName) != null ) {
					PrintStyles model = new PrintStylesManager(project);
					GlobalPrintStyleInput input = new GlobalPrintStyleInput(selectedPrintStyleName, model, project);
					try {
						//open the editor
						PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
						printStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
						printStyleEditor.setProject(project);
					} catch (PartInitException e) {
						// handle error
						logger.error(e.getMessage());
					}
				}
				else {
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "Invalid Print Style Name.");
					return;
				}
			}
			
			//close the dialog
			super.okPressed();
		}
	}
	
	@Override
	public int open() {
		
		try {
			// TTN-2387 - check for open conflicting editors
			if (EditorUtils.checkForEditorConflicts(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), ViewDialog.class)) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", Constants.EDITOR_CONFLICT_OPEN_MESSAGE);
					return 0;
			}
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return super.open();
	}

}

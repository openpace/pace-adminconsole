/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ButtonId;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.db.membertags.MemberTagType;
import com.pace.server.client.PafMdbProps;

/**
 * 
 * Member Tag Dialog.  Used to define, rename, delete member tags.
 * 
 *
 * @version	1.00
 * @author jmilliron
 *
 */
public class MemberTagsDialog extends Dialog {
			
	private static final String SAVE_MEMBER_TAG_BEFORE_CONTINUING = "Save Member Tag before continuing?";
	
	private static final Logger logger = Logger.getLogger(MemberTagsDialog.class);
	
	private List listFromListViewer;
	
	private Combo typeCombo;

	private Table dimensionTableViewerTable;

	private Button cancelButton;

	private Button applyButton;

	private Text labelTextBox;

	private Text nameTextBox;

	private IProject project = null;

	private Button newButton;

	private Button deleteButton;

	private Button editableCheckButton;

	private Button commentVisibleCheckBox;

	private CheckboxTableViewer dimensionCheckboxTableViewer;

	private MemberTagModelManager modelManager;
	
	private boolean isFormDirty;

	private boolean isFormNew;

	private Set<Control> formControlSet = new HashSet<Control>();

	private PafMdbProps cachedPafMdbProps = null;

	private MemberTagDef currentMemberTag = null;

	private ModifyListener formModifyListener;

	private SelectionAdapter formSelectionListener;

	private ISelectionChangedListener formSelectionChangedListener;

	private Label nameLabel;

	private Label labelLabel;

	private Label typeLabel;

	private Label dimensionsLabel;

	private ListViewer memberTagListViewer;

	private ISelectionChangedListener memberTagSelectionListener;
	
	private ViewSectionModelManager viewSectionModelManager = null;
	private Text defaultValueText;
	private Label defaultValueLabel;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 * @throws ServerNotRunningException 
	 */
	public MemberTagsDialog(Shell parentShell, IProject project) throws ServerNotRunningException, Exception {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.project = project;
		
		viewSectionModelManager = new ViewSectionModelManager(project);
						
		//try to get the cached paf mdb props.  Server must be running if props not cached.
		//The props are used for the dimension names
		try {
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(PafProjectUtil
					.getProjectServer(project), PafProjectUtil
					.getApplicationName(project));
		} catch (ServerNotRunningException sne ) {
			
			throw sne;
			
		} catch (Exception e) {
			logger
					.error("Cannot load cached dimensions, please reload your dimension cache.");
			throw e;
		}
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 25;
		gridLayout.marginLeft = 25;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);

		final Label memberTagsLabel = new Label(container, SWT.NONE);
		memberTagsLabel.setText("Member Tags");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		memberTagListViewer = new ListViewer(container, SWT.BORDER);
		memberTagListViewer.setLabelProvider(new ListLabelProvider());
		memberTagListViewer.setContentProvider(new ContentProvider());
		listFromListViewer = memberTagListViewer.getList();
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.heightHint = 379;
		gridData.widthHint = 225;
		listFromListViewer.setLayoutData(gridData);
		
		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(25, SWT.DEFAULT));
		composite.setLayout(new GridLayout());

		final Group memberTagGroup = new Group(container, SWT.NONE);
		memberTagGroup.setText("Member Tag");
		GridData gd_memberTagGroup = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_memberTagGroup.widthHint = 375;
		gd_memberTagGroup.heightHint = 398;
		memberTagGroup.setLayoutData(gd_memberTagGroup);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginTop = 10;
		gridLayout_1.marginRight = 10;
		gridLayout_1.marginLeft = 10;
		gridLayout_1.marginBottom = 10;
		memberTagGroup.setLayout(gridLayout_1);

		nameLabel = new Label(memberTagGroup, SWT.NONE);
		nameLabel.setText("Name");

		nameTextBox = new Text(memberTagGroup, SWT.BORDER);
		//set text limit TTN-1335
		nameTextBox.setTextLimit(80);
		GridData gd_nameTextBox = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_nameTextBox.widthHint = 328;
		nameTextBox.setLayoutData(gd_nameTextBox);

		labelLabel = new Label(memberTagGroup, SWT.NONE);
		labelLabel.setText("Label");

		labelTextBox = new Text(memberTagGroup, SWT.BORDER);
		//set a reasonable limit.
		labelTextBox.setTextLimit(255);
		labelTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		defaultValueLabel = new Label(memberTagGroup, SWT.NONE);
		defaultValueLabel.setText("Default Value");
		
		defaultValueText = new Text(memberTagGroup, SWT.BORDER);
		GridData gd_defaultValueText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_defaultValueText.widthHint = 318;
		defaultValueText.setLayoutData(gd_defaultValueText);

		typeLabel = new Label(memberTagGroup, SWT.NONE);
		typeLabel.setText("Type");

		typeCombo = new Combo(memberTagGroup, SWT.READ_ONLY);
		typeCombo.select(0);
		typeCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
		
				editableCheckButton = new Button(memberTagGroup, SWT.CHECK);
				editableCheckButton.setText("Editable");
				editableCheckButton.setLayoutData(new GridData(SWT.DEFAULT, 19));
		
				commentVisibleCheckBox = new Button(memberTagGroup, SWT.CHECK);
				commentVisibleCheckBox.setText("Comment Visible (checking may affect performance)");
		dimensionsLabel = new Label(memberTagGroup, SWT.NONE);
		dimensionsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false));
		dimensionsLabel.setText("Dimensions");
		
				dimensionCheckboxTableViewer = CheckboxTableViewer.newCheckList(memberTagGroup, SWT.BORDER);
				dimensionCheckboxTableViewer.setLabelProvider(new DimensionsTableLabelProvider());
				dimensionCheckboxTableViewer.setContentProvider(new DimensionsContentProvider());
				dimensionTableViewerTable = dimensionCheckboxTableViewer.getTable();
				dimensionTableViewerTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		final Composite composite_2 = new Composite(container, SWT.NONE);
		composite_2.setLayoutData(new GridData(125, SWT.DEFAULT));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginWidth = 0;
		gridLayout_2.makeColumnsEqualWidth = true;
		gridLayout_2.numColumns = 2;
		composite_2.setLayout(gridLayout_2);

		newButton = new Button(composite_2, SWT.NONE);
		newButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		newButton.setText("New");

		deleteButton = new Button(composite_2, SWT.NONE);
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		deleteButton.setText("Delete");
		new Label(container, SWT.NONE);

		final Composite composite_1_1 = new Composite(container, SWT.NONE);
		final GridData gridData_3 = new GridData(SWT.CENTER, SWT.CENTER, true, false);
		gridData_3.heightHint = 33;
		gridData_3.widthHint = 125;
		composite_1_1.setLayoutData(gridData_3);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		gridLayout_3.marginWidth = 0;
		gridLayout_3.makeColumnsEqualWidth = true;
		composite_1_1.setLayout(gridLayout_3);

		applyButton = new Button(composite_1_1, SWT.NONE);
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		applyButton.setEnabled(false);
		applyButton.setText("Apply");

		cancelButton = new Button(composite_1_1, SWT.NONE);
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		cancelButton.setText("Cancel");
		
		//populate ref data in form controls
		populateControlReferenceData();
		
		//add all form controls to a set
		populateFormControlSet();
		
		//create listeners
		createListeners();
		
		//add button listeners
		addButtonListeners();

		//enable form controls
		enableFormControlListeners(true);
		
		//enable member Tag listeners
		enableMemberTagListViewerListeners(true);
		
		//update the Member Tag List Viewer
		updateMemberTagListViewer(true);				
		
		return container;
	}

	/**
	 * 
	 * Populates the Type dropdown and Dimension checkbox table viewer with 
	 * values.
	 *
	 */
	private void populateControlReferenceData() {

		//Convert the type from an enumeration to a String ArrayList
		java.util.List<String> memberTagTypeList = new ArrayList<String>();

		//populate list
		for (MemberTagType memberTagType : MemberTagType.values()){
		
			memberTagTypeList.add(StringUtil.initCap(memberTagType.toString()));
			
		}
		
		//set type values with string array, values from the enumeration
		EditorControlUtil.addItems(typeCombo, memberTagTypeList.toArray(new String[0]));
		
		java.util.List<String> dimenisonList = new ArrayList<String>();
		
		dimenisonList.addAll(cachedPafMdbProps.getBaseDims());
		
		//TODO: verify i need to use all att dims, not just cached
		dimenisonList.addAll(cachedPafMdbProps.getAttributeDims());
		
		String[] allDimensions = dimenisonList.toArray(new String[0]);
		
		if ( allDimensions != null ) {
			
			Arrays.sort(allDimensions);
			
		}
		
		//add dimensions to checkbox table viewer
		EditorControlUtil.addElements(dimensionCheckboxTableViewer, allDimensions);
		
	}
	
	/**
	 * 
	 * Updates teh Member Tag List viewer from Model Manager
	 *
	 */
	private void updateMemberTagListViewer(boolean selectFirstItem) {
		
		if ( memberTagListViewer != null && getModelManager() != null ) {
			
			ISelection selection = memberTagListViewer.getSelection();
			
			memberTagListViewer.setInput(getModelManager().getMemberTags(true));
			memberTagListViewer.refresh();
			
			enableControl(memberTagListViewer.getControl(), true);
			
			//select 1st item or select existing item
			if ( selectFirstItem ) {
				
				//if model manager has models, load 1st one
				if ( getModelManager().size() > 0 ) {
					
					//select 1st item
					EditorControlUtil.selectItem(memberTagListViewer, getModelManager().getItem(getModelManager().getSortedKeys()[0]));
					
					//enable delete button
					enableControl(deleteButton, true);
															
				} else {
					
					//disable form controls
					enableFormControls(false);
					
					//disable delete button
					enableControl(deleteButton, false);
										
				}
				
			} else {
				
				//if selection is not empty, select item again
				if ( selection != null && ! selection.isEmpty() ) {
					
					EditorControlUtil.selectItem(memberTagListViewer, selection);
					
					//enable delete button
					enableControl(deleteButton, true);
					
				} else {
					
					//disable form controls
					enableFormControls(false);

					//disable delete button					
					enableControl(deleteButton, false);
					
				}
				
			}
			
		}
		
	}
	
	/**
	 * 
	 * Populates the form controls with the Member Tag Definition.
	 *
	 * @param memberTagDef Member Tag
	 */
	private void populateForm(MemberTagDef memberTagDef) {
		
		//if not null, populate form controls with values
		if ( memberTagDef != null ) {
		
			enableFormControlListeners(false);
			
			EditorControlUtil.setText(nameTextBox, memberTagDef.getName());
			EditorControlUtil.setText(labelTextBox, memberTagDef.getLabel());
			
			if ( memberTagDef.getType() != null ) {
				
				EditorControlUtil.selectItem(typeCombo, StringUtil.initCap(memberTagDef.getType().toString()));
				
			}
			
			if( memberTagDef.getDefaultValue() != null ) {
				EditorControlUtil.setText(defaultValueText, memberTagDef.getDefaultValue());
			}
			else {
				EditorControlUtil.setText(defaultValueText, "");
			}
			
			EditorControlUtil.checkButton(editableCheckButton, memberTagDef.isEditable());
			EditorControlUtil.checkButton(commentVisibleCheckBox, memberTagDef.isCommentVisible());
			
			EditorControlUtil.setCheckedElements(dimensionCheckboxTableViewer, memberTagDef.getDims());
						
			enableFormControlListeners(true);
						
		}				
		
	}
	
	/**
	 * 
	 * Enables/Disableds the Form Control Listeners
	 *
	 * @param enableListeners true/false (enable/disable)
	 */
	private void enableFormControlListeners(boolean enableListeners) {
		
		//if enabled, add form listeners
		if ( enableListeners ) {
			
			nameTextBox.addModifyListener(formModifyListener);

			labelTextBox.addModifyListener(formModifyListener);
			
			typeCombo.addModifyListener(formModifyListener);
			
			defaultValueText.addModifyListener(formModifyListener);
					
			editableCheckButton.addSelectionListener(formSelectionListener);
			
			commentVisibleCheckBox.addSelectionListener(formSelectionListener);
			
			dimensionCheckboxTableViewer.addSelectionChangedListener(formSelectionChangedListener);	
			
		//if disable, remove form listeners
		} else {
			
			nameTextBox.removeModifyListener(formModifyListener);

			labelTextBox.removeModifyListener(formModifyListener);
			
			typeCombo.removeModifyListener(formModifyListener);
					
			editableCheckButton.removeSelectionListener(formSelectionListener);
			
			commentVisibleCheckBox.removeSelectionListener(formSelectionListener);
			
			dimensionCheckboxTableViewer.removeSelectionChangedListener(formSelectionChangedListener);
			
		}
		
	}

	/**
	 * 
	 * Adds the form controls to the form control set.  The form control set
	 * is used to loop through to enable controls or clear their settings.
	 *
	 */
	private void populateFormControlSet() {

		formControlSet.add(nameTextBox);
		formControlSet.add(labelTextBox);
		formControlSet.add(defaultValueText);
		formControlSet.add(typeCombo);
		formControlSet.add(editableCheckButton);
		formControlSet.add(commentVisibleCheckBox);
		formControlSet.add(dimensionCheckboxTableViewer.getControl());
		
	}
	
	/**
	 * 
	 *  Enables/disables the form controls
	 *
	 * @param enableFormControls true/false
	 */
	private void enableFormControls(boolean enableFormControls) {
		
		for (Control formControl : formControlSet ) {
		
			formControl.setEnabled(enableFormControls);
			
		}	
		
	}
	
	/**
	 * 
	 *  Enables/Disables control if not null and  if the control is not disposed
	 *
	 * @param control	control to enable/disable
	 * @param enable true/false (enable/disable)
	 */
	private void enableControl(Control control, boolean enable) {
		
		if ( control != null && ! control.isDisposed() ) {
			
			control.setEnabled(enable);
			
		}
		
	}
	
	/**
	 * 
	 *  Clears the form control value
	 *
	 */
	private void clearFormControlValues() {
		
		//if form control set is not null and has more than 1 item in it
		if ( formControlSet != null && formControlSet.size() > 0 ) {

			//disable form control listeners
			enableFormControlListeners(false);
					
			//loop through controls and reset
			for (Control formControl : formControlSet ) {
			
				//if text set to blank
				if ( formControl instanceof Text ) {
					
					((Text) formControl).setText("");
					
				//if button [check] un check
				} else if ( formControl instanceof Button ) {
					
					((Button) formControl).setSelection(false);
					
				//if combo select 1st item in list
				} else if ( formControl instanceof Combo ) {
					
					((Combo) formControl).select(0);
					
				//if is same table as the dimenison table viewer, uncheck all items
				} else if ( formControl instanceof Table ) {
					
					if ( ((Table) formControl).equals(dimensionTableViewerTable)) {
					
						dimensionCheckboxTableViewer.setAllChecked(false);
						
					}				
					
				}
				
			}	
			
			//enable form control listeners
			enableFormControlListeners(true);
		}
		
	}
	
	/**
	 * 
	 * Saves the current Member Tag to disk.
	 *
	 */
	private void doSave() {	
		
		String missingField = isRequiredInputValid();
		
		if(missingField != null){
			GUIUtil.openMessageWindow(
					Constants.MEMBER_TAGS_WIZARD_NAME,
					"The following fields are required:\n-" + missingField,
					SWT.ICON_INFORMATION);
									
			return;
		}
		
		String invalidDataField = isInputDataValid();
		
		if ( invalidDataField != null ) {
			
			GUIUtil.openMessageWindow(
					Constants.MEMBER_TAGS_WIZARD_NAME,
					"The value in the following field is not valid:\n-" + invalidDataField,
					SWT.ICON_INFORMATION);
			
			return;
			
		}			
						
		MemberTagDef memberTagDefToSave = createMemberTagFromForm();
	
		if ( isFormNew() ) {

			if ( doesMemberTagNameAlreadyExists() ) {
				
				GUIUtil.openMessageWindow(
						Constants.MEMBER_TAGS_WIZARD_NAME,
						"A Member Tag with name:\n-" + nameTextBox.getText().trim() + " already exists.",
						SWT.ICON_INFORMATION);
										
				return;
				
			}
						
			getModelManager().add(memberTagDefToSave.getName(), memberTagDefToSave);
			
												
		} else {

			//TODO: check if dims changed
			
			//set the name from current so we can check for invalid configurations of dims
			memberTagDefToSave.setName(currentMemberTag.getName());			
			
			String[] invalidViewSections = viewSectionModelManager.getInvalidMemberTagTupleViewSectionNames(memberTagDefToSave);
			
			if ( invalidViewSections != null ) {
			
				logger.debug("We have invalid view seciton member tags here.");
				
				StringBuffer strBuff = new StringBuffer("Warning:  The dimensions have changed.  ");
				strBuff.append("\n\nThis member tag on the following view sections will become invalid if you continue.  Continue with change?\n\n");
				
				for ( String invalidViewSection : invalidViewSections ) {
				
					strBuff.append("\t" + invalidViewSection + "\n");
					
				}				
				
				if (! MessageDialog.openQuestion(this.getShell(), Constants.DIALOG_WARNING_HEADING, strBuff.toString())) {
					
					return;
					
				}
				
			}			
			
			//create from from again because we changed the name
			memberTagDefToSave = createMemberTagFromForm();
			
			//If member tag has been renamed, do a dup check, update view sections, remove old model and add new
			if ( currentMemberTag != null && ! currentMemberTag.getName().equalsIgnoreCase(memberTagDefToSave.getName())) {
				
				if ( doesMemberTagNameAlreadyExists() ) {
					
					GUIUtil.openMessageWindow(
							Constants.MEMBER_TAGS_WIZARD_NAME,
							"A Member Tag with name:\n-" + nameTextBox.getText().trim() + " already exists.",
							SWT.ICON_INFORMATION);
											
					return;
					
				}
				
				viewSectionModelManager.updateMemberTagReferences(currentMemberTag.getName(), memberTagDefToSave.getName());
				
				getModelManager().remove(currentMemberTag.getName());
				
				getModelManager().add(memberTagDefToSave.getName(), memberTagDefToSave);
				
			} else {
			
				getModelManager().replace(memberTagDefToSave.getName(), memberTagDefToSave);
				
			}
			//
			
			
									
		}
		
		getModelManager().save();
		
		isFormNew = false;
		isFormDirty = false;
		
		enableMemberTagListViewerListeners(false);			
		updateMemberTagListViewer(false);
		enableMemberTagListViewerListeners(true);
			
		EditorControlUtil.selectItem(memberTagListViewer, memberTagDefToSave);

		//current member tag as last one saved
		currentMemberTag = memberTagDefToSave;
		
		resetApplyButton();

	}
	
	/**
	 * 
	 * Verify the input data is valid.
	 *
	 * @return Name of label for invalid item.
	 */
	private String isInputDataValid() {
		
		//must have at least 1 item checked
		if ( dimensionCheckboxTableViewer.getCheckedElements().length == 0 ) {
			
			return this.dimensionsLabel + " - Must check at least 1 dimension.";
			
		//all items can not be checked
		} else if ( dimensionCheckboxTableViewer.getCheckedElements().length ==  dimensionTableViewerTable.getItemCount() ) {
			
			return this.dimensionsLabel + " - Can't check all dimensions.  Please uncheck one or more dimensions.";
			
		}		
		
		return null;
		
	}

	/**
	 * 
	 * Check required input
	 *
	 * @return
	 */
	private String isRequiredInputValid() {

		if ( nameTextBox.getText().trim().length() == 0 ) {
			return nameLabel.getText();
		}
		
		return null;
	}
	
	/**
	 * 
	 * Check to see if member tag name already exists
	 *
	 * @return true if name exist, false if not
	 */
	private boolean doesMemberTagNameAlreadyExists() {
						
		//check if name in member tag name text box already exists in the model manager
		return getModelManager().contains(nameTextBox.getText().trim());
		
	}

	/**
	 * 
	 * Creates a member tag def from the form values
	 *
	 * @return
	 */
	private MemberTagDef createMemberTagFromForm() {
		
		MemberTagDef newMemberTagDef = new MemberTagDef();
		
		newMemberTagDef.setName(nameTextBox.getText().trim());
		
		//if label textbox is blank, use value from name text box [requirement]
		if ( labelTextBox.getText().trim().length() == 0 ) {
			
			newMemberTagDef.setLabel(nameTextBox.getText().trim());
			
		} else {
			
			newMemberTagDef.setLabel(labelTextBox.getText().trim());
			
		}
		
		//get type combo value and then uppercase to match enumeration
		String updatedTypeValue = typeCombo.getText().toUpperCase();
		
		newMemberTagDef.setType(MemberTagType.valueOf(updatedTypeValue));
		
		newMemberTagDef.setEditable(editableCheckButton.getSelection());
		
		newMemberTagDef.setCommentVisible(commentVisibleCheckBox.getSelection());
		
		newMemberTagDef.setDefaultValue(defaultValueText.getText());
		
		java.util.List<String> updatedDimsList = new ArrayList<String>();
		
		for (Object checkedElement : dimensionCheckboxTableViewer.getCheckedElements() ) {
			
			updatedDimsList.add((String) checkedElement);
			
		}
		
		newMemberTagDef.setDims(updatedDimsList.toArray(new String[0]));
		
		return newMemberTagDef;
		
	}

	/**
	 * 
	 * Called when a form control has been modified.
	 *
	 */
	private void formControlModified() {
		isFormDirty = true;
		enableControl(applyButton, true);
		
	}

	/**
	 * 
	 * Returns if form is dirty or not
	 *
	 * @return
	 */
	private boolean isFormDirty() {
		return isFormDirty;
	}
	
	/**
	 * 
	 * Create listeners that will be used for the controls
	 *
	 */
	private void createListeners() {

		formModifyListener = new ModifyListener() {

			public void modifyText(ModifyEvent arg0) {

				formControlModified();
				
			}
				
		};
		
		formSelectionListener = new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				
				formControlModified();
				
			}
			
		};
		
		formSelectionChangedListener = new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent arg0) {

				formControlModified();
				
			}
			
		};		
		
		memberTagSelectionListener = new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent selectionChangedEvent) {
				
				//get selection
				ISelection selection = selectionChangedEvent.getSelection();
				
				if ( selection instanceof IStructuredSelection ) {
					
					IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					
					Object selectedObject = structuredSelection.getFirstElement();
					
					if ( selectedObject instanceof MemberTagDef	) {
																		
						MemberTagDef selectedMemberTagDef = (MemberTagDef) selectedObject;
											
						memberTagListViewerChanged(selectedMemberTagDef);
						
					}
				}
								
			}
		};
			
		
		
	}

	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param selectedMemberTag
	 */
	private void memberTagListViewerChanged(MemberTagDef selectedMemberTag) {
		
//		 check if form has been modified
		if ( isFormDirty() ) {
			
			//prompt to save changes
			if ( MessageDialog.openConfirm(this.getShell(), Constants.MEMBER_TAGS_WIZARD_NAME, SAVE_MEMBER_TAG_BEFORE_CONTINUING) ) {
			
				//user confirmed, save
				doSave();
				
				//if form is still diry, means that member tag didn't save properly
				if ( isFormDirty() ) {					
					
					if ( currentMemberTag == null ) {
																	
						enableMemberTagListViewerListeners(false);			
						memberTagListViewer.setSelection(null);
						enableMemberTagListViewerListeners(true);
						
					} else {
						
						enableMemberTagListViewerListeners(false);			
						EditorControlUtil.selectItem(memberTagListViewer, currentMemberTag);
						enableMemberTagListViewerListeners(true);
						
					}
									
					return;					
				}
				
			}									
			
		}
		
		//enable form controls
		enableFormControls(true);
		
		//populate form
		populateForm(selectedMemberTag);
		
		//lastMemberTagSelected = selectedMemberTagDef.getName();
		currentMemberTag = selectedMemberTag;
		
		enableMemberTagListViewerListeners(false);			
		EditorControlUtil.selectItem(memberTagListViewer, selectedMemberTag);
		enableMemberTagListViewerListeners(true);
		
		enableControl(deleteButton, true);
		
		resetApplyButton();
		isFormNew = false;
		isFormDirty = false;
		
		
	}

	/**
	 * 
	 * Enables/Disables the member Tag List viewer listeners
	 *
	 * @param enableListeners true/false (enable/disable) listeners
	 */
	private void enableMemberTagListViewerListeners(boolean enableListeners) {
		
		if ( enableListeners ) {
			//member tag list viewers selection changed listener
			memberTagListViewer.addSelectionChangedListener(memberTagSelectionListener);
		} else {
			//member tag list viewers selection changed listener
			memberTagListViewer.removeSelectionChangedListener(memberTagSelectionListener);
		}


		
	}

	/**
	 * Sets up the listeners for the buttons
	 * 
	 */
	private void addButtonListeners() {

		
//		 new button listeners
		setupButtonSelectionListener(newButton, ButtonId.New);

		// delete button listeners
		setupButtonSelectionListener(deleteButton, ButtonId.Delete);

		// add button listeners
		setupButtonSelectionListener(applyButton, ButtonId.Apply);

		// cancel button listeners
		setupButtonSelectionListener(cancelButton, ButtonId.Cancel);
	}
	
	/**
	 * 
	 * Sets up a button selection listeners
	 *
	 * @param button	button to add listener to
	 * @param buttonId	button id of button to add listener to
	 */
	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}
	
	
	/**
	 * Used to perfrom an action when a button is pressed
	 * 
	 * @param buttonId
	 *            enum for which button was selected
	 */
	private void buttonSelected(ButtonId buttonId) {

		logger.debug(buttonId + " button clicked");
		
		switch (buttonId) {

		case Apply:
		
			applyButtonAction();
			
			break;

		case Delete:

			deleteButtonAction();

			break;

		case New:
		
			newButtonAction();

			break;

		case Cancel:

			cancelButtonAction();

			break;
		}
	}
	
	
	/**
	 * 
	 * New Button Action
	 *
	 */
	private void newButtonAction() {
		
		//if new, don't init again
		if ( isFormNew() ) {
			
			return;
			
		}
		
		//check if form has been modified
		if ( isFormDirty() ) {
			
			if ( MessageDialog.openConfirm(this.getShell(), Constants.MEMBER_TAGS_WIZARD_NAME, SAVE_MEMBER_TAG_BEFORE_CONTINUING) ) {
			
				doSave();
				
				//if form is still diry, means that member tag didn't save properly
				if ( isFormDirty() ) {
					
					return;
					
				}
				
			}			
			
		}
		
		isFormNew = true;
		
		enableFormControls(true);

		memberTagListViewer.setSelection(null);
		
		clearFormControlValues();
		
		currentMemberTag = null;
		
		enableControl(deleteButton, false);
		
		enableControl(memberTagListViewer.getControl(), false);

		nameTextBox.setFocus();
		
		resetApplyButton();
		
	}
	
	/**
	 * 
	 * Delete Action
	 *
	 */
	private void deleteButtonAction() {
				
		String[] memberTagNameSelectionAr = listFromListViewer.getSelection();
		
		if ( memberTagNameSelectionAr == null || memberTagNameSelectionAr.length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.MEMBER_TAGS_WIZARD_NAME, "Invalid Member Tag selection.");
			
		} else {
				
			String memberTagToDelete = memberTagNameSelectionAr[0];
			
			if ( MessageDialog.openConfirm(this.getShell(), Constants.MEMBER_TAGS_WIZARD_NAME, "Delete Member Tag: " + memberTagToDelete + "\n\nAll underlying data will still exist, but will not have any member tag associations.  Views will automatically be updated with this change." ) ) {
			
				if ( getModelManager().contains(memberTagToDelete) ) {
					
					getModelManager().remove(memberTagToDelete);
															
					getModelManager().save();
					
					clearFormControlValues();
					
					currentMemberTag = null;
					
					resetApplyButton();
					
					this.isFormDirty = false;
					this.isFormNew = false;
					
					
				}
				
				updateMemberTagListViewer(true);

			}			
			
		}
										
		
	}

	/**
	 * 
	 * Apply Button
	 *
	 */
	private void applyButtonAction() {
		
		doSave();
		
	}
	
	/**
	 * 
	 *  Cancel Button
	 *
	 */
	private void cancelButtonAction() {
		
		isFormDirty = false;
		
		//if form is new, clear values
		if ( isFormNew() ) {
			
			enableFormControlListeners(false);
			clearFormControlValues();
			enableFormControlListeners(true);
			enableFormControls(false);
			updateMemberTagListViewer(true);
			
		//else populate with last loaded member tag
		} else {
		
			populateForm(currentMemberTag);
			
		}
				
		isFormNew = false;
		
		resetApplyButton();
		
	}
		

	/**
	 * 
	 * Disables the apply button
	 *
	 */
	private void resetApplyButton() {
		
		enableControl(applyButton, false);		
		
	}
	
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(695, 683);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Constants.MEMBER_TAGS_WIZARD_NAME);
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
			
//			check if form has been modified
			if ( isFormDirty() ) {
				
				if ( MessageDialog.openConfirm(this.getShell(), Constants.MEMBER_TAGS_WIZARD_NAME, SAVE_MEMBER_TAG_BEFORE_CONTINUING) ) {
				
					doSave();
					
					//if form is still diry, means that member tag didn't save properly
					if ( isFormDirty() ) {
						
						return;
						
					}
					
				}			
				
			}
			
						
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 * Class_description_goes_here
	 *
	 * @version	x.xx
	 * @author Jason
	 *
	 */
	class DimensionsTableLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object element, int columnIndex) {
			return element.toString();
		}
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
	}
	
	/**
	 * 
	 * Class_description_goes_here
	 *
	 * @version	x.xx
	 * @author Jason
	 *
	 */
	class DimensionsContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (Object[]) inputElement;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	/**
	 * 
	 * Class_description_goes_here
	 *
	 * @version	x.xx
	 * @author Jason
	 *
	 */
	class ListLabelProvider extends LabelProvider {
		public String getText(Object element) {
						
			if ( element instanceof MemberTagDef ) {
				
				return ((MemberTagDef) element).getName();
				
			}
						
			return element.toString();
		}
		public Image getImage(Object element) {
			return null;
		}
	}
	
	/**
	 * 
	 * Class_description_goes_here
	 *
	 * @version	x.xx
	 * @author Jason
	 *
	 */
	class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (Object[]) inputElement;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	/**
	 * 
	 * Creates an instance of model manager if doesn't exists, otherwise returns model manager
	 * 
	 * @return the modelManager
	 */
	private MemberTagModelManager getModelManager() {
		
		if ( modelManager == null ) {
			
			if ( project == null ) {
				throw new NullPointerException("Project can not be null");
			}
			
			modelManager = new MemberTagModelManager(project);
			
		}		
		
		return modelManager;
	}

	/**
	 * @return the isFormNew
	 */
	private boolean isFormNew() {
		return isFormNew;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

public class RolesDialog extends Dialog {

	private Table table_1;
	private Combo planTypeCombo;
	private Text descriptionText;
	private Text nameText;
	private List list;
	public RolesDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		list = new List(container, SWT.BORDER);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 300);
		formData.right = new FormAttachment(0, 170);
		formData.top = new FormAttachment(0, 55);
		formData.left = new FormAttachment(0, 30);
		list.setLayoutData(formData);

		final Label usersLabel = new Label(container, SWT.NONE);
		final FormData formData_2 = new FormData();
		formData_2.right = new FormAttachment(0, 120);
		formData_2.top = new FormAttachment(0, 40);
		formData_2.left = new FormAttachment(list, 0, SWT.LEFT);
		usersLabel.setLayoutData(formData_2);
		usersLabel.setText("Current Roles:");

		final Button newButton = new Button(container, SWT.NONE);
		final FormData formData_1_2_2 = new FormData();
		formData_1_2_2.bottom = new FormAttachment(0, 335);
		formData_1_2_2.right = new FormAttachment(0, 75);
		formData_1_2_2.top = new FormAttachment(0, 310);
		formData_1_2_2.left = new FormAttachment(0, 30);
		newButton.setLayoutData(formData_1_2_2);
		newButton.setText("New");

		final Button deleteButton = new Button(container, SWT.NONE);
		final FormData formData_1_2_3 = new FormData();
		formData_1_2_3.left = new FormAttachment(newButton, 5, SWT.RIGHT);
		formData_1_2_3.bottom = new FormAttachment(0, 335);
		formData_1_2_3.top = new FormAttachment(0, 310);
		formData_1_2_3.right = new FormAttachment(newButton, 50, SWT.RIGHT);
		deleteButton.setLayoutData(formData_1_2_3);
		deleteButton.setText("Delete");

		final Group roleGroup = new Group(container, SWT.NONE);
		roleGroup.setText("Role");
		final FormData formData_1 = new FormData();
		formData_1.top = new FormAttachment(0, 50);
		formData_1.bottom = new FormAttachment(0, 375);
		formData_1.right = new FormAttachment(0, 630);
		formData_1.left = new FormAttachment(0, 187);
		roleGroup.setLayoutData(formData_1);
		roleGroup.setLayout(new FormLayout());

		final Label nameLabel = new Label(roleGroup, SWT.NONE);
		nameLabel.setAlignment(SWT.RIGHT);
		final FormData formData_3 = new FormData();
		formData_3.bottom = new FormAttachment(0, 22);
		formData_3.right = new FormAttachment(0, 165);
		formData_3.top = new FormAttachment(0, 7);
		formData_3.left = new FormAttachment(0, 131);
		nameLabel.setLayoutData(formData_3);
		nameLabel.setText("Name:");

		final Label descriptionLabel = new Label(roleGroup, SWT.NONE);
		descriptionLabel.setAlignment(SWT.RIGHT);
		final FormData formData_4 = new FormData();
		formData_4.top = new FormAttachment(0, 31);
		formData_4.left = new FormAttachment(0, 105);
		formData_4.bottom = new FormAttachment(0, 46);
		formData_4.right = new FormAttachment(0, 165);
		descriptionLabel.setLayoutData(formData_4);
		descriptionLabel.setText("Description:");

		nameText = new Text(roleGroup, SWT.BORDER);
		final FormData formData_5 = new FormData();
		formData_5.top = new FormAttachment(0, 9);
		formData_5.bottom = new FormAttachment(0, 29);
		formData_5.right = new FormAttachment(0, 315);
		formData_5.left = new FormAttachment(0, 175);
		nameText.setLayoutData(formData_5);

		descriptionText = new Text(roleGroup, SWT.BORDER);
		final FormData formData_6 = new FormData();
		formData_6.left = new FormAttachment(0, 175);
		formData_6.top = new FormAttachment(0, 34);
		formData_6.bottom = new FormAttachment(0, 81);
		formData_6.right = new FormAttachment(0, 363);
		descriptionText.setLayoutData(formData_6);

		final Label planTypeLabel = new Label(roleGroup, SWT.NONE);
		planTypeLabel.setAlignment(SWT.RIGHT);
		final FormData formData_7 = new FormData();
		formData_7.top = new FormAttachment(0, 84);
		formData_7.left = new FormAttachment(0, 105);
		formData_7.bottom = new FormAttachment(0, 98);
		formData_7.right = new FormAttachment(0, 165);
		planTypeLabel.setLayoutData(formData_7);
		planTypeLabel.setText("Plan Type:");

		planTypeCombo = new Combo(roleGroup, SWT.NONE);
		final FormData formData_8 = new FormData();
		formData_8.bottom = new FormAttachment(0, 105);
		formData_8.right = new FormAttachment(0, 280);
		formData_8.top = new FormAttachment(descriptionText, 5, SWT.BOTTOM);
		formData_8.left = new FormAttachment(descriptionText, 0, SWT.LEFT);
		planTypeCombo.setLayoutData(formData_8);

		final Group newGroup = new Group(roleGroup, SWT.NONE);
		newGroup.setText("Season Id's");
		final FormData formData_9 = new FormData();
		formData_9.bottom = new FormAttachment(0, 265);
		formData_9.right = new FormAttachment(0, 425);
		formData_9.top = new FormAttachment(planTypeCombo, 5, SWT.BOTTOM);
		formData_9.left = new FormAttachment(0, 10);
		newGroup.setLayoutData(formData_9);
		newGroup.setLayout(new FormLayout());

		table_1 = new Table(newGroup, SWT.BORDER);
		final FormData formData_9_1 = new FormData();
		formData_9_1.bottom = new FormAttachment(0, 130);
		formData_9_1.top = new FormAttachment(0, 5);
		formData_9_1.right = new FormAttachment(0, 325);
		formData_9_1.left = new FormAttachment(0, 10);
		table_1.setLayoutData(formData_9_1);
		table_1.setLinesVisible(true);
		table_1.setHeaderVisible(true);

		final TableColumn planCycleTableColumn = new TableColumn(table_1, SWT.NONE);
		planCycleTableColumn.setWidth(100);
		planCycleTableColumn.setText("Plan Cycle");

		final TableColumn newColumnTableColumn = new TableColumn(table_1, SWT.NONE);
		newColumnTableColumn.setWidth(100);
		newColumnTableColumn.setText("Year");

		final TableColumn newColumnTableColumn_1 = new TableColumn(table_1, SWT.NONE);
		newColumnTableColumn_1.setWidth(80);
		newColumnTableColumn_1.setText("Season");

		final Button newButton_1 = new Button(newGroup, SWT.NONE);
		final FormData formData_10 = new FormData();
		formData_10.bottom = new FormAttachment(0, 35);
		formData_10.right = new FormAttachment(0, 390);
		formData_10.top = new FormAttachment(0, 10);
		formData_10.left = new FormAttachment(0, 340);
		newButton_1.setLayoutData(formData_10);
		newButton_1.setText("New");

		final Button editComboButton = new Button(newGroup, SWT.NONE);
		final FormData formData_10_1 = new FormData();
		formData_10_1.top = new FormAttachment(0, 40);
		formData_10_1.left = new FormAttachment(0, 340);
		formData_10_1.bottom = new FormAttachment(0, 65);
		formData_10_1.right = new FormAttachment(0, 390);
		editComboButton.setLayoutData(formData_10_1);
		editComboButton.setText("Edit");

		final Button deleteComboButton = new Button(newGroup, SWT.NONE);
		final FormData formData_10_2 = new FormData();
		formData_10_2.top = new FormAttachment(0, 70);
		formData_10_2.left = new FormAttachment(0, 340);
		formData_10_2.bottom = new FormAttachment(0, 95);
		formData_10_2.right = new FormAttachment(0, 390);
		deleteComboButton.setLayoutData(formData_10_2);
		deleteComboButton.setText("Delete");

		final Button button = new Button(newGroup, SWT.ARROW);
		final FormData formData_11 = new FormData();
		formData_11.bottom = new FormAttachment(0, 115);
		formData_11.right = new FormAttachment(0, 360);
		formData_11.top = new FormAttachment(deleteComboButton, 5, SWT.BOTTOM);
		formData_11.left = new FormAttachment(deleteComboButton, 0, SWT.LEFT);
		button.setLayoutData(formData_11);
		button.setText("button");

		final Button button_1 = new Button(newGroup, SWT.ARROW | SWT.DOWN);
		final FormData formData_11_1 = new FormData();
		formData_11_1.right = new FormAttachment(deleteComboButton, 0, SWT.RIGHT);
		formData_11_1.top = new FormAttachment(0, 100);
		formData_11_1.left = new FormAttachment(0, 370);
		formData_11_1.bottom = new FormAttachment(0, 115);
		button_1.setLayoutData(formData_11_1);
		button_1.setText("button");

		final Button addButton = new Button(roleGroup, SWT.NONE);
		final FormData formData_12 = new FormData();
		formData_12.left = new FormAttachment(0, 320);
		formData_12.bottom = new FormAttachment(0, 304);
		formData_12.right = new FormAttachment(0, 368);
		formData_12.top = new FormAttachment(0, 280);
		addButton.setLayoutData(formData_12);
		addButton.setText("Add");

		final Button addButton_1 = new Button(roleGroup, SWT.NONE);
		final FormData formData_12_1 = new FormData();
		formData_12_1.bottom = new FormAttachment(addButton, 24, SWT.TOP);
		formData_12_1.top = new FormAttachment(addButton, 0, SWT.TOP);
		formData_12_1.right = new FormAttachment(0, 423);
		formData_12_1.left = new FormAttachment(0, 375);
		addButton_1.setLayoutData(formData_12_1);
		addButton_1.setText("Cancel");

		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(653, 472);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Roles");
	}

}

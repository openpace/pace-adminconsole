/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pace.admin.menu.editors.internal.InvalidSecurityMember;

public class InvalidMemberErrorDialog extends TitleAreaDialog {

	class ContentProvider implements IStructuredContentProvider {
		
		public Object[] getElements(Object inputElement) {
			
			if ( inputElement instanceof Set ) {
								
				Set<InvalidSecurityMember> invalidSecurityMemberSet = (LinkedHashSet<InvalidSecurityMember>) inputElement;
				
				return invalidSecurityMemberSet.toArray();
			}
			
			return null;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		
		public String getColumnText(Object element, int columnIndex) {

			String strValue = null;
			
			if ( element instanceof InvalidSecurityMember ) {
				
				InvalidSecurityMember invalidSecurityMember = (InvalidSecurityMember) element;
				
				switch ( columnIndex ) {
				
				case 0: 
					strValue = invalidSecurityMember.getRoleName();
					break;
				case 1:
					strValue = invalidSecurityMember.getDimensionName();
					break;
				case 2:
					strValue = invalidSecurityMember.getMemberName();
					break;									
				}
								
			}		
			return strValue;
		}
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		
	}
	private Table table;
	
	private Set<InvalidSecurityMember> invalidSecurityMemberSet;
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public InvalidMemberErrorDialog(Shell parentShell, Set<InvalidSecurityMember> invalidSecurityMemberSet) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.invalidSecurityMemberSet = invalidSecurityMemberSet;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 207;
		container.setLayoutData(gridData);

		final TableViewer tableViewer = new TableViewer(container, SWT.BORDER);
		tableViewer.setContentProvider(new ContentProvider());
		tableViewer.setLabelProvider(new TableLabelProvider());
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setBounds(67, 25, 377, 210);

		final TableColumn roleTableColumn = new TableColumn(table, SWT.NONE);
		roleTableColumn.setWidth(123);
		roleTableColumn.setText("Role");

		final TableColumn dimensionTableColumn = new TableColumn(table, SWT.NONE);
		dimensionTableColumn.setWidth(100);
		dimensionTableColumn.setText("Dimension");

		final TableColumn securityMemberTableColumn = new TableColumn(table, SWT.NONE);
		securityMemberTableColumn.setWidth(143);
		securityMemberTableColumn.setText("Invalid Security Member");
		tableViewer.setInput(invalidSecurityMemberSet);
		setTitle("Invalid Members Detected");
		setMessage("The following are invalid security members for this user. Please configure new security members for these invalid security members.");
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Close",
				true);
//		final Button button = createButton(parent, IDialogConstants.CANCEL_ID,
//				IDialogConstants.CANCEL_LABEL, false);
//		button.setEnabled(false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(519, 382);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Warning...");
	}

}

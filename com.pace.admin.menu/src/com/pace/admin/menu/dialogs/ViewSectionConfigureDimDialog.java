/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.ErrorConstants;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.view.GroupingSpec;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;
import com.pace.server.client.PafMdbProps;

public class ViewSectionConfigureDimDialog extends Dialog {

	// logger
	private static Logger logger = Logger.getLogger(ViewSectionConfigureDimDialog.class);
	
	// page button id's
	private static final int PAGE_IN_BUTTON = 0;

	private static final int PAGE_OUT_BUTTON = 1;

	private static final int PAGE_UP_BUTTON = 2;

	private static final int PAGE_DOWN_BUTTON = 3;

	// column button id's
	private static final int COL_IN_BUTTON = 4;

	private static final int COL_OUT_BUTTON = 5;

	private static final int COL_UP_BUTTON = 6;

	private static final int COL_DOWN_BUTTON = 7;

	// row button id's
	private static final int ROW_IN_BUTTON = 8;

	private static final int ROW_OUT_BUTTON = 9;

	private static final int ROW_UP_BUTTON = 10;

	private static final int ROW_DOWN_BUTTON = 11;

	// directions
	private static final int BUTTON_UP = 0;

	private static final int BUTTON_DOWN = 1;

	// button groups
	private static final int PAGE_BUTTON_GROUP = 0;

	private static final int COLUMN_BUTTON_GROUP = 1;

	private static final int ROW_BUTTON_GROUP = 2;
	
	private Tree dimensionsTree;
	
	private List rowList;

	private List columnList;

	private List pageList;

	private Button pageInButton;

	private Button pageOutButton;

	private Button pageUpButton;

	private Button pageDownButton;

	private Button columnInButton;

	private Button columnOutButton;

	private Button columnUpButton;

	private Button columnDownButton;

	private Button rowInButton;

	private Button rowOutButton;

	private Button rowUpButton;

	private Button rowDownButton;

	private Button showAllButton;
	
	private Button hideAllButton;
	
	private ViewSectionModelManager viewSectionModelManager;
	
	private MemberTagModelManager memberTagModelManager;
	
	private PafViewSectionUI viewSection = null;

	private String viewSectionName = null;

	private IProject project = null;
	//base dim, list of attributes
	private Map<String, ArrayList<String>> baseMemberLookup = new HashMap<String, ArrayList<String>>();
	//a set of invalid dimensions, this may occur when reopening an existing view section.
	private Set<String> invalidDimensions = new TreeSet<String>();
	//Used to change the color of a selected dimension, so it looks disabled.
	private Color disabledTreeItemColor = null;
	//The default color for an enabled dimension item.
	private Color enabledTreeItemColor = null;
	//bold font
	private Font boldFont;
	//standard font
	private Font normalFont;
	
	public ViewSectionConfigureDimDialog(Shell parentShell, IProject project,
			String viewSectionName, ViewSectionModelManager viewSectionModelManager) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;
		this.viewSectionName = viewSectionName;
		this.viewSectionModelManager = viewSectionModelManager;
		this.memberTagModelManager = new MemberTagModelManager(project);
	}

	private void initializeListBoxes() {

		if (viewSectionModelManager.contains(viewSectionName)) {

			Set<String> allDimensions = new TreeSet<String>();
			Set<String> allBaseDims = new TreeSet<String>();
			Set<String> selectedDims = new TreeSet<String>();
			Set<String> availableAttrDims = new TreeSet<String>();
			PafMdbProps cachedPafMdbProps = null;	
			
			//get the cached mdb props object.
			try{
				cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
						PafProjectUtil.getProjectServer(project), 
						PafProjectUtil.getApplicationName(project));
			} catch(Exception e){
				logger.error(e.getMessage());
				MessageDialog.openError(this.getShell(),
    					"Server returned error", e.getMessage());
//						"An error occurred while trying to load the available cached dimensions.");
			}
			
			if( cachedPafMdbProps != null ) {
				//get an set of the base members.
				for(String baseDim : cachedPafMdbProps.getBaseDims()){
					allBaseDims.add(baseDim);			
					baseMemberLookup.put(baseDim, new ArrayList<String>());
				}
				
				
				//build a set of available attribute dimensions.
				for(String dim : cachedPafMdbProps.getCachedAttributeDims()){
					availableAttrDims.add(dim);
					this.showAllButton.setEnabled(true);
					this.hideAllButton.setEnabled(true);
				}
				
				//create the allDimensions set.
				allDimensions.addAll(allBaseDims);
				allDimensions.addAll(availableAttrDims);
				
				//build a map of base dimension, list<attribute dim>
				//only add the attribute dimension if it in the array of available dimensions.
				if(cachedPafMdbProps != null){
					for(int i = 0; i < cachedPafMdbProps.getBaseDimLookupValues().size(); i++){
						if(availableAttrDims.contains(cachedPafMdbProps.getBaseDimLookupKeys().get(i))){
							ArrayList<String> tmp = null;
							tmp = baseMemberLookup.get(cachedPafMdbProps.getBaseDimLookupValues().get(i));
							tmp.add(cachedPafMdbProps.getBaseDimLookupKeys().get(i));
							baseMemberLookup.remove(cachedPafMdbProps.getBaseDimLookupValues().get(i));
							baseMemberLookup.put(cachedPafMdbProps.getBaseDimLookupValues().get(i), tmp);
						}
					}
				}
			}		
			viewSection = (PafViewSectionUI) viewSectionModelManager.getItem(viewSectionName);
			String[] pageDims = viewSection.getPageAxisDims();

			if (pageDims != null) {
				pageList.setItems(pageDims);
				for (String dim : pageDims) {
					selectedDims.add(dim);
				}

			}

			String[] colDims = viewSection.getColAxisDims();

			if (colDims != null) {
				columnList.setItems(colDims);
				for (String dim : colDims) {
					selectedDims.add(dim);
				}
			}

			String[] rowDims = viewSection.getRowAxisDims();

			if (rowDims != null) {
				rowList.setItems(rowDims);
				for (String dim : rowDims) {
					selectedDims.add(dim);
				}
			}
			
			for(String dim : allBaseDims){
				//create the node.
				TreeItem node = EditorControlUtil.addTreeNode(dimensionsTree, null, dim);
				//if the node has already been selected, then disable it.
				if(selectedDims.contains(dim)){
					node.setForeground(disabledTreeItemColor);
				} else {
					boldTreeItem(node, true);
				}
					
				//get the list of attributes for the base member.
				ArrayList<String> attrs = baseMemberLookup.get(dim);
				//if we have some loop thru them and add them.
				for(String attr : attrs){
					TreeItem child = EditorControlUtil.addTreeNode(dimensionsTree, node, attr);
					//if the attribute nodes have been sel, then disable them.
					if(selectedDims.contains(attr) && child != null){
						child.setForeground(disabledTreeItemColor);
					}else {
						boldTreeItem(child, true);
					}
				}
			}
			validateDimensionLayout(selectedDims, allDimensions);
		}
	}
	
	/**
	 * Checks the selected dimensions set against the all dimensions set to validate that
	 * the dimensions are no longer valid.
	 * @param selectedDimensions Set of the currently selected dimensions.
	 * @param allDimensions Set of all of the available dimensions.
	 */
	private void validateDimensionLayout(Set<String> selectedDimensions, Set<String> allDimensions){
		String missingDims = "";
		for(String dim : selectedDimensions){
			if(! allDimensions.contains(dim)){
				invalidDimensions.add(dim);
				missingDims += dim + "\n";
			}
		}
		if(missingDims.length() > 0){
			MessageDialog.openError(this.getShell(),
					ErrorConstants.MISSING_DIMENSIONS,
					ErrorConstants.THE_FOLLOWING_DIMENSIONS_ARE_NO_LONGER_VALID + missingDims);
		}
	}

	private void save() {

		viewSection = (PafViewSectionUI) viewSectionModelManager.getItem(viewSectionName);

		updateTupleViaDimChange(viewSection);

		MemberTagDef[] memberTagDefs = null;
		
		//if member tag model manager is not null
		if ( memberTagModelManager != null ) {
			
			//get member tags
			memberTagDefs = memberTagModelManager.getMemberTags(true);
			
		}
		
		//update member tag entries
		viewSection.updateMemberTags(memberTagDefs);
		
		viewSectionModelManager.replace(viewSectionName, viewSection);

	}

	private void updateTupleViaDimChange(PafViewSectionUI currentViewSection) {
	
		boolean newViewSection = false;
		
		boolean pageDimChanged = false;
		boolean colDimChanged = false;
		boolean rowDimChanged = false;

		String[] currentPageDims = currentViewSection.getPageAxisDims();
		String[] currentColumnDims = currentViewSection.getColAxisDims();
		String[] currentRowDims = currentViewSection.getRowAxisDims();

		
		PageTuple[] pageTuples = currentViewSection.getPafViewSection().getPageTuples();
		ViewTuple[] colTuples = currentViewSection.getPafViewSection()
				.getColTuples();
		ViewTuple[] rowTuples = currentViewSection.getPafViewSection()
				.getRowTuples();

		
		if ( ( pageTuples == null || ( pageTuples != null && pageTuples.length > 0 && pageTuples[0].getMember() == null)) 
				&& ( colTuples == null || ( colTuples != null && colTuples.length == 0) ) && 
				( rowTuples == null || (rowTuples != null && rowTuples.length == 0) )) {
			
			newViewSection = true;
			
		}
						
		String[] updatedPageDims = pageList.getItems();
		String[] updatedColumnDims = columnList.getItems();
		String[] updatedRowDims = rowList.getItems();

//		 dimensions changed, set page tuples to null
		if (dimensionsChanged(currentPageDims, updatedPageDims)) {
			
			pageDimChanged = true;
			
		}
		
//		 dimensions changed, set column tuples to null
		if (dimensionsChanged(currentColumnDims, updatedColumnDims)) {
			
			colDimChanged = true;
			
		}
		
//		 dimensions changed, set row tuples to null
		if (dimensionsChanged(currentRowDims, updatedRowDims)) {
			
			rowDimChanged = true;
			
		}
		
		if ( pageDimChanged || colDimChanged || rowDimChanged ) {
			
			if ( newViewSection ) {
			
				viewSection.setPageAxisDims(pageList.getItems());
				viewSection.setColAxisDims(columnList.getItems());
				viewSection.setRowAxisDims(rowList.getItems());
				
			} else {
				
				if (GUIUtil.askUserAQuestion("The dimensions have changed therefore tuples and properties tied to those axis's will be removed.  Continue?")) {
					
					if ( pageDimChanged ) {
						
						//currentViewSection.getPafViewSection().setPageTuples(null);
						//viewSection.setPageAxisDims(pageList.getItems());
						viewSection.updatePageAxisDims(updatedPageDims, pageTuples);
					} 
					
					if ( colDimChanged) {
						viewSection.setSortingTuples(null);
						removeGroupingSpec(PafAxis.COL, currentViewSection.getPafViewSection());
						currentViewSection.getPafViewSection().setColTuples(null);
						viewSection.setColAxisDims(columnList.getItems());
						
					}
					
					if ( rowDimChanged ) {
						
						currentViewSection.getPafViewSection().setRowTuples(null);
						removeGroupingSpec(PafAxis.ROW, currentViewSection.getPafViewSection());
						viewSection.setRowAxisDims(rowList.getItems());
						
					}
					
				}				
						
				
			}
			
		}

	}
	
	private void removeGroupingSpec(int axisIndex, PafViewSection viewSection){
		boolean found = false;
		int grpIndex = 0;
		if(viewSection.getGroupingSpecs() != null){
			for(int i  = 0; i < viewSection.getGroupingSpecs().length; i++){
				GroupingSpec spec = viewSection.getGroupingSpecs()[i];
				if(spec == null) continue;
				PafAxis axis = new PafAxis(spec.getAxis());
				if(axis.getValue() == axisIndex){
					found = true;
					grpIndex = i;
					break;
				}
			}
			if(found){
				GroupingSpec[] temp = viewSection.getGroupingSpecs();
				temp[grpIndex] = null;
				viewSection.setGroupingSpecs(temp);
			}
		}
	}

	private boolean dimensionsChanged(String[] currentDims, String[] updatedDims) {

		if (currentDims == null
				&& (updatedDims != null || updatedDims.length != 0)) {
			return true;
		}

		if (currentDims.length != updatedDims.length) {
			return true;
		}

		for (int i = 0; i < currentDims.length; i++) {

			if (!currentDims[i].equals(updatedDims[i])) {
				return true;
			}

		}

		return false;

	}
	
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout_4 = new GridLayout();
		container.setLayout(gridLayout_4);

		final Group dimensionsGroup = new Group(container, SWT.NONE);
		dimensionsGroup.setText("Dimensions");
		final GridData gridData_5 = new GridData(SWT.FILL,
				SWT.FILL, true, true);
		gridData_5.widthHint = 549;
		gridData_5.heightHint = 356;
		dimensionsGroup.setLayoutData(gridData_5);
		dimensionsGroup.setLayout(new GridLayout());

		final Composite container_1 = new Composite(dimensionsGroup, SWT.NONE);
		final GridData gridData_4 = new GridData(SWT.FILL,
				SWT.FILL, true, true);
		gridData_4.widthHint = 470;
		container_1.setLayoutData(gridData_4);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container_1.setLayout(gridLayout);

		final Composite composite_1 = new Composite(container_1, SWT.NONE);
		final GridData gridData_7 = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData_7.heightHint = 333;
		gridData_7.widthHint = 235;
		composite_1.setLayoutData(gridData_7);
		composite_1.setLayout(new GridLayout());

		final Composite composite = new Composite(composite_1, SWT.NONE);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL,
				true, true);
		gridData_1.heightHint = 333;
		gridData_1.widthHint = 231;
		composite.setLayoutData(gridData_1);
		final GridLayout gridLayout_6 = new GridLayout();
		composite.setLayout(gridLayout_6);

		final Label dimensionsLabel = new Label(composite, SWT.NONE);
		GridData gd_dimensionsLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gd_dimensionsLabel.widthHint = 161;
		dimensionsLabel.setLayoutData(gd_dimensionsLabel);
		dimensionsLabel.setText("Available Dimensions:");

		dimensionsTree = new Tree(composite, SWT.BORDER);
		final GridData gridData_6 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_6.heightHint = 119;
		gridData_6.widthHint = 189;
		dimensionsTree.setLayoutData(gridData_6);
		final Composite composite_8 = new Composite(composite, SWT.NONE);
		composite_8.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.marginWidth = 0;
		gridLayout_7.marginHeight = 0;
		gridLayout_7.verticalSpacing = 0;
		gridLayout_7.numColumns = 2;
		composite_8.setLayout(gridLayout_7);

		showAllButton = new Button(composite_8, SWT.NONE);
		showAllButton.setEnabled(false);
		showAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				EditorControlUtil.expandTree(dimensionsTree.getItems());
			}
		});
		showAllButton.setText("&Show Attributes");

		hideAllButton = new Button(composite_8, SWT.NONE);
		hideAllButton.setEnabled(false);
		hideAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				EditorControlUtil.collapseTree(dimensionsTree.getItems());
			}
		});
		hideAllButton.setText("&Hide Attributes");

		final Composite composite_2 = new Composite(container_1, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 336;
		gridData.widthHint = 323;
		composite_2.setLayoutData(gridData);
		composite_2.setLayout(new GridLayout());

		final Composite composite_3 = new Composite(composite_2, SWT.NONE);
		final GridData gridData_8 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_8.heightHint = 101;
		gridData_8.widthHint = 281;
		composite_3.setLayoutData(gridData_8);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 3;
		composite_3.setLayout(gridLayout_1);

		final Label label = new Label(composite_3, SWT.NONE);

		final Label pageLabel = new Label(composite_3, SWT.NONE);
		pageLabel.setText("Page Dimensions:");

		final Label label_1 = new Label(composite_3, SWT.NONE);

		final Composite composite_7 = new Composite(composite_3, SWT.NONE);
		composite_7.setLayoutData(new GridData(65, 50));
		composite_7.setLayout(new GridLayout());

		pageInButton = new Button(composite_7, SWT.ARROW | SWT.RIGHT);
		pageInButton.setLayoutData(new GridData(40, SWT.DEFAULT));
		addSelectionListener(pageInButton, PAGE_IN_BUTTON, PAGE_BUTTON_GROUP);

		pageOutButton = new Button(composite_7, SWT.ARROW | SWT.LEFT);
		pageOutButton.setLayoutData(new GridData(40, SWT.DEFAULT));
		addSelectionListener(pageOutButton, PAGE_OUT_BUTTON, PAGE_BUTTON_GROUP);

		pageList = new List(composite_3, SWT.V_SCROLL | SWT.BORDER
				| SWT.H_SCROLL);
		GridData gd_pageList = new GridData(115, 55);
		gd_pageList.verticalAlignment = SWT.FILL;
		gd_pageList.horizontalAlignment = SWT.FILL;
		gd_pageList.grabExcessVerticalSpace = true;
		gd_pageList.grabExcessHorizontalSpace = true;
		pageList.setLayoutData(gd_pageList);

		final Composite composite_4 = new Composite(composite_3, SWT.NONE);
		composite_4.setLayoutData(new GridData(31, 51));
		composite_4.setLayout(new GridLayout());

		pageUpButton = new Button(composite_4, SWT.ARROW);
		addSelectionListener(pageUpButton, PAGE_UP_BUTTON, PAGE_BUTTON_GROUP);

		pageDownButton = new Button(composite_4, SWT.ARROW | SWT.DOWN);

		addSelectionListener(pageDownButton, PAGE_DOWN_BUTTON,
				PAGE_BUTTON_GROUP);

		final Composite composite_5 = new Composite(composite_2, SWT.NONE);
		final GridData gridData_9 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gridData_9.heightHint = 101;
		gridData_9.widthHint = 268;
		composite_5.setLayoutData(gridData_9);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 3;
		composite_5.setLayout(gridLayout_2);

		final Label label_2 = new Label(composite_5, SWT.NONE);

		final Label pageLabel_1 = new Label(composite_5, SWT.NONE);
		pageLabel_1.setText("Column Dimensions:");

		final Label label_3 = new Label(composite_5, SWT.NONE);

		final Composite composite_7_1 = new Composite(composite_5, SWT.NONE);
		composite_7_1.setLayoutData(new GridData(63, 50));
		composite_7_1.setLayout(new GridLayout());

		columnInButton = new Button(composite_7_1, SWT.ARROW | SWT.RIGHT);
		columnInButton.setLayoutData(new GridData(40, SWT.DEFAULT));

		addSelectionListener(columnInButton, COL_IN_BUTTON, COLUMN_BUTTON_GROUP);

		columnOutButton = new Button(composite_7_1, SWT.ARROW | SWT.LEFT);
		columnOutButton.setLayoutData(new GridData(40, SWT.DEFAULT));
		addSelectionListener(columnOutButton, COL_OUT_BUTTON,
				COLUMN_BUTTON_GROUP);

		columnList = new List(composite_5, SWT.V_SCROLL | SWT.BORDER
				| SWT.H_SCROLL);
		GridData gd_columnList = new GridData(115, 55);
		gd_columnList.verticalAlignment = SWT.FILL;
		gd_columnList.grabExcessVerticalSpace = true;
		gd_columnList.grabExcessHorizontalSpace = true;
		gd_columnList.horizontalAlignment = SWT.FILL;
		columnList.setLayoutData(gd_columnList);

		final Composite composite_4_1 = new Composite(composite_5, SWT.NONE);
		composite_4_1.setLayoutData(new GridData(33, 60));
		composite_4_1.setLayout(new GridLayout());

		columnUpButton = new Button(composite_4_1, SWT.ARROW);
		addSelectionListener(columnUpButton, COL_UP_BUTTON, COLUMN_BUTTON_GROUP);

		columnDownButton = new Button(composite_4_1, SWT.ARROW | SWT.DOWN);
		addSelectionListener(columnDownButton, COL_DOWN_BUTTON,
				COLUMN_BUTTON_GROUP);

		final Composite composite_6 = new Composite(composite_2, SWT.NONE);
		final GridData gridData_10 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gridData_10.heightHint = 112;
		gridData_10.widthHint = 268;
		composite_6.setLayoutData(gridData_10);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 3;
		composite_6.setLayout(gridLayout_3);

		final Label label_4 = new Label(composite_6, SWT.NONE);

		final Label pageLabel_1_1 = new Label(composite_6, SWT.NONE);
		pageLabel_1_1.setText("Row Dimensions:");

		final Label label_5 = new Label(composite_6, SWT.NONE);

		final Composite composite_7_2 = new Composite(composite_6, SWT.NONE);
		composite_7_2.setLayoutData(new GridData(64, 50));
		composite_7_2.setLayout(new GridLayout());

		rowInButton = new Button(composite_7_2, SWT.ARROW | SWT.RIGHT);
		rowInButton.setLayoutData(new GridData(40, SWT.DEFAULT));
		addSelectionListener(rowInButton, ROW_IN_BUTTON, ROW_BUTTON_GROUP);

		rowOutButton = new Button(composite_7_2, SWT.ARROW | SWT.LEFT);
		rowOutButton.setLayoutData(new GridData(40, SWT.DEFAULT));
		addSelectionListener(rowOutButton, ROW_OUT_BUTTON, ROW_BUTTON_GROUP);

		rowList = new List(composite_6, SWT.V_SCROLL | SWT.BORDER
				| SWT.H_SCROLL);
		GridData gd_rowList = new GridData(115, 55);
		gd_rowList.grabExcessVerticalSpace = true;
		gd_rowList.grabExcessHorizontalSpace = true;
		gd_rowList.horizontalAlignment = SWT.FILL;
		gd_rowList.verticalAlignment = SWT.FILL;
		rowList.setLayoutData(gd_rowList);

		final Composite composite_4_1_1 = new Composite(composite_6, SWT.NONE);
		composite_4_1_1.setLayoutData(new GridData(34, 58));
		composite_4_1_1.setLayout(new GridLayout());

		rowUpButton = new Button(composite_4_1_1, SWT.ARROW);
		addSelectionListener(rowUpButton, ROW_UP_BUTTON, ROW_BUTTON_GROUP);

		rowDownButton = new Button(composite_4_1_1, SWT.ARROW | SWT.DOWN);
		addSelectionListener(rowDownButton, ROW_DOWN_BUTTON, ROW_BUTTON_GROUP);

		final Label youMustAssignLabel = new Label(container, SWT.CENTER);
		youMustAssignLabel.setAlignment(SWT.CENTER);
		youMustAssignLabel.setLayoutData(new GridData(476, SWT.DEFAULT));
		youMustAssignLabel.setText("You must assign every available base dimension to an axis dimension.");

		updateButtonGroup(PAGE_BUTTON_GROUP);
		updateButtonGroup(COLUMN_BUTTON_GROUP);
		updateButtonGroup(ROW_BUTTON_GROUP);

		enabledTreeItemColor = dimensionsTree.getForeground();
		disabledTreeItemColor = this.getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY);
		
		initializeListBoxes();

		setupListeners();

		setupMenus();
		
		return container;
	}

	
	/**
	 * The tree item should be bolded / unbolded based on bold flag.
	 * Then the parent item should be bolded / unbolded based on if 
	 * any of it's children are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded            
	 */	
	
	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		Font font = treeItem.getFont();
		
		//get font data ar for tree item
		FontData[] fontDataAr = font.getFontData();
		
		if (fontDataAr.length > 0) {

			//get font data
			FontData fontData = fontDataAr[0];

			//if bold is true, set style to bold, else to none
			if (bold) {
				
				fontData.setStyle(SWT.BOLD);
				
				if (boldFont == null ) {
				
					boldFont = new Font(treeItem.getDisplay(), fontData);
					
				}
			
				treeItem.setFont(boldFont);
				
			} else {
				
				fontData.setStyle(SWT.NONE);
				
				if ( normalFont == null) {
					
					normalFont = new Font(treeItem.getDisplay(), fontData);
					
				}
				
				treeItem.setFont(normalFont);
			}
		
			
		}

	}
	
	
	private void setupListeners() {

		dimensionsTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				columnList.deselectAll();
				pageList.deselectAll();
				rowList.deselectAll();
				
				updateInButtons();
				disableButtonGroup(PAGE_BUTTON_GROUP);
				disableButtonGroup(COLUMN_BUTTON_GROUP);
				disableButtonGroup(ROW_BUTTON_GROUP);
			}
		});

		pageList.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				dimensionsTree.deselectAll();
				columnList.deselectAll();
				rowList.deselectAll();

				updateButtonGroup(PAGE_BUTTON_GROUP);
				disableButtonGroup(ROW_BUTTON_GROUP);
				disableButtonGroup(COLUMN_BUTTON_GROUP);

			}

		});

		columnList.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				dimensionsTree.deselectAll();
				pageList.deselectAll();
				rowList.deselectAll();

				updateButtonGroup(COLUMN_BUTTON_GROUP);
				disableButtonGroup(ROW_BUTTON_GROUP);
				disableButtonGroup(PAGE_BUTTON_GROUP);

			}

		});

		rowList.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				dimensionsTree.deselectAll();
				columnList.deselectAll();
				pageList.deselectAll();

				updateButtonGroup(ROW_BUTTON_GROUP);
				disableButtonGroup(COLUMN_BUTTON_GROUP);
				disableButtonGroup(PAGE_BUTTON_GROUP);

			}

		});

	}

	private void setupMenus(){
		
		final MenuManager sourceMenuManager = new MenuManager();
		sourceMenuManager.setRemoveAllWhenShown(true);
		sourceMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				
				
				ImageDescriptor rightImage = MenuPlugin.getImageDescriptor("icons/right_arrow.gif");
				
				Action addToPage = new Action() {
					
					@Override
					public String getText() {
						return "Add To Page";
					}

					@Override
					public void run() {
						buttonSelected(PAGE_IN_BUTTON, PAGE_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};
				
				Action addToColumn = new Action() {
					
					@Override
					public String getText() {
						return "Add To Column";
					}

					@Override
					public void run() {
						buttonSelected(COL_IN_BUTTON, COLUMN_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};

				
				Action addToRow = new Action() {
					
					@Override
					public String getText() {
						return "Add To Row";
					}

					@Override
					public void run() {
						buttonSelected(ROW_IN_BUTTON, ROW_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};
				
				addToPage.setImageDescriptor(rightImage);
				addToColumn.setImageDescriptor(rightImage);
				addToRow.setImageDescriptor(rightImage);
				
				boolean disabledItem = false;
				
				//if no items are selected then make sure the in buttons are disabled.
				if(dimensionsTree.getSelection().length == 0){
					disabledItem = true;
				}else{
					for(TreeItem item : dimensionsTree.getSelection()){
						if(item.getForeground().equals(disabledTreeItemColor)){
							disabledItem = true;
							break;
						}
					}
				}
				
				
				if(disabledItem){
					addToPage.setEnabled(false);
					addToColumn.setEnabled(false);
					addToRow.setEnabled(false);
				}
				
				sourceMenuManager.add(addToPage);
				sourceMenuManager.add(addToColumn);
				sourceMenuManager.add(addToRow);
				
				
			}
		});
		dimensionsTree.setMenu(sourceMenuManager.createContextMenu(dimensionsTree));
		
		final MenuManager pageDimensionMenuManager = new MenuManager();
		pageDimensionMenuManager.setRemoveAllWhenShown(true);
		pageDimensionMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				Image removeImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
				ImageDescriptor remove = ImageDescriptor.createFromImage(removeImage);
				
				Action pageDeleteMember = new Action() {
					
					@Override
					public String getText() {
						return "Remove Dimension";
					}

					@Override
					public void run() {
						buttonSelected(PAGE_OUT_BUTTON, PAGE_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};
			
				pageDeleteMember.setImageDescriptor(remove);
			
				int numberOfItems = pageList.getItemCount();
				int selectionIdx = pageList.getSelectionIndex();
				
				if (numberOfItems >= 1
						&& selectionIdx >= 0) {
					pageDeleteMember.setEnabled(true);
				} else{
					pageDeleteMember.setEnabled(false);
				}

				pageDimensionMenuManager.add(pageDeleteMember);
			}
		});
		pageList.setMenu(pageDimensionMenuManager.createContextMenu(pageList));
		
		final MenuManager columnDimensionMenuManager = new MenuManager();
		columnDimensionMenuManager.setRemoveAllWhenShown(true);
		columnDimensionMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				Image removeImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
				ImageDescriptor remove = ImageDescriptor.createFromImage(removeImage);
				
				Action columnDeleteMember = new Action() {
					
					@Override
					public String getText() {
						return "Remove Dimension";
					}

					@Override
					public void run() {
						buttonSelected(COL_OUT_BUTTON, COLUMN_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};
			
				columnDeleteMember.setImageDescriptor(remove);
			
				int numberOfItems = columnList.getItemCount();
				int selectionIdx = columnList.getSelectionIndex();
				
				if (numberOfItems >= 1
						&& selectionIdx >= 0) {
					columnDeleteMember.setEnabled(true);
				} else{
					columnDeleteMember.setEnabled(false);
				}

				columnDimensionMenuManager.add(columnDeleteMember);
			}
		});
		columnList.setMenu(columnDimensionMenuManager.createContextMenu(columnList));
		
		
		
		final MenuManager rowDimensionMenuManager = new MenuManager();
		rowDimensionMenuManager.setRemoveAllWhenShown(true);
		rowDimensionMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				Image removeImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
				ImageDescriptor remove = ImageDescriptor.createFromImage(removeImage);
				
				Action rowDeleteMember = new Action() {
					
					@Override
					public String getText() {
						return "Remove Dimension";
					}

					@Override
					public void run() {
						buttonSelected(ROW_OUT_BUTTON, ROW_BUTTON_GROUP);
						getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());
					}

				};
			
				rowDeleteMember.setImageDescriptor(remove);
			
				int numberOfItems = rowList.getItemCount();
				int selectionIdx = rowList.getSelectionIndex();
				
				if (numberOfItems >= 1
						&& selectionIdx >= 0) {
					rowDeleteMember.setEnabled(true);
				} else{
					rowDeleteMember.setEnabled(false);
				}

				rowDimensionMenuManager.add(rowDeleteMember);
			}
		});
		rowList.setMenu(rowDimensionMenuManager.createContextMenu(rowList));
	}
	
	private void addSelectionListener(Button button, final int buttonId,
			final int buttonGroup) {

		button.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				buttonSelected(buttonId, buttonGroup);

				// disable other button groups.
				if (buttonGroup == PAGE_BUTTON_GROUP) {
					disableButtonGroup(COLUMN_BUTTON_GROUP);
					disableButtonGroup(ROW_BUTTON_GROUP);
				} else if (buttonGroup == COLUMN_BUTTON_GROUP) {
					disableButtonGroup(PAGE_BUTTON_GROUP);
					disableButtonGroup(ROW_BUTTON_GROUP);
				} else if (buttonGroup == ROW_BUTTON_GROUP) {
					disableButtonGroup(PAGE_BUTTON_GROUP);
					disableButtonGroup(COLUMN_BUTTON_GROUP);
				}

				getButton(IDialogConstants.OK_ID).setEnabled(
						getOkButtonStatus());

			}

		});

	}

	protected void buttonSelected(int buttonId, int buttonGroup) {

		boolean outButtonPressed = false;

		if (buttonId == PAGE_IN_BUTTON) {
			moveFromTree(dimensionsTree, pageList);
		} else if (buttonId == PAGE_OUT_BUTTON) {
			moveToTree(pageList, dimensionsTree);
			outButtonPressed = true;
		} else if (buttonId == PAGE_UP_BUTTON) {
			moveInList(pageList, BUTTON_UP);
		} else if (buttonId == PAGE_DOWN_BUTTON) {
			moveInList(pageList, BUTTON_DOWN);
		} else if (buttonId == COL_IN_BUTTON) {
			moveFromTree(dimensionsTree, columnList);
		} else if (buttonId == COL_OUT_BUTTON) {
			moveToTree(columnList, dimensionsTree);
			outButtonPressed = true;
		} else if (buttonId == COL_UP_BUTTON) {
			moveInList(columnList, BUTTON_UP);
		} else if (buttonId == COL_DOWN_BUTTON) {
			moveInList(columnList, BUTTON_DOWN);
		} else if (buttonId == ROW_IN_BUTTON) {
			moveFromTree(dimensionsTree, rowList);
		} else if (buttonId == ROW_OUT_BUTTON) {
			moveToTree(rowList, dimensionsTree);
			outButtonPressed = true;
		} else if (buttonId == ROW_UP_BUTTON) {
			moveInList(rowList, BUTTON_UP);
		} else if (buttonId == ROW_DOWN_BUTTON) {
			moveInList(rowList, BUTTON_DOWN);
		}

		// if an out button was pressed, disable group
		if (outButtonPressed) {
			disableButtonGroup(buttonGroup);
			updateInButtons();
		} else {
			updateButtonGroup(buttonGroup);
		}

	}

	private void updateButtonGroup(int buttonGroup) {

		if (buttonGroup == PAGE_BUTTON_GROUP) {
			updateButtons(pageList, pageOutButton, pageUpButton, pageDownButton);
		} else if (buttonGroup == COLUMN_BUTTON_GROUP) {
			updateButtons(columnList, columnOutButton, columnUpButton,
					columnDownButton);
		} else if (buttonGroup == ROW_BUTTON_GROUP) {
			updateButtons(rowList, rowOutButton, rowUpButton, rowDownButton);
		}

	}

	private void updateInButtons() {
		boolean disabledItem = false;
		
		//if no items are selected then make sure the in buttons are disabled.
		if(dimensionsTree.getSelection().length == 0){
			disabledItem = true;
		}else{
			for(TreeItem item : dimensionsTree.getSelection()){
				if(item.getForeground().equals(disabledTreeItemColor)){
					disabledItem = true;
					break;
				}
			}
		}
		
		if(! disabledItem){
			enableButton(pageInButton);
			enableButton(columnInButton);
			enableButton(rowInButton);
		} else {
			disableButton(pageInButton);
			disableButton(columnInButton);
			disableButton(rowInButton);
		}

	}

	private void updateButtons(List list, Button outButton, Button upButton,
			Button downButton) {

		updateInButtons();

		int numberOfItems = list.getItemCount();

		if (numberOfItems == 0) {
			disableButton(outButton);
			disableButton(upButton);
			disableButton(downButton);
		} else if (numberOfItems == 1) {
			enableButton(outButton);
			disableButton(upButton);
			disableButton(downButton);
		} else if (numberOfItems > 1 && list.getSelectionIndex() == 0) {
			enableButton(outButton);
			disableButton(upButton);
			enableButton(downButton);
		} else if (numberOfItems > 1
				&& list.getSelectionIndex() == (list.getItemCount() - 1)) {
			enableButton(outButton);
			enableButton(upButton);
			disableButton(downButton);
		} else {
			enableButton(outButton);
			enableButton(upButton);
			enableButton(downButton);
		}

	}
	
	private void moveFromTree(Tree from, List to) {
		TreeItem[] items = from.getSelection();
		
		if (items.length >= 0) {
			for(TreeItem item : items){
				to.add(item.getText());
				item.setForeground(disabledTreeItemColor);
				boldTreeItem(item, false);
				to.select(to.getItemCount() - 1);
			}
			from.setSelection(new TreeItem[0]);
		}
		
	}

	private void moveToTree(List from, Tree to){
		int selectedIndex = from.getSelectionIndex();

		if (selectedIndex >= 0) {
			String selectedText = from.getItem(selectedIndex);
			TreeItem node = EditorControlUtil.findTreeItem(to.getItems(), selectedText);
			if(node != null){
				node.setForeground(this.enabledTreeItemColor);
				boldTreeItem(node, true);
			}
			from.remove(selectedIndex);
			invalidDimensions.remove(selectedText);
		}
	}
	
	
	
	
	private void moveInList(List list, int direction) {

		int selectedIndex = list.getSelectionIndex();

		if (selectedIndex >= 0) {

			if (direction == BUTTON_UP) {

				if (selectedIndex != 0 && list.getItemCount() >= 2) {

					String selectedText = list.getItem(selectedIndex);
					int swappedIndex = selectedIndex - 1;
					String swappedText = list.getItem(swappedIndex);

					// remove both selectedIndex and next one
					list.remove(selectedIndex);
					list.remove(swappedIndex);

					// add again
					list.add(selectedText, swappedIndex);
					list.add(swappedText, selectedIndex);

					list.select(swappedIndex);

				}

			} else if (direction == BUTTON_DOWN) {

				if (selectedIndex != list.getItemCount()
						&& list.getItemCount() >= 2) {

					String selectedText = list.getItem(selectedIndex);
					int swappedIndex = selectedIndex + 1;
					String swappedText = list.getItem(swappedIndex);

					// remove both selectedIndex and next one
					list.remove(swappedIndex);
					list.remove(selectedIndex);

					// add again
					list.add(swappedText, selectedIndex);
					list.add(selectedText, swappedIndex);

					list.select(swappedIndex);

				}

			}

		}

	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(584, 501);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Assign Dimensions");
	}

	private void disableButtonGroup(int buttonGroup) {

		if (buttonGroup == PAGE_BUTTON_GROUP) {
			disableButton(pageOutButton);
			disableButton(pageUpButton);
			disableButton(pageDownButton);
		} else if (buttonGroup == COLUMN_BUTTON_GROUP) {
			disableButton(columnOutButton);
			disableButton(columnUpButton);
			disableButton(columnDownButton);
		} else if (buttonGroup == ROW_BUTTON_GROUP) {
			disableButton(rowOutButton);
			disableButton(rowUpButton);
			disableButton(rowDownButton);
		}

	}

	private void disableButton(Button button) {
		button.setEnabled(false);
	}

	private void enableButton(Button button) {
		button.setEnabled(true);
	}

	@Override
	protected void okPressed() {
		save();
		super.okPressed();

	}

	@Override
	public void create() {

		super.create();

		getButton(IDialogConstants.OK_ID).setEnabled(getOkButtonStatus());

	}

	public boolean close(){
		if ( disabledTreeItemColor != null ) {
			disabledTreeItemColor.dispose();
		}
		
		if ( enabledTreeItemColor != null ) {
			enabledTreeItemColor.dispose();
		}
		
		if(boldFont != null){
			boldFont.dispose();
		}
		
		if(normalFont != null){
			normalFont.dispose();
		}
		
		super.close();
		
		return true;
	}
	
	private boolean getOkButtonStatus() {

		boolean enableButton = true;

		if(invalidDimensions.size() > 0){
			return false;
		}
		
		for(TreeItem item : dimensionsTree.getItems()){
			if(item.getForeground().equals(enabledTreeItemColor)){
				enableButton = false;
				break;
			}
		}
		return enableButton;
	}
}
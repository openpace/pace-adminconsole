/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class SeasonIdDialog extends Dialog {

	private Combo combo_2;
	private Combo combo_1;
	private Combo combo;
	public SeasonIdDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE  );
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		final Label planCycleLabel = new Label(container, SWT.NONE);
		planCycleLabel.setAlignment(SWT.RIGHT);
		final FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 35);
		formData.right = new FormAttachment(0, 96);
		formData.top = new FormAttachment(0, 20);
		formData.left = new FormAttachment(0, 36);
		planCycleLabel.setLayoutData(formData);
		planCycleLabel.setText("Plan Cycle:");

		combo = new Combo(container, SWT.NONE);
		final FormData formData_1 = new FormData();
		formData_1.top = new FormAttachment(0, 20);
		formData_1.bottom = new FormAttachment(0, 41);
		formData_1.right = new FormAttachment(0, 236);
		formData_1.left = new FormAttachment(0, 106);
		combo.setLayoutData(formData_1);

		final Label yearLabel = new Label(container, SWT.NONE);
		yearLabel.setAlignment(SWT.RIGHT);
		final FormData formData_2 = new FormData();
		formData_2.top = new FormAttachment(0, 47);
		formData_2.left = new FormAttachment(0, 36);
		formData_2.right = new FormAttachment(0, 96);
		formData_2.bottom = new FormAttachment(0, 67);
		yearLabel.setLayoutData(formData_2);
		yearLabel.setText("Year:");

		combo_1 = new Combo(container, SWT.NONE);
		final FormData formData_3 = new FormData();
		formData_3.left = new FormAttachment(0, 106);
		formData_3.bottom = new FormAttachment(0, 69);
		formData_3.right = new FormAttachment(0, 236);
		formData_3.top = new FormAttachment(0, 48);
		combo_1.setLayoutData(formData_3);

		final Label seasonLabel = new Label(container, SWT.NONE);
		seasonLabel.setAlignment(SWT.RIGHT);
		final FormData formData_4 = new FormData();
		formData_4.top = new FormAttachment(0, 72);
		formData_4.left = new FormAttachment(0, 41);
		formData_4.bottom = new FormAttachment(0, 87);
		formData_4.right = new FormAttachment(0, 96);
		seasonLabel.setLayoutData(formData_4);
		seasonLabel.setText("Season:");

		combo_2 = new Combo(container, SWT.NONE);
		final FormData formData_5 = new FormData();
		formData_5.top = new FormAttachment(0, 74);
		formData_5.left = new FormAttachment(0, 106);
		formData_5.bottom = new FormAttachment(0, 95);
		formData_5.right = new FormAttachment(0, 236);
		combo_2.setLayoutData(formData_5);
		//
		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(280, 205);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Season Id");
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.server.client.PafMdbProps;

/**
 * Dialog used to selected attribute dimension names.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DimensionAttributeSelectorDialog extends TitleAreaDialog {

	// selected attrbuite dimensions list
	private List selectedAttributeDimensionsList;

	// tree of hier dims with associated attribute names
	private Tree dimensionAttributeTree;

	// Used to change the color of a selected dimension, so it looks disabled.
	private Color disabledTreeItemColor = null;

	// The default color for an enabled dimension item.
	private Color enabledTreeItemColor = null;

	// bold font
	private Font boldFont;

	// standard font
	private Font normalFont;

	// current project
	private IProject project;
	
	private String[] selectedAttributeDimensions;

	private Map<String, java.util.List<String>> dimensionAttributesMap = new HashMap<String, java.util.List<String>>();

	private Map<String, TreeItem> treeItemMap = new HashMap<String, TreeItem>();

	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 */
	public DimensionAttributeSelectorDialog(Shell parentShell,
			IProject project, String[] selectedAttributeDimensions) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;
		this.selectedAttributeDimensions = selectedAttributeDimensions;
	}

	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 10;
		gridLayout.marginHeight = 30;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		final Composite container_1 = new Composite(container, SWT.NONE);
		container_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 4;
		container_1.setLayout(gridLayout_1);

		final Composite composite = new Composite(container_1, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(new GridLayout());

		final Label dimensionsAndAttributeLabel = new Label(composite, SWT.NONE);
		dimensionsAndAttributeLabel.setText("Security Dimension Attributes");

		dimensionAttributeTree = new Tree(composite, SWT.MULTI | SWT.BORDER);
		dimensionAttributeTree.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent arg0) {

				moveSelectedItemsToListAction();

			}
		});
		dimensionAttributeTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				selectedAttributeDimensionsList.deselectAll();

			}
		});
		final GridData gridData_3 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_3.widthHint = 200;
		dimensionAttributeTree.setLayoutData(gridData_3);

		final Composite composite_1 = new Composite(container_1, SWT.NONE);
		final GridData gridData = new GridData(SWT.CENTER, SWT.FILL, false,
				true);
		gridData.widthHint = 56;
		composite_1.setLayoutData(gridData);
		composite_1.setLayout(new GridLayout());

		final Composite composite_1_1 = new Composite(composite_1, SWT.NONE);
		final GridData gridData_1 = new GridData(SWT.CENTER, SWT.FILL, false,
				true);
		gridData_1.widthHint = 48;
		composite_1_1.setLayoutData(gridData_1);
		composite_1_1.setLayout(new GridLayout());

		final Composite composite_3 = new Composite(composite_1_1, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.CENTER, SWT.CENTER, false,
				true);
		gridData_2.widthHint = 36;
		composite_3.setLayoutData(gridData_2);
		composite_3.setLayout(new GridLayout());

		final Button rightButton = new Button(composite_3, SWT.ARROW
				| SWT.RIGHT);
		rightButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				moveSelectedItemsToListAction();

			}
		});
		rightButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));
		rightButton.setText("button");

		final Button leftButton = new Button(composite_3, SWT.ARROW | SWT.LEFT);
		leftButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				removeSelectedItemsFromListAction();

			}
		});
		leftButton
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		leftButton.setText("button");

		final Composite composite_2 = new Composite(container_1, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite_2.setLayout(new GridLayout());

		final Label selectedDimensionAndLabel = new Label(composite_2, SWT.NONE);
		selectedDimensionAndLabel.setText("Selected Attribute Dimensions");

		selectedAttributeDimensionsList = new List(composite_2, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		selectedAttributeDimensionsList.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent arg0) {
				removeSelectedItemsFromListAction();
			}
		});
		selectedAttributeDimensionsList
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {

						dimensionAttributeTree.setSelection(new TreeItem[0]);

					}
				});
		final GridData gridData_4 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_4.widthHint = 200;
		selectedAttributeDimensionsList.setLayoutData(gridData_4);

		final Composite composite_2_1 = new Composite(container_1, SWT.NONE);
		composite_2_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				true));
		composite_2_1.setLayout(new GridLayout());

		final Composite composite_3_1 = new Composite(composite_2_1, SWT.NONE);
		composite_3_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false));
		composite_3_1.setLayout(new GridLayout());

		final Button upButton = new Button(composite_3_1, SWT.ARROW);
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				ControlUtil.directionalButtonPressed(
						selectedAttributeDimensionsList,
						ControlUtil.UP_BUTTON_ID);

			}
		});
		upButton.setText("button");

		final Button downButton = new Button(composite_3_1, SWT.ARROW
				| SWT.DOWN);
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				ControlUtil.directionalButtonPressed(
						selectedAttributeDimensionsList,
						ControlUtil.DOWN_BUTTON_ID);

			}
		});
		downButton.setText("button");
		setTitle("Attribute Dimensions");
		setMessage("Please select the Attribute Dimenisons from the Security Dimensions Attribute trees.");

		enabledTreeItemColor = dimensionAttributeTree.getForeground();
		disabledTreeItemColor = this.getShell().getDisplay().getSystemColor(
				SWT.COLOR_DARK_GRAY);

		initializeTreeAndListBoxes();

		//
		return area;
	}

	/**
	 * 
	 *  Moves selected tree items from tree to list.
	 *
	 */
	protected void moveSelectedItemsToListAction() {

		//get selected tree items
		TreeItem[] selectedTreeItems = dimensionAttributeTree.getSelection();

		//convert into a List<String>
		java.util.List<String> selectedItemList = new ArrayList<String>(Arrays
				.asList(selectedAttributeDimensionsList.getItems()));

		//for each item in list
		for (TreeItem selectedTreeItem : selectedTreeItems) {

			//get tree item name
			String treeItemName = selectedTreeItem.getText();

			// if not a base dimension and not already in list
			if (!dimensionAttributesMap.containsKey(treeItemName)
					&& !selectedItemList.contains(treeItemName)) {

				selectedAttributeDimensionsList.add(treeItemName);

				selectedTreeItem.setForeground(disabledTreeItemColor);

				boldTreeItem(selectedTreeItem, false);

				selectedAttributeDimensionsList
						.select(selectedAttributeDimensionsList.getItemCount() - 1);

			}

		}

		//clear selection
		dimensionAttributeTree.setSelection(new TreeItem[0]);

	}

	/**
	 * 
	 *  Remove item from list box and put value back into the tree.
	 *
	 */
	protected void removeSelectedItemsFromListAction() {

		//get selected list itmes
		String[] selectedItems = selectedAttributeDimensionsList.getSelection();

		//create empty list of tree itmes
		java.util.List<TreeItem> treeItems = new ArrayList<TreeItem>();

		//loop over selected itmes, remove from list, create new tree item, bold it and then add to tree item list
		for (String selectedItem : selectedItems) {

			selectedAttributeDimensionsList.remove(selectedItem);

			TreeItem treeItem = treeItemMap.get(selectedItem);

			treeItem.setForeground(enabledTreeItemColor);

			boldTreeItem(treeItem, true);

			treeItems.add(treeItem);
		}

		//select items in tree
		dimensionAttributeTree.setSelection(treeItems.toArray(new TreeItem[0]));

	}

	/**
	 * 
	 *  Initializes the 
	 *
	 */
	private void initializeTreeAndListBoxes() {

		Set<String> selectedDims = new TreeSet<String>();

		//if selected attributes is not null, add them to the selected dims set List control
		if (selectedAttributeDimensions != null) {

			selectedDims.addAll(Arrays.asList(selectedAttributeDimensions));

			EditorControlUtil.addItemsToList(selectedAttributeDimensionsList,
					selectedAttributeDimensions);

		}

		PafMdbProps cachedPafMdbProps = null;

		// get the cached mdb props object.
		try {
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(PafProjectUtil
					.getProjectServer(project), PafProjectUtil
					.getApplicationName(project));
		} catch (Exception e) {
			MessageDialog
					.openError(this.getShell(), "Server returned error",
							"An error occurred while trying to load the available cached dimensions.");
		}

		//if cachedPafMdbProps exists
		if (cachedPafMdbProps != null) {

			//get hier dims
			String[] hierarchyDimensions = PafProjectUtil
					.getHierarchyDimensions(project);

			//if hiers exists
			if (hierarchyDimensions != null) {
								
				//get attributes lookup array
				java.util.List<String> attributeDimensionsLookup = cachedPafMdbProps
						.getBaseDimLookupKeys();
				
				//get base lookup array
				java.util.List<String> baseDimensionsLookup = cachedPafMdbProps
						.getBaseDimLookupValues();

				//if both are not null and lenght of arrays match
				if (attributeDimensionsLookup != null
						&& baseDimensionsLookup != null
						&& baseDimensionsLookup.size() == attributeDimensionsLookup.size()) {

					int ndx = 0;

					//loop over base dimensions lookups.  This is using a map keyd off of
					//dimension name, for each dim key, we pull out the value (list of attributes).
					//Next it adds each attrbuite name to dimenion array list.  By the end,
					//there is a complte map keyd off of dim name and values are associated
					//attribute names.
					for (String dimName : baseDimensionsLookup) {

						//get attrbuite dim list
						java.util.List<String> attributeDimensionsList = dimensionAttributesMap
								.get(dimName);

						//if null, create new array list
						if (attributeDimensionsList == null) {
							attributeDimensionsList = new ArrayList<String>();
						}

						//add attribute name into list
						attributeDimensionsList
								.add(attributeDimensionsLookup.get(ndx++));

						//put dimension back into map
						dimensionAttributesMap.put(dimName,
								attributeDimensionsList);

					}

				}

				//for each hier dimension, create the tree structure from the map populated before
				for (String hierarchyDimension : hierarchyDimensions) {
					
					if ( ! dimensionAttributesMap.containsKey(hierarchyDimension)) {
						
						dimensionAttributesMap.put(hierarchyDimension, new ArrayList<String>());
						
					}

					//create dimension node
					TreeItem dimensionNameNode = EditorControlUtil.addTreeNode(
							dimensionAttributeTree, null, hierarchyDimension);

					//bold it
					boldTreeItem(dimensionNameNode, true);

					//push into map
					treeItemMap.put(dimensionNameNode.getText(), dimensionNameNode);

					//get assoicated attributes
					java.util.List<String> attributeDimensions = dimensionAttributesMap
							.get(hierarchyDimension);

					//if attributes list is not null
					if (attributeDimensions != null) {

						//loop over attributes and create attribute tree times
						for (String attributeDimension : attributeDimensions) {

							TreeItem attributeNameNode = EditorControlUtil.addTreeNode(
									dimensionAttributeTree, dimensionNameNode,
									attributeDimension);

							if (selectedDims.contains(attributeDimension)) {
								attributeNameNode.setForeground(disabledTreeItemColor);
							} else {
								boldTreeItem(attributeNameNode, true);
							}

							treeItemMap.put(attributeNameNode.getText(), attributeNameNode);
						}

						dimensionNameNode.setExpanded(true);

					}

				}

			}
		}

	}

	/**
	 * The tree item should be bolded / unbolded based on bold flag. Then the
	 * parent item should be bolded / unbolded based on if any of it's children
	 * are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded
	 */

	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		Font font = treeItem.getFont();

		// get font data ar for tree item
		FontData[] fontDataAr = font.getFontData();

		if (fontDataAr.length > 0) {

			// get font data
			FontData fontData = fontDataAr[0];

			// if bold is true, set style to bold, else to none
			if (bold) {

				fontData.setStyle(SWT.BOLD);

				if (boldFont == null) {

					boldFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(boldFont);

			} else {

				fontData.setStyle(SWT.NONE);

				if (normalFont == null) {

					normalFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(normalFont);
			}

		}

	}

	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(588, 473);
	}

	/**
	 * Configures the shell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Role Filter Attribute Dimension Selector");
	}

	/**
	 * On closing of dialog
	 */
	@Override
	public boolean close() {
		if (disabledTreeItemColor != null) {
			disabledTreeItemColor.dispose();
		}

		if (enabledTreeItemColor != null) {
			enabledTreeItemColor.dispose();
		}

		if (boldFont != null) {
			boldFont.dispose();
		}

		if (normalFont != null) {
			normalFont.dispose();
		}

		super.close();

		return true;
	}

	/**
	 * Any button pressed
	 */
	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == IDialogConstants.OK_ID) {

			selectedAttributeDimensions = selectedAttributeDimensionsList
					.getItems();

		}

		super.buttonPressed(buttonId);
	}

	/**
	 * @return Returns the selectedAttributeDimensions.
	 */
	public String[] getSelectedAttributeDimensions() {
		return selectedAttributeDimensions;
	}
}

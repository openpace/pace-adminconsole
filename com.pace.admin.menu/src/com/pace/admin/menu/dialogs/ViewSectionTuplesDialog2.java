/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
//Disclaimer: this code is complicated and not documented well.  
//You will receive a free pat on the back if enhancing this functionality.
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.model.managers.DynamicMembersModelManager;
import com.pace.admin.global.model.managers.GlobalStyleModelManager;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.model.managers.NumericFormatModelManager;
import com.pace.admin.global.model.managers.UserSelectionModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.MemberTagUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.internal.MemberSelection;
import com.pace.admin.menu.dialogs.internal.MemberSelectionType;
import com.pace.admin.menu.model.PafMember;
import com.pace.admin.menu.model.PafMemberData;
import com.pace.admin.menu.model.PafMemberHeader;
import com.pace.admin.menu.model.PafTuple;
import com.pace.admin.menu.nodes.TempNode;
import com.pace.admin.menu.util.TreeUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.DynamicMemberDef;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.base.ui.PafServer;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafBorder;
import com.pace.base.view.PafUserSelection;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;
import com.pace.base.utility.UserSelectionUtil;
import com.swtdesigner.ResourceManager;

public class ViewSectionTuplesDialog2 extends Dialog {
		
	private static final String BOTTOM = "Bottom";

	private Text headerRowHeight;

	private Text headerColumnWidth;

	private static Logger logger = Logger
			.getLogger(ViewSectionTuplesDialog2.class);
	
	private String[] levelGenBottomDropdownValues = new String[] { BOTTOM, LevelGenerationType.Level.toString(),
			LevelGenerationType.Generation.toString() };
	
	private String[] levelGenDropdownValues = new String[] { LevelGenerationType.Level.toString(),
			LevelGenerationType.Generation.toString() };
	
	private String versionName;

	private TabItem headerTabItem;

	private TabItem dataTabItem;

	private Button downButton;

	private Button upButton;

	private final static int APPLY_BUTTON_ID = 1;

	private final static int CANCEL_BUTTON_ID = 2;

	private static String url;

	public static final int PAGE_INDEX = 0;

	public static final int COL_INDEX = 1;

	public static final int ROW_INDEX = 2;

	private String[] pageDims;

	private String[] colDims;

	private String[] rowDims;

	private Group tuplePropertiesGroup;

	private TabFolder tuplePropsFolder;

	private Button rightButton;

	private Button leftButton;

	private Button applyButton;

	private Button cancelButton;

	private Button selectionButton;

	private Button selectionAndChildrenButton;

	private Button justChildrenOfSelectionButton;

	private Button selectionAndDecendantsButton;

	private Button justDescendantsOfSelectionButton;
	
	private Button justLevelOrGenButton;

	private Button memberSelection;
	
	private Combo levelGenDropDown;
	
	private Spinner levelGenSpinner;	

	private Button headerBorderAllCheck;

	private Button headerBorderRightCheck;

	private Button headerBorderTopCheck;

	private Button headerBorderLeftCheck;

	private Button headerBorderBottomCheck;

	private Button headerParentFirstButton;

	private Button dataBorderAllCheck;

	private Button dataBorderRightCheck;

	private Button dataBorderTopCheck;

	private Button dataBorderLeftCheck;

	private Button dataBorderBottomCheck;

	private Button dataOverrideProtectionCheck;

	private Combo dataMemberTagEditableCombo;
	
	private Tree rowTupleTree;

	private Tree pageTupleTree;

	private Tree columnTupleTree;

	private TabFolder rightTabFolder;

	private Tree rowTree;

	private Tree pageTree;

	private Tree columnTree;

	private TabFolder leftTabFolder;

	private Combo dataOverrideNumeric;

	private Combo dataGlobalStyle;

	private Combo headerGlobalStyle;

	private final static int UP_BUTTON_ID = -1;

	private final static int DOWN_BUTTON_ID = 1;

	private String currentViewSectionName;

	private ViewSectionModelManager viewSectionManager = null;

	private GlobalStyleModelManager globalStyleManager = null;

	private NumericFormatModelManager numericFormatManager = null;

	private UserSelectionModelManager userSelectionManager = null;
	
	private DynamicMembersModelManager dynamicMembersManager = null;
	
	private MemberTagModelManager memberTagModelManager = null;

	private PafViewSectionUI currentViewSection = null;

	private PafMemberHeader currentHeaderState = null;

	private PafMemberData currentDataState = null;

	private PafTuple lastUsedPafTuple = null;

	private boolean dirtyData;

	private boolean cacheHeader = true;

	private boolean cacheData = true;

	private boolean bootingUp = true;

	private boolean updateMemberSelection = false;

	private static final int NUMBER_OF_LEVELS = 13;

	private String[] additionalDimItems = { PafBaseConstants.UOW_ROOT,
			PafBaseConstants.PAF_BLANK };
	
	private String[] additionalAttributeDimItems = { PafBaseConstants.PAF_BLANK };

	public static final String USER_SELECTION_MULTIPLE = "USER_SELECT_MULTIPLE";

	private static final String DISPALY_ONLY_KEY = "DISPLAY_ONLY";
	
	public static final String MEMBER_TAG_KEY = "MEMBER_TAG";

	private Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();

	private Map<String, PafUserSelection[]> userSelectionsMap = null;

	private ArrayList<String> attributeDimensions = null;
	
	private List<String> rootDimsNotAvailForSelection = new ArrayList<String>();
	
	private IProject project;
	
	//<dim name>, map<member key, member>
	private Map<String, Map<String, PafSimpleDimMember>> pafSimpleDimMemberDimensionMap = new HashMap<String, Map<String, PafSimpleDimMember>>();
	
	//map to hold the max number of levels
	private Map<String, Integer> maxTreeDepthMap = new HashMap<String, Integer>();

	private SelectionAdapter levelGenerationComboSelectionAdapter = null;
	
	private ModifyListener levelGenerationSpinnerModifyListener = null;

	boolean creatingDialogArea = true;
	
	public ViewSectionTuplesDialog2(Shell parentShell, IProject project,
			String currentViewSectionName,
			ViewSectionModelManager viewSectionManager) throws Exception {
		super(parentShell);
		
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.project = project;
		this.currentViewSectionName = currentViewSectionName;
		this.versionName = DimensionUtil.getVersionDimensionName(project);
		
		//Get the dimensions which have root nodes that are not selectable, and 
		//store them into an array list.
		PafApplicationDef[] pafAppDef = PafApplicationUtil.getPafApps(project);
		if (pafAppDef != null && pafAppDef.length > 0){
			rootDimsNotAvailForSelection.add(pafAppDef[0].getMdbDef().getMeasureDim().trim());
			rootDimsNotAvailForSelection.add(pafAppDef[0].getMdbDef().getYearDim().trim());
			rootDimsNotAvailForSelection.add(pafAppDef[0].getMdbDef().getVersionDim().trim());
			rootDimsNotAvailForSelection.add(pafAppDef[0].getMdbDef().getPlanTypeDim().trim());
		}
		
		// Create view section Manager
		this.viewSectionManager = viewSectionManager;

		globalStyleManager = new GlobalStyleModelManager(project);
		numericFormatManager = new NumericFormatModelManager(project);
		userSelectionManager = new UserSelectionModelManager(project);
		dynamicMembersManager = new DynamicMembersModelManager(project);
		memberTagModelManager = new MemberTagModelManager(project);

		currentViewSection = (PafViewSectionUI) viewSectionManager
				.getItem(currentViewSectionName);

		if (currentViewSection == null) {
			throw new Exception("View Section: " + currentViewSectionName
					+ " does not exist.");
		}

		// assign dimensions
		pageDims = currentViewSection.getPageAxisDims();
		colDims = currentViewSection.getColAxisDims();
		rowDims = currentViewSection.getRowAxisDims();
		
		//Load the attribute dimensions into an array list.
		PafMdbProps cachedPafMdbProps = null;
		try{
			cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
					PafProjectUtil.getProjectServer(project), 
					PafProjectUtil.getApplicationName(project));
		} catch(Exception e){
			logger.error(e.getMessage());
		}
		if(cachedPafMdbProps != null){
			attributeDimensions = new ArrayList<String>();
			for(String attribute : cachedPafMdbProps.getCachedAttributeDims()){
				attributeDimensions.add(attribute);
			}
			
			//check to see if any dimensions have been dropped.
			validateDimensions(cachedPafMdbProps);
			
		}
		
		
		
		PafServer pafServer = PafProjectUtil.getProjectServer(project);
		
		if ( pafServer != null ) {
			
			url = pafServer.getCompleteWSDLService();
			
			if ( url == null ) {
				
				throw new RuntimeException("No WSDL Service defined in your server.  Check server settings and try again.");
				
			}
			
		}

	}

	/**
	 * Validate the view section to see if any tuples have dropped.
	 * @param cachedPafMdbProps
	 */
	private void validateDimensions(PafMdbProps cachedPafMdbProps){
		Set<String> allDimensions = new HashSet<String>();
		String missingDims = "";
		
		//get an set of the base members.
		for(String baseDim : cachedPafMdbProps.getBaseDims()){
			allDimensions.add(baseDim);			
		}
		
		//build a set of available attribute dimensions.
		for(String dim : cachedPafMdbProps.getCachedAttributeDims()){
			allDimensions.add(dim);
		}

		
		for(String page : pageDims){
			if(! allDimensions.contains(page)){
				currentViewSection.getPafViewSection().setPageTuples(null);	
				missingDims += page + "\n";
			} 
		}
		
		for(String col : colDims){
			if(! allDimensions.contains(col)){
				currentViewSection.getPafViewSection().setColTuples(null);	
				missingDims += col + "\n";
			} 
		}
		
		for(String row : rowDims){
			if(! allDimensions.contains(row)){
				currentViewSection.getPafViewSection().setRowTuples(null);	
				missingDims += row + "\n";
			}
		}

		if(missingDims.length() > 0){
			MessageDialog.openError(this.getShell(),
					"Invalid Dimensions",
					"The following dimensions are no longer valid:\n" + missingDims);
		}
	}
	
	/*
	@Override
	protected void setShellStyle(int newShellStyle) {
		//super.setShellStyle(getShellStyle() | SWT.RESIZE);
		super.setShellStyle(getShellStyle());
	}
	*/
	
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
			
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		container.setLayout(gridLayout_3);

		final Composite composite_3 = new Composite(container, SWT.NONE);
		composite_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 4;
		composite_3.setLayout(gridLayout_4);
		final Label avaliableDimensionsLabel = new Label(composite_3, SWT.NONE);
		avaliableDimensionsLabel.setText("Available Dimensions:");
		new Label(composite_3, SWT.NONE);
		final Label builtTuplesLabel = new Label(composite_3, SWT.NONE);
		builtTuplesLabel.setLayoutData(new GridData());
		builtTuplesLabel.setText("Built Tuples:");
		new Label(composite_3, SWT.NONE);

		leftTabFolder = new TabFolder(composite_3, SWT.NONE);
		final GridData gd_leftTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_leftTabFolder.widthHint = 230;
		leftTabFolder.setLayoutData(gd_leftTabFolder);
		leftTabFolder.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				//if dialog is not starting
				if ( ! creatingDialogArea  ) {
				
					updateTabs(leftTabFolder.getSelectionIndex());
					updateLeftButtonStatus(getTabFolderTree());
					initilizeButton(upButton);
					initilizeButton(downButton);
					initializeForm();
					
				}
				
			}

		});

		final TabItem pageTabItem = new TabItem(leftTabFolder, SWT.NONE);
		pageTabItem.setText("Page");

		pageTree = new Tree(leftTabFolder, SWT.BORDER);
		pageTabItem.setControl(pageTree);

		createTreeModel(pageTree, pageDims);

		final TabItem columnTabItem = new TabItem(leftTabFolder, SWT.NONE);
		columnTabItem.setText("Column");

		columnTree = new Tree(leftTabFolder, SWT.BORDER);
		columnTabItem.setControl(columnTree);

		final TabItem rowTabItem = new TabItem(leftTabFolder, SWT.NONE);
		rowTabItem.setText("Row");

		rowTree = new Tree(leftTabFolder, SWT.BORDER);
		rowTabItem.setControl(rowTree);

		createTreeModel(columnTree, colDims);
		createTreeModel(rowTree, rowDims);

		pageTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				updateRightButtonStatusForPageTuples(pageTree, pageTupleTree);
				initializeForm();

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				
				doubleClickLeftTreeAction(pageTree);
				
							
			}

		});

		rowTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				dimensionTreeItemSelected(rowTree, rowTupleTree);

			}

			public void widgetDefaultSelected(SelectionEvent e) {

				doubleClickLeftTreeAction(rowTree);
			}

		});

		columnTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				dimensionTreeItemSelected(columnTree, columnTupleTree);

			}

			public void widgetDefaultSelected(SelectionEvent e) {

				doubleClickLeftTreeAction(columnTree);

			}

		});

		final Composite composite_4 = new Composite(composite_3, SWT.NONE);
		composite_4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true));
		composite_4.setLayout(new GridLayout());

		final Button button = new Button(composite_4, SWT.NONE);
		button.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/synced.gif"));

		rightButton = new Button(composite_4, SWT.NONE);
		rightButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/right_arrow.gif"));
		rightButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		rightButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				int index = leftTabFolder.getSelectionIndex();

				copyTreeItems(index);

				updateButtonStatus(getTabFolderTree(), upButton, UP_BUTTON_ID);
				updateButtonStatus(getTabFolderTree(), downButton,
						DOWN_BUTTON_ID);

			}

		});
		rightButton.setEnabled(false);

		leftButton = new Button(composite_4, SWT.NONE);
		leftButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/left_arrow.gif"));
		leftButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		leftButton.setEnabled(false);
		leftButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(final SelectionEvent e) {

				removeTreeItems();

				updateLeftButtonStatus(getTabFolderTree());
				updateButtonStatus(getTabFolderTree(), upButton, UP_BUTTON_ID);
				updateButtonStatus(getTabFolderTree(), downButton,
						DOWN_BUTTON_ID);
				initializeForm();
			}

		});

		rightTabFolder = new TabFolder(composite_3, SWT.NONE);
		final GridData gd_rightTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_rightTabFolder.widthHint = 250;
		rightTabFolder.setLayoutData(gd_rightTabFolder);
		rightTabFolder.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				//if dialog is not starting
				if ( ! creatingDialogArea ) {
					
					updateTabs(rightTabFolder.getSelectionIndex());
					initilizeButton(upButton);
					initilizeButton(downButton);
					initializeForm();
					
				}

			}

		});

		final TabItem pageTabItem_1 = new TabItem(rightTabFolder, SWT.NONE);
		pageTabItem_1.setText("Page Tuples");

		pageTupleTree = new Tree(rightTabFolder, SWT.BORDER);
		pageTupleTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				updateLeftButtonStatus(pageTupleTree);
			}

		});
				
		pageTabItem_1.setControl(pageTupleTree);

		final TabItem columnTuplesTabItem = new TabItem(rightTabFolder,
				SWT.NONE);
		columnTuplesTabItem.setText("Column Tuples");

		columnTupleTree = new Tree(rightTabFolder, SWT.BORDER | SWT.MULTI);
		columnTupleTree.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				//if selection count is 1, set to null, otherwise, keep alive
				if ( columnTupleTree.getSelectionCount() == 1 ) {
					currentHeaderState = null;
					currentDataState = null;
				}
				updateLeftButtonStatus(columnTupleTree);
				updateTupleProperties(columnTupleTree, COL_INDEX);
				updateButtonStatus(columnTupleTree, upButton, UP_BUTTON_ID);
				updateButtonStatus(columnTupleTree, downButton, DOWN_BUTTON_ID);
				updateSelectionGroup(columnTupleTree.getSelection());
				
				
				
				populateForm(columnTupleTree.getSelection());

			}
			
			

		});

		columnTuplesTabItem.setControl(columnTupleTree);
		
/*		Menu menu = new Menu(columnTupleTree);
		
		MenuItem mi1 = new MenuItem(menu, SWT.PUSH);
		mi1.setText("Select All");
		mi1.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				//select all tuples
			}
			
		});
		
		MenuItem mi2 = new MenuItem(menu, SWT.PUSH);
		mi2.setText("Select All Blanks");
		mi2.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				//select all PAFBLANK tuples
			}
			
		});
		
		MenuItem mi3 = new MenuItem(menu, SWT.PUSH);
		mi3.setText("Select All Non-Blanks");
		mi3.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				//select all non-PAFBLANK tuples
			}
			
		});
		
		columnTupleTree.setMenu(menu);*/
		
		ViewTuple[] existingColumnTuples = ((PafViewSectionUI) viewSectionManager
				.getItem(currentViewSectionName)).getPafViewSection()
				.getColTuples();

		populateTupleTree(columnTupleTree, existingColumnTuples);

		final TabItem builtRowTabItem = new TabItem(rightTabFolder, SWT.NONE);
		builtRowTabItem.setText("Row Tuples");

		rowTupleTree = new Tree(rightTabFolder, SWT.BORDER | SWT.MULTI);
		rowTupleTree.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				if ( rowTupleTree.getSelectionCount() == 1 ) {
					currentHeaderState = null;
					currentDataState = null;
				}
				
				updateLeftButtonStatus(rowTupleTree);
				updateTupleProperties(rowTupleTree, ROW_INDEX);
				updateButtonStatus(rowTupleTree, upButton, UP_BUTTON_ID);
				updateButtonStatus(rowTupleTree, downButton, DOWN_BUTTON_ID);
				updateSelectionGroup(rowTupleTree.getSelection());
				populateForm(rowTupleTree.getSelection());

			}


		});

		builtRowTabItem.setControl(rowTupleTree);

		ViewTuple[] existingRowTuples = ((PafViewSectionUI) viewSectionManager
				.getItem(currentViewSectionName)).getPafViewSection()
				.getRowTuples();
				
		populateTupleTree(rowTupleTree, existingRowTuples);

		pageTupleTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				pageDimensionTreeItemSelected(pageTree, pageTupleTree);
				
			}

			public void widgetDefaultSelected(SelectionEvent e) {

				TreeUtil.doubleClickSelectionAction(pageTupleTree);

			}

		});

		rowTupleTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				updateMemberSelection = true;
				updateRightButtonStatusForOtherTuples(rowTree, rowTupleTree);
				updateLeftButtonStatus(rowTupleTree);

			}

			public void widgetDefaultSelected(SelectionEvent e) {

				TreeUtil.doubleClickSelectionAction(rowTupleTree);

			}

		});

		columnTupleTree.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				updateMemberSelection = true;
				updateRightButtonStatusForOtherTuples(columnTree,
						columnTupleTree);
				updateLeftButtonStatus(columnTupleTree);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				TreeUtil.doubleClickSelectionAction(columnTupleTree);

			}

		});

		final Composite composite_5 = new Composite(composite_3, SWT.NONE);
		composite_5.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true));
		composite_5.setLayout(new GridLayout());

		upButton = new Button(composite_5, SWT.NONE);
		upButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/up_arrow.gif"));
		upButton.setEnabled(false);
		upButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {

				directionalButtonPressed(UP_BUTTON_ID);

			}

			public void widgetDefaultSelected(SelectionEvent e) {

			}

		});

		downButton = new Button(composite_5, SWT.DOWN);
		downButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/down_arrow.gif"));
		downButton.setEnabled(false);
		downButton.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				directionalButtonPressed(DOWN_BUTTON_ID);

			}

		});

		final Composite composite_6 = new Composite(container, SWT.NONE);
		composite_6.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.numColumns = 2;
		composite_6.setLayout(gridLayout_5);

		final Group memberPropertiesGroup = new Group(composite_6, SWT.NONE);
		final GridLayout gridLayout_8 = new GridLayout();
		gridLayout_8.numColumns = 2;
		memberPropertiesGroup.setLayout(gridLayout_8);
		memberPropertiesGroup.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true));
		memberPropertiesGroup.setText("Member Properties");

		selectionButton = new Button(memberPropertiesGroup, SWT.RADIO);
		selectionButton.setText("Selection");
		selectionButton.setData(MemberSelectionType.Selection);
		selectionButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateMemberSelection((MemberSelectionType) selectionButton
						.getData());
			}

		});
		new Label(memberPropertiesGroup, SWT.NONE);

		memberSelection = new Button(memberPropertiesGroup, SWT.RADIO);
		memberSelection.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		memberSelection.setText("Member Selection");
		memberSelection.setData(MemberSelectionType.Members);
		memberSelection.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateMemberSelection((MemberSelectionType) memberSelection
						.getData());
			}

		});

		selectionAndChildrenButton = new Button(memberPropertiesGroup,
				SWT.RADIO);
		selectionAndChildrenButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		selectionAndChildrenButton.setText("Selection and Children");
		selectionAndChildrenButton.setData(MemberSelectionType.IChildren);
		selectionAndChildrenButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateMemberSelection((MemberSelectionType) selectionAndChildrenButton
						.getData());
			}

		});

		justChildrenOfSelectionButton = new Button(memberPropertiesGroup,
				SWT.RADIO);
		justChildrenOfSelectionButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		justChildrenOfSelectionButton.setText("Just Children of Selection");
		justChildrenOfSelectionButton.setData(MemberSelectionType.Children);
		justChildrenOfSelectionButton
				.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						updateMemberSelection((MemberSelectionType) justChildrenOfSelectionButton
								.getData());
					}

				});

		selectionAndDecendantsButton = new Button(memberPropertiesGroup,
				SWT.RADIO);
		selectionAndDecendantsButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		selectionAndDecendantsButton.setText("Selection and Descendants");
		selectionAndDecendantsButton.setData(MemberSelectionType.IDescendants);
		selectionAndDecendantsButton
				.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						
						updateMemberSelection((MemberSelectionType) selectionAndDecendantsButton
								.getData());

						levelGenDropDown.setItems(levelGenBottomDropdownValues);
						
						populateLevelGenerationFields();
					}

				});

		justDescendantsOfSelectionButton = new Button(memberPropertiesGroup,
				SWT.RADIO);
		justDescendantsOfSelectionButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		justDescendantsOfSelectionButton
				.setText("Just Descendants of Selection");
		justDescendantsOfSelectionButton
				.setData(MemberSelectionType.Descendants);
		justDescendantsOfSelectionButton
				.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						updateMemberSelection((MemberSelectionType) justDescendantsOfSelectionButton
								.getData());
						
						levelGenDropDown.setItems(levelGenBottomDropdownValues);
						
						populateLevelGenerationFields();
						
					}

				});

		justLevelOrGenButton = new Button(memberPropertiesGroup, SWT.RADIO);
		justLevelOrGenButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		justLevelOrGenButton.setText("Just Level or Generation");
		justLevelOrGenButton.setData(MemberSelectionType.Level);
		
		justLevelOrGenButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateMemberSelection((MemberSelectionType) justLevelOrGenButton
						.getData());
				
				levelGenDropDown.setItems(levelGenDropdownValues);
				
				if ( levelGenDropDown.getSelectionIndex() < 0 ) {
					levelGenDropDown.select(0);
				}
				
				populateLevelGenerationFields();
				
			}

		});

		levelGenDropDown = new Combo(memberPropertiesGroup, SWT.READ_ONLY);
		
		levelGenDropDown.setItems(levelGenBottomDropdownValues);
		
		levelGenDropDown.select(0);

		levelGenSpinner = new Spinner(memberPropertiesGroup, SWT.BORDER);

		tuplePropertiesGroup = new Group(composite_6, SWT.NONE);
		tuplePropertiesGroup.setLayout(new GridLayout());
		tuplePropertiesGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		tuplePropertiesGroup.setText("Tuple Properties");

		Label columnWidthLabel;

		Label rowHeightLabel;
		
		Composite composite_2;

		tuplePropsFolder = new TabFolder(tuplePropertiesGroup, SWT.NONE);
		tuplePropsFolder.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		headerTabItem = new TabItem(tuplePropsFolder, SWT.NONE);
		headerTabItem.setText("Header");

		final Composite composite = new Composite(tuplePropsFolder, SWT.NONE);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.numColumns = 6;
		composite.setLayout(gridLayout_6);
		headerTabItem.setControl(composite);

		headerParentFirstButton = new Button(composite, SWT.CHECK);
		headerParentFirstButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		headerParentFirstButton.setText("Parent First");
		headerParentFirstButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();

			}

		});
		columnWidthLabel = new Label(composite, SWT.NONE);
		columnWidthLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		columnWidthLabel.setText("Column Width:");

		headerColumnWidth = new Text(composite, SWT.BORDER);
		headerColumnWidth.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				headerStateChanged();

			}

		});

		headerColumnWidth.addVerifyListener(new VerifyListener() {

			public void verifyText(VerifyEvent e) {

				verifyOnlyNumeric(e);

			}

		});
		rowHeightLabel = new Label(composite, SWT.NONE);
		rowHeightLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		rowHeightLabel.setText("Row Height:");

		headerRowHeight = new Text(composite, SWT.BORDER);
		headerRowHeight.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				headerStateChanged();

			}

		});

		headerRowHeight.addVerifyListener(new VerifyListener() {

			public void verifyText(VerifyEvent e) {

				verifyOnlyNumeric(e);

			}

		});

		final Group borderGroup = new Group(composite, SWT.NONE);
		borderGroup.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 5, 1));
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 5;
		borderGroup.setLayout(gridLayout);
		borderGroup.setText("Border");

		headerBorderAllCheck = new Button(borderGroup, SWT.CHECK);
		headerBorderAllCheck.setText("All");
		headerBorderAllCheck.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();
				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);

			}

		});

		headerBorderRightCheck = new Button(borderGroup, SWT.CHECK);
		headerBorderRightCheck.setText("Right");
		headerBorderRightCheck.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();

			}

		});

		headerBorderTopCheck = new Button(borderGroup, SWT.CHECK);
		headerBorderTopCheck.setText("Top");
		headerBorderTopCheck.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();

			}

		});

		headerBorderLeftCheck = new Button(borderGroup, SWT.CHECK);
		headerBorderLeftCheck.setText("Left");
		headerBorderLeftCheck.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();

			}

		});

		headerBorderBottomCheck = new Button(borderGroup, SWT.CHECK);
		headerBorderBottomCheck.setText(BOTTOM);
		headerBorderBottomCheck.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				headerStateChanged();

			}

		});
		new Label(composite, SWT.NONE);

		final Label globalStyleLabel = new Label(composite, SWT.NONE);
		globalStyleLabel.setText("Global Style:");

		headerGlobalStyle = new Combo(composite, SWT.READ_ONLY);
		final GridData gd_headerGlobalStyle = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_headerGlobalStyle.widthHint = 100;
		headerGlobalStyle.setLayoutData(gd_headerGlobalStyle);
		headerGlobalStyle.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				headerStateChanged();

			}

		});

		String[] headerGlobalStyles = createStringItemsArrayForCombo(globalStyleManager
				.getKeys());
		
		headerGlobalStyle.setItems(headerGlobalStyles);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		dataTabItem = new TabItem(tuplePropsFolder, SWT.NONE);
		dataTabItem.setText("Data");

		final Composite composite_1 = new Composite(tuplePropsFolder, SWT.NONE);
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.numColumns = 4;
		composite_1.setLayout(gridLayout_7);
		dataTabItem.setControl(composite_1);

		final Composite composite_7 = new Composite(composite_1, SWT.NONE);
		composite_7.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginWidth = 0;
		composite_7.setLayout(gridLayout_1);

		dataOverrideProtectionCheck = new Button(composite_7, SWT.CHECK);
		dataOverrideProtectionCheck.setText("Override Protection");
		dataOverrideProtectionCheck
				.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {

						dataStateChanged();

					}
				});

		final Label memberTagEditableLabel = new Label(composite_1, SWT.NONE);
		memberTagEditableLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		memberTagEditableLabel.setText("Member Tag Editable:");

		dataMemberTagEditableCombo = new Combo(composite_1, SWT.READ_ONLY);
		dataMemberTagEditableCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		dataMemberTagEditableCombo.setItems(new String[] { Constants.DROPDOWN_GLOBAL_OPTION, "True", "False" });
		dataMemberTagEditableCombo.select(0);
		
		dataMemberTagEditableCombo.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				dataStateChanged();

			}

		});

		final Group borderGroup_1 = new Group(composite_1, SWT.NONE);
		borderGroup_1.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 3, 1));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 5;
		borderGroup_1.setLayout(gridLayout_2);
		borderGroup_1.setText("Border");

		dataBorderAllCheck = new Button(borderGroup_1, SWT.CHECK);
		dataBorderAllCheck.setText("All");
		dataBorderAllCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				dataStateChanged();
				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);

			}
		});

		dataBorderRightCheck = new Button(borderGroup_1, SWT.CHECK);
		dataBorderRightCheck.setText("Right");
		dataBorderRightCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				dataStateChanged();

			}
		});

		dataBorderTopCheck = new Button(borderGroup_1, SWT.CHECK);
		dataBorderTopCheck.setText("Top");
		dataBorderTopCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				dataStateChanged();

			}
		});

		dataBorderLeftCheck = new Button(borderGroup_1, SWT.CHECK);
		dataBorderLeftCheck.setText("Left");
		dataBorderLeftCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				dataStateChanged();

			}
		});

		dataBorderBottomCheck = new Button(borderGroup_1, SWT.CHECK);
		dataBorderBottomCheck.setText(BOTTOM);
		dataBorderBottomCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				dataStateChanged();

			}
		});
		new Label(composite_1, SWT.NONE);

		final Label globalStyleLabel_1 = new Label(composite_1, SWT.NONE);
		globalStyleLabel_1.setText("Global Style:");

		dataGlobalStyle = new Combo(composite_1, SWT.READ_ONLY);
		dataGlobalStyle.setLayoutData(new GridData(100, SWT.DEFAULT));
		dataGlobalStyle.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				dataStateChanged();

			}

		});


		String[] dataGlobalStyles = createStringItemsArrayForCombo(globalStyleManager
				.getKeys());
		
		dataGlobalStyle.setItems(dataGlobalStyles);

		final Label globalStyleLabel_1_1 = new Label(composite_1, SWT.NONE);
		globalStyleLabel_1_1.setText("Override Numeric Format:");

		dataOverrideNumeric = new Combo(composite_1, SWT.READ_ONLY);
		dataOverrideNumeric.setLayoutData(new GridData(100, SWT.DEFAULT));
		dataOverrideNumeric.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				dataStateChanged();

			}

		});

		String[] numberFormats = createStringItemsArrayForCombo(numericFormatManager
				.getSimpleKeys());
		
		dataOverrideNumeric.setItems(numberFormats);
		composite_2 = new Composite(tuplePropertiesGroup,
				SWT.NONE);
		final GridLayout gridLayout_9 = new GridLayout();
		gridLayout_9.makeColumnsEqualWidth = true;
		gridLayout_9.marginWidth = 0;
		gridLayout_9.numColumns = 2;
		composite_2.setLayout(gridLayout_9);
		final GridData gd_composite_2 = new GridData(SWT.RIGHT, SWT.TOP, false, false);
		gd_composite_2.widthHint = 120;
		composite_2.setLayoutData(gd_composite_2);

		applyButton = new Button(composite_2, SWT.NONE);
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		applyButton.setEnabled(false);
		applyButton.setText("Apply");
		applyButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(APPLY_BUTTON_ID);
			}

		});

		cancelButton = new Button(composite_2, SWT.NONE);
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		cancelButton.setEnabled(false);
		cancelButton.setText("Cancel");
		cancelButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(CANCEL_BUTTON_ID);
			}

		});

		populatePageTupleTree();
	
		String[] levelNumbers = new String[NUMBER_OF_LEVELS+1];
		
		for (int i = 0; i < levelNumbers.length; i++) {
			levelNumbers[i] = new Integer(i).toString();
		}

		initializeForm();
		bootingUp = false;

		addLevelGenerationControlListeners();
		
		creatingDialogArea = false;
		
		//
		return container;

	}

	protected void doubleClickLeftTreeAction(Tree tree) {

		if ( rightButton.isEnabled() ) {
			
			int index = leftTabFolder.getSelectionIndex();

			copyTreeItems(index);

			updateButtonStatus(getTabFolderTree(), upButton, UP_BUTTON_ID);
			updateButtonStatus(getTabFolderTree(), downButton,
					DOWN_BUTTON_ID);

			
		} else {

			TreeUtil.doubleClickSelectionAction(tree);
			
		}
		
	}

	protected void pageDimensionTreeItemSelected(Tree tree, Tree tupleTree) {

		updateRightButtonStatusForPageTuples(tree, tupleTree);
		updateLeftButtonStatus(tupleTree);
		
	}

	private void dimensionTreeItemSelected(Tree tree, Tree tupleTree) {

		updateMemberSelection = false;

		updateRightButtonStatusForOtherTuples(tree, tupleTree);
		initializeForm();

		/*
		 * selection is pafblank, set memeber properties to false and selection
		 * to false, otherwise, use the status of right button.
		 */
		
		TreeItem selectedItem = tree.getSelection()[0];
		
		if (isPafBlank(selectedItem) || isMemberTagNode(selectedItem)) {
			setMemberPropertiesEnabled(false);
			selectionButton.setSelection(false);
		} else if (selectedItem.getText().contains(
				PafBaseConstants.USER_SEL_TAG)
				&& ((PafUserSelection) selectedItem.getData())
						.isMultiples()) {
			setSelectionGroupEnabledSelectionOnly(rightButton.getEnabled());
			selectionButton.setSelection(rightButton.getEnabled());
		} else {
			setMemberPropertiesEnabled(rightButton.getEnabled());
			selectionButton.setSelection(rightButton.getEnabled());
			
			String dimensionName = TreeUtil.getParentOfTree(selectedItem).getText();
			String memberName = selectedItem.getText();
			
			if ( isMemberInOutline(dimensionName, memberName)) {
				
				int memberLevel = getMemberLevel(dimensionName, memberName);
				
				if ( memberLevel == 0 ) {
					
					disableAllChildrenDescLevelAndGenSelectionsControls();
					
				}
				
			}
		}

	}

	private String[] createStringItemsArrayForCombo(String[] items) {

		ArrayList<String> itemList = new ArrayList<String>();
		itemList.add("");

		if (items != null) {

			for (String item : items) {
				itemList.add(item);
			}

		}

		return itemList.toArray(new String[0]);

	}

	private void createTreeModel(Tree tree, String[] dimNames) {

		logger.debug("Create Tree Model start");

		for (String dimName : dimNames) {
			logger.debug("Getting Dim: " + dimName + " tree start");

			PafSimpleDimTree pafSimpleDimTree = null;
			
			try {
				pafSimpleDimTree = DimensionTreeUtility.getDimensionTree(project, 
						PafProjectUtil.getApplicationName(project), dimName);
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage());
			}
					
			
			logger.debug("Getting Dim: " + dimName + " tree end");

			logger.debug("Converting TreeInto hash start");

			HashMap<String, PafSimpleDimMember> treeHashMap = null;
		
			treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(pafSimpleDimTree);
			
			logger.debug("Converting TreeInto hash end");

			PafSimpleDimMember rootMember = null;
			
			if ( dimName.equals(pafSimpleDimTree.getRootKey())) {
			
				rootMember = (PafSimpleDimMember) treeHashMap.get(dimName);
				
			} else {
				
				PafSimpleDimMember oldRootMember = (PafSimpleDimMember) treeHashMap.get(pafSimpleDimTree.getRootKey());

				rootMember = new PafSimpleDimMember();
				rootMember.setKey(dimName);
				rootMember.getChildKeys().addAll(Arrays.asList(new String[] { pafSimpleDimTree.getRootKey() } ));
				
				PafSimpleDimMemberProps simpleDimMemberProps = new PafSimpleDimMemberProps();
				
				simpleDimMemberProps.setLevelNumber(oldRootMember.getPafSimpleDimMemberProps().getLevelNumber() + 1);
				simpleDimMemberProps.setGenerationNumber(oldRootMember.getPafSimpleDimMemberProps().getGenerationNumber() - 1);
								
				rootMember.setPafSimpleDimMemberProps(simpleDimMemberProps);
				
				treeHashMap.put(dimName, rootMember);
								
			}
					
			logger.info("MAX LEVEL: " + rootMember.getPafSimpleDimMemberProps().getLevelNumber());

			maxTreeDepthMap.put(dimName, rootMember.getPafSimpleDimMemberProps().getLevelNumber());
			
			logger.debug("Building Tree start");

			addTreeNode(treeHashMap, tree, null, rootMember, dimName);

			cachedTreeMap.put(dimName, tree);

			logger.debug("Building Tree end");
			
		}

		//if row or column tree
		if ( tree.equals(rowTree) || tree.equals(columnTree) ) {
								
			//add member tag items to member tag node
			
			//get all member tags for current set of dimensions
			MemberTagDef[] memberTagDefs = memberTagModelManager.getMemberTags(currentViewSection.getDimensionSet().toArray(new String[0]), true);
			
			if ( memberTagDefs != null ) {
		
				//add member tag node to end of tree
				TreeItem memberTagRoot = null;			
				
				PafAxis currentAxis = null;
				
				//get current axis for row or column
				if ( tree.equals(rowTree)) {
					
					currentAxis = new PafAxis(PafAxis.ROW);
					
				} else if ( tree.equals(columnTree)) {
					
					currentAxis = new PafAxis(PafAxis.COL);
				} 
				
				//go over member tags and find out which ones are valid for the current axis
				for (MemberTagDef memberTagDef : memberTagDefs ) {
					
					//if valid, create a new member tag tree item node
					if ( MemberTagUtil.isMemberTagValidForAxis(currentViewSection, memberTagDef, currentAxis)) {
						
						//if member tag root node is null, create new one (only happens once)
						if ( memberTagRoot == null ) {
							
							//create parent
							memberTagRoot = new TreeItem(tree, SWT.NONE);
							
							memberTagRoot.setText(Constants.MEMBER_TAGS_NODE_NAME);
							memberTagRoot.setData(DISPALY_ONLY_KEY, new Boolean(true));	
						}
						
						
						//create member tag tree item
						TreeItem memberTagTreeItem = new TreeItem(memberTagRoot, SWT.NONE);
						memberTagTreeItem.setText(memberTagDef.getName());
						memberTagTreeItem.setData(MEMBER_TAG_KEY, memberTagDef.getName());
						
					}
					
					if ( memberTagRoot != null ) {
						
						memberTagRoot.setExpanded(true);
						
					}
					
				}
				
			}
		
		}
		logger.debug("Create Tree Model end");
	}

	/**
	 * 
	 * Tests if a tree item is a member tag node
	 *
	 * @param treeItem
	 * @return
	 */
	private boolean isMemberTagNode(TreeItem treeItem) {
		
		boolean isMemberTagNode = false;
		
		if ( treeItem != null ) {
			
			if ( treeItem.getData(MEMBER_TAG_KEY) != null ) {
				
				isMemberTagNode = true;
				
			}
			
		}
		
		return isMemberTagNode;
		
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	protected Point getInitialSize() {
		return new Point(780, 596);
	}

	/**
	 * 
	 *  Copy tree items from the "From" tree to the "To" tree by index.
	 *
	 * @param index
	 */
	private void copyTreeItems(int index) {

		Tree fromTree = null;
		Tree toTree = null;

		if (index == PAGE_INDEX) {

			fromTree = pageTree;
			toTree = pageTupleTree;

		} else if (index == COL_INDEX) {

			fromTree = columnTree;
			toTree = columnTupleTree;

		} else if (index == ROW_INDEX) {

			fromTree = rowTree;
			toTree = rowTupleTree;

		}

		TreeItem[] fromSelectedItems = fromTree.getSelection();
		TreeItem fromSelectedItem = null;

		if (fromSelectedItems.length > 0) {
			fromSelectedItem = fromSelectedItems[0];
		}

		TreeItem newTreeItem = null;

		TreeItem[] toSelectedItems = toTree.getSelection();
		TreeItem toSelectedItem = null;

		if (toSelectedItems.length > 0) {
			toSelectedItem = toSelectedItems[0];
		}

		if (fromSelectedItem != null) {

			// tree index of selected item
			int fromTreeIndex = findTreeIndex(fromSelectedItem);

			logger.debug("Level: " + TreeUtil.getTreeItemLevel(fromSelectedItem));

			//if from item is member tag
			if ( isMemberTagNode(fromSelectedItem) ) {				
				
				if ( toSelectedItem == null ) {
				
					newTreeItem = new TreeItem(toTree, SWT.NONE);
					
				} else {
					
					TreeItem dim1Parent = TreeUtil.getParentOfTree(toSelectedItem);
					
					int itemNdx = toTree.indexOf(dim1Parent);
					
					newTreeItem = new TreeItem(toTree, SWT.NONE, itemNdx+1);
					
				}
				
				
				newTreeItem.setText(fromSelectedItem.getText());
				
				String memberTagDefName = (String) fromSelectedItem.getData(MEMBER_TAG_KEY);
				
				if ( memberTagDefName != null ) {
					
					newTreeItem.setData(MEMBER_TAG_KEY, memberTagDefName);
					
				}
				
				
			// if selected item is null, add to end of tree
			} else if (toSelectedItem == null ) {

				newTreeItem = new TreeItem(toTree, SWT.NONE);
				newTreeItem.setText(fromSelectedItem.getText());
			
			} else {

				// get the item level
				int toLevel = TreeUtil.getTreeItemLevel(toSelectedItem);

				// if same dimension, add to parent node.
				if (fromTreeIndex == toLevel) {

					MemberSelection memberSelection = getMemberSelectionFromTreeItem(toSelectedItem
							.getData());

					// if member is a members, add to list of members.
					if (memberSelection != null
							&& memberSelection.getSelectionType().equals(
									MemberSelectionType.Members)) {

						String newMemberName = fromSelectedItem.getText();

						memberSelection.addMember(newMemberName);

						toSelectedItem.setText(memberSelection
								.getMemberWithSelection());

						selectMemberSelection(memberSelection
								.getSelectionType());

					} else {
							
						int itemNdx = 0;
						
						if (toSelectedItem.getParentItem() == null) {
										
							itemNdx = toTree.indexOf(toSelectedItem);
						
							//add after to selected item
							newTreeItem = new TreeItem(toTree, SWT.NONE, itemNdx+1);
							
						} else {
							
							itemNdx = toSelectedItem.getParentItem().indexOf(toSelectedItem);

							//add after to selected item
							newTreeItem = new TreeItem(toSelectedItem
									.getParentItem(), SWT.NONE, itemNdx+1);
						}					
						
						newTreeItem.setText(fromSelectedItem.getText());
					}

				} else if (fromTreeIndex == toLevel + 1) {

					if ( toSelectedItem.getItemCount() == 0 ) {
						
						newTreeItem = new TreeItem(toSelectedItem, SWT.NONE);
						
					} else {
						
						newTreeItem = new TreeItem(toSelectedItem, SWT.NONE, 0);
						
					}
					
					newTreeItem.setText(fromSelectedItem.getText());
					
				} else if (fromTreeIndex == 0) {					
					
					int itemNdx = toTree.indexOf(toSelectedItem);
					
					if ( itemNdx >= 0 ) {
					
						newTreeItem = new TreeItem(toTree, SWT.NONE, itemNdx + 1);
						
					} else {
						
						newTreeItem = new TreeItem(toTree, SWT.NONE);
						
					}
										
					newTreeItem.setText(fromSelectedItem.getText());
				}

				toSelectedItem.setExpanded(true);

			}

		}

		//TTN-889
		//fromTree.deselectAll();

		rightButton.setEnabled(false);

		if (newTreeItem != null) {

			if (fromSelectedItem != null) {

				// check for user selection on from selected item.
				if (isUserSelectionMember(fromSelectedItem.getText())) {

					PafUserSelection pafUserSelection = (PafUserSelection) fromSelectedItem
							.getData();
					newTreeItem.setData(USER_SELECTION_MULTIPLE,
							pafUserSelection.isMultiples());

				}
			}

			logger.debug("Selected new node: " + newTreeItem);

			toTree.setSelection(new TreeItem[] { newTreeItem });

			updateTupleProperties(toTree, index);

			if (index == ROW_INDEX || index == COL_INDEX) {

				String fromMemberName = fromSelectedItem.getText();
				String fromMemberDimensionName =  TreeUtil.getParentOfTree(fromSelectedItem).getText();
				
				boolean isMemberInOutline = isMemberInOutline(fromMemberDimensionName, fromMemberName);
				
				int itemLevel = TreeUtil.getTreeItemLevel(newTreeItem);
				int tabIndex = rightTabFolder.getSelectionIndex();
				int numberOfDims = 0;

				if (tabIndex == ROW_INDEX) {
					numberOfDims = rowDims.length;
				} else if (tabIndex == COL_INDEX) {
					numberOfDims = colDims.length;
				}

				// if last level configure as PafTuple, otherwise as PafMember
				if (itemLevel == numberOfDims - 1) {

					PafTuple tuple = new PafTuple();

					TreeItem[] treeItems = getArrayFromParentToItem(newTreeItem);

					ArrayList<PafMember> listOfMembers = new ArrayList<PafMember>();

					for (TreeItem treeItem : treeItems) {

						PafMember member = (PafMember) treeItem.getData();

						if (member == null) {

							member = new PafMember();
							member.setName(treeItem.getText().trim());

							MemberSelectionType selectionType = null;

							if (member.getName().equals(
									PafBaseConstants.PAF_BLANK)) {
								selectionType = MemberSelectionType.Selection;
							
							} else if ( isMemberTagNode(treeItem ) ) {
								
								selectionType = MemberSelectionType.Selection;								
								
							} else {
								
								selectionType = getSelectionTypeFromMemberProperties();

								if ( isMemberInOutline ) {
									
									int level = getMemberLevel(fromMemberDimensionName, fromMemberName);
									
									if ( level == 0 ) {
										
										disableAllChildrenDescLevelAndGenSelectionsControls();
										
										if ( MemberSelection.isKindOfChildrenDescLevelOrGenSelectionType(selectionType)) {
											
											selectionType = MemberSelectionType.Selection;
											
										}
										
									}
									
								}								
								
							}
							
							//if user selection has multiples set, default selection type to Selection
							if ( newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE) != null ) {
								
								if ( newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE) instanceof Boolean ) {
								
									Boolean multiples = (Boolean) newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE);
									
									if ( multiples ) {
										
										selectionType = MemberSelectionType.Selection;
										
									}
									
								}
								 
								
							}

							MemberSelection memberSelection = new MemberSelection(
									new String[] { member.getName() },
									selectionType);
							member.setSelectionType(memberSelection);

							treeItem.setText(memberSelection
									.getMemberWithSelection());

						}

						listOfMembers.add(member);

					}

					tuple.setMemberAr(listOfMembers.toArray(new PafMember[0]));

					tuple.setHeader(new PafMemberHeader());
					tuple.setData(new PafMemberData());
					tuple.setMemberTag(isMemberTagNode(newTreeItem));
					
					newTreeItem.setData(tuple);

					populateForm(toTree.getSelection());

					// else create member item
				} else {

					PafMember member = new PafMember();

					member.setName(newTreeItem.getText());

					MemberSelectionType selectionType = null;

					if (member.getName().equals(PafBaseConstants.PAF_BLANK)) {
						
						selectionType = MemberSelectionType.Selection;

						TreeItem tmpTreeItem = newTreeItem;

						member.setSelectionType(new MemberSelection(
								new String[] { member.getName() },
								selectionType));

						// add member to tree item data
						newTreeItem.setData(member);

						ArrayList<PafMember> newPafMemberList = getMembersFromNodeToTreeItem(newTreeItem
								.getParentItem());

						// add to end of list.
						newPafMemberList.add(member);

						for (int i = itemLevel + 1; i <= numberOfDims - 1; i++) {

							TreeItem tmpPafBlankTreeItem = new TreeItem(
									tmpTreeItem, SWT.NONE);
							tmpPafBlankTreeItem
									.setText(PafBaseConstants.PAF_BLANK);
							tmpTreeItem.setExpanded(true);

							newPafMemberList.add(member);

							if (i == numberOfDims - 1) {

								PafTuple pafBlankTuple = new PafTuple();
								pafBlankTuple.setData(new PafMemberData());
								pafBlankTuple.setHeader(new PafMemberHeader());
								pafBlankTuple.setMemberAr(newPafMemberList
										.toArray(new PafMember[0]));

								tmpPafBlankTreeItem.setData(pafBlankTuple);

								toTree
										.setSelection(new TreeItem[] { tmpPafBlankTreeItem });
								updateTupleProperties(toTree, index);

							} else {

								tmpPafBlankTreeItem.setData(member);

							}

							tmpTreeItem = tmpPafBlankTreeItem;

						}

					} else if ( isMemberTagNode(newTreeItem) ) {						
						
						selectionType = MemberSelectionType.Selection;

						TreeItem tmpTreeItem = newTreeItem;

						member.setSelectionType(new MemberSelection(
								new String[] { member.getName() },
								selectionType));

						// add member to tree item data
						newTreeItem.setData(member);

						List<PafMember> newPafMemberList = new ArrayList<PafMember>();
						
						// add 1st meember
						newPafMemberList.add(member);

						// get member tag def from data
						String memberTagDefName = (String) newTreeItem.getData(MEMBER_TAG_KEY);
												
						for (int i = 1; i <= numberOfDims - 1; i++) {

							TreeItem tmpMemberDefTreeItem = new TreeItem(
									tmpTreeItem, SWT.NONE);
							tmpMemberDefTreeItem
									.setText(newTreeItem.getText());
							
							tmpTreeItem.setExpanded(true);

							tmpMemberDefTreeItem.setData(MEMBER_TAG_KEY, memberTagDefName);
							
							if (i == numberOfDims - 1) {

								newPafMemberList.add(member);
								
								PafTuple pafMemberDefTuple = new PafTuple();
								pafMemberDefTuple.setData(new PafMemberData());
								pafMemberDefTuple.setHeader(new PafMemberHeader());
								pafMemberDefTuple.setMemberAr(newPafMemberList
										.toArray(new PafMember[0]));
								//set as member Tag
								pafMemberDefTuple.setMemberTag(true);
								
								tmpMemberDefTreeItem.setData(pafMemberDefTuple);

								toTree
										.setSelection(new TreeItem[] { tmpMemberDefTreeItem });
								updateTupleProperties(toTree, index);

							} else {

								newPafMemberList.add(member);
								
								tmpMemberDefTreeItem.setData(member);

							}

							tmpTreeItem = tmpMemberDefTreeItem;

						}
						
					} else {
						
						selectionType = getSelectionTypeFromMemberProperties();
						
						if ( isMemberInOutline ) {
							
							int level = getMemberLevel(fromMemberDimensionName, fromMemberName);
							
							if ( level == 0 ) {
								
								disableAllChildrenDescLevelAndGenSelectionsControls();
								
								if ( MemberSelection.isKindOfChildrenDescLevelOrGenSelectionType(selectionType)) {
									
									selectionType = MemberSelectionType.Selection;
									
								}
								
							}
							
						}						
						
					}
					
					//if user selection has multiples set, default selection type to Selection
					if ( newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE) != null ) {
						
						if ( newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE) instanceof Boolean ) {
						
							Boolean multiples = (Boolean) newTreeItem.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE);
							
							if ( multiples ) {
								
								selectionType = MemberSelectionType.Selection;
								
							}
							
						}
						 
						
					}

					MemberSelection memberSelection = new MemberSelection(
							new String[] { member.getName() }, selectionType);
					member.setSelectionType(memberSelection);

					newTreeItem.setText(memberSelection
							.getMemberWithSelection());
					newTreeItem.setData(member);

				}
			}

		}

		applyButton.setEnabled(false);
		cancelButton.setEnabled(false);
		updateLeftButtonStatus(toTree);

		if (index != PAGE_INDEX) {
			updateSelectionGroup(toTree.getSelection());
		}

		updateMemberSelection = true;

	}

	private ArrayList<PafMember> getMembersFromNodeToTreeItem(
			TreeItem tmpPafBlankTreeItem) {

		ArrayList<PafMember> nodeNameList = new ArrayList<PafMember>();

		populateListFromRootToItem(nodeNameList, tmpPafBlankTreeItem);

		return nodeNameList;
	}

	private void populateListFromRootToItem(ArrayList<PafMember> nodeNameList,
			TreeItem treeItem) {

		if (treeItem != null) {

			populateListFromRootToItem(nodeNameList, treeItem.getParentItem());

			Object dataItem = treeItem.getData();

			if (dataItem instanceof PafMember) {

				nodeNameList.add((PafMember) dataItem);

			} else {
				logger.error("DataItem was not an instance of PafMember: "
						+ dataItem);
			}

		}

	}

	private MemberSelection getMemberSelectionFromTreeItem(Object dataItem) {

		MemberSelection memberSelection = null;

		if (dataItem != null) {

			PafMember pafMember = getPafMember(dataItem);

			if (pafMember != null) {
				memberSelection = pafMember.getSelectionType();
			}

		}

		return memberSelection;
	}

	private PafMember getPafMember(Object treeDataItem) {

		PafMember pafMember = null;

		if (treeDataItem instanceof PafMember) {

			pafMember = (PafMember) treeDataItem;

		} else if (treeDataItem instanceof PafTuple) {

			PafTuple pafTuple = (PafTuple) treeDataItem;

			PafMember[] pafMembers = pafTuple.getMemberAr();

			pafMember = pafMembers[pafMembers.length - 1];

		}

		return pafMember;
	}

	private void updateTabs(int index) {

		if (leftTabFolder != null && rightTabFolder != null) {
			leftTabFolder.setSelection(index);
			rightTabFolder.setSelection(index);
		}

	}

	private void updateRightButtonStatusForPageTuples(Tree allTree,
			Tree builtTree) {

		TreeItem[] allTreeSelectedItems = allTree.getSelection();
		TreeItem selectedItem = null;

		if (allTreeSelectedItems.length > 0) {
			selectedItem = allTreeSelectedItems[0];
		}

		if (selectedItem != null) {

			TreeItem root = TreeUtil.getParentOfTree(selectedItem);

			int treeIndex = findTreeIndex(root);
			
			if (selectedItem.getParentItem() == null &&
					rootDimsNotAvailForSelection.contains(root.getText())) {
					//added to fix TTN-664
					//allows root nodes (ie "Product" to be selected)
					//does not allow the second root node to be selected, until
					//a selection from the first one has been made.
					rightButton.setEnabled(false);
			} else if (treeIndex == 0) {

				int numOfChildren = builtTree.getItemCount();

				if (numOfChildren == 0) {
					rightButton.setEnabled(true);
				} else {
					rightButton.setEnabled(false);
				}

			} else {

				TreeItem[] selectedItems = builtTree.getSelection();

				if (selectedItems.length > 0) {

					int toLevel = TreeUtil.getTreeItemLevel(selectedItems[0]);

					int numOfChildren = selectedItems[0].getItemCount();

					if (numOfChildren == 0) {

						if (treeIndex == (toLevel + 1)) {

							
							Boolean isDisplayOnly = (Boolean) selectedItem.getData(DISPALY_ONLY_KEY);
							
							if ( isDisplayOnly == null ) {
							
								rightButton.setEnabled(true);
								
							} else {
								
								rightButton.setEnabled(! isDisplayOnly);
								
							}							
							

						} else {

							rightButton.setEnabled(false);

						}

					} else {

						rightButton.setEnabled(false);

					}

				} else {

					rightButton.setEnabled(false);

				}

			}
		} else {
			rightButton.setEnabled(false);
		}

	}

/*	private void updateLeftButtonStatusForRowColumnTuples(Tree tupleTree) {

		boolean isEnabled = true;
		
		TreeItem[] tupleSelectedTreeItems = tupleTree.getSelection();

		//loop all selected items, if any are member tag nodes, ensure they are top level
		for (TreeItem tupleTreeItem : tupleSelectedTreeItems ) {
								
			if ( isMemberTagNode(tupleTreeItem) ) {
		
				int memberTagNodeLevel = TreeUtil.getTreeItemLevel(tupleTreeItem);
				
				if ( memberTagNodeLevel != 0 ) {
				
					isEnabled = false;
					break;
					
				}				
				
			}			
			
		}
		
		leftButton.setSelection(isEnabled);


	}
*/	
	private void updateRightButtonStatusForOtherTuples(Tree allTree,
			Tree builtTree) {

		TreeItem[] fromSelectedItems = allTree.getSelection();
		TreeItem fromSelectedItem = null;

		TreeItem[] toSelectedItems = builtTree.getSelection();
		TreeItem toSelectedItem = null;

		if (toSelectedItems.length > 0) {
			toSelectedItem = toSelectedItems[0];
		}

		if (fromSelectedItems.length > 0) {

			fromSelectedItem = fromSelectedItems[0];
		}

		boolean enable = false;

		if (fromSelectedItem != null) {

			TreeItem root = TreeUtil.getParentOfTree(fromSelectedItem);

			int treeIndex = findTreeIndex(root);
			
			
			if ( isMemberTagNode(fromSelectedItem) ) {
				
				enable = true;
				
			} else 	if (fromSelectedItem.getParentItem() == null &&
					rootDimsNotAvailForSelection.contains(root.getText())) {
					//added to fix TTN-664
					//allows root nodes (ie "Product" to be selected)
					//does not allow the second root node to be selected, until
					//a selection from the first one has been made.
					enable = false;
			} else if (treeIndex == 0) {

				enable = true;

			} else {

				if (toSelectedItem != null && toSelectedItem.getItems() != null) {

					if (toSelectedItems.length > 0) {

						int toLevel = TreeUtil.getTreeItemLevel(toSelectedItem);

						// if to level member is at same level or previous,
						// enable
						if (treeIndex == toLevel) {

							if (isPafBlank(toSelectedItem.getParentItem()) && isPafBlank(toSelectedItem) && ! isPafBlank(fromSelectedItem)) {
								enable = false;
								
							} else {

								Boolean isDisplayOnly = (Boolean) fromSelectedItem.getData(DISPALY_ONLY_KEY);
								
								//check if to itme is a  member tag item, if so disable
								String memberTagName = (String) toSelectedItem.getData(MEMBER_TAG_KEY);
								
								if ( isDisplayOnly == null && memberTagName == null) {
								
									enable = true;
									
								} else {
									
									if ( isDisplayOnly != null ) {
									
										enable = ! isDisplayOnly;
										
									} else if ( memberTagName != null ) {
										
										enable = false;
										
									} else {
										
										enable = true;
										
									}
									
									
								}							
								
							}

						} else if (treeIndex == (toLevel + 1)) {

							if (isPafBlank(toSelectedItem)) {

								if (isPafBlank(fromSelectedItem)) {
									enable = true;
								} else {
									enable = false;
								}

							} else {
								
								Boolean isDisplayOnly = (Boolean) fromSelectedItem.getData(DISPALY_ONLY_KEY);
								
								//check if to itme is a  member tag item, if so disable
								String memberTagName = (String) toSelectedItem.getData(MEMBER_TAG_KEY);
								
								if ( isDisplayOnly == null && memberTagName == null) {
								
									enable = true;
									
								} else {
									
									if ( isDisplayOnly != null ) {
									
										enable = ! isDisplayOnly;
										
									} else if ( memberTagName != null ) {
										
										enable = false;
										
									} else {
										
										enable = true;
										
									}
									
									
								}							
								
							}

						} else {

							enable = false;

						}

					} else {

						enable = false;

					}

				} else {

					enable = false;

				}

			}

		} else {
			enable = false;
		}

		rightButton.setEnabled(enable);

	}

	private int findTreeIndex(TreeItem item) {

		int index = 0;

		TreeItem root = TreeUtil.getParentOfTree(item);

		int tabIndex = rightTabFolder.getSelectionIndex();

		String[] dims = null;

		if (tabIndex == PAGE_INDEX) {
			dims = pageDims;
		} else if (tabIndex == COL_INDEX) {
			dims = colDims;
		} else if (tabIndex == ROW_INDEX) {
			dims = rowDims;
		}

		for (String dim : dims) {
			if (dim.equals(root.getText())) {
				break;
			}
			index++;
		}

		return index;
	}

	private void updateLeftButtonStatus(Tree tree) {

		if (tree != null) {
			
			TreeItem[] selectedTreeItems = tree.getSelection();

			if (selectedTreeItems.length > 0) {				
							
				//loop all selected items, if any are member tag nodes, ensure they are top level
				for (TreeItem selectedTreeItem : selectedTreeItems ) {
										
					if ( isMemberTagNode(selectedTreeItem) ) {
				
						int memberTagNodeLevel = TreeUtil.getTreeItemLevel(selectedTreeItem);
						
						if ( memberTagNodeLevel != 0 ) {
						
							leftButton.setEnabled(false);
							return;
							
						}				
						
					}			
					
				}
				
				leftButton.setEnabled(true);
				
			} else {
				leftButton.setEnabled(false);
			}
		}
	}

	private void updateTupleProperties(Tree tree, int tabIndex) {

		int numberOfDims = 0;

		//if row or column, get number of dims, otherwise, return
		if (tabIndex == ROW_INDEX) {
			numberOfDims = rowDims.length;
		} else if (tabIndex == COL_INDEX) {
			numberOfDims = colDims.length;
		} else {
			return;
		}

		//get selected items
		TreeItem[] selectedItems = tree.getSelection();
		
		TreeItem selectedItem = null;

		//true if every item selected is a paf tuple
		boolean everySelectedItemLastLevel = true;

		boolean everySelectedItemMemberTag = true;
		
		//if the length of array is great than 0
		if (selectedItems.length > 0) {

			//loop through all items and see if any are not least level
			for (TreeItem item : selectedItems) {

				//get tree level of selected item
				int treeLevelOfSelection = TreeUtil.getTreeItemLevel(item);

				//if not = to the number of dims, set to false
				if (treeLevelOfSelection != (numberOfDims - 1)) {
					everySelectedItemLastLevel = false;
				}
				
				if ( ! isMemberTagNode( item ) ) {
					everySelectedItemMemberTag = false;
				}

			}

			//set selected item to first item set
			selectedItem = selectedItems[0];
			
		} 
		
		// if only one item selected, disable otherwise enable so one could use
		// existing properties to set multiple tuples.
		if (selectedItems.length == 0) {

			applyButton.setEnabled(false);
			cancelButton.setEnabled(false);
			everySelectedItemLastLevel = false;

		} else if (selectedItems.length == 1) {

			applyButton.setEnabled(false);
			cancelButton.setEnabled(false);

			Object dataMember = selectedItem.getData();

			if (dataMember instanceof PafTuple) {
				
				PafTuple tuple = (PafTuple) dataMember;

				setLastTupleUsed(tuple);
				
			}

		} else {

			applyButton.setEnabled(true);
			cancelButton.setEnabled(true);

			if (lastUsedPafTuple != null) {

				if (!dirtyData) {
							
					currentHeaderState = lastUsedPafTuple.getHeader();
					currentDataState = lastUsedPafTuple.getData();
					
					lastUsedPafTuple = null;
				}
			}

		}

		if (tabIndex == PAGE_INDEX) {

			setBlankContentEnabled(false);
			setHeaderContentEnabled(false);
			setDataContentEnabled(false);
			setSelectionGroupEnabled(false);

			cancelButton.setEnabled(false);
			applyButton.setEnabled(false);

		} else if (everySelectedItemLastLevel) {

			setBlankContentEnabled(false);
			
			//1st enable header content, 2nd enable on special cases
			setHeaderContentEnabled(true);

			int index = leftTabFolder.getSelectionIndex();

			if (index == COL_INDEX) {

				headerRowHeight.setEnabled(false);
				headerColumnWidth.setEnabled(true);

			} else if (index == ROW_INDEX) {
				headerRowHeight.setEnabled(true);
				headerColumnWidth.setEnabled(false);
			}

			//1st enable data content, 2nd enable member tag combo			
			setDataContentEnabled(true);
			
			dataMemberTagEditableCombo.setEnabled(everySelectedItemMemberTag);			

			setSelectionGroupEnabled(true);
			performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
					headerBorderRightCheck, headerBorderTopCheck,
					headerBorderBottomCheck);
			performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
					dataBorderRightCheck, dataBorderTopCheck,
					dataBorderBottomCheck);
			
		} else {

			setBlankContentEnabled(false);
			setHeaderContentEnabled(false);
			setDataContentEnabled(false);
			setSelectionGroupEnabled(true);

			applyButton.setEnabled(false);
			cancelButton.setEnabled(false);
			

		}

	}

	private void setLastTupleUsed(PafTuple tupleToClone) {
		
		try {
			lastUsedPafTuple = (PafTuple) tupleToClone.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("Clone from tuple did not work: "
					+ e.getMessage());
			e.printStackTrace();
		}
		
	}

	private void addTreeNode(HashMap<String, PafSimpleDimMember> treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		if (member.getChildKeys().size() > 0 ) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap.get(child);

				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}
		addItemToTree(tree, newItem, dimension);
		
		
		Map<String, PafSimpleDimMember> pafSimpleDimMemberSet = pafSimpleDimMemberDimensionMap.get(dimension);
		
		if ( pafSimpleDimMemberSet == null ) {
			pafSimpleDimMemberSet = new HashMap<String, PafSimpleDimMember>();
		}
		
		pafSimpleDimMemberSet.put(member.getKey(), member);
		
		pafSimpleDimMemberDimensionMap.put(dimension, pafSimpleDimMemberSet);
		
		logger.debug("Adding to pafSimpleDimMemberSet: " + member.getKey() + " : " + member.toString());
		
	}
	
	private void addItemToTree(Tree tree, TreeItem newItem, String dimension){
		// add to end of tree and not to page tree
		if (newItem.getParentItem() == null) {

			if(attributeDimensions.contains(dimension)){
				//for each additional item to append, append
				for (String additionalItem : additionalAttributeDimItems) {
					
					//if this is UOW_ROOT, skip
					if (additionalItem.equals(PafBaseConstants.UOW_ROOT)) {
						continue;
					//if page dim and paf blank, skip (TTN-775)
					} else if ( isPageDim(dimension) && additionalItem.equals(PafBaseConstants.PAF_BLANK)) {
						continue;
					}
	
					TreeItem additionalTreeItem = new TreeItem(newItem,
							SWT.NONE);
					additionalTreeItem.setText(additionalItem);
	
				}
			}else {
				//for each additional item to append, append
				for (String additionalItem : additionalDimItems) {
					
					//if this is a version dimension and the additional item is UOW_ROOT, skip
					if (newItem.getText().equals(versionName)
							&& additionalItem.equals(PafBaseConstants.UOW_ROOT)) {
						continue;
					} else if ( isPageDim(dimension) && additionalItem.equals(PafBaseConstants.PAF_BLANK)) {
						continue;
					}
	
					TreeItem additionalTreeItem = new TreeItem(newItem,
							SWT.NONE);
					additionalTreeItem.setText(additionalItem);
	
				}
			}

			//if the user selections map is null, get from manager
			if (userSelectionsMap == null) {

				userSelectionsMap = userSelectionManager.getDimensionMap();

			}

			//if map contains dimension key
			if (userSelectionsMap.containsKey(dimension)) {

				
				//get all user selections
				PafUserSelection[] userSelections = userSelectionsMap
						.get(dimension);

				//if user selections are not null
				if (userSelections != null) {

					//for each user selection
					for (PafUserSelection userSelection : userSelections) {
										
						//if a page dimension and user selecitons is a multiples, don't add TTN-353
						if ( isPageDim(dimension) && userSelection.isMultiples()) {
							continue;
						}
						
						//create tree item and set text and data
						TreeItem userSelectionTreeItem = new TreeItem(newItem,
								SWT.NONE);
						userSelectionTreeItem
								.setText(PafBaseConstants.USER_SEL_TAG + "("
										+ userSelection.getId() + ")");
						userSelectionTreeItem.setData(userSelection);

					}

				}

			}

		}

		
		// if version dim, add additional members. eventually make this more
		// dynamic
		if (newItem.getText().equals(versionName)) {

			TreeItem dynamicMembersFolderItem = new TreeItem(newItem, SWT.NONE);
			dynamicMembersFolderItem.setText(Constants.DYNAMIC_MEMBERS_NODE_NAME);
			dynamicMembersFolderItem.setData(DISPALY_ONLY_KEY, new Boolean(true));
			
			DynamicMemberDef versionDynamicMemberDef = (DynamicMemberDef) dynamicMembersManager.getItem(versionName);
			
			if ( versionDynamicMemberDef != null ) {
				
				String[] memberSpecs = versionDynamicMemberDef.getMemberSpecs();
				
				for (String memberSpec : memberSpecs ) {
					
					TreeItem additionalTreeItem = new TreeItem(dynamicMembersFolderItem, SWT.NONE);
					additionalTreeItem.setText(memberSpec);
					
				}
			
				dynamicMembersFolderItem.setExpanded(true);
			}
								
		}
	}
	
	
	
	//checks to see if dim is on page axis
	private boolean isPageDim(String dimension) {

		//if page dims exist
		if (pageDims != null) {

			//loop over page dimes
			for (String dim : pageDims) {

				//if dim is found on page axis, return true
				if (dim.equals(dimension)) {
					return true;
				}

			}

		}

		return false;

	}

	private int getTupleTreeIndex(Tree tree) {

		int treeIndex = 0;

		if (tree.equals(pageTupleTree)) {
			treeIndex = PAGE_INDEX;
		} else if (tree.equals(rowTupleTree)) {
			treeIndex = ROW_INDEX;
		} else if (tree.equals(columnTupleTree)) {
			treeIndex = COL_INDEX;
		} else {
			treeIndex = -1;
		}

		return treeIndex;
	}

	private Tree getTabFolderTree() {

		int index = leftTabFolder.getSelectionIndex();

		Tree removeTree = null;

		if (index == PAGE_INDEX) {

			removeTree = pageTupleTree;

		} else if (index == COL_INDEX) {

			removeTree = columnTupleTree;

		} else if (index == ROW_INDEX) {

			removeTree = rowTupleTree;

		}

		return removeTree;
	}

	private void removeTreeItems() {

		Tree tree = getTabFolderTree();

		TreeItem[] treeSelections = tree.getSelection();

		if (treeSelections.length > 0) {

			// check to see if this is a member selection
			if (treeSelections.length == 1) {

				TreeItem selectedItem = treeSelections[0];

				MemberSelection memberSelection = getMemberSelectionFromTreeItem(selectedItem
						.getData());

				// if member is a members, add to list of members.
				if (memberSelection != null
						&& memberSelection.getSelectionType().equals(
								MemberSelectionType.Members)) {

					memberSelection.removeMember();

					selectedItem.setText(memberSelection
							.getMemberWithSelection());

					// if members [] is not empty, return.
					if (memberSelection.numberOfMembers() != 0) {
						return;
					}

				}

			}

			ArrayList<TreeItem> modifiedSelections = new ArrayList<TreeItem>();

			// if parent is null, dispose of widget, otherwise add to list of
			// modified selections
			for (TreeItem tempSelection : treeSelections) {
				if (tempSelection.getParentItem() == null) {
					tempSelection.dispose();
				} else {
					modifiedSelections.add(tempSelection);
				}
			}

			// convert into an array of tree item array
			treeSelections = modifiedSelections.toArray(new TreeItem[0]);

			// if no more items need to be removed, exit
			if (treeSelections.length == 0) {
				return;
			}

			// used to hold all selected tree items parents
			HashSet<TreeItem> parentSet = new HashSet<TreeItem>();

			// add all parents to set
			for (TreeItem tempSelection : treeSelections) {
				parentSet.add(tempSelection.getParentItem());
			}

			// loop throught unique set, get children
			for (TreeItem parent : parentSet) {

				ArrayList<TempNode> children = new ArrayList<TempNode>();
				TreeItem[] items = parent.getItems();

				TempNode parentNode = new TempNode(null, parent.getText());

				Set<TreeItem> set = new HashSet<TreeItem>();

				for (TreeItem selectedItem : treeSelections) {
					set.add(selectedItem);
				}

				for (int i = 0; i < items.length; i++) {
					
					if (!set.contains(items[i])) {

						TempNode currentNode = new TempNode(parentNode,
								items[i].getText());
						currentNode.setExpanded(items[i].getExpanded());
						currentNode.setData(items[i].getData());

						currentNode.setUserSelectMultipleDataItem(items[i].getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE));
						currentNode.setMemberTagNode(items[i].getData(MEMBER_TAG_KEY));
						
						convertTreeItemToNodes(items[i], currentNode);

						children.add(currentNode);

					}

				}
				assert children.size() == (items.length - 1);
				parent.removeAll();

				for (TempNode currentNode : children) {
					populateTreeItemFromNode(parent, currentNode);
				}

				parent.setExpanded(true);
			}
		} else {
			logger.debug("Remove - nothing selected.");
		}
	}

	private void convertTreeItemToNodes(TreeItem item, TempNode node) {

		if (item.getItems() != null && item.getItems().length > 0) {

			for (TreeItem childItem : item.getItems()) {

				TempNode childNode = new TempNode(node, childItem.getText());

				childNode.setExpanded(childItem.getExpanded());
				childNode.setData(childItem.getData());
				
				childNode.setUserSelectMultipleDataItem(childItem.getData(USER_SELECTION_MULTIPLE));
				childNode.setMemberTagNode(childItem.getData(MEMBER_TAG_KEY));
				
				convertTreeItemToNodes(childItem, childNode);

			}

		}

	}

	private void populateTreeItemFromNode(TreeItem item, TempNode node) {

		TreeItem newItem = new TreeItem(item, SWT.NONE);
		newItem.setText(node.getText());

		if (node.numberOfChildren() > 0) {

			for (TempNode child : node.getChildren()) {
				populateTreeItemFromNode(newItem, child);
			}
		}

		newItem.setExpanded(node.isExpanded());
		newItem.setData(node.getData());
		
		if ( node.getUserSelectMultipleDataItem() != null ) {
			newItem.setData(USER_SELECTION_MULTIPLE, node.getUserSelectMultipleDataItem());
		}
		
		if ( node.getMemberTagNode() != null ) {
			newItem.setData(MEMBER_TAG_KEY, node.getMemberTagNode());
		}

	}

	private void populateTreeFromNode(Tree tree, TempNode node) {

		TreeItem newItem = new TreeItem(tree, SWT.NONE);
		newItem.setText(node.getText());

		if (node.numberOfChildren() > 0) {

			for (TempNode child : node.getChildren()) {
				populateTreeItemFromNode(newItem, child);
			}
		}

		newItem.setExpanded(node.isExpanded());
		newItem.setData(node.getData());
		
		if ( node.getUserSelectMultipleDataItem() != null ) {
			newItem.setData(USER_SELECTION_MULTIPLE, node.getUserSelectMultipleDataItem());
		}
		
		if ( node.getMemberTagNode() != null ) {
			newItem.setData(MEMBER_TAG_KEY, node.getMemberTagNode());
		}

	}

	private void initilizeButton(Button button) {

		if (button != null) {
			button.setEnabled(false);
		}

	}

	private void updateButtonStatus(Tree tupleTree, Button button, int buttonId) {

		boolean enabled = true;

		TreeItem[] selectedItems = tupleTree.getSelection();

		if (selectedItems.length == 0 || selectedItems.length > 1) {

			enabled = false;

		} else {

			TreeItem selection = selectedItems[0];

			TreeItem parent = selection.getParentItem();

			int selectionLevel = TreeUtil.getTreeItemLevel(selection);

			if (selectionLevel > 0 && parent.getItems().length > 1) {

				int selectionIndex = getIndexWithinParent(parent, selection);

				if (buttonId == UP_BUTTON_ID) {

					if (selectionIndex > 0) {

						enabled = true;

					} else {

						enabled = false;

					}

				} else if (buttonId == DOWN_BUTTON_ID) {

					if (selectionIndex + 1 != parent.getItemCount()) {

						enabled = true;

					} else {

						enabled = false;

					}
				}

			} else if (selectionLevel == 0) {

				Tree builtTree = getTabFolderTree();

				int numOfChildren = builtTree.getItemCount();

				if (numOfChildren == 0 || numOfChildren == 1) {
					enabled = false;
				} else {

					int selectionIndex = 0;
					for (TreeItem item : builtTree.getItems()) {

						if (item.equals(selection)) {
							break;
						}
						selectionIndex++;
					}

					if (buttonId == UP_BUTTON_ID) {
						if (selectionIndex == 0) {
							enabled = false;
						} else {
							enabled = true;
						}
					} else if (buttonId == DOWN_BUTTON_ID) {

						if (selectionIndex + 1 == builtTree.getItemCount()) {
							enabled = false;
						} else {
							enabled = true;
						}

					}

				}

			} else {
				enabled = false;
			}
		}

		button.setEnabled(enabled);
	}

	private int getIndexWithinParent(TreeItem parent, TreeItem selection) {

		TreeItem[] parentItems = parent.getItems();

		int index = 0;
		int selectionIndex = 0;

		for (TreeItem item : parentItems) {

			if (item.equals(selection)) {
				selectionIndex = index;
				break;
			}

			index++;
		}

		return selectionIndex;

	}

	private void directionalButtonPressed(int buttonId) {

		Tree tree = getTabFolderTree();

		TreeItem selection = tree.getSelection()[0];

		TreeItem parent = selection.getParentItem();

		int selectionLevel = TreeUtil.getTreeItemLevel(selection);

		if (selectionLevel > 0 && parent.getItems().length > 1) {

			int selectionIndex = getIndexWithinParent(parent, selection);

			ArrayList<TempNode> tempNodes = new ArrayList<TempNode>();

			for (TreeItem childItem : parent.getItems()) {

				TempNode tempNode = new TempNode(null, childItem);

				convertTreeItemToNodes(childItem, tempNode);

				tempNodes.add(tempNode);

			}

			TempNode switchNode = tempNodes.get(selectionIndex + buttonId);
			TempNode selectedNode = tempNodes.get(selectionIndex);

			if (buttonId == UP_BUTTON_ID) {
				tempNodes.remove(selectionIndex + buttonId);
				tempNodes.remove(selectionIndex + buttonId);
				tempNodes.add(selectionIndex + buttonId, selectedNode);
				tempNodes.add(selectionIndex, switchNode);
			} else if (buttonId == DOWN_BUTTON_ID) {
				tempNodes.remove(selectionIndex);
				tempNodes.remove(selectionIndex);
				tempNodes.add(selectionIndex, switchNode);
				tempNodes.add(selectionIndex + buttonId, selectedNode);
			}

			parent.removeAll();

			for (TempNode currentNode : tempNodes) {
				populateTreeItemFromNode(parent, currentNode);
			}

			parent.setExpanded(true);

			TreeItem newSelection = parent.getItem(selectionIndex + buttonId);

			newSelection.getParent().setSelection(
					new TreeItem[] { newSelection });

			updateButtonStatus(getTabFolderTree(), upButton, UP_BUTTON_ID);
			updateButtonStatus(getTabFolderTree(), downButton, DOWN_BUTTON_ID);

		} else if (selectionLevel == 0) {

			Tree builtTree = getTabFolderTree();

			int selectionIndex = 0;

			for (TreeItem item : builtTree.getItems()) {

				if (item.equals(selection)) {
					break;
				}
				selectionIndex++;
			}

			ArrayList<TempNode> tempNodes = new ArrayList<TempNode>();

			for (TreeItem childItem : builtTree.getItems()) {

				TempNode tempNode = new TempNode(null, childItem);

				convertTreeItemToNodes(childItem, tempNode);

				tempNodes.add(tempNode);

			}

			TempNode switchNode = tempNodes.get(selectionIndex + buttonId);
			TempNode selectedNode = tempNodes.get(selectionIndex);

			if (buttonId == UP_BUTTON_ID) {
				tempNodes.remove(selectionIndex + buttonId);
				tempNodes.remove(selectionIndex + buttonId);
				tempNodes.add(selectionIndex + buttonId, selectedNode);
				tempNodes.add(selectionIndex, switchNode);
			} else if (buttonId == DOWN_BUTTON_ID) {
				tempNodes.remove(selectionIndex);
				tempNodes.remove(selectionIndex);
				tempNodes.add(selectionIndex, switchNode);
				tempNodes.add(selectionIndex + buttonId, selectedNode);
			}

			builtTree.removeAll();

			for (TempNode currentNode : tempNodes) {
				populateTreeFromNode(builtTree, currentNode);
			}

			TreeItem newSelection = builtTree
					.getItem(selectionIndex + buttonId);

			builtTree.setSelection(new TreeItem[] { newSelection });

			updateButtonStatus(getTabFolderTree(), upButton, UP_BUTTON_ID);
			updateButtonStatus(getTabFolderTree(), downButton, DOWN_BUTTON_ID);

		}

	}

	private void setHeaderContentEnabled(boolean enabled) {
		headerBorderAllCheck.setEnabled(enabled);
		headerBorderLeftCheck.setEnabled(enabled);
		headerBorderRightCheck.setEnabled(enabled);
		headerBorderTopCheck.setEnabled(enabled);
		headerBorderBottomCheck.setEnabled(enabled);
		headerGlobalStyle.setEnabled(enabled);
		headerParentFirstButton.setEnabled(enabled);
		headerRowHeight.setEnabled(enabled);
		headerColumnWidth.setEnabled(enabled);

	}

	private void setDataContentEnabled(boolean enabled) {
		dataOverrideProtectionCheck.setEnabled(enabled);
		dataBorderAllCheck.setEnabled(enabled);
		dataBorderLeftCheck.setEnabled(enabled);
		dataBorderRightCheck.setEnabled(enabled);
		dataBorderTopCheck.setEnabled(enabled);
		dataBorderBottomCheck.setEnabled(enabled);
		dataGlobalStyle.setEnabled(enabled);
		dataOverrideNumeric.setEnabled(enabled);
		dataMemberTagEditableCombo.setEnabled(enabled);

	}

	private void setBlankContentEnabled(boolean enabled) {

	}

	private void setMemberPropertiesEnabled(boolean enable) {

		setSelectionGroupEnabled(enable);

	}

	private void setSelectionGroupEnabledSelectionOnly(boolean enable) {

		if (selectionButton != null) {

			selectionButton.setEnabled(enable);

			//true, disable other fields
			if ( enable ) {
				selectionAndChildrenButton.setEnabled(!enable);
	
				justChildrenOfSelectionButton.setEnabled(!enable);
	
				selectionAndDecendantsButton.setEnabled(!enable);
	
				justDescendantsOfSelectionButton.setEnabled(!enable);
				
				justLevelOrGenButton.setEnabled(!enable);
	
				memberSelection.setEnabled(!enable);
				
				enableLevelGenerationControls(!enable);
								
			}
		}

	}

	private void setSelectionGroupEnabled(boolean enable) {

		// if one of buttons is not null, then rest aren't
		if (selectionButton != null) {

			selectionButton.setEnabled(enable);

			selectionAndChildrenButton.setEnabled(enable);

			justChildrenOfSelectionButton.setEnabled(enable);

			selectionAndDecendantsButton.setEnabled(enable);

			justDescendantsOfSelectionButton.setEnabled(enable);
			
			justLevelOrGenButton.setEnabled(enable);

			memberSelection.setEnabled(enable);
			
			
			if ( ! enable ) {

				enableLevelGenerationControls(false);
				
			}

		}
	}


	private void initializeForm() {

		clearHeaderContent();
		clearDataContent();
		clearBlankContent();
		clearSelectionGroup();

		setHeaderContentEnabled(false);
		setDataContentEnabled(false);
		setBlankContentEnabled(false);
		setMemberPropertiesEnabled(false);

		dirtyData = false;
		applyButton.setEnabled(false);
		cancelButton.setEnabled(false);
	}

	private void clearSelectionGroup() {

		if (selectionButton != null) {

			selectionButton.setSelection(false);

			selectionAndChildrenButton.setSelection(false);

			justChildrenOfSelectionButton.setSelection(false);

			selectionAndDecendantsButton.setSelection(false);

			justDescendantsOfSelectionButton.setSelection(false);
			
			justLevelOrGenButton.setSelection(false);

			memberSelection.setSelection(false);

		}
	}

	private void updateSelectionGroup(TreeItem[] selectedItems) {

		// if selected items are 0 or if more than one, disable the selecte
		// group
		if (selectedItems.length == 0 || selectedItems.length > 1) {

			setSelectionGroupEnabled(false);

			return;

		}

		// get the first selected item
		TreeItem selectedItem = selectedItems[0];

		// if selected item is paf blank, clear selection group and disable
		// selection group
		if (isPafBlank(selectedItem) || isMemberTagNode(selectedItem )) {

			clearSelectionGroup();
			setSelectionGroupEnabled(false);

		// if selected item is a user selection, update selection group
		// based on multiples. User
		// selections can have multi select enabled.
		} else if (isUserSelectionMember(selectedItem.getText())) {

			Object dataItem = selectedItem.getData();

			// if data item is a paf user selection and is multiples, set the
			// selection member only
			if (dataItem instanceof PafUserSelection) {

				if (((PafUserSelection) selectedItem.getData()).isMultiples()) {
					setSelectionGroupEnabledSelectionOnly(true);

					selectMemberSelection(MemberSelectionType.Selection);
				}

				// if paf member or paf tuple, perform multiples logic
			} else if (dataItem instanceof PafMember
					|| dataItem instanceof PafTuple) {

				Object multiples = selectedItem
						.getData(USER_SELECTION_MULTIPLE);
				
				// if mulitples is a boolean
				if (multiples != null && multiples instanceof Boolean) {

					// value of boolean
					Boolean multipleSelectOnUserSelection = (Boolean) multiples;

					if (multipleSelectOnUserSelection != null) {

						// if ture, set selection group with selection only
						// enabled
						if (multipleSelectOnUserSelection) {

							setSelectionGroupEnabledSelectionOnly(true);

							selectMemberSelection(MemberSelectionType.Selection);

							// enabled normally
						} else {

							setSelectionGroupEnabled(true);

							PafMember pafMember = getPafMember(dataItem);

							MemberSelection memberSelection = pafMember
									.getSelectionType();
							
							selectMemberSelection(memberSelection
									.getSelectionType());

							configureLevelGenerationControlsForKindsOfDescendants(memberSelection);

						}

					}

				}

			}

		} else {

			setSelectionGroupEnabled(true);

			Object dataItem = selectedItem.getData();
			
			PafMember pafMember = getPafMember(dataItem);

			MemberSelection memberSelection = pafMember.getSelectionType();

			selectMemberSelection(memberSelection.getSelectionType());			
			
			//Get member or 1st member if multiple exits.
			if ( memberSelection.getMembers().length >= 1 ) {
				
				String currentMember = memberSelection.getMembers()[0];
				
				String currentDimensionName = getDimensionNameFromTreeItem(getTabFolderTree().getSelection()[0]);
				
				if ( isMemberInOutline(currentDimensionName, currentMember ) ) {
														
					int memberLevel = getMemberLevel(currentDimensionName, currentMember);
					
					if ( memberLevel == 0 ) {						
						
						disableAllChildrenDescLevelAndGenSelectionsControls();
						
					}
					
				}
				
			}
			
			configureLevelGenerationControlsForKindsOfDescendants(memberSelection);
						 
			
		}

	}


	private String getDimensionNameFromTreeItem(TreeItem selectedItem) {
		
		int selectedTreeItemNdx = getTreeItemLevelNdx(selectedItem);
		
		int treeIndex = getTupleTreeIndex(selectedItem.getParent());
		
		String[] dimensions = null;
		
		switch(treeIndex) {
						
		case ViewSectionTuplesDialog2.ROW_INDEX:
			dimensions = rowDims;
			break;
		case ViewSectionTuplesDialog2.COL_INDEX:
			dimensions = colDims;
			break;
		
		}								
		
		return dimensions[selectedTreeItemNdx];
	}

	private void selectMemberSelection(MemberSelectionType selectionType) {

		if (selectionType != null) {

			clearSelectionGroup();

			//disable leve gen controls by default.  only enabled for desc and idesc
			enableLevelGenerationControls(false);
			
			switch (selectionType) {

			case Selection:
				selectionButton.setSelection(true);
				break;
			case IChild:
			case IChildren:
				selectionAndChildrenButton.setSelection(true);
				break;
			case Child:
			case Children:
				justChildrenOfSelectionButton.setSelection(true);
				break;
			case IDesc:
			case IDescendants:
				selectionAndDecendantsButton.setSelection(true);

				enableLevelGenerationControls(true);
				
				break;
			case Desc:
			case Descendants:
				justDescendantsOfSelectionButton.setSelection(true);

				enableLevelGenerationControls(true);
				
				break;
			case Members:
				memberSelection.setSelection(true);
				break;
				
			case Level:
				justLevelOrGenButton.setSelection(true);
				enableLevelGenerationControls(true);
				break;
				
			case Generation:
				justLevelOrGenButton.setSelection(true);
				enableLevelGenerationControls(true);
				break;				
			
			}

		} else {
			selectionButton.setSelection(true);
		}

	}

	private MemberSelectionType getSelectionTypeFromMemberProperties() {

		MemberSelectionType selectionType = null;

		// if selection and other's are enabled, get selection type
		if (selectionButton.isEnabled()) {

			if (selectionButton.getSelection()) {
				selectionType = MemberSelectionType.Selection;
			} else if (selectionAndChildrenButton.getSelection()) {
				selectionType = MemberSelectionType.IChildren;
			} else if (justChildrenOfSelectionButton.getSelection()) {
				selectionType = MemberSelectionType.Children;
			} else if (selectionAndDecendantsButton.getSelection()) {
				selectionType = MemberSelectionType.IDesc;
			} else if (justDescendantsOfSelectionButton.getSelection()) {
				selectionType = MemberSelectionType.Desc;
			} else if (justLevelOrGenButton.getSelection()) {
				
				String levelGenDropdownValue = levelGenDropDown.getText();
				
				if ( levelGenDropDown.isEnabled() && levelGenDropdownValue.equals(LevelGenerationType.Generation.toString()) ) {
					
					selectionType = MemberSelectionType.Generation;
					
				} else {
					
					selectionType = MemberSelectionType.Level;
				}
				
				
			} else if (memberSelection.getSelection()) {
				selectionType = MemberSelectionType.Members;
			}

		}

		if (selectionType == null) {
			selectionType = MemberSelectionType.Selection;
		}

		return selectionType;

	}

	private void populateForm(TreeItem[] selectedItems) {

		if (selectedItems.length > 1) {

			return;

		}

		clearBlankContent();
		clearDataContent();
		clearHeaderContent();

		if (selectedItems.length == 0) {
			return;
		}

		TreeItem selectedItem = selectedItems[0];

		tuplePropsFolder.setSelection(tuplePropsFolder.getSelectionIndex());

		Object member = selectedItem.getData();

		if (member instanceof PafTuple) {

			PafTuple tuple = (PafTuple) member;

			populateTupleData(tuple);

		}

		dirtyData = false;

	}

	private boolean isPafBlank(TreeItem item) {

		boolean pafBlank = false;

		if (item != null) {

			if (item.getText().equals(PafBaseConstants.PAF_BLANK)) {
				pafBlank = true;
			}

		}

		return pafBlank;

	}

	private void clearHeaderContent() {

		cacheHeader = false;

		headerBorderAllCheck.setSelection(false);
		headerBorderLeftCheck.setSelection(false);
		headerBorderRightCheck.setSelection(false);
		headerBorderTopCheck.setSelection(false);
		headerBorderBottomCheck.setSelection(false);
		headerGlobalStyle.deselectAll();
		headerGlobalStyle.clearSelection();
		headerParentFirstButton.setSelection(false);
		headerRowHeight.setText("");
		headerColumnWidth.setText("");

		cacheHeader = true;
	}

	private void clearDataContent() {

		cacheData = false;

		dataOverrideProtectionCheck.setSelection(false);
		dataBorderAllCheck.setSelection(false);
		dataBorderLeftCheck.setSelection(false);
		dataBorderRightCheck.setSelection(false);
		dataBorderTopCheck.setSelection(false);
		dataBorderBottomCheck.setSelection(false);

		dataGlobalStyle.deselectAll();
		dataGlobalStyle.clearSelection();

		dataOverrideNumeric.deselectAll();
		dataOverrideNumeric.clearSelection();
		
		dataMemberTagEditableCombo.select(0);	

		cacheData = true;

	}

	private void clearBlankContent() {
	}

	private void headerStateChanged() {

		if (!dirtyData && !bootingUp) {
			logger.info("Header State Changed");
			dirtyData = true;
		}

		if (dirtyData && !bootingUp && headerGlobalStyle.isEnabled()) {

			if (cacheHeader) {

				if (applyButton != null && getTabFolderTree().getSelection().length > 1	) {
					applyButton.setEnabled(true);
					cancelButton.setEnabled(true);
				} else {
					applyButton.setEnabled(false);
					cancelButton.setEnabled(false);
				}
					

				cacheHeaderForm();
				
				if ( getTabFolderTree().getSelection().length == 1 ) {
					saveForm();
				}
			}
		}

	}

	private void dataStateChanged() {

		if (!dirtyData && !bootingUp) {
			logger.info("Data State Changed");
			dirtyData = true;
		}

		if (dirtyData && !bootingUp && dataGlobalStyle.isEnabled()) {

			if (cacheData) {

				if (applyButton != null && getTabFolderTree().getSelection().length > 1	) {
					applyButton.setEnabled(true);
					cancelButton.setEnabled(true);
				} else {
					applyButton.setEnabled(false);
					cancelButton.setEnabled(false);
				}

				cacheDataForm();
			
				if ( getTabFolderTree().getSelection().length == 1 ) {
					saveForm();
				}
			}
		}
	}

	private void cacheDataForm() {

		logger.debug("Cacheing Data Form");

		if (currentDataState == null) {
			currentDataState = new PafMemberData();
		}

		if (dataOverrideProtectionCheck.getSelection()) {
			currentDataState.setPlannable(false);
		} else {
			currentDataState.setPlannable(null);
		}

		PafBorder dataBorder = new PafBorder();

		if (dataBorderAllCheck.getSelection()) {
			dataBorder.setBorder(PafBorder.BORDER_ALL);
		} else {

			int borderState = 0;

			if (dataBorderLeftCheck.getSelection()) {
				borderState = PafBorder.BORDER_LEFT;
			}

			if (dataBorderTopCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_TOP;
			}

			if (dataBorderRightCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_RIGHT;
			}

			if (dataBorderBottomCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_BOTTOM;
			}

			dataBorder.setBorder(borderState);

		}

		currentDataState.setBorder(dataBorder);

		if (dataGlobalStyle.getText().length() != 0) {
			currentDataState.setGlobalStyle(dataGlobalStyle.getText());
		} else {
			currentDataState.setGlobalStyle(null);
		}

		if (dataOverrideNumeric.getText().trim().length() != 0) {
			currentDataState.setOverrideNumericFormat(dataOverrideNumeric
					.getText().trim());
		} else {
			currentDataState.setOverrideNumericFormat(null);
		}
		
		//if the dropdown has default, set value to null, otherwise convert value to a boolean
		if ( ! dataMemberTagEditableCombo.getText().equals(Constants.DROPDOWN_GLOBAL_OPTION)) {
			
			currentDataState.setMemberTagEditable(Boolean.valueOf(dataMemberTagEditableCombo.getText()));
			
		} else {
			
			currentDataState.setMemberTagEditable(null);
		}

	}

	private void cacheHeaderForm() {

		logger.debug("Cacheing Header Form");

		if (currentHeaderState == null) {
			currentHeaderState = new PafMemberHeader();
		}

		currentHeaderState.setParentFirst(headerParentFirstButton
				.getSelection());

		PafBorder headerBorder = new PafBorder();

		if (headerBorderAllCheck.getSelection()) {
			headerBorder.setBorder(PafBorder.BORDER_ALL);
		} else {

			int borderState = 0;

			if (headerBorderLeftCheck.getSelection()) {
				borderState = PafBorder.BORDER_LEFT;
			}

			if (headerBorderTopCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_TOP;
			}

			if (headerBorderRightCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_RIGHT;
			}

			if (headerBorderBottomCheck.getSelection()) {
				borderState = borderState | PafBorder.BORDER_BOTTOM;
			}

			headerBorder.setBorder(borderState);

		}

		currentHeaderState.setBorder(headerBorder);

		currentHeaderState.setGlobalStyle(headerGlobalStyle.getText().trim());

		if (headerRowHeight.getText().trim().length() == 0) {
			currentHeaderState.setRowHeight(null);
		} else {
			currentHeaderState.setRowHeight(new Float(headerRowHeight.getText()));
		}

		if (headerColumnWidth.getText().trim().length() == 0) {
			currentHeaderState.setColumnWidth(null);
		} else {
			currentHeaderState.setColumnWidth(new Float(headerColumnWidth.getText()));
		}

	}

	private void buttonSelected(int buttonId) {

		switch (buttonId) {
		case CANCEL_BUTTON_ID:

			TreeItem[] selectedItems = getTabFolderTree().getSelection();

			if (selectedItems.length > 1) {

				getTabFolderTree().deselectAll();
				updateTupleProperties(getTabFolderTree(), leftTabFolder
						.getSelectionIndex());
				clearBlankContent();
				clearDataContent();
				clearHeaderContent();

			} else {

				populateForm(selectedItems);

			}

			applyButton.setEnabled(false);
			cancelButton.setEnabled(false);

			break;

		case APPLY_BUTTON_ID:

			saveMultipleFormItems();

			break;
		}

	}
	
	private void saveForm() {
		
		TreeItem[] selectedItems = getTabFolderTree().getSelection();
		
		logger.info("Saving Form() ");
		
		if ( selectedItems.length == 1 ) {
			
			TreeItem selectedItem = selectedItems[0];
		
			Object dataObject = selectedItem.getData();
			
			if (dataObject instanceof PafTuple) {

				logger.debug("Saving Form - saving tuple ");
				
				PafTuple tuple = (PafTuple) dataObject;

				if (currentHeaderState != null) {
									
					try {
						tuple.setHeader((PafMemberHeader) currentHeaderState.clone());
					} catch (CloneNotSupportedException e) {
						
						e.printStackTrace();
						
					}
				}
				
				if (currentDataState != null) {
					try {
						
						tuple.setData((PafMemberData) currentDataState.clone());

					} catch (CloneNotSupportedException e) {

						e.printStackTrace();
					}
				}

				setLastTupleUsed(tuple);					
				
			}
			
		}
		
		
	}

	private void saveMultipleFormItems() {

		TreeItem[] selectedItems = getTabFolderTree().getSelection();
		
		logger.info("Saving Multiple Form() ");

		if (selectedItems != null) {

			for (TreeItem selectedItem : selectedItems) {

				Object dataObject = selectedItem.getData();

				// maybe check for member and continue if found

				if (dataObject instanceof PafTuple) {

					PafTuple tuple = (PafTuple) dataObject;

					if (currentHeaderState != null) {
						
						try {
							
							tuple.setHeader((PafMemberHeader) currentHeaderState.clone());
							
						} catch (CloneNotSupportedException e) {
							
							e.printStackTrace();
							logger.error("Problem cloning the PafMemberHeader");
							
						}
					}

					if (currentDataState != null) {
						
						try {
						
							tuple.setData((PafMemberData) currentDataState.clone());

							//if tuple is not a member tag, null out member tag only prop after clone
							if ( ! tuple.isMemberTag() ) {
								tuple.getData().setMemberTagEditable(null);
							}
							
						} catch (CloneNotSupportedException e) {

							e.printStackTrace();
							logger.error("Problem cloning the PafMemberData");
						}
					}

					// if only 1 item selected, cache this updated tuple
					if (selectedItems.length == 1) {

						setLastTupleUsed(tuple);
					}

				}

			}

			currentHeaderState = null;
			currentDataState = null;
			// currentBlankState = null;
			dirtyData = false;
			applyButton.setEnabled(false);
			cancelButton.setEnabled(false);

		}

	}

	private TreeItem[] getArrayFromParentToItem(TreeItem item) {

		ArrayList<TreeItem> reverseList = new ArrayList<TreeItem>();

		reverseList.add(item);

		populateListOfParentItems(reverseList, item);

		ArrayList<TreeItem> list = new ArrayList<TreeItem>(reverseList.size());

		int size = reverseList.size();

		for (int i = size - 1; i >= 0; i--) {
			list.add(reverseList.get(i));
		}

		return list.toArray(new TreeItem[0]);

	}

	private void populateListOfParentItems(ArrayList<TreeItem> list,
			TreeItem item) {

		TreeItem parentItem = item.getParentItem();

		if (parentItem != null) {

			list.add(parentItem);

			populateListOfParentItems(list, parentItem);

		}

	}

	private void populateTupleData(PafTuple tuple) {

		if (tuple != null) {

			populateHeaderForm(tuple.getHeader());
			populateDataForm(tuple.getData());

		}

	}

	private void populateHeaderForm(PafMemberHeader header) {

		if (header != null) {

			cacheHeader = false;

			if (header.getParentFirst() != null) {

				headerParentFirstButton.setSelection(header.getParentFirst());

			}

			PafBorder border = header.getBorder();

			if (border != null) {

				headerBorderAllCheck.setSelection(border.isAll());
				headerBorderLeftCheck.setSelection(border.isLeft());
				headerBorderTopCheck.setSelection(border.isTop());
				headerBorderBottomCheck.setSelection(border.isBottom());
				headerBorderRightCheck.setSelection(border.isRight());

				performBorderLogic(headerBorderAllCheck, headerBorderLeftCheck,
						headerBorderRightCheck, headerBorderTopCheck,
						headerBorderBottomCheck);

			}

			if (header.getGlobalStyle() != null) {
				headerGlobalStyle.setText(header.getGlobalStyle());
			}

			if (header.getRowHeight() != null) {
				headerRowHeight.setText(header.getRowHeight().toString());
			}

			if (header.getColumnWidth() != null) {
				headerColumnWidth.setText(header.getColumnWidth().toString());
			}

			cacheHeader = true;
		}

	}

	private void populateDataForm(PafMemberData data) {

		if (data != null) {

			cacheData = false;

			PafBorder border = data.getBorder();

			if (border != null) {

				dataBorderAllCheck.setSelection(border.isAll());
				dataBorderLeftCheck.setSelection(border.isLeft());
				dataBorderTopCheck.setSelection(border.isTop());
				dataBorderBottomCheck.setSelection(border.isBottom());
				dataBorderRightCheck.setSelection(border.isRight());

				performBorderLogic(dataBorderAllCheck, dataBorderLeftCheck,
						dataBorderRightCheck, dataBorderTopCheck,
						dataBorderBottomCheck);

			}

			if (data.getPlannable() != null) {

				dataOverrideProtectionCheck.setSelection(!data.getPlannable());

			}

			if (data.getGlobalStyle() != null) {
				dataGlobalStyle.setText(data.getGlobalStyle());
			}

			if (data.getOverrideNumericFormat() != null) {
				dataOverrideNumeric.setText(data.getOverrideNumericFormat());
			}
			
			if ( data.getMemberTagEditable() != null ) {
				dataMemberTagEditableCombo.setText(StringUtil.initCap(data.getMemberTagEditable().toString()));
			}

			cacheData = true;
		}

	}

	@Override
	protected void okPressed() {

		PafViewSection viewSection = currentViewSection.getPafViewSection();

		if (!verifyCompleteTupleTrees(viewSection)) {
			return;
		}

		PageTuple[] pageTuples = createPageTupleArray();

		viewSection.setPageTuples(pageTuples);

		ViewTuple[] columnTuples = createTupleArray(columnTupleTree,
				PafAxis.COL);

		viewSection.setColTuples(columnTuples);

		ViewTuple[] rowTuples = createTupleArray(rowTupleTree, PafAxis.ROW);

		viewSection.setRowTuples(rowTuples);
						
		currentViewSection.setPafViewSection(viewSection);

		//REMOVED BECAUSE OF FARKAS
		//currentViewSection = updateCommentMemberTags(currentViewSection);
		
		viewSectionManager.replace(currentViewSectionName, currentViewSection);

		viewSectionManager.save();

		super.okPressed();
	}

	/*REMOVED BECAUSE OF FARKAS
	private PafViewSectionUI updateCommentMemberTags(PafViewSectionUI viewSection) {
		
		if ( viewSection.getMemberTagCommentNames() != null ) {
		
			List<String> validMemberTagCommentNameList = new ArrayList<String>();
						
			Set<String> tupleMemberTagNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
			
			tupleMemberTagNames.addAll(Arrays.asList(viewSection.getMemberTagNamesFromViewTuples()));
			
			for (String memberTagCommentName : viewSection.getMemberTagCommentNames()) {
				
				if ( ! tupleMemberTagNames.contains(memberTagCommentName) ) {
					
					validMemberTagCommentNameList.add(memberTagCommentName);
					
				}
				
			}
			
			if ( validMemberTagCommentNameList.size() <= 0 ) {
				
				viewSection.setMemberTagCommentNames(null);
				
			} else {
				
				viewSection.setMemberTagCommentNames(validMemberTagCommentNameList.toArray(new String[0]));
				
			}
			
			
		}		
				
		return viewSection;
	}
	*/

	/*private String[] getTupleMemberTagName(ViewTuple[] viewTuples) {

		Set<String> tupleMemberTagNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		if ( viewTuples != null ) {
			
			for (ViewTuple viewTuple : viewTuples ) {
				
				if ( viewTuple.isMemberTag() ) {
					
					tupleMemberTagNames.add(viewTuple.getMemberDefs()[0]);
					
				}
				
			}
			
		}
		
		if ( tupleMemberTagNames.size() <= 0 ) {
			
			return null;
			
		} else {
			
			return tupleMemberTagNames.toArray(new String[0]);
		}
		
	}*/

	// verify that tuple trees are completely built
	private boolean verifyCompleteTupleTrees(PafViewSection viewSection) {

		
		boolean pageTreeComplete = verifyTupleTree(pageDims.length,
				pageTupleTree, "page");

		if (!pageTreeComplete) {
			return false;
		}

		boolean colTreeComplete = verifyTupleTree(colDims.length,
				columnTupleTree, "column");

		if (!colTreeComplete) {
			return false;
		}

		boolean rowTreeComplete = verifyTupleTree(rowDims.length, rowTupleTree,
				"row");

		if (!rowTreeComplete) {
			return false;
		}

		return true;

	}

	private boolean verifyTupleTree(int numberOfDims, Tree tree,
			String currentTreeName) {

		TreeItem root = tree.getTopItem();

		if (root != null) {

			for (TreeItem rootItem : tree.getItems()) {

				ArrayList<TreeItem> treeItemList = new ArrayList<TreeItem>();

				populateListFromItem(treeItemList, rootItem);

				for (TreeItem treeItem : treeItemList) {

					int treeItemLevel = TreeUtil.getTreeItemLevel(treeItem);

					if (treeItemLevel < numberOfDims - 1) {

						if (treeItem.getItems().length == 0) {

							// get tree tab index
							int tabIndex = getTupleTreeIndex(tree);

							// select tab folder for current tree
							rightTabFolder.setSelection(tabIndex);
							leftTabFolder.setSelection(tabIndex);

							//set focus
							tree.setFocus();
														
							TreeItem[] newSelectedItems = new TreeItem[] { treeItem };
							
							initializeForm();
														
							tree.setSelection(newSelectedItems);
							
							if ( ! tree.equals(pageTupleTree)) {
																
								updateSelectionGroup(newSelectedItems);
	
							}
														
							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "Incomplete "
									+ currentTreeName
									+ " tuple tree.  Check member "
									+ treeItem.getText(), MessageDialog.ERROR);

							return false;
						}

					}
				}

			}

		} else {

			// get tree tab index
			int tabIndex = getTupleTreeIndex(tree);
			
			//if page dimensions, check to see if number of dims are 0, if are, just return true
			//because a user doesn't have to define page dimensions.
			if ( tabIndex == 0 ) {
				
				if ( numberOfDims == 0 ) {
					return true;
				}
				
			}

			// select tab folder for current tree
			rightTabFolder.setSelection(tabIndex);
			leftTabFolder.setSelection(tabIndex);
						
			initializeForm();
						
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "No " + currentTreeName
					+ " members have been built.", MessageDialog.ERROR);
				
			return false;

		}

		return true;

	}

	private PageTuple[] createPageTupleArray() {

		ArrayList<PageTuple> pageTuplesList = new ArrayList<PageTuple>();

		ArrayList<TreeItem> treeItemList = new ArrayList<TreeItem>();

		// if items exist, get populate the treeItemList with the items
		if (pageTupleTree.getItemCount() > 0) {

			populateListFromItem(treeItemList, pageTupleTree.getTopItem());

		}

		// loop through the page dimensions
		for (int i = 0; i < pageDims.length; i++) {

			PageTuple pageTuple = new PageTuple();
			pageTuple.setAxis(pageDims[i]);

			if (i < treeItemList.size()) {

				TreeItem currentTreeItem = (TreeItem) treeItemList.get(i);

				if (currentTreeItem != null) {

					pageTuple.setMember(currentTreeItem.getText());

				}

			}

			pageTuplesList.add(pageTuple);
		}

		return pageTuplesList.toArray(new PageTuple[0]);
	}

	/**
	 * 
	 *	Create a view tuple array based on the tuple tree and axis passed in
	 *
	 * @param tupleTree
	 * @param axis
	 * @return
	 */
	private ViewTuple[] createTupleArray(Tree tupleTree, int axis) {

		ArrayList<ViewTuple> viewTupleList = new ArrayList<ViewTuple>();

		// if item count is greater than 0
		if (tupleTree.getItemCount() > 0) {

			// get root items
			TreeItem[] rootItems = tupleTree.getItems();

			// level tracker used to keep a heirachael map of Level of members
			// in tree
			Map<Integer, Integer> levelTracker = new TreeMap<Integer, Integer>();

			/*
			 * For each root item, a tree item array list gets created and all members
			 * from the root item down to the last level or empty leaf will be pushed 
			 * into this list.
			 * 
			 * For example, if the tuple tree is like this:
			 * 	prod
			 * 		uow_root
			 * 			plan_version
			 * 				uow_root
			 * 					s01
			 * 		SLS$
			 * 			WP
			 * 				uow_root
			 * 					s02
			 * 
			 * the treeItem list would be:
			 * 	[prod, uow_root, plan_vesion, uow_root, s01, SLS$, WP, uow_root, S02].
			 * Each of those members in the list are tree items and each tree item
			 * has a level number in the tree (not essbase level, tree level).  For 
			 * example, prod is level 0, uow_root and SLS$ are level 1, etc.
			 */
			for (TreeItem rootItem : rootItems) {

				ArrayList<TreeItem> treeItemList = new ArrayList<TreeItem>();

				populateListFromItem(treeItemList, rootItem);

				ArrayList<PafMember> pafMembersList = new ArrayList<PafMember>();

				// if level tracker has a 0, meaning it's already been used,
				// cache level 0
				// value and then clear contents, then put level 0 back.
				if (levelTracker.containsKey(0)) {

					int cacheLevel0 = levelTracker.get(0);

					levelTracker.clear();

					levelTracker.put(0, cacheLevel0);

				}

				/*
				 * For each item in the list, find the tree item level number
				 * and keep track.  Get the data item and see if it's a pafMember
				 * or a PafTuple.  If a pafTuple, it can be assumed its the last
				 * level of the tree.  If not a paf tuple, but a pafMember, add 
				 * pafMember to list of pafMemberList.  Once a pafTyple is found, 
				 * hte pafMemberList is used to get all the pafMembers for that 
				 * tuple.
				 */
				for (TreeItem treeItem : treeItemList) {

					// get tree item level
					int itemLevel = TreeUtil.getTreeItemLevel(treeItem);
					
					//Begin - TTN-750
					/*
					 * This if checks to see if the current paf member size is greater than the item level,
					 * and if so, i will truncated the size of the pafMemberList down to the itemLevel.
					 */
					if ( pafMembersList.size() > itemLevel ) {
						
						for (int i = pafMembersList.size()-1; i >= itemLevel; i--) {
							
							pafMembersList.remove(i);
							
						}
						
					}
					//End - TTN-750

					int level = 0;

					// if level tracker is already using level, get and
					// increment by 1
					if (levelTracker.containsKey(itemLevel)) {
						level = levelTracker.get(itemLevel);
						level++;
					} else {
						levelTracker.put(itemLevel, level);
					}

					// last level and not level 0, remove member from list
					if (itemLevel != 0
							&& itemLevel == pafMembersList.size() - 1) {

						pafMembersList.remove(itemLevel);

					}

					logger.debug("\nITEM LEVEL: " + treeItem.getText() + ":"
							+ itemLevel);

					levelTracker.put(itemLevel, level);

					logger.debug("LEVEL Tracker: " + levelTracker);

					Object dataItem = treeItem.getData();

					// if paf tuple, create a paf tuple object
					if (dataItem instanceof PafTuple) {

						ViewTuple viewTuple = new ViewTuple();

						viewTuple.setAxis(axis);

						PafTuple pafTuple = (PafTuple) dataItem;

						PafMemberHeader pafHeader = pafTuple.getHeader();

						viewTuple.setParentFirst(pafHeader.getParentFirst());

						viewTuple.setHeaderBorder(pafHeader.getBorder());

						viewTuple.setHeaderGlobalStyleName(pafHeader
								.getGlobalStyle());

						viewTuple.setRowHeight(pafHeader.getRowHeight());

						viewTuple.setColumnWidth(pafHeader.getColumnWidth());

						PafMemberData pafData = pafTuple.getData();

						viewTuple.setPlannable(pafData.getPlannable());

						viewTuple.setDataBorder(pafData.getBorder());

						viewTuple.setDataGlobalStyleName(pafData
								.getGlobalStyle());
						if (pafData.getOverrideNumericFormat() != null
								&& (!pafData.getOverrideNumericFormat().trim()
										.equals(""))) {
							viewTuple.setNumberFormatOverrideLabel(pafData
									.getOverrideNumericFormat());
						}

						PafMember[] pafMembers = pafTuple.getMemberAr();

						String[] members = new String[pafMembers.length];

						// get the actual paf members first.
						for (int i = 0; i < pafMembersList.size(); i++) {
								
							String strMember = pafMembersList.get(i).getSelectionType().getMemberWithSelection();
							
							try {
								
							members[i] = strMember;
							
							} catch (RuntimeException re) {
								re.printStackTrace();
							}

						}

						members[pafMembers.length - 1] = pafMembers[pafMembers.length - 1]
								.getSelectionType().getMemberWithSelection();

						viewTuple.setMemberDefs(members);

						int sizeOfSymetricGroupNoAr = levelTracker.size() - 1;

						// if size of the symertirc group number ar is 0, then
						// set to null
						if (sizeOfSymetricGroupNoAr == 0) {

							viewTuple.setSymetricGroupNo(null);

						} else {

							// the [] should be 1 less than number of
							// dimensions, per alan
							Integer[] symetricGroupNoAr = new Integer[sizeOfSymetricGroupNoAr];

							int ndx = 0;

							for (Integer key : levelTracker.keySet()) {
								Integer symetricGroupNo = levelTracker.get(key);
								symetricGroupNoAr[ndx++] = symetricGroupNo;

								// if the index equals the size of the symetric
								// group no ar, break;
								if (ndx == sizeOfSymetricGroupNoAr) {
									break;
								}
							}

							viewTuple.setSymetricGroupNo(symetricGroupNoAr);

						}
						
						//if tree itme is a member node, set view tuple property
						if (isMemberTagNode(treeItem)) {
							
							viewTuple.setMemberTag(true);
							
							viewTuple.setIsMemberTagEditable(pafData.getMemberTagEditable());
							
						}

						viewTupleList.add(viewTuple);

					} else if (dataItem instanceof PafMember) {

						pafMembersList.add((PafMember) dataItem);

					}

				}

			}

		}

		return viewTupleList.toArray(new ViewTuple[0]);
	}

	/**
	 * 
	 * Recursive method that will populate the treeItemList with tree items from current item
	 * all the way down the hierarchy.
	 *
	 * @param treeItemList
	 * @param parentItem
	 */
	private void populateListFromItem(List<TreeItem> treeItemList, TreeItem parentItem) {

		treeItemList.add(parentItem);

		TreeItem[] childrenItems = parentItem.getItems();

		if (childrenItems != null && childrenItems.length != 0) {

			for (TreeItem childItem : childrenItems) {

				populateListFromItem(treeItemList, childItem);

			}

		}

	}

	private void populatePageTupleTree() {

		PageTuple[] pageTuples = currentViewSection.getPafViewSection()
				.getPageTuples();

		if (pageTuples != null) {

			TreeItem parentItem = null;

			for (PageTuple pageTuple : pageTuples) {

				if (pageTuple.getMember() != null) {

					TreeItem treeItem = null;

					if (parentItem == null) {
						treeItem = new TreeItem(pageTupleTree, SWT.NONE);
					} else {
						treeItem = new TreeItem(parentItem, SWT.NONE);
					}

					treeItem.setText(pageTuple.getMember());
					parentItem = treeItem;

				}

			}

			expandTreeItems(pageTupleTree.getTopItem());

		}

	}

	// used to populated the paf tuple tree
	private void populateTupleTree(Tree tupleTree, ViewTuple[] tuples) {

		// if tuples are not null
		if (tuples != null) {

			// loop through all tuples
			for (ViewTuple tuple : tuples) {

				// if tuple not null
				if (tuple != null) {

					// get member def array
					String[] memberAr = tuple.getMemberDefs();

					// number of dimensions for the Member Def Ar
					int numberOfDims = memberAr.length;

					int index = 0;

					// tmp parent node
					TreeItem parent = null;

					// for each member in member def ar
					for (String member : memberAr) {

						// if first member
						if (index == 0) {

							// set parent to null
							parent = null;

							// get tuple tree children count
							int itemCount = tupleTree.getItemCount();

							// if no itmes or if item not equals - blah blah
							// blah
							if (itemCount == 0
									|| (!tupleTree.getItem(itemCount - 1)
											.getText().equals(member))) {

								// create new tree item on the tuple tree
								TreeItem treeItem = new TreeItem(tupleTree,
										SWT.NONE);

								// set text for new tree item to current member
								treeItem.setText(member);

								// if index == number of dims - 1 which means
								// last tree item on tree
								// structure, then create a paf tuple and
								// populate from view tuple.
								if (index == numberOfDims - 1) {

									PafTuple pafTuple = populatePafTuple(tuple);

									treeItem.setData(pafTuple);

									//add member tag data key to data object.  This tells program is a member tag
									if ( tuple.isMemberTag() ) {
										
										treeItem.setData(MEMBER_TAG_KEY, tuple.getMemberDefs()[0]);
										
									}
									
									// go over members and see if any are user
									// selections
									for (PafMember pafMember : pafTuple
											.getMemberAr()) {
										if (isUserSelectionMember(pafMember
												.getName())) {

											addUserSelectionToTreeItem(
													treeItem, pafMember
															.getName(), index,
													tuple.getAxis());

										}
									}

									// else create a paf member
								} else {

									// new instance of paf member
									PafMember pafMember = new PafMember();
																		
									//set name to name of member
									pafMember.setName(member);
									
									if (isUserSelectionMember(member)) {

										addUserSelectionToTreeItem(treeItem,
												member, index, tuple.getAxis());

									}
								
									// set selection type
									pafMember
											.setSelectionType(MemberSelection
													.createMemberSelectionFromExistingMember(member));
																		
									// set data on tree item to paf Member
									treeItem.setData(pafMember);
									
									//add member tag data key to data object.  This tells program is a member tag
									if ( tuple.isMemberTag() ) {
										
										treeItem.setData(MEMBER_TAG_KEY, tuple.getMemberDefs()[0]);
										
									}

								}

								// set parent to last tree item
								parent = treeItem;

							} else {

								// set paretn to the item count - 1 from root of
								// tree.
								parent = tupleTree.getItem(itemCount - 1);
							}

						} else {

							// get item count from parent. number of children.
							int itemCount = parent.getItemCount();

							// if first item in array or ( previous item doesn't
							// exist or not last dim)
							if (itemCount == 0
									|| ((!parent.getItem(itemCount - 1)
											.getText().equals(member)) || (index + 1)
											% numberOfDims == 0)) {

								// add tree item to parent tree item
								TreeItem treeItem = new TreeItem(parent,
										SWT.NONE);
								treeItem.setText(member);

								// if index is at last level, create a paf
								// tuple.
								if (index == numberOfDims - 1) {

									PafTuple pafTuple = populatePafTuple(tuple);

									treeItem.setData(pafTuple);
									
									//add member tag data key to data object.  This tells program is a member tag
									if ( tuple.isMemberTag() ) {
										
										treeItem.setData(MEMBER_TAG_KEY, tuple.getMemberDefs()[0]);
										
									}

									for (PafMember pafMember : pafTuple
											.getMemberAr()) {
										if (isUserSelectionMember(pafMember
												.getName())) {

											addUserSelectionToTreeItem(
													treeItem, pafMember
															.getName(), index,
													tuple.getAxis());

										}
									}

									// else create a paf Member
								} else {

									PafMember pafMember = new PafMember();

									pafMember.setName(member);
									
									if (isUserSelectionMember(member)) {

										addUserSelectionToTreeItem(treeItem,
												member, index, tuple.getAxis());

									}
									
									pafMember
											.setSelectionType(MemberSelection
													.createMemberSelectionFromExistingMember(member));

									treeItem.setData(pafMember);
									
									//add member tag data key to data object.  This tells program is a member tag
									if ( tuple.isMemberTag() ) {
										
										treeItem.setData(MEMBER_TAG_KEY, tuple.getMemberDefs()[0]);
										
									}

								}

								// set parent to last tree item
								parent = treeItem;

							} else {

								// parent = child itemcount - 1
								parent = parent.getItem(itemCount - 1);
							}

						}

						// increment
						index++;

						// if index mod of num of dims == 0, then reset index
						// and parent to null
						if (index % numberOfDims == 0) {
							index = 0;
							parent = null;
						}

					}

				}

			}

			// if childnren exist, expand tree items.
			if (tupleTree.getItemCount() > 0) {
				for (TreeItem rootItem : tupleTree.getItems()) {

					expandTreeItems(rootItem);

				}
			}

		}

	}

	private void addUserSelectionToTreeItem(TreeItem treeItem, String member,
			int dimIndex, int axis) {

		String[] axisDims = null;

		// if col axis, get col dimensions
		if (PafAxis.COL == axis) {
			axisDims = colDims;
			// if row axis, get row dimensions
		} else if (PafAxis.ROW == axis) {
			axisDims = rowDims;
		} else {
			logger.error("Wrong Axis selected");
			return;
		}

		// used dim index to get dimension name
		String dimension = axisDims[dimIndex];

		// get user selections [] from the user selection map based on dimension
		PafUserSelection[] pafUserSelectionsForDim = userSelectionsMap
				.get(dimension);

		// will be populated if found
		PafUserSelection foundUserSelection = null;

		// if user selection array is not null
		if (pafUserSelectionsForDim != null) {

			// parse out the user selection member, would be ID field on user
			// selection
			String userSelectionMember = UserSelectionUtil
					.getUserSelectionMember(member);

			// for each user selection in [], try to find one that matches the
			// member
			for (PafUserSelection pafUserSelection : pafUserSelectionsForDim) {

				// if id of user selection equals user selection member, set
				// found user selection
				if (pafUserSelection.getId().equals(userSelectionMember)) {

					foundUserSelection = pafUserSelection;

					break;
				}

			}

		}

		// if user selection is found, set up data item with the attribute of
		// multiples, else set to false
		if (foundUserSelection != null) {
			treeItem.setData(USER_SELECTION_MULTIPLE, foundUserSelection
					.isMultiples());
		} else {
			treeItem.setData(USER_SELECTION_MULTIPLE, false);
		}

	}

	private PafTuple populatePafTuple(ViewTuple viewTuple) {

		PafTuple pafTuple = new PafTuple();

		PafMemberHeader pafHeader = new PafMemberHeader();

		pafHeader.setParentFirst(viewTuple.getParentFirst());
		pafHeader.setBorder(viewTuple.getHeaderBorder());
		pafHeader.setGlobalStyle(viewTuple.getHeaderGlobalStyleName());
		pafHeader.setRowHeight(viewTuple.getRowHeight());
		pafHeader.setColumnWidth(viewTuple.getColumnWidth());

		pafTuple.setHeader(pafHeader);

		PafMemberData pafData = new PafMemberData();

		pafData.setBorder(viewTuple.getDataBorder());
		pafData.setPlannable(viewTuple.getPlannable());
		pafData.setGlobalStyle(viewTuple.getDataGlobalStyleName());
		pafData.setOverrideNumericFormat(viewTuple
				.getNumberFormatOverrideLabel());
		
		if ( viewTuple.isMemberTag() ) {
			
			pafData.setMemberTagEditable(viewTuple.getIsMemberTagEditable());
						
		}

		pafTuple.setMemberTag(viewTuple.isMemberTag());
		pafTuple.setData(pafData);

		String[] memberDefs = viewTuple.getMemberDefs();
		ArrayList<PafMember> pafMembers = new ArrayList<PafMember>();

		if (memberDefs != null) {

			PafMember pafMember = null;

			for (String memberDef : memberDefs) {

				pafMember = new PafMember();
				
				//memberDef = convertLevelToIDesc(memberDef);
				
				pafMember.setName(memberDef);
				
				pafMember.setSelectionType(MemberSelection
						.createMemberSelectionFromExistingMember(memberDef));

				pafMembers.add(pafMember);

			}

		}

		pafTuple.setMemberAr(pafMembers.toArray(new PafMember[0]));

		return pafTuple;

	}


	private boolean isUserSelectionMember(String memberDef) {

		return (memberDef.contains(PafBaseConstants.USER_SEL_TAG));

	}

	private void expandTreeItems(TreeItem item) {

		if (item != null) {

			item.setExpanded(true);

			TreeItem[] items = item.getItems();

			if (items != null && items.length != 0) {

				for (TreeItem treeItem : items) {
					expandTreeItems(treeItem);
				}

			}

		}
	}


	/**
	 * 	
	 *
	 * @param selectionType
	 */
	private void updateMemberSelection(MemberSelectionType selectionType) {

		if (updateMemberSelection) {

			logger.debug(selectionType);

			Tree selectedTree = getTabFolderTree();

			TreeItem[] selectedItems = selectedTree.getSelection();

			if (selectedItems.length > 0) {

				for (TreeItem selectedItem : selectedItems) {

					Object dataItem = selectedItem.getData();

					PafMember pafMember = getPafMember(dataItem);

					MemberSelection memberSelection = pafMember
							.getSelectionType();

					if ( selectionType.equals(MemberSelectionType.Level)) {
						
						if ( memberSelection.getLevelGenType() != null 
								&& memberSelection.getLevelGenType().equals(LevelGenerationType.Generation)) {
							
							selectionType = MemberSelectionType.Generation;
							
						}
						
					}
					
					memberSelection.setSelectionType(selectionType);

					if ( MemberSelection.isKindOfDescendantLevelOrGenSelectionType(selectionType)) {
						
						enableLevelGenerationControls(true);
						
					} else {
						
						enableLevelGenerationControls(false);
						
					}
							
										
					selectedItem.setText(memberSelection
							.getMemberWithSelection());
					
					
					if (dataItem instanceof PafMember) {

						pafMember.setSelectionType(memberSelection);

						selectedItem.setData(pafMember);

					} else if (dataItem instanceof PafTuple) {

						PafTuple pafTuple = (PafTuple) dataItem;

						PafMember[] pafMembers = pafTuple.getMemberAr();

						pafMembers[pafMembers.length - 1] = pafMember;

						selectedItem.setData(pafTuple);
					}

				}
			}
		} 
		
	}
	

	/**	  
	 *	
	 *	Performs border logic based on which check is selected.
	 *
	 * @param all
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 */
	private void performBorderLogic(Button all, Button left, Button right,
			Button top, Button bottom) {

		if (all.getSelection()) {

			left.setSelection(false);
			right.setSelection(false);
			top.setSelection(false);
			bottom.setSelection(false);

			left.setEnabled(false);
			right.setEnabled(false);
			top.setEnabled(false);
			bottom.setEnabled(false);

		} else {
			left.setEnabled(true);
			right.setEnabled(true);
			top.setEnabled(true);
			bottom.setEnabled(true);
		}

	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Assign Members");
	}

	private void verifyOnlyNumeric(VerifyEvent event) {

		// only allow positive numbers
		if (!event.text.matches("[0123456789]*")) {
			event.doit = false;
		}

	}
	
	private int getTreeItemLevelNdx(TreeItem treeItem) {
		
		int treeItemLevelNdx = 0;
		
		if ( treeItem != null ) {
			
			TreeItem parentItem = treeItem;
			
			while (parentItem != null) {
				
				parentItem = parentItem.getParentItem();
				
				treeItemLevelNdx++;
				
			}
			
		}
		
		if ( treeItemLevelNdx > 0 ) {
			treeItemLevelNdx--;
		}
		
		return treeItemLevelNdx;
		
	}


	/**
	 * 
	 *	Adds level generation control listeners to widgets
	 *
	 */
	private void addLevelGenerationControlListeners() {

		logger.info("Adding Level Generation Control Listeners");
		
		levelGenDropDown.addSelectionListener(getLevelGenerationComboSelectionAdapter());
	
		levelGenSpinner.addModifyListener(getLevelGenerationSpinnerModifyListener());
		
	}
	
	/**
	 * 
	 *	Removes level generation control listeners to widgets
	 *
	 */
	private void removeLevelGenerationControlListeners() {
		
		logger.info("Removing Level Generation Control Listeners");
		
		levelGenDropDown.removeSelectionListener(getLevelGenerationComboSelectionAdapter());
	
		levelGenSpinner.removeModifyListener(getLevelGenerationSpinnerModifyListener());
						
	}
	
	/**
	 * 
	 *	Gets the level generation combo selection adapter.  
	 *
	 */
	private SelectionAdapter getLevelGenerationComboSelectionAdapter() {
		
		//if null, create once and then user again next time
		if ( levelGenerationComboSelectionAdapter == null ) {
			
			levelGenerationComboSelectionAdapter = new SelectionAdapter() {

				public void widgetSelected(SelectionEvent arg0) {
					
					levelGenComboSelectionEvent();				
					
				}
			};
			
		}		
		
		return levelGenerationComboSelectionAdapter;
		
	}

	/**
	 * 
	 *	Gets the level generation spinner modify listener.  
	 *
	 */
	private ModifyListener getLevelGenerationSpinnerModifyListener() {

		//if null, create once and then user again next time
		if ( levelGenerationSpinnerModifyListener == null ) {
			
			levelGenerationSpinnerModifyListener = new ModifyListener() {

				public void modifyText(ModifyEvent arg0) {

					
					levelGenSpinnerModifyEvent();
										
				}
				
			};
			
		}
		
		return levelGenerationSpinnerModifyListener;
	}


	/** 
	 *	Populates the level generation spinner with dynamic values from existing members or level spinner.
	 *
	 * @param pafMemberSelection		Current member selection
	 * @param populateFromMemberSelection  
	 */
	private void populateLevelGenerationSpinnerValues(MemberSelection pafMemberSelection, boolean populateFromMemberSelection) {

		//get current member
		String currentMember = pafMemberSelection.getMembers()[0];
		
		//get current dimension name
		String currentDimensionName = getDimensionNameFromTreeItem(getTabFolderTree().getSelection()[0]);

		//get members selection type
		MemberSelectionType memberSelectionType = pafMemberSelection.getSelectionType();
		
		//get the max depth of tree for current dimension
		int maxTreeDepth = maxTreeDepthMap.get(currentDimensionName);
		
		//default min is 0
		int minLevelGenSpinnerLength = 0;
		
		//default max is max depth of tree for this dimension
		int maxLevelGenSpinnerLength = maxTreeDepth; 
		
		//first check for generation becasue by default, then levelGenType is Level
		if (MemberSelection.isKindOfGenerationSelectionType(memberSelectionType) || pafMemberSelection.getLevelGenType().equals(LevelGenerationType.Generation)) {

			//if current member is not UOW_ROOT and is in the outline cache, set min spinner value to the calculated max based on member selected
			if ( ! currentMember.equals(PafBaseConstants.UOW_ROOT) && isMemberInOutline(currentDimensionName, currentMember) ) {

				minLevelGenSpinnerLength = getMemberGeneration(currentDimensionName, currentMember);
				
			} else {			
				
				minLevelGenSpinnerLength = 1;				
				
			}
			
			maxLevelGenSpinnerLength = maxTreeDepth+1;
			
		} else if ( MemberSelection.isKindOfLevelSelectionType(memberSelectionType) || pafMemberSelection.getLevelGenType().equals(LevelGenerationType.Level)) {
			
			//if current member is not UOW_ROOT and is in the outline cache, set max spinner value to the calculated min based on member selected
			if ( ! currentMember.equals(PafBaseConstants.UOW_ROOT) && isMemberInOutline(currentDimensionName, currentMember)) {
				
				maxLevelGenSpinnerLength = getMemberLevel(currentDimensionName, currentMember);
								
			} 
			
		} 
		
		//remove level generation listeners
		removeLevelGenerationControlListeners();
		
		//get old min and max spinner values and log
		int oldMinSpinnerValue = levelGenSpinner.getMinimum();
		int oldMaxSpinnerValue = levelGenSpinner.getMaximum();
		
		logger.info("Before spinner min: " + oldMinSpinnerValue);
		logger.info("Before spinner max: " + oldMaxSpinnerValue);
		logger.info("Before spinner value: " + levelGenSpinner.getSelection());
		
		logger.info("Setting spinner min: " + minLevelGenSpinnerLength);
		logger.info("Setting spinner max: " + maxLevelGenSpinnerLength);
		
		//set min or max first based on if new min is > or equal to old max value.  Spinner can't have same min and max at any given time.
		if ( minLevelGenSpinnerLength >= oldMaxSpinnerValue) {
			
			levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
			levelGenSpinner.setMinimum(minLevelGenSpinnerLength);			
			
		} else {
		
			levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
			levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
			
		}
			
		int currentLevelGenSpinnerNumber = 0;
		
		//either popualte from member selection or get value from spinner
		if ( populateFromMemberSelection ) {
			
			currentLevelGenSpinnerNumber = pafMemberSelection.getLevelGenNumber();
			
		} else {
			
			currentLevelGenSpinnerNumber = levelGenSpinner.getSelection();
		}
		
	
		//readjust current level spinner number based on these conditions.
		if (currentLevelGenSpinnerNumber < minLevelGenSpinnerLength) {
			currentLevelGenSpinnerNumber = minLevelGenSpinnerLength;
		} else if (currentLevelGenSpinnerNumber > maxLevelGenSpinnerLength) {
			currentLevelGenSpinnerNumber = maxLevelGenSpinnerLength;
		}

		//set level gen spinner with new current value
		levelGenSpinner.setSelection(currentLevelGenSpinnerNumber);

		//log
		logger.info("After spinner min: " + levelGenSpinner.getMinimum());
		logger.info("After spinner max: " + levelGenSpinner.getMaximum());
		logger.info("After spinner value: " + levelGenSpinner.getSelection());

		//set level gen number on member selection
		pafMemberSelection.setLevelGenNumber(currentLevelGenSpinnerNumber);
		
		//add level gen listeners again
		addLevelGenerationControlListeners();
		
	}

	/**
	 * 
	 *	Populates the level and generation fields
	 *
	 */
	protected void populateLevelGenerationFields() {
		
		Tree selectedTree = getTabFolderTree();

		TreeItem[] selectedItems = selectedTree.getSelection();

		//only do if one item is selected
		if (selectedItems.length == 1 ) {
			
			TreeItem selectedItem = selectedItems[0];
		
			Object dataItem = selectedItem.getData();
			 
			PafMember pafMember = getPafMember(dataItem);
			 
			MemberSelection pafMemberSelection = pafMember .getSelectionType();
			
			MemberSelectionType selectionType = pafMemberSelection.getSelectionType();
									
			//if selection type is kind of desc, level or gen
			if ( MemberSelection.isKindOfDescendantLevelOrGenSelectionType(selectionType)) {
				
				//get level gen type
				LevelGenerationType memberSelectionLevelGen = pafMemberSelection.getLevelGenType();
				
				//if null, set to level as default
				if ( memberSelectionLevelGen == null ) {
					
					
					if ( ControlUtil.isItemInCombo(levelGenDropDown, LevelGenerationType.Bottom.toString()) ) {
						memberSelectionLevelGen = LevelGenerationType.Bottom;
					} else {
						memberSelectionLevelGen = LevelGenerationType.Level;
					}
					
				//if bottom but bottom doesn't but bottom not in dropdown, defualt as level
				} else if ( memberSelectionLevelGen.equals(LevelGenerationType.Bottom) && 
							! ControlUtil.isItemInCombo(levelGenDropDown, LevelGenerationType.Bottom.toString())) {
					
						memberSelectionLevelGen = LevelGenerationType.Level;
						
						//TODO: (TTN-1237) figure out why this is bottom, and maybe it should be.  reguards to michaels problem
						//pafMemberSelection.setLevelGenType(memberSelectionLevelGen);
						
						
				}
				
				if ( memberSelectionLevelGen != null && memberSelectionLevelGen.equals(LevelGenerationType.Bottom)) {
					
					enableLevelGenSpinnerControl(false);
					
				} else {
					
					if ( levelGenDropDown.isEnabled()) {
						
						enableLevelGenSpinnerControl(true);

					}
					
					//popualte level gen spinner values from current member selection
					populateLevelGenerationSpinnerValues(pafMemberSelection, true);
					
				}
												
				//remove listeners
				removeLevelGenerationControlListeners();

				if ( ControlUtil.isItemInCombo(levelGenDropDown, memberSelectionLevelGen.toString()) ) {
					//populate level gen dropdown with member selection's level or gen
					levelGenDropDown.setText(memberSelectionLevelGen.toString());
				} else {
					levelGenDropDown.select(0);
				}
				
				//add listeners
				addLevelGenerationControlListeners();
				
				
			} else {

				//remove listeners
				removeLevelGenerationControlListeners();
				
				//select 1st value in dropdown
				levelGenDropDown.select(0);
				
				//get current dim name
				String currentDimensionName = getDimensionNameFromTreeItem(getTabFolderTree().getSelection()[0]);
				
				//get max tree depth for current dimension
				int maxTreeDepth = maxTreeDepthMap.get(currentDimensionName);
			
				//set min as 0, max as max depth of tree and set spinner value to 0
				levelGenSpinner.setMinimum(0);
				levelGenSpinner.setMaximum(maxTreeDepth);
				levelGenSpinner.setSelection(0);
				
				//add listeners				
				addLevelGenerationControlListeners();	
				
			}
			
			
			
		}
		
	}

	/**
	 * 
	 *	Called when the level/gen combo selection changed event gets fired
	 *
	 */
	protected void levelGenComboSelectionEvent() {

		Tree selectedTree = getTabFolderTree();

		TreeItem[] selectedItems = selectedTree.getSelection();

		if (selectedItems.length == 1 ) {
		
			TreeItem selectedItem = selectedItems[0]; 
			
			Object dataItem = selectedItem.getData();
			 
			PafMember pafMember = getPafMember(dataItem);
			 
			MemberSelection pafMemberSelection = pafMember.getSelectionType();
			
			String newLevelGenSelection = levelGenDropDown.getText();
			
			LevelGenerationType newLevelGenType = null;
			
			if ( newLevelGenSelection.equals(LevelGenerationType.Level.toString())) {
				
				newLevelGenType = LevelGenerationType.Level;

				//if current selection type is level or generation, set member selection to level
				if ( MemberSelection.isKindOfLevelSelectionType(pafMemberSelection.getSelectionType()) || MemberSelection.isKindOfGenerationSelectionType(pafMemberSelection.getSelectionType())) { 
					
					pafMemberSelection.setSelectionType(MemberSelectionType.Level);
					
				}
				
			} else if ( newLevelGenSelection.equals(LevelGenerationType.Generation.toString()) ) {
				
				newLevelGenType = LevelGenerationType.Generation;

				//if current selection type is level or generation, set member selection to generation
				if ( MemberSelection.isKindOfLevelSelectionType(pafMemberSelection.getSelectionType()) || MemberSelection.isKindOfGenerationSelectionType(pafMemberSelection.getSelectionType())) {				
					
					pafMemberSelection.setSelectionType(MemberSelectionType.Generation);
					
				}
				
				
			} else {
				
				newLevelGenType = LevelGenerationType.Bottom;
				
			}
		
			//set level gen type
			pafMemberSelection.setLevelGenType(newLevelGenType);
			
			if ( newLevelGenType.equals(LevelGenerationType.Bottom)) {

				enableLevelGenSpinnerControl(false);
				
			} else {
				
				enableLevelGenSpinnerControl(true);
				
				//populate level gen spinner value from spinner itself
				populateLevelGenerationSpinnerValues(pafMemberSelection, false);
				
			}
			
			//update tree item text
			selectedItem.setText(pafMemberSelection
					.getMemberWithSelection());
			
		}
		
	}
	
	/**
	 * 
	 *	Called when the level / gen spinner throws a modfiy event.
	 *
	 */
	protected void levelGenSpinnerModifyEvent() {
		
		logger.info("Level Generation Spinner has been modified: " + levelGenSpinner.getSelection());
				
		Tree selectedTree = getTabFolderTree();

		TreeItem[] selectedItems = selectedTree.getSelection();

		if (selectedItems.length == 1 ) {
					
			TreeItem selectedItem = selectedItems[0]; 
		
			//get data item
			Object dataItem = selectedItem.getData();
			 
			//get paf member from data item
			PafMember pafMember = getPafMember(dataItem);
			 
			//get member selection
			MemberSelection pafMemberSelection = pafMember .getSelectionType();
			
			//get new level gen number from spinner
			int newLevelGenNumber = levelGenSpinner.getSelection();
			
			//set the level / gen number on member selection
			pafMemberSelection.setLevelGenNumber(newLevelGenNumber);
			
			//update text for tree item.
			selectedItem.setText(pafMemberSelection
					.getMemberWithSelection());
			
		}
		
		
	}
	
	/**
	 * Gets the level member for a particular dimension.
	 *
	 * @param dimensionName Dimension name used to search for member.
	 * @param memberName	Member to search for.
	 * @return dimension member or null if not found
	 */
	private int getMemberLevel(String dimensionName, String memberName) {

		PafSimpleDimMember pafSimpleDimMember = getCurrentPafSimpleDimMember(dimensionName, memberName);
		
		return pafSimpleDimMember.getPafSimpleDimMemberProps().getLevelNumber();
	}
	
	/**
	 * Gets the generation member for a particular dimension.
	 *
	 * @param dimensionName Dimension name used to search for member.
	 * @param memberName	Member to search for.
	 * @return dimension member or null if not found
	 */
	private int getMemberGeneration(String dimensionName, String memberName) {

		PafSimpleDimMember pafSimpleDimMember = getCurrentPafSimpleDimMember(dimensionName, memberName);
		
		return pafSimpleDimMember.getPafSimpleDimMemberProps().getGenerationNumber();
	}

	/**
	 * Gets the current paf simple dim member from the dimension cache	
	 *
	 * @param dimensionName Dimension name used to search for member.
	 * @param memberName	Member to search for.
	 * @return dimension member or null if not found
	 */
	private PafSimpleDimMember getCurrentPafSimpleDimMember(String dimensionName, String memberName) {
				
		//get dimension member map
		Map<String, PafSimpleDimMember> pafSimpleDimMemberMap = pafSimpleDimMemberDimensionMap.get(dimensionName);

		//if map is null, return null
		if ( pafSimpleDimMemberMap == null ) {
			
			return null;
			
		}
		
		//return member
		return pafSimpleDimMemberMap.get(memberName);

	}
	
	/**
	 * Checks to see if members is in outline for that dimension	
	 *
	 * @param dimensionName Dimension to check for member.
	 * @param memberName	Member to search for in outline cache
	 * @return	true if member is in outline, false if not
	 */
	private boolean isMemberInOutline(String dimensionName, String memberName) {
		
		//get dimension member map
		Map<String, PafSimpleDimMember> pafSimpleDimMemberMap = pafSimpleDimMemberDimensionMap.get(dimensionName);
		
		if ( pafSimpleDimMemberMap != null ) {
		
			//try to get member from dimension map
			PafSimpleDimMember pafSimpleDimMember = pafSimpleDimMemberMap.get(memberName);

			//if member exists, return true
			if ( pafSimpleDimMember != null ) {
			
				return true;
				
			}			
			
		}
		
		return false;
		
	}
	
	/**
	 * 
	 *	Disable all the Children, Descendants, Level and Generation member selections.
	 *
	 */
	private void disableAllChildrenDescLevelAndGenSelectionsControls() {

		justChildrenOfSelectionButton.setEnabled(false);
		selectionAndChildrenButton.setEnabled(false);
		justDescendantsOfSelectionButton.setEnabled(false);
		selectionAndDecendantsButton.setEnabled(false);
		justLevelOrGenButton.setEnabled(false);
		
	}
	
	/** 
	 *	Configure the level / generation controls for kinds of descendants
	 *
	 * @param memberSelection Current member selection
	 */
	private void configureLevelGenerationControlsForKindsOfDescendants(MemberSelection memberSelection) {

		MemberSelectionType memberSelectionType = memberSelection.getSelectionType();
		
		//if member selection is kind of desc, level or generation
		if ( MemberSelection.isKindOfDescendantLevelOrGenSelectionType( memberSelectionType )){
			
			
			if ( MemberSelection.isKindOfLevelSelectionType(memberSelectionType) 
				|| MemberSelection.isKindOfGenerationSelectionType(memberSelectionType)) {
				
				levelGenDropDown.setItems(levelGenDropdownValues);
				
			} else {
				levelGenDropDown.setItems(levelGenBottomDropdownValues);
			}
			
		
			//get level / gen type
			LevelGenerationType memberSelectionLevelGen = memberSelection.getLevelGenType();
			
			//if null, set to level by default and set member selection to level (level/gen) type
			if ( memberSelectionLevelGen == null ) {
				
				if ( ControlUtil.isItemInCombo(levelGenDropDown, LevelGenerationType.Bottom.toString()) ) {
					memberSelectionLevelGen = LevelGenerationType.Bottom;
				} else {
					memberSelectionLevelGen = LevelGenerationType.Level;
				}
			
				memberSelection.setLevelGenType(memberSelectionLevelGen);
					
			} 
			
			if ( memberSelectionLevelGen != null && memberSelectionLevelGen.equals(LevelGenerationType.Bottom)) {
				
				enableLevelGenSpinnerControl(false);
				
			} else {			
					
				enableLevelGenSpinnerControl(true);
							
			}
			
			//populate spinner values based on current member selection
			populateLevelGenerationSpinnerValues(memberSelection, true);
			
			//remove level gen control listerenrs
			removeLevelGenerationControlListeners();
			
			//if member selection typ is kind of descendant, select correct level/gen dropdown
			if ( MemberSelection.isKindOfDescendantSelectionType(memberSelectionType)) {
				
				levelGenDropDown.setText(memberSelectionLevelGen.toString());	

		    //if member selection typ is kind of level, select level in dropdown
			} else if ( MemberSelection.isKindOfLevelSelectionType(memberSelectionType) ) {
				
				levelGenDropDown.setText(LevelGenerationType.Level.toString());
				
			//if member selection typ is kind of generation, select generation in dropdown				
			} else if ( MemberSelection.isKindOfGenerationSelectionType(memberSelectionType)  ) {
				
				levelGenDropDown.setText(LevelGenerationType.Generation.toString());
				
			}		
			
			//add listeners again
			addLevelGenerationControlListeners();
	
			//get current tree selection ( page, row, col )
			Tree selectedTree = getTabFolderTree();

			//get selected items from tree
			TreeItem[] selectedItems = selectedTree.getSelection();

			//if selected items is only 1, get selected item and repopulate text with member selection
			if (selectedItems.length == 1 ) {
			
				TreeItem selectedItem = selectedItems[0];
				
				selectedItem.setText(memberSelection.getMemberWithSelection());
				
			}
		}
	}

	private void enableLevelGenerationControls(boolean enable) {
		
		enableLevelGenDropdownControl(enable);
		enableLevelGenSpinnerControl(enable);
		
	}
	
	private void enableLevelGenDropdownControl(boolean enable) {
		
		levelGenDropDown.setEnabled(enable);
		
	}
	
	private void enableLevelGenSpinnerControl(boolean enable) {
		
		levelGenSpinner.setEnabled(enable);
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ButtonId;
import com.pace.admin.global.enums.FormState;
import com.pace.admin.global.enums.ItemState;
import com.pace.admin.global.model.managers.DynamicMembersModelManager;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.ViewSectionModelManager;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.DynamicMemberDef;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafPlannerConfig;
import com.pace.base.data.ExpOperation;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;

/**
 * 
 * Dynamic Members Dialog
 * 
 * 1st phase is to only include the version dimension.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DynamicMembersDialog extends Dialog {
	
	//logger
	private static Logger logger = Logger.getLogger(DynamicMembersDialog.class);
	
	//TreeItem data keys
	private static final String DIMENSION_KEY = "Dimension";
	private static final String INITIAL_VALUE_KEY = "InitialValue";
			
	//form widgets
	private Combo dimensionCombo;
	private Text dynamicMemberText;
	private Tree dynamicMembersByDimensionTree;
	
	//buttons
	private Button newButton;
	private Button deleteButton;
	private Button saveButton;
	private Button cancelButton;
	private Button upArrowButton;
	private Button downArrowButton;

	
	//current project
	private IProject project;
	
	//model managers
	private DynamicMembersModelManager dynamicMembersModelManager;
	private ViewSectionModelManager viewSectionModelManager;
	private PafPlannerConfigModelManager plannerConfigModelManager;
			
	//map to hold the tree item.  key = <dimension name>|<initial treeItem text>, value is tree item 
	private Map<String, TreeItem> treeItemMap;	
	
	//map to hold state of tree items.  key = <dimension name>|<initial treeItem text>, value item state
	private Map<String, ItemState> treeItemStateMap;

	//modify listener for the dynamic member text box
	private ModifyListener dynamicMemberModifyListener;

	//current state of form
	private FormState currentFormState;

	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public DynamicMembersDialog(Shell parentShell, IProject project) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.project = project;
		
		//if project is not null, create model managers
		if ( project != null ) {
			
			dynamicMembersModelManager = new DynamicMembersModelManager(project);
			viewSectionModelManager = new ViewSectionModelManager(project);
			plannerConfigModelManager = new PafPlannerConfigModelManager(project);
			
		}
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		gridLayout.marginRight = 25;
		gridLayout.marginLeft = 25;
		gridLayout.marginTop = 20;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);

		final Label dynamicMembersByLabel = new Label(container, SWT.NONE);
		dynamicMembersByLabel.setText("Dynamic Members by Dimension:");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		dynamicMembersByDimensionTree = new Tree(container, SWT.MULTI | SWT.BORDER);
		
		final GridData gridData = new GridData(SWT.LEFT, SWT.FILL, false, true);
		gridData.widthHint = 220;
		dynamicMembersByDimensionTree.setLayoutData(gridData);

		final Composite composite_3 = new Composite(container, SWT.NONE);
		composite_3.setLayout(new GridLayout());

		upArrowButton = new Button(composite_3, SWT.ARROW);
		downArrowButton = new Button(composite_3, SWT.ARROW | SWT.DOWN);
		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginBottom = 20;
		gridLayout_1.marginTop = 20;
		gridLayout_1.marginLeft = 20;
		composite.setLayout(gridLayout_1);

		final Composite composite_2 = new Composite(composite, SWT.NONE);
		final GridData gridData_4 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_4.heightHint = 100;
		composite_2.setLayoutData(gridData_4);
		composite_2.setLayout(new GridLayout());

		final Label dynamicMemberLabel = new Label(composite_2, SWT.NONE);
		dynamicMemberLabel.setText("Dynamic Member");

		dynamicMemberText = new Text(composite_2, SWT.BORDER);
		dynamicMemberText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label dimensionLabel = new Label(composite_2, SWT.NONE);
		dimensionLabel.setText("Dimension");

		dimensionCombo = new Combo(composite_2, SWT.READ_ONLY);
		dimensionCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Composite composite_1_1 = new Composite(composite, SWT.NONE);
		final GridData gridData_3 = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gridData_3.heightHint = 33;
		gridData_3.widthHint = 125;
		composite_1_1.setLayoutData(gridData_3);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		gridLayout_3.marginWidth = 0;
		gridLayout_3.makeColumnsEqualWidth = true;
		composite_1_1.setLayout(gridLayout_3);

		saveButton = new Button(composite_1_1, SWT.NONE);
		saveButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		saveButton.setText("Save");
		saveButton.setVisible(false);

		cancelButton = new Button(composite_1_1, SWT.NONE);
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		cancelButton.setText("Cancel");
		cancelButton.setVisible(false);
		
		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(125, 33));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginWidth = 0;
		gridLayout_2.numColumns = 2;
		gridLayout_2.makeColumnsEqualWidth = true;
		composite_1.setLayout(gridLayout_2);

		newButton = new Button(composite_1, SWT.NONE);
		
		newButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		newButton.setText("New");

		deleteButton = new Button(composite_1, SWT.NONE);
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		deleteButton.setText("Delete");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
		//initialize dialog with data
		initalizeDialogWithData();

		//setup listeners
		setupListeners();
		
		//
		return container;
	}
	
	/**
	 * 
	 * Sets up the tree listeners and calls the button listener setup
	 *
	 */
	private void setupListeners() {

		dynamicMembersByDimensionTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				currentFormState = FormState.Edit;
				
				populateForm();
			}
		});
		
		setupButtonListeners();
		
	}

	/**
	 * 
	 * Enables the form listeners
	 *
	 * @param enable true to enable (add) form listeners, false to disable (remove)
	 */
	private void enableFormListeners(boolean enable) {
		
		//if enabled, add form listener
		if ( enable ) {
		
			dynamicMemberText.addModifyListener(getDynamicMemberModifyListener());
			
		//if not enable, remove form listener
		} else {
			
			dynamicMemberText.removeModifyListener(getDynamicMemberModifyListener());
			
		}

		
	}

	/**
	 * 
	 * Gets the dynamic member modify listener
	 *
	 * @return
	 */
	private ModifyListener getDynamicMemberModifyListener() {
		
		//if null, create dynamic member modify listener 
		if ( dynamicMemberModifyListener == null ) {

			//create new modify listener
			dynamicMemberModifyListener = new ModifyListener() {
				
				/**
				 * fired when modify text
				 */
				public void modifyText(ModifyEvent arg0) {
					
					//
					dynamicMembersByDimensionTree.setEnabled(false);
					
					//displays form buttons (save & cancel)
					displayFormButtons(true);
					
					//enable save button
					saveButton.setEnabled(true);					
					
				}
			};
		}
				
		return dynamicMemberModifyListener;
	}

	/**
	 * 
	 * Displays or hids the save and cancel button
	 *
	 * @param displayButtons true - displays buttons, false - hides button
	 */
	protected void displayFormButtons(boolean displayButtons) {

		saveButton.setVisible(displayButtons);
		cancelButton.setVisible(displayButtons);
		
		
	}

	/**
	 * 
	 *  Populates form with selection.
	 *
	 */
	protected void populateForm() {

		TreeItem[] selections = dynamicMembersByDimensionTree.getSelection();
		
		if ( selections != null && selections.length == 1 ) {
			
			TreeItem currentSelection = selections[0];
			
			//get current dimension
			String selectedItemDimension = (String) currentSelection.getData(DIMENSION_KEY);
			
			//get initial value
			String initialValue = (String) currentSelection.getData(INITIAL_VALUE_KEY);
			
			//clear current contents of form
			clearForm();
			
			//if dimension is not null, initial value is not null, and dimension doesn't equal the intial (meaning dim item)
			if ( selectedItemDimension != null && initialValue != null && ! selectedItemDimension.equals(initialValue) ) {
				
				if ( currentSelection.getText().equals(PafBaseConstants.PLAN_VERSION )) {

					//disable the form controls
					enableFormControls(false);
					
				} else {
					
					//enable the form controls
					enableFormControls(true);
				}
				
				//enable the form listeners
				enableFormListeners(false);
				
				//set the text
				dynamicMemberText.setText(currentSelection.getText());
				dimensionCombo.setText(selectedItemDimension);
				
				//enable form listeners
				enableFormListeners(true);				
				
			} else {
				
				//disable form controls
				enableFormControls(false);
			}
			
		} else if ( selections.length == 0 ) {
			
			//clear form
			clearForm();
			
			//disable form controls
			enableFormControls(false);
									
		}
		
		
	}

	/**
	 * 
	 *  Initializes the dialog with data.  Populates the tree.
	 *
	 */
	private void initalizeDialogWithData() {

		if ( dynamicMembersModelManager != null ) {
			
			//create tree item map
			treeItemMap = new HashMap<String, TreeItem>();
			
			//create tree item state map, holds tree item state
			treeItemStateMap = new TreeMap<String, ItemState>(String.CASE_INSENSITIVE_ORDER);
			
			//get dimension keys
			String[] dimensionKeys = dynamicMembersModelManager.getKeys();
			
			//if not null
			if ( dimensionKeys != null ) {
				
				//loop through dimensions
				for (String dimensionKey : dimensionKeys ) {
					
					//create dimension tree item
					TreeItem rootDimensionItem = new TreeItem(dynamicMembersByDimensionTree, SWT.None);
					rootDimensionItem.setText(dimensionKey);
					rootDimensionItem.setData(DIMENSION_KEY, dimensionKey);
					rootDimensionItem.setData(INITIAL_VALUE_KEY, dimensionKey);
											
					//get the dynamic member def
					DynamicMemberDef dynamicMemberDef = (DynamicMemberDef) dynamicMembersModelManager.getItem(dimensionKey);
					
					if ( dynamicMemberDef != null ) {
					
						//get member specs
						String[] memberSpecs = dynamicMemberDef.getMemberSpecs();
						
						//if not null
						if ( memberSpecs != null ) {
							
							//loop through specs and create tree times
							for (String memberSpec : memberSpecs ) {
								
								//create child tree item using root dimension item as parent
								TreeItem memberSpecItem = new TreeItem(rootDimensionItem, SWT.None);
								memberSpecItem.setText(memberSpec);
								memberSpecItem.setData(DIMENSION_KEY, dimensionKey);
								memberSpecItem.setData(INITIAL_VALUE_KEY, memberSpec);
								
								//add to treeItemMap
								treeItemMap.put(createDimensionMemberKey(dimensionKey, memberSpec), memberSpecItem);
								
								//add to tree item state
								treeItemStateMap.put(createDimensionMemberKey(dimensionKey, memberSpec), ItemState.Edit);
								
							}
							
						}
						
						//expand root dimension itme
						rootDimensionItem.setExpanded(true);
					}
					
					//add root dimension items to tree item map and tree item state map
					treeItemMap.put(createDimensionMemberKey(dimensionKey, dimensionKey), rootDimensionItem);
					treeItemStateMap.put(createDimensionMemberKey(dimensionKey, dimensionKey), ItemState.Edit);
					
				}
				
				//set items drop down with dimension keys
				dimensionCombo.setItems(dimensionKeys);
				
			}
				
			
			
		}
		
		//disable form controls
		enableFormControls(false);

		
	}
	
	/**
	 * 
	 *  Enables/Disables form controls
	 *
	 * @param enabled
	 */
	private void enableFormControls(boolean enabled) {
		
		dynamicMemberText.setEnabled(enabled);
		dimensionCombo.setEnabled(enabled);
		
	}
	
	
		
	/**
	 * Clears the current form.
	 */
	private void clearForm() {

		enableFormListeners(false);
		
		dynamicMemberText.setText("");
		dimensionCombo.setText("");
		
		enableFormListeners(true);
	}
	
	/**
	 * Sets up the listeners for the buttons
	 * 
	 */
	private void setupButtonListeners() {

		// new button listeners
		setupButtonSelectionListener(newButton, ButtonId.New);

		// delete button listeners
		setupButtonSelectionListener(deleteButton, ButtonId.Delete);

		// add button listeners
		setupButtonSelectionListener(saveButton, ButtonId.Save);

		// cancel button listeners
		setupButtonSelectionListener(cancelButton, ButtonId.Cancel);

		// up button listeners
		setupButtonSelectionListener(upArrowButton, ButtonId.Up);

		// down button listeners		
		setupButtonSelectionListener(downArrowButton, ButtonId.Down);
		
	}

	/**
	 * 
	 * Sets up a button selection listeners
	 *
	 * @param button	button to add listener to
	 * @param buttonId	button id of button to add listener to
	 */
	private void setupButtonSelectionListener(Button button,
			final ButtonId buttonId) {

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonSelected(buttonId);
			}

		});

	}

	/**
	 * Used to perfrom an action when a button is pressed
	 * 
	 * @param buttonId
	 *            enum for which button was selected
	 */
	private void buttonSelected(ButtonId buttonId) {

		logger.debug(buttonId + " button clicked");
		
		switch (buttonId) {

		case Save:
		
			if ( ! verifyDynamicMemberInput()  ) {
				return;
			}
			
			switch (currentFormState ) {
			case New: 
					saveButtonNewAction();
					
					break;
			case Edit:
					saveButtonEditAction();
					break;
			}
			
			break;

		case Delete:

			deleteButtonAction();

			break;

		case New:
		
			currentFormState = FormState.New;
			
			newButtonAction();

			break;

		case Cancel:

			cancelButtonAction();
			
			//change state back to edit			
			currentFormState = FormState.Edit;

			break;
		case Up:
			
			upButtonAction();
			break;
		case Down:
			downButtonAction();
			break;
		}
	}

	/**
	 * 
	 * Moves the selected tree item up the parent's child list.
	 *
	 */
	private void upButtonAction() {	
		
		//get selected items
		TreeItem[] selectedTreeItems = dynamicMembersByDimensionTree.getSelection();
		
		//if length is 1, continue
		if ( selectedTreeItems.length == 1 ) {

			TreeItem selectedTreeItem = selectedTreeItems[0]; 
			
			TreeItem parentItem = selectedTreeItem.getParentItem();
			
			int itemBelowNdx = parentItem.indexOf(selectedTreeItem);
			
			if ( itemBelowNdx != 0 ) {
				
				int itemAboveNdx = itemBelowNdx-1;
				
				TreeItem itemAbove = parentItem.getItem(itemAboveNdx);				
				
				TreeItem newItemAbove = new TreeItem(parentItem, SWT.NONE, itemAboveNdx);
				
				//copy into new item
				copyTreeItem(selectedTreeItem, newItemAbove);

				TreeItem newItemBelow = new TreeItem(parentItem, SWT.NONE, itemBelowNdx);

				//copy into new item
				copyTreeItem(itemAbove, newItemBelow);
				
				//dispose old items
				selectedTreeItem.dispose();
				itemAbove.dispose();

				//new item above
				String dimensionAbove = (String) newItemAbove.getData(DIMENSION_KEY);
				String initialValueAbove = (String) newItemAbove.getData(INITIAL_VALUE_KEY);
				
				treeItemMap.put(createDimensionMemberKey(dimensionAbove, initialValueAbove), newItemAbove);
				
				//new item below
				String dimensionBelow = (String) newItemBelow.getData(DIMENSION_KEY);
				String initialValueBelow = (String) newItemBelow.getData(INITIAL_VALUE_KEY);
				
				treeItemMap.put(createDimensionMemberKey(dimensionBelow, initialValueBelow), newItemBelow);

				dynamicMembersByDimensionTree.setSelection(newItemAbove);
				
			}		
			
		}
		
	}

	/**
	 * 
	 * Copies text and data properties from one tree item to another
	 *
	 * @param fromItem
	 * @param toItem
	 */
	private void copyTreeItem(TreeItem fromItem, TreeItem toItem) {

		toItem.setText(fromItem.getText());
		toItem.setData(DIMENSION_KEY, (String) fromItem.getData(DIMENSION_KEY));
		toItem.setData(INITIAL_VALUE_KEY, (String) fromItem.getData(INITIAL_VALUE_KEY));
		
		
	}

	/**
	 * 
	 * Moves the selected tree item down the parent's child list.
	 *
	 */	
	private void downButtonAction() {
		
		TreeItem[] selectedTreeItems = dynamicMembersByDimensionTree.getSelection();
		
		if ( selectedTreeItems.length == 1 ) {

			TreeItem selectedTreeItem = selectedTreeItems[0]; 
			
			TreeItem parentItem = selectedTreeItem.getParentItem();
			
			int itemAboveNdx = parentItem.indexOf(selectedTreeItem);
			
			if ( itemAboveNdx != parentItem.getItemCount() -1 ) {
				
				int itemBelowNdx = itemAboveNdx+1;
				
				TreeItem itemBelow = parentItem.getItem(itemBelowNdx);				
				
				//create new item
				TreeItem newItemAbove = new TreeItem(parentItem, SWT.NONE, itemAboveNdx);
				
				//copy values in
				copyTreeItem(itemBelow, newItemAbove);

				//create new item
				TreeItem newItemBelow = new TreeItem(parentItem, SWT.NONE, itemBelowNdx);

				//copy values in
				copyTreeItem(selectedTreeItem, newItemBelow);
				
				//dispose of items
				selectedTreeItem.dispose();
				itemBelow.dispose();
				
//				new item above
				String dimensionAbove = (String) newItemAbove.getData(DIMENSION_KEY);
				String initialValueAbove = (String) newItemAbove.getData(INITIAL_VALUE_KEY);
				
				treeItemMap.put(createDimensionMemberKey(dimensionAbove, initialValueAbove), newItemAbove);
				
				//new item below
				String dimensionBelow = (String) newItemBelow.getData(DIMENSION_KEY);
				String initialValueBelow = (String) newItemBelow.getData(INITIAL_VALUE_KEY);
				
				treeItemMap.put(createDimensionMemberKey(dimensionBelow, initialValueBelow), newItemBelow);

				dynamicMembersByDimensionTree.setSelection(newItemBelow);
				
			}
		}
		
		
	}
	
	/**
	 * Action called when new button was pressed
	 */
	private void newButtonAction() {

		clearForm();
						
		dynamicMembersByDimensionTree.deselectAll();
		dynamicMembersByDimensionTree.setEnabled(false);
		
		newButton.setEnabled(false);
		deleteButton.setEnabled(false);

		enableFormControls(true);
		displayFormButtons(true);
		
		saveButton.setEnabled(false);
		cancelButton.setEnabled(true);
		
		dynamicMemberText.setText(PafBaseConstants.PLAN_VERSION);
		dimensionCombo.select(0);

	}

	/**
	 * Action called when delete button was pressed
	 */
	private void deleteButtonAction() {

		TreeItem[] selectedTreeItems = dynamicMembersByDimensionTree.getSelection();
		
		//if nothing is selected, return
		if ( selectedTreeItems.length == 0 ) {
			return;
		}
		
		/*Start fix for TTN
		//key = dim name; value = set of tree items
		Map<String, Set<TreeItem>> treeItemsByDim = new HashMap<String, Set<TreeItem>>();
		
		for (TreeItem selectedTreeItem : selectedTreeItems) {
		
			TreeItem parentItem = selectedTreeItem.getParentItem();

			Set<TreeItem> treeItemSet = null;
			
			String dimName = parentItem.getText();
			
			if ( treeItemsByDim.containsKey(dimName)) {
				
				treeItemSet = treeItemsByDim.get(dimName);
				
			}
			
			if ( treeItemSet == null ) {
				
				treeItemSet = new HashSet<TreeItem>();
				
			}
			
			treeItemSet.add(selectedTreeItem);
			
			treeItemsByDim.put(dimName, treeItemSet);
			
		}
		*/
		
		
		//if any items selected is a dimension item, open error dialog
		for (TreeItem selectedTreeItem : selectedTreeItems) {

			//check if member is a dimension or not
			if ( ! isMemberTreeItem(selectedTreeItem)) {
				
				MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "One or more items selected are not dynamic members.  Please unselect and try again.");
				return;
				
			}
			
			if ( selectedTreeItem.getText().equals(PafBaseConstants.PLAN_VERSION) ) {
				
				MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Can not delete member '"+ PafBaseConstants.PLAN_VERSION + "'.");
				return;
				
			}
			
		}
		
		List<String> membersToDelete = new ArrayList<String>();
		Set<String> memberInViewSectionsSet = new LinkedHashSet<String>();
		Set<String> memberInRoleConfigsSet = new LinkedHashSet<String>();
		
		//loop through selected tree items and populate list and sets
		for (TreeItem itemToDelete : selectedTreeItems) {
			
			String orignalValue = (String) itemToDelete.getData(INITIAL_VALUE_KEY);
			String dimensionName = (String) itemToDelete.getData(DIMENSION_KEY);
			
			membersToDelete.add(orignalValue);
			memberInViewSectionsSet.addAll(getViewSectionNamesWithMember(dimensionName, orignalValue));
			memberInRoleConfigsSet.addAll(getRoleConfigurationNamesWithMember(dimensionName, orignalValue));
			
		}
		
		//create confirmation dialog.  dialog displays members to delete and references in view secitions and role configurations
		DynamicMembersDeleteConfirmDialog confimDialog = new DynamicMembersDeleteConfirmDialog(this.getShell(), 
				membersToDelete.toArray(new String[0]), memberInViewSectionsSet.toArray(new String[0]), 
				memberInRoleConfigsSet.toArray(new String[0]));
		
		//if ok was pressed
		if ( confimDialog.open() == IDialogConstants.OK_ID ) {
			
			//clear form
			clearForm();
			
			//disable form controls
			enableFormControls(false);
			
			//hide form buttons
			displayFormButtons(false);
			
			//remove items from maps and dispose
			for (TreeItem itemToDelete : selectedTreeItems) {
				
				String orignalValue = (String) itemToDelete.getData(INITIAL_VALUE_KEY);
				String dimensionName = (String) itemToDelete.getData(DIMENSION_KEY);
				
				if (dimensionName != null && orignalValue != null ) {
				
					treeItemMap.remove(createDimensionMemberKey(dimensionName, orignalValue));
					treeItemStateMap.put(createDimensionMemberKey(dimensionName, orignalValue), ItemState.Delete);
					
				}
							
				itemToDelete.dispose();
				
			}
		
		}
		
		

	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param selectedTreeItem
	 * @return
	 */
	private boolean isMemberTreeItem(TreeItem selectedTreeItem) {
		
		if ( selectedTreeItem != null ) {
			
			String dimensionOfItem = (String) selectedTreeItem.getData(DIMENSION_KEY);
	
			if ( ! dimensionOfItem.equalsIgnoreCase(selectedTreeItem.getText()) ) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}

	/**
	 * Action called when cancel button was pressed
	 */
	private void cancelButtonAction() {

		enableFormControls(false);
		displayFormButtons(false);
		
		newButton.setEnabled(true);
		deleteButton.setEnabled(true);

		clearForm();
		
		dynamicMembersByDimensionTree.setEnabled(true);
		
		if ( dynamicMembersByDimensionTree.getSelectionCount() > 0 ) {
		
			populateForm();
			
		}
		


	}

	/**
	 * 
	 * When Save button is clicked
	 *
	 */
	private void saveButtonAction() {
		
		newButton.setEnabled(true);
		deleteButton.setEnabled(true);
		saveButton.setVisible(false);
		cancelButton.setVisible(false);
		dynamicMembersByDimensionTree.setEnabled(true);
		
	}
	
	/**
	 * Action called when add button was pressed
	 */
	private void saveButtonNewAction() {
				
		String dynamicMember = dynamicMemberText.getText().trim();
		
		String dimensionName = dimensionCombo.getText();
		
		//get parent item
		TreeItem parentItem = treeItemMap.get(createDimensionMemberKey(dimensionName, dimensionName));
		
		if ( parentItem != null ) {
			
			//create key
			String key = createDimensionMemberKey(dimensionName, dynamicMember);
			
			//if item alreay exists
			if ( treeItemStateMap.containsKey(key)) {
				
				//get item state
				ItemState itemState = treeItemStateMap.get(key);
				
				//if edit, open error telling user the dyanmic member already exists.  can't create again
				if ( itemState.equals(ItemState.Edit)) {
				
					MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Dynamic Member '" + dynamicMember + "' exists or initally existed.");
					return;
					
				//if delete, change status to edit to redisplay
				} else if ( itemState.equals(ItemState.Delete ) ) {
					
					treeItemStateMap.put(key, ItemState.Edit);
									
				}
				
			} 
			
			//create new tree item
			TreeItem newItem = new TreeItem(parentItem, SWT.None);
			newItem.setText(dynamicMember);
			newItem.setData(DIMENSION_KEY, dimensionName);
			newItem.setData(INITIAL_VALUE_KEY, dynamicMember);
						
			treeItemMap.put(createDimensionMemberKey(dimensionName, dynamicMember), newItem);
			
			//if new only
			if ( ! treeItemStateMap.containsKey(key) ) {
				treeItemStateMap.put(createDimensionMemberKey(dimensionName, dynamicMember), ItemState.New);
			}
			
			dynamicMembersByDimensionTree.setSelection(newItem);
			
			//change state back to edit
			currentFormState = FormState.Edit;
			
		}
		
		saveButtonAction();
		
	}
	
	/**
	 * 
	 *  Verifies form input
	 *
	 * @return if form input is good or not
	 */
	private boolean verifyDynamicMemberInput() {
		
		//disable form listeners
		enableFormListeners(false);
		
		//trim leading or trailing spaces
		dynamicMemberText.setText(dynamicMemberText.getText().trim());
		
		enableFormListeners(true);
		
		String dynamicMember = dynamicMemberText.getText();
						
		boolean returnVal = false;
		
		//check for spaces
		if ( dynamicMember.contains(" ")) {

			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Dynamic Member '" + dynamicMember + "' can not contain spaces.");
			
		//check for @PLAN_VERSION tag in string
		} else if ( ! dynamicMember.contains(PafBaseConstants.PLAN_VERSION	)) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Dynamic Member '" + dynamicMember + "' must contain '" + PafBaseConstants.PLAN_VERSION + "'.");
									
		} else {
			
			returnVal = true;
			
		}
		
		return returnVal;
	}

	/**
	 * Action called when add button was pressed
	 */
	private void saveButtonEditAction() {
		
		String updatedDynamicMember = dynamicMemberText.getText().trim();
						
		TreeItem currentTreeItem = dynamicMembersByDimensionTree.getSelection()[0];
				
		String dimensionName = (String) currentTreeItem.getData(DIMENSION_KEY);
		
		String initialMember = (String) currentTreeItem.getData(INITIAL_VALUE_KEY);
		
		//if text from textBox doesn't equal the inital dynamic member value
		if ( ! updatedDynamicMember.equals(initialMember) ) {			
			
			String initial_key = createDimensionMemberKey(dimensionName, initialMember);
			String updated_key = createDimensionMemberKey(dimensionName, updatedDynamicMember); 
			
			//if new edit item alreay exists and does not have deleted state
			if ( treeItemStateMap.containsKey(updated_key) && ! treeItemStateMap.get(updated_key).equals(ItemState.Delete)) {
			
				MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Dynamic Member '" + updatedDynamicMember + "' exists or initially existed.");
				return;
/*
			//if new edit item alreay exists and has deleted state				
			} else if ( treeItemStateMap.containsKey(updated_key) &&  treeItemStateMap.get(updated_key).equals(ItemState.Delete) ) {
				
				//change from deleted state to edit
				treeItemStateMap.put(updated_key, ItemState.Edit);
				
				//change inital from edit to deleted
				treeItemStateMap.put(initial_key, ItemState.Delete);				
*/
			} else {
												
				if (MessageDialog.openConfirm(this.getShell(), Constants.DIALOG_QUESTION_HEADING, "Replace dynamic member '" + currentTreeItem.getText() + "' with '" +
						updatedDynamicMember + "'?  All xml references will be updated!")) {					
					
										
					TreeItem treeItem = treeItemMap.get(createDimensionMemberKey(dimensionName, initialMember));
					
					treeItem.setText(updatedDynamicMember);
					
					currentTreeItem = treeItem;							
					
					//if new, but editing, replace the initial member with new text
					if ( (treeItemStateMap.get(initial_key).equals(ItemState.New)) ) {
						
						treeItemStateMap.remove(initial_key);
						treeItemMap.remove(initial_key);
						
						initialMember = currentTreeItem.getText();
						currentTreeItem.setData(INITIAL_VALUE_KEY, initialMember);
																		
						treeItemStateMap.put(createDimensionMemberKey(dimensionName, initialMember), ItemState.New);
						
					}
					
					treeItemMap.put(createDimensionMemberKey(dimensionName, initialMember), treeItem);
							
				}
				
			}
			
		} else {
			
			TreeItem treeItem = treeItemMap.get(createDimensionMemberKey(dimensionName, initialMember));
			
			treeItem.setText(updatedDynamicMember);
			
			treeItemMap.put(createDimensionMemberKey(dimensionName, initialMember), treeItem);
			
		}
	
		saveButtonAction();

	}

	/**
	 * 
	 * Gets a list of view section names that have references to the member name on dimension
	 *
	 * @param dimensionName	Name of dimension
	 * @param memberName	Name of member
	 * @return				A list of view section names wher dimesion and member were referenced
	 */
	private List<String> getViewSectionNamesWithMember(String dimensionName, String memberName) {
		
		List<String> viewSectionNamesWithMember = new ArrayList<String>();
		
		if ( dimensionName != null && memberName != null  ) {
		
			String[] viewSectionNames = viewSectionModelManager.getKeys();
			
			if ( viewSectionNames != null ) {
			
				//loop through view sections
				key_loop:
				for (String viewSectionName : viewSectionNames) {
					
					//try go get view section
					PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionModelManager.getItem(viewSectionName);
					
					if ( viewSection != null ) {
						
						//search page tuples for dimension name
						if ( viewSection.getPafViewSection().getPageTuples() != null ) {
							
							for (PageTuple pageTuple : viewSection.getPafViewSection().getPageTuples() ) {
								
								if ( pageTuple.getAxis().equalsIgnoreCase(dimensionName) && pageTuple.getMember().equalsIgnoreCase(memberName)) {
									
									viewSectionNamesWithMember.add(viewSectionName);
									continue key_loop;
																		
								}
																								
							}
							
						}
						
						//search row tuples for dimension and member instances
						if ( viewSection.getPafViewSection().getRowAxisDims() != null && viewSection.getPafViewSection().getRowTuples() != null) {
							
							String[] rowDimensions =  viewSection.getRowAxisDims();
							
							int dimensionNdx = -1;
							
							//try to get dimension index
							for (int i = 0; i < rowDimensions.length; i++ ) {
								
								if ( rowDimensions[i].equalsIgnoreCase(dimensionName)) {
									
									dimensionNdx = i;
									
									break;
								}
																
							}
							
							//if dimension index was found, find any occurance of member
							if ( dimensionNdx != -1) {
								
								for (ViewTuple rowTuple : viewSection.getPafViewSection().getRowTuples() ) {
									
									String dimensionMember = rowTuple.getMemberDefs()[dimensionNdx];
									
									if ( dimensionMember.startsWith("@")) {
										
										ExpOperation expOperation = new ExpOperation(dimensionMember);
										
										String[] parms = expOperation.getParms();
										
										if ( parms != null ) {
											
											for (String parm : parms ) {
												
												if ( parm.equalsIgnoreCase(memberName) ) {
													
													viewSectionNamesWithMember.add(viewSectionName);
													continue key_loop;					
												
												}
											}
																						
										}
										
									} else {
		
									
										if ( dimensionMember.equalsIgnoreCase(memberName)) {
											
											viewSectionNamesWithMember.add(viewSectionName);
											continue key_loop;
											
										}
										
									}
									
								}
							}
							
						}
						
						if ( viewSection.getPafViewSection().getColAxisDims() != null && viewSection.getPafViewSection().getColTuples() != null) {
							
							String[] colDimensions =  viewSection.getColAxisDims();
							
							int dimensionNdx = -1;
							
							for (int i = 0; i < colDimensions.length; i++ ) {
								
								if ( colDimensions[i].equalsIgnoreCase(dimensionName)) {
									
									dimensionNdx = i;
									
									break;
								}
																
							}
							
							if ( dimensionNdx != -1) {
								
								for (ViewTuple colTuple : viewSection.getPafViewSection().getColTuples() ) {
									
									String dimensionMember = colTuple.getMemberDefs()[dimensionNdx];
									
									if ( dimensionMember.startsWith("@")) {
										
										ExpOperation expOperation = new ExpOperation(dimensionMember);
										
										String[] parms = expOperation.getParms();
										
										if ( parms != null ) {
											
											for (String parm : parms ) {
												
												if ( parm.equalsIgnoreCase(memberName) ) {
													
													viewSectionNamesWithMember.add(viewSectionName);
													continue key_loop;					
												
												}
											}
																						
										}
										
									} else {
									
										if ( dimensionMember.equalsIgnoreCase(memberName)) {
											
											viewSectionNamesWithMember.add(viewSectionName);
											continue key_loop;
											
										}
										
									}
								}
							}
							
						}
						
					}
					
				}
			
			}
		}
		
		return viewSectionNamesWithMember;		
	}
	
	/**
	 * 
	 * Gets a list of role configuration names that have references to the member name
	 *
	 * @param dimensionName	Name of dimension
	 * @param memberName	Name of member
	 * @return				A list of role configuration names wher dimesion and member were referenced
	 */
	private List<String> getRoleConfigurationNamesWithMember(String dimensionName, String memberName) { 
		
		List<String> roleConfigNamesWithMember = new ArrayList<String>();
		
		if ( dimensionName != null && memberName != null  ) {
		
			String[] keys = plannerConfigModelManager.getKeys();
			
			if ( keys != null ) {
			
				key_loop:
				for (String key : keys) {
					
					PafPlannerConfig roleConfig = (PafPlannerConfig) plannerConfigModelManager.getItem(key);
					
					if ( roleConfig != null ) {
						
						PafApplicationDef pafApp = PafApplicationUtil.getPafApp(project);
						
						String versionDim = pafApp.getMdbDef().getVersionDim();
						
						if ( versionDim.equalsIgnoreCase(dimensionName)) {
						
							if ( roleConfig.getVersionFilter() != null ) {
								
								for (String versionFilter : roleConfig.getVersionFilter()) {
									
									if ( versionFilter.equalsIgnoreCase(memberName)) {
										
										roleConfigNamesWithMember.add(key);
										continue key_loop;
										
									}
									
								}
								
							}
							
						}
						
						if ( roleConfig.getDataFilterSpec() != null)  {
							
							for ( PafDimSpec pafDimSpec :  roleConfig.getDataFilterSpec().getDimSpec() ) {
								
								String pafDimSpecDimension = pafDimSpec.getDimension();
								
								if ( pafDimSpecDimension.equalsIgnoreCase(dimensionName) ) {
									
									if ( pafDimSpec.getExpressionList() != null) {
										
										for (String expression : pafDimSpec.getExpressionList()) {
											
											if ( expression.equalsIgnoreCase(memberName) ) {
												
												roleConfigNamesWithMember.add(key);
												continue key_loop;
												
											}
											
										}
										
									}
									
								}
								
							}
							
						}
					}
					
				}
			}
		}
		
		return roleConfigNamesWithMember;
		
	}
	
	//returns a map of items taht need to be updated
	private Map<String, String> getItemsToUpdate() {
		
//		old value, new value
		Map<String, String> itemsToUpdate = new TreeMap<String, String>();
		
		for (String key : treeItemStateMap.keySet()) {
			
			ItemState itemState = treeItemStateMap.get(key);
			
			if ( itemState.equals(ItemState.Edit)) {
				
				TreeItem treeItem = treeItemMap.get(key);
				
				if ( treeItem != null ) {
					
					String initialText = (String) treeItem.getData(INITIAL_VALUE_KEY);
					String updatedText = treeItem.getText();					
					
					if ( ! updatedText.equals(initialText)) {
						
						itemsToUpdate.put(initialText, updatedText);
						
					}
					
					
				}
				
			}
			
		}
		
		return itemsToUpdate;
		
	}
	
	/**
	 * 
	 * Updates the View Sections and Role Configurations
	 *
	 */
	private void updateReferences() {
	
		//change data structure for all dimensions, not just version for future
		Map<String, String> updatedVersionItemMap = getItemsToUpdate();
		
		//if there are members to update
		if ( updatedVersionItemMap.size() > 0 ) {
		
			PafApplicationDef pafApp = PafApplicationUtil.getPafApp(project);
			
			updateViewSections(pafApp.getMdbDef().getVersionDim(), updatedVersionItemMap);
			updateRoleConfigs(pafApp.getMdbDef().getVersionDim(), updatedVersionItemMap);
			
		}
		
		
	}

	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param dimensionName
	 * @param updatedVersionItemMap
	 */
	private void updateViewSections(String dimensionName, Map<String, String> updatedVersionItemMap) {

		String[] keys = viewSectionModelManager.getKeys();
		
		if ( keys != null ) {
		
			key_loop: 
			for (String key : keys) {
				
				boolean saveItem = false;
				
				PafViewSectionUI viewSection = (PafViewSectionUI) viewSectionModelManager.getItem(key);
				
				if ( viewSection != null ) {
								
					if ( viewSection.getPafViewSection().getPageTuples() != null ) {
						
						for (PageTuple pageTuple : viewSection.getPafViewSection().getPageTuples() ) {
							
							if ( pageTuple.getAxis().equalsIgnoreCase(dimensionName) && updatedVersionItemMap.containsKey(pageTuple.getMember())) {
								
								String updatedItem = updatedVersionItemMap.get(pageTuple.getMember());
								
								pageTuple.setMember(updatedItem);
								
								saveItem = true;
								
								continue key_loop;
							}
																							
						}
						
					}
					
					if ( viewSection.getPafViewSection().getRowAxisDims() != null && viewSection.getPafViewSection().getRowTuples() != null) {
						
						String[] rowDimensions =  viewSection.getRowAxisDims();
						
						int dimensionNdx = -1;
						
						for (int i = 0; i < rowDimensions.length; i++ ) {
							
							if ( rowDimensions[i].equalsIgnoreCase(dimensionName)) {
								
								dimensionNdx = i;
								
								break;
							}
															
						}
						
						if ( dimensionNdx != -1) {
							
							for (ViewTuple rowTuple : viewSection.getPafViewSection().getRowTuples() ) {
								
								String dimensionMember = rowTuple.getMemberDefs()[dimensionNdx];
								
								if ( dimensionMember.startsWith("@")) {
									
									ExpOperation expOperation = new ExpOperation(dimensionMember);
									
									String[] parms = expOperation.getParms();
									
									if ( parms != null ) {
										
										boolean parmUpdated = false;
										
										for (int i = 0; i < parms.length; i++ ) {
											
											String parm = parms[i];
											
											if ( updatedVersionItemMap.containsKey(parm)) {
												
												parms[i] = updatedVersionItemMap.get(parm);						
											
												parmUpdated = true;
											}
											
										}
										
										if ( parmUpdated ) {
	
											saveItem = true;
											
											expOperation.setParms(parms);
											
											rowTuple.getMemberDefs()[dimensionNdx] = expOperation.toString();
											
										}
										
									}
									
								} else {
															
									if ( updatedVersionItemMap.containsKey(dimensionMember) ) {
										
										String updatedItem = updatedVersionItemMap.get(dimensionMember);
										
										rowTuple.getMemberDefs()[dimensionNdx] = updatedItem; 								
										
										saveItem = true;
										
									}
								}
								
							}
						}
						
					}
					
					if ( viewSection.getPafViewSection().getColAxisDims() != null && viewSection.getPafViewSection().getColTuples() != null) {
						
						String[] colDimensions =  viewSection.getColAxisDims();
						
						int dimensionNdx = -1;
						
						for (int i = 0; i < colDimensions.length; i++ ) {
							
							if ( colDimensions[i].equalsIgnoreCase(dimensionName)) {
								
								dimensionNdx = i;
								
								break;
							}
															
						}
						
						if ( dimensionNdx != -1 ) {
							
							for (ViewTuple colTuple : viewSection.getPafViewSection().getColTuples() ) {
								
								String dimensionMember = colTuple.getMemberDefs()[dimensionNdx];
								
								if ( dimensionMember.startsWith("@")) {
									
									ExpOperation expOperation = new ExpOperation(dimensionMember);
									
									String[] parms = expOperation.getParms();
									
									if ( parms != null ) {
										
										boolean parmUpdated = false;
										
										for (int i = 0; i < parms.length; i++ ) {
											
											String parm = parms[i];
											
											if ( updatedVersionItemMap.containsKey(parm)) {
												
												parms[i] = updatedVersionItemMap.get(parm);						
											
												parmUpdated = true;
											}
											
										}
										
										if ( parmUpdated ) {
	
											saveItem = true;
											
											expOperation.setParms(parms);
											
											colTuple.getMemberDefs()[dimensionNdx] = expOperation.toString();
											
										}
										
									}
									
								} else {
								
									if ( updatedVersionItemMap.containsKey(dimensionMember) ) {
										
										String updatedItem = updatedVersionItemMap.get(dimensionMember);
										
										colTuple.getMemberDefs()[dimensionNdx] = updatedItem; 								
										
										saveItem = true;
																	
									}
									
								}
								
							}
							
						}
					}
					
				}
				
				if ( saveItem ) {
					
					viewSectionModelManager.replace(key, viewSection);
					
				}
				
			}
		
			viewSectionModelManager.save();
			
		}

		
		
	}
	
	/**
	 * 
	 *  Updates the dimension/exiting member with the new member.
	 *
	 * @param dimensionName			Name of dimension
	 * @param updatedVersionItemMap	Map of updated versions.  key = old vlaue, value = new value
	 */
	private void updateRoleConfigs(String dimensionName, Map<String, String> updatedVersionItemMap) {

		
		String[] keys = plannerConfigModelManager.getKeys();
		
		if ( keys != null ) {
		
			for (String key : keys) {
				
				PafPlannerConfig roleConfig = (PafPlannerConfig) plannerConfigModelManager.getItem(key);
				
				if ( roleConfig != null ) {
					
					PafApplicationDef pafApp = PafApplicationUtil.getPafApp(project);
					
					String versionDim = pafApp.getMdbDef().getVersionDim();
					
					if ( versionDim.equalsIgnoreCase(dimensionName)) {
					
						if ( roleConfig.getVersionFilter() != null ) {
							
							for (int i = 0; i < roleConfig.getVersionFilter().length; i++ ) {
								
								String versionFilter = roleConfig.getVersionFilter()[i];
								
								if ( updatedVersionItemMap.containsKey(versionFilter)) {
									
									String updatedText= updatedVersionItemMap.get(versionFilter);
									
									roleConfig.getVersionFilter()[i] = updatedText;   
									
								}
								
							}
							
						}
						
					}
					
					if ( roleConfig.getDataFilterSpec() != null)  {
						
						for ( PafDimSpec pafDimSpec :  roleConfig.getDataFilterSpec().getDimSpec() ) {
							
							String pafDimSpecDimension = pafDimSpec.getDimension();
							
							if ( pafDimSpecDimension.equalsIgnoreCase(dimensionName) ) {
								
								if ( pafDimSpec.getExpressionList() != null) {
									
									for (int i = 0; i < pafDimSpec.getExpressionList().length; i++ ) {
										
										String expression  = pafDimSpec.getExpressionList()[i];
										
										if ( updatedVersionItemMap.containsKey(expression) ) {
											
											String updatedText= updatedVersionItemMap.get(expression);
											
											pafDimSpec.getExpressionList()[i] = updatedText; 
										}
										
									}
									
								}
								
							}
							
						}
						
					}
				}
				
			}
			plannerConfigModelManager.save();
		}
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(654, 500);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			
			if ( saveButton.isVisible() && saveButton.isEnabled()) {
				
				if ( MessageDialog.openConfirm(getShell(), Constants.DIALOG_QUESTION_HEADING, "Save dynamic member?") ) {
					
					buttonSelected(ButtonId.Save);
					
				}
				
			}
			
			updateReferences();
			
			saveDynamicMembers();
						
		}
		super.buttonPressed(buttonId);
	}
	
	/**
	 * 
	 *  Saves the dynamic members
	 *
	 */
	private void saveDynamicMembers() {
				
		TreeItem parentOfTree = dynamicMembersByDimensionTree.getTopItem();
						
		String dimensionKey = parentOfTree.getText();
		
		DynamicMemberDef dynamicMemberDef = (DynamicMemberDef) dynamicMembersModelManager.getItem(dimensionKey);
		
		List<String> dimensionMembers = new ArrayList<String>();
		
		for (TreeItem childItem : parentOfTree.getItems()) {
					
			dimensionMembers.add(childItem.getText());
						
		}
		
		dynamicMemberDef.setMemberSpecs(dimensionMembers.toArray(new String[0]));
		
		dynamicMembersModelManager.replace(dimensionKey, dynamicMemberDef);
		
		dynamicMembersModelManager.save();
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Dynamic Members Wizard");
	}

	/**
	 * 
	 *  Creates the key used for maps.  Concatinates the dimension name + | + member name together.
	 *
	 * @param dimensionName
	 * @param memberName
	 * @return
	 */
	private String createDimensionMemberKey(String dimensionName, String memberName) {
		
		if ( dimensionName != null && memberName != null ) {
			
			return dimensionName + '|' + memberName;
		}
		
		return null;
	}
	
}

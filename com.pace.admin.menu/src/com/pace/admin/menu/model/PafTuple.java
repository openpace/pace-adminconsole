/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.model;

public class PafTuple implements Cloneable {
	
	private PafMember[] memberAr;
	
	private PafMemberHeader header;
	
	private PafMemberData data;
	
	private boolean memberTag;

	public PafMemberData getData() {
		return data;
	}

	public void setData(PafMemberData data) {
		this.data = data;
	}

	public PafMemberHeader getHeader() {
		return header;
	}

	public void setHeader(PafMemberHeader header) {
		this.header = header;
	}

	public PafMember[] getMemberAr() {
		return memberAr;
	}

	public void setMemberAr(PafMember[] memberAr) {
		this.memberAr = memberAr;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		
		PafTuple clonedPafTuple = (PafTuple) super.clone();
		
		if ( this.header != null ) {
			clonedPafTuple.header = (PafMemberHeader) this.header.clone();
		}
		
		if ( this.data != null ) {
			clonedPafTuple.data = (PafMemberData) this.data.clone();
		}
		
		return clonedPafTuple;
	}

	/**
	 * @return the memberTag
	 */
	public boolean isMemberTag() {
		return memberTag;
	}

	/**
	 * @param memberTag the memberTag to set
	 */
	public void setMemberTag(boolean memberTag) {
		this.memberTag = memberTag;
	}
	
	

}

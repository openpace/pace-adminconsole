/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.model;

import com.pace.base.view.PafBorder;

public class PafMemberHeader implements Cloneable {

	private Boolean parentFirst;
	
	private PafBorder border; 

	private String globalStyle;
	
	private String overrideLabel;
	
	private Float rowHeight;
	
	private Float columnWidth;

	public PafBorder getBorder() {
		return border;
	}

	public void setBorder(PafBorder border) {
		this.border = border;
	}

	public String getGlobalStyle() {
		return globalStyle;
	}

	public void setGlobalStyle(String globalStyle) {
		this.globalStyle = globalStyle;
	}

	public String getOverrideLabel() {
		return overrideLabel;
	}

	public void setOverrideLabel(String overrideLabel) {
		this.overrideLabel = overrideLabel;
	}

	public Boolean getParentFirst() {
		return parentFirst;
	}

	public void setParentFirst(Boolean parentFirst) {
		this.parentFirst = parentFirst;
	}

	public Float getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(Float columnWidth) {
		this.columnWidth = columnWidth;
	}

	public Float getRowHeight() {
		return rowHeight;
	}

	public void setRowHeight(Float rowHeight) {
		this.rowHeight = rowHeight;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		
		PafMemberHeader clonedMemberHeader = (PafMemberHeader) super.clone();
		
		if ( this.border != null ) {
			clonedMemberHeader.border = (PafBorder) this.border.clone();
		}
		
		return clonedMemberHeader;
	}
	
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer(50);
	
		strBuff.append("Parent First: " + parentFirst);
		strBuff.append("\nBorder: " + border.getBorder());
		strBuff.append("\nGlobal Sytle: " + globalStyle);
		strBuff.append("\nRow Height: " + rowHeight + ", Col Width: " + columnWidth);
		
		return strBuff.toString();
		
	}
	
	
	
}

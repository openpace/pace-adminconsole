/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.model;

import com.pace.admin.menu.dialogs.internal.MemberSelection;

public class PafMember {

	private String name;
	private MemberSelection selectionType;
	private String dimensionName;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MemberSelection getSelectionType() {
		return selectionType;
	}
	public void setSelectionType(MemberSelection selectionType) {
		this.selectionType = selectionType;
	}
	
	/**
	 * @return Returns the dimensionName.
	 */
	public String getDimensionName() {
		return dimensionName;
	}
	/**
	 * @param dimensionName The dimensionName to set.
	 */
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.model;

import com.pace.base.view.PafBorder;

public class PafMemberData implements Cloneable {
	
	private Boolean plannable;
	
	private PafBorder border;
	
	private String globalStyle;
	
	private String alignment;
	
	private String overrideLabel;
	
	private String overrideNumericFormat;
	
	private Boolean memberTagEditable;

	public String getAlignment() {
		return alignment;
	}

	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

	public PafBorder getBorder() {
		return border;
	}

	public void setBorder(PafBorder border) {
		this.border = border;
	}

	public String getGlobalStyle() {
		return globalStyle;
	}

	public void setGlobalStyle(String globalStyle) {
		this.globalStyle = globalStyle;
	}

	public String getOverrideLabel() {
		return overrideLabel;
	}

	public void setOverrideLabel(String overrideLabel) {
		this.overrideLabel = overrideLabel;
	}

	public String getOverrideNumericFormat() {
		return overrideNumericFormat;
	}

	public void setOverrideNumericFormat(String overrideNumericFormat) {
		this.overrideNumericFormat = overrideNumericFormat;
	}

	public Boolean getPlannable() {
		return plannable;
	}

	public void setPlannable(Boolean plannable) {
		this.plannable = plannable;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		PafMemberData clonedMemberData = (PafMemberData) super.clone();
		
		if ( this.border != null ) {
			clonedMemberData.border = (PafBorder) clonedMemberData.border.clone();
		}
		
		return clonedMemberData;
	}

	public Boolean getMemberTagEditable() {
		return memberTagEditable;
	}

	public void setMemberTagEditable(Boolean memberTagEditable) {
		this.memberTagEditable = memberTagEditable;
	}
	
	
}

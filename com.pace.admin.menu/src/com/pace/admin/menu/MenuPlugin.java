/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.views.MenuView;

/**
 * The main plugin class to be used in the desktop.
 */
public class MenuPlugin extends AbstractUIPlugin {

	public static final String ID = "com.pace.admin.menu";
	
	//The shared instance.
	private static MenuPlugin plugin;
	/**
	 * The constructor.
	 */
	public MenuPlugin() {
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		PafServerUtil.loadPafServers();
		PafProjectUtil.loadExpandProjectTree();
		this.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeAllEditors(false);
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 */
	public static MenuPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path.
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin("com.pace.admin.menu", path);
	}
	
	public MenuView getMenuView() {
		
		if( this.getWorkbench().getActiveWorkbenchWindow() != null 
				&& this.getWorkbench().getActiveWorkbenchWindow().getActivePage() != null ) {
			return (MenuView) MenuPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(MenuView.ID);
		}
		return null;
	}
}

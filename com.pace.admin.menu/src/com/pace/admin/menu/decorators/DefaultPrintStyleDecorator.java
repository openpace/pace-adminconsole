/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.decorators;

import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.base.ui.PrintStyles;

public class DefaultPrintStyleDecorator implements ILabelDecorator {

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image decorateImage(Image image, Object element) {
		if( isPrintStyleDefault(element)) {
			return MenuPlugin.getImageDescriptor("/icons/check.png").createImage();
		}
		else {
			return MenuPlugin.getImageDescriptor("icons\\wi0064-16.gif").createImage();
			
		}
	}

	@Override
	public String decorateText(String text, Object element) {
		if ( element instanceof PrintStyleNode ) {
			
			String pritnStyleName = ((PrintStyleNode) element).getName();
		
			if( isPrintStyleDefault(element) ) {
				return pritnStyleName + Constants.DEFAULT_MARKER;
			}
			else {
				return pritnStyleName;
			}
		}
		return null;
	}
	
	private boolean isPrintStyleDefault(  Object element ) {
		PrintStyles model = new PrintStylesManager(((PrintStyleNode)element).getProject());
		return model.isPrintStyleDefaultPrintStyleGivenName(((PrintStyleNode)element).getName());
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import com.pace.admin.menu.actions.projects.DeleteProjectAction;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	private DeleteProjectAction deleteProjectAction = null;
	
    public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
    }

    protected void makeActions(IWorkbenchWindow window) {
    	
    	deleteProjectAction = new DeleteProjectAction(window);
    	register(deleteProjectAction);
    	
    }

    protected void fillMenuBar(IMenuManager menuBar) {
    	    	
    	MenuManager projectsMenu = new MenuManager("&Projects", "projects");
    	projectsMenu.add(deleteProjectAction);
    	projectsMenu.add(new Separator());
    	
    	menuBar.add(projectsMenu);
		
    }
    
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.project;

/**
 * @author jmilliron
 *
 */
public class ProjectSource {

	protected String name;
	protected String description;
	protected ProjectSourceType type;
	
	public ProjectSource(String name, String description, ProjectSourceType type) {
		this.name = name;
		this.description = description;
		this.type = type;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pace.admin.menu.project.IProjectSource#getDescription()
	 */
	public String getDescription() {
		return description;
	}
	/*
	 * (non-Javadoc)
	 * @see com.pace.admin.menu.project.IProjectSource#getName()
	 */
	public String getName() {
		return name;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pace.admin.menu.project.IProjectSource#getType()
	 */
	public ProjectSourceType getType() {
		return type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ProjectSourceType type) {
		this.type = type;
	}
	
	
	

}

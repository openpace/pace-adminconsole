/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.views;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;

import com.pace.admin.menu.dialogs.internal.InvalidSpecificationMember;

public class InvalidSpecificationMemberView extends ViewPart {
	public InvalidSpecificationMemberView() {
	}

	class InvalidSecurityMemberContentProvider implements
			IStructuredContentProvider {

		public Object[] getElements(Object inputElement) {

			if (inputElement instanceof Set) {

				Set<InvalidSpecificationMember> invalidSecurityMemberSet = (LinkedHashSet<InvalidSpecificationMember>) inputElement;

				return invalidSecurityMemberSet.toArray();
			}

			return new Object[0];
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	class InvalidSecurityMemberTableLabelProvider extends LabelProvider
			implements ITableLabelProvider {

		public String getColumnText(Object element, int columnIndex) {

			String strValue = null;

			if (element instanceof InvalidSpecificationMember) {

				InvalidSpecificationMember invalidSpecMember = (InvalidSpecificationMember) element;

				switch (columnIndex) {

				case 0:
					strValue = invalidSpecMember.getDimensionName();
					break;
				case 1:
					strValue = invalidSpecMember.getMemberName();
					break;
				}

			}
			return strValue;
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

	}

	private Table table_1;

	private Set<InvalidSpecificationMember> invalidSpecMemberSet;
	
	public static final String ID = "com.pace.admin.menu.views.InvalidSecurityMemberView"; //$NON-NLS-1$
	
	private TableViewer tableViewer = null;

	/**
	 * Create contents of the view part
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout());

		tableViewer = new TableViewer(container, SWT.BORDER);
		tableViewer
				.setContentProvider(new InvalidSecurityMemberContentProvider());
		tableViewer
				.setLabelProvider(new InvalidSecurityMemberTableLabelProvider());
		table_1 = tableViewer.getTable();
		table_1.setLinesVisible(true);
		table_1.setHeaderVisible(true);

		final TableColumn dimensionTableColumn = new TableColumn(table_1,
				SWT.NONE);
		dimensionTableColumn.setWidth(117);
		dimensionTableColumn.setText("Dimension");

		final TableColumn securityMemberTableColumn = new TableColumn(table_1,
				SWT.NONE);
		securityMemberTableColumn.setWidth(143);
		securityMemberTableColumn.setText("Invalid Security Member");
		
		

		if ( invalidSpecMemberSet == null ) {
			invalidSpecMemberSet = new LinkedHashSet();
		}
		
		
		tableViewer.setInput(invalidSpecMemberSet);
		//
		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}

	/**
	 * Initialize the menu
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	/**
	 * @return Returns the invalidSecurityMemberSet.
	 */
	public Set<InvalidSpecificationMember> getInvalidSpecMemberSet() {
		return invalidSpecMemberSet;
	}

	/**
	 * @param invalidSecurityMemberSet The invalidSecurityMemberSet to set.
	 */
	public void setInvalidSpecMemberSet(
			Set<InvalidSpecificationMember> invalidSpecMemberSet) {
		this.invalidSpecMemberSet = invalidSpecMemberSet;
		
		
		if ( tableViewer != null ) {
			
			tableViewer.setInput(this.invalidSpecMemberSet);
			tableViewer.refresh();
		}
		
	}
	
	
}

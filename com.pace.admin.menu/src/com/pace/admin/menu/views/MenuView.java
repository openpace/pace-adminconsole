/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DecoratingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DecorationContext;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;
import org.springframework.util.StopWatch;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.project.ProjectEvent;
import com.pace.admin.global.project.ProjectMonitor;
import com.pace.admin.global.project.ProjectRequest;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.actions.AbstractUploadAction;
import com.pace.admin.menu.actions.adminlock.AdminLockAction;
import com.pace.admin.menu.actions.adminlock.CopyAdminLockAction;
import com.pace.admin.menu.actions.adminlock.CreateNewAdminLockAction;
import com.pace.admin.menu.actions.adminlock.DeleteAdminLockAction;
import com.pace.admin.menu.actions.importexport.AbstractUploadViewAction;
import com.pace.admin.menu.actions.importexport.AbstractUploadViewSectionAction;
import com.pace.admin.menu.actions.importexport.DownloadViewSectionsFromServerAction;
import com.pace.admin.menu.actions.importexport.DownloadViewsFromServerAction;
import com.pace.admin.menu.actions.importexport.HotDownloadProjectFromServer;
import com.pace.admin.menu.actions.importexport.HotUploadProjectToServerAction;
import com.pace.admin.menu.actions.importexport.ImportAndDeployRulesetsAction;
import com.pace.admin.menu.actions.importexport.UploadViewSectionsToServerAction;
import com.pace.admin.menu.actions.importexport.UploadViewSectionsToServerGroupAction;
import com.pace.admin.menu.actions.importexport.UploadViewsToServerAction;
import com.pace.admin.menu.actions.importexport.UploadViewsToServerGroupAction;
import com.pace.admin.menu.actions.migration.PafCustomFunctionsMigrationAction;
import com.pace.admin.menu.actions.printstyles.AbstractUploadPrintStylesAction;
import com.pace.admin.menu.actions.printstyles.DownloadPrintStylesAction;
import com.pace.admin.menu.actions.printstyles.OpenNewPrintStyleAction;
import com.pace.admin.menu.actions.printstyles.PrintStyleAction;
import com.pace.admin.menu.actions.printstyles.UploadPrintStylesAction;
import com.pace.admin.menu.actions.printstyles.UploadPrintStylesToServerGroupAction;
import com.pace.admin.menu.actions.projects.CollapseAllAction;
import com.pace.admin.menu.actions.projects.CopyProjectAction;
import com.pace.admin.menu.actions.projects.DeleteProjectAction;
import com.pace.admin.menu.actions.projects.DownloadProjectFromServerAction;
import com.pace.admin.menu.actions.projects.ExpandAllProjectsAction;
import com.pace.admin.menu.actions.projects.NewProjectButtonAction;
import com.pace.admin.menu.actions.projects.RefreshProjectFolderAction;
import com.pace.admin.menu.actions.projects.RefreshProjectsAction;
import com.pace.admin.menu.actions.projects.SelectDefaultServerProjectAction;
import com.pace.admin.menu.actions.projects.ToggleAutoConvertProjectAction;
import com.pace.admin.menu.actions.projects.UploadProjectToServerAction;
import com.pace.admin.menu.actions.projects.UploadProjectToServerGroupAction;
import com.pace.admin.menu.actions.rolesprocesses.DeletePlanCycleAction;
import com.pace.admin.menu.actions.rolesprocesses.DeletePlannerRoleAction;
import com.pace.admin.menu.actions.rolesprocesses.DeleteRoleConfigurationAction;
import com.pace.admin.menu.actions.rolesprocesses.DeleteSeasonAction;
import com.pace.admin.menu.actions.rolesprocesses.RenamePlanCycleAction;
import com.pace.admin.menu.actions.rolesprocesses.RenameSeasonAction;
import com.pace.admin.menu.actions.rolesprocesses.OpenNewPlanCycleAction;
import com.pace.admin.menu.actions.rolesprocesses.OpenNewPlannerRoleAction;
import com.pace.admin.menu.actions.rolesprocesses.OpenNewRoleConfigurationAction;
import com.pace.admin.menu.actions.rolesprocesses.OpenNewSeasonAction;
import com.pace.admin.menu.actions.rolesprocesses.PlanCycleAction;
import com.pace.admin.menu.actions.rolesprocesses.PlannerRoleAction;
import com.pace.admin.menu.actions.rolesprocesses.RenamePlannerRoleAction;
import com.pace.admin.menu.actions.rolesprocesses.RoleConfigurationAction;
import com.pace.admin.menu.actions.rolesprocesses.SeasonAction;
import com.pace.admin.menu.actions.stylesformats.ConditionalFormatAction;
import com.pace.admin.menu.actions.stylesformats.ConditionalStyleAction;
import com.pace.admin.menu.actions.stylesformats.CopyConditionalFormatAction;
import com.pace.admin.menu.actions.stylesformats.CopyConditionalStyleAction;
import com.pace.admin.menu.actions.stylesformats.CreateNewConditionalFormatAction;
import com.pace.admin.menu.actions.stylesformats.CreateNewConditionalStyleAction;
import com.pace.admin.menu.actions.stylesformats.DeleteConditionalFormatAction;
import com.pace.admin.menu.actions.stylesformats.DeleteConditionalStyleAction;
import com.pace.admin.menu.actions.usersecurity.DeleteUserSecurityAction;
import com.pace.admin.menu.actions.usersecurity.OpenNewUserSecurityAction;
import com.pace.admin.menu.actions.usersecurity.UploadUserSecurityAction;
import com.pace.admin.menu.actions.usersecurity.UserSecurityAction;
import com.pace.admin.menu.actions.usersecurity.ValidateUserSecurityAction;
import com.pace.admin.menu.actions.viewgroups.CopyViewGroupAction;
import com.pace.admin.menu.actions.viewgroups.DeleteViewGroupAction;
import com.pace.admin.menu.actions.viewgroups.OpenNewViewGroupAction;
import com.pace.admin.menu.actions.viewgroups.OpenViewGroupAction;
import com.pace.admin.menu.actions.viewgroups.RenameViewGroupAction;
import com.pace.admin.menu.actions.views.CopyViewAction;
import com.pace.admin.menu.actions.views.DeleteViewAction;
import com.pace.admin.menu.actions.views.OpenNewViewAction;
import com.pace.admin.menu.actions.views.RenameViewAction;
import com.pace.admin.menu.actions.viewsections.CopyViewSectionAction;
import com.pace.admin.menu.actions.viewsections.DeleteViewSectionAction;
import com.pace.admin.menu.actions.viewsections.OpenNewViewSectionAction;
import com.pace.admin.menu.actions.viewsections.RenameViewSectionAction;
import com.pace.admin.menu.dialogs.DynamicMembersDialog;
import com.pace.admin.menu.dialogs.GlobalStylePropertiesDialog;
import com.pace.admin.menu.dialogs.HierarchyFormattingDialog;
import com.pace.admin.menu.dialogs.MemberTagsDialog;
import com.pace.admin.menu.dialogs.NumericFormatsDialog;
import com.pace.admin.menu.dialogs.UserSelectionsDialog;
import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.dialogs.ViewSectionDialog;
import com.pace.admin.menu.job.UpgradeProjectJob;
import com.pace.admin.menu.job.UpgradeProjectJobChangeAdapter;
import com.pace.admin.menu.nodes.AdminLockNode;
import com.pace.admin.menu.nodes.AdminLocksNode;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.nodes.ConditionalFormatsNode;
import com.pace.admin.menu.nodes.ConditionalStyleNode;
import com.pace.admin.menu.nodes.ConditionalStylesNode;
import com.pace.admin.menu.nodes.DummyNode;
import com.pace.admin.menu.nodes.DynamicMembersDialogNode;
import com.pace.admin.menu.nodes.GlobalStylesWizardNode;
import com.pace.admin.menu.nodes.HierarchyFormattingWizardNode;
import com.pace.admin.menu.nodes.MemberTagDialogNode;
import com.pace.admin.menu.nodes.NumericFormatWizardNode;
import com.pace.admin.menu.nodes.PlanCycleNode;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.PlannerRoleNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ProjectSettingsWizardNode;
import com.pace.admin.menu.nodes.RoleConfigurationNode;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.RolesProcessesNode;
import com.pace.admin.menu.nodes.RootNode;
import com.pace.admin.menu.nodes.SeasonNode;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.nodes.StylesFormatsNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.nodes.UserSecurityDimensionNode;
import com.pace.admin.menu.nodes.UserSecurityDomainNode;
import com.pace.admin.menu.nodes.UserSecurityNode;
import com.pace.admin.menu.nodes.UserSecurityRoleNode;
import com.pace.admin.menu.nodes.UserSecuritySpecificationNode;
import com.pace.admin.menu.nodes.UserSelectionDialogNode;
import com.pace.admin.menu.nodes.UsersSecurityNode;
import com.pace.admin.menu.nodes.ViewGroupNode;
import com.pace.admin.menu.nodes.ViewGroupsNode;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.nodes.ViewSectionNode;
import com.pace.admin.menu.nodes.ViewSectionsNode;
import com.pace.admin.menu.nodes.ViewsNode;
import com.pace.admin.menu.providers.MyStyledLabelProvider;
import com.pace.admin.menu.providers.TreeContentProvider;
import com.pace.admin.menu.util.PafProjectUpgradeUtil;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.servers.actions.DownloadCubeChangesFromServerAction;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.base.migration.PafDynamicMembersMigrationAction;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.base.ui.PrintStyle;
import com.pace.base.ui.PrintStyles;
import com.pace.base.utility.GUIDUtil;
import com.pace.admin.menu.nodes.SecurityNode;
import com.pace.admin.menu.nodes.StylesNode;
import com.pace.admin.menu.nodes.FormatsNode;
import com.pace.admin.menu.nodes.GlobalStylesNode;
import com.pace.admin.menu.nodes.HierarchyFormatsNode;
import com.pace.admin.menu.nodes.NumericFormatsNode;

public class MenuView extends ViewPart implements Observer {
	public MenuView() {
	}

	private static Logger logger = Logger.getLogger(MenuView.class);
	
	private TreeViewer viewer;
	private RootNode rootNode;
	
	private OpenNewViewAction openNewViewAction;
	private OpenNewViewSectionAction openNewViewSectionAction;
	private DeleteViewAction deleteViewAction;
	private DeleteViewSectionAction deleteViewSectionAction ;
	private CopyViewAction copyViewAction;
	private CopyViewSectionAction copyViewSectionAction;
	private RenameViewAction renameViewAction;
	private RenameViewSectionAction renameViewSectionAction;
	private RefreshProjectsAction refreshAction;
	private RefreshProjectFolderAction refreshProjectFolderAction;
	private CopyProjectAction copyProjectAction;
//	private ChooseColorsAction chooseColorsAction = null;
	private PlanCycleAction planCycleAction;
	private PlannerRoleAction plannerRoleAction;
	private SeasonAction seasonAction;
	private RoleConfigurationAction roleConfigurationAction;
	
	private OpenNewPlanCycleAction openNewPlanCycleAction;
	private RenamePlanCycleAction renamePlanCycleAction;
	private DeletePlanCycleAction deletePlanCycleAction;
	
	private OpenNewSeasonAction openNewSeasonAction;
	private RenameSeasonAction renameSeasonAction;
	private DeleteSeasonAction deleteSeasonAction;
	
	private OpenNewPlannerRoleAction openNewPlannerRoleAction;
	private RenamePlannerRoleAction renamePlannerRoleAction;
	private DeletePlannerRoleAction deletePlannerRoleAction;
	
	private OpenNewRoleConfigurationAction openNewRoleConfigurationAction;
	private DeleteRoleConfigurationAction deleteRoleConfigurationAction;
	
	private UserSecurityAction userSecurityAction;
	private UserSecurityAction cloneUserSecurityAction;
	private OpenNewUserSecurityAction openNewUserSecurityAction;
	private DeleteUserSecurityAction deleteUserSecurityAction;
	private ValidateUserSecurityAction validateUserSecurityAction;
	
//	private DeployProjectToServerAction[] deployProjectToServerActions = null;
	private UploadViewsToServerAction[] deployViewsToServerActions;
	private UploadViewsToServerGroupAction[] deployViewsToServerGroupActions;
	private UploadViewSectionsToServerAction[] deployViewSectionsToServerActions;
	private UploadViewSectionsToServerGroupAction[] deployViewSectionsToServerGroupActions;
	private UploadUserSecurityAction[] uploadUserSecurityActions;
	private UploadProjectToServerAction[] uploadProjectToServerActions;
	private DownloadProjectFromServerAction[] downloadProjectFromServerActions;
	private DownloadCubeChangesFromServerAction[] downloadCubeChangesFromServerActions;
	private DownloadViewsFromServerAction[] downloadViewsFromServerActions;
	private DownloadViewSectionsFromServerAction[] downloadViewSectionsFromServerActions;
	private UploadProjectToServerGroupAction[] uploadProjectToServerGroupActions;
	
	//TTN - Print Preference - added by Iris
	private PrintStyleAction printStyleAction;
	private OpenNewPrintStyleAction openNewPrintStyleAction;
	private DownloadPrintStylesAction[] downloadPrintStylesActions;
	private UploadPrintStylesAction[] uploadPrintStylesActions;
	private UploadPrintStylesToServerGroupAction[] uploadPrintStylesToServerGroupActions;
	
	private SelectDefaultServerProjectAction[] selectDefaultServerProjectActions;
	
	private CopyViewGroupAction copyViewGroupAction;
	private RenameViewGroupAction renameViewGroupAction; 
	private OpenViewGroupAction openViewGroupAction;
	private OpenNewViewGroupAction openNewViewGroupAction;
	private DeleteViewGroupAction deleteViewGroupAction;
	
	private ConditionalFormatAction condFormatAction;
	private ConditionalStyleAction condStyleAction;
	private CreateNewConditionalStyleAction createCondtionalStyleAction;
	private CreateNewConditionalFormatAction createCondtionalFormatAction;
	private DeleteConditionalStyleAction deleteCondtionalStyleAction;
	private DeleteConditionalFormatAction deleteCondtionalFormatAction ;
	private CopyConditionalStyleAction copyCondtionalStyleAction;
	private CopyConditionalFormatAction copyCondtionalFormatAction;

	private AdminLockAction openAdminLockAction;
	private CreateNewAdminLockAction createAdminLockAction;
	private DeleteAdminLockAction deleteAdminLockAction;
	private CopyAdminLockAction copyAdminLockAction;

	private HotUploadProjectToServerAction hotProjectDeployAction;
	private HotDownloadProjectFromServer hotProjectRefreshAction;
	
	private DeleteProjectAction deleteProjectAction;
	private ToggleAutoConvertProjectAction autoConvertProjectAction;
	private IAction newProjectAction;
	private IAction importProjectAction;
	private IAction importAndDeployRulesetAction; 
	private IAction exportProjectAction;
	
	private CollapseAllAction collapseAllAction;
	private ExpandAllProjectsAction expandAllAction;

	public static final String ID = Constants.MENU_VIEW_ID;
	
	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl(final Composite parent) {
		
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL);

		viewer.setContentProvider(new TreeContentProvider());

		Tree tree = viewer.getTree();
		tree.addListener(SWT.Expand, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Object object = event.item;
				if(object instanceof TreeItem){
					TreeItem ti = (TreeItem) object;
					Object dummyObj = ti.getItem(0).getData();
					if(dummyObj instanceof DummyNode){
						DummyNode dummy = (DummyNode)dummyObj;
						ITreeNode parent = dummy.getParent();
						if (parent instanceof ProjectNode) {
							 final ProjectNode node = (ProjectNode) parent;
							 loadProject(node);
							 viewer.expandToLevel(node, 1);
						 }
					}
				}
			}
	    });
		tree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				IStructuredSelection selection = (IStructuredSelection)viewer.getSelection();
				Object object = selection.getFirstElement();
				 if (object instanceof PrintStyleNode) {
					//check click together?
					 if ( (e.stateMask & SWT.CTRL) != 0 && (e.keyCode == 86 || e.keyCode == 118 ) ) {
					    PrintStyleNode copyFromPrintStyleNode = (PrintStyleNode) object;
						PrintStyles model = new PrintStylesManager(((PrintStyleNode)object).getProject());
					    PrintStyle copyFromPrintStyle = (PrintStyle) model.getPrintStyleByName(copyFromPrintStyleNode.getName());
						PrintStyle copyToPrintStyle = copyFromPrintStyle.clone();
						if(copyToPrintStyle != null) {
							String copyName = copyToPrintStyle.getName() + " - " + Constants.COPY;
							if( model.findDulicatePrintStyleName(copyName) ) {
						    	GUIUtil.openMessageWindow( "Save Error",
							    		"Print style '" + copyName+ "' already exists. Please use a different name.");
								return;
							}
								
							copyToPrintStyle.setGUID(GUIDUtil.getGUID());
							copyToPrintStyle.setName(copyName);
							if( copyToPrintStyle.getDefaultStyle() ) //only 1 default, don't want to copy default property
								copyToPrintStyle.setDefaultStyle(false);
						}
						model.add(copyToPrintStyle.getGUID(), copyToPrintStyle);
						model.save();
						model.load();
						((PrintStylesNode)((PrintStyleNode)object).getParent()).createChildren(null);
					}
					else if ( e.keyCode == SWT.DEL ) {
						IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
						try {
							handlerService.executeCommand("com.pace.admin.menu.delete_print_style", null);
						} catch (Exception ex) {
							throw new RuntimeException("DeletePrintStyleCommand not found");
							// Give message
						}
					}
					viewer.refresh();
				 }
				 
				 if ( ((e.stateMask & SWT.CTRL) !=0 ) && (e.keyCode == SWT.F5) ) {
					 refreshAction.run();
				 }
				 
				 if (object instanceof ProjectNode) {
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						logger.debug("Project refresh");
					 }
				 }
				 
				 // TTN-2529 Do on-demand refresh of Project Nodes/Objects when pressing the F5 key.
				 if (object instanceof ViewsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						logger.debug("Views refresh");
					 }
				 }
				 if (object instanceof ViewSectionsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("ViewSections Refresh");
					 }
				 }
				 if (object instanceof ViewGroupsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug(" ViewGroups Refresh");
					 }
				 }
				 if (object instanceof ViewGroupNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug(" ViewGroup Refresh");
					 }
				 }
				 if (object instanceof SecurityNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Security Refresh");
					 }
				 }
				 if (object instanceof UsersSecurityNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Users Security Refresh");
					 }
				 }
				 if (object instanceof UserSecurityNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("User Security Refresh");
					 }
				 }	
				 if (object instanceof UserSecurityDomainNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("User Security Domain Refresh");
					 }
				 }
				 if (object instanceof RolesProcessesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Roles and Processes Refresh");
					 }
				 }
				 if (object instanceof PlanCyclesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Plan Cycles Refresh");
					 }
				 }
				 if (object instanceof SeasonsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Seasons Refresh");
					 }
				 }
				 if (object instanceof PlannerRolesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Roles Refresh");
					 }
				 }
				 if (object instanceof RoleConfigurationsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Role Configs Refresh");
					 }
				 }
				 if (object instanceof StylesFormatsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Styles Formats Refresh");
					 }
				 }
				 if (object instanceof StylesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Styles  Refresh");
					 }
				 }
				 if (object instanceof FormatsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Formats  Refresh");
					 }
				 }
				 if (object instanceof PrintStylesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Print Styles Refresh");
					 }
				 }
				 if (object instanceof ConditionalFormatsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("ConditionalFormats Refresh");
					 }
				 }
				 if (object instanceof ConditionalStylesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("ConditionalStyles Refresh");
					 }
				 }
				 if (object instanceof GlobalStylesNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("Global Styles Refresh");
					 }
				 }
				 if (object instanceof HierarchyFormatsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("HierarchyFormats Refresh");
					 }
				 }
				 if (object instanceof NumericFormatsNode) {
					 
					 if ( e.keyCode == SWT.F5 ) {
						 refreshProjectFolderAction.run();
						 logger.debug("NumericFormats Refresh");
					 }
				 }
				 
			}
		});
		
		
		ILabelDecorator labelDecorator = PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator();
	
		DecoratingStyledCellLabelProvider labelProvider = new DecoratingStyledCellLabelProvider(new MyStyledLabelProvider(), labelDecorator, DecorationContext.DEFAULT_CONTEXT);
				
		viewer.setLabelProvider(labelProvider);
		
		//viewer.setLabelProvider(new TreeLabelProvider());
		
		// TTN-761 Set auto-expand for project tree
		refreshProjectStructure();
			
		//This job loads the XML asynchronously
		List<IProject> childProjects = rootNode.getChildProjects();
		UpgradeProjectJobChangeAdapter jobChangeAdapter = new UpgradeProjectJobChangeAdapter(this, childProjects.size());
		for(IProject project : childProjects){
			new UpgradeProjectJob(this, project, 20, 1, jobChangeAdapter);
		}
		
		//This code uses one synchronously jobs to load all the xml
//		UpgradeProjectsJob job = new UpgradeProjectsJob(this);
//		job.setPriority(20);//10
//		job.schedule(1);
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				
				createContextMenu(event.getSelection());
								
			}
						
		});
		
//		selectTopViewerNode();
				
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			public void doubleClick(DoubleClickEvent event) {

						IStructuredSelection selection = (IStructuredSelection) event.getSelection();

						Object object = selection.getFirstElement();
						Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
						
						if (object instanceof ViewsNode || object instanceof ViewSectionsNode || object instanceof ProjectNode
								|| object instanceof RolesProcessesNode || object instanceof PlanCyclesNode 
								|| object instanceof PlannerRolesNode || object instanceof SeasonsNode
								|| object instanceof RoleConfigurationsNode || object instanceof UsersSecurityNode  
								|| object instanceof UserSecurityDomainNode || object instanceof ViewGroupsNode
								|| object instanceof ConditionalStylesNode || object instanceof ConditionalFormatsNode
								|| object instanceof StylesFormatsNode || object instanceof PrintStylesNode )//TNN 900 - added by Iris
						{
								
							if(object instanceof ProjectNode){
								final ProjectNode node = (ProjectNode) object;
								if(!node.isBuilt()){
									loadProject(node);
									return;
								}
							}
						
							if (viewer.getExpandedState(object)) {
								viewer.collapseToLevel(object, 1);
							} else {
																
								if ( object instanceof ViewsNode || object instanceof ViewSectionsNode  ) {
																	
									updateTreeObject((TreeNode) object);
									
								}					
								
								viewer.expandToLevel(object, 1);
								viewer.refresh(object);
							}
							
						} else if (object instanceof ViewNode) {
							
							ViewNode viewNode = (ViewNode) object;
							
							Object parent = null;
							ProjectNode projectNode = null;
							
							if(viewNode.getParent() instanceof ViewsNode){
								//projectNode = (ProjectNode) viewNode.getParent().getParent();
								//dialog = new ViewDialog(shell, projectNode.getProject(), viewNode.getName());
								parent = (ProjectNode) viewNode.getParent().getParent();
								projectNode = (ProjectNode) parent;
								ViewDialog dialog = new ViewDialog(shell, projectNode.getProject(), viewNode.getName());
								dialog.open();
							}else if(viewNode.getParent() instanceof ViewGroupNode){
								//parent = (ViewGroupNode) viewSectionNode.getParent();
								//dialog = new ViewSectionDialog(shell, ((ViewGroupNode)parent).getProject(), viewSectionNode.getName());
								parent = (ViewGroupNode) viewNode.getParent();
								//ViewSectionDialog dialog = new ViewSectionDialog(shell, ((ViewGroupNode)parent).getProject(), viewNode.getViewSections().get(0));
								ViewDialog dialog = new ViewDialog(shell, ((ViewGroupNode)parent).getProject(), viewNode.getName());
								dialog.open();
							}
							
							if(projectNode != null){
								for (Object obj : projectNode.getChildren()) {
									
									if ( obj instanceof ViewsNode || obj instanceof ViewSectionsNode  ) {
										
										updateTreeObject((TreeNode) obj);
									
									}							
								}
							}
							viewer.refresh();
							
						} else if (object instanceof ViewSectionNode){							
							
							ViewSectionNode viewSectionNode = (ViewSectionNode) object;
							Object parent = null;
							ProjectNode projectNode = null;
							
							if(viewSectionNode.getParent() instanceof ViewSectionsNode){
								//ViewSectionsNode parent = (ViewSectionsNode) viewSectionNode.getParent();
								//ViewSectionDialog dialog = new ViewSectionDialog(shell, parent.getProject(), viewSectionNode.getName());
								//dialog.open();
								parent = (TreeNode) viewSectionNode.getParent();
								projectNode = (ProjectNode)((TreeNode) parent).getParent();
								ViewSectionDialog dialog = new ViewSectionDialog(shell, projectNode.getProject(), viewSectionNode.getName());
								dialog.open();
								
							}else if(viewSectionNode.getParent() instanceof ViewNode){
								ViewNode brother  = (ViewNode) viewSectionNode.getParent();
								parent = (ViewGroupNode) brother.getParent();
								ViewSectionDialog dialog = new ViewSectionDialog(shell, ((ViewGroupNode)parent).getProject(), viewSectionNode.getName());
								dialog.open();
							}
							
							
							if(projectNode != null){
								for (Object obj : projectNode.getChildren()) {
									
									if ( obj instanceof ViewsNode || obj instanceof ViewSectionsNode  ) {
										
										updateTreeObject((TreeNode) obj);
									
									}							
								}
							}
							
//							//Original Code
//							ViewSectionsNode parent = (ViewSectionsNode) viewSectionNode.getParent();
//							ViewSectionDialog dialog = new ViewSectionDialog(shell, parent.getProject(), viewSectionNode.getName());
//							dialog.open();
							
//							updateTreeObject((TreeNode) parent);
														
						} else if (object instanceof NumericFormatWizardNode) {

							NumericFormatWizardNode node = (NumericFormatWizardNode) object;
							ProjectNode parent = (ProjectNode) node.getParent().getParent().getParent().getParent();
							NumericFormatsDialog dialog = new NumericFormatsDialog(shell, parent.getProject());
							dialog.open();
							
							
						} else if ( object instanceof GlobalStylesWizardNode ) {
							
							GlobalStylesWizardNode node = (GlobalStylesWizardNode) object;
							ProjectNode parentNode = (ProjectNode) node.getParent().getParent().getParent().getParent();
							GlobalStylePropertiesDialog dialog = new GlobalStylePropertiesDialog(parent.getShell(), parentNode.getProject());
							dialog.open();
							
							
						} else if ( object instanceof UserSelectionDialogNode ) {
							
							UserSelectionDialogNode node = (UserSelectionDialogNode) object;
							ProjectNode parentNode = (ProjectNode) node.getParent();
							
							try {
								UserSelectionsDialog dialog = new UserSelectionsDialog(shell, parentNode.getProject(), false);
								dialog.open();
							} catch (RuntimeException re) {
								
								GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, re.getMessage(), MessageDialog.ERROR);
								
							}
							
						} else if ( object instanceof DynamicMembersDialogNode ) {
							
							DynamicMembersDialogNode node = (DynamicMembersDialogNode) object;
							ProjectNode parentNode = (ProjectNode) node.getParent();
							
							
							
							XMLPaceProject pp;
							try {
								
								pp = new XMLPaceProject(
										getConfDir(parentNode.getProject()), 
										new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.DynamicMembers)),
												false);
								
								//try to run dynamic members migration action
								new PafDynamicMembersMigrationAction(pp).run();
								
								DynamicMembersDialog dialog = new DynamicMembersDialog(shell, parentNode.getProject());
								
								dialog.open();
								
							} catch (InvalidPaceProjectInputException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (PaceProjectCreationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						} else if ( object instanceof MemberTagDialogNode ) {
							
							MemberTagDialogNode node = (MemberTagDialogNode) object;
							final ProjectNode parentNode = (ProjectNode) node.getParent();
																												
							//use an hourglass and start up the tuples dialog
							BusyIndicator.showWhile(parent.getDisplay(),
									new Runnable() {
										public void run() {
											try {
												
												MemberTagsDialog dialog = new MemberTagsDialog(parent.getShell(), parentNode.getProject());
												dialog.open();
												
											} catch (ServerNotRunningException sne ) {
												
												GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, sne.getMessage(), MessageDialog.ERROR);
												
											} catch (Exception e1) {
												//TTN 1723 - Member Tag wizard is throwing null pointer exception when no default project server nor default server defined 		
												GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1.getMessage(), MessageDialog.ERROR);
												
											}
										}
									});
							
						} else if ( object instanceof HierarchyFormattingWizardNode ) {
							
							HierarchyFormattingWizardNode node = (HierarchyFormattingWizardNode) object;
							final ProjectNode parentNode = (ProjectNode) node.getParent().getParent().getParent().getParent();
														
//							use an hourglass and start up the tuples dialog
							BusyIndicator.showWhile(parent.getDisplay(),
									new Runnable() {
										public void run() {
											try {
												
												HierarchyFormattingDialog dialog = new HierarchyFormattingDialog(parent.getShell(), parentNode.getProject());
												dialog.open();
												
											} catch (ServerNotRunningException sne ) {
												
												GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, sne.getMessage(), MessageDialog.ERROR);
												
											} catch (Exception e1) {
												//TTN 1723 - Hierarchy Formatting Wizard is throwing null exception when no default project server nor default server defined 		
												GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e1.getMessage(), MessageDialog.ERROR);
												
											}
										}
									});
							
						} else if (object instanceof PrintStyleNode) {

							PrintStyleNode node = (PrintStyleNode) object;
							PrintStylesNode parent = (PrintStylesNode) node.getParent();
							printStyleAction.setProject(parent.getProject());
							printStyleAction.run();
						} else if (object instanceof PlanCycleNode) {

							PlanCycleNode node = (PlanCycleNode) object;
							PlanCyclesNode parent = (PlanCyclesNode) node.getParent();
							logger.debug("Double Clicked(PlanCycleNode): " + node.getName());
							planCycleAction.setProject(parent.getProject());
							planCycleAction.run();
							
						} else if (object instanceof PlannerRoleNode) {

							PlannerRoleNode node = (PlannerRoleNode) object;
							PlannerRolesNode parent = (PlannerRolesNode) node.getParent();
							logger.debug("Double Clicked(PlannerRoleNode): " + node.getName());
							plannerRoleAction.setProject(parent.getProject());
							plannerRoleAction.run();
							
						}	else if (object instanceof SeasonNode) {

							SeasonNode node = (SeasonNode) object;
							SeasonsNode parent = (SeasonsNode) node.getParent();
							logger.debug("Double Clicked(SeasonNode): " + node.getName());
							seasonAction.setProject(parent.getProject());
							seasonAction.run();
							
						}	else if (object instanceof RoleConfigurationNode) {

							RoleConfigurationNode node = (RoleConfigurationNode) object;
							RoleConfigurationsNode parent = (RoleConfigurationsNode) node.getParent();
							logger.debug("Double Clicked(RoleConfigurationNode): " + node.getName());
							roleConfigurationAction.setProject(parent.getProject());
							roleConfigurationAction.run();
							
						} else if ( object instanceof UserSecurityNode ) {
							
							UserSecurityNode node = (UserSecurityNode) object;
							UsersSecurityNode parent = (UsersSecurityNode) node.getParent().getParent();
							ProjectNode projectNode = (ProjectNode) parent.getParent().getParent();
							
							logger.debug("Double Clicked(UserSecurityNode): " + node.getName());
							
							userSecurityAction.setProject(projectNode.getProject());
							userSecurityAction.run();
							
							
						} else if ( object instanceof UserSecurityRoleNode ) {
							
							UserSecurityRoleNode node = (UserSecurityRoleNode) object;
							ProjectNode projectNode = (ProjectNode) node.getParent().getParent().getParent().getParent().getParent();
														
							logger.debug("Double Clicked(UserSecurityRoleNode): " + node.getName());
							
							userSecurityAction.setProject(projectNode.getProject());
							userSecurityAction.run();
							
						} else if ( object instanceof UserSecurityDimensionNode ) {
							
							UserSecurityDimensionNode node = (UserSecurityDimensionNode) object;
							ProjectNode projectNode = (ProjectNode) node.getParent().getParent().getParent().getParent().getParent().getParent();
														
							logger.debug("Double Clicked(UserSecurityDimensionNode): " + node.getName());
							
							userSecurityAction.setProject(projectNode.getProject());
							userSecurityAction.run();							
						} else if ( object instanceof UserSecuritySpecificationNode ) {
							
							UserSecuritySpecificationNode node = (UserSecuritySpecificationNode) object;
							ProjectNode projectNode = (ProjectNode) node.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
														
							logger.debug("Double Clicked(UserSecuritySpecificationNode): " + node.getName());
							
							userSecurityAction.setProject(projectNode.getProject());
							userSecurityAction.run();							
						} else if ( object instanceof ViewGroupNode ) {
							
							ViewGroupNode node = (ViewGroupNode) object;
							
							logger.debug("Double Clicked(ViewGroupNode): " + node.getName());
							
							openViewGroupAction.setProject(node.getProject());
							openViewGroupAction.run();
							
						} else if ( object instanceof ProjectSettingsWizardNode ) {
							IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
							try {
								handlerService.executeCommand("com.pace.admin.menu.project_settings_command", null);
							} catch (Exception ex) {
								throw new RuntimeException("ProjectSettingsCommand not found");
								// Give message
							}
						} else if ( object instanceof ConditionalFormatNode ) {
							ConditionalFormatNode node = (ConditionalFormatNode) object;
							ProjectNode parent = (ProjectNode) node.getParent().getParent().getParent().getParent();
							condFormatAction.setProject(parent.getProject());
							condFormatAction.run();
						} else if ( object instanceof ConditionalStyleNode ) {
							ProjectNode parent = null;
							ConditionalStyleNode node = (ConditionalStyleNode) object;
							if(node.getParent() instanceof ConditionalStylesNode){
								parent = (ProjectNode) node.getParent().getParent().getParent().getParent();
							}else if(node.getParent() instanceof ConditionalFormatNode){
								parent = (ProjectNode) node.getParent().getParent().getParent().getParent().getParent();
							}
							condStyleAction.setProject(parent.getProject());
							condStyleAction.run();
						} else if ( object instanceof AdminLockNode ) {
							AdminLockNode node = (AdminLockNode) object;
							ProjectNode parent = (ProjectNode) node.getParent().getParent().getParent();
							openAdminLockAction.setProject(parent.getProject());
							openAdminLockAction.run();
						}
			}
		});

		getSite().setSelectionProvider(viewer);
		createActions();
		getViewSite().getActionBars().getToolBarManager().add(hotProjectDeployAction);
		getViewSite().getActionBars().getToolBarManager().add(hotProjectRefreshAction);
		getViewSite().getActionBars().getToolBarManager().add(refreshAction);
		getViewSite().getActionBars().getToolBarManager().add(expandAllAction);
		getViewSite().getActionBars().getToolBarManager().add(collapseAllAction);
		
		//register with observer to get project updates
		ProjectMonitor.getInstance().addObserver(this);
		
		//TTN-1450, 2140 - refresh project tree if any file has been changed
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IResourceChangeListener listener = new IResourceChangeListener() {
			public void resourceChanged(IResourceChangeEvent event) {
				try {
					final List<IResource> changed = new ArrayList<IResource>();
					if (event.getType() == IResourceChangeEvent.POST_CHANGE ) {
						event.getDelta().accept(new IResourceDeltaVisitor() {
							public boolean visit(IResourceDelta delta)	throws CoreException {
								IResource resource = delta.getResource();
								if ( resource.getType() == IResource.PROJECT 
										&& ( delta.getKind() == IResourceDelta.ADDED
											|| delta.getKind() == IResourceDelta.REMOVED) ) {
									changed.add(resource);
								}
								return true;
							}
						});
						if (changed.size() == 0)
							return;
						// post this update to the table
						Display display = parent.getDisplay();
						if ( !display.isDisposed() ) {
							display.asyncExec(new Runnable() {
								public void run() {
												
									ConsoleWriter.writeMessage("Refreshing Project View  at " + Calendar.getInstance().getTime());
									refreshProjectStructure();
									
								}
							});
						}
					}
				} catch (CoreException e) {
					// log it
					e.printStackTrace();
				}
			}
		};
		workspace.addResourceChangeListener(listener);

	}	
	
	public void refreshProjectStructure(){
		
		boolean isLoading = true;
		//if not null
		if (viewer != null ) {

			//refresh viewer
//			viewer.setSelection(null);
			if(rootNode != null){
				isLoading = false;
			}
			
			rootNode = createProjectStructures();
			viewer.setInput(rootNode);
			
			// TTN-761
			if(!isLoading && PafProjectUtil.getExpandProjectTree()){
				loadProjects(1);
			} 		
		}
	}
	
	public void selectTopViewerNode() {

		if (viewer != null) {

			Object rootObject = viewer.getInput();

			// if object exist and is type of root node, try to select first
			// project
			if (rootObject != null && rootObject instanceof RootNode) {

				RootNode rootNode = (RootNode) rootObject;

				List children = rootNode.getChildren();

				// if children exists
				if (children != null && children.size() > 0) {

					// select first child
					viewer.setSelection(new StructuredSelection(children
									.get(0)));

				}

			}
		}

	}
	
	private void selectProjectNode(ProjectNode projectNode) {

		// if object exist and is type of root node, try to select first project
		if (projectNode != null) {

			if (viewer != null) {

				// select first child
				viewer.setSelection(new StructuredSelection(projectNode));

			}

		}

	}
	
	public void selectProjectNode(String name) {
	
		if (viewer != null) {

			Object rootObject = viewer.getInput();

			// if object exist and is type of root node, try to select first
			// project
			if (rootObject != null && rootObject instanceof RootNode) {

				RootNode rootNode = (RootNode) rootObject;

				List children = rootNode.getChildren();

				// if children exists
				if (children != null && children.size() > 0) {

					for ( Object child : children) {
						
						if ( child instanceof ProjectNode ) {
							
							ProjectNode projectNode = (ProjectNode) child;
							
							if ( projectNode.getName().equalsIgnoreCase(name)) {
								selectProjectNode(projectNode);
								break;
							}
							
						}
						
					}
							

				} 

			}
		}

		
	}

	private RootNode createProjectStructures() {
		
		StopWatch sw = new StopWatch();
		sw.start();
		logger.info("Creating project structures");
		
		RootNode root = new RootNode(null);
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot projectWSRoot = workspace.getRoot();
		File workspaceRoot = new File(projectWSRoot.getLocation().toString());
				
		//if workpace root is not null and is in fact a dir
		if ( workspaceRoot != null && workspaceRoot.isDirectory() ) {
		
			//list files in workspace folders
			for (File projectFolder : workspaceRoot.listFiles()	) {
			
				//if workspace file is a dir and doesn't start with a .
				if ( projectFolder.isDirectory() && ! projectFolder.getName().equals(Constants.META_DATA_FOLDER_NAME)) {
					String projectName = projectFolder.getName();
					//add existing project work project set
					
					IProject project = projectWSRoot.getProject(projectName);
					
					//handles project folder that does not have project description file
					if( ! project.exists() ) {
						try {
							
							IProjectDescription projectDesc = ResourcesPlugin.getWorkspace().newProjectDescription(projectName);

							project.create(projectDesc, null);
										
							//try to open project
							openIProject(project);

						} catch (CoreException e) {
							logger.error("Problem creating or refreshing new project.  Error: " + e.getMessage());
						}
					}
					
					//handles project folder does not have resource files
					if( project.isAccessible() ) {
						
						String confDir = getConfDir(project);
						File confFolder = new File(confDir);
						if( ! confFolder.exists() ) {
							ConsoleWriter.writeMessage("Missing 'Conf' folder in project: " + projectName + ".");
//					    	GUIUtil.openMessageWindow( "Error Opening Project",
//						    		"Missing 'Conf' folder in project: " + projectName + ".", MessageDialog.ERROR);
							try {
								project.delete(true, null);
							} catch (CoreException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					    	continue;
						}
					}
					
					/** TTN-2527
					try {
						PafProjectUtil.addPafProjectMap(project);
					} catch (PafServerNotFound e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if( PafProjectUtil.isProjectAutoConvert(project) != null && PafProjectUtil.isProjectAutoConvert(project) ) {
						autoConvertProject(project);
					}
					**/
						
					//add project to root node
					root.addProject(new ProjectNode(root, project));
					
				}
			}
						
		}							
				
		//if projects don't exist
		if ( projectWSRoot.getProjects().length == 0 ) {
														
			createContextMenu(null);
			
		}
	
		sw.stop();
		logger.info(String.format("Took %s ms to load projects.", sw.getTotalTimeMillis()));
		return root;
	}

	/**
	 * 
	 * Converts a project into the new format
	 *
	 * @param project current project
	 */
	public void autoConvertProject(IProject project) {

		StopWatch sw = new StopWatch();
		sw.start();

		try {
			PafProjectUtil.addPafProjectMap(project);
		} catch (PafServerNotFound e) {
			logger.info(String.format("Cannot find server: %s ", e.getMessage()));
		}
		
		if( PafProjectUtil.isProjectAutoConvert(project) != null && PafProjectUtil.isProjectAutoConvert(project) ) {
			PafProjectUpgradeUtil.autoConvertProject(project, getConfDir(project));
		}

		sw.stop();
		logger.info(String.format("Took %s (ms) to auto convert project: %s", sw.getTotalTimeMillis(), project.getName()));
	}

	private String getConfDir(IProject project) {
		
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		
		return iConfFolder.getLocation().toString();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	public void createActions() {
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		openNewViewAction = new OpenNewViewAction("Create New View", window, viewer);
		openNewViewSectionAction = new OpenNewViewSectionAction("Create New View Section", window, viewer);
		deleteViewAction = new DeleteViewAction("Delete View", window, viewer);
		deleteViewSectionAction = new DeleteViewSectionAction("Delete View Section", window, viewer);
		copyViewAction = new CopyViewAction("Copy View", window, viewer);
		copyViewSectionAction = new CopyViewSectionAction("Copy View Section", window, viewer);
		renameViewAction = new RenameViewAction(window);
		renameViewSectionAction = new RenameViewSectionAction(window);
		refreshAction = new RefreshProjectsAction(window);
		refreshProjectFolderAction = new RefreshProjectFolderAction(window);
		copyProjectAction = new CopyProjectAction(window);
//		chooseColorsAction = new ChooseColorsAction(window);
		planCycleAction = new PlanCycleAction(window, viewer);
		plannerRoleAction = new PlannerRoleAction(window, viewer);
		userSecurityAction = new UserSecurityAction(window, viewer);
		cloneUserSecurityAction = new UserSecurityAction(window, viewer, true);
		
		deleteUserSecurityAction = new DeleteUserSecurityAction(window, viewer);
		openNewUserSecurityAction = new OpenNewUserSecurityAction(window, viewer);
		validateUserSecurityAction = new ValidateUserSecurityAction(window, viewer);
		seasonAction = new SeasonAction(window, viewer);
		roleConfigurationAction = new RoleConfigurationAction(window, viewer); 
		
		openNewPlanCycleAction = new OpenNewPlanCycleAction("Create a new Plan Cycle", window, viewer);
		renamePlanCycleAction = new RenamePlanCycleAction("Rename Plan Cycle", window, viewer);
		deletePlanCycleAction = new DeletePlanCycleAction("Delete Plan Cycle", window, viewer);
		
		openNewSeasonAction = new OpenNewSeasonAction("Create a new " + Constants.MENU_SEASON, window, viewer);
		renameSeasonAction = new RenameSeasonAction("Rename " + Constants.MENU_SEASON, window, viewer);
		deleteSeasonAction = new DeleteSeasonAction("Delete " + Constants.MENU_SEASON,window, viewer);
		
		openNewPlannerRoleAction = new OpenNewPlannerRoleAction("Create a new Role", window, viewer);
		renamePlannerRoleAction = new RenamePlannerRoleAction("Rename Role", window, viewer);
		deletePlannerRoleAction = new DeletePlannerRoleAction("Delete Role", window, viewer);
		
		openNewRoleConfigurationAction = new OpenNewRoleConfigurationAction("Create a new Role Configuration", window, viewer);
		deleteRoleConfigurationAction = new DeleteRoleConfigurationAction("Delete Role Configuration",window, viewer);
		
		renameViewGroupAction = new RenameViewGroupAction(window);
		copyViewGroupAction = new CopyViewGroupAction(window);
		openViewGroupAction = new OpenViewGroupAction(window, viewer);
		openNewViewGroupAction = new OpenNewViewGroupAction(window, viewer);
		deleteViewGroupAction = new DeleteViewGroupAction(window, viewer);
		expandAllAction = new ExpandAllProjectsAction();
		collapseAllAction = new CollapseAllAction();
		//collapseAllAction.setToolTipText("Collapse All Projects");
			
		hotProjectDeployAction = new HotUploadProjectToServerAction(window);
		hotProjectRefreshAction = new HotDownloadProjectFromServer(window);
		
		newProjectAction = new NewProjectButtonAction(window);
		deleteProjectAction = new DeleteProjectAction(window);
		autoConvertProjectAction = new ToggleAutoConvertProjectAction(window);
		importProjectAction = ActionFactory.IMPORT.create(window);
		importAndDeployRulesetAction = new ImportAndDeployRulesetsAction("Import and Deploy Rulesets", window, null);  
		exportProjectAction = ActionFactory.EXPORT.create(window);
		
		//TTN - Print Preference - added by Iris
		printStyleAction = new PrintStyleAction(window, viewer);
		openNewPrintStyleAction  = new OpenNewPrintStyleAction("Create New Print Style", window, viewer);
		
		//TTN-2222 HeatMap Formatting
		condFormatAction = new ConditionalFormatAction( window, viewer);
		condStyleAction = new ConditionalStyleAction(window, viewer);
		createCondtionalStyleAction = new CreateNewConditionalStyleAction("Create New Conditional Style", window, viewer);
		createCondtionalFormatAction = new CreateNewConditionalFormatAction("Create New Conditional Format", window, viewer);
		deleteCondtionalStyleAction = new DeleteConditionalStyleAction("Delete Conditional Style", window, viewer);
		deleteCondtionalFormatAction = new DeleteConditionalFormatAction("Delete Conditional Format", window, viewer);
		copyCondtionalStyleAction = new CopyConditionalStyleAction("Copy Conditional Style", window, viewer);
		copyCondtionalFormatAction = new CopyConditionalFormatAction("Copy Conditional Format", window, viewer);
		
		openAdminLockAction = new AdminLockAction( window, viewer);
		createAdminLockAction = new CreateNewAdminLockAction(window, viewer);
		deleteAdminLockAction = new DeleteAdminLockAction(window, viewer);
		copyAdminLockAction = new CopyAdminLockAction(window, viewer);
		
		createServerRelatedActions();
	}

	public void createServerRelatedActions() {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		Set<PafServer> servers = PafServerUtil.getPafServers();
		
		if (servers != null && servers.size() > 0 ) {
															
			uploadProjectToServerActions = new UploadProjectToServerAction[servers.size()];
			deployViewsToServerActions = new UploadViewsToServerAction[servers.size()];
			deployViewSectionsToServerActions = new UploadViewSectionsToServerAction[servers.size()];
			uploadUserSecurityActions = new UploadUserSecurityAction[servers.size()];

			downloadProjectFromServerActions = new DownloadProjectFromServerAction[servers.size()];
			downloadViewsFromServerActions = new DownloadViewsFromServerAction[servers.size()];
			downloadViewSectionsFromServerActions = new DownloadViewSectionsFromServerAction[servers.size()];
			selectDefaultServerProjectActions = new SelectDefaultServerProjectAction[servers.size()];
			
			downloadPrintStylesActions = new DownloadPrintStylesAction[servers.size()];
			uploadPrintStylesActions = new UploadPrintStylesAction[servers.size()];

			downloadCubeChangesFromServerActions = new DownloadCubeChangesFromServerAction[servers.size()];
			
			int serverCnt = 0;
			
			for (PafServer server : servers) {
				
				uploadProjectToServerActions[serverCnt] = new UploadProjectToServerAction(window, server);
				deployViewsToServerActions[serverCnt] = new UploadViewsToServerAction(window, server);
				deployViewSectionsToServerActions[serverCnt] = new UploadViewSectionsToServerAction(window, server);
				uploadUserSecurityActions[serverCnt] = new UploadUserSecurityAction(window, server);
				
				downloadProjectFromServerActions[serverCnt] = new DownloadProjectFromServerAction(window, server);
				downloadViewsFromServerActions[serverCnt] = new DownloadViewsFromServerAction(window, server, viewer);
				downloadViewSectionsFromServerActions[serverCnt] = new DownloadViewSectionsFromServerAction(window, server, viewer);
			
				downloadPrintStylesActions[serverCnt] = new DownloadPrintStylesAction(window, server);
				uploadPrintStylesActions[serverCnt] = new UploadPrintStylesAction(window, server);
				selectDefaultServerProjectActions[serverCnt] = new SelectDefaultServerProjectAction(window, server);

				downloadCubeChangesFromServerActions[serverCnt] = new DownloadCubeChangesFromServerAction(window, server);
				serverCnt++;
			}
		
		} 
		
		List<String> serverGroups = PafServerUtil.getServerGroups();
		int serverCnt = 0;
		if (serverGroups != null && serverGroups.size() > 0 ) {
			
			uploadProjectToServerGroupActions = new UploadProjectToServerGroupAction[serverGroups.size()];
			deployViewsToServerGroupActions = new UploadViewsToServerGroupAction[serverGroups.size()];
			deployViewSectionsToServerGroupActions = new UploadViewSectionsToServerGroupAction[serverGroups.size()];
			uploadPrintStylesToServerGroupActions = new UploadPrintStylesToServerGroupAction[serverGroups.size()];
			

			for (String group : serverGroups) {
			
				List<PafServer> groupServers = PafServerUtil.getServers(group);
				
				uploadProjectToServerGroupActions[serverCnt] = new UploadProjectToServerGroupAction(window, group, groupServers);
				deployViewsToServerGroupActions[serverCnt] = new UploadViewsToServerGroupAction(window, group, groupServers);
				deployViewSectionsToServerGroupActions[serverCnt] = new UploadViewSectionsToServerGroupAction(window, group, groupServers); 	
				uploadPrintStylesToServerGroupActions[serverCnt]  = new UploadPrintStylesToServerGroupAction(window, group, groupServers); 
				
				serverCnt++;
			}
			
		} else {
			uploadProjectToServerGroupActions = new UploadProjectToServerGroupAction[0];
			deployViewsToServerGroupActions = new UploadViewsToServerGroupAction[0];
			deployViewSectionsToServerGroupActions = new UploadViewSectionsToServerGroupAction[0]; 
			uploadPrintStylesToServerGroupActions = new UploadPrintStylesToServerGroupAction[0];
		}
			
	}

	private void createContextMenu(final ISelection iSelection) {
		
		logger.debug("Creating Context Menu");
		final MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			@SuppressWarnings("unchecked")
			public void menuAboutToShow(IMenuManager m) {

			
				if ( iSelection != null ) {
				
					IStructuredSelection selection = (IStructuredSelection) iSelection;
					
					fillDefaultContextMenu(m);
					
					if ( ! selection.isEmpty() ) {
						
						Object object = selection.getFirstElement();
						Object[] objects = selection.toArray();
						
						ITreeNode selNode = null;
						if(object instanceof ITreeNode){
							ITreeNode treeNode = (ITreeNode)object;
							if(treeNode.hasChildren()){
								menuMgr.insertBefore(RefreshProjectsAction.ID, refreshProjectFolderAction);
							}
						}
						
						Set<PafServer> runningServerSet = new HashSet<PafServer>();
						
						if ( object instanceof ProjectNode || object instanceof UsersSecurityNode || object instanceof UserSecurityNode ||
								object instanceof ViewsNode || object instanceof ViewNode || object instanceof ViewSectionsNode ||
								object instanceof ViewSectionNode || object instanceof PrintStylesNode || object instanceof PrintStyleNode) {
							
							runningServerSet = PafServerUtil.getRunningPafServers();
							
						}
						
						boolean nodesHaveSameParents = allNodesHaveSameParents(objects);
						
						logger.debug(object.getClass());
						
						if ( object instanceof ProjectNode ) {
	
							copyProjectAction.setProject(((ProjectNode) object).getProject());
//							chooseColorsAction.setProject(((ProjectNode) object).getProject());
							if(selection.size() > 1 || !nodesHaveSameParents){
								copyProjectAction.setEnabled(false);
//								chooseColorsAction.setEnabled(false);
							} else{
								copyProjectAction.setEnabled(true);
//								chooseColorsAction.setEnabled(true);
							}
							fillProjectContextMenu(m, nodesHaveSameParents, runningServerSet);					
							
						} else if ( object instanceof ViewsNode ) {
							
							openNewViewAction.setProject(((ViewsNode) object).getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewViewAction.setEnabled(false);
								fillViewsContextMenu(m, false, runningServerSet);	
							} else{
								openNewViewAction.setEnabled(true);
								fillViewsContextMenu(m, true, runningServerSet);	
							}
							//fillViewsContextMenu(m);	
							
						} else if (object instanceof ViewNode ) {
							
							if( ((ViewNode) object).getParent() instanceof ViewsNode ) {
								ViewNode viewNode = (ViewNode) object;
								
								Object parent = viewNode.getParent();
								ProjectNode  projectNode = null;
								IProject project = null;
								
								if(parent instanceof ViewsNode){
									projectNode = (ProjectNode) ((ViewsNode) viewNode.getParent()).getParent();
									project = projectNode.getProject();
								} else if (parent instanceof ViewGroupNode){
									parent = (ViewGroupNode) viewNode.getParent();
									project = ((ViewGroupNode)parent).getProject();
								}
								
								deleteViewAction.setProject(project);
								deleteViewAction.setViewName(objects);
								
								if(selection.size() == 1){
									deleteViewAction.setEnabled(true);
									copyViewAction.setEnabled(true);
									copyViewAction.setProject(project);
									copyViewAction.setViewName(viewNode.getName());
									renameViewAction.setEnabled(true);
									renameViewAction.setProject(project);
								} else if(selection.size() > 1 && !nodesHaveSameParents){
									deleteViewAction.setEnabled(false);
									copyViewAction.setEnabled(false);
									renameViewAction.setEnabled(false);
								} else{
									//if the user selected more than one node, we can't copy that...
									//so disable the action.
									copyViewAction.setEnabled(false);
									renameViewAction.setEnabled(false);
								}
								
								fillViewContextMenu(m, nodesHaveSameParents, runningServerSet);	
							}
							
						} else if ( object instanceof ViewSectionsNode ) {
							
							openNewViewSectionAction.setProject(((ViewSectionsNode) object).getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewViewSectionAction.setEnabled(false);
								fillViewSectionsContextMenu(m, false, runningServerSet);
							} else{
								openNewViewSectionAction.setEnabled(true);
								fillViewSectionsContextMenu(m, true, runningServerSet);
							}
							
						} else if ( object instanceof ViewSectionNode ) {
							
							if( ((ViewSectionNode) object).getParent() instanceof ViewSectionsNode ) {
								ViewSectionNode viewSectionNode = (ViewSectionNode) object;
								Object parent = viewSectionNode.getParent();
								ProjectNode  projectNode = null;
								IProject project = null;
								
								if(parent instanceof ViewSectionsNode){
									projectNode = (ProjectNode) ((ViewSectionsNode) viewSectionNode.getParent()).getParent();
									project = projectNode.getProject();
								} else if(parent instanceof ViewNode){
									ViewNode brother  = (ViewNode) viewSectionNode.getParent();
									parent = (ViewGroupNode) brother.getParent();
									project = ((ViewGroupNode)parent).getProject();
								}
		
								deleteViewSectionAction.setProject(project);
								deleteViewSectionAction.setViewSectionName(objects);
								
								if(selection.size() == 1){
									deleteViewSectionAction.setEnabled(true);
									copyViewSectionAction.setEnabled(true);
									copyViewSectionAction.setProject(project);
									copyViewSectionAction.setViewSectionName(viewSectionNode.getName());
									renameViewSectionAction.setEnabled(true);
									renameViewSectionAction.setProject(project);
								} else if(selection.size() > 1 && !nodesHaveSameParents){
									deleteViewSectionAction.setEnabled(false);
									copyViewSectionAction.setEnabled(false);
									renameViewSectionAction.setEnabled(false);
								}else{
									//if the user selected more than one node, we can't copy that...
									//so disable the action.
									copyViewSectionAction.setEnabled(false);
									renameViewSectionAction.setEnabled(false);
								}
								
								fillViewSectionContextMenu(m, nodesHaveSameParents, runningServerSet);
							}
							
						} else if ( object instanceof PlanCyclesNode ) {
							
							logger.debug("Right click on PlanCyclesNode.");
					
							PlanCyclesNode node = (PlanCyclesNode) object;
							openNewPlanCycleAction.setProject(node.getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewPlanCycleAction.setEnabled(false);
							} else{
								openNewPlanCycleAction.setEnabled(true);
							}
							fillPlanCyclesContextMenu(m);
							
						} else if ( object instanceof PlanCycleNode ) {
							
							logger.debug("Right click on PlanCycleNode.");
							PlanCycleNode node = (PlanCycleNode) object;
							PlanCyclesNode parent = (PlanCyclesNode) node.getParent();
							renamePlanCycleAction.setProject(parent.getProject());
							renamePlanCycleAction.setPlanCycleName(node.getName());
							deletePlanCycleAction.setProject(parent.getProject());
							deletePlanCycleAction.setPlanCycleName(node.getName());
							//only allow one view group to be selected, because the 
							//actions only handle one.
							if(selection.size() > 1 || !nodesHaveSameParents){
								renamePlanCycleAction.setEnabled(false);
								deletePlanCycleAction.setEnabled(false);
							} else{
								renamePlanCycleAction.setEnabled(true);
								deletePlanCycleAction.setEnabled(true);
							}
							fillPlanCycleContextMenu(m);
							
						} else if ( object instanceof SeasonsNode ) {
							
							logger.debug("Right click on SeasonsNode.");
							SeasonsNode node = (SeasonsNode) object;
							openNewSeasonAction.setProject(node.getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewSeasonAction.setEnabled(false);
							} else{
								openNewSeasonAction.setEnabled(true);
							}
							fillSeasonsContextMenu(m);
							
						} else if ( object instanceof SeasonNode ) {
							
							logger.debug("Right click on SeasonNode.");
							SeasonNode node = (SeasonNode) object;
							SeasonsNode parent = (SeasonsNode) node.getParent();
							renameSeasonAction.setProject(parent.getProject());
							renameSeasonAction.setSeasonName(node.getName());
							deleteSeasonAction.setProject(parent.getProject());
							deleteSeasonAction.setSeasonName(node.getName());
							//only allow one view group to be selected, because the 
							//actions only handle one.
							if(selection.size() > 1 || !nodesHaveSameParents){
								renameSeasonAction.setEnabled(false);
								deleteSeasonAction.setEnabled(false);
							} else{
								renameSeasonAction.setEnabled(true);
								deleteSeasonAction.setEnabled(true);
							}
							fillSeasonContextMenu(m);
						} else if ( object instanceof PlannerRolesNode ) {
							
							logger.debug("Right click on PlannerRolesNode.");
							PlannerRolesNode node = (PlannerRolesNode) object;
							openNewPlannerRoleAction.setProject(node.getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewPlannerRoleAction.setEnabled(false);
							} else{
								openNewPlannerRoleAction.setEnabled(true);
							}
							fillPlannerRolesContextMenu(m);
							
						} else if (object instanceof PlannerRoleNode) {		// TTN-2466 - Add rename functionality to Roles & Processes
							logger.debug("Right click on PlannerRoleNode.");
							PlannerRoleNode node = (PlannerRoleNode) object;
							PlannerRolesNode parent = (PlannerRolesNode) node.getParent();
							renamePlannerRoleAction.setProject(parent.getProject());
							renamePlannerRoleAction.setPlannerRoleName(node.getName());
							deletePlannerRoleAction.setProject(parent.getProject());
							deletePlannerRoleAction.setPlannerRoleName(node.getName());
							//only allow one view group to be selected, because the 
							//actions only handle one.
							if(selection.size() > 1 || !nodesHaveSameParents){
								renamePlannerRoleAction.setEnabled(false);
								deletePlannerRoleAction.setEnabled(false);
							} else{
								renamePlannerRoleAction.setEnabled(true);
								deletePlannerRoleAction.setEnabled(true);
							}
							fillPlannerRoleContextMenu(m);
						} else if ( object instanceof RoleConfigurationsNode ) {
							
							logger.debug("Right click on RoleConfigurationsNode.");
							RoleConfigurationsNode node = (RoleConfigurationsNode) object;
							openNewRoleConfigurationAction.setProject(node.getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewRoleConfigurationAction.setEnabled(false);
							} else{
								openNewRoleConfigurationAction.setEnabled(true);
							}
							fillRoleConfigurationsContextMenu(m);
							
						} else if ( object instanceof RoleConfigurationNode ) {
							
							logger.debug("Right click on RoleConfigurationNode.");
							RoleConfigurationNode node = (RoleConfigurationNode) object;
							RoleConfigurationsNode parent = (RoleConfigurationsNode) node.getParent();
							deleteRoleConfigurationAction.setProject(parent.getProject());
							deleteRoleConfigurationAction.setRoleConfigurationName(node.getName());
							//only allow one view group to be selected, because the 
							//actions only handle one.
							if(selection.size() > 1 || !nodesHaveSameParents){
								deleteRoleConfigurationAction.setEnabled(false);
							} else{
								deleteRoleConfigurationAction.setEnabled(true);
							}
							fillRoleConfigurationContextMenu(m);
							
						} else if ( object instanceof UsersSecurityNode ) {
							
							logger.debug("Right click on UsersSecurityNode.");
							UsersSecurityNode node = (UsersSecurityNode) object;
							
							ProjectNode parent = (ProjectNode) node.getParent().getParent();
							
							openNewUserSecurityAction.setProject(parent.getProject());
							validateUserSecurityAction.setProject(parent.getProject());
														
							for ( UploadUserSecurityAction uploadUserSecurityAction : uploadUserSecurityActions) {
								uploadUserSecurityAction.setProject(parent.getProject());
							}
							
							fillUsersSecurityContextMenu(m, runningServerSet);
	
						} else if ( object instanceof UserSecurityNode ) {
							
							logger.debug("Right click on UsersSecurityNode.");
							
							UserSecurityNode node = (UserSecurityNode) object;
							UsersSecurityNode usersSecurityNode = (UsersSecurityNode) node.getParent().getParent();
							ProjectNode parent = (ProjectNode) usersSecurityNode.getParent().getParent();
							userSecurityAction.setProject(parent.getProject());
							cloneUserSecurityAction.setProject(parent.getProject());
							deleteUserSecurityAction.setProject(parent.getProject());
							fillUserSecurityContextMenu(m, runningServerSet);
							
							for ( UploadUserSecurityAction uploadUserSecurityAction : uploadUserSecurityActions) {
								
								uploadUserSecurityAction.setProject(parent.getProject());
								
							}
	
						} else if ( object instanceof ViewGroupsNode ) {
							
							logger.debug("Right click on ViewGroupNode.");
					
							ViewGroupsNode viewGroupsNode = (ViewGroupsNode) object;
							openNewViewGroupAction.setProject(viewGroupsNode.getProject());
							//Don't allow this action to be enabled on more than the
							//"folder" level node.
							if(selection.size() > 1 || !nodesHaveSameParents){
								openNewViewGroupAction.setEnabled(false);
							} else{
								openNewViewGroupAction.setEnabled(true);
							}
							fillViewGroupsContextMenu(m);
						
						
						} else if ( object instanceof ViewGroupNode ) {
							
							logger.debug("Right click on ViewGroupNode.");
					
							ViewGroupNode viewGroupNode = (ViewGroupNode) object;
							deleteViewGroupAction.setProject(viewGroupNode.getProject());
							renameViewGroupAction.setProject(viewGroupNode.getProject());
							copyViewGroupAction.setProject(viewGroupNode.getProject());
							//only allow one view group to be selected, because the 
							//actions only handle one.
							if(selection.size() > 1 || !nodesHaveSameParents){
								renameViewGroupAction.setEnabled(false);
								copyViewGroupAction.setEnabled(false);
							} else{
								renameViewGroupAction.setEnabled(true);
								copyViewGroupAction.setEnabled(true);
							}
								
							fillViewGroupContextMenu(m);
							
						} else if ( object instanceof PrintStylesNode ) {
							//user right click on Print Preferences folder, allowed to new/deploy/refresh print preferences
							logger.debug("Right click on Print Preferences.");
							
							PrintStylesNode printPrefsNode = (PrintStylesNode) object;
							openNewPrintStyleAction.setProject(printPrefsNode.getProject());
							openNewPrintStyleAction.setEnabled(true);
							if ( uploadPrintStylesActions != null ) {
								for (AbstractUploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesActions) {
									deployPrintPrefsAction.setProject(printPrefsNode.getProject());
								}
							}
							if ( uploadPrintStylesToServerGroupActions != null ) {
								for (AbstractUploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesToServerGroupActions) {
									deployPrintPrefsAction.setProject(printPrefsNode.getProject());
								}
							}
							if ( downloadPrintStylesActions != null ) {
								for (DownloadPrintStylesAction importPrintPrefsAction : downloadPrintStylesActions) {
									importPrintPrefsAction.setProject(printPrefsNode.getProject());
								}
							}
							fillPrintStylesContextMenu(m, nodesHaveSameParents, runningServerSet);
						} else if ( object instanceof PrintStyleNode ) {
							//user right click on a Print Style item, allowed to delete/rename/copy/deploy/refresh a print style
							logger.debug("Right click on a Print Style Node.");
							
							IProject project = ViewerUtil.getProjectNode(selection);
							openNewPrintStyleAction.setProject(project);
							openNewPrintStyleAction.setEnabled(true);
							if ( uploadPrintStylesActions != null ) {
								for (AbstractUploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesActions) {
									deployPrintPrefsAction.setProject(project);
								}
							}
							if ( uploadPrintStylesToServerGroupActions != null ) {
								for (AbstractUploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesToServerGroupActions) {
									deployPrintPrefsAction.setProject(project);
								}
							}
							if ( downloadPrintStylesActions != null ) {
								for (DownloadPrintStylesAction importPrintPrefsAction : downloadPrintStylesActions) {
									importPrintPrefsAction.setProject(project);
								}
							}
							fillPrintStyleContextMenu(m, nodesHaveSameParents, runningServerSet);
						} else if ( object instanceof ConditionalStylesNode ) {
							IProject project = ViewerUtil.getProjectNode(selection);
							createCondtionalStyleAction.setProject(project);
							createCondtionalStyleAction.setEnabled(true);
							fillConditionalStylesContextMenu(menuMgr);
							
						} else if ( object instanceof ConditionalFormatsNode ) {
							IProject project = ViewerUtil.getProjectNode(selection);
							createCondtionalFormatAction.setProject(project);
							createCondtionalFormatAction.setEnabled(true);
							fillConditionalFormatsContextMenu(menuMgr);
							
						} else if ( object instanceof ConditionalStyleNode ) {
							if( ((ConditionalStyleNode) object).getParent() instanceof ConditionalStylesNode ) {
								IProject project = ViewerUtil.getProjectNode(selection);
								createCondtionalStyleAction.setProject(project);
								createCondtionalStyleAction.setEnabled(true);
								copyCondtionalStyleAction.setProject(project);
								copyCondtionalStyleAction.setEnabled(true);
								deleteCondtionalStyleAction.setProject(project);
								deleteCondtionalStyleAction.setEnabled(true);
								fillConditionalStyleContextMenu(menuMgr);
							}
							
						} else if ( object instanceof ConditionalFormatNode ) {
							IProject project = ViewerUtil.getProjectNode(selection);
							createCondtionalFormatAction.setProject(project);
							createCondtionalFormatAction.setEnabled(true);
							copyCondtionalFormatAction.setProject(project);
							copyCondtionalFormatAction.setEnabled(true);
							deleteCondtionalFormatAction.setProject(project);
							deleteCondtionalFormatAction.setEnabled(true);
							fillConditionalFormatContextMenu(menuMgr);
						} else if ( object instanceof AdminLocksNode ) {
							IProject project = ViewerUtil.getProjectNode(selection);
							createAdminLockAction.setProject(project);
							createAdminLockAction.setEnabled(true);
							fillAdminLocksContextMenu(menuMgr);
							
						} else if ( object instanceof AdminLockNode ) {
							IProject project = ViewerUtil.getProjectNode(selection);
							createAdminLockAction.setProject(project);
							createAdminLockAction.setEnabled(true);
							copyAdminLockAction.setProject(project);
							copyAdminLockAction.setEnabled(true);
							deleteAdminLockAction.setProject(project);
							deleteAdminLockAction.setEnabled(true);
							fillAdminLockContextMenu(menuMgr);
						}
						menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
					}
					
				} else {
					
					fillDefaultContextMenu(m);
					
				}
			}
			
		});
		
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}
		
	private boolean allNodesHaveSameParents(Object[] nodes){
		boolean ret = true;
		if(nodes != null && nodes.length > 0){
			//get the parent of the first node.
			TreeNode parent = (TreeNode) nodes[0];
			//now loop thru the rest.
			for(int i = 1; i < nodes.length; i++){
				TreeNode treeNode = (TreeNode) nodes[i];
				if(treeNode.getParent() != parent.getParent()){
					return false;
				}
			}
		}
		return ret;
	}
	
	private void fillViewsContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {
		
		menuMgr.add(openNewViewAction);
		menuMgr.add(new Separator());
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		MenuManager menuDeploy = new MenuManager("Upload Views To", "idInsert");
		if ( deployViewsToServerActions != null ) {
			for (UploadViewsToServerAction deployViewsToServerAction : deployViewsToServerActions) {
				//deployViewsToServerAction.setEnabled(enableDeployMenus);
				if ( enableDeployMenus && deployViewsToServerAction.getServer() != null && runningServerSet.contains(deployViewsToServerAction.getServer())) {
					deployViewsToServerAction.setEnabled(true);
				} else {
					deployViewsToServerAction.setEnabled(false);
				}
				menuDeploy.add(deployViewsToServerAction);
			}
		}
		
		
		
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload Views to Server Group");
		if(deployViewsToServerGroupActions != null){
			for(AbstractUploadViewAction action : deployViewsToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
				
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		MenuManager refreshViewsFromServersMenu = new MenuManager("Download Views From");
		if ( downloadViewsFromServerActions != null ) {
			for (DownloadViewsFromServerAction importAction : downloadViewsFromServerActions ) {
				//importAction.setEnabled(enableDeployMenus);
				if ( enableDeployMenus && importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshViewsFromServersMenu.add(importAction);
			}
		}
			
		menuMgr.add(refreshViewsFromServersMenu);
		
			
	}
	
	private void fillViewSectionsContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {

		menuMgr.add(openNewViewSectionAction);
		menuMgr.add(new Separator());
		
		MenuManager menuDeploy = new MenuManager("Upload View Sections To", "idInsert");
		if ( deployViewSectionsToServerActions != null ) {
			for (AbstractUploadViewSectionAction deployViewSectionsToServerAction : deployViewSectionsToServerActions) {
				if ( enableDeployMenus && deployViewSectionsToServerAction.getServer() != null && runningServerSet.contains(deployViewSectionsToServerAction.getServer())) {
					deployViewSectionsToServerAction.setEnabled(true);
				} else {
					deployViewSectionsToServerAction.setEnabled(false);
				}
				menuDeploy.add(deployViewSectionsToServerAction);
			}
		}
		
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload View Sections to Server Group");
		if(deployViewSectionsToServerGroupActions != null){
			for(AbstractUploadViewSectionAction action : deployViewSectionsToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
	
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		MenuManager refreshViewSectionsFromServersMenu = new MenuManager("Download View Sections From");
		if ( downloadViewSectionsFromServerActions != null ) {
			for (DownloadViewSectionsFromServerAction importAction : downloadViewSectionsFromServerActions ) {
				if ( enableDeployMenus && importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshViewSectionsFromServersMenu.add(importAction);
			}
		}
			
		menuMgr.add(refreshViewSectionsFromServersMenu);
		
	}
	
	private void fillViewContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {

		menuMgr.add(deleteViewAction);
		menuMgr.add(renameViewAction);
		menuMgr.add(copyViewAction);
		menuMgr.add(new Separator());
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menuMgr.add(new Separator());
				
		MenuManager menuDeploy = new MenuManager("Upload View To", "idInsert");
		if ( deployViewsToServerActions != null ) {
			for (UploadViewsToServerAction deployViewsToServerAction : deployViewsToServerActions) {
				if ( enableDeployMenus && deployViewsToServerAction.getServer() != null && runningServerSet.contains(deployViewsToServerAction.getServer())) {
					deployViewsToServerAction.setEnabled(true);
				} else {
					deployViewsToServerAction.setEnabled(false);
				}
				menuDeploy.add(deployViewsToServerAction);
			}
		}
		
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload View to Server Group");
		if(deployViewsToServerGroupActions != null){
			for(AbstractUploadViewAction action : deployViewsToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
	
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		MenuManager refreshViewsFromServersMenu = new MenuManager("Download View From");
		if ( downloadViewsFromServerActions != null ) {
			for (DownloadViewsFromServerAction importAction : downloadViewsFromServerActions ) {
				if ( enableDeployMenus && importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshViewsFromServersMenu.add(importAction);
			}
		}
			
		menuMgr.add(refreshViewsFromServersMenu);
		
						
	}
	
	private void fillViewSectionContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {
		
		menuMgr.add(deleteViewSectionAction);
		menuMgr.add(renameViewSectionAction);
		menuMgr.add(copyViewSectionAction);
		menuMgr.add(new Separator());
			
		MenuManager menuDeploy = new MenuManager("Upload View Section To", "idInsert");
		if ( deployViewSectionsToServerActions != null ) {
			for (UploadViewSectionsToServerAction deployViewSectionsToServerAction : deployViewSectionsToServerActions) {
				if ( enableDeployMenus && deployViewSectionsToServerAction.getServer() != null && runningServerSet.contains(deployViewSectionsToServerAction.getServer())) {
					deployViewSectionsToServerAction.setEnabled(true);
				} else {
					deployViewSectionsToServerAction.setEnabled(false);
				}	
				menuDeploy.add(deployViewSectionsToServerAction);
			}
		}
		
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload View Section to Server Group");
		if(deployViewSectionsToServerGroupActions != null){
			for(AbstractUploadViewSectionAction action : deployViewSectionsToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
	
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		MenuManager refreshViewSectionsFromServersMenu = new MenuManager("Download View Section From");
		if ( downloadViewSectionsFromServerActions != null ) {
			for (DownloadViewSectionsFromServerAction importAction : downloadViewSectionsFromServerActions ) {
				if ( enableDeployMenus && importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshViewSectionsFromServersMenu.add(importAction);
			}
		}
		
		menuMgr.add(refreshViewSectionsFromServersMenu);
						
	}
	
	private void fillPlanCyclesContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(openNewPlanCycleAction);
	}
	
	private void fillPlanCycleContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menuMgr.add(renamePlanCycleAction);
		menuMgr.add(deletePlanCycleAction);
						
	}
	
	
	private void fillSeasonsContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(openNewSeasonAction);
	}
	
	private void fillSeasonContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menuMgr.add(renameSeasonAction);
		menuMgr.add(deleteSeasonAction);
						
	}
	
	private void fillPlannerRolesContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(openNewPlannerRoleAction);
	}
	
	private void fillPlannerRoleContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menuMgr.add(renamePlannerRoleAction);
		menuMgr.add(deletePlannerRoleAction);
						
	}
	
	private void fillRoleConfigurationsContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(openNewRoleConfigurationAction);
	}
	
	private void fillRoleConfigurationContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(deleteRoleConfigurationAction);
						
	}
	
	private void fillUsersSecurityContextMenu(IMenuManager menuMgr, Set<PafServer> runningServerSet) {
		
		menuMgr.add(openNewUserSecurityAction);
		menuMgr.add(validateUserSecurityAction);
		menuMgr.add(new Separator());
		
		MenuManager securityDeploy = new MenuManager("Upload User Security To", "idInsert");

		if ( uploadUserSecurityActions != null ) {
			for (UploadUserSecurityAction uploadUserSecurityAction : uploadUserSecurityActions) {
				
				if ( uploadUserSecurityAction.getServer() != null && runningServerSet.contains(uploadUserSecurityAction.getServer())) {
					uploadUserSecurityAction.setEnabled(true);
				} else {
					uploadUserSecurityAction.setEnabled(false);
				}
				
				securityDeploy.add(uploadUserSecurityAction);
			}
		}
		
		menuMgr.add(securityDeploy);
						
	}
	
	private void fillUserSecurityContextMenu(IMenuManager menuMgr, Set<PafServer> runningServerSet) {
		
		
		// TTN-2720 - Adding "Upload User Security" to individual user security nodes
		MenuManager securityDeploy = new MenuManager("Upload User Security To", "idInsert");

		if ( uploadUserSecurityActions != null ) {
			for (UploadUserSecurityAction uploadUserSecurityAction : uploadUserSecurityActions) {
				
				if ( uploadUserSecurityAction.getServer() != null && runningServerSet.contains(uploadUserSecurityAction.getServer())) {
					uploadUserSecurityAction.setEnabled(true);
				} else {
					uploadUserSecurityAction.setEnabled(false);
				}
				
				securityDeploy.add(uploadUserSecurityAction);
			}
		}
		menuMgr.add(securityDeploy);
		
		menuMgr.add(new Separator());
//		menuMgr.add(userSecurityAction);
		menuMgr.add(cloneUserSecurityAction);
//		menuMgr.add(new Separator());
		menuMgr.add(deleteUserSecurityAction);
						
	}
	
	private void fillViewGroupsContextMenu(IMenuManager menuMgr) {
		
		menuMgr.add(openNewViewGroupAction);
						
	}
	
	private void fillViewGroupContextMenu(IMenuManager menuMgr) {
				
		menuMgr.add(deleteViewGroupAction);		
		menuMgr.add(renameViewGroupAction);
		menuMgr.add(copyViewGroupAction);
						
	}
	
	//TTN - 900 - Print Preferences - by Iris
	//Right click on the Print Preferences folder 
	private void fillPrintStylesContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {
		
		menuMgr.add(openNewPrintStyleAction);
		menuMgr.add(new Separator());
		
		//Deploy Print Styles		
		MenuManager menuDeploy = new MenuManager("Upload Print Styles To", "idInsert");
		if ( uploadPrintStylesActions != null ) {
			for (UploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesActions) {
				if ( deployPrintPrefsAction.getServer() != null && runningServerSet.contains(deployPrintPrefsAction.getServer())) {
					deployPrintPrefsAction.setEnabled(true);
				} else {
					deployPrintPrefsAction.setEnabled(false);
				}	
				menuDeploy.add(deployPrintPrefsAction);
			}
		}
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload Print Styles to Server Group");
		if(uploadPrintStylesToServerGroupActions != null){
			for(AbstractUploadPrintStylesAction action : uploadPrintStylesToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
	
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		//Refresh Print Styles		
		MenuManager refreshPrintPrefsMenu = new MenuManager("Download Print Styles From");
		if ( downloadPrintStylesActions != null ) {
			for (DownloadPrintStylesAction importAction : downloadPrintStylesActions ) {
				if ( importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshPrintPrefsMenu.add(importAction);
			}
		}
		menuMgr.add(refreshPrintPrefsMenu);
	}
	
	private void fillConditionalStylesContextMenu(IMenuManager menuMgr) {
		menuMgr.add(createCondtionalStyleAction);
		menuMgr.add(new Separator());
	}
	
	private void fillConditionalStyleContextMenu(IMenuManager menuMgr) {
		menuMgr.add(copyCondtionalStyleAction);		
		menuMgr.add(deleteCondtionalStyleAction);
	}

	private void fillConditionalFormatsContextMenu(IMenuManager menuMgr) {
		menuMgr.add(createCondtionalFormatAction);
		menuMgr.add(new Separator());
	}
	
	private void fillConditionalFormatContextMenu(IMenuManager menuMgr) {
		menuMgr.add(copyCondtionalFormatAction);		
		menuMgr.add(deleteCondtionalFormatAction);
	}
	
	private void fillAdminLocksContextMenu(IMenuManager menuMgr) {
		menuMgr.add(createAdminLockAction);
		menuMgr.add(new Separator());
	}
	
	private void fillAdminLockContextMenu(IMenuManager menuMgr) {
		menuMgr.add(copyAdminLockAction);		
		menuMgr.add(deleteAdminLockAction);
	}
	
	//Right click on the Print Style item 
	private void fillPrintStyleContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {
				
		menuMgr.add(openNewPrintStyleAction);
	
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(new Separator());
		
		//Deploy Print Styles		
		MenuManager menuDeploy = new MenuManager("Upload Print Style To", "idInsert");
		if ( uploadPrintStylesActions != null ) {
			for (UploadPrintStylesAction deployPrintPrefsAction : uploadPrintStylesActions) {
				if ( deployPrintPrefsAction.getServer() != null && runningServerSet.contains(deployPrintPrefsAction.getServer())) {
					deployPrintPrefsAction.setEnabled(true);
				} else {
					deployPrintPrefsAction.setEnabled(false);
				}		
				menuDeploy.add(deployPrintPrefsAction);
			}
		}
		menuMgr.add(menuDeploy);
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload Print Style to Server Group");
		if(uploadPrintStylesToServerGroupActions != null){
			for(AbstractUploadPrintStylesAction action : uploadPrintStylesToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
	
				uploadToGroupMenu.add(action);
			}
		}
		menuMgr.add(uploadToGroupMenu);
		
		//Refresh Print Styles		
		MenuManager refreshPritnStyleMenu = new MenuManager("Download Print Styles From");
		if ( downloadPrintStylesActions != null ) {
			for (DownloadPrintStylesAction importAction : downloadPrintStylesActions ) {
				if ( importAction.getServer() != null && runningServerSet.contains(importAction.getServer())) {
					importAction.setEnabled(true);
				} else {
					importAction.setEnabled(false);
				}
				refreshPritnStyleMenu.add(importAction);
			}
		}
		menuMgr.add(refreshPritnStyleMenu);
}
	
	private void fillProjectContextMenu(IMenuManager menuMgr, boolean enableDeployMenus, Set<PafServer> runningServerSet) {
				
		logger.info("start buid project right click menu");
		
		createServerRelatedActions();
		
		logger.info("completed createServerRelatedActions");
		
		if ( viewer.getSelection() != null ) {
			viewer.setSelection(viewer.getSelection());
		}
			
		MenuManager uploadMenu = new MenuManager("Upload Local Project to Server", "idInsert");
		if ( uploadProjectToServerActions != null ) {
			for (UploadProjectToServerAction uploadAction : uploadProjectToServerActions) {
				
				//if server is running enable  option
				if ( uploadAction.getServer() != null && runningServerSet.contains(uploadAction.getServer())) {
					uploadAction.setEnabled(true);
				} else {
					uploadAction.setEnabled(false);
				}			
				
				uploadMenu.add(uploadAction);	
			}
		}
		
		MenuManager uploadToGroupMenu = new MenuManager("Upload Local Project to Server Group");
		if(uploadProjectToServerGroupActions != null){
			for(AbstractUploadAction action : uploadProjectToServerGroupActions){
				if(action.allServersRunning()) {
					action.setEnabled(true);
				} else {
					if(action.allServersStopped()) {
						action.setEnabled(false);
					}
				}
				
				uploadToGroupMenu.add(action);
			}
		}
		
		MenuManager downloadMenu = new MenuManager("Download Local Project From Server");
		if ( downloadProjectFromServerActions != null ) {
			for (DownloadProjectFromServerAction downloadAction : downloadProjectFromServerActions ) {
				
				//if server is running enable option
				if ( downloadAction.getServer() != null && runningServerSet.contains(downloadAction.getServer())) {
					downloadAction.setEnabled(true);
				} else {
					downloadAction.setEnabled(false);
				}		
				
				downloadMenu.add(downloadAction);
			}
		}
		
		MenuManager downloadCubeMenu = new MenuManager("Download Cube Changes from Server");
		
		if ( downloadCubeChangesFromServerActions != null ) {
			
			for (DownloadCubeChangesFromServerAction downloadCubeAction : downloadCubeChangesFromServerActions ) {
				if ( downloadCubeAction.getServer() != null && runningServerSet.contains(downloadCubeAction.getServer())) {
					downloadCubeAction.setEnabled(true);
				} else {
					downloadCubeAction.setEnabled(false);
				}
				downloadCubeMenu.add(downloadCubeAction);
			}
		}
	
		MenuManager defaultProjectServersMenu = new MenuManager("Default Project Server");
		
		if ( selectDefaultServerProjectActions != null ) {
			
			for (SelectDefaultServerProjectAction defaultServerAction : selectDefaultServerProjectActions ) {
				defaultServerAction.setEnabled(enableDeployMenus);
				defaultProjectServersMenu.add(defaultServerAction);
			}
		}
	
			
		menuMgr.add(newProjectAction);
		menuMgr.add(copyProjectAction);
		menuMgr.add(deleteProjectAction);
		menuMgr.add(new Separator());
		menuMgr.add(defaultProjectServersMenu);
		menuMgr.add(autoConvertProjectAction);
		menuMgr.add(new Separator());
		menuMgr.add(uploadMenu);
		menuMgr.add(uploadToGroupMenu);
		menuMgr.add(downloadMenu);
		menuMgr.add(downloadCubeMenu);
		menuMgr.add(new Separator());
//		menuMgr.add(chooseColorsAction);
		logger.info("end buid project right click menu");
	}
	

	private void fillDefaultContextMenu(IMenuManager menuMgr) {
	
		menuMgr.add(refreshAction);
		menuMgr.add(new Separator());
//		menuMgr.add(newProjectAction);
//		menuMgr.add(deleteProjectAction);
//		menuMgr.add(new Separator());
		menuMgr.add(importProjectAction);
		menuMgr.add(importAndDeployRulesetAction);	
		menuMgr.add(exportProjectAction);
		menuMgr.add(new Separator());
	}
	

	public TreeViewer getViewer() {
		return viewer;
	}
	
	public void updateTreeObject(TreeNode node ) {
		
		if ( node != null ) {
			node.createChildren(null);
			viewer.refresh(node);
		}
		
	}
	
	private void openIProject(IProject project) {
		if ( ! project.isOpen()) {
			try {
				project.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Problem opening project: " + project);
			}
		}
	}
//	public boolean isExpandProjectTree(){
//		
//		return expandProjectTree;
//		
//	}
	
	public DownloadCubeChangesFromServerAction[] getDownloadCubeChangesFromServerActions() {
		return downloadCubeChangesFromServerActions;
	}

	@Override
	public void update(Observable o, Object object) {

		if ( object != null && object instanceof ProjectEvent ) {
			
			ProjectEvent pe = (ProjectEvent) object;
			
			//if project and project request are not null
				if ( pe.getProject() != null && pe.getProjectRequest() != null && pe.getProjectRequest().equals(ProjectRequest.Refresh)) {
				
//				final IProject project = pe.getProject();
				
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {

						refreshProjectStructure();
//						String projectName = project.getProject().getName(); 
//						selectProjectNode(projectName);
						
					}
					
				});			
				
			}
			
		}
	}
	
	/**
	 * Performs the initial setup and auto conversion of all the projects in the workspace.
	 */
	public void autoConvertProjects(){
		Object root = viewer.getInput();
		if(!(root instanceof RootNode))return;
		RootNode rootNode = (RootNode)root;
		
		StopWatch sw = new StopWatch();
		sw.start();

				
		for(Object child : rootNode.getChildren()){
			if(child instanceof ProjectNode){
				autoConvertProject(((ProjectNode) child).getProject());
			}
		}

		sw.stop();
		logger.info(String.format("Took %s (ms) to load all projects", sw.getTotalTimeMillis()));
	}

	/**
	 * Loads all the nodes for all the projects in the workspace.  
	 * If a project is already built no action is performed on the project.
	 * @param expandToLevel Level of expansion to set when building is complete.
	 */
	public void loadProjects(final int expandToLevel){
		Object root = viewer.getInput();
		if(!(root instanceof RootNode))return;
		final RootNode rootNode = (RootNode)root;
		
		final StopWatch sw = new StopWatch();
		sw.start();
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
			public void run() {
				
				for(Object child : rootNode.getChildren()){
					if(child instanceof ProjectNode){
						ProjectNode node = (ProjectNode) child;
						if(node.isBuilt()) {
							viewer.expandToLevel(node, expandToLevel);
						} else {
							loadProject(node, expandToLevel);
						}
					}
				}
			}
		});
		sw.stop();
		logger.info(String.format("Took %s (ms) to load all projects", sw.getTotalTimeMillis()));
	}
	
	/**
	 * Loads the specified workspace project, expansion is set level 1.   If the project is already built, no action is performed.
	 * @param node Project node to expand
	 */
	public void loadProject(final ProjectNode node){
		int level = 0;
		if(PafProjectUtil.getExpandProjectTree()){
			level = 1;
		}
		loadProject(node, level);
	}
	
	/**
	 * Loads the specified workspace project.  If the project is already built, no action is performed.
	 * @param node Project node to expand
	 * @param expandToLevel Project expansion level.
	 */
	private void loadProject(final ProjectNode node, final int expandToLevel){
		if(!node.isBuilt()){

			StopWatch sw = new StopWatch();
			sw.start();
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
				public void run() {
					//Get the IProject from the node
					IProject project = node.getProject();
					//Run the conversion.  Method checks for upgrade flag, etc.
					autoConvertProject(project);
					//Build the nodes children.
					//node.buildTree();
					node.setBuilt(true);
					//Refresh the UI
					viewer.refresh();
					if(expandToLevel >= 0){
						 viewer.expandToLevel(node, expandToLevel);
					 }
				}
			});
			sw.stop();
			logger.info(String.format("Took %s (ms) to load project: %s", sw.getTotalTimeMillis(), node.getName()));
		}
	}
}

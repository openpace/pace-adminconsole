/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.preferences;

import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class MenuPreferences {

	private Preferences preferences;
	
	public MenuPreferences(String node){
		
		preferences = new ConfigurationScope().getNode(node);
		try {
			preferences.sync();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getStringSetting(String key){
		String def = "";
		return preferences.get(key, def);
	}
	
	public boolean getBooleanSetting(String key){
		boolean def = false;
		return preferences.getBoolean(key, def);
	}
	
	public void saveBooleanSetting(String key, boolean value){
		preferences.putBoolean(key, value);
		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStringSetting(String key, String value){
		
		preferences.put(key, value);
		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class NewProjectValidationWizardPage extends WizardPage {
	
	// fields for project validation
	
	String lastPeriod;
	String CY;
	String msrRoot;
	String[] dims;
	String server;
	String datasourceid;
	//SWT objects for validation
	private Label msrRootLbl;

	private Label msrRootTxt;

	private Label CYLbl;

	private Label CYTxt;
	
	private Label lastPrdLbl;

	private Label lastPrdTxt;
	
	private Label serverLbl;

	private Label serverTxt;
	
	
	private Label dsIdLbl;

	private Label dsIdTxt;
	
	private Label[] dimsLbl = new Label[5];

	private Label[] dimsTxt = new Label[5];
	
	public NewProjectValidationWizardPage(String wizardPageName,
			String wizardPageTitle, String wizardPageDesc,
			boolean existingOptionsEnabled,String lastPeriod, String CY,String msrRoot,String[] dims,String server,String datasourceId)
	{
		super(wizardPageName);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
		
		this.lastPeriod = lastPeriod;
		this.CY = CY;
		this.msrRoot = msrRoot;
		this.dims = dims;
		this.server = server;
		this.datasourceid = datasourceId;
		
		
		
	}

	
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
		setControl(container);
		{
			Group dimGroup = new Group(container, SWT.NONE);
			{
				GridData gridData = new GridData(SWT.FILL, SWT.FILL, true,
						true, 1, 1);
				gridData.heightHint = 300;
				gridData.widthHint = 558;
				dimGroup.setLayoutData(gridData);
			}
			{
				
				msrRootLbl = new Label(dimGroup, SWT.BOLD);
				Font boldFontSmall = new Font( msrRootLbl.getDisplay(), new FontData( "Arial", 9, SWT.BOLD ) );
				
				CYLbl = new Label(dimGroup, SWT.BOLD);
				lastPrdLbl = new Label(dimGroup, SWT.BOLD);
				for(int i=0;i<5;i++)
				dimsLbl[i] = new Label(dimGroup, SWT.BOLD);
				
				serverLbl = new Label(dimGroup, SWT.BOLD);
				dsIdLbl = new Label(dimGroup, SWT.BOLD);
				
				msrRootTxt = new Label(dimGroup , SWT.NONE);
				CYTxt = new Label(dimGroup , SWT.NONE);
				lastPrdTxt = new Label(dimGroup ,SWT.NONE);
				for(int i=0;i<5;i++)
					dimsTxt[i] = new Label(dimGroup, SWT.NONE);
				
				serverTxt = new Label(dimGroup , SWT.NONE);
				dsIdTxt = new Label(dimGroup , SWT.NONE);
				
				msrRootLbl.setText("Measure Root");
				CYLbl.setText("Current Year");
				lastPrdLbl.setText("Last Elapsed Period");
				serverLbl.setText("Pace Server");
				dsIdLbl.setText("DataSource");
				
				msrRootLbl.setFont(boldFontSmall);
				CYLbl.setFont(boldFontSmall);
				lastPrdLbl.setFont(boldFontSmall);
				serverLbl.setFont(boldFontSmall);
				dsIdLbl.setFont(boldFontSmall);
				
				dimsLbl[0].setText("Measures");
				dimsLbl[1].setText("Time");
				dimsLbl[2].setText("Years");
				dimsLbl[3].setText("PlanType");
				dimsLbl[4].setText("Version");
				
				dimsLbl[0].setFont(boldFontSmall);
				dimsLbl[1].setFont(boldFontSmall);
				dimsLbl[2].setFont(boldFontSmall);
				dimsLbl[3].setFont(boldFontSmall);
				dimsLbl[4].setFont(boldFontSmall);
				
				msrRootTxt.setText(msrRoot);
				
				CYTxt.setText(CY);
				lastPrdTxt.setText(lastPeriod);
				
				serverTxt.setText(server);
				dsIdTxt.setText(datasourceid);
				
				// get the background white color before setting the editable property.
				Color bkColor = msrRootTxt.getBackground();
				
				// set the properties to be not editable on the validation 
			//	msrRootTxt.setEditable(false);
			//	msrRootTxt.setEnabled(false);
				
			
				
				dimsTxt[0].setText(dims[0]);
				dimsTxt[1].setText(dims[1]);
				dimsTxt[2].setText(dims[2]);
				dimsTxt[3].setText(dims[3]);
				dimsTxt[4].setText(dims[4]);
				
				
				
			
				// set the background color
		/*		dimsTxt[0].setBackground(bkColor);
				dimsTxt[1].setBackground(bkColor);
				dimsTxt[2].setBackground(bkColor);
				dimsTxt[3].setBackground(bkColor);
				dimsTxt[4].setBackground(bkColor);
				
				msrRootTxt.setBackground(bkColor);
				
				CYTxt.setBackground(bkColor);
				lastPrdTxt.setBackground(bkColor);
				
				serverTxt.setBackground(bkColor);
				dsIdTxt.setBackground(bkColor);         */
				
				serverLbl.setBounds(20, 20, 80, 20);
				serverTxt.setBounds(170, 20, 200, 20);
				
				dsIdLbl.setBounds(20, 50, 80, 20);
				dsIdTxt.setBounds(170, 50, 200, 20);
				
				for(int i=0;i<5;i++)
				{
					dimsLbl[i].setBounds(20, 80 + i*20, 80, 20);
					dimsTxt[i].setBounds(170, 80 + i*20,200, 20);
				}
				
				msrRootLbl.setBounds(20, 200, 80, 20);
				msrRootTxt.setBounds(170, 200, 200, 20);
				
				CYLbl.setBounds(20, 240, 80, 20);
				CYTxt.setBounds(170, 240, 200, 20);
				
				lastPrdLbl.setBounds(20, 280, 120, 20);
				lastPrdTxt.setBounds(170, 280,200, 20);
				
				
				
				
			}
			}
	}
	

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.DimLevelGenInfo;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafMdbPropsRequest;
import com.pace.server.client.PafMdbPropsResponse;
import com.pace.server.client.PafService;

public class DimensionNameSelectionWizardPage extends WizardPage {

	private List<String> itemList;
	private Label[] lblDim;
	
	// Label and Combo for showing the Measures

	private Label[] msrLbl;

	private Combo[] msrCombo;

	private Combo msrRootCombo;
	
	// dimensions for Measures and Time 

	private String accountDimension;

	private String timeDimension;
	
	private String yearDimension;

	private boolean existingOptionsEnabled;
	
	private int noOfDimensions = 5;
	
	String[] checkedItems = new String[noOfDimensions];

	private String[] items;

	private DimLevelGenInfo[] levelInfoArray = null;
	
	private Label paceDim;

	private Label essbaseDim;
	
	boolean duplicates[] = new boolean[noOfDimensions];

	private MenuPreferences preferences;
	private final String PREF_NODE = "com.pace.admin.menu.wizards.DimensionNameSelectionWizardPage";

	public Label[] getMsrLbl() {
		return msrLbl;
	}

	public Combo[] getMsrCombo() {
		return msrCombo;
	}

	public String getAcctDim() {
		return accountDimension;
	}

	public String getTimeDim() {
		return timeDimension;
	}

	public String getYearDim()
	{
		return yearDimension;
	}
	public DimLevelGenInfo[] getInfoArray() {
		return levelInfoArray;
	}

	public void setInfoArray(DimLevelGenInfo[] levelInfoArray) {
		this.levelInfoArray = levelInfoArray;
	}

	
	
	public DimensionNameSelectionWizardPage(String wizardPageName,
			String wizardPageTitle, String wizardPageDesc,
			boolean existingOptionsEnabled, String[] itemList,
			String accountDim, String timeDim) {

		super(wizardPageName);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);

		preferences = new MenuPreferences(PREF_NODE);
		

		this.existingOptionsEnabled = existingOptionsEnabled;
		

		msrCombo = new Combo[noOfDimensions];
		
		msrLbl = new Label[noOfDimensions];

		// set the itemlist for Combo box of dimensions.
		if (itemList != null && itemList.length > 0) {
			this.itemList = new ArrayList<String>();
			this.itemList.addAll(Arrays.asList(itemList));
			
		} else {
			this.itemList = new ArrayList<String>();
		}

		if (itemList != null) {

			for (int i = 0; i < itemList.length; i++) {
				// msrLbl[i].setText(itemList[i]);
				// msrCombo[i].setItems(itemList);
			}
		}

		items = itemList;
		accountDimension = accountDim;
		timeDimension = timeDim;

	}

	public String[] getItems() {
		return items;
	}

	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
		setControl(container);
		{
			Group dimGroup = new Group(container, SWT.BOLD);
			{
				GridData gridData = new GridData(SWT.FILL, SWT.FILL, true,
						true, 1, 1);
				gridData.heightHint = 200;
				gridData.widthHint = 558;
				dimGroup.setLayoutData(gridData);
			}
			{
				
				paceDim = new Label(dimGroup, SWT.NONE);
				paceDim.setText("Pace Dimension");
				
				Font boldFont = new Font( paceDim.getDisplay(), new FontData( "Arial", 10, SWT.BOLD ) );
				paceDim.setFont(boldFont);
				
				essbaseDim = new Label(dimGroup, SWT.BOLD);
				essbaseDim.setText("Essbase Dimension");
				essbaseDim.setFont(boldFont);
				
				if (this.itemList.size() > 0) {
					
					Font boldFontSmall = new Font( paceDim.getDisplay(), new FontData( "Arial", 9, SWT.BOLD ) );
					
					msrLbl[0] = new Label(dimGroup, SWT.BOLD);
					msrLbl[0].setText("Measure");
					msrLbl[0].setFont(boldFontSmall);

					msrLbl[1] = new Label(dimGroup, SWT.BOLD);
					msrLbl[1].setText("Time");
					msrLbl[1].setFont(boldFontSmall);

					msrLbl[2] = new Label(dimGroup, SWT.BOLD);
					msrLbl[2].setText("Year");
					msrLbl[2].setFont(boldFontSmall);

					msrLbl[3] = new Label(dimGroup, SWT.BOLD);
					msrLbl[3].setText("Plan Type");
					msrLbl[3].setFont(boldFontSmall);

					msrLbl[4] = new Label(dimGroup, SWT.BOLD);
					msrLbl[4].setText("Version");
					msrLbl[4].setFont(boldFontSmall);

				}
				
				paceDim.setBounds(20,10,120,20);
				essbaseDim.setBounds(200,10,160,20);
				// set the Combo box and populate the vaues
				for (int k = 0; k < noOfDimensions; k++) {
					
					
				
                    if(msrLbl[k]!=null)
					msrLbl[k].setBounds(20, 50 + (k * 30), 100, 20);

					msrCombo[k] = new Combo(dimGroup, SWT.READ_ONLY);
					final int test = k;
					
					msrCombo[k].addModifyListener(new ModifyListener() {
						   public void modifyText(ModifyEvent e) {
						     
							   updatePageComplete(test);
						      
									
									
						   }
						});
					
				
					msrCombo[k].setItems((itemList.toArray(new String[0])));
					msrCombo[k].setBounds(200, 50 + k * 30, 160, 55);
					
					
				}
				for (int k = 0; k < noOfDimensions; k++) {

					if(itemList.isEmpty() || itemList == null) {
						// TTN-2555 - Notify user there is a problem loading Essbase dimensions when creating new project
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"Issue connecting to Essbase or other Essbase issue.\n Please check Pace Error logs for more information",
								MessageDialog.INFORMATION);
				    }
					
					if (k == 0) // set default account dimensions
					{	
						if(itemList.get(k).equals(accountDimension))
							msrCombo[0].select(k);
						else if(itemList.get(k).equals(timeDimension))
							msrCombo[1].select(k); 
					}

					if (k == 1){ // set default time dimension
						if(itemList.get(k).equals(timeDimension))
							msrCombo[1].select(k);
						else if(itemList.get(k).equals(accountDimension))
							msrCombo[0].select(k); 
						
					}
				}
			}
		}
		
	}

	// method to check if the page is complete with dimensions selected by the user . If validated, then can move to next page.
	public boolean canFlipNextPage()
	{
		boolean isFlip = false;
		
		
		String[] canFlipCheck = new String[noOfDimensions];
		canFlipCheck = 	this.getDimensionSelections();
		
		for(int i=0;i<canFlipCheck.length;i++)
		{
			
			
			if(canFlipCheck[i]==null)
			{
				isFlip = false;
				break;
			}
			else
				isFlip = true;
			   
		}
		
		// TTN-2608 Check for duplicate dimension selection .
		boolean duplicates=false;
		for (int j=0;j<canFlipCheck.length;j++){
		  for (int k=j+1;k<canFlipCheck.length;k++){
		    if (k!=j && canFlipCheck[k]!=null && canFlipCheck[k].equals(canFlipCheck[j]) ){
		      duplicates=true;
		      GUIUtil.openMessageWindow(
						Constants.DIALOG_WARNING_HEADING,
						"Dimension "+canFlipCheck[k]+" cannot be selected more than once !",
						MessageDialog.INFORMATION);
		    }
		  }
		}
		
		if(duplicates)
			isFlip = false;
		
		return isFlip;
		
	}
	
    // TTN-2608 Check for duplicate dimension names and validate.
	public boolean canFlipNextPageDuplicate(int id)
	{
		boolean isFlip = false;
		
		
		String[] canFlipCheck = new String[noOfDimensions];
		canFlipCheck = 	this.getDimensionSelections();
		
		// Once a selection is made for each member you can flip the page
		for(int i=0;i<canFlipCheck.length;i++)
		{
			// TTN-2608 Reset the duplicate array .
			duplicates[i] = false;
			if(canFlipCheck[i]==null)
			{
				isFlip = false;
				break;
			}
			else
				isFlip = true;
			   
		}
		
		// TTN-2608 Check for duplicate dimension selection .
		//boolean duplicates=false;
		boolean tested = false;
		for (int j=0;j<canFlipCheck.length;j++){
		 for (int k=j+1;k<canFlipCheck.length;k++) {
			 if (canFlipCheck[j]!=null && canFlipCheck[k]!=null && canFlipCheck[j].equals(canFlipCheck[k])) {
				 tested = true;
				 GUIUtil.openMessageWindow(
							Constants.DIALOG_WARNING_HEADING,
							"Dimension "+canFlipCheck[k]+" cannot be selected more than once !",
							MessageDialog.INFORMATION);
				 isFlip = false;
			 }
		 }  
		}
		
		// if id is tested and no duplicates found, then set to false. This is used for the case when the dimension is reset.
		if(!tested){
			duplicates[id] = false;
			
		}
		
		
	    // do not flip page if any duplicates exist.
		for(boolean dup:duplicates){
			if(dup)
				isFlip = false;
		}
		
		
		
		try{
			if (id==2 && !duplicates[id])
			yearDimension =	itemList.get(msrCombo[2].getSelectionIndex());
		}catch(Exception e){
			
		}
		
		
		return isFlip;
		
	}
	
	// update the complete property if the page is validated to be complete.
	public void updatePageComplete()
	{
		
		if(canFlipNextPage())
		{
			this.setPageComplete(true);
			yearDimension =	itemList.get(msrCombo[2].getSelectionIndex());
			
		}
		else
		{
			this.setPageComplete(false);
			
		}
	}
	
	// update the complete property if the page is validated to be complete.
		public void updatePageComplete(int id)
		{
			
			if(canFlipNextPageDuplicate(id))
			{
				this.setPageComplete(true);
			//	yearDimension =	itemList.get(msrCombo[2].getSelectionIndex());
				
			}
			else
			{  
				this.setPageComplete(false);
				
			}
		}
		
	// get dimension selections made by the user.
	public String[] getDimensionSelections()
	{
		for(int i=0;i<msrCombo.length;i++)
		{
			if(msrCombo[i]!=null)
			{
		
			if(msrCombo[i].getSelectionIndex() != -1)
			checkedItems[i] = msrCombo[i].getText();
			}
			else
			checkedItems[i] = null;
		}
		
		return checkedItems;
	}
}

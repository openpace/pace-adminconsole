/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.actions.importexport.ExportProjectToFileset;
import com.pace.admin.menu.actions.importexport.ExportProjectToServerAction;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.projectnavigator.ProjectNavPlugin;
import com.pace.base.project.ExcelPaceProject;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSerializationType;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;

/**
 * Project export wizard allows a user to export a project to a server, excel, paf file or template.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class ProjectExportWizard extends Wizard implements IExportWizard {

	private static Logger logger = Logger.getLogger(ProjectExportWizard.class);
	
	private ProjectExportTypeSelectionPage projectExportWizardPage;
	private GenericSelectionOptionsWizardPage  projectSelectionWizardPage;
	private ProjectDataSelectionPage projectExportDataSelectionWizardPage;
	private Set<PafServer> pafServers;
	private Map<String, IProject> currentProjects = null;
	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrorMap;
	
	private IWorkbench workbench;
//	private boolean uploadProjectError = false;
	
	public ProjectExportWizard() {
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		
		addPage(projectSelectionWizardPage);
		addPage(projectExportDataSelectionWizardPage);
		addPage(projectExportWizardPage);
		
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {

		setWindowTitle("Export Pace Project");
		
		this.workbench = workbench;
		
		//get the current projects.
		currentProjects =  ViewerUtil.getCurrentProjects();
		//get the current paf server.
		pafServers = PafServerUtil.getRunningPafServers();
		List<String> serverNameList = new ArrayList<String>();
		
		if(pafServers != null && pafServers.size() > 0){
			
			for(PafServer server : pafServers){
				serverNameList.add(server.getName());
			}
			
		}
		
		
		//get the current project
		List<String> currentProject = new ArrayList<String>();
		
		IProject node = null;
		if(ProjectNavPlugin.getDefault().isVisible()){
			node = ViewerUtil.getProjectNode(ProjectNavPlugin.getDefault().getProjectNavPage().getSelection());
		} else{
			node = ViewerUtil.getProjectNode(MenuPlugin.getDefault().getMenuView().getViewer().getSelection());
		}
		
		
		
		//IProject node = ViewerUtil.getProjectNode(MenuPlugin.getDefault().getMenuView().getViewer().getSelection());
		if(node != null){
			
			currentProject.add(node.getName());
			
		}
		
		
		//get the templates
		List<File> templates = ProjectWizardUtil.getTemplates();
		//list to hold the name of templates.
		List<String> templateNameList = new ArrayList<String>();
		if(templates != null && templates.size() > 0){
			//for each file in the list of templates.
			for(File f : templates){
				
				//get the file name.
				String s = f.getName();
				//create the name
				String name = s.replace("." + Constants.PAF_EXT, "");
				//strip off the file ext.
				templateNameList.add(name);
				
			}

		}
		
		
		projectSelectionWizardPage = ProjectWizardUtil.getExportProjectSelectionWizardPage(currentProjects.keySet().toArray(new String[0]), currentProject.toArray(new String[0]));
		
		projectExportWizardPage  = ProjectWizardUtil.getProjectExportTypeSelectionPage(serverNameList.toArray(new String[0]), templateNameList);
		
		projectExportDataSelectionWizardPage = ProjectWizardUtil.getProjectExportDataSelectionWizardPage();
		
	}

	
	@Override
	public boolean performFinish() {
		
		
		String projectName = projectSelectionWizardPage.getSelectedItems()[0];
		final IProject project = getProject(projectName);
		if(project == null){
			return false;
		}
		//get the conf dir off the PafServer.
		final String confDir = project.getFolder(Constants.CONF_DIR).getLocation().toString();
		//data filter of items to import.
		final ProjectElementId[] elements = projectExportDataSelectionWizardPage.getSelectedProjectItems();
		//get the list of items to export.
		final Set<ProjectSourceType> selectedItemsToExport = projectExportWizardPage.getSelectedItemsToExport();
		//get the project name.
		final String serverName = projectExportWizardPage.getServerName();
		//the the refresh projects actions.
		final String templateDir = ProjectWizardUtil.getTemplatePath();
		//get the excel path file name.
		final String excelPathFileName = projectExportWizardPage.getExcelPathFileName();
		//get excel dependencies
		final boolean addDependencies = projectExportWizardPage.isAddDependencies();
		//get excel referencing
		final boolean cellReferencing = projectExportWizardPage.isExcelCellReferences();
		//get the paf path file name.
		final String pafPathFileName = projectExportWizardPage.getPafPathFileName();
		//get the selected templates.
		final String templateName = projectExportWizardPage.getTemplateName();
		
		
//		//use an hourglass
//		BusyIndicator.showWhile(workbench.getDisplay(),
//				new Runnable() {
//					public void run() {		
						
//						String confDir = project.getFolder(Constants.CONF_DIR).getLocation().toString();
						
//						XMLPaceProject xpp = null;
						boolean returnValue = false;
						String errorMessage = "";
						
							
						Set<ProjectElementId> elementSet = new HashSet<ProjectElementId>();
						
						if ( elements != null ) {
							
							elementSet.addAll(Arrays.asList(elements));
							
						}
						
						
						if(selectedItemsToExport.contains(ProjectSourceType.Excel) && addDependencies ) {
							
							elementSet.addAll(ExcelPaceProject.getProjectIdSetDependencies(elementSet));
							
						}
						

						
						try {
							new XMLPaceProject(
									confDir,
									elementSet,
									false);
						} catch (InvalidPaceProjectInputException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (PaceProjectCreationException e) {
							
							logger.error("There was a problem creating the project: " + e.getMessage());

							List<ProjectElementId> projectWarningList = new ArrayList<ProjectElementId>();
																								
							if ( e.getProjectCreationErrorMap() != null ) {
								
								projectWarningList.addAll(e.getProjectCreationErrorMap().keySet());
								
							}

							GUIUtil.openMessageWindow("Invalid XML Project Data...", "The following project data item(s) could not be loaded: \n\n\t" + projectWarningList.toString().substring(1, projectWarningList.toString().length()-1) + "\n\nPlease go to the Project Navigator tab and validate the project to see the errors.", MessageDialog.ERROR);
							
							handleError();
							return  false;
							
						}	
						
						errorMessage = "There was a problem exporting project: '" + project.getName() + "'. Please select the Show Errors button on the last page to see the errors.";
						if(selectedItemsToExport.contains(ProjectSourceType.Server)){
							
							if(serverName != null && serverName.length() > 0){
								
								try {
									returnValue = exportToServer2(elements, project, serverName, projectExportWizardPage.isApplyConfig(), projectExportWizardPage.isApplyCube());
								} catch (Throwable e) {
									e.printStackTrace();
									GUIUtil.openMessageWindow(
											getWindowTitle(),
											errorMessage,
											SWT.ERROR);
								}
								
								/*PafServer server = getServerByName(serverName);
								deployProjectToServer(project, server);*/
								
								/*//call loadApplicaiton() service
								if( projectExportWizardPage.isApplyConfig() || projectExportWizardPage.isApplyCube() ) {
									loadApplication( project, server, projectExportWizardPage.isApplyCube());
								}*/
								//String outMessage = null;
								//if error, alert users
								/*if ( uploadProjectError ) {
									outMessage = "Project '" + project.getName() + "' was not successfully uploaded to server '" + server.getName() + "'.  There was a problem copying one or more files.";
									GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
									
								} else {
									outMessage = "Project '" + project.getName() + "' was successfully uploaded to server '" + server.getName() + "'.";
									GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
								}*/
								
							}
							
						}
						
						if(selectedItemsToExport.contains(ProjectSourceType.Excel)){
							
							if(excelPathFileName != null && excelPathFileName.length() > 0){
								
								try {
									returnValue = exportToFileset2(project, excelPathFileName, elements, cellReferencing, addDependencies, ProjectSerializationType.XLSX);
								} catch (Throwable e) {
									e.printStackTrace();
									GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, errorMessage, SWT.ICON_WARNING);
//									GUIUtil.openMessageWindow(
//											getWindowTitle(),
//											errorMessage,
//											SWT.ERROR);
//									handleError();
								}
								
							}
							
						}
						
						
						if(selectedItemsToExport.contains(ProjectSourceType.PaceArchiveFile)){
							
							if(pafPathFileName != null && pafPathFileName.length() > 0){
					
								try {
									returnValue = exportToFileset2(project, pafPathFileName, elements, cellReferencing, false, ProjectSerializationType.PAF);
								} catch (Throwable e) {
									e.printStackTrace();
									GUIUtil.openMessageWindow(
											getWindowTitle(),
											errorMessage,
											SWT.ERROR);
								}
								
							}
							
						}
						
						if(selectedItemsToExport.contains(ProjectSourceType.Template)){
							
							if(templateName != null && templateName.length() > 0){
								
								String newTemplatePath = templateDir + templateName + "." +  Constants.PAF_EXT;
								
								try {
									returnValue = exportToFileset2(project, newTemplatePath, elements, cellReferencing, false, ProjectSerializationType.PAF);
								} catch (Throwable e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
							
						}
						
						if(!returnValue){
							handleError();
						}
//			};
//		});
		
		return returnValue;
	}
	

//	private void deployProjectToServer(IProject project, PafServer server) {
//		try {
//			//try to deploy to server (added server TTN-751)
//			PafProjectUtil.deployProjectToServer(project, server);
//		} catch (PafServerNotFound e) {
//			e.printStackTrace();
////			uploadProjectError= true;
//		} catch (IOException e) {
//			e.printStackTrace();
////			uploadProjectError= true;
//		}
//				
//		File deployDir = new File(server.getHomeDirectory() + Constants.CONF_DIR);			
//		//if more than one file exist, upload success
//		if ( deployDir.list().length > 0 ) {
////			uploadProjectError = false;
//		}
//	}
	
/*	private void loadApplication(IProject project, PafServer server, boolean bLoadMdb) {
		String url = server.getCompleteWSDLService();
		PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
		if (pafServerAck != null) {
			PafService service = WebServicesUtil.getPafService(url);
			
			PafSuccessResponse pafSuccuessResponse = null;
			
			LoadApplicationRequest loadAppRequest = new LoadApplicationRequest();
			loadAppRequest.getAppIds().add(project.getName());
			loadAppRequest.setClientId(pafServerAck.getClientId());
			if( bLoadMdb )
				loadAppRequest.setLoadMdb(true);
			
			try {
				pafSuccuessResponse = service.loadApplication(loadAppRequest);
				if( pafSuccuessResponse == null ) 
					uploadProjectError = true;
				else {
					if( pafSuccuessResponse.isSuccess() ) 
						uploadProjectError = false;
					else
						uploadProjectError = true;
				}
			} catch (PafSoapException_Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				uploadProjectError = true;
			}
		}
	}*/
	
	public boolean exportToFileset2(IProject project, String sourceFile, ProjectElementId[] elements, 
			boolean excelCellReferencing, boolean addDependencies, ProjectSerializationType exportProjectType) throws Throwable{
		
		String confDir = project.getFolder(Constants.CONF_DIR).getLocation().toString();
		
		final ExportProjectToFileset exportProjectToFileset = 
			new ExportProjectToFileset(project.getName(), confDir, sourceFile, elements, excelCellReferencing, addDependencies, exportProjectType); 
		
		
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
				new Runnable() {
									
			public void run() {
			
				exportProjectToFileset.run();
				
			}
			
		});
		if(exportProjectToFileset.isError()){
			projectDataErrorMap = exportProjectToFileset.getProjectDataErrors();
			throw exportProjectToFileset.getThrowable();
		}
//		final CompletionAction completionAction = new
//        	CompletionAction(Constants.MENU_VIEW_ID, 
//        			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
//		
//		completionAction.setShowDialog(false);
//		completionAction.setOkTitle("Project exported successfully");
//		completionAction.setOkMsg("Successfully exported project: '" + project.getName() + "' to '" + sourceFile + "'");
//		completionAction.setFailTitle("Project exported failed");
//		completionAction.setFailMsg("There was an exception while exporting project: ' " + project.getName() + "'");
//
//		ClientJob job = new ClientJob("Exporting Project: '" + project.getName() + "' to '" + sourceFile + "'", completionAction, exportProjectToFileset);
//
//		// if short action, otherwise is long Job.LONG
//		//job.setPriority(Job.LONG);
//		// show a dialog immediately
//		job.setUser(true);
//		// start as soon as possible
//		job.schedule();
		return true;
	}
	
	public boolean exportToServer2(ProjectElementId[] elements, IProject project, 
			String serverName, boolean refreshConfiguration, boolean refreshCube) throws Throwable{
		
		//String confDir = project.getFolder(Constants.CONF_DIR).getLocation().toString();
		
		PafServer pafServer = null;
		
		try {
			//Get the PafServer object from the PafServerUtil
			pafServer = PafServerUtil.getServer(serverName);
			
		} catch (PafServerNotFound e) {

			logger.error("Server '" + serverName + "' was not found.");
		}
		
		
		final ExportProjectToServerAction exportProjectToServerAction = new ExportProjectToServerAction(workbench.getActiveWorkbenchWindow(), 
							project, pafServer, elements, refreshConfiguration, refreshCube);
				
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
				new Runnable() {
									
			public void run() {
			
				exportProjectToServerAction.run();
				
			}
			
		});
		if(exportProjectToServerAction.isError()){
			projectDataErrorMap = exportProjectToServerAction.getProjectDataErrors();
			throw exportProjectToServerAction.getThrowable();
		}
		
//		final CompletionAction completionAction = new
//    	CompletionAction(Constants.MENU_VIEW_ID, 
//    			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
//	
//		completionAction.setShowDialog(false);
//		completionAction.setOkTitle("Project exported successfully");
//		completionAction.setOkMsg("Successfully exported project: '" + project.getName() + "' to server: '" + pafServer.getName() + "'" );
//		completionAction.setFailTitle("Project exported failed");
//		completionAction.setFailMsg("There was an exception while exporting project: '" + project.getName() + "'");
//	
//		ClientJob job = new ClientJob("Exporting Project: '" + project.getName() + "' to server: '" + pafServer.getName() + "'", 
//				completionAction, 
//				exportProjectToServerAction);
//	
//		// if short action, otherwise is long Job.LONG
//		job.setPriority(Job.LONG);
//		// show a dialog immediately
//		job.setUser(true);
//		// start as soon as possible
//		job.schedule();
	
		
		return true;
	}
	
	
	private IProject getProject(String projectName){
		
	
		if(currentProjects != null && !currentProjects.isEmpty()){
			return currentProjects.get(projectName);
		}
		return null;
		//throw new IllegalArgumentException("Cannot find project: " + projectName);	

	}
	
//	private PafServer getServerByName( String serverName ) {
//		pafServers = PafServerUtil.getPafServers();
//		
//		if(pafServers != null && pafServers.size() > 0){
//			
//			for(PafServer server : pafServers){
//				if( server.getName().equalsIgnoreCase(serverName) )
//					return server;
//			}
//		}
//		return null;
//	}
	
	private void handleError(){
		
		List<ProjectDataError> errors = getProjectDataErrors();
		
		if(errors != null && errors.size() > 0){
			projectExportWizardPage.setProjectDataErrors(errors);
		} 
		
	}
	
	private List<ProjectDataError> getProjectDataErrors(){
		

		List<ProjectDataError> errors = new ArrayList<ProjectDataError>();
		
		if(projectDataErrorMap != null && projectDataErrorMap.keySet() != null){
			
			for(ProjectElementId element : projectDataErrorMap.keySet()){
				
				List<ProjectDataError> error = projectDataErrorMap.get(element);
				errors.addAll(error);
				
			}
			
		}
		
		return errors;
		
	}
	
}
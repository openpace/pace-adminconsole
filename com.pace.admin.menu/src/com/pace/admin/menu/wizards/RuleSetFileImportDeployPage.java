/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.menu.project.ProjectSource;

public class RuleSetFileImportDeployPage extends WizardPage {
	
	private java.util.List<ProjectSource> projectSourceList = new ArrayList<ProjectSource>();
	private java.util.List<String> fileExtenstionList = new ArrayList<String>();
	private Text text;
	private String fileNameLabel;
	private String fileDialogTitle;
	private String SelectedFileName;
	
	MenuPreferences preferences;
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.RuleSetFileImportandDeployPage";
	
	public RuleSetFileImportDeployPage(String wizardPageName, String wizardPageTitle,
			String wizardPageDesc, String[] itemList, String fileNameLabel, String fileDialogTitle, java.util.List<String> fileExtenstionList)
	{
		super(wizardPageName);
		setPageComplete(false);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
				
		preferences = new MenuPreferences(PREF_NODE);
		
		if ( projectSourceList != null ) {
			this.projectSourceList.addAll(projectSourceList);
		}
		

		this.fileNameLabel = fileNameLabel;
		this.fileDialogTitle = fileDialogTitle;
		
		if ( fileExtenstionList != null ) {
			
			this.fileExtenstionList.addAll(fileExtenstionList);
			
		}
	}
	
	public void createControl(Composite parent){
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
		container.setBounds(0, 0, 200, 300);
	//	container.layout(false);
		setControl(container);
	}
/*
	public void createControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(3, false));
		setControl(container);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			Label lblProjectName_1 = new Label(container, SWT.NONE);
			lblProjectName_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			
			if ( fileNameLabel != null ) {				
				lblProjectName_1.setText(fileNameLabel);
			} else {
				lblProjectName_1.setText("File Name:");
			}
		}
		{
			text = new Text(container, SWT.BORDER | SWT.READ_ONLY);
			text.setEnabled(false);
			text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			String fileName = preferences.getStringSetting(getPageSettingsPrefix() + "text");
			
			if(fileName != null){
				File file = getFileHandle(fileName);
				if(file.exists()){
					text.setText(fileName);
				}
		
			}
		}
		{
			Button btnBrowse = new Button(container, SWT.NONE);
			btnBrowse.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					// Create a new file dialog
					FileDialog fd =  new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
					
					// Set the title
					fd.setText(fileDialogTitle);
					
					// Set the default directory
					String filterPath = preferences.getStringSetting(getPageSettingsPrefix() + "lastFilterPath");
					
					if(filterPath != null){
						
						fd.setFilterPath(filterPath);
					}else{
						
						fd.setFilterPath("C:/");
						
					}
					
					// Set the filtered file extension
					if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {					
				        
						String[] filterExt = fileExtenstionList.toArray(new String[0]);
				        String[] filterStr = new String[1];
				        filterStr[0] = "";
						for (int i = 0; i < filterExt.length; i++ ) {
							
//							filterExt[i] = "*." + filterExt[i];
							filterStr[0]  += "*." + filterExt[i];
							if( i != filterExt.length -1 ) {
								filterStr[0]  += ";";
							}
						}
						
				        fd.setFilterExtensions(filterStr);
//				        fd.setFilterExtensions(filterExt);
				        
					}
			        
			        // Get the selected path string
			        String selected = fd.open();
			        
			        if(FileHelperUtil.doesFileExist(selected)){
			        	
			        }
			        
			        if ( selected != null ) {
			            
			        	preferences.saveStringSetting(getPageSettingsPrefix() + "lastFilterPath", fd.getFilterPath());

			    		if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {
			    		
			    			boolean validFileExtension = false;
			    			
			    			if ( fd.getFilterIndex() >= 0 ) {
			    				
			    				validFileExtension = true;
			    				
//			    				String selectedExtension = fileExtenstionList.get(fd.getFilterIndex());
//			    				
//			    				if ( ! selected.endsWith("." + selectedExtension)) {
//			    					
//			    					text.setText(selected + "." + selectedExtension );
//			    					
//			    				} else {
			    					
			    					text.setText(selected);
			    					
//			    				}			    				
		    						    				
			    			} else {
			    				
			    				for (String fileExtension : fileExtenstionList ) {
				    				
				    				if ( selected.endsWith("." + fileExtension)) {
				    					
				    					validFileExtension = true;
				    					
				    					text.setText(selected);
				    					
				    					break;
				    				}
			    				}
			    				
			    			}			    			
			    			
			    			if ( validFileExtension ) {

			    				setPageComplete(pageComplete());
			    				
			    			} else {
			    				
			    				setPageComplete(false);
			    				
			    			}
			    			
			    		} else {
			    			//i don't think this should ever happen, so I set it 
			    			// to false.  KRM-1/21/2010
			    			setPageComplete(false);
			    			
			    		}
			        		
			        			        	
			        }

				}
			});
			btnBrowse.setText("Browse...");
		}

		setPageComplete(pageComplete());

	}
	
	*/
	
	private boolean pageComplete(){
		
		if(text != null && text.getText().trim().length() > 0 && validateFile()){
			
			setSelectedFileName(text.getText().trim());
			preferences.saveStringSetting(getPageSettingsPrefix() + "text", text.getText().trim());
			return true;
			
		}
		
		return false;
		
	}
	
	private String getPageSettingsPrefix(){
		return getName() + ".";
	}

	public String getSelectedFileName() {
		return SelectedFileName;
	}

	public void setSelectedFileName(String selectedFileName) {
		SelectedFileName = selectedFileName;
	}
	
	 private File getFileHandle(String pathFileName) {
	    	
	    	//get file to deploy
	    	File file = new File(pathFileName);
	    	
	        return file;
	    }
	 
	 /**
	     * Returns whether this page's controls currently all contain valid 
	     * values.
	     *
	     * @return <code>true</code> if all controls are valid, and
	     *   <code>false</code> if at least one is invalid
	     */
	    protected boolean validateFile() {
	        //IWorkspace workspace = ResourcesPlugin.getWorkspace();

	        String projectFieldContents = getTemplateNameFieldValue();
	        if (projectFieldContents.equals("")) { //$NON-NLS-1$
	            setErrorMessage("Export filename is blank.");
	            return false;
	        }

//	        IStatus nameStatus = workspace.validateName(projectFieldContents,
//	                IResource.PROJECT);
//	        if (!nameStatus.isOK()) {
//	            setErrorMessage(nameStatus.getMessage());
//	            return false;
//	        }

	        File handle = getFileHandle(projectFieldContents);
	        if (!handle.exists()) {
	            setErrorMessage("Invalid file.  File does not exists.");
	            return false;
	        }
	                

	        setErrorMessage(null);
	        setMessage(null);
	        return true;
	    }

	    
	    /**
	     * Returns the value of the project name field
	     * with leading and trailing spaces removed.
	     * 
	     * @return the project name in the field
	     */
	    private String getTemplateNameFieldValue() {
	        if (text == null) {
				return ""; //$NON-NLS-1$
			}

	        return text.getText().trim();
	    }
		
	    
}

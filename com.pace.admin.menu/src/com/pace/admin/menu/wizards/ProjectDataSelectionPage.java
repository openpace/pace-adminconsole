/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.springframework.util.StringUtils;

import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectElementIdComparator;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ProjectDataSelectionPage extends WizardPage {
	private static class Sorter extends ViewerSorter {
		
		private int propertyIndex;
		private static final int ASCENDING = 0;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;
		
		public Sorter() {
			this.propertyIndex = 0;
			//this.direction = DESCENDING;
			this.direction = ASCENDING;
		}
		
		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int rc = 0;

			if(e1 instanceof ProjectElementId && e2 instanceof ProjectElementId){
				
				ProjectElementId p1 = (ProjectElementId) e1;
				
				ProjectElementId p2 = (ProjectElementId) e2;
				
				switch ( propertyIndex ) {
				
					case 0: 
						rc =  p1.getName().compareToIgnoreCase(p2.getName());
						break;
					case 1:
						rc =  p1.getDescription().compareToIgnoreCase(p2.getDescription());
						break;
					default:
						rc = 0;

				}
				
				// If descending order, flip the direction
				if (direction == DESCENDING) {
					rc = -rc;
				}
				return rc;

				
			}
			return rc;
		}
		
	}
	
	
	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		public String getColumnText(Object element, int columnIndex) {
			
			if ( element instanceof ProjectElementId ) {
				
				switch (columnIndex) {
				
				case 0:
					return ((ProjectElementId) element).getName();
				case 1:
					return ((ProjectElementId) element).getDescription();				
				}
				
			}
			
			return element.toString();
		}
	}
	private static class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (Object[]) inputElement;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	private Table table;
	private CheckboxTableViewer checkboxTableViewer;
	private TableColumn tblclmnDescription;
	private ProjectElementId[] selectedProjectItems;
	private TableColumn tblclmnProjectItem;
	private MenuPreferences preferences;
	private Sorter tableSorter;
	private boolean ignoreDependencies;
	private boolean displayIgnoreDependencies;
	private java.util.List<ProjectElementId> projectDataIdList = new ArrayList<ProjectElementId>();
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.ProjectDataSelectionPage";
	private Button buttonIgnoreDependencies;
	
	

	public ProjectDataSelectionPage(String wizardPageName, String wizardPageTitle, 
			String wizardPageDesc, boolean displayIgnoreDependencies) {
		
		super(wizardPageName);		
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
		
		this.displayIgnoreDependencies = displayIgnoreDependencies;
		
		preferences = new MenuPreferences(PREF_NODE);
		
		if ( ProjectElementId.values() != null ) {
			
			
			//projectDataIdList = Arrays.asList(ProjectElementId.values());
			
			
//			//this is bad, but none of the compareto operators work in ProjectElementId!
			for(ProjectElementId elementId : ProjectElementId.values()){
				
				if(elementId != null){
					
					if(!elementId.toString().equalsIgnoreCase(ProjectElementId.RuleSet_Rule.toString()) &&
					   !elementId.toString().equalsIgnoreCase(ProjectElementId.RuleSet_RuleGroup.toString()) && 
					   !elementId.toString().equalsIgnoreCase(ProjectElementId.RuleSet_RuleSet.toString())){
							
							projectDataIdList.add(elementId);
							
					}
					
				}
				
			}
			
			ProjectElementIdComparator compare = new ProjectElementIdComparator();
			Collections.sort(projectDataIdList, compare);
			
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createControl(Composite parent) {
	
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
		setControl(container);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			tableSorter = new Sorter();
			checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
			checkboxTableViewer.setSorter(tableSorter);
			checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
				public void checkStateChanged(CheckStateChangedEvent event) {
					setSelectedProjectItems(Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new ProjectElementId[0]));
					setPageComplete(pageComplete());
				}
			});
			checkboxTableViewer.setLabelProvider(new TableLabelProvider());
			checkboxTableViewer.setContentProvider(new ContentProvider());
			table = checkboxTableViewer.getTable();
			table.setHeaderVisible(true);
			table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			{
				tblclmnProjectItem = new TableColumn(table, SWT.NONE);
				tblclmnProjectItem.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						doSort(tblclmnProjectItem, 0);
					}
				});
				tblclmnProjectItem.setWidth(155);
				tblclmnProjectItem.setText("Data Item");
			}
			{
				tblclmnDescription = new TableColumn(table, SWT.NONE);
				tblclmnDescription.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						
						doSort(tblclmnDescription, 1);
						
					}
				});
				tblclmnDescription.setWidth(468);
				tblclmnDescription.setText("Description");
			}
		}
		{
			final Button btnSelectDeselect = new Button(container, SWT.CHECK);
			btnSelectDeselect.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					checkboxTableViewer.setAllChecked(btnSelectDeselect.getSelection());
					
					if ( checkboxTableViewer.getCheckedElements().length > 0 ){
						setSelectedProjectItems(Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new ProjectElementId[0]));
					} 
					setPageComplete(pageComplete());
				}
			});
			btnSelectDeselect.setSelection(true);
			btnSelectDeselect.setText("Select / Deselect All");
		}
		{
			buttonIgnoreDependencies = new Button(container, SWT.CHECK);
			buttonIgnoreDependencies.setToolTipText("If checked the import process will not import any dependent files.  If creating a new project, the value of this setting is ignored.");
			buttonIgnoreDependencies.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setIgnoreDependencies(buttonIgnoreDependencies.getSelection());
				}
			});
			buttonIgnoreDependencies.setText("Ignore Dependencies");
			if(!displayIgnoreDependencies){
				buttonIgnoreDependencies.setVisible(false);
				setIgnoreDependencies(false);
			}
			//TTN-2297 - Change AC Import wizard default of "Ignore Dependencies" to be checked
			buttonIgnoreDependencies.setSelection(true);
		}
		
		
			String[] s = StringUtils.commaDelimitedListToStringArray(preferences.getStringSetting(getPageSettingsPrefix() + "checkboxTableViewer"));
		
			if(s != null && s.length > 0){
				
				List<ProjectElementId> elements = new ArrayList<ProjectElementId>();
				
				for(String str : s){
					
					ProjectElementId element = ProjectElementId.valueOf(str);
					
					if(element != null){
						
						elements.add(element);
						
					}
					
				}

				checkboxTableViewer.setInput(projectDataIdList.toArray(new ProjectElementId[0]));
				checkboxTableViewer.setCheckedElements(elements.toArray(new ProjectElementId[0]));
				setSelectedProjectItems(elements.toArray(new ProjectElementId[0]));
				
				
			}else if ( projectDataIdList.size() > 0 ) {
		
				checkboxTableViewer.setInput(projectDataIdList.toArray(new ProjectElementId[0]));
				checkboxTableViewer.setAllChecked(true);
				setSelectedProjectItems(projectDataIdList.toArray(new ProjectElementId[0]));
				
			}
			
			setPageComplete(pageComplete());

	}
	
	private boolean pageComplete(){
		
		if(checkboxTableViewer != null && checkboxTableViewer.getCheckedElements().length > 0){
			
			return true;
			
		}
		
		return false;
		
	}
	
	private void doSort(TableColumn column, int index){
		
		tableSorter.setColumn(index);
		int dir = checkboxTableViewer.getTable().getSortDirection();
		if (checkboxTableViewer.getTable().getSortColumn() == column) {
			dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
		} else {

			dir = SWT.DOWN;
		}
		checkboxTableViewer.getTable().setSortDirection(dir);
		checkboxTableViewer.getTable().setSortColumn(column);
		checkboxTableViewer.refresh();
		
	}
	
	private String getPageSettingsPrefix(){
		return getName() + ".";
	}
	
	
	/*
	 * 
	 */
	public void setSelectedProjectItems(ProjectElementId[] selectedProjectItems) {
		this.selectedProjectItems = selectedProjectItems;
		
		String s = StringUtils.collectionToDelimitedString(Arrays.asList(checkboxTableViewer.getCheckedElements()), ",");
		
		preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", s);
		
	}
	
	/*
	 * Get the checked items.
	 */
	public ProjectElementId[] getSelectedProjectItems() {
		return selectedProjectItems;
	}

	public boolean isIgnoreDependencies() {
		return ignoreDependencies;
	}

	public void setIgnoreDependencies(boolean ignoreDependencies) {
		this.ignoreDependencies = ignoreDependencies;
	}

}

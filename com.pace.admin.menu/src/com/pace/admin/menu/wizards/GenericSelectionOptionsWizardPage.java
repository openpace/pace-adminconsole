/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.springframework.util.StringUtils;

import com.pace.admin.menu.preferences.MenuPreferences;

public class GenericSelectionOptionsWizardPage extends WizardPage {
	
	private CheckboxTableViewer checkboxTableViewer;
	
	private Table optionTable;
	
	private List<String> optionList;
	
	

	private boolean isMultiSelection;

	private String optionsText;
	
	private String[] selectedItems;
	
	private List<String> initallyCheckedItemList;
	
	MenuPreferences preferences;
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.GenericSelectionOptionsWizardPage";
	
	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		
		public String getColumnText(Object element, int columnIndex) {
			return (String) element;
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
	}

	private class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (String[]) inputElement;
		}

		public void dispose() {
		}
		
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	
	/**
	 * Create the wizard.
	 */
	public GenericSelectionOptionsWizardPage(String wizardPageName, String wizardPageTitle, 
			String wizardPageDesc, String optionsText, String[] optionList, String[] initallyCheckedItems,
			boolean isMultiSelection) {

		super(wizardPageName);
		
		preferences = new MenuPreferences(PREF_NODE);
		
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
		this.optionsText = optionsText;
		
		if ( optionList != null ) {
			
			this.optionList = Arrays.asList(optionList);
			
		}
		
		if(initallyCheckedItems != null && initallyCheckedItems.length > 0){
			
		//	this.initallyCheckedItemList = Arrays.asList(initallyCheckedItems);
			
		}
		
		
		this.isMultiSelection = isMultiSelection;
		
		
		
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));

		setControl(container);
		{
			
			//DEFAULT IS SWT.SINGLE
			int singleSelectionStyle = SWT.BORDER | SWT.FULL_SELECTION ;
			
			if ( isMultiSelection ) {
				
				singleSelectionStyle = SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI;
				
			}
			{
							
				Label lblOptionsText = new Label(container, SWT.NONE);
				lblOptionsText.setText(optionsText);
			}
			
			checkboxTableViewer = CheckboxTableViewer.newCheckList(container, singleSelectionStyle);
			
			checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
				public void checkStateChanged(CheckStateChangedEvent event) {
					
					if ( isMultiSelection ) {
						
						if ( checkboxTableViewer != null && checkboxTableViewer.getCheckedElements().length > 0 ) {
							
							String s = StringUtils.collectionToDelimitedString(Arrays.asList(checkboxTableViewer.getCheckedElements()), ",");
							
							preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", s);
							
							setPageComplete(pageComplete());
							
						} else {
							
							preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", "");
							
							setPageComplete(pageComplete());
							
						}
						
												
					} else {
						
						if ( event.getChecked() ) {
							
							String checkedItem = (String) event.getElement();
							
							checkboxTableViewer.setCheckedElements(new String[] { checkedItem });
							
							preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", checkedItem);
							
							setPageComplete(pageComplete());
													
						} else {
						
							checkboxTableViewer.setAllChecked(false);
							
							preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", "");
							
							setPageComplete(pageComplete());	
							
						}
						
					}
					setSelectedItems(Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new String[0]));
					
				}
			});
			checkboxTableViewer.setContentProvider(new ContentProvider());
			checkboxTableViewer.setLabelProvider(new TableLabelProvider());
			checkboxTableViewer.setAllChecked(false);
			optionTable = checkboxTableViewer.getTable();
			optionTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			{
				//TableViewerColumn tableViewerColumn = new TableViewerColumn(checkboxTableViewer, SWT.NONE);
				TableColumn tableColumn = new TableColumn(optionTable, SWT.None);//tableViewerColumn.getColumn();
				tableColumn.setText("Option");
				tableColumn.setWidth(300);
			}
			
			if ( this.optionList != null ) {
				checkboxTableViewer.setInput(this.optionList.toArray(new String[0]));
				setSelectedItems(Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new String[0]));
				
				String[] s = StringUtils.commaDelimitedListToStringArray(preferences.getStringSetting(getPageSettingsPrefix() + "checkboxTableViewer"));
				
				if(isMultiSelection && s != null && s.length > 0){
				
					setSelectedItems(Arrays.asList(s).toArray(new String[0]));
					checkboxTableViewer.setCheckedElements(Arrays.asList(s).toArray(new String[0]));
					
					
				} else if(!isMultiSelection && s != null && s.length > 0){
					
					setSelectedItems(new String[] {s[0]});
					checkboxTableViewer.setCheckedElements(new String[] {s[0]});
					
				}
				
			}
			
			//if initally checked items exists, check the items.
			if (  initallyCheckedItemList != null && initallyCheckedItemList.size() > 0 ) {
				
				checkboxTableViewer.setCheckedElements(initallyCheckedItemList.toArray(new String[0]));
				setSelectedItems((initallyCheckedItemList.toArray(new String[0])));
				//preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", (initallyCheckedItemList.toArray(new String[0]));
			} 
			
		}
		
		setPageComplete(pageComplete());
	}
	
	
	private boolean pageComplete(){
		
		if(checkboxTableViewer != null && checkboxTableViewer.getCheckedElements().length > 0){
			
			return true;
			
		}
		
		return false;
		
	}
	
	private String getPageSettingsPrefix(){
		return getName() + ".";
	}
	
	public boolean isMultiSelection() {
		return isMultiSelection;
	}
	
	/**
	 * @return the selectedItems
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}

	/**
	 * @param selectedItems the selectedItems to set
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = null;
		this.selectedItems = selectedItems;
		//TODO move pref save in this method.
	}
	
	public List<String> getOptionList() {
		return optionList;
	}

	public void setOptionList(List<String> optionList) {
		this.optionList = optionList;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.base.project.ProjectDataError;

public class ProjectNameSelectionWizardPage extends WizardPage {
	private Text txtNewProject;
	private Combo combo;
	private Button btnNew;
	private Button btnExisting;
	private Button btnShowImportErrors;
	private Label lblExistingProjects;
	private Label lblNew;
	private List<String> itemList;
	private List<ProjectDataError> projectDataErrors;
	private boolean existingOptionsEnabled;
	
	private MenuPreferences preferences;
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.ProjectNameSelectionWizardPage";
	
	private static Logger logger = Logger.getLogger(ProjectNameSelectionWizardPage.class);
	
	
	public ProjectNameSelectionWizardPage(String wizardPageName, String wizardPageTitle, 
			String wizardPageDesc, boolean existingOptionsEnabled, String[] itemList) {
		
		super(wizardPageName);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
		
		preferences = new MenuPreferences(PREF_NODE);
	
		
		this.existingOptionsEnabled = existingOptionsEnabled;
		
		if(itemList != null && itemList.length > 0){
			this.itemList = new ArrayList<String>();
			this.itemList.addAll(Arrays.asList(itemList));
		} else{
			this.itemList = new ArrayList<String>();
		}
		
		
		
	}

	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
		setControl(container);
		{
			Group group = new Group(container, SWT.NONE);
			{
				GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
				gridData.heightHint = 153;
				gridData.widthHint = 558;
				group.setLayoutData(gridData);
			}
			{
				lblNew = new Label(group, SWT.NONE);
				lblNew.setBounds(10, 44, 135, 15);
				lblNew.setText("New Project Name");
			}
			{
				txtNewProject = new Text(group, SWT.BORDER);
				txtNewProject.setTextLimit(128);
				txtNewProject.addModifyListener(new ModifyListener() {
					public void modifyText(ModifyEvent e) {
						
						validateSelections();
						
					}
				});
				txtNewProject.setBounds(151, 44, 403, 21);
				checkTextLenght();
			}
			{
				btnNew = new Button(group, SWT.RADIO);
				btnNew.setSelection(true);
				btnNew.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						validateSelections();
					}
				});
				btnNew.setBounds(10, 22, 191, 16);
				btnNew.setText("Create a new project");
			}
			{
				btnExisting = new Button(group, SWT.RADIO);
				btnExisting.setBounds(10, 87, 211, 16);
				btnExisting.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						validateSelections();
					}
				});
				btnExisting.setText("Use an existing project");
				btnExisting.setEnabled(existingOptionsEnabled);
			}
			boolean status = false;
			if(existingOptionsEnabled && !txtNewProject.getEnabled()){
				status = true;
			}
			{
				lblExistingProjects = new Label(group, SWT.NONE);
				lblExistingProjects.setBounds(12, 109, 110, 15);
				lblExistingProjects.setText("Existing projects");
				lblExistingProjects.setEnabled(status);
			}
			{
				combo = new Combo(group, SWT.READ_ONLY);
				combo.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						validateSelections();
					}
				});
				combo.setItems(itemList.toArray(new String[0]));
				combo.setEnabled(false);
				combo.setBounds(151, 109, 403, 23);
				combo.setEnabled(status);
			}
		}
		{
			btnShowImportErrors = new Button(container, SWT.NONE);
			btnShowImportErrors.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					ProjectErrorsDialog projectDataErrors = ProjectWizardUtil.getProjectImportErrorsWizardPage(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
							getProjectDataErrors());
					
					projectDataErrors.open();
				}
			});
			btnShowImportErrors.setEnabled(false);
			btnShowImportErrors.setText("Show errors");
		}
		
		loadSettings();
		
	}
	
	
	private void loadSettings(){
		
		boolean bNew = preferences.getBooleanSetting(getPageSettingsPrefix() + "btnNew");
		boolean bExisting = preferences.getBooleanSetting(getPageSettingsPrefix() + "btnExisting");
		
		if(bExisting){
			
			btnNew.setSelection(false);
			btnExisting.setFocus();
			btnExisting.setSelection(true);
			
			String s = preferences.getStringSetting(getPageSettingsPrefix() + "combo");
			int i = combo.indexOf(s);
			if(i >= 0){
				
				combo.select(i);
				
			}
			validateSelections();
			
			
		} else if(bNew){
			
			btnExisting.setSelection(false);
			btnNew.setFocus();
			btnNew.setSelection(true);
			
			
			String s = preferences.getStringSetting(getPageSettingsPrefix() + "txtNewProject");
			
			if(s != null && s.length() > 0){
				txtNewProject.setText(s.trim());
			}
			
			validateSelections();
		}
		
	}
	
	private String getPageSettingsPrefix(){
		return getName() + ".";
	}
	
	private boolean checkForExistingProject(String project){
		if(itemList.contains(project)){
			return true;
		}
		return false;
	}
	
	private boolean validateSelections() {
		
		boolean value = false;

		if(btnNew.getSelection()){
			preferences.saveBooleanSetting(getPageSettingsPrefix() + "btnNew", btnNew.getSelection());
			preferences.saveBooleanSetting(getPageSettingsPrefix() + "btnExisting", false);
			txtNewProject.setEnabled(true);
			lblNew.setEnabled(true);
			combo.setEnabled(false);
			lblExistingProjects.setEnabled(false);
			return checkTextLenght();
		} else if(btnExisting.getSelection()){
			preferences.saveBooleanSetting(getPageSettingsPrefix() + "btnExisting", btnExisting.getSelection());
			preferences.saveBooleanSetting(getPageSettingsPrefix() + "btnNew", false);
			txtNewProject.setEnabled(false);
			lblNew.setEnabled(false);
			combo.setEnabled(true);
			lblExistingProjects.setEnabled(true);
			return checkComboIndex();
		}
		return value;
	}
	
	private boolean checkTextLenght(){
		String text = txtNewProject.getText().trim();
		boolean b = validatePage();
		if(!b){
			setPageComplete(false);
			return false;
		} else if(text == "" || checkForExistingProject(text)){
			setPageComplete(false);
			return false;
		}
		preferences.saveStringSetting(getPageSettingsPrefix() + "combo", "");
		preferences.saveStringSetting(getPageSettingsPrefix() + "txtNewProject", txtNewProject.getText().trim());
		setPageComplete(true);
		return true;
	}
	
	private boolean checkComboIndex(){
		if(combo != null && combo.getSelectionIndex() >= 0){
			setErrorMessage(null);
		    setMessage(null);
			setPageComplete(true);
			preferences.saveStringSetting(getPageSettingsPrefix() + "txtNewProject", "");
			preferences.saveStringSetting(getPageSettingsPrefix() + "combo", combo.getText().trim());
			return true;
		}
		setErrorMessage("Please select a valid project from the dropdown box.");
		setPageComplete(false);
		return false;
	}
	
	
	  /**
     * Returns the value of the project name field
     * with leading and trailing spaces removed.
     * 
     * @return the project name in the field
     */
    private String getProjectNameFieldValue() {
        if (txtNewProject == null) {
			return ""; //$NON-NLS-1$
		}

        return txtNewProject.getText().trim();
    }

    
    /**
	 * Creates a project resource handle for the current project name field
	 * value. The project handle is created relative to the workspace root.
	 * <p>
	 * This method does not create the project resource; this is the
	 * responsibility of <code>IProject::create</code> invoked by the new
	 * project resource wizard.
	 * </p>
	 * 
	 * @return the new project resource handle
	 */
    public IProject getProjectHandle() {
        return ResourcesPlugin.getWorkspace().getRoot().getProject(getProjectName());
    }

	
	/**
     * Returns whether this page's controls currently all contain valid 
     * values.
     *
     * @return <code>true</code> if all controls are valid, and
     *   <code>false</code> if at least one is invalid
     */
    protected boolean validatePage() {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();

        String projectFieldContents = getProjectNameFieldValue();
        if (projectFieldContents.equals("")) { //$NON-NLS-1$
            setErrorMessage("Project name cannot be blank.");
            return false;
        }

        IStatus nameStatus = workspace.validateName(projectFieldContents,
                IResource.PROJECT);
        if (!nameStatus.isOK()) {
            setErrorMessage(nameStatus.getMessage());
            return false;
        }

        IProject handle = getProjectHandle();
        if (handle.exists()) {
            setErrorMessage("Project \"" + projectFieldContents + "\" already exists.");
            return false;
        }
                
//        IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(getProjectNameFieldValue());
//		locationArea.setExistingProject(project);
//		
//		String validLocationMessage = locationArea.checkValidLocation();
//		if (validLocationMessage != null) { // there is no destination location given
//			setErrorMessage(validLocationMessage);
//			return false;
//		}

        setErrorMessage(null);
        setMessage(null);
        return true;
    }
	
	public boolean createNewProject(){
		if(btnNew.getSelection()){
			return true;
		} else if(btnExisting.getSelection()){
			return false;
		}
		return false;
	}
	
	public String getProjectName() {
		
		String projectName = null;
		
		if ( btnNew.getSelection() && txtNewProject != null && ! txtNewProject.isDisposed() && txtNewProject.getText().trim().length() != 0 ) {
			
			projectName = txtNewProject.getText().trim();
			
		} else if( btnExisting.getSelection() && combo != null && ! combo.isDisposed() && combo.getText().trim().length() != 0 ){
			
			projectName = combo.getText().trim();
			
		}
		
		return projectName;
		
	}

	/**
	 * @return the projectDataErrors
	 */
	public List<ProjectDataError> getProjectDataErrors() {
		return projectDataErrors;
	}

	/**
	 * @param projectDataErrors the projectDataErrors to set
	 */
	public void setProjectDataErrors(List<ProjectDataError> projectDataErrors) {
		this.projectDataErrors = projectDataErrors;
		if(this.projectDataErrors != null && this.projectDataErrors.size() > 0){
			btnShowImportErrors.setEnabled(true);
		} else{
			btnShowImportErrors.setEnabled(false);
		}
	}

	//TTN-2161 Set project name in preferences
	public void setExistingProjectNamePreference( String projectName){
		preferences.saveStringSetting(getPageSettingsPrefix() + "combo", projectName);
	}
}

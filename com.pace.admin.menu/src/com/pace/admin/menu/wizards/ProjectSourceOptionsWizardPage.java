/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.wizards;

import java.util.ArrayList;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.menu.project.ProjectSource;

/**
 * @author jmilliron
 *
 */
public class ProjectSourceOptionsWizardPage extends WizardPage {
	
	private Table table;

	private java.util.List<ProjectSource> projectSourceList = new ArrayList<ProjectSource>();
	
	private java.util.List<ProjectSource> initallyCheckedItemList = new ArrayList<ProjectSource>();

	private CheckboxTableViewer checkboxTableViewer;
	
	
	MenuPreferences preferences;
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.ProjectSourceOptionsWizardPage";
	
	public ProjectSourceOptionsWizardPage(String wizardPageName, String wizardPageTitle,
			String wizardPageDesc, java.util.List<ProjectSource> projectSourceList, java.util.List<ProjectSource> initallyCheckedItemList) {
		
		super(wizardPageName);
		setPageComplete(false);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
				
		preferences = new MenuPreferences(PREF_NODE);
		
		if ( projectSourceList != null ) {
			this.projectSourceList.addAll(projectSourceList);
		}
		
		if ( initallyCheckedItemList != null ) {
			this.initallyCheckedItemList.addAll(initallyCheckedItemList);
		}
					
	}
	
	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		public String getColumnText(Object element, int columnIndex) {
			
			if ( element instanceof ProjectSource ) {
				
				ProjectSource ProjectSource = (ProjectSource) element;
				
				switch ( columnIndex ) {
				
				case 0: 
					return ProjectSource.getName();
				case 1:
					return ProjectSource.getDescription();
					
				}
				
			}
			return element.toString();
		}
	}
	private static class ContentProvider implements IStructuredContentProvider {
		
		public Object[] getElements(Object inputElement) {
			
			/*if ( inputElement instanceof List ) {
				
				List<ProjectSource> paceSourceList = (List) inputElement;
				
				return paceSourceList.toArray(new ProjectSource[0]);
				
			} else if ( inputElement instanceof ProjectSource[] ) {
				
				return (ProjectSource[]) inputElement;
				
			}*/
			
			return (Object[]) inputElement;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createControl(Composite parent) {
		
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(4, false));
		
		setControl(container);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
			checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
				public void checkStateChanged(CheckStateChangedEvent event) {				
									
					if ( event.getChecked() ) {
						
						ProjectSource checkedItem = (ProjectSource) event.getElement();
						
						checkboxTableViewer.setCheckedElements(new ProjectSource[] { checkedItem });
							
						preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", checkedItem.getName());
						
						setPageComplete(true);
												
					} else {
					
						checkboxTableViewer.setAllChecked(false);
						
						preferences.saveStringSetting(getPageSettingsPrefix() + "checkboxTableViewer", "");
						
						setPageComplete(false);	
						
					}
					
									}
			});
			checkboxTableViewer.setLabelProvider(new TableLabelProvider());
			checkboxTableViewer.setContentProvider(new ContentProvider());
			
			table = checkboxTableViewer.getTable();
			table.setHeaderVisible(true);
			table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
			{
				TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
				tblclmnName.setWidth(121);
				tblclmnName.setText("Option");
			}
			{
				TableColumn tblclmnDescription = new TableColumn(table, SWT.NONE);
				tblclmnDescription.setWidth(435);
				tblclmnDescription.setText("Description");
			}
			
			checkboxTableViewer.setInput(projectSourceList.toArray(new ProjectSource[0]));
						
		}
		
		//if initally checked items exists, check the items.
		if ( initallyCheckedItemList.size() > 0 ) {
			
			checkboxTableViewer.setCheckedElements(initallyCheckedItemList.toArray(new ProjectSource[0]));
			
			setPageComplete(true);
			
		} else {
		
			String s = preferences.getStringSetting(getPageSettingsPrefix() + "checkboxTableViewer");
			
			for(ProjectSource project : projectSourceList){
				
				if(project.getName().toLowerCase().equalsIgnoreCase(s)){
					
					checkboxTableViewer.setCheckedElements(new ProjectSource[] {project});
					
					setPageComplete(true);
					
					return;
					
				}
			}
			
			setPageComplete(false);
			
		}

	}
	
	private String getPageSettingsPrefix(){
		return getName() + ".";
	}
	
	public java.util.List<ProjectSource> getSelectedSourceOptions() {
		
		java.util.List<ProjectSource> checkedItems = new ArrayList<ProjectSource>();
		
		if (checkboxTableViewer != null ) {
			
			for (Object checkedObject : checkboxTableViewer.getCheckedElements()) {
				
				if ( checkedObject instanceof ProjectSource) {
					
					checkedItems.add((ProjectSource) checkedObject);
					
				}
				
			}			
			
		}
		
		return checkedItems;
		
	}
}

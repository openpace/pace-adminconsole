/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.menu.preferences.MenuPreferences;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.base.project.ProjectDataError;

public class ProjectExportTypeSelectionPage extends WizardPage {
	private Text txtPaf;
	private Text txtTemplate;
	private Text txtExcel;
	private Button chkServer;
	private Combo cboServer;
	private Button chkPafArchive;
	private Button chkTemplate;
	private Button chkExcel;
	private Button cmdPaf;
	private Button cmdExcel;
	private String fileDialogTitle;
	private Label lblCellReferencing;
	private Label lblAddDep;
	private Combo cboCellReferencing;
	private Combo cboAddDep;
	private Group grpExcelFileOptions;
	private List<String> excelFileExtenstionList = new ArrayList<String>();
	private List<String> pafFileExtenstionList = new ArrayList<String>();
	private List<String> projectNameList;
	private List<String> templateNameList;

	
	private String serverName;
	private String excelPathFileName;
	private boolean excelCellReferences;
	private boolean addDependencies;
	private String pafPathFileName;
	private String templateName;
	private Set<ProjectSourceType> selectedItemsToExport;
	
	private MenuPreferences preferences;
	
	private final String PREF_NODE = "com.pace.admin.menu.wizards.ProjectExportTypeSelectionPage";
	private Button btnApplyServerConfig;
	private Button btnApplyCubeChanges;
	private boolean applyConfig = true;
	private boolean applyCube = false;
	
	private Button btnShowExportErrors;
	private List<ProjectDataError> projectDataErrors;
	
	public ProjectExportTypeSelectionPage(String wizardPageName, String wizardPageTitle, 
			String wizardPageDesc,String[] projectNameList, List<String> templateNameList, 
			List<String> excelFileExtenstionList, List<String> pafFileExtenstionList) {
		
		super(wizardPageName);
		
		setPageComplete(false);
		setTitle(wizardPageTitle);
		setDescription(wizardPageDesc);
		fileDialogTitle = "Select a file:";
		
		preferences = new MenuPreferences(PREF_NODE);

		
		if(excelFileExtenstionList != null){
			this.excelFileExtenstionList.addAll(excelFileExtenstionList);
		}
		
		if(pafFileExtenstionList != null){
			this.pafFileExtenstionList.addAll(pafFileExtenstionList);
		}
		
		if(templateNameList != null && templateNameList.size() > 0){
			this.templateNameList = templateNameList;
		} else{
			this.templateNameList = new ArrayList<String>();
		}
		
		if(projectNameList != null && projectNameList.length > 0){
			this.projectNameList = new ArrayList<String>();
			this.projectNameList.addAll(Arrays.asList(projectNameList));
		} else{
			this.projectNameList = new ArrayList<String>();
		}
		
		selectedItemsToExport = new HashSet<ProjectSourceType>();
		
	}
	
	
	public void createControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(3, false));
		setControl(container);
		{
			chkServer = new Button(container, SWT.CHECK);
			chkServer.setSelection(preferences.getBooleanSetting("chkServer"));
			chkServer.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					cboServer.setEnabled(chkServer.getSelection());
					preferences.saveBooleanSetting("chkServer", chkServer.getSelection());
					setPageComplete(pageComplete());
					btnApplyServerConfig.setEnabled(chkServer.getSelection());
					btnApplyCubeChanges.setEnabled(chkServer.getSelection());
				}
			});
			chkServer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			chkServer.setText("Server");
		}
		{
			cboServer = new Combo(container, SWT.READ_ONLY);
			cboServer.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setPageComplete(pageComplete());
				}
			});
			cboServer.setEnabled(chkServer.getSelection());
			{
				GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
				gridData.widthHint = 463;
				cboServer.setLayoutData(gridData);
			}
			cboServer.setItems(projectNameList.toArray(new String[0]));
			String s = preferences.getStringSetting("cboServer");
			if(s != null && s.length() > 0){
				int i = cboServer.indexOf(s);
				if(i >= 0){
					cboServer.select(i);
				}
			}
		}
		new Label(container, SWT.NONE);
		{
			btnApplyServerConfig = new Button(container, SWT.CHECK);
			btnApplyServerConfig.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setApplyConfig(btnApplyServerConfig.getSelection());
				}
			});
			btnApplyServerConfig.setEnabled(chkServer.getSelection());
			btnApplyServerConfig.setSelection(true);
			btnApplyServerConfig.setText("Reload Server's Application Configuration");
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			btnApplyCubeChanges = new Button(container, SWT.CHECK);
			btnApplyCubeChanges.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setApplyCube( btnApplyCubeChanges.getSelection() );
				}
			});
			btnApplyCubeChanges.setEnabled(chkServer.getSelection());
			btnApplyCubeChanges.setText("Start/Restart Application");
		}
		new Label(container, SWT.NONE);
		{
			chkPafArchive = new Button(container, SWT.CHECK);
			chkPafArchive.setSelection(preferences.getBooleanSetting("chkPafArchive"));
			chkPafArchive.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					txtPaf.setEnabled(chkPafArchive.getSelection());
					cmdPaf.setEnabled(chkPafArchive.getSelection());
					preferences.saveBooleanSetting("chkPafArchive", chkPafArchive.getSelection());
					setPageComplete(pageComplete());
				}
			});
			chkPafArchive.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			chkPafArchive.setText("Paf Archive");
		}
		{
			txtPaf = new Text(container, SWT.BORDER | SWT.READ_ONLY);
			txtPaf.setEditable(false);
			txtPaf.setEnabled(chkPafArchive.getSelection());
			txtPaf.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			txtPaf.setText(preferences.getStringSetting("txtPaf"));
		}
		{
			cmdPaf = new Button(container, SWT.NONE);
			cmdPaf.setEnabled(chkPafArchive.getSelection());
			cmdPaf.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					showFileDialog(txtPaf, pafFileExtenstionList);
				}
			});
			cmdPaf.setText("Browse...");
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			chkTemplate = new Button(container, SWT.CHECK);
			chkTemplate.setSelection(preferences.getBooleanSetting("chkTemplate"));
			chkTemplate.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					txtTemplate.setEnabled(chkTemplate.getSelection());
					preferences.saveBooleanSetting("chkTemplate", chkTemplate.getSelection());
					setPageComplete(pageComplete());
				}
			});
			chkTemplate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			chkTemplate.setText("Template");
		}
		{
			txtTemplate = new Text(container, SWT.BORDER);
			txtTemplate.setTextLimit(128);
			txtTemplate.setEnabled(chkTemplate.getSelection());
			txtTemplate.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					//validateTemplate();
					if(templateNameList != null && templateNameList.size() > 0){
						if(templateNameList.contains(txtTemplate.getText().toLowerCase().trim())){
							setPageComplete(false);
						} else{
							setPageComplete(pageComplete());
						}
					} else{
						setPageComplete(pageComplete());
					}
				}
			});
			txtTemplate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			txtTemplate.setText(preferences.getStringSetting("txtTemplate"));
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			chkExcel = new Button(container, SWT.CHECK);
			chkExcel.setSelection(preferences.getBooleanSetting("chkExcel"));
			chkExcel.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					cmdExcel.setEnabled(chkExcel.getSelection());
					txtExcel.setEnabled(chkExcel.getSelection());
					lblCellReferencing.setEnabled(chkExcel.getSelection());
					cboCellReferencing.setEnabled(chkExcel.getSelection());
					lblAddDep.setEnabled(chkExcel.getSelection());
					cboAddDep.setEnabled(chkExcel.getSelection());
					grpExcelFileOptions.setEnabled(chkExcel.getSelection());
					preferences.saveBooleanSetting("chkExcel", chkExcel.getSelection());
					setPageComplete(pageComplete());
				}
			});
			chkExcel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			chkExcel.setText("Excel");
			
		}
		{
			txtExcel = new Text(container, SWT.BORDER | SWT.READ_ONLY);
			txtExcel.setEnabled(chkExcel.getSelection());
			txtExcel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			txtExcel.setText(preferences.getStringSetting("txtExcel"));
		}
		{
			cmdExcel = new Button(container, SWT.NONE);
			cmdExcel.setEnabled(chkExcel.getSelection());
			cmdExcel.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					showFileDialog(txtExcel, excelFileExtenstionList);
				}
			});
			cmdExcel.setText("Browse...");
		}
		{
			grpExcelFileOptions = new Group(container, SWT.NONE);
			grpExcelFileOptions.setEnabled(chkExcel.getSelection());
			grpExcelFileOptions.setText("Excel File Options");
			{
				GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 3, 1);
				gridData.heightHint = 82;
				grpExcelFileOptions.setLayoutData(gridData);
			}
			{
				lblCellReferencing = new Label(grpExcelFileOptions, SWT.NONE);
				lblCellReferencing.setEnabled(chkExcel.getSelection());
				lblCellReferencing.setBounds(10, 29, 86, 15);
				lblCellReferencing.setText("Cell Referencing");
			}
			cboCellReferencing = new Combo(grpExcelFileOptions, SWT.READ_ONLY);
			cboCellReferencing.setEnabled(chkExcel.getSelection());
			cboCellReferencing.setBounds(121, 21, 55, 23);
			cboCellReferencing.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setPageComplete(pageComplete());
				}
			});
			cboCellReferencing.setVisibleItemCount(2);
			cboCellReferencing.setItems(new String[] {"True", "False"});
			String s = preferences.getStringSetting("cboCellReferencing");
			if(s != null && s.length() > 0){
				int i = cboCellReferencing.indexOf(s);
				if(i >= 0){
					cboCellReferencing.select(i);
				}
			} else {
				cboCellReferencing.select(0);
			}
			{
				lblAddDep = new Label(grpExcelFileOptions, SWT.NONE);
				lblAddDep.setEnabled(chkExcel.getSelection());
				lblAddDep.setBounds(10, 68, 108, 15);
				lblAddDep.setText("Add Dependencies");
			}
			{
				cboAddDep = new Combo(grpExcelFileOptions, SWT.READ_ONLY);
				cboAddDep.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						setPageComplete(pageComplete());
					}
				});
				cboAddDep.setEnabled(chkExcel.getSelection());
				cboAddDep.setBounds(121, 60, 55, 23);
				cboAddDep.setVisibleItemCount(2);
				cboAddDep.setItems(new String[] {"True", "False"});
				
				{
					btnShowExportErrors = new Button(container, SWT.NONE);
					btnShowExportErrors.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(SelectionEvent e) {
							ProjectErrorsDialog errorDialog = ProjectWizardUtil.getProjectExportErrorsWizardPage(
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
									getProjectDataErrors());
							
							errorDialog.open();
							getShell().close();
							//(pageComplete());
						}
					});
					btnShowExportErrors.setEnabled(false);
					btnShowExportErrors.setText("Show errors");
				}
				new Label(container, SWT.NONE);
				new Label(container, SWT.NONE);
				String s2 = preferences.getStringSetting("cboAddDep");
				if(s2 != null && s2.length() > 0){
					int i = cboAddDep.indexOf(s2);
					if(i >= 0){
						cboAddDep.select(i);
					}
				} else {
					cboAddDep.select(0);
			
				}
			}
		}
		setPageComplete(pageComplete());
	}
	
	private boolean pageComplete(){
		boolean ret = false;
		
		
		//user has selected notin...
		if(chkServer == null || chkPafArchive == null || chkTemplate == null || chkExcel == null){
			
			return false;
			
		}
		
		//user has selected notin...
		if(!chkServer.getSelection() && !chkPafArchive.getSelection() && !chkTemplate.getSelection() && !chkExcel.getSelection()){
			
			selectedItemsToExport.clear();
			return false;
			
		}
		
		if(chkServer.getSelection()){
			
			if(cboServer != null && cboServer.getSelectionIndex() >= 0){
				
				setServerName(cboServer.getText().trim());
				preferences.saveStringSetting("cboServer", cboServer.getText().trim());
				selectedItemsToExport.add(ProjectSourceType.Server);
				ret = true;
				
			} else{
				
				return false;
				
			}
		} else{
			
			selectedItemsToExport.remove(ProjectSourceType.Server);
			
		}
		
		if(chkPafArchive.getSelection()){
					
			if(txtPaf.getText() != null && txtPaf.getText().trim().length() > 0){
				
				setPafPathFileName(txtPaf.getText().trim());
				preferences.saveStringSetting("txtPaf", txtPaf.getText().trim());
				selectedItemsToExport.add(ProjectSourceType.PaceArchiveFile);
				ret = true;
				
			} else{
				
				return false;
				
			}
			
		}else{
			
			selectedItemsToExport.remove(ProjectSourceType.PaceArchiveFile);
			
		}
		
		if(chkTemplate.getSelection()){
			
			if(txtTemplate.getText() != null && txtTemplate.getText().trim().length() > 0){
				
				setTemplateName(txtTemplate.getText().trim());
				preferences.saveStringSetting("txtTemplate", txtTemplate.getText().trim());
				selectedItemsToExport.add(ProjectSourceType.Template);
				ret = true;
				
			} else{
				
				return false;
				
			}
			
		}else{
			
			selectedItemsToExport.remove(ProjectSourceType.Template);
			
		}
		
		if(chkExcel.getSelection()){
			
			if(txtExcel.getText() != null && txtExcel.getText().trim().length() > 0 && 
					cboCellReferencing.getSelectionIndex() >= 0 && 
					cboAddDep.getSelectionIndex() >= 0){
				
				setExcelPathFileName(txtExcel.getText().trim());
				setExcelCellReferences(new Boolean(cboCellReferencing.getText()));
				setAddDependencies(new Boolean(cboAddDep.getText()));
				preferences.saveStringSetting("txtExcel", txtExcel.getText().trim());
				preferences.saveStringSetting("cboCellReferencing", cboCellReferencing.getText());
				preferences.saveStringSetting("cboAddDep", cboAddDep.getText());
				selectedItemsToExport.add(ProjectSourceType.Excel);
				ret = true;
				
			} else{
				
				return false;
				
			}
			
		}else{
			
			selectedItemsToExport.remove(ProjectSourceType.Excel);
			
		}
		
		
		return ret;
	}
	
	
	private void showFileDialog(Text textBox, List<String> fileExtenstionList){
		// Create a new file dialog
		FileDialog fd =  new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		
		// Set the title
		fd.setText(fileDialogTitle);
		
		// Set the default directory
		String filterPath = preferences.getStringSetting("lastFilterPath");
		
		if(filterPath != null){
			
			fd.setFilterPath(filterPath);
		}else{
			
			fd.setFilterPath("C:/");
			
		}
		
		// Set the filtered file extension
		if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {					
	        
			String[] filterExt = fileExtenstionList.toArray(new String[0]);
	        
			for (int i = 0; i < filterExt.length; i++ ) {
				
				filterExt[i] = "*." + filterExt[i];
				
			}
			
	        fd.setFilterExtensions(filterExt);
	        
		}
        
        // Get the selected path string
        String selected = fd.open();
        
        if ( selected != null ) {
        

        	preferences.saveStringSetting("lastFilterPath", fd.getFilterPath());
        	
    		if ( fileExtenstionList != null && fileExtenstionList.size() > 0 ) {
    		
    			boolean validFileExtension = false;
    			
    			if ( fd.getFilterIndex() >= 0 ) {
    				
    				validFileExtension = true;
    				
    				String selectedExtension = fileExtenstionList.get(fd.getFilterIndex());
    				
    				if ( ! selected.endsWith("." + selectedExtension)) {
    					
    					textBox.setText(selected + "." + selectedExtension );
    					
    				} else {
    					
    					textBox.setText(selected);
    					
    				}			    				
						    				
    			} else {
    				
    				for (String fileExtension : fileExtenstionList ) {
	    				
	    				if ( selected.endsWith("." + fileExtension)) {
	    					
	    					validFileExtension = true;
	    					
	    					textBox.setText(selected);
	    					
	    					break;
	    				}
    				}
    				
    			}			    	
    			
    			/*for (String fileExtension : fileExtenstionList ) {
    				
    				if ( selected.endsWith("." + fileExtension)) {
    					
    					validFileExtension = true;
    					
    					textBox.setText(selected);
    					
    					break;
    					
    				} else {
    					
    					validFileExtension = true;
    					
    					if ( fd.getFilterIndex() > 0 ) {
    						
    						textBox.setText(selected + "." + fileExtenstionList.get(fd.getFilterIndex()));
    						
    					} else {
    						    						
    						textBox.setText(selected + "." + fileExtension);
    						
    					}   					
    					
    					break;
    					
    				}
    				
    			}*/
    			
    			if ( validFileExtension ) {

    				setPageComplete(pageComplete());
    				
    			} else {
    				
    				setPageComplete(false);
    				
    			}
    			
    		} else {
    			//i don't think this should ever happen, so I set it 
    			// to false.  KRM-1/21/2010
    			setPageComplete(false);
    			
    		}
        		
        			        	
        }
       
	}

	
	 /**
     * Returns the value of the project name field
     * with leading and trailing spaces removed.
     * 
     * @return the project name in the field
     */
    private String getTemplateNameFieldValue() {
        if (txtTemplate == null) {
			return ""; //$NON-NLS-1$
		}

        return txtTemplate.getText().trim();
    }

	
	/**
	 * Creates a project resource handle for the current project name field
	 * value. The project handle is created relative to the workspace root.
	 * <p>
	 * This method does not create the project resource; this is the
	 * responsibility of <code>IProject::create</code> invoked by the new
	 * project resource wizard.
	 * </p>
	 * 
	 * @return the new project resource handle
	 */
    public File getFileHandle(String txtTemplateName) {
    	
    	String newTemplatePath = ProjectWizardUtil.getTemplatePath() + 
    	txtTemplateName + "." +  Constants.PAF_EXT;
    	
    	//get file to deploy
    	File file = new File(newTemplatePath);
    	
        return file;
    }
	
	/**
     * Returns whether this page's controls currently all contain valid 
     * values.
     *
     * @return <code>true</code> if all controls are valid, and
     *   <code>false</code> if at least one is invalid
     */
    protected boolean validateTemplate() {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();

        String projectFieldContents = getTemplateNameFieldValue();
        if (chkTemplate.getSelection() && projectFieldContents.equals("")) { //$NON-NLS-1$
            setErrorMessage("Template name is blank.");
            return false;
        }

        IStatus nameStatus = workspace.validateName(projectFieldContents,
                IResource.PROJECT);
        if (!nameStatus.isOK()) {
            setErrorMessage(nameStatus.getMessage());
            return false;
        }

        File handle = getFileHandle(projectFieldContents);
        if (handle.exists()) {
            setErrorMessage("Template name already exists.");
            return false;
        }
                

        setErrorMessage(null);
        setMessage(null);
        return true;
    }
	

	/**
	 * @return the projectName
	 */
	public String getServerName() {
		return serverName;
	}


	/**
	 * @param projectName the projectName to set
	 */
	public void setServerName(String projectName) {
		this.serverName = projectName;
	}


	/**
	 * @return the excelPathFileName
	 */
	public String getExcelPathFileName() {
		return excelPathFileName;
	}


	/**
	 * @param excelPathFileName the excelPathFileName to set
	 */
	public void setExcelPathFileName(String excelPathFileName) {
		this.excelPathFileName = excelPathFileName;
	}


	/**
	 * @return the excelCellReferences
	 */
	public boolean isExcelCellReferences() {
		return excelCellReferences;
	}


	/**
	 * @param excelCellReferences the excelCellReferences to set
	 */
	public void setExcelCellReferences(boolean excelCellReferences) {
		this.excelCellReferences = excelCellReferences;
	}


	/**
	 * @return the pafPathFileName
	 */
	public String getPafPathFileName() {
		return pafPathFileName;
	}


	/**
	 * @param pafPathFileName the pafPathFileName to set
	 */
	public void setPafPathFileName(String pafPathFileName) {
		
		String s = pafPathFileName;
		
		for (String fileExtension : pafFileExtenstionList ) {
			
			if(pafPathFileName.endsWith("." + fileExtension)){
				
				this.pafPathFileName = s.replace("." + fileExtension, "");
				
				break;
			}
			
		}
		
		this.pafPathFileName = s;
	}

	/**
	 * @return the projectDataErrors
	 */
	public List<ProjectDataError> getProjectDataErrors() {
		return projectDataErrors;
	}

	/**
	 * @param projectDataErrors the projectDataErrors to set
	 */
	public void setProjectDataErrors(List<ProjectDataError> projectDataErrors) {
		this.projectDataErrors = projectDataErrors;
		if(this.projectDataErrors != null && this.projectDataErrors.size() > 0){
			btnShowExportErrors.setEnabled(true);
		} else{
			btnShowExportErrors.setEnabled(false);
		}
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}


	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public Set<ProjectSourceType> getSelectedItemsToExport() {
		return selectedItemsToExport;
	}


	public void setSelectedItemsToExport(
			Set<ProjectSourceType> selectedItemsToExport) {
		this.selectedItemsToExport = selectedItemsToExport;
	}


	public boolean isAddDependencies() {
		return addDependencies;
	}


	public void setAddDependencies(boolean addDependencies) {
		this.addDependencies = addDependencies;
	}
	public boolean isApplyConfig() {
		return applyConfig;
	}


	public void setApplyConfig(boolean applyConfig) {
		this.applyConfig = applyConfig;
	}


	public boolean isApplyCube() {
		return applyCube;
	}


	public void setApplyCube(boolean applyCube) {
		this.applyCube = applyCube;
	}
}

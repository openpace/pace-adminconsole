/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.actions.importexport.ImportProjectFromFileset;
import com.pace.admin.menu.actions.importexport.ImportProjectFromServer;
import com.pace.admin.menu.actions.projects.RefreshProjectsAction;
import com.pace.admin.menu.project.ProjectSource;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.mdb.PafConnectionProps;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PropertyLoader;
import com.pace.base.app.MdbDef;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.DimLevelGenInfo;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafMdbPropsRequest;
import com.pace.server.client.PafMdbPropsResponse;
import com.pace.server.client.PafRequest;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

import java.util.Properties;

/**
 * @author kbafna
 *
 */
public class ProjectNewWizard extends Wizard implements INewWizard {

	private static Logger logger = Logger.getLogger(ProjectNewWizard.class);

	public static final String ID = "com.pace.admin.menu.wizards.ProjectNewWizard";

	private String newProjectName;

	private List<ProjectSource> projectSourceList = new ArrayList<ProjectSource>();

	private ProjectSourceOptionsWizardPage projectSourceOptionsWizardPage;

	private FileSelectionWizardPage excelFileSelectionWizardPage;

	private FileSelectionWizardPage pafFileSelectionWizardPage;

	private GenericSelectionOptionsWizardPage serverSelectionWizardPage;

	private GenericSelectionOptionsWizardPage templateSelectionWizardPage;

	private GenericSelectionOptionsWizardPage dataSourceSelectionWizardPage;

	private DimensionNameSelectionWizardPage dimensionSelectionWizardPage;

	private ProjectNameSelectionWizardPage projectNameSelectionWizardPage;

	private DimensionTreeSelectorWizardPage dimensionMeasureWizardPage;
	
	private DimensionTreeSelectorWizardPage CurrentYearWizardPage;
	
	private DimensionTreeSelectorWizardPage lastPeriodWizardPage;
	
	private NewProjectValidationWizardPage validationWizardPage;

	private DimLevelGenInfo[] levelGenInfo;

	private ProjectSource lastSelectedProjectSource;

	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrorMap;

	private IWorkbench workbench = null;

	private Map<ProjectSourceType, IWizardPage> wizardPageMap = new HashMap<ProjectSourceType, IWizardPage>();

	private Map<String, IProject> currentProjects = null;

	String accountDim = "";
	String timeDim ="";

	// added to hold the first version used for Plan cycles for the Essbase New Project
	
	private String firstVersion = "";
	
	private List<String> versionList ;
	
	// to hold the first plan type level 0 member
	
	private String firstPlanTypes = "";
	
	private Set<PafServer> pafServers;

	private Map<String, File> templateFiles = null;

	private ProjectElementId[] elements;

	public boolean isShowDims = false;

	public PafApplicationDef pafAppDefEssbase = null;
	

	public ProjectNewWizard() {

	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("New Project Wizard");

		this.workbench = workbench;

		// get the current projects.
		currentProjects = ViewerUtil.getCurrentProjects();

		ProjectSource projectSourceNone = new ProjectSource("Empty",
				"Creates an empty Pace Project", ProjectSourceType.None);

		// build the default elements.
		java.util.List<ProjectElementId> projectDataIdList = new ArrayList<ProjectElementId>();
		for (ProjectElementId elementId : ProjectElementId.values()) {

			if (!elementId.toString().equalsIgnoreCase(
					ProjectElementId.RuleSet_Rule.toString())
					&& !elementId.toString().equalsIgnoreCase(
							ProjectElementId.RuleSet_RuleGroup.toString())
					&& !elementId.toString().equalsIgnoreCase(
							ProjectElementId.RuleSet_RuleSet.toString())) {

				projectDataIdList.add(elementId);
			}
		}

		Collections.sort(projectDataIdList);
		elements = projectDataIdList.toArray(new ProjectElementId[0]);

		
		
		// get the templates
		templateFiles = new HashMap<String, File>();
		List<File> templates = ProjectWizardUtil.getTemplates();
		// list to hold the name of templates.
		List<String> templateNameList = new ArrayList<String>();

		if (templates != null && templates.size() > 0) {
			// for each file in the list of templates.
			for (File f : templates) {

				// get the file name.
				String s = f.getName();
				// create the name
				String name = s.replace("." + Constants.PAF_EXT, "");
				// strip off the file ext.
				templateNameList.add(name);
				// add to the map
				templateFiles.put(name, f);

			}

		}

		// Project source list: different types of projects you can create
		// projectSourceList.add(projectSourceNone);
		projectSourceList.add(new ProjectSource("Essbase Outline",
				"Creates a project from an Essbase Outline.",
				ProjectSourceType.EssbaseOutline));
		projectSourceList.add(new ProjectSource(ProjectSourceType.Excel
				.toString(), "Creates a project from an Excel Workbook.",
				ProjectSourceType.Excel));

		projectSourceList.add(new ProjectSource("Pace Archive File",
				"Creates a project from a Pace archive file.",
				ProjectSourceType.PaceArchiveFile));
		projectSourceList.add(new ProjectSource(ProjectSourceType.Server
				.toString(), "Creates a project from a server's config.",
				ProjectSourceType.Server));
		// only add the template option if the user has at least one temple
		// installed.
		if (templateNameList.size() > 0) {
			projectSourceList.add(new ProjectSource(ProjectSourceType.Template
					.toString(), "Creates a project from a Pace template.",
					ProjectSourceType.Template));
		}

		// Initializing pages
		projectSourceOptionsWizardPage = ProjectWizardUtil
				.getNewProjectOptionsWizardPage(projectSourceList, null);

		excelFileSelectionWizardPage = ProjectWizardUtil
				.getExcelFileSelectionWizardPage();

		pafFileSelectionWizardPage = ProjectWizardUtil
				.getPaceArchiveFileSelectionWizardPage();

		
		// get the current list of servers.
		List<String> serverNameList = new ArrayList<String>();
		
		// Get the list of all running Pace Servers. Used for Essbase Wizard and for TTN 2228.

		pafServers = PafServerUtil.getRunningPafServers();

		if (pafServers != null && pafServers.size() > 0) {

			for (PafServer server : pafServers) {
				serverNameList.add(server.getName());

			}

		}

		ArrayList<String> inList = new ArrayList<String>();
		inList.add("");

		if (serverNameList.size() > 0) {
			String[] serverSelect = new String[] { serverNameList.get(0) };
			serverSelectionWizardPage = ProjectWizardUtil
					.getServerSelectionWizardPage(
							serverNameList.toArray(new String[0]), serverSelect);

		} else {
			serverSelectionWizardPage = ProjectWizardUtil
					.getServerSelectionWizardPage(
							serverNameList.toArray(new String[0]),
							new String[0]);
		}

		// Initializing pages
		templateSelectionWizardPage = ProjectWizardUtil
				.getTemplateSelectionWizardPage(
						templateNameList.toArray(new String[0]), new String[0]);

		projectNameSelectionWizardPage = ProjectWizardUtil
				.getNewProjectNameSelectionWizardPage(currentProjects.keySet()
						.toArray(new String[0]));

		wizardPageMap.put(ProjectSourceType.None,
				projectNameSelectionWizardPage);
		wizardPageMap
				.put(ProjectSourceType.Excel, excelFileSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.PaceArchiveFile,
				pafFileSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.Server, serverSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.Template,
				templateSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.EssbaseOutline,
				serverSelectionWizardPage);

	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {

		if (page.equals(projectSourceOptionsWizardPage)) {

			List<ProjectSource> selectedOptionList = projectSourceOptionsWizardPage
					.getSelectedSourceOptions();

			if (selectedOptionList.size() == 1) {

				ProjectSource selectedSource = selectedOptionList.get(0);

				// //if user changes selection type, clear page complete on last
				// page.
				// if ( lastSelectedProjectSource != null &&
				// lastSelectedProjectSource.equals(selectedSource)) {
				//
				// projectNameSelectionWizardPage.setPageComplete(false);
				//
				// }

				lastSelectedProjectSource = selectedSource;

				switch (selectedSource.getType()) {

				case Excel:
					pafFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					return excelFileSelectionWizardPage;
				case PaceArchiveFile:
					excelFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					return pafFileSelectionWizardPage;
				case Server:
					pafFileSelectionWizardPage.setPageComplete(true);
					excelFileSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					pafServers = PafServerUtil.getRunningPafServers();
					// Fix for TTN 2228 . If No servers running, show message.
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else {
						return serverSelectionWizardPage;
					}
				case Template:
					pafFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					excelFileSelectionWizardPage.setPageComplete(true);
					return templateSelectionWizardPage;
				case None:
					pafFileSelectionWizardPage.setPageComplete(false);
					serverSelectionWizardPage.setPageComplete(false);
					excelFileSelectionWizardPage.setPageComplete(false);
					templateSelectionWizardPage.setPageComplete(false);
					return projectNameSelectionWizardPage;
				case EssbaseOutline:
					pafFileSelectionWizardPage.setPageComplete(true);
					excelFileSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					projectNameSelectionWizardPage.setPageComplete(false);
					pafAppDefEssbase = new PafApplicationDef();
					MdbDef mdbDef = new MdbDef();
					pafAppDefEssbase.setMdbDef(mdbDef);
					// Fix for TTN 2228 . If No servers running, show message.
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else
						return serverSelectionWizardPage;
				}

			}

			// if not project selection wizard but is in map
		} else if (!page.equals(projectNameSelectionWizardPage)) {

			// if ( projectNameSelectionWizardPage.getProjectName() != null ) {
			//
			// projectNameSelectionWizardPage.setPageComplete(true);
			//
			// }

			List<ProjectSource> selectedOptionList = projectSourceOptionsWizardPage
					.getSelectedSourceOptions();
			if (selectedOptionList.size() == 1) {
				ProjectSource selectedSource = selectedOptionList.get(0);
				// TTN-2628 Check if the array is not empty .
				if(serverSelectionWizardPage.getSelectedItems()!=null && serverSelectionWizardPage.getSelectedItems().length>0)
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& serverSelectionWizardPage.getSelectedItems()[0] != null
						&& page.getPreviousPage().equals(
								projectSourceOptionsWizardPage)) {
					
                   
					List<String> dsNameList = new ArrayList<String>();
					dsNameList = getDataSourceIds(serverSelectionWizardPage
							.getSelectedItems()[0]);
					 
						dataSourceSelectionWizardPage = ProjectWizardUtil
								.getDataSourceSelectionWizardPage(
										dsNameList.toArray(new String[0]),
										new String[0]);
					addPage(dataSourceSelectionWizardPage);
					
					wizardPageMap.put(ProjectSourceType.EssbaseOutline,
							dataSourceSelectionWizardPage);
					
					
					isShowDims = true;
					pafServers = PafServerUtil.getRunningPafServers();
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else {
						

					return dataSourceSelectionWizardPage;}

				} // if ends
				
				// EssbaseOutline page sequence 
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& page.equals(dataSourceSelectionWizardPage)) {
					
					if ( dataSourceSelectionWizardPage.getSelectedItems() != null) {
						
						if(dataSourceSelectionWizardPage.getSelectedItems().length > 0 && dataSourceSelectionWizardPage.getSelectedItems()[0] != null){
						
						// Set the datasource id on the app def based on what was selected on the previous page.
						pafAppDefEssbase.getMdbDef().setDataSourceId(
								dataSourceSelectionWizardPage
										.getSelectedItems()[0]);
						
						// Get the list of dimensions from a service call for the selected server and datasource.
						List<String> dimensions = getDimensions(
								serverSelectionWizardPage.getSelectedItems()[0],
								dataSourceSelectionWizardPage
										.getSelectedItems()[0]);
						
						// TTN-2607 Do not return next page is application status is not running.
						try {
							String serverName = serverSelectionWizardPage.getSelectedItems()[0];
							ApplicationState appState = ServerMonitor.getInstance().getApplicationState(PafServerUtil.getServer(serverName));
							String appStatus = appState.getCurrentRunState().toString();
							if(appStatus!="RUNNING"){
								String appNotRunningMsg = "The Pace Server application [%s] is not running. Please go [BACK] to the previous"
										+ " screen and select a Pace Server with a running application. Otherwise, [CANCEL] this wizard, restart"
										+ " the [%s] application and then re-run this wizard.";
								String errMsg = String.format(appNotRunningMsg, appState.getApplicationId(), appState.getApplicationId());
								GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, errMsg, MessageDialog.ERROR);
								return null;
							}
							
						} catch (PafServerNotFound e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						// create the page
						dimensionSelectionWizardPage = new DimensionNameSelectionWizardPage(
								"Dimensions", "Pace Dimension Mapping", "Map Essbase Dimensions to their corresponding Pace Dimensions.",
								true, dimensions.toArray(new String[0]),
								accountDim, timeDim);

						dimensionSelectionWizardPage
								.setInfoArray(getMeasureRootArray());

						addPage(dimensionSelectionWizardPage);
						
						// check if any Pace servers are running  TTN 2372
						pafServers = PafServerUtil.getRunningPafServers();
						if (pafServers == null || pafServers.size() <= 0) {
							GUIUtil.openMessageWindow(
									Constants.DIALOG_WARNING_HEADING,
									"No Pace Servers running !",
									MessageDialog.INFORMATION);
							return null;
						} else {
							

						return dimensionSelectionWizardPage; }
						}
					}
				} // if ends
				//If project created from Essbase Outline and can select the measure root.
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& page.equals(dimensionSelectionWizardPage)) {

					// Add the dimensions selected on the previous page to the App Def.
					pafAppDefEssbase.getMdbDef().setMeasureDim(accountDim);
					pafAppDefEssbase.getMdbDef().setTimeDim(timeDim);
					pafAppDefEssbase.getMdbDef().setYearDim(dimensionSelectionWizardPage.getDimensionSelections()[2]);
					pafAppDefEssbase.getMdbDef().setPlanTypeDim(dimensionSelectionWizardPage.getDimensionSelections()[3]);
					pafAppDefEssbase.getMdbDef().setVersionDim(dimensionSelectionWizardPage.getDimensionSelections()[4]);
					
					// Set the hierarchy dimensions on the App Def
					pafAppDefEssbase.getMdbDef().setHierDims(dimensionSelectionWizardPage.getItems());
					
					// create the page
					dimensionMeasureWizardPage = new DimensionTreeSelectorWizardPage(
							"Measure Root", 
							"Measure Root Selection","Select Measure Root", true, accountDim,
							serverSelectionWizardPage.getSelectedItems()[0],
							dataSourceSelectionWizardPage.getSelectedItems()[0]);
					addPage(dimensionMeasureWizardPage);
			
					
					// check if any Pace servers are running  TTN 2372
					pafServers = PafServerUtil.getRunningPafServers();
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else {
						
					return dimensionMeasureWizardPage;
					}
				}
				
				// If project created from Essbase Outline and can select the Elapsed period.
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& page.equals(dimensionMeasureWizardPage)) {

					lastPeriodWizardPage = new DimensionTreeSelectorWizardPage(
							"Elapsed Period",
							"Elapsed Period Selection", "Set Last Elapsed Period", true, timeDim,
							serverSelectionWizardPage.getSelectedItems()[0],
							dataSourceSelectionWizardPage.getSelectedItems()[0]);
					
					// set the measure root selected on the previous page
					pafAppDefEssbase.getMdbDef().setMeasureRoot(dimensionMeasureWizardPage.getSelectedMember());
					
					addPage(lastPeriodWizardPage);
					
					// check if any Pace servers are running  TTN 2372
					pafServers = PafServerUtil.getRunningPafServers();
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else {
						
					return lastPeriodWizardPage;
					}

				}
				
				// If project created from Essbase Outline and can select the Current Year.
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& page.equals(lastPeriodWizardPage)) {

					CurrentYearWizardPage = new DimensionTreeSelectorWizardPage(
							"Current Year", 
							"Current Year Selection","Select Current Year", true, dimensionSelectionWizardPage.getYearDim(),
							serverSelectionWizardPage.getSelectedItems()[0],
							dataSourceSelectionWizardPage.getSelectedItems()[0]);
					
					// get all the versions
					// TTN-2606 performance improvement
					//versionList = getAllVersions(serverSelectionWizardPage.getSelectedItems()[0],dimensionSelectionWizardPage.getDimensionSelections()[4], dataSourceSelectionWizardPage.getSelectedItems()[0]);
					
					// get the first plan type
					// TTN-2606 performance improvement
					//firstPlanTypes = getFirstPlanType(serverSelectionWizardPage.getSelectedItems()[0],dimensionSelectionWizardPage.getDimensionSelections()[3], dataSourceSelectionWizardPage.getSelectedItems()[0]);
					addPage(CurrentYearWizardPage);
					
					// check if any Pace servers are running  TTN 2372
					pafServers = PafServerUtil.getRunningPafServers();
					if (pafServers == null || pafServers.size() <= 0) {
						GUIUtil.openMessageWindow(
								Constants.DIALOG_WARNING_HEADING,
								"No Pace Servers running !",
								MessageDialog.INFORMATION);
						return null;
					} else {
						
					return CurrentYearWizardPage;
					}

				}

				// If project created from Essbase Outline then show validation screen with project selections.
				if (selectedSource.getType().toString() == "EssbaseOutline"
						&& page.equals(CurrentYearWizardPage)) {

					
					validationWizardPage = new NewProjectValidationWizardPage(
							"Project Validation", "Confirm Project Selections",
							"Confirm Project Selections", true, lastPeriodWizardPage.getSelectedMember(),
							CurrentYearWizardPage.getSelectedMember(), dimensionMeasureWizardPage.getSelectedMember(),dimensionSelectionWizardPage.getDimensionSelections(),serverSelectionWizardPage.getSelectedItems()[0],
							dataSourceSelectionWizardPage.getSelectedItems()[0]);
					
					addPage(validationWizardPage);
					return validationWizardPage;

				}
			} // if ends

			
			return projectNameSelectionWizardPage;

		} // else if ends

		// return super.getNextPage(page);
		return null;
	}

	@Override
	public boolean canFinish() {

		if (projectNameSelectionWizardPage != null
				&& projectNameSelectionWizardPage.isPageComplete()) {

			if (lastSelectedProjectSource != null
					&& lastSelectedProjectSource.getType() == ProjectSourceType.None) {

				
				return true;

			}

			// return projectNameSelectionWizardPage.isPageComplete();
		}

		return super.canFinish();
	}

	@Override
	public void addPages() {

		addPage(projectSourceOptionsWizardPage);
		addPage(excelFileSelectionWizardPage);
		addPage(pafFileSelectionWizardPage);
		addPage(serverSelectionWizardPage);
		addPage(templateSelectionWizardPage);
		addPage(projectNameSelectionWizardPage);

	}

	@Override
	public boolean performFinish() {

		// data filter of items to import.
		// ProjectElementId[] elements =
		// projectImportDataSelectionWizardPage.getSelectedProjectItems();
		// create a new project?
		boolean createNewProject = projectNameSelectionWizardPage
				.createNewProject();
		// get the project name.
		String projectName = projectNameSelectionWizardPage.getProjectName();
		// get the excel path file name.
		String excelPathFileName = excelFileSelectionWizardPage
				.getSelectedFileName();
		// get the paf path file name.
		String pafPathFileName = pafFileSelectionWizardPage
				.getSelectedFileName();
		// get/create the project
		IProject project = null;
		try {
			project = ViewerUtil.getProject(createNewProject, projectName);
		} catch (CoreException e) {
			// TTN 1724 - AC not handling new project / import project with same
			// project name but different case
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
					e.getMessage(), MessageDialog.WARNING);
			logger.error("Cannot create project: " + projectName);
			return false;
		}
		// get the selected templates.
		File templatePathFileName = null;
		if (templateSelectionWizardPage.getSelectedItems() != null
				&& templateSelectionWizardPage.getSelectedItems().length > 0) {
			
			templatePathFileName = templateFiles
					.get(templateSelectionWizardPage.getSelectedItems()[0]);
		}

		
		File essbaseOutlineTemplatePathFileName = templateFiles
				.get("Essbase Outline Template");

		boolean returnValue = true;

		switch (lastSelectedProjectSource.getType()) {

		case None:
			RefreshProjectsAction refreshAction = new RefreshProjectsAction(
					workbench.getActiveWorkbenchWindow());
			refreshAction.run();
			break;
		case Excel:
			returnValue = importFromFileset(excelPathFileName, elements,
					project, lastSelectedProjectSource.getType());

			if (!returnValue) {
				ViewerUtil.deleteProject(project);
			}

			projectNameSelectionWizardPage
					.setProjectDataErrors(getProjectDataErrors());
			break;
		case PaceArchiveFile:
			returnValue = importFromFileset(pafPathFileName, elements, project,
					lastSelectedProjectSource.getType());

			if (!returnValue) {
				ViewerUtil.deleteProject(project);
			}

			projectNameSelectionWizardPage
					.setProjectDataErrors(getProjectDataErrors());

			break;
		case Server:
			returnValue = importFromServer(elements, project);

			if (!returnValue) {
				ViewerUtil.deleteProject(project);
			}

			projectNameSelectionWizardPage
					.setProjectDataErrors(getProjectDataErrors());

			break;
		case Template:
			returnValue = importFromFileset(
					templatePathFileName.getAbsolutePath(), elements, project,
					lastSelectedProjectSource.getType());

			if (!returnValue) {
				ViewerUtil.deleteProject(project);
			}

			projectNameSelectionWizardPage
					.setProjectDataErrors(getProjectDataErrors());

			break;
		case EssbaseOutline:
			returnValue = importFromFileset(
					essbaseOutlineTemplatePathFileName.getAbsolutePath(),
					elements, project, lastSelectedProjectSource.getType());

			if (!returnValue) {
				ViewerUtil.deleteProject(project);
			}

			projectNameSelectionWizardPage
					.setProjectDataErrors(getProjectDataErrors());

			break;

		}

		// TTN-1449 BEGIN
		// if project exists
		if (project != null && project.exists()) {

			PafApplicationDef[] pafApps = PafApplicationUtil
					.getPafApps(project);

			if (pafApps != null && pafApps.length > 0) {

				PafApplicationDef pafAppDef = pafApps[0];

				pafAppDef.setAppId(project.getName());

				PafApplicationUtil.setPafApps(project, pafAppDef);

			}

		}
		// TTN-1449 END

		return returnValue;

	}

	private boolean importFromFileset(String pathFileName,
			ProjectElementId[] elements, IProject project,
			ProjectSourceType projectType) {

		projectDataErrorMap = null;

		final ImportProjectFromFileset importProjectFromFileset = new ImportProjectFromFileset(
				workbench.getActiveWorkbenchWindow(), pathFileName, project,
				elements, false, projectType, pafAppDefEssbase);
		
		
		// added a special case if project created from Essbase Outline . Pass the AppDef and the Measure root descendants.
		if(projectType.toString().equals("EssbaseOutline"))
		{
			
			importProjectFromFileset.setSimpleDimTree(dimensionMeasureWizardPage.getPafSimpleTree());
			
			importProjectFromFileset.setSimpleBaseTreeMembers(dimensionMeasureWizardPage.getMemberRootDescendants());
			
			importProjectFromFileset.setLastPeriod(lastPeriodWizardPage.getSelectedMember());
			
			importProjectFromFileset.setCurrentYear(CurrentYearWizardPage.getSelectedMember());
			
			// TTN-2606 performance improvement
			versionList = getAllVersions(serverSelectionWizardPage.getSelectedItems()[0],dimensionSelectionWizardPage.getDimensionSelections()[4], dataSourceSelectionWizardPage.getSelectedItems()[0]);
			
			
			importProjectFromFileset.setAllVersions(versionList);
			
			importProjectFromFileset.setFirstVersion(firstVersion);
			
			importProjectFromFileset.setFirstPlanType(firstPlanTypes);
			
			
		}

		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
				new Runnable() {

					public void run() {

						importProjectFromFileset.run();

					}

				});

		if (importProjectFromFileset.isError()) {
			projectDataErrorMap = importProjectFromFileset
					.getProjectDataErrors();
			return false;
		}
		return true;

	}

	private boolean importFromServer(ProjectElementId[] elements,
			IProject project) {

		projectDataErrorMap = null;

		// get the user selected PafServer from the Willard.
		String paceServer = null;
		if (serverSelectionWizardPage.getSelectedItems() != null
				&& serverSelectionWizardPage.getSelectedItems().length > 0) {

			paceServer = serverSelectionWizardPage.getSelectedItems()[0];
		}

		PafServer pafServer = null;

		try {
			// Get the PafServer object from the PafServerUtil
			pafServer = PafServerUtil.getServer(paceServer);

		} catch (PafServerNotFound e) {

			logger.error("Server '" + paceServer + "' was not found.");
		}

		final ImportProjectFromServer importProjectFromServer = new ImportProjectFromServer(
				workbench.getActiveWorkbenchWindow(), elements, project,
				pafServer);

		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
				new Runnable() {

					public void run() {

						importProjectFromServer.run();

					}

				});

		if (importProjectFromServer.isError()) {
			// projectDataErrorMap =
			// importProjectFromServer.getProjectDataErrors();
			return false;
		}
		return true;

	}

	private List<ProjectDataError> getProjectDataErrors() {

		List<ProjectDataError> errors = new ArrayList<ProjectDataError>();

		if (projectDataErrorMap != null && projectDataErrorMap.keySet() != null) {

			for (ProjectElementId element : projectDataErrorMap.keySet()) {

				List<ProjectDataError> error = projectDataErrorMap.get(element);
				errors.addAll(error);

			}

		}

		return errors;

	}

	private void createNewProject() {

		/*
		 * //get new project name String newProjectName =
		 * newProjectPage.getNewProjectName();
		 * 
		 * //if user wants to create project from sample project boolean
		 * createFromSampleProject = newProjectPage
		 * .isCreateFromSampleProject();
		 * 
		 * //if new project name is not null if (newProjectName != null) {
		 * 
		 * IProject newProject = ResourcesPlugin.getWorkspace().getRoot()
		 * .getProject(newProjectName);
		 * 
		 * try {
		 * 
		 * //create new project newProject.create(null);
		 * 
		 * //open new project newProject.open(null);
		 * 
		 * //create conf folder IFolder confFolder =
		 * newProject.getFolder(Constants.CONF_DIR); confFolder.create(true,
		 * true, null);
		 * 
		 * //if create from samples, copy the samples project dir, else create
		 * basic project if (createFromSampleProject) {
		 * 
		 * Files.copy(new File(Constants.SAMPLE_PROJECT_DIR), new
		 * File(newProject.getLocation().toString()));
		 * 
		 * //refresh local filesystem
		 * newProject.refreshLocal(IResource.DEPTH_INFINITE, null);
		 * 
		 * } else {
		 * 
		 * //create lib folder IFolder libFolder =
		 * newProject.getFolder(Constants.LIB_DIR); libFolder.create(true, true,
		 * null);
		 * 
		 * //create paf_views folder IFolder viewsFolder =
		 * confFolder.getFolder(PafBaseConstants.DN_ViewsFldr);
		 * viewsFolder.create(true, true, null);
		 * 
		 * //create paf_view_secitons folder IFolder viewSectionsFolder =
		 * confFolder.getFolder(PafBaseConstants.DN_ViewSectionsFldr);
		 * viewSectionsFolder.create(true, true, null);
		 * 
		 * //create paf_rules folder IFolder rulesFolder =
		 * confFolder.getFolder(PafBaseConstants.DN_RuleSetsFldr);
		 * rulesFolder.create(true, true, null);
		 * 
		 * //get numeric formats file name String numericFormatsFileName =
		 * Constants.ADMIN_CONSOLE_CONF_DIRECTORY +
		 * Constants.NUMERIC_FORMATS_FILE;
		 * 
		 * //get ifile for numeric formats file IFile numericFormatsFile =
		 * confFolder.getFile(Constants.NUMERIC_FORMATS_FILE);
		 * 
		 * //copy numeric formats file Files.copy(numericFormatsFileName,
		 * numericFormatsFile.getLocation().toString());
		 * 
		 * 
		 * //get paf apps name String pafAppFileName =
		 * Constants.ADMIN_CONSOLE_CONF_DIRECTORY + Constants.PAF_APPS_FILE;
		 * 
		 * //get ifile for pafapp file IFile pafAppFile =
		 * confFolder.getFile(Constants.PAF_APPS_FILE);
		 * 
		 * //copy paf app file Files.copy(pafAppFileName,
		 * pafAppFile.getLocation().toString());
		 * 
		 * //refresh local filesystem
		 * confFolder.refreshLocal(IResource.DEPTH_INFINITE, null); }
		 * 
		 * //TTN-710 BEGIN PafApplicationDef[] pafApps =
		 * PafApplicationUtil.getPafApps(newProject);
		 * 
		 * if ( pafApps != null && pafApps.length > 0) {
		 * 
		 * PafApplicationDef pafAppDef = pafApps[0];
		 * 
		 * pafAppDef.setAppId(newProjectName);
		 * 
		 * PafApplicationUtil.setPafApps(newProject, pafAppDef);
		 * 
		 * } //TTN-710 END
		 * 
		 * } catch (CoreException e) {
		 * 
		 * logger.error(e.getMessage());
		 * 
		 * }
		 * 
		 * }
		 */}

	/**
	 * @return Returns the newProjectName.
	 */
	public String getNewProjectName() {
		return newProjectName;
	}

	/**
	 * @param newProjectName
	 *            The newProjectName to set.
	 */
	public void setNewProjectName(String newProjectName) {
		this.newProjectName = newProjectName;
	}

	public List<String> getDataSourceIds(String serverName) {

		List<String> dsList = new ArrayList<String>();

		final String url = PafServerUtil.getServerWebserviceUrl(serverName);

		try {
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil
					.getServer(serverName));
		} catch (PafServerNotFound e1) {
			logger.error("Server '" + serverName + "' is not defined in the "
					+ Constants.SERVERS_FILE);
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if authenticated, continue
		// if ( SecurityManager.isAuthenticated(url) ) {

		// get session
		final ServerSession serverSession = SecurityManager.getSession(url);

		// create new paf request and set client id
		PafMdbPropsRequest pafMdbRequest = new PafMdbPropsRequest();
		

		pafMdbRequest.setClientId(serverSession.getClientId());

		try {

			// query server to get cell note info ar in this response
			
			PafService service = WebServicesUtil.getPafService(url);
			

			if (service != null) {

				
				dsList = service.getDataSourceIds(pafMdbRequest);
				

			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			
		}

		return dsList;
	}

	
	/** This method returns the list of dimensions for the server and datasource id. 
	 * @param serverName
	 * @param dataSourceId
	 * @return
	 */
	public List<String> getDimensions(String serverName, String dataSourceId) {

		List<String> dimList = new ArrayList<String>();

		final String url = PafServerUtil.getServerWebserviceUrl(serverName);

		try {
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil
					.getServer(serverName));
		} catch (PafServerNotFound e1) {
			logger.error("Server '" + serverName + "' is not defined in the "
					+ Constants.SERVERS_FILE);
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if authenticated, continue
		if (SecurityManager.isAuthenticated(url)) {

			// get session
			ServerSession serverSession = SecurityManager.getSession(url);

			// create new paf request and set client id
			PafMdbPropsRequest pafMdbRequest = new PafMdbPropsRequest();
			
			pafMdbRequest.setDataSourceId(dataSourceId);
			

			pafMdbRequest.setClientId(serverSession.getClientId());

			try {

				// query server to get cell note info ar in this response
				
				PafService service = WebServicesUtil.getPafService(url);
				
				// Get the PafResponse to get the dimension list
				if (service != null) {

					
					PafMdbPropsResponse pafResponse = service
							.getMdbProps(pafMdbRequest);
					PafMdbProps pafProps = pafResponse.getMdbProps();

					dimList = pafProps.getBaseDims();
					
					
					// Get the Measures dimension and time dimension.
					accountDim = pafProps.getAccountsDim();
					
					timeDim = pafProps.getTimeDim();
					
					
				
				

				}
			} catch (Exception e) {
				logger.info(e.getMessage());
				
			}
		}
		return dimList;
	}


		
	public void setDim(Combo[] dimCombo, Label[] dimLbl, String dimName) {
		for (int i = 0; i < dimLbl.length; i++) {
			if (dimLbl[i].getText().equals(dimName.toString()))
				dimCombo[i].select(i);
		}
	}

	public void setMeasureRootArray(DimLevelGenInfo[] msrRootArray) {
		levelGenInfo = msrRootArray;
		
	}

	public DimLevelGenInfo[] getMeasureRootArray() {
		return levelGenInfo;
	}
	
	public String getFirstVersion(String serverName, String dimensionName, String dataSourceId)
	{
		
		return firstVersion;
	}
	
	
	// method to get a list of all Level 0 members from the version dimension
	public List<String> getAllVersions(String serverName, String dimensionName, String dataSourceId)
	{
		
		List<String> versionList = new ArrayList<String>();
		boolean firstVersionNotFound = true;
		try{
		
		PafServer pafServer = PafServerUtil.getServer(serverName);
		 
		 String url = pafServer.getCompleteWSDLService();
		 
		  PafSimpleDimTree pafSimpleTree = SecurityManager.getPafSimpleDimTreeForDataSource(url,dimensionName,dataSourceId);
		  
		  List<PafSimpleDimMember> versionMember  = pafSimpleTree.getMemberObjects();
		  
		  for(int i=0;i<versionMember.size();i++)
		  {
			  
		       if(versionMember.get(i).getPafSimpleDimMemberProps().getLevelNumber()==0)
		       {
		    	   // get the first version to be used to create a plan cycle and season
		    	   if(firstVersionNotFound)
		    	   {
		    	   firstVersion = versionMember.get(i).getKey();
		    	   firstVersionNotFound = false;
		    	   }
		    	   
		    	   versionList.add(versionMember.get(i).getKey());
		    	
		       }
			 
		  }
		  
		  
		}catch(Exception e){
				logger.error(e.getMessage());
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
				throw new RuntimeException(e.getMessage());
			}
		
		return versionList;
	}
	
	// method to get the first plan type
		public String getFirstPlanType(String serverName, String dimensionName, String dataSourceId)
		{
			
			String firstPlanType = "";
			boolean firstPTNotFound = true;
			try{
			
			PafServer pafServer = PafServerUtil.getServer(serverName);
			 
			 String url = pafServer.getCompleteWSDLService();
			 
			  PafSimpleDimTree pafSimpleTree = SecurityManager.getPafSimpleDimTreeForDataSource(url,dimensionName,dataSourceId);
			  
			  List<PafSimpleDimMember> planMember  = pafSimpleTree.getMemberObjects();
			  
			  for(int i=0;i<planMember.size();i++)
			  {
				  
			       if(planMember.get(i).getPafSimpleDimMemberProps().getLevelNumber()==0)
			       {
			    	   // get the first plan type to be used to create a role and role config
			    	   if(firstPTNotFound)
			    	   {
			    		   firstPlanType = planMember.get(i).getKey();
			    		   firstPTNotFound = false;
			    	   }
			    	   
			    	   
			    	
			       }
				 
			  }
			  
			  
			}catch(Exception e){
					logger.error(e.getMessage());
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
					throw new RuntimeException(e.getMessage());
				}
			
			return firstPlanType;
		}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.menu.actions.importexport.ImportProjectFromFileset;
import com.pace.admin.menu.actions.importexport.ImportProjectFromServer;
import com.pace.admin.menu.project.ProjectSource;
import com.pace.admin.menu.project.ProjectSourceType;
import com.pace.admin.menu.util.ProjectWizardUtil;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

/**
 * @author jmilliron
 *
 */
public class ProjectImportWizard extends Wizard implements IImportWizard {

	private static Logger logger = Logger.getLogger(ProjectImportWizard.class);
	
	private ProjectSourceOptionsWizardPage projectSourceOptionsWizardPage;
	
	private FileSelectionWizardPage excelFileSelectionWizardPage;
	
	private FileSelectionWizardPage pafFileSelectionWizardPage;
	
	private GenericSelectionOptionsWizardPage  serverSelectionWizardPage;
	
	private GenericSelectionOptionsWizardPage  templateSelectionWizardPage;
	
	private ProjectNameSelectionWizardPage projectNameSelectionWizardPage;
	
	private ProjectDataSelectionPage projectImportDataSelectionWizardPage;
	
	private List<ProjectSource> projectSourceList = new ArrayList<ProjectSource>();
	
	private ProjectSource lastSelectedProjectSource;
	
	private Map<ProjectSourceType, IWizardPage> wizardPageMap = new HashMap<ProjectSourceType, IWizardPage>();
	
	private Set<PafServer> pafServers;
	
	private Map<ProjectElementId, List<ProjectDataError>> projectDataErrorMap;
	
	private IWorkbench workbench = null;
	
	private Map<String, IProject> currentProjects = null;
	
	private Map<String, File> templateFiles = null;
			
	public ProjectImportWizard() {

	}
		
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
		setWindowTitle("Import Pace Project");
		setNeedsProgressMonitor(true);
		
		this.workbench = workbench;
		//this.selection = selection;
		
		currentProjects =  ViewerUtil.getCurrentProjects();
		
		//get the templates
		templateFiles = new HashMap<String, File>();
		List<File> templates = ProjectWizardUtil.getTemplates();
		//list to hold the name of templates.
		List<String> templateNameList = new ArrayList<String>();
		
		if(templates != null && templates.size() > 0){
			//for each file in the list of templates.
			for(File f : templates){
				
				//get the file name.
				String s = f.getName();
				//create the name
				String name = s.replace("." + Constants.PAF_EXT, "");
				//strip off the file ext.
				templateNameList.add(name);
				//add to the map
				templateFiles.put(name, f);
				
			}

		}
		
		
		projectSourceList.add(new ProjectSource(ProjectSourceType.Server.toString(), "Import project data from a server's config.", ProjectSourceType.Server));
		projectSourceList.add(new ProjectSource(ProjectSourceType.Excel.toString(), "Import project data from an Excel Workbook.", ProjectSourceType.Excel));
		projectSourceList.add(new ProjectSource("Pace Archive File", "Import project data from a Pace archive file.", ProjectSourceType.PaceArchiveFile));
		//only add the template option if the user has at least one temple installed.
		if(templateNameList.size() > 0){
			projectSourceList.add(new ProjectSource(ProjectSourceType.Template.toString(), "Import project data from a Pace template.", ProjectSourceType.Template));
		}
		
		
		projectSourceOptionsWizardPage = ProjectWizardUtil.getImportProjectOptionsWizardPage(projectSourceList, null);
		
		excelFileSelectionWizardPage =  ProjectWizardUtil.getExcelFileSelectionWizardPage();
		
		pafFileSelectionWizardPage = ProjectWizardUtil.getPaceArchiveFileSelectionWizardPage();
		
		//get the current list of servers.
		List<String> serverNameList = new ArrayList<String>();
		
		pafServers = PafServerUtil.getRunningPafServers();
		
		if(pafServers != null && pafServers.size() > 0){
			
			for(PafServer server : pafServers){
				serverNameList.add(server.getName());
			}
			
		}
		
		serverSelectionWizardPage = ProjectWizardUtil.getServerSelectionWizardPage(serverNameList.toArray(new String[0]), new String[0]);
		
		templateSelectionWizardPage = ProjectWizardUtil.getTemplateSelectionWizardPage(templateNameList.toArray(new String[0]), new String[0]);
		
		projectNameSelectionWizardPage = ProjectWizardUtil.getImportProjectNameSelectionWizardPage(currentProjects.keySet().toArray(new String[0]));
		
		//TTN-2161 Set project name in preferences if a project is selected.
		if(ViewerUtil.getProjectNode(selection)!=null){
			projectNameSelectionWizardPage.setExistingProjectNamePreference(ViewerUtil.getProjectNode(selection).getName());
		}else{
			projectNameSelectionWizardPage.setExistingProjectNamePreference("");
		}
		
		wizardPageMap.put(ProjectSourceType.Excel, excelFileSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.PaceArchiveFile, pafFileSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.Server, serverSelectionWizardPage);
		wizardPageMap.put(ProjectSourceType.Template, templateSelectionWizardPage);		
		
		projectImportDataSelectionWizardPage = ProjectWizardUtil.getProjectImportDataSelectionWizardPage();

		
		
	}
	
	//@Override
	public IWizardPage getNextPage(IWizardPage page) {

		if (page.equals(projectSourceOptionsWizardPage)) {
			
			List<ProjectSource> selectedOptionList = projectSourceOptionsWizardPage.getSelectedSourceOptions();
			
			if ( selectedOptionList.size() == 1 ) {
							
				ProjectSource selectedSource = selectedOptionList.get(0);

//				//if user changes selection type, clear page complete on last page.
//				if ( lastSelectedProjectSource != null && lastSelectedProjectSource.equals(selectedSource)) {
//
//					projectNameSelectionWizardPage.setPageComplete(false);
//				
//				}
				
				lastSelectedProjectSource = selectedSource;
				
				switch ( selectedSource.getType() ) {
				
				case Excel:
					pafFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					return excelFileSelectionWizardPage;
				case PaceArchiveFile:
					excelFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					return pafFileSelectionWizardPage;
				case Server:
					pafFileSelectionWizardPage.setPageComplete(true);
					excelFileSelectionWizardPage.setPageComplete(true);
					templateSelectionWizardPage.setPageComplete(true);
					return serverSelectionWizardPage;
				case Template:
					pafFileSelectionWizardPage.setPageComplete(true);
					serverSelectionWizardPage.setPageComplete(true);
					excelFileSelectionWizardPage.setPageComplete(true);
					return templateSelectionWizardPage;
				case None:
					pafFileSelectionWizardPage.setPageComplete(false);
					serverSelectionWizardPage.setPageComplete(false);
					excelFileSelectionWizardPage.setPageComplete(false);
					templateSelectionWizardPage.setPageComplete(false);
					return projectNameSelectionWizardPage;
									
				}		
				
				
			}	
			
		//if not project selection wizard but is in map
		} else if ( wizardPageMap.values().contains(page) ) {	
			
			/*if ( projectNameSelectionWizardPage.getProjectName() != null ) {
				
				projectNameSelectionWizardPage.setPageComplete(true);
				
			}*/
			
			return projectImportDataSelectionWizardPage;
			
		} else if ( page.equals(projectImportDataSelectionWizardPage )) {
			

			return projectNameSelectionWizardPage;
			
		}
		
		return null;
	}
//	
//	@Override
//	public boolean canFinish() {
//
//		if ( projectNameSelectionWizardPage != null  && projectNameSelectionWizardPage.isPageComplete()) {
//			
//			return projectNameSelectionWizardPage.isPageComplete();
//		}
//		
//		return super.canFinish();
//	}
	
	@Override
	public void addPages() {
				
		addPage(projectSourceOptionsWizardPage);
		addPage(excelFileSelectionWizardPage);
		addPage(pafFileSelectionWizardPage);
		addPage(serverSelectionWizardPage);
		addPage(templateSelectionWizardPage);
		addPage(projectImportDataSelectionWizardPage);
		addPage(projectNameSelectionWizardPage);
		
				
	}
	
	@Override
	public boolean performFinish() {
		
		//data filter of items to import.
		ProjectElementId[] elements = projectImportDataSelectionWizardPage.getSelectedProjectItems();
		//create a new project?
		boolean createNewProject = projectNameSelectionWizardPage.createNewProject();
		//get the project name.
		String projectName = projectNameSelectionWizardPage.getProjectName();
		//get the excel path file name.
		String excelPathFileName = excelFileSelectionWizardPage.getSelectedFileName();
		//get the paf path file name.
		String pafPathFileName = pafFileSelectionWizardPage.getSelectedFileName();
		//Get the ignoreDependencies check box, ignore if create/importing into a new project.
	    boolean ignoreDependencies = false;
	    if(!createNewProject){
	    	ignoreDependencies = projectImportDataSelectionWizardPage.isIgnoreDependencies();
	    } else {
	    	ConsoleWriter.writeMessage("New project being created, ignoring: 'Ignore Dependencies' option.");	    	
	    }
	    
		//get/create the project
	    IProject project = getProject(createNewProject, projectName);
	    //TTN 1724 - AC not handling new project / import project with same project name but different case
	    if( project == null ) {
	    	return false;
	    }
		//get the selected templates.
		File templatePathFileName = null;
		if(templateSelectionWizardPage.getSelectedItems() != null && templateSelectionWizardPage.getSelectedItems().length > 0){
			templatePathFileName = templateFiles.get(templateSelectionWizardPage.getSelectedItems()[0]);
		}
		
		boolean returnValue = false;
		String errorMessage = "";
		
		
		switch ( lastSelectedProjectSource.getType() ) {
		
			case Excel:
				
				try {
					
					errorMessage = "There was a problem importing project from: " + excelPathFileName + ". \nPlease select the Show Errors button on the last page to see the errors.";
					returnValue = importFromFileset(excelPathFileName, elements, project, ignoreDependencies, lastSelectedProjectSource.getType());
				
				} catch (Throwable e) {
					e.printStackTrace();
					GUIUtil.openMessageWindow(
							getWindowTitle(),
							errorMessage,
							SWT.ERROR);
				}
				break;
				
			case PaceArchiveFile:
				
				try {
					
					errorMessage = "There was a problem importing project from: " + pafPathFileName + ". \nPlease select the Show Errors button on the last page to see the errors.";
					returnValue = importFromFileset(pafPathFileName, elements, project, ignoreDependencies, lastSelectedProjectSource.getType());
					
				} catch (Throwable e) {
					e.printStackTrace();
					GUIUtil.openMessageWindow(
							getWindowTitle(),
							errorMessage,
							SWT.ERROR);
				}
				break;
				
			case Server:
				try {
					
					errorMessage = "There was a problem with importing project: '" + project.getName() + "'. \nPlease check the log messages to determine the problem.";
					returnValue = importFromServer(elements, project);
					
				} catch (Throwable e) {
					e.printStackTrace();
					GUIUtil.openMessageWindow(
							getWindowTitle(),
							errorMessage,
							SWT.ERROR);
				}
				break;
				
			case Template:
				try {
					
					errorMessage = "There was a problem importing project from: " + templatePathFileName.getAbsolutePath() + ". \nPlease select the Show Errors button on the last page to see the errors.";
					returnValue = importFromFileset(templatePathFileName.getAbsolutePath(), elements, project, ignoreDependencies, lastSelectedProjectSource.getType());
					
				} catch (Throwable e) {
					e.printStackTrace();
					GUIUtil.openMessageWindow(
							getWindowTitle(),
							errorMessage,
							SWT.ERROR);
				}
				break;
					
		}	
		
		if(!returnValue){
			handleError(returnValue, createNewProject, project);
		}
		//TTN-1449 BEGIN
		//if project exists
		if ( createNewProject && project != null && project.exists()) {
		
			PafApplicationDef[] pafApps = PafApplicationUtil.getPafApps(project);
			
			if ( pafApps != null && pafApps.length > 0) {
				
				PafApplicationDef pafAppDef = pafApps[0];
				
				pafAppDef.setAppId(project.getName());
				
				PafApplicationUtil.setPafApps(project, pafAppDef);
				
			}
					
		}		
		//TTN-1449 END	
		
		
		return returnValue;
	}
	
	
	private void handleError(boolean returnValue, boolean createNewProject, IProject project){
		
		if(!returnValue && createNewProject){
			ViewerUtil.deleteProject(project);
		}
		
		List<ProjectDataError> errors = getProjectDataErrors();
		
		if(errors != null && errors.size() > 0){
			projectNameSelectionWizardPage.setProjectDataErrors(errors);
		} 
		
	}
	
//	 public void buildAndWaitForEnd() {
//		 
//		 final RefreshProjectsAction refreshProjectsAction = new RefreshProjectsAction(
//	        		workbench.getActiveWorkbenchWindow());
//		 
//		 IRunnableWithProgress op = new IRunnableWithProgress() {
//	            public void run(IProgressMonitor monitor) throws InvocationTargetException {
//	                try {
//	                    doFinish(monitor, refreshProjectsAction);
//	                } catch (CoreException e) {
//	                    throw new InvocationTargetException(e);
//	                } finally {
//	                    monitor.done();
//	                }
//	            }
//	        };
//	        try {
//	            getContainer().run(true, false, op);
//	        } catch (InterruptedException e) {
//	        	Throwable realException = e;
//	            MessageDialog.openError(getShell(), "Error", realException.getMessage());
//	        } catch (InvocationTargetException e) {
//	            Throwable realException = e.getTargetException();
//	            MessageDialog.openError(getShell(), "Error", realException.getMessage());
//	        }
//
//     } 
//	
//	 private void doFinish(IProgressMonitor monitor, RefreshProjectsAction refreshProjectsAction)
//		        throws CoreException {
//			
//		 monitor.beginTask("Refreshing projects", IProgressMonitor.UNKNOWN);
//
//		 try {
//			 refreshProjectsAction.run(monitor);
//	        } catch (Exception e) {
//	        }
//	}
	 
	private List<ProjectDataError> getProjectDataErrors(){
		

		List<ProjectDataError> errors = new ArrayList<ProjectDataError>();
		
		if(projectDataErrorMap != null && projectDataErrorMap.keySet() != null){
			
			for(ProjectElementId element : projectDataErrorMap.keySet()){
				
				List<ProjectDataError> error = projectDataErrorMap.get(element);
				errors.addAll(error);
				
			}
			
		}
		
		return errors;
		
	}
	
	
	private boolean importFromFileset(String pathFileName, ProjectElementId[] elements,
			IProject project, boolean ignoreDependencies, ProjectSourceType projectType) throws Throwable{
		
		projectDataErrorMap = null;
		
		final ImportProjectFromFileset importProjectFromFileset = new ImportProjectFromFileset(
				workbench.getActiveWorkbenchWindow(), pathFileName, project, elements, ignoreDependencies, projectType);
		
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
				new Runnable() {
									
			public void run() {
			
				importProjectFromFileset.run();
				
			}
			
		});
		
		
		if(importProjectFromFileset.isError()){
			projectDataErrorMap = importProjectFromFileset.getProjectDataErrors();
			throw importProjectFromFileset.getThrowable();
		}
		return true;
		
	}
	
	
	private IProject getProject(boolean createNewProject, String projectName){
		IProject project = null;
		
		if(!createNewProject){
			
			project = currentProjects.get(projectName);
			
		}else{
			
			//a new pace project.
			try {
				
				//create a new pace project.
				project = ViewerUtil.createNewProject(projectName);

			} catch (CoreException e) {
				
				logger.error("Cannot create project: " + projectName  + " : " +  e.getMessage());
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.WARNING);
			}  
			
		}

		
		return project;
	}
	
	
	private boolean importFromServer(ProjectElementId[] elements, IProject project)  throws Throwable {
		
		projectDataErrorMap = null;
		
		//get the user selected PafServer from the Willard.
		String paceServer = null;
		if(serverSelectionWizardPage.getSelectedItems() != null && serverSelectionWizardPage.getSelectedItems().length >0){
			
			paceServer = serverSelectionWizardPage.getSelectedItems()[0];
		}
		
		PafServer pafServer = null;
		
		try {
			//Get the PafServer object from the PafServerUtil
			pafServer = PafServerUtil.getServer(paceServer);
			
		} catch (PafServerNotFound e) {

			logger.error("Server '" + paceServer + "' was not found.");
		}
		
		final ImportProjectFromServer importProjectFromServer = new ImportProjectFromServer(
				workbench.getActiveWorkbenchWindow(), elements, project, pafServer);
		
		
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
				new Runnable() {
									
			public void run() {
			
				importProjectFromServer.run();
				
			}
			
		});
		
		if(importProjectFromServer.isError()){
			//projectDataErrorMap = importProjectFromServer.getProjectDataErrors();
			throw importProjectFromServer.getThrowable();
			//return false;
		}
		return true;
		
		
		
	}
	
}

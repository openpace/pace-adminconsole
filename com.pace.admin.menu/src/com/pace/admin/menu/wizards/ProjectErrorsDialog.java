/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.wizards;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pace.base.project.ProjectDataError;

public class ProjectErrorsDialog extends TitleAreaDialog {
	private static class Sorter extends ViewerSorter {
		
		private int propertyIndex;
		private static final int ASCENDING = 0;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;
		
		public Sorter() {
			this.propertyIndex = 0;
			//this.direction = DESCENDING;
			this.direction = ASCENDING;
		}
		
		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		
		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int rc = 0;

			if(e1 instanceof ProjectDataError && e2 instanceof ProjectDataError){
				
				ProjectDataError p1 = (ProjectDataError) e1;
				
				ProjectDataError p2 = (ProjectDataError) e2;
				
				switch ( propertyIndex ) {
				
					case 0: 
						rc =  p1.getProjectElementId().compareTo(p2.getProjectElementId());
						break;
					case 1:
						rc =  p1.getErrorSource().compareToIgnoreCase(p2.getErrorSource());
						break;
					case 2:
						rc =  p1.getErrorMessage().compareToIgnoreCase(p2.getErrorMessage());
						break;
					default:
						rc = 0;

				}
				
				// If descending order, flip the direction
				if (direction == DESCENDING) {
					rc = -rc;
				}
				return rc;

				
			}
			return rc;
		}
		
	}
	
	private List<ProjectDataError> projectDataErrors;
	private Table table;
	private Sorter tableSorter;
	private TableViewer tableViewer;
	private final String dialogPageDesc;
	private final String dialogPageTitle;
	
	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		public String getColumnText(Object element, int columnIndex) {

			String strValue = null;
			
			if ( element instanceof ProjectDataError ) {
				
				ProjectDataError member = (ProjectDataError) element;
				
				switch ( columnIndex ) {
				
				case 0: 
					strValue = member.getProjectElementId().getName();
					break;
				case 1:
					strValue = member.getErrorSource();
					break;
				case 2:
					strValue = member.getErrorMessage();
					break;									
				}
								
			}		
			return strValue;
		}
	}
	private static class ContentProvider implements IStructuredContentProvider {
		@SuppressWarnings("unchecked")
		public Object[] getElements(Object inputElement) {
			
			if ( inputElement instanceof List ) {
								
				List<ProjectDataError> invalidSecurityMemberSet = (List<ProjectDataError>) inputElement;
				
				return invalidSecurityMemberSet.toArray();
			}
			
			return null;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	

	public ProjectErrorsDialog(Shell parentShell, String wizardPageTitle, 
			String wizardPageDesc, List<ProjectDataError> projectDataErrors) {
		super(parentShell);
		
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		
		this.dialogPageTitle = wizardPageTitle;
		this.dialogPageDesc = wizardPageDesc;
		this.projectDataErrors = projectDataErrors;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 207;
		container.setLayoutData(gridData);
		tableSorter = new Sorter();
		
				tableViewer = new TableViewer(container, SWT.BORDER);
				tableViewer.setSorter(tableSorter);
				tableViewer.setContentProvider(new ContentProvider());
				tableViewer.setLabelProvider(new TableLabelProvider());
				table = tableViewer.getTable();
				{
					GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gridData_1.heightHint = 331;
					gridData_1.widthHint = 581;
					table.setLayoutData(gridData_1);
				}
				table.setHeaderVisible(true);
				table.setLinesVisible(true);
				
						final TableColumn projectElementIdColumn = new TableColumn(table, SWT.NONE);
						projectElementIdColumn.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent e) {
								
								doSort(projectElementIdColumn, 0);

							}
						});
						projectElementIdColumn.setWidth(167);
						projectElementIdColumn.setText("Project Element Id");
						
								final TableColumn errorSourceColumn = new TableColumn(table, SWT.NONE);
								errorSourceColumn.addSelectionListener(new SelectionAdapter() {
									@Override
									public void widgetSelected(SelectionEvent e) {
										
										doSort(errorSourceColumn, 1);
										
									}
								});
								errorSourceColumn.setWidth(119);
								errorSourceColumn.setText("Error Source");
								
										final TableColumn errorMessageColumn = new TableColumn(table, SWT.NONE);
										errorMessageColumn.addSelectionListener(new SelectionAdapter() {
											@Override
											public void widgetSelected(SelectionEvent e) {
												
												doSort(errorMessageColumn, 2);
												
											}
										});
										errorMessageColumn.setWidth(401);
										errorMessageColumn.setText("Error Message");
		
		tableViewer.setInput(projectDataErrors);
		
		setTitle(this.dialogPageTitle);
		setMessage(this.dialogPageDesc);
		//
		return area;

	}
	
	private void doSort(TableColumn column, int index){
		
		tableSorter.setColumn(index);
		int dir = tableViewer.getTable().getSortDirection();
		if (tableViewer.getTable().getSortColumn() == column) {
			dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
		} else {

			dir = SWT.DOWN;
		}
		tableViewer.getTable().setSortDirection(dir);
		tableViewer.getTable().setSortColumn(column);
		tableViewer.refresh();
		
	}
	
	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		
//		cancelButton = createButton(parent, IDialogConstants.CANCEL_ID,
//				IDialogConstants.CANCEL_LABEL, true);

	}
	
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(this.dialogPageTitle);
	}
	
	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(700, 500);
	}

}

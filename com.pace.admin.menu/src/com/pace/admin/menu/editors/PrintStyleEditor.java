/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.menu.editors.input.PrintStyleInput;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.ui.IPafMapModelManager;

public class PrintStyleEditor extends EditorPart {

	public static final String ID = "com.pace.admin.menu.editors.PrintStyleEditor";
	private static Logger logger = Logger.getLogger(PrintStyleEditor.class);

	private IProject project = null;
	private PrintStyleInput printStyleInput;
	private TreeViewer viewer;
	private IPafMapModelManager model;
	private Text txtPrintStyle;

	//Page Tab
	private Button btnPortrait;
	private Button btnLandscape;
	private Button btnAdjustTo;
	private Spinner spinnerPercentNormalSize;
	private Button btnFitTo;
	private Spinner spinnerPagesWide;
	private Spinner spinnerPagesTall;
	private Combo comboPaperSize;
	private Text txtFirstPageNumber;
	//Margins Tab
	private Spinner spinnerTop;
	private Spinner spinnerHeader;
	private Spinner spinnerLeft;
	private Spinner spinnerRight;
	private Spinner spinnerBottom;
	private Spinner spinnerFooter;
	private Button btnHorizontally;
	private Button btnVertically;
	//Header/Footer Tab
	private Text textHeader;
	private Text textFooter;
	private Button btnDiffOddAndEvenPages;
	private Button btnDiffFirstPage;
	private Button btnScaleWithDocument;
	private Button btnAlignWithPageMargins;
	//Sheet Tab
	private Button btnEntireSheet;
	private Button btnUserSelection;
	private Text txtUserSelection;
	private Combo comboRowsToRepeatAtTop;
	private Combo comboColsToRepeatAtLeft;
	private Button btnGridlines;
	private Button btnBlackAndWhite;
	private Button btnDraftQuality;
	private Button btnRowAndColumnHeadings;
	private Combo comboComment;
	private Combo comboCellErrors;
	private Button btnDownThenOver;
	private Button btnOverThenDown;
	
	private boolean isDirty;
	
	public PrintStyleEditor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	public IPafMapModelManager getModel() {
		return model;
	}

	public void setModel(IPafMapModelManager model) {
		this.model = model;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		try {
			//Page Tab
			printStyleInput.setPrintStyleName(txtPrintStyle.getText());
			printStyleInput.setPortrait(btnPortrait.getSelection());
			printStyleInput.setLandscape(btnLandscape.getSelection());
			printStyleInput.setAdjustTo(btnAdjustTo.getSelection());
			printStyleInput.setPercentNormalSize(spinnerPercentNormalSize.getSelection());
			printStyleInput.setFitTo(btnFitTo.getSelection());
			printStyleInput.setPageWide(spinnerPagesWide.getSelection());
			printStyleInput.setPageTall(spinnerPagesTall.getSelection());
			printStyleInput.setPaperSize(comboPaperSize.getText());
			printStyleInput.setFirstPageNumber(txtFirstPageNumber.getText());
			//Margins Tab
			printStyleInput.setTop((float)(spinnerTop.getSelection()*0.01));
			printStyleInput.setHeader((float)(spinnerHeader.getSelection()*0.01));
			printStyleInput.setLeft((float)(spinnerLeft.getSelection()*0.01));
			printStyleInput.setRight((float)(spinnerRight.getSelection()*0.01));
			printStyleInput.setBottom((float)(spinnerBottom.getSelection()*0.01));
			printStyleInput.setFooter((float)(spinnerFooter.getSelection()*0.01));
			printStyleInput.setCenterHorizontally(btnHorizontally.getSelection());
			printStyleInput.setCenterVertically(btnVertically.getSelection());
			//Header/Footer Tab
			printStyleInput.setHeaderText(this.textHeader.getText());
			printStyleInput.setFooterText(this.textFooter.getText());
			printStyleInput.setDiffOddAndEvenPages(btnDiffOddAndEvenPages.getSelection());
			printStyleInput.setDiffFirstPage(btnDiffFirstPage.getSelection());
			printStyleInput.setScaleWithDocument(btnScaleWithDocument.getSelection());
			printStyleInput.setAlignWithPageMargin(btnAlignWithPageMargins.getSelection());
			//Sheet Tab
			printStyleInput.setEntireSheet(btnEntireSheet.getSelection());
			printStyleInput.setUserSelection(btnUserSelection.getSelection());
			printStyleInput.setUserSelectionText(txtUserSelection.getText());
			printStyleInput.setRowsToRepeatAtTop(comboRowsToRepeatAtTop.getText());
			printStyleInput.setColsToRepeatAtLeft(comboColsToRepeatAtLeft.getText());
			printStyleInput.setGridlines(btnGridlines.getSelection());
			printStyleInput.setBlackAndWhite(btnBlackAndWhite.getSelection());
			printStyleInput.setDraftQuality(this.btnDraftQuality.getSelection());
			printStyleInput.setRowAndColHeadings(btnRowAndColumnHeadings.getSelection());
			printStyleInput.setComment(comboComment.getText());
			printStyleInput.setCellErrorsAs(comboCellErrors.getText());
			printStyleInput.setDownThenOver(btnDownThenOver.getSelection());
			printStyleInput.setOverThenDown(btnOverThenDown.getSelection());
	
			printStyleInput.save();
			
			isDirty = false;
			
			firePropertyChange(IEditorPart.PROP_DIRTY);
			//Refresh the tree nodes.
			updateTreeViewer();
		}	
		catch(Exception e){
			logger.error(e.getMessage());
			return;
		}

	}

	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		try{
			ProjectNode projectNode = ViewerUtil.getProjectNode(project.getName());
			
			if(projectNode != null){
				for (Object obj : projectNode.getChildren()) {
					if ( obj instanceof PrintStylesNode) {
						//get the high level node.
						PrintStylesNode printPrefsNode = (PrintStylesNode)obj;
						//re-create children
						printPrefsNode.createChildren(null);
						//update the menu plug in.
						viewer.refresh(printPrefsNode);
					}	
				}
			}	
		} catch(Exception e){
			logger.error(e.getMessage());
		}
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		// TODO Auto-generated method stub
		printStyleInput = (PrintStyleInput) input;
		setSite(site);
		setInput(input);
		setModel(printStyleInput.getModel());

		isDirty = false;
	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		if( model instanceof PrintStylesManager ) {
			setPartName(Constants.PRINT_STYLE_NAME + ": " + this.txtPrintStyle.getText());
		}
		else if( model instanceof ViewModelManager ) {
			setPartName(printStyleInput.getName() + ": " + PafBaseConstants.EMBEDED_PRINT_SETTINGS);
		}
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
	
		Composite container = new Composite(parent, SWT.NONE);
		container.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(final DisposeEvent e) {
				logger.debug("PrintStyleEditor disposed.");
			}
		});
		container.setLayout(new GridLayout(1, false));
		
		Composite compositeName = new Composite(container, SWT.NONE);
		compositeName.setLayout(new GridLayout(2, false));
		GridData gd_compositeName = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_compositeName.widthHint = 369;
		gd_compositeName.heightHint = 37;
		compositeName.setLayoutData(gd_compositeName);
		
		Label lblNewLabel_11 = new Label(compositeName, SWT.CENTER);
		lblNewLabel_11.setAlignment(SWT.LEFT);
		GridData gd_lblNewLabel_11 = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_lblNewLabel_11.heightHint = 24;
		gd_lblNewLabel_11.widthHint = 98;
		lblNewLabel_11.setLayoutData(gd_lblNewLabel_11);
		lblNewLabel_11.setText("Print Style Name:");
		
		txtPrintStyle = new Text(compositeName,SWT.BORDER);
		txtPrintStyle.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		
		GridData gd_txtPrintStyle = new GridData(SWT.RIGHT, SWT.BORDER, false, false, 1, 1);
		gd_txtPrintStyle.widthHint = 248;
		gd_txtPrintStyle.heightHint = 19;
		txtPrintStyle.setLayoutData(gd_txtPrintStyle);
		if( printStyleInput.isNew() || printStyleInput.isCopy() ) {
			txtPrintStyle.setEnabled(true);
			txtPrintStyle.setEditable(true);
		}
		else {
			txtPrintStyle.setEnabled(false);
			txtPrintStyle.setEditable(false);
		}
		
		//Page Tab
		final TabFolder tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TabItem tbtmPage = new TabItem(tabFolder, SWT.NONE);
		tbtmPage.setText("Page");
		
		Composite compositeTabPage = new Composite(tabFolder, SWT.NONE);
		tbtmPage.setControl(compositeTabPage);
		compositeTabPage.setLayout(new GridLayout(1, false));
		

		Group grpOri = new Group(compositeTabPage, SWT.NONE);
		grpOri.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpOri.setText("Orientation");
		
		btnPortrait = new Button(grpOri, SWT.RADIO);
		btnPortrait.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnPortrait.setText("Portrait");
		btnPortrait.setBounds(10, 23, 90, 16);
		
		btnLandscape = new Button(grpOri, SWT.RADIO);
		btnLandscape.setSelection(true);
		btnLandscape.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnLandscape.setBounds(122, 23, 90, 16);
		btnLandscape.setText("Landscape");
		
		Group grpScaling = new Group(compositeTabPage, SWT.NONE);
		grpScaling.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpScaling.setText("Scaling");
		
		btnAdjustTo = new Button(grpScaling, SWT.RADIO);
		btnAdjustTo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnAdjustTo.setBounds(10, 22, 90, 16);
		btnAdjustTo.setText("Adjust to:");
		
		btnFitTo = new Button(grpScaling, SWT.RADIO);
		btnFitTo.setSelection(true);
		btnFitTo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnFitTo.setBounds(10, 50, 90, 16);
		btnFitTo.setText("Fit to:");
		
		spinnerPagesWide = new Spinner(grpScaling, SWT.BORDER);
		spinnerPagesWide.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerPagesWide.setMinimum(1);
		spinnerPagesWide.setSelection(1);
		spinnerPagesWide.setBounds(106, 44, 47, 22);
		
		spinnerPercentNormalSize = new Spinner(grpScaling, SWT.BORDER);
		spinnerPercentNormalSize.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerPercentNormalSize.setMaximum(400);
		spinnerPercentNormalSize.setMinimum(10);
		spinnerPercentNormalSize.setSelection(100);
		spinnerPercentNormalSize.setBounds(106, 16, 47, 22);
		
		Label lblNewLabel = new Label(grpScaling, SWT.NONE);
		lblNewLabel.setBounds(159, 50, 90, 15);
		lblNewLabel.setText("page(s) wide by");
		
		spinnerPagesTall = new Spinner(grpScaling, SWT.BORDER);
		spinnerPagesTall.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerPagesTall.setMinimum(1);
		spinnerPagesTall.setSelection(1);
		spinnerPagesTall.setBounds(255, 44, 47, 22);
		
		Label lblTall = new Label(grpScaling, SWT.NONE);
		lblTall.setBounds(308, 51, 55, 15);
		lblTall.setText("tall");
		
		Label lblNormalSize = new Label(grpScaling, SWT.NONE);
		lblNormalSize.setBounds(159, 23, 104, 15);
		lblNormalSize.setText("% normal size");
		
		Label label = new Label(grpScaling, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(10, 72, 427, 14);
		
		Label lblNewLabel_1 = new Label(grpScaling, SWT.NONE);
		lblNewLabel_1.setBounds(10, 98, 64, 15);
		lblNewLabel_1.setText("Paper size:");
		
		Label lblNewLabel_2 = new Label(grpScaling, SWT.NONE);
		lblNewLabel_2.setBounds(10, 132, 104, 15);
		lblNewLabel_2.setText("First page number:");
		
		comboPaperSize = new Combo(grpScaling, SWT.READ_ONLY);
		comboPaperSize.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		comboPaperSize.setItems(new String[] {"Letter", "Tabloid", "Legal", "Executive", "A3", "A4", "B4(JIS)", "B5(JIS)", "Envelope #10", "Envelope Monarch"});
		comboPaperSize.setBounds(117, 95, 320, 23);
		comboPaperSize.select(0);
		
		txtFirstPageNumber = new Text(grpScaling, SWT.BORDER);
		txtFirstPageNumber.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		txtFirstPageNumber.setText("Auto");
		txtFirstPageNumber.setBounds(120, 129, 55, 21);
			
		//Margins Tab
		TabItem tbtmMargins = new TabItem(tabFolder, SWT.NONE);
		tbtmMargins.setText("Margins");
		
		Composite compositeTabMargins = new Composite(tabFolder, SWT.NONE);
		tbtmMargins.setControl(compositeTabMargins);
		
		Group grpCenterOnPage = new Group(compositeTabMargins, SWT.NONE);
		grpCenterOnPage.setText("Center on page");
		grpCenterOnPage.setBounds(219, 10, 228, 93);
		
		btnHorizontally = new Button(grpCenterOnPage, SWT.CHECK);
		btnHorizontally.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnHorizontally.setBounds(10, 25, 93, 16);
		btnHorizontally.setText("Horizontally");
		
		btnVertically = new Button(grpCenterOnPage, SWT.CHECK);
		btnVertically.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnVertically.setBounds(10, 47, 93, 16);
		btnVertically.setText("Vertically");
		
		Group grpMargins = new Group(compositeTabMargins, SWT.NONE);
		grpMargins.setText("Margins");
		grpMargins.setBounds(10, 10, 203, 269);
		
		Label lblNewLabel_3 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_3.setBounds(10, 32, 55, 15);
		lblNewLabel_3.setText("Header:");
		
		spinnerHeader = new Spinner(grpMargins, SWT.BORDER);
		spinnerHeader.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerHeader.setTextLimit(10000);
		spinnerHeader.setPageIncrement(25);
		spinnerHeader.setIncrement(25);
		spinnerHeader.setMaximum(10000);
		spinnerHeader.setDigits(2);
		spinnerHeader.setSelection(30);
		spinnerHeader.setBounds(80, 25, 74, 22);
		
		Label lblNewLabel_4 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_4.setBounds(10, 63, 55, 15);
		lblNewLabel_4.setText("Top:");
		
		spinnerTop = new Spinner(grpMargins, SWT.BORDER);
		spinnerTop.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerTop.setTextLimit(10000);
		spinnerTop.setPageIncrement(25);
		spinnerTop.setIncrement(25);
		spinnerTop.setMaximum(10000);
		spinnerTop.setDigits(2);
		spinnerTop.setSelection(75);
		spinnerTop.setBounds(80, 60, 74, 22);
		
		Label lblNewLabel_5 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_5.setBounds(10, 99, 55, 15);
		lblNewLabel_5.setText("Left:");
		
		spinnerLeft = new Spinner(grpMargins, SWT.BORDER);
		spinnerLeft.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerLeft.setTextLimit(10000);
		spinnerLeft.setPageIncrement(25);
		spinnerLeft.setIncrement(25);
		spinnerLeft.setMaximum(10000);
		spinnerLeft.setDigits(2);
		spinnerLeft.setSelection(70);
		spinnerLeft.setBounds(80, 92, 74, 22);
		
		Label lblNewLabel_6 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_6.setBounds(10, 134, 55, 15);
		lblNewLabel_6.setText("Right:");
		
		spinnerRight = new Spinner(grpMargins, SWT.BORDER);
		spinnerRight.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerRight.setTextLimit(10000);
		spinnerRight.setPageIncrement(25);
		spinnerRight.setIncrement(25);
		spinnerRight.setMaximum(10000);
		spinnerRight.setDigits(2);
		spinnerRight.setSelection(70);
		spinnerRight.setBounds(80, 127, 74, 22);
		
		Label lblNewLabel_7 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_7.setBounds(10, 168, 55, 15);
		lblNewLabel_7.setText("Bottom:");
		
		spinnerBottom = new Spinner(grpMargins, SWT.BORDER);
		spinnerBottom.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerBottom.setTextLimit(10000);
		spinnerBottom.setPageIncrement(25);
		spinnerBottom.setIncrement(25);
		spinnerBottom.setMaximum(10000);
		spinnerBottom.setDigits(2);
		spinnerBottom.setSelection(75);
		spinnerBottom.setBounds(80, 161, 74, 22);
		
		Label lblNewLabel_8 = new Label(grpMargins, SWT.NONE);
		lblNewLabel_8.setBounds(10, 205, 55, 15);
		lblNewLabel_8.setText("Footer:");
		
		spinnerFooter = new Spinner(grpMargins, SWT.BORDER);
		spinnerFooter.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		spinnerFooter.setTextLimit(10000);
		spinnerFooter.setPageIncrement(25);
		spinnerFooter.setIncrement(25);
		spinnerFooter.setMaximum(10000);
		spinnerFooter.setDigits(2);
		spinnerFooter.setSelection(30);
		spinnerFooter.setBounds(80, 198, 74, 22);
		
		//Header/Footer Tab
		TabItem tbtmHeader_Footer = new TabItem(tabFolder, SWT.NONE);
		tbtmHeader_Footer.setText("Header/Footer");
		
		Composite compositeTabHeaderFooter = new Composite(tabFolder, SWT.NONE);
		tbtmHeader_Footer.setControl(compositeTabHeaderFooter);
		
		textHeader = new Text(compositeTabHeaderFooter, SWT.BORDER | SWT.MULTI);
		textHeader.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textHeader.setBounds(10, 31, 437, 52);
		
		Label lblHeader = new Label(compositeTabHeaderFooter, SWT.NONE);
		lblHeader.setBounds(10, 10, 55, 15);
		lblHeader.setText("Header");
		
		textFooter = new Text(compositeTabHeaderFooter, SWT.BORDER | SWT.MULTI);
		textFooter.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textFooter.setBounds(10, 124, 437, 52);
		
		Label lblFooter = new Label(compositeTabHeaderFooter, SWT.NONE);
		lblFooter.setText("Footer");
		lblFooter.setBounds(10, 103, 55, 15);
		
		btnDiffOddAndEvenPages = new Button(compositeTabHeaderFooter, SWT.CHECK);
		btnDiffOddAndEvenPages.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnDiffOddAndEvenPages.setBounds(10, 195, 202, 16);
		btnDiffOddAndEvenPages.setText("Different odd and even pages");
		
		btnDiffFirstPage = new Button(compositeTabHeaderFooter, SWT.CHECK);
		btnDiffFirstPage.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnDiffFirstPage.setBounds(10, 217, 156, 16);
		btnDiffFirstPage.setText("Different first page");
		
		btnScaleWithDocument = new Button(compositeTabHeaderFooter, SWT.CHECK);
		btnScaleWithDocument.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnScaleWithDocument.setSelection(true);
		btnScaleWithDocument.setBounds(10, 239, 156, 16);
		btnScaleWithDocument.setText("Scale with document");
		
		btnAlignWithPageMargins = new Button(compositeTabHeaderFooter, SWT.CHECK);
		btnAlignWithPageMargins.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnAlignWithPageMargins.setSelection(true);
		btnAlignWithPageMargins.setBounds(10, 261, 189, 16);
		btnAlignWithPageMargins.setText("Align with page margins");
		
		//sheet tab
		TabItem tabSheet = new TabItem(tabFolder, SWT.NONE);
		tabSheet.setText("Sheet");
		
		Composite compositeTabSheet = new Composite(tabFolder, SWT.NONE);
		tabSheet.setControl(compositeTabSheet);
		
		Label lblPrint = new Label(compositeTabSheet, SWT.NONE);
		lblPrint.setBounds(10, 154, 35, 15);
		lblPrint.setText("Print");
		
		Label label_2 = new Label(compositeTabSheet, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_2.setBounds(41, 162, 410, 2);
		
		btnGridlines = new Button(compositeTabSheet, SWT.CHECK);
		btnGridlines.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnGridlines.setBounds(10, 177, 93, 16);
		btnGridlines.setText("Gridlines");
		
		btnBlackAndWhite = new Button(compositeTabSheet, SWT.CHECK);
		btnBlackAndWhite.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnBlackAndWhite.setBounds(10, 199, 143, 16);
		btnBlackAndWhite.setText("Black and white");
		
		btnDraftQuality = new Button(compositeTabSheet, SWT.CHECK);
		btnDraftQuality.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnDraftQuality.setBounds(10, 221, 158, 16);
		btnDraftQuality.setText("Draft quality");
		
		btnRowAndColumnHeadings = new Button(compositeTabSheet, SWT.CHECK);
		btnRowAndColumnHeadings.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnRowAndColumnHeadings.setBounds(10, 243, 183, 16);
		btnRowAndColumnHeadings.setText("Row and column headings");
		
		Label lblComments = new Label(compositeTabSheet, SWT.NONE);
		lblComments.setBounds(190, 178, 55, 15);
		lblComments.setText("Comments: ");
		
		Label lblCellErrorsAs = new Label(compositeTabSheet, SWT.NONE);
		lblCellErrorsAs.setBounds(190, 210, 70, 15);
		lblCellErrorsAs.setText("Cell errors as:");
		
		comboCellErrors = new Combo(compositeTabSheet, SWT.READ_ONLY);
		comboCellErrors.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		comboCellErrors.setItems(new String[] {"displayed", "<blank>", "--", "N/A"});
		comboCellErrors.setBounds(266, 214, 163, 23);
		comboCellErrors.select(0);
		
		comboComment = new Combo(compositeTabSheet, SWT.READ_ONLY);
		comboComment.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		comboComment.setItems(new String[] {"(None)", "At end of sheet", "As displayed on sheet"});
		comboComment.setBounds(266, 175, 163, 23);
		comboComment.select(0);
		
		Group grpPageOrder = new Group(compositeTabSheet, SWT.NONE);
		grpPageOrder.setText("Page order");
		grpPageOrder.setBounds(8, 276, 443, 62);
		
		btnDownThenOver = new Button(grpPageOrder, SWT.RADIO);
		btnDownThenOver.setBounds(10, 28, 108, 16);
		btnDownThenOver.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnDownThenOver.setSelection(true);
		btnDownThenOver.setText("Down, then over");
		
		btnOverThenDown = new Button(grpPageOrder, SWT.RADIO);
		btnOverThenDown.setBounds(160, 28, 139, 16);
		btnOverThenDown.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		btnOverThenDown.setText("Over, then down");
		
		Label label_1 = new Label(compositeTabSheet, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setBounds(72, 78, 379, 2);
		
		Label lblPrintTitles = new Label(compositeTabSheet, SWT.NONE);
		lblPrintTitles.setText("Print titles");
		lblPrintTitles.setBounds(12, 70, 65, 15);
		
		Label lblNewLabel_9 = new Label(compositeTabSheet, SWT.NONE);
		lblNewLabel_9.setBounds(10, 89, 132, 15);
		lblNewLabel_9.setText("Rows to repeat at top:");
		
		Label lblNewLabel_10 = new Label(compositeTabSheet, SWT.NONE);
		lblNewLabel_10.setBounds(10, 118, 143, 15);
		lblNewLabel_10.setText("Columns to repeat at left:");
		
		comboColsToRepeatAtLeft = new Combo(compositeTabSheet, SWT.READ_ONLY);
		comboColsToRepeatAtLeft.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		comboColsToRepeatAtLeft.setItems(new String[] {"Pace Row Headings", "None"});
		comboColsToRepeatAtLeft.setBounds(159, 115, 282, 23);
		comboColsToRepeatAtLeft.select(0);
		
		comboRowsToRepeatAtTop = new Combo(compositeTabSheet, SWT.READ_ONLY);
		comboRowsToRepeatAtTop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		comboRowsToRepeatAtTop.setItems(new String[] {"Pace Column Headings", "None"});
		comboRowsToRepeatAtTop.setBounds(159, 86, 282, 23);
		comboRowsToRepeatAtTop.select(0);
		
		Group grpPrintArea = new Group(compositeTabSheet, SWT.NONE);
		grpPrintArea.setText("Print area");
		grpPrintArea.setBounds(10, 10, 441, 54);
		
		btnUserSelection = new Button(grpPrintArea, SWT.RADIO);
		btnUserSelection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
				if( btnUserSelection.isEnabled() )
					txtUserSelection.setEnabled(true);
			}
		});
		btnUserSelection.setText("User selection");
		btnUserSelection.setBounds(109, 21, 96, 16);
		
		btnEntireSheet = new Button(grpPrintArea, SWT.RADIO);
		btnEntireSheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
				if( btnEntireSheet.isEnabled() )
					txtUserSelection.setEnabled(false);
			}
		});
		btnEntireSheet.setText("Entire view");
		btnEntireSheet.setSelection(true);
		btnEntireSheet.setBounds(20, 21, 183, 16);
		
		txtUserSelection = new Text(grpPrintArea, SWT.BORDER);
		txtUserSelection.setEnabled(false);
		txtUserSelection.setBounds(210, 21, 96, 21);

//		EditorUtils.closeEditor(this, PrintStyleEditor.class);
		
		populateControlValues();
		
		if( printStyleInput.isNew() || printStyleInput.isCopy() ) 
			isDirty = true;
		else
			isDirty = false;
	}

	/**
	 * Populates the control with the default/startup values.
	 */
	/**
	 * 
	 */
	private void populateControlValues(){
		//Page Tab
		EditorControlUtil.setText(this.txtPrintStyle, printStyleInput.getPrintStyleName());
		if( ! printStyleInput.isPortrait() && ! printStyleInput.isLandscape() )
			printStyleInput.setPortrait(true);
		EditorControlUtil.checkButton(this.btnPortrait, printStyleInput.isPortrait());
		if( printStyleInput.isPortrait() && printStyleInput.isLandscape() )
			printStyleInput.setLandscape(false);
		EditorControlUtil.checkButton(this.btnLandscape, printStyleInput.isLandscape());
		if( ! printStyleInput.isAdjustTo() && ! printStyleInput.isFitTo() )
			printStyleInput.setAdjustTo(true);
		EditorControlUtil.checkButton(this.btnAdjustTo, printStyleInput.isAdjustTo());
		spinnerPercentNormalSize.setSelection(this.printStyleInput.getPercentNormalSize());
		if( printStyleInput.isAdjustTo() && printStyleInput.isFitTo() )
			printStyleInput.setFitTo(false);
		EditorControlUtil.checkButton(this.btnFitTo, printStyleInput.isFitTo());
		spinnerPagesWide.setSelection(this.printStyleInput.getPageWide());
		spinnerPagesTall.setSelection(this.printStyleInput.getPageTall());
		EditorControlUtil.selectItem(this.comboPaperSize, printStyleInput.getPaperSize());
		EditorControlUtil.setText(this.txtFirstPageNumber, printStyleInput.getFirstPageNumber());
		//Margin Tab
		spinnerTop.setSelection((int)(this.printStyleInput.getTop()*100));
		spinnerHeader.setSelection((int)(this.printStyleInput.getHeader()*100));
		spinnerLeft.setSelection((int)(this.printStyleInput.getLeft()*100));
		spinnerRight.setSelection((int)(this.printStyleInput.getRight()*100));
		spinnerBottom.setSelection((int)(this.printStyleInput.getBottom()*100));
		spinnerFooter.setSelection((int)(this.printStyleInput.getFooter()*100));
		EditorControlUtil.checkButton(this.btnHorizontally, printStyleInput.getCenterHorizontally());
		EditorControlUtil.checkButton(this.btnVertically, printStyleInput.getCenterVertically());
		//Header/Footer Tab
		EditorControlUtil.setText(this.textHeader, printStyleInput.getHeaderText());
		EditorControlUtil.setText(this.textFooter, printStyleInput.getFooterText());
		EditorControlUtil.checkButton(this.btnDiffOddAndEvenPages, printStyleInput.getDiffOddAndEvenPages());
		EditorControlUtil.checkButton(this.btnDiffFirstPage, printStyleInput.getDiffFirstPage());
		EditorControlUtil.checkButton(this.btnScaleWithDocument, printStyleInput.getScaleWithDocument());
		EditorControlUtil.checkButton(this.btnAlignWithPageMargins, printStyleInput.getAlignWithPageMargin());
		//Sheet Tab
		if( ! printStyleInput.getEntireSheet() && ! printStyleInput.getUserSelection() )
			printStyleInput.setEntireSheet(true);
		EditorControlUtil.checkButton(this.btnEntireSheet, printStyleInput.getEntireSheet());
		if( printStyleInput.getEntireSheet() && printStyleInput.getUserSelection() )
			printStyleInput.setUserSelection(false);
		EditorControlUtil.checkButton(this.btnUserSelection, printStyleInput.getUserSelection());
		EditorControlUtil.setText(this.txtUserSelection, printStyleInput.getUserSelectionText());
		if(printStyleInput.getUserSelection()==true)
			txtUserSelection.setEnabled(true);
		else
			txtUserSelection.setEnabled(false);
		EditorControlUtil.checkButton(this.btnGridlines, printStyleInput.getGridlines());
		EditorControlUtil.checkButton(this.btnBlackAndWhite, printStyleInput.getBlackAndWhite());
		EditorControlUtil.checkButton(this.btnDraftQuality, printStyleInput.getDraftQuality());
		EditorControlUtil.checkButton(this.btnRowAndColumnHeadings, printStyleInput.getRowAndColHeadings());
		comboComment.setText(this.printStyleInput.getComment());
		comboCellErrors.setText(this.printStyleInput.getCellErrorsAs());
		if( ! printStyleInput.getDownThenOver() && ! printStyleInput.getOverThenDown() )
			printStyleInput.setDownThenOver(true);
		EditorControlUtil.checkButton(this.btnDownThenOver, printStyleInput.getDownThenOver());
		if( printStyleInput.getDownThenOver() && printStyleInput.getOverThenDown() )
			printStyleInput.setOverThenDown(false);
		EditorControlUtil.checkButton(this.btnOverThenDown, printStyleInput.getOverThenDown());
	}
	
	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		if( ! printStyleInput.isNew() && ! printStyleInput.isCopy()) {
			spinnerPercentNormalSize.setFocus();
		}
		else {
			txtPrintStyle.setFocus();
		}
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

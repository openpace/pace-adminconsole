/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.LinkedList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.input.ViewGroupEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.TempNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.util.TreeUtil;
import com.pace.base.view.PafViewGroup;
import com.pace.base.view.PafViewGroupItem;

public class ViewGroupEditor extends EditorPart {

	private Tree availableViewsAndViewGroups;
	public static String ID = "com.pace.admin.menu.editors.ViewGroupEditor";
	
	private Tree builtViewsAndViewGroups;
	
	private Text viewGroupNameText;
	
	private Button rightArrowButton;
	
	private Button leftArrowButton;
	
	private Button upArrowButton;
	
	private Button downArrowButton;
	
	private TreeViewer viewer;
	
	private TreeNode selectedNode;
	
	private ViewGroupEditorInput viewGroupEditorInput;
	
	private boolean isDirty;
	
	private final static int UP_BUTTON_ID = -1;
	
	private final static int DOWN_BUTTON_ID = 1;
	private Label infoMessage = null;
	
	/**
	 * Constructor.
	 */
	public ViewGroupEditor() {
		super();
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {

		if ( isDirty() ) {
			
			String viewGroupName = viewGroupNameText.getText().trim();
			
			if(viewGroupEditorInput.isNew() && ( viewGroupName.length() == 0 || ! viewGroupEditorInput.validViewGroupName(viewGroupName))){

				GUIUtil.openMessageWindow(this.getTitle(),
		    			"'" + viewGroupName + "' is not a valid View Group name because it's already a View or View Group name or the name is blank.", MessageDialog.WARNING);
				
				monitor.setCanceled(true);
				
				return;
			}
			
			
			TreeItem[] treeItems = builtViewsAndViewGroups.getItems();		
			
			ArrayList<String> viewAndViewGroupNamesList = new ArrayList<String>();
			
			for (TreeItem treeItem : treeItems ) {
				
				viewAndViewGroupNamesList.add(treeItem.getText());
				
			}
			
			if(viewGroupEditorInput.isNew()) {
				viewGroupEditorInput.setKey(viewGroupName);
			}
			
			viewGroupEditorInput.setViewGroupItems(viewAndViewGroupNamesList.toArray(new String[0]));
			viewGroupEditorInput.save();

			
			//set the dirty flag, to clean.
			isDirty = false;
			//the the dirty flag change property.
			firePropertyChange(IEditorPart.PROP_DIRTY);
			
			//update the tab name.
			setPartName(Constants.MENU_VIEW_GROUP + ": " + viewGroupEditorInput.getName());
			
			updateTreeViewer();

			viewGroupNameText.setEditable(false);
		
		}

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(Constants.MENU_VIEW_GROUP + ": " + input.getName());

		if ( input instanceof ViewGroupEditorInput ) {
			
			viewGroupEditorInput = (ViewGroupEditorInput) input;
			
		}
		
		isDirty = false;
	}

	@Override
	public boolean isDirty() {
		
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		
		return false;
		
	}

	@Override
	public void createPartControl(Composite parent) {
		GridData gridData1 = new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1);
		Composite container = new Composite(parent, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		container.setLayout(gridLayout);

		final Label viewGroupNameLabel = new Label(container, SWT.NONE);
		viewGroupNameLabel.setText("View Group Name");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		viewGroupNameText = new Text(container, SWT.BORDER);
		
		viewGroupNameText.setEditable(false);
		
		if ( viewGroupEditorInput != null ) {
			
			viewGroupNameText.setEditable(viewGroupEditorInput.isNew());				
			
		}

		viewGroupNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		final Label viewsLabel = new Label(container, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData.widthHint = 140;
		viewsLabel.setLayoutData(gridData);
		viewsLabel.setText("Available Views");
		new Label(container, SWT.NONE);

		final Label selectedViewsLabel = new Label(container, SWT.NONE);
		selectedViewsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
		selectedViewsLabel.setText("Selected Views/View Groups");
		new Label(container, SWT.NONE);

		availableViewsAndViewGroups = new Tree(container, SWT.MULTI | SWT.BORDER);
		availableViewsAndViewGroups.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite composite = new Composite(container, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.LEFT, SWT.CENTER, false, true);
		gridData_2.widthHint = 26;
		gridData_2.heightHint = 56;
		composite.setLayoutData(gridData_2);
		composite.setLayout(new GridLayout());

		rightArrowButton = new Button(composite, SWT.ARROW | SWT.RIGHT);
		rightArrowButton.setEnabled(false);

		leftArrowButton = new Button(composite, SWT.ARROW | SWT.LEFT);
		leftArrowButton.setEnabled(false);

		builtViewsAndViewGroups = new Tree(container, SWT.MULTI | SWT.BORDER);
		builtViewsAndViewGroups.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite composite_1 = new Composite(container, SWT.NONE);
		infoMessage = new Label(container, SWT.WRAP);
		infoMessage.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		infoMessage.setLayoutData(gridData1);
		final GridData gridData_5 = new GridData(SWT.DEFAULT, 61);
		composite_1.setLayoutData(gridData_5);
		composite_1.setLayout(new GridLayout());

		upArrowButton = new Button(composite_1, SWT.ARROW);
		upArrowButton.setEnabled(false);
		upArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				directionalButtonPressed(builtViewsAndViewGroups, UP_BUTTON_ID);
				builtViewsAndViewGroupsSelectionChanged();
				editorModified();
			}
		});

		downArrowButton = new Button(composite_1, SWT.ARROW | SWT.DOWN);
		downArrowButton.setEnabled(false);
		downArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				directionalButtonPressed(builtViewsAndViewGroups, DOWN_BUTTON_ID);
				builtViewsAndViewGroupsSelectionChanged();
				editorModified();
			}
		});
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
//		EditorUtils.closeEditor(this, ViewGroupEditor.class);
		populateControlValues();
		addListeners();
	}


	private void populateControlValues() {

		EditorControlUtil.setText(viewGroupNameText, viewGroupEditorInput.getName());
		
		TreeItem views = new TreeItem(this.availableViewsAndViewGroups, SWT.NONE);
		views.setText("Views");
		for(String child : viewGroupEditorInput.getViewNames()){
			EditorControlUtil.addTreeNode(availableViewsAndViewGroups, views, child);
		}
		
		//Build the "Views" section of the tree.
		TreeItem viewGroups = new TreeItem(this.availableViewsAndViewGroups, SWT.NONE);
		viewGroups.setText("View Groups");
		for(String child : viewGroupEditorInput.getViewGroupNames()){
			EditorControlUtil.addTreeNode(this.availableViewsAndViewGroups, viewGroups, child);
		}
		

		if ( ! viewGroupEditorInput.isNew() ) {
				
			buildTreeItems(builtViewsAndViewGroups, builtViewsAndViewGroups.getParentItem(), viewGroupEditorInput.getName());
			
		}
		
	}


	private void addListeners() {
		
		viewGroupNameText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				
				editorModified();
				
			}
			
		});
				
		availableViewsAndViewGroups.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				
				updateRightButtonStatus();
				
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {

				TreeUtil.doubleClickSelectionAction(availableViewsAndViewGroups);

			}
			
		});
		
		builtViewsAndViewGroups.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
								
				builtViewsAndViewGroupsSelectionChanged();
				
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {

				TreeUtil.doubleClickSelectionAction(builtViewsAndViewGroups);

			}
		});
		
		rightArrowButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				TreeItem[] selectedItems = availableViewsAndViewGroups.getSelection();
				
				ArrayList<TreeItem> newTreeItems = new ArrayList<TreeItem>();
				
				for (TreeItem selectedItem : selectedItems ) {
					
					TreeItem newTreeItem = EditorControlUtil.addTreeNode(builtViewsAndViewGroups, null, selectedItem.getText());
					
					PafViewGroup pafViewGroup = viewGroupEditorInput.getViewGroup(newTreeItem.getText());
					
					if ( pafViewGroup != null ) {
						
						buildTreeItems(builtViewsAndViewGroups, newTreeItem, newTreeItem.getText());
						
					}
					
					newTreeItems.add(newTreeItem);
									
				}
				
				builtViewsAndViewGroups.setSelection(newTreeItems.toArray(new TreeItem[0]));
				builtViewsAndViewGroups.setFocus();
				builtViewsAndViewGroupsSelectionChanged();
				availableViewsAndViewGroups.deselectAll();
				updateRightButtonStatus();
				editorModified();
				
			}
			
		});
		
		leftArrowButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				TreeItem[] selectedItems = builtViewsAndViewGroups.getSelection();
				
				for ( TreeItem selectedItem : selectedItems ) {
					
					selectedItem.dispose();
					
				}
				
				/* TTN-671
				if ( builtViewsAndViewGroups.getTopItem() != null ) {
					
					builtViewsAndViewGroups.setSelection(builtViewsAndViewGroups.getTopItem());
					builtViewsAndViewGroups.setFocus();
					
				}
				*/
				
				builtViewsAndViewGroupsSelectionChanged();
				
				editorModified();
			}
			
		}); 
		
		
	}
	
	protected void updateRightButtonStatus() {

		TreeItem[] selectedItems = availableViewsAndViewGroups.getSelection();
		
		boolean invalidSelection = false;
		
		infoMessage.setText("");
		
		if ( selectedItems.length == 0 ) {
			
			invalidSelection = true;
			
			infoMessage.setText("");
			
		} else {
		
			for (TreeItem selectedItem : selectedItems) {
				
				
				//if parent or current view group name
				if ( selectedItem.getParentItem() == null || selectedItem.getText().equalsIgnoreCase(viewGroupEditorInput.getName())) {
					
					invalidSelection = true;
					
					if ( selectedItem.getText().equalsIgnoreCase(viewGroupEditorInput.getName()) ) {
						infoMessage.setText("You cannot add a view group to itself.");
					}
					
					break;
				}
				
				PafViewGroup viewGroup = viewGroupEditorInput.getViewGroup(selectedItem.getText());
				
				if (viewGroup != null) {
					
					LinkedList<PafViewGroup> viewGroups = new LinkedList<PafViewGroup>();
					viewGroups.addLast(viewGroup);
					while(!viewGroups.isEmpty()) {

						PafViewGroup currentViewGroup = viewGroups.removeFirst();
						
						PafViewGroupItem [] viewGroupItems = currentViewGroup.getPafViewGroupItems();
						
						for (PafViewGroupItem item : viewGroupItems) {
							
							if(item.isViewGroup()){
								if(item.getName().equalsIgnoreCase(viewGroupEditorInput.getName())){
									
									invalidSelection = true;
									infoMessage.setText("The view group you are editing is contained in the view group you are trying to add. You cannot do that, since that would create a circular reference.");
									break;
									
								}
								/*else{
									
									viewGroups.add(viewGroupEditorInput.getViewGroup(item.getName()));
									
								}*/
							}
						}				
					}
				}
			}
		
		}
		
		rightArrowButton.setEnabled(! invalidSelection);
		
	}

	protected void builtViewsAndViewGroupsSelectionChanged() {
		
		updateLeftButtonStatus();
		updateUpButtonStatus();
		updateDownButtonStatus();
		
	}	

	protected void updateDownButtonStatus() {
		
		TreeItem[] selectedItems = builtViewsAndViewGroups.getSelection();
		
		boolean invalidDownButtonSelection = false;
		
		if ( selectedItems.length == 1 && selectedItems[0].getParentItem() == null) {
			
			TreeItem selectedItem = selectedItems[0];
			
			int selectedItemNdx = getIndexWithinParent(selectedItem);
			
			int topLevelTreeItemCnt = builtViewsAndViewGroups.getItems().length - 1;
		
			if ( selectedItemNdx == topLevelTreeItemCnt ) {
				
				invalidDownButtonSelection = true;
				
			}
			
		} else {
			
			invalidDownButtonSelection = true;
			
		}
		
		downArrowButton.setEnabled(! invalidDownButtonSelection);
		
	}

	protected void updateUpButtonStatus() {
		
		TreeItem[] selectedItems = builtViewsAndViewGroups.getSelection();
		
		boolean invalidUpButtonSelection = false;
		
		if ( selectedItems.length == 1 && selectedItems[0].getParentItem() == null) {
			
			TreeItem selectedItem = selectedItems[0];
			
			int selectedItemNdx = getIndexWithinParent(selectedItem);
			
			if ( selectedItemNdx == 0 ) {
				
				invalidUpButtonSelection = true;
				
			}
					
		} else {
			
			invalidUpButtonSelection = true;
		
			
		}
		
		upArrowButton.setEnabled(! invalidUpButtonSelection);
				
	}

	protected void updateLeftButtonStatus() {
		
		TreeItem[] selectedItems = builtViewsAndViewGroups.getSelection();
		
		boolean invalidLeftButtonSelection = false;
		
		if ( selectedItems.length == 0 ) {
			
			invalidLeftButtonSelection = true;
			
		} else {
		
			for (TreeItem selectedItem : selectedItems) {
				
				TreeItem parentItem = selectedItem.getParentItem();
				
				if ( parentItem != null ) {
					invalidLeftButtonSelection = true;
					break;
				} 
				
			}
			
		}
		
		leftArrowButton.setEnabled(! invalidLeftButtonSelection);			
		
	}

	private void buildTreeItems(Tree tree, TreeItem parentItem, String viewGroupName) {
		
		if ( tree != null && ! tree.isDisposed() ) {
			
			PafViewGroup viewGroup  = viewGroupEditorInput.getViewGroup(viewGroupName);
			
			if ( viewGroup != null && viewGroup.getPafViewGroupItems() != null && viewGroup.getPafViewGroupItems().length > 0 ) {
			
				for (PafViewGroupItem viewGroupItem : viewGroup.getPafViewGroupItems() ) {

					TreeItem viewGroupNode = EditorControlUtil.addTreeNode(tree, parentItem, viewGroupItem.getName());
					
					if ( viewGroupItem.isViewGroup() ) {
						
						buildTreeItems(tree, viewGroupNode, viewGroupItem.getName());
						
						//viewGroupNode.setExpanded(true);
												
					} 
					
				}
				
			}
			
		}
		
	}

	@Override
	public void setFocus() {

		if ( viewGroupNameText != null && ! viewGroupNameText.isDisposed()) {
			viewGroupNameText.setFocus();
		}

	}

	/**
	 * @return Returns the selectedNode.
	 */
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	/**
	 * @param selectedNode The selectedNode to set.
	 */
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	/**
	 * @return Returns the viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * @param viewer The viewer to set.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty) {
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	      }
	}
	
	/**
	 * Event hander for when a directional button is pushed.
	 * @param control List control that is associated to the directional buttons.
	 * @param buttonId Id of the button that was pushed.
	 */
	private void directionalButtonPressed(Tree tree, int buttonId) {

		TreeItem[] selectedTreeItems = tree.getSelection();
		
		int selectionIndex = 0;

		for (TreeItem item : tree.getItems()) {

			if (item.equals(selectedTreeItems[0])) {
				break;
			}
			selectionIndex++;
		}

		ArrayList<TempNode> tempNodes = new ArrayList<TempNode>();

		for (TreeItem childItem : tree.getItems()) {

			TempNode tempNode = new TempNode(null, childItem);

			convertTreeItemToNodes(childItem, tempNode);

			tempNodes.add(tempNode);

		}

		TempNode switchNode = tempNodes.get(selectionIndex + buttonId);
		TempNode selectedNode = tempNodes.get(selectionIndex);

		if (buttonId == UP_BUTTON_ID) {
			tempNodes.remove(selectionIndex + buttonId);
			tempNodes.remove(selectionIndex + buttonId);
			tempNodes.add(selectionIndex + buttonId, selectedNode);
			tempNodes.add(selectionIndex, switchNode);
		} else if (buttonId == DOWN_BUTTON_ID) {
			tempNodes.remove(selectionIndex);
			tempNodes.remove(selectionIndex);
			tempNodes.add(selectionIndex, switchNode);
			tempNodes.add(selectionIndex + buttonId, selectedNode);
		}

		tree.removeAll();

		for (TempNode currentNode : tempNodes) {
			populateTreeFromNode(tree, currentNode);
		}

		TreeItem newSelection = tree
				.getItem(selectionIndex + buttonId);

		tree.setSelection(new TreeItem[] { newSelection });
		tree.setFocus();

		
	}
	
	private int getIndexWithinParent(TreeItem selection) {

		TreeItem[] parentItems = null;
		
		if ( selection.getParentItem() == null ) {
			
			parentItems = selection.getParent().getItems();
			
		} else {
			
			parentItems = selection.getParentItem().getItems();
			
		}		

		int index = 0;
		int selectionIndex = 0;

		for (TreeItem item : parentItems) {

			if (item.equals(selection)) {
				selectionIndex = index;
				break;
			}

			index++;
		}

		return selectionIndex;

	}
	
	private void convertTreeItemToNodes(TreeItem item, TempNode node) {

		if (item.getItems() != null && item.getItems().length > 0) {

			for (TreeItem childItem : item.getItems()) {

				TempNode childNode = new TempNode(node, childItem.getText());

				childNode.setExpanded(childItem.getExpanded());
				childNode.setData(childItem.getData());

				convertTreeItemToNodes(childItem, childNode);

			}

		}

	}
	
	private void populateTreeFromNode(Tree tree, TempNode node) {

		TreeItem newItem = new TreeItem(tree, SWT.NONE);
		newItem.setText(node.getText());

		if (node.numberOfChildren() > 0) {

			for (TempNode child : node.getChildren()) {
				populateTreeItemFromNode(newItem, child);
			}
		}

		newItem.setExpanded(node.isExpanded());
		newItem.setData(node.getData());

	}
	
	private void populateTreeItemFromNode(TreeItem item, TempNode node) {

		TreeItem newItem = new TreeItem(item, SWT.NONE);
		newItem.setText(node.getText());

		if (node.numberOfChildren() > 0) {

			for (TempNode child : node.getChildren()) {
				populateTreeItemFromNode(newItem, child);
			}
		}

		newItem.setExpanded(node.isExpanded());
		newItem.setData(node.getData());

	}
	
	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		try{
			if(viewer != null && selectedNode != null){
											
					selectedNode.createChildren(null);
					
					viewer.refresh(selectedNode);
					
				}
									
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}  //  @jve:decl-index=0:visual-constraint="33,3,514,279"

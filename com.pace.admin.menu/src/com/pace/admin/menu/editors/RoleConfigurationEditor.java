/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.GenericAddRemoveDialog;
import com.pace.admin.global.dialogs.GenericPaceTreeDialog;
import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.global.widgets.StringListSelectionDialog;
import com.pace.admin.menu.dialogs.DimensionAttributeSelectorDialog;
import com.pace.admin.menu.dialogs.MeasureFlagIntersectionDialog;
import com.pace.admin.menu.dialogs.ReferenceVersionDialog;
import com.pace.admin.menu.dialogs.VersionFilterDialog;
import com.pace.admin.menu.editors.input.RoleConfigurationInput;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.utility.StringUtils;
import com.pace.server.client.PafSimpleDimTree;
import com.swtdesigner.SWTResourceManager;

public class RoleConfigurationEditor extends EditorPart {

	private Combo allowSuppressInvalidIntersectionsCombo;
	private Combo allowRoleFilterCombo;
	private Combo multiRoleFilterCombo;
	private Combo filteredSubtotalsCombo; 
	/**
	 */
	public static final String ID = "com.pace.admin.menu.editors.roleconfiguration";
	/**
	 */
	public static final String EDITOR_ID = "Role Configuration Editor";

	private static final Logger logger = Logger.getLogger(RoleConfigurationEditor.class);

	//tree
	private Tree availableViewGroupsAndViewsTree;
	
	//list
	private List selectedViewGroupsAndViewsList;
	
	//labels
	private Label selectedViewsLabel;
	private Label largeCellOverrideLabel;
	private Label maximumCellOverrideLabel;
	private Label allowRoleFilterLabel;
	private Label multiRoleFilterLabel;
	private Label allowSuppressInvalidIntersectionsLabel;

	//Text boxes
	private Text allowSuppressInvalidIntersectionsTextBox;
	private Text allowRoleFiltersTextBox;
	private Text autoRunOnSaveMenuTextBox;
	private Text visibleMenuTextBox;
	private Text maxCellLimitTextBox;
	private Text largeCellLimitTextBox;
	private Text versionFilters;
	private Text refVersionsText;
	//tables
	private Table availableRuleSetsCheckbox;
	
	private Combo replicateAllEnabledCombo;
	private Combo replicateEnabledCombo;
	private Combo liftAllEnabledCombo;
	private Combo liftEnabledCombo;
	private Combo defaultRuleSet;
	private Combo planCycle;
	private Combo roleName;
	
	private CheckboxTableViewer availableRuleSetsCheckboxViewer;
	
	//buttons
	private Button allowRoleFilterEllipseButton;
	private Button allowSuppressInvalidIntersectionsEllipseButton;
	private Button defaultEvalEnabledPlanVersionButton;
	private Button mdbSaveOnUowLoadButton;
	
	private boolean isDirty;
	private RoleConfigurationInput roleConfigInput;
	private TreeViewer viewer;
	private RoleConfigurationsNode roleConfigurationsNode;
	private ArrayList<String> versionFilterList;
	private ArrayList<String> refVersionList;
	//composites
//	private Composite container;
	private Composite scrolledChildComposite;
	private ScrolledComposite scrolledComposite;
	
	private static Map<String, Boolean> dropdownBooleanMap = new LinkedHashMap<String, Boolean>();
	
	static {
		
		dropdownBooleanMap.put(Constants.DROPDOWN_GLOBAL_OPTION, null);
		dropdownBooleanMap.put(Constants.DROPDOWN_BOOL_TRUE, Boolean.TRUE);
		dropdownBooleanMap.put(Constants.DROPDOWN_BOOL_FALSE, Boolean.FALSE);
		
	}
	
	private String[] trueFalseAr = dropdownBooleanMap.keySet().toArray(new String[0]);
	
	private Label defaultRuleSetsLabel;
	private Text readOnlyMeasuresTextBox;
	private Button modifyRefVersionsButton;
	private Text textAdminLocks;
	private String[] selectedAdminLocks;
	/**
	 * Constructor.
	 */
	public RoleConfigurationEditor() {
		super();
	}
	
	/**
	 * Implementation of EditorPart.doSave.
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		try{
			
			
			String missingField = isRequiredInputValid();
			if(missingField != null){
				GUIUtil.openMessageWindow(
						this.getTitle(),
						"The following fields are required:\n-" + missingField,
						SWT.ICON_INFORMATION);
				
				monitor.setCanceled(true);
				
				return;
			}
			
			String invalidDataField = isInputDataValid();
			
			if ( invalidDataField != null ) {
				
				GUIUtil.openMessageWindow(
						this.getTitle(),
						"The value in the following field is not valid:\n-" + invalidDataField,
						SWT.ICON_INFORMATION);
				
				monitor.setCanceled(true);
				
				return;
				
			}						
			
			if((roleConfigInput.isNew() || roleConfigInput.isCopy()) && roleConfigInput.itemExists(roleName.getText(), planCycle.getText())){
				if (! GUIUtil.askUserAQuestion(
						this.getTitle(),
		    			Constants.MENU_ROLE_CONFIGURATION + ": '" + roleConfigInput.getKey(roleName.getText(), planCycle.getText()) +
							"' already exists.\nReplace existing item?")) {
					
					monitor.setCanceled(true);
					
					return;
				}
			}
			
			//set the role
			roleConfigInput.setRole(roleName.getText().trim());
			//set the plan cycle
			roleConfigInput.setPlanCycle(planCycle.getText().trim());
			
			//if a value exists, convert str to Integer and set, else set to null
			if ( largeCellLimitTextBox.getText().length() > 0 ) {
				
				roleConfigInput.setLargeCellCount(Integer.parseInt(largeCellLimitTextBox.getText()));
				
			} else {
				
				roleConfigInput.setLargeCellCount(null);
				
			}

			//if a value exists, convert str to Integer and set, else set to null
			if ( maxCellLimitTextBox.getText().length() > 0 ) {
				
				roleConfigInput.setMaxCellCount(Integer.parseInt(maxCellLimitTextBox.getText()));
				
			} else {
				
				roleConfigInput.setMaxCellCount(null);
				
			}
			
			roleConfigInput.setSelectedAdminLocks(selectedAdminLocks);
			
			//set default eval plan version eval
			roleConfigInput.setDefaultEvalEnabledWorkingVersion(defaultEvalEnabledPlanVersionButton.getSelection());
			
			roleConfigInput.setMdbSaveWorkingVersionOnUowLoad(mdbSaveOnUowLoadButton.getSelection());
			
			//set teh calc elapsed periods option.
			//roleConfigInput.setCalcElapsedPeriods(calculateElapsedPeriodsButton.getSelection());

			//set the available rule sets;
			String[] selItems = new String[availableRuleSetsCheckboxViewer.getCheckedElements().length];
			for(int i = 0; i < selItems.length; i++){
				selItems[i] = availableRuleSetsCheckboxViewer.getCheckedElements()[i].toString().trim();
			}
			roleConfigInput.setSelectedRuleSets(selItems);
			//set the reference versions.
			roleConfigInput.setRefVersions(refVersionList);
			//set the version filters.
			roleConfigInput.setVersionFilters(versionFilterList);
			//set the default rule set
			roleConfigInput.setDefaultRulesetName(defaultRuleSet.getText().trim());
			//set the selected views/view groups
			roleConfigInput.setSelectedViewAndViewGroups(selectedViewGroupsAndViewsList.getItems());
			
			roleConfigInput.setAllowRoleFilter(dropdownBooleanMap.get(allowRoleFilterCombo.getText()));
			//TTN 1735 - Role Filter Single Select - Design Summary
			roleConfigInput.setUserFilteredMultiSelect(dropdownBooleanMap.get(multiRoleFilterCombo.getText()));
			roleConfigInput.setAllowSuppressInvalidIntersections(dropdownBooleanMap.get(allowSuppressInvalidIntersectionsCombo.getText()));
			roleConfigInput.setFilteredSubtotals(dropdownBooleanMap.get(filteredSubtotalsCombo.getText()));
			roleConfigInput.setReplicateEnabled(dropdownBooleanMap.get(replicateEnabledCombo.getText()));
			roleConfigInput.setReplicateAllEnabled(dropdownBooleanMap.get(replicateAllEnabledCombo.getText()));
			roleConfigInput.setLiftEnabled(dropdownBooleanMap.get(liftEnabledCombo.getText()));
			roleConfigInput.setLiftAllEnabled(dropdownBooleanMap.get(liftAllEnabledCombo.getText()));
			
			//do the save.
			roleConfigInput.save();
			//set the dirty flag, to clean.
			isDirty = false;
			//the the dirty flag change property.
			firePropertyChange(IEditorPart.PROP_DIRTY);
			//update the tab name.
			setPartName(Constants.MENU_ROLE_CONFIGURATION + ": " + this.getEditorInput().getName());
			//Refresh the tree nodes.
			updateTreeViewer();
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
		}
	}

	/**
	 * Implementation of EditorPart.doSaveAs.
	 */
	@Override
	public void doSaveAs() {
		// Not allowed.
	}

	/**
	 * Implementation of EditorPart.init.
	 * @param input
	 * @param site
	 * @throws org.eclipse.ui.PartInitException
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		roleConfigInput = (RoleConfigurationInput) this.getEditorInput();
		isDirty = false;
		setPartName(Constants.MENU_ROLE_CONFIGURATION + ": " + input.getName());
	}

	/**
	 * Implementation of EditorPart.isDirty.
	 * @return boolean
	 */
	@Override
	public boolean isDirty() {
		setPartName(Constants.MENU_ROLE_CONFIGURATION + ": " + roleConfigInput.getKey(roleName.getText(), planCycle.getText()));
		return isDirty;
	}

	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}
	
	/**
	 * Implementation of EditorPart.isSaveAsAllowed.
	 * @return boolean
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	
	@Override
	public void createPartControl(Composite container) {
		//==============================================
//		container = new Composite(arg0, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		container.setLayout(gridLayout);
		//==============================================
		scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL|SWT.RESIZE);
		scrolledComposite.getVerticalBar().setIncrement(20);
		scrolledComposite.setAlwaysShowScrollBars(true);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				resizeForm(e);
			}
		});
		final GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_scrolledComposite.widthHint = 823;
		gd_scrolledComposite.heightHint = 527;
		scrolledComposite.setLayoutData(gd_scrolledComposite);

		scrolledChildComposite = new Composite(scrolledComposite, SWT.NONE);
		scrolledChildComposite.setLocation(0, 0);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 4;
		gridLayout_1.marginWidth = 0;
		scrolledChildComposite.setLayout(gridLayout_1);
		final GridData gd_scrolledChildComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_scrolledChildComposite.widthHint = 80;
		gd_scrolledChildComposite.heightHint = 228;
		scrolledChildComposite.setLayoutData(gd_scrolledChildComposite);

		final Composite roleInformationComposite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData roleInformationGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		roleInformationGridData.widthHint = 823;
		roleInformationGridData.heightHint = 26;
		roleInformationComposite.setLayoutData(roleInformationGridData);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 3;
		gridLayout_2.marginWidth = 0;
		roleInformationComposite.setLayout(gridLayout_2);

		final Label roleInformationCompositeLabel = new Label(roleInformationComposite, SWT.NONE);
		GridData gd_roleInformationCompositeLabel = new GridData(SWT.LEFT, SWT.FILL, false, false);
		gd_roleInformationCompositeLabel.widthHint = 117;
		roleInformationCompositeLabel.setLayoutData(gd_roleInformationCompositeLabel);
		roleInformationCompositeLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		roleInformationCompositeLabel.setText(Constants.MENU_ROLE + " Information");

		final Label roleInformationCompositeSeparator = new Label(roleInformationComposite, SWT.HORIZONTAL | SWT.SEPARATOR);
		final GridData gd_roleInformationCompositeSeparator = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_roleInformationCompositeSeparator.widthHint = 395;
		roleInformationCompositeSeparator.setLayoutData(gd_roleInformationCompositeSeparator);
		roleInformationCompositeSeparator.setText("Label");
		new Label(roleInformationComposite, SWT.NONE);
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite composite_6 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_composite_6 = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_composite_6.widthHint = 443;
		composite_6.setLayoutData(gd_composite_6);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.numColumns = 2;
		composite_6.setLayout(gridLayout_3);

		final Label roleNameLabel = new Label(composite_6, SWT.NONE);
		roleNameLabel.setText("Role");

		roleName = new Combo(composite_6, SWT.READ_ONLY);
		final GridData gd_roleName_1 = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_roleName_1.widthHint = 153;
		roleName.setLayoutData(gd_roleName_1);
		roleName.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite composite_7 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_composite_7 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_composite_7.horizontalSpan = 2;
		gd_composite_7.widthHint = 125;
		composite_7.setLayoutData(gd_composite_7);
		final GridLayout gridLayout_18 = new GridLayout();
		gridLayout_18.numColumns = 2;
		composite_7.setLayout(gridLayout_18);

		final Label planCycleLabel = new Label(composite_7, SWT.NONE);
		planCycleLabel.setText(Constants.MENU_ROLES_PROCESS_PLANCYCLE);

		planCycle = new Combo(composite_7, SWT.READ_ONLY);
		planCycle.select(0);
		final GridData gd_planCycle_1 = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_planCycle_1.widthHint = 114;
		planCycle.setLayoutData(gd_planCycle_1);
		planCycle.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});

		final Composite composite_2 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_composite_2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_composite_2.heightHint = 170;
		gd_composite_2.widthHint = 660;
		composite_2.setLayoutData(gd_composite_2);
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 5;
		composite_2.setLayout(gridLayout_4);
				
		final Label replicateEnabledLabel = new Label(composite_2, SWT.NONE);
		GridData gd_replicateEnabledLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_replicateEnabledLabel.widthHint = 158;
		replicateEnabledLabel.setLayoutData(gd_replicateEnabledLabel);
		replicateEnabledLabel.setText("Replicate Enabled");
		
		replicateEnabledCombo = new Combo(composite_2, SWT.READ_ONLY);
		replicateEnabledCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				editorModified();
			}
		});
		replicateEnabledCombo.select(0);
		replicateEnabledCombo.setItems(trueFalseAr);
		final GridData gd_replicateEnabledCombo_1 = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_replicateEnabledCombo_1.widthHint = 62;
		replicateEnabledCombo.setLayoutData(gd_replicateEnabledCombo_1);
		
		new Label(composite_2, SWT.NONE);
		
		Label lblLiftenabled = new Label(composite_2, SWT.NONE);
		GridData gd_lblLiftenabled = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblLiftenabled.widthHint = 140;
		lblLiftenabled.setLayoutData(gd_lblLiftenabled);
		lblLiftenabled.setText("Lift Enabled");
		
		liftEnabledCombo = new Combo(composite_2, SWT.READ_ONLY);
		GridData gd_liftEnabledCombo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_liftEnabledCombo.widthHint = 64;
		liftEnabledCombo.setLayoutData(gd_liftEnabledCombo);
		liftEnabledCombo.setItems(trueFalseAr);
		liftEnabledCombo.select(0);
		liftEnabledCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				editorModified();
			}
		});
		
		
		final Label replicateAllEnabledLabel = new Label(composite_2, SWT.NONE);
		GridData gd_replicateAllEnabledLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_replicateAllEnabledLabel.widthHint = 143;
		replicateAllEnabledLabel.setLayoutData(gd_replicateAllEnabledLabel);
		replicateAllEnabledLabel.setText("Replicate All Enabled");
		replicateAllEnabledCombo = new Combo(composite_2, SWT.READ_ONLY);
		replicateAllEnabledCombo.select(0);
		replicateAllEnabledCombo.setItems(trueFalseAr);
		final GridData gd_replicateAllEnabledCombo_1 = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_replicateAllEnabledCombo_1.widthHint = 63;
//		gd_replicateAllEnabledCombo_1.widthHint = 71;
		replicateAllEnabledCombo.setLayoutData(gd_replicateAllEnabledCombo_1);
		replicateAllEnabledCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				editorModified();
			}
		});
		
		new Label(composite_2, SWT.NONE);
		
		Label lblLiftAllEnabled = new Label(composite_2, SWT.NONE);
		GridData gd_lblLiftAllEnabled = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblLiftAllEnabled.widthHint = 138;
		lblLiftAllEnabled.setLayoutData(gd_lblLiftAllEnabled);
		lblLiftAllEnabled.setText("Lift All Enabled");
		
		liftAllEnabledCombo = new Combo(composite_2, SWT.READ_ONLY);
		GridData gd_liftAllEnabledCombo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_liftAllEnabledCombo.widthHint = 65;
		liftAllEnabledCombo.setLayoutData(gd_liftAllEnabledCombo);
		liftAllEnabledCombo.setItems(trueFalseAr);
		liftAllEnabledCombo.select(0);
		liftAllEnabledCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				editorModified();
			}
		});
		
		Label lblAdminLocks = new Label(composite_2, SWT.NONE);
		lblAdminLocks.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lblAdminLocks.setText("Admin Locks");
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		
		textAdminLocks = new Text(composite_2, SWT.BORDER | SWT.READ_ONLY);
		textAdminLocks.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Button AdminLockButton = new Button(composite_2, SWT.NONE);
		AdminLockButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				GenericAddRemoveDialog dlg = new GenericAddRemoveDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"Admin Lock Selector",
						"Select the Admin Lock specification(s) to associate with this Role Configuration:",
						roleConfigInput.getCachedAdminLocks(),
						roleConfigInput.getSelectedAdminLocks(),
						false);
				
				if( dlg.open() == IDialogConstants.OK_ID ) {
					int len = dlg.getSelectedItems().length;
					selectedAdminLocks = new String[len];
					if( len > 0 ) {
				        for (int i = 0, n = len; i < n; i++) {
				        	selectedAdminLocks[i] = dlg.getSelectedItems()[i];
				        }
						EditorControlUtil.setText(textAdminLocks, StringUtils.arrayToString(selectedAdminLocks, ","));
					}
					else {
						EditorControlUtil.setText(textAdminLocks, "");
					}
					roleConfigInput.setSelectedAdminLocks(selectedAdminLocks);
					editorModified();
				}
			}
		});
		AdminLockButton.setText("...");
		
		mdbSaveOnUowLoadButton = new Button(composite_2, SWT.CHECK);
		final GridData gridData_5 = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gridData_5.widthHint = 200;
		mdbSaveOnUowLoadButton.setLayoutData(gridData_5);
		mdbSaveOnUowLoadButton.setText("Save Working Version On Uow Load");
		mdbSaveOnUowLoadButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				editorModified();
			}
		});
		
		Label lblDefaultEvalOn = new Label(composite_2, SWT.NONE);
		GridData gd_lblDefaultEvalOn = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_lblDefaultEvalOn.widthHint = 221;
		lblDefaultEvalOn.setLayoutData(gd_lblDefaultEvalOn);
		lblDefaultEvalOn.setText("Default Eval On Reference Versions:");
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);
		
		refVersionsText = new Text(composite_2, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_refVersionsText = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_refVersionsText.widthHint = 276;
		refVersionsText.setLayoutData(gd_refVersionsText);
		
		modifyRefVersionsButton = new Button(composite_2, SWT.NONE);
		modifyRefVersionsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				try {
					ReferenceVersionDialog refVersionDialog = new ReferenceVersionDialog(shell, roleConfigInput.getProject(), refVersionList);				
					if( refVersionDialog.open() == IDialogConstants.OK_ID ) {
						refVersionList = refVersionDialog.getRefVersionList();
						EditorControlUtil.setText(refVersionsText, StringUtils.arrayListToString(refVersionList, ", "));
						editorModified();
					}
				} catch (RuntimeException re) {
					
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, re.getLocalizedMessage(), MessageDialog.ERROR);
					logger.error(re.getMessage());
					
				}
			}
		});
		modifyRefVersionsButton.setText("...");
		
		defaultEvalEnabledPlanVersionButton = new Button(composite_2, SWT.CHECK);
		GridData gd_defaultEvalEnabledPlanVersionButton = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_defaultEvalEnabledPlanVersionButton.widthHint = 227;
		defaultEvalEnabledPlanVersionButton.setLayoutData(gd_defaultEvalEnabledPlanVersionButton);
		defaultEvalEnabledPlanVersionButton.setText("Default Eval On Working Version");
		defaultEvalEnabledPlanVersionButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				editorModified();
			}
		});
		
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite composite_1 = new Composite(scrolledChildComposite, SWT.NONE);
		GridData gd_composite_1 = new GridData(SWT.FILL, SWT.TOP, true, false);
		gd_composite_1.heightHint = 133;
		gd_composite_1.horizontalSpan = 2;
		composite_1.setLayoutData(gd_composite_1);
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.numColumns = 2;
		composite_1.setLayout(gridLayout_5);

		largeCellOverrideLabel = new Label(composite_1, SWT.NONE);
		largeCellOverrideLabel.setText("Large Cell Limit");

		largeCellLimitTextBox = new Text(composite_1, SWT.BORDER);
		largeCellLimitTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		largeCellLimitTextBox.addVerifyListener(new VerifyListener() {

			public void verifyText(VerifyEvent arg0) {
				
				verifyOnlyPositiveInt(arg0);

			}
			
		});
		largeCellLimitTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		maximumCellOverrideLabel = new Label(composite_1, SWT.NONE);
		maximumCellOverrideLabel.setText("Max Cell Limit");

		maxCellLimitTextBox = new Text(composite_1, SWT.BORDER);
		maxCellLimitTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		
		maxCellLimitTextBox.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent arg0) {
				
				verifyOnlyPositiveInt(arg0);
							
			}
		});
		maxCellLimitTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		defaultRuleSetsLabel = new Label(composite_1, SWT.NONE);
		defaultRuleSetsLabel.setText("Default Rule Set");

		defaultRuleSet = new Combo(composite_1, SWT.READ_ONLY);
		defaultRuleSet.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label lblRoMeasures = new Label(composite_1, SWT.NONE);
		lblRoMeasures.setToolTipText("Read Only Measures");
		lblRoMeasures.setText("RO Measures");
		
		Composite composite = new Composite(composite_1, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		GridLayout gl_composite = new GridLayout(2, false);
		gl_composite.marginHeight = 0;
		gl_composite.verticalSpacing = 0;
		gl_composite.marginWidth = 0;
		composite.setLayout(gl_composite);
		
		readOnlyMeasuresTextBox = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_readOnlyMeasuresTextBox = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_readOnlyMeasuresTextBox.widthHint = 80;
		readOnlyMeasuresTextBox.setLayoutData(gd_readOnlyMeasuresTextBox);
		readOnlyMeasuresTextBox.setBounds(0, 0, 76, 21);
		
		Button readOnlyMeasuresButton = new Button(composite, SWT.NONE);
		readOnlyMeasuresButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if ( roleConfigInput.getProject() != null ) {
				
					PafApplicationDef[] pafApps = PafProjectUtil.getPafApplicationDef(roleConfigInput.getProject());
					
					if ( pafApps != null && pafApps.length > 0) {
					
						PafApplicationDef pafApp = pafApps[0];
						
						if ( pafApp.getMdbDef() != null && pafApp.getMdbDef().getMeasureDim() != null) {
					
							PafSimpleDimTree measureTree = null;
							
							try {
								
								measureTree = DimensionTreeUtility.getDimensionTree(roleConfigInput.getProject(), PafProjectUtil.getApplicationName(roleConfigInput.getProject()), DimensionUtil.getMeasureDimensionName(roleConfigInput.getProject()));
								
								String[] readOnlyMeasures = roleConfigInput.getReadOnlyMeasures();
								
								java.util.List<String> readOnlyMeasureList = null;
								
								if ( readOnlyMeasures != null && readOnlyMeasures.length > 0 ) {
									
									readOnlyMeasureList = new ArrayList<String>(Arrays.asList(readOnlyMeasures));
									
								}
								
								Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
								GenericPaceTreeDialog dialog = new GenericPaceTreeDialog(shell, "Read Only " + DimensionUtil.getMeasureDimensionName(roleConfigInput.getProject()) + " Selector", measureTree, readOnlyMeasureList);
								
								int rc = dialog.open();
								
								logger.debug(GenericPaceTreeDialog.class.toString() + ".open() : rc=" + rc);
								
								if ( rc == IDialogConstants.OK_ID) {
									
									java.util.List<String> checkedMemberList = dialog.getCheckedMemberList();
														
									//if no items are checked, set null and empty text box
									if ( checkedMemberList == null || checkedMemberList.size() == 0) {
										
										roleConfigInput.setReadOnlyMeasures(null);
										EditorControlUtil.setText(readOnlyMeasuresTextBox, "");										

									//if at least one item is checked, convert to array and set text in text box
									} else {
										
										roleConfigInput.setReadOnlyMeasures(checkedMemberList.toArray(new String[0]));
										EditorControlUtil.setText(readOnlyMeasuresTextBox, StringUtils.arrayListToString(checkedMemberList, ", "));
										
									}
									
									editorModified();
									
								}
								
							} catch (Exception e1) {
								logger.error(e1.getMessage());
							}
														
						}
					}													
				}
			}
		});
		readOnlyMeasuresButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		readOnlyMeasuresButton.setBounds(0, 0, 75, 25);
		readOnlyMeasuresButton.setText("...");
		defaultRuleSet.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});

		final Composite menuInformationComposite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_menuInformationComposite = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		gd_menuInformationComposite.widthHint = 238;
		menuInformationComposite.setLayoutData(gd_menuInformationComposite);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.marginWidth = 0;
		gridLayout_6.numColumns = 2;
		menuInformationComposite.setLayout(gridLayout_6);

		final Label menuCompositeLabel = new Label(menuInformationComposite, SWT.NONE);
		menuCompositeLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		menuCompositeLabel.setText("Menus");

		final Label menuCompositeSeparator = new Label(menuInformationComposite, SWT.HORIZONTAL | SWT.SEPARATOR);
		menuCompositeSeparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		menuCompositeSeparator.setText("Label");
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite composite_3 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gridData_1.widthHint = 152;
		gridData_1.heightHint = 118;
		composite_3.setLayoutData(gridData_1);
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.numColumns = 2;
		composite_3.setLayout(gridLayout_7);

		final Label visibleMenusLabel = new Label(composite_3, SWT.NONE);
		visibleMenusLabel.setText("Visible Menus");
		new Label(composite_3, SWT.NONE);

		visibleMenuTextBox = new Text(composite_3, SWT.READ_ONLY | SWT.BORDER);
		GridData gd_visibleMenuTextBox = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_visibleMenuTextBox.widthHint = 441;
		visibleMenuTextBox.setLayoutData(gd_visibleMenuTextBox);

		final Button visibleMenuButton = new Button(composite_3, SWT.NONE);
		visibleMenuButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
		
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				StringListSelectionDialog dialog = new StringListSelectionDialog(shell, 
						"Visible Custom Menu Selector", "Visible Custom Menu Selectors", 
						"Please select from the Available Custom Menus and then order the Selected Visible Custom Menus", 
						"Available Custom Menus", "Selected Custom Visible Menus", roleConfigInput.getCustomMenuDefNames(), roleConfigInput.getVisibleMenuItemNames() );
				
				int rc = dialog.open();
				
				if ( rc == Dialog.OK ) {
					
					roleConfigInput.setVisibleMenuItemNames(dialog.getSelectedItems());
					
					EditorControlUtil.setText(visibleMenuTextBox, StringUtils.arrayToString(roleConfigInput.getVisibleMenuItemNames(), ", "));
					
					editorModified();
					
				}
				
			}
		});
		visibleMenuButton.setText("...");

		final Label autorunMenusLabel = new Label(composite_3, SWT.NONE);
		autorunMenusLabel.setText("Auto Run On Save Menus");
		new Label(composite_3, SWT.NONE);

		autoRunOnSaveMenuTextBox = new Text(composite_3, SWT.READ_ONLY | SWT.BORDER);
		autoRunOnSaveMenuTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Button autoRunOnSaveMenuButton = new Button(composite_3, SWT.NONE);
		autoRunOnSaveMenuButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				StringListSelectionDialog dialog = new StringListSelectionDialog(shell, 
						"Auto Run On Save Custom Menu Selector", "Auto Run On Save Custom Menu Selectors", 
						"Please select from the Available Custom Menus and then order the Selected Auto Run On Save Custom Menus", 
						"Available Custom Menus", "Selected Custom Auto Run On Save Menus", roleConfigInput.getCustomMenuDefNames(), roleConfigInput.getAutoRunOnSaveMenuItemNames() );
				
				int rc = dialog.open();
				
				if ( rc == Dialog.OK ) {
					
					roleConfigInput.setAutoRunOnSaveMenuItemNames(dialog.getSelectedItems());
					
					EditorControlUtil.setText(autoRunOnSaveMenuTextBox, StringUtils.arrayToString(roleConfigInput.getAutoRunOnSaveMenuItemNames(), ", "));
					
					editorModified();
					
				}
			}
		});
		autoRunOnSaveMenuButton.setText("...");

		final Composite composite_4 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_composite_4 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_composite_4.horizontalSpan = 2;
		gd_composite_4.heightHint = 86;
		composite_4.setLayoutData(gd_composite_4);
		final GridLayout gridLayout_8 = new GridLayout();
		composite_4.setLayout(gridLayout_8);

		final Label availableRuleSetLabel = new Label(composite_4, SWT.NONE);
		availableRuleSetLabel.setText("Available Rule Sets");

		availableRuleSetsCheckboxViewer = CheckboxTableViewer.newCheckList(composite_4, SWT.BORDER);
		availableRuleSetsCheckbox = availableRuleSetsCheckboxViewer.getTable();
		availableRuleSetsCheckbox.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				editorModified();
			}
		});
		availableRuleSetsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite composite_4_1 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_3_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData_3_1.heightHint = 27;
		composite_4_1.setLayoutData(gridData_3_1);
		final GridLayout gridLayout_9 = new GridLayout();
		gridLayout_9.numColumns = 2;
		gridLayout_9.marginWidth = 0;
		composite_4_1.setLayout(gridLayout_9);

		final Label menuCompositeLabel_1 = new Label(composite_4_1, SWT.NONE);
		menuCompositeLabel_1.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		menuCompositeLabel_1.setText("Filters");

		final Label menuCompositeSeparator_1 = new Label(composite_4_1, SWT.HORIZONTAL | SWT.SEPARATOR);
		menuCompositeSeparator_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		menuCompositeSeparator_1.setText("Label");
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite composite_5 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gd_composite_5 = new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1);
		gd_composite_5.widthHint = 271;
		composite_5.setLayoutData(gd_composite_5);
		composite_5.setLayout(new GridLayout());

		final Label versionFiltersLabel = new Label(composite_5, SWT.NONE);
		versionFiltersLabel.setText("Version Filters");

		final Composite versionFilterComposite = new Composite(composite_5, SWT.NONE);
		versionFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_10 = new GridLayout();
		gridLayout_10.numColumns = 2;
		gridLayout_10.marginWidth = 0;
		gridLayout_10.marginHeight = 0;
		versionFilterComposite.setLayout(gridLayout_10);

		versionFilters = new Text(versionFilterComposite, SWT.READ_ONLY | SWT.BORDER);
		versionFilters.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		final GridData gd_versionFilters_1 = new GridData(SWT.FILL, SWT.TOP, true, false);
		gd_versionFilters_1.widthHint = 224;
		versionFilters.setLayoutData(gd_versionFilters_1);

		final Button modifyVersionFiltersButton = new Button(versionFilterComposite, SWT.NONE);
		modifyVersionFiltersButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				try {
					VersionFilterDialog versionFilterDialog = new VersionFilterDialog(shell, roleConfigInput.getProject(), versionFilterList);				
					if( versionFilterDialog.open() == IDialogConstants.OK_ID ) {
						versionFilterList = versionFilterDialog.getVersionFilterList();
						EditorControlUtil.setText(versionFilters, StringUtils.arrayListToString(versionFilterList, ", "));
						editorModified();
					}
				} catch (RuntimeException re) {
					
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, re.getLocalizedMessage(), MessageDialog.ERROR);
					logger.error(re.getMessage());
					
				}
			}
		});
		modifyVersionFiltersButton.setText("...");

		final Composite composite_8 = new Composite(composite_5, SWT.NONE);
		final GridLayout gridLayout_11 = new GridLayout();
		gridLayout_11.numColumns = 4;
		gridLayout_11.marginWidth = 0;
		gridLayout_11.marginHeight = 0;
		gridLayout_11.marginBottom = 3;
		composite_8.setLayout(gridLayout_11);

		allowRoleFilterLabel = new Label(composite_8, SWT.NONE);
		allowRoleFilterLabel.setText("Allow Role Filter");

		allowRoleFilterCombo = new Combo(composite_8, SWT.READ_ONLY);
		allowRoleFilterCombo.setItems(trueFalseAr);
		allowRoleFilterCombo.select(0);
		allowRoleFilterCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		allowRoleFilterCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				if ( allowRoleFilterCombo.getText().equals(Constants.DROPDOWN_BOOL_TRUE) ) {
					
					allowRoleFilterEllipseButton.setEnabled(true);
					
				} else {
					
					allowRoleFilterEllipseButton.setEnabled(false);					
					
				}
				//TTN 1735 - multi select role filters
				//PA0003: Enable the Multi-Select combo box if the 'Allow Role Filter' is set to default or true
				if ( allowRoleFilterCombo.getText().equals(Constants.DROPDOWN_GLOBAL_OPTION) || 
						allowRoleFilterCombo.getText().equals(Constants.DROPDOWN_BOOL_TRUE) ) {
					
					multiRoleFilterLabel.setVisible(true);
					multiRoleFilterCombo.setVisible(true);
					
				} 
				//PA0004: Disable the Multi-Select combo box if the 'Allow Role Filter' is set to false
				else {
					
					multiRoleFilterLabel.setVisible(false);
					multiRoleFilterCombo.setVisible(false);
					
				}

				editorModified();
			}
		});
		//TTN 1735 - multi select role filters
		multiRoleFilterLabel = new Label(composite_8, SWT.NONE);
		multiRoleFilterLabel.setText("Multi-Select");

		multiRoleFilterCombo = new Combo(composite_8, SWT.READ_ONLY | SWT.BORDER);
		multiRoleFilterCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		multiRoleFilterCombo.setItems(trueFalseAr);
		multiRoleFilterCombo.select(0);
		multiRoleFilterCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		final Composite versionFilterComposite_1 = new Composite(composite_5, SWT.NONE);
		versionFilterComposite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_12 = new GridLayout();
		gridLayout_12.numColumns = 2;
		gridLayout_12.marginWidth = 0;
		gridLayout_12.marginHeight = 0;
		versionFilterComposite_1.setLayout(gridLayout_12);

		allowRoleFiltersTextBox = new Text(versionFilterComposite_1, SWT.READ_ONLY | SWT.BORDER);
		allowRoleFiltersTextBox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		allowRoleFilterEllipseButton = new Button(versionFilterComposite_1, SWT.NONE);
		allowRoleFilterEllipseButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				DimensionAttributeSelectorDialog dialog = new DimensionAttributeSelectorDialog(menuInformationComposite.getShell(), roleConfigInput.getProject(), roleConfigInput.getRoleFilters());
				int rc = dialog.open();
				
				if ( rc == Dialog.OK) {
					
					String[] selectedRoleFilters = dialog.getSelectedAttributeDimensions();
					
					roleConfigInput.setRoleFilters(selectedRoleFilters);
					
					EditorControlUtil.setText(allowRoleFiltersTextBox, StringUtils.arrayToString(selectedRoleFilters, ", "));
					
					editorModified();
					
				}
			}
		});
		allowRoleFilterEllipseButton.setText("...");

		final Composite composite_9 = new Composite(composite_5, SWT.NONE);
		final GridLayout gridLayout_13 = new GridLayout();
		gridLayout_13.numColumns = 2;
		gridLayout_13.marginWidth = 0;
		gridLayout_13.marginHeight = 0;
		gridLayout_13.marginBottom = 3;
		composite_9.setLayout(gridLayout_13);

		allowSuppressInvalidIntersectionsLabel = new Label(composite_9, SWT.NONE);
		allowSuppressInvalidIntersectionsLabel.setText("Allow Suppress Invalid Intersections");

		allowSuppressInvalidIntersectionsCombo = new Combo(composite_9, SWT.READ_ONLY);
		allowSuppressInvalidIntersectionsCombo.setItems(trueFalseAr);
		allowSuppressInvalidIntersectionsCombo.select(0);
		allowSuppressInvalidIntersectionsCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		allowSuppressInvalidIntersectionsCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				if ( allowSuppressInvalidIntersectionsCombo.getText().equals(Constants.DROPDOWN_BOOL_TRUE) ) {
					
					allowSuppressInvalidIntersectionsEllipseButton.setEnabled(true);
					
				} else {
					
					allowSuppressInvalidIntersectionsEllipseButton.setEnabled(false);					
					
				}

				editorModified();
			}
		});
		
		final Composite versionFilterComposite_1_1 = new Composite(composite_5, SWT.NONE);
		versionFilterComposite_1_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_14 = new GridLayout();
		gridLayout_14.numColumns = 2;
		gridLayout_14.marginWidth = 0;
		gridLayout_14.marginHeight = 0;
		versionFilterComposite_1_1.setLayout(gridLayout_14);

		allowSuppressInvalidIntersectionsTextBox = new Text(versionFilterComposite_1_1, SWT.READ_ONLY | SWT.BORDER);
		allowSuppressInvalidIntersectionsTextBox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		allowSuppressInvalidIntersectionsEllipseButton = new Button(versionFilterComposite_1_1, SWT.NONE);
		allowSuppressInvalidIntersectionsEllipseButton.addSelectionListener(new SelectionAdapter() {	
			public void widgetSelected(SelectionEvent arg0) {
				
				//get non hier dims
				String[] nonHierDimensions = PafProjectUtil.getNonHierarchyDimensions(roleConfigInput.getProject());
				
				java.util.List<String> nonHierDimenisonList = new ArrayList<String>();
				
				//if no hiers exists, push into list
				if ( nonHierDimensions != null ) {
					
					nonHierDimenisonList.addAll(Arrays.asList(nonHierDimensions));
					
				}
				
				//get current pafDimSpec
				PafDimSpec[] pafDimSpecAr = roleConfigInput.getSuppressInvalidIntersections();
				
				//if null, create new paf dim spec ar 
				if ( pafDimSpecAr == null ) {
					
					pafDimSpecAr = new PafDimSpec[nonHierDimenisonList.size()];
					
					int ndx = 0;
					
					for ( String nonHierDim : nonHierDimenisonList) {						
						pafDimSpecAr[ndx] = new PafDimSpec();
						pafDimSpecAr[ndx++].setDimension(nonHierDim);												
					}
					
				//if not null, remove any old non hier dimensions
				} else {
					
					java.util.List<PafDimSpec> pafDimSpecList = new ArrayList<PafDimSpec>();
					
					for ( PafDimSpec pafDimSpec : pafDimSpecAr) {
						
						//if not null and non hier dim list contains the dimensions, add it, otherwise don't
						if ( pafDimSpec != null && nonHierDimenisonList.contains(pafDimSpec.getDimension() )) {
							
							pafDimSpecList.add(pafDimSpec);
							
						}
						
					}
					
					//convert list to ar
					pafDimSpecAr = pafDimSpecList.toArray(new PafDimSpec[0]);
					
				}
				
				MeasureFlagIntersectionDialog dialog = new MeasureFlagIntersectionDialog(menuInformationComposite.getShell(), roleConfigInput.getProject(), pafDimSpecAr);
				
				int rc = dialog.open();
				
				if ( rc == Dialog.OK) {
					
					//get updated paf dim specs
					pafDimSpecAr = dialog.getPafDimSpecAr();
					
					//set on input
					roleConfigInput.setSuppressInvalidIntersections(pafDimSpecAr);
					
					//populate text box
					EditorControlUtil.setText(allowSuppressInvalidIntersectionsTextBox, parseDimSpecArIntoString(roleConfigInput.getSuppressInvalidIntersections()));
					
					//fire dirty change
					editorModified();
					
				}
				
			}
		});
		allowSuppressInvalidIntersectionsEllipseButton.setText("...");
		
		Composite composite_10 = new Composite(composite_5, SWT.NONE);
		GridLayout gl_composite_10 = new GridLayout(4, false);
		gl_composite_10.marginWidth = 0;
		gl_composite_10.marginHeight = 0;
		gl_composite_10.marginBottom = 3;
		composite_10.setLayout(gl_composite_10);
		
		Label lblEnableFiltSubtotals = new Label(composite_10, SWT.NONE);
		lblEnableFiltSubtotals.setBounds(0, 0, 55, 15);
		lblEnableFiltSubtotals.setText("Enable Filtered Subtotals");
		
		filteredSubtotalsCombo = new Combo(composite_10, SWT.READ_ONLY);
		filteredSubtotalsCombo.setItems(trueFalseAr);
		filteredSubtotalsCombo.setVisibleItemCount(3);
		filteredSubtotalsCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		filteredSubtotalsCombo.setBounds(0, 0, 91, 23);
		new Label(composite_10, SWT.NONE);
		new Label(composite_10, SWT.NONE);

		final Composite viewComposite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_12 = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData_12.heightHint = 17;
		viewComposite.setLayoutData(gridData_12);
		final GridLayout gridLayout_15 = new GridLayout();
		gridLayout_15.numColumns = 2;
		gridLayout_15.marginWidth = 0;
		gridLayout_15.marginTop = 2;
		gridLayout_15.marginHeight = 0;
		viewComposite.setLayout(gridLayout_15);

		final Label viewCompositeLabel = new Label(viewComposite, SWT.NONE);
		viewCompositeLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		viewCompositeLabel.setText(Constants.MENU_VIEWS);

		final Label viewCompositeSeparator = new Label(viewComposite, SWT.HORIZONTAL | SWT.SEPARATOR);
		viewCompositeSeparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		viewCompositeSeparator.setText("Label");
		new Label(scrolledChildComposite, SWT.NONE);

		final Label viewSectionsLabel = new Label(scrolledChildComposite, SWT.NONE);
		final GridData gridData_10 = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		gridData_10.horizontalIndent = 2;
		viewSectionsLabel.setLayoutData(gridData_10);
		viewSectionsLabel.setText("Available Views && View Groups");
		new Label(scrolledChildComposite, SWT.NONE);

		final Composite selectedViewsLabelComposite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_8 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_8.heightHint = 20;
		selectedViewsLabelComposite.setLayoutData(gridData_8);
		final GridLayout gridLayout_16 = new GridLayout();
		gridLayout_16.numColumns = 2;
		gridLayout_16.marginWidth = 0;
		gridLayout_16.horizontalSpacing = 1;
		selectedViewsLabelComposite.setLayout(gridLayout_16);

		selectedViewsLabel = new Label(selectedViewsLabelComposite, SWT.NONE);
		selectedViewsLabel.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false));
		selectedViewsLabel.setText("Selected Views && View Groups");

		final Label viewLabelRequired = new Label(selectedViewsLabelComposite, SWT.NONE);
		viewLabelRequired.setForeground(SWTResourceManager.getColor(255, 0, 0));
		viewLabelRequired.setFont(SWTResourceManager.getFont("", 10, SWT.BOLD));
		viewLabelRequired.setText("*");
		new Label(scrolledChildComposite, SWT.NONE);

		availableViewGroupsAndViewsTree = new Tree(scrolledChildComposite, SWT.MULTI | SWT.BORDER);
		final GridData gridData_6 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_6.widthHint = 187;
		gridData_6.minimumWidth = 200;
		availableViewGroupsAndViewsTree.setLayoutData(gridData_6);

		final Composite leftRightComposite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_4 = new GridData(SWT.CENTER, SWT.CENTER, true, true);
		gridData_4.heightHint = 49;
		gridData_4.widthHint = 27;
		leftRightComposite.setLayoutData(gridData_4);
		final GridLayout gridLayout_17 = new GridLayout();
		gridLayout_17.verticalSpacing = 3;
		gridLayout_17.marginHeight = 0;
		leftRightComposite.setLayout(gridLayout_17);

		final Button availableViewsRight = new Button(leftRightComposite, SWT.ARROW | SWT.RIGHT);
		final GridData gridData_13 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_13.widthHint = 22;
		availableViewsRight.setLayoutData(gridData_13);
		availableViewsRight.setText("button");

		final Button availableViewsLeft = new Button(leftRightComposite, SWT.ARROW | SWT.LEFT);
		final GridData gridData_14 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_14.widthHint = 23;
		availableViewsLeft.setLayoutData(gridData_14);
		availableViewsLeft.setText(">");

		selectedViewGroupsAndViewsList = new List(scrolledChildComposite, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER);
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 71;
		selectedViewGroupsAndViewsList.setLayoutData(gridData);

		final Composite upDownComposite = new Composite(scrolledChildComposite, SWT.NONE);
		upDownComposite.setLayout(new GridLayout());

		final Button viewGroupButtonUp = new Button(upDownComposite, SWT.ARROW);
		viewGroupButtonUp.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				//user pressed up button
				ControlUtil.directionalButtonPressed(selectedViewGroupsAndViewsList, ControlUtil.UP_BUTTON_ID);
				
				editorModified();
			}
		});
		viewGroupButtonUp.setText("button");

		final Button viewGroupButtonDown = new Button(upDownComposite, SWT.ARROW | SWT.DOWN);
		viewGroupButtonDown.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				//user pressed down button
				ControlUtil.directionalButtonPressed(selectedViewGroupsAndViewsList, ControlUtil.DOWN_BUTTON_ID);
				editorModified();
			}
		});
		viewGroupButtonDown.setText("button");
		scrolledChildComposite.setSize(516, 685);
		scrolledComposite.setContent(scrolledChildComposite);
		
		
		//Adds items from the selectedViewGroupsAndViewsList
		//from the availableViewGroupsAndViewsTree.
		availableViewsRight.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				for(TreeItem item : availableViewGroupsAndViewsTree.getSelection()){
					if(item.getParentItem() != null){
						EditorControlUtil.addItemToList(selectedViewGroupsAndViewsList, item.getText());
						editorModified();
					}
				}
			}
		});
		
		//Remove the items from the selectedViewGroups list.
		availableViewsLeft.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				EditorControlUtil.removeItemFromList(
						selectedViewGroupsAndViewsList, 
						selectedViewGroupsAndViewsList.getSelectionIndices());
				editorModified();
			}
		});
		
//		EditorUtils.closeEditor(this, RoleConfigurationEditor.class);
		populateControlValues();

		//if isCopy, then flag as dirty
		if ( roleConfigInput != null && ( roleConfigInput.isNew() || roleConfigInput.isCopy()) ) {
			
			editorModified();
			
		}
		

	}
	
	/*
	 * 
	 */
	private void resizeForm(ControlEvent e){
		scrolledComposite.setMinSize(scrolledChildComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	/**
	 * Default control to set focus to when the editor gets focus.
	 */
	@Override
	public void setFocus() {
//		if (roleName != null && roleName.isEnabled() && !roleName.isDisposed()) {
//			roleName.setFocus();
//		} else if (planCycle != null && planCycle.isEnabled() && ! planCycle.isDisposed()){
//			planCycle.setFocus();
//		}
		
		//NFR-311
		if(scrolledComposite != null && scrolledComposite.isEnabled() && !scrolledComposite.isDisposed()){
			scrolledComposite.setFocus();
		}
	}

	/**
	 * Parses the versionFilterList array list into a comma seperated string.
	 * @return A comma seperated list of version filters.
	 */


	/**
	 * Gets the role configurations node. 
	 * @return The role configurations node.
	 */
	public RoleConfigurationsNode getRoleConfigurationsNode() {
		return roleConfigurationsNode;
	}

	/**
	 * Sets the role configurations node.
	 * @param roleConfigurationsNode The role configurations node.
	 */
	public void setRoleConfigurationsNode(
			RoleConfigurationsNode roleConfigurationsNode) {
		this.roleConfigurationsNode = roleConfigurationsNode;
	}

	/**
	 * Gets the tree viewer.
	 * @return The tree viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the tree viewer.
	 * @param viewer The tree viewer.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	/**
	 * Validates the required control input values.
	 * @return The label associated with the control if an invalid value is found,
	 * or null if the entries are valid. 
	 */
	private String isRequiredInputValid(){
		
		if(this.selectedViewGroupsAndViewsList == null || 
				this.selectedViewGroupsAndViewsList.getItemCount() == 0){
			return selectedViewsLabel.getText();
		}
	
	
		return null;
	}
	
	private String isInputDataValid() {
		
		if ( this.largeCellLimitTextBox.getText().length() > 0 ) {
			
			if ( ! EditorControlUtil.isTextValidPositiveInt(this.largeCellLimitTextBox)) {
				
				this.largeCellLimitTextBox.selectAll();
				
				return this.largeCellOverrideLabel.getText();
				
			}
			
		}
		
		if ( this.maxCellLimitTextBox.getText().length() > 0 ) {
			
			if ( ! EditorControlUtil.isTextValidPositiveInt(this.maxCellLimitTextBox)) {
		
				this.maxCellLimitTextBox.setFocus();
				
				return this.maximumCellOverrideLabel.getText();
				
			}
			
		}
		
		if ( this.availableRuleSetsCheckboxViewer.getCheckedElements().length > 0 ) {
			
			String defaultRuleSetName = this.defaultRuleSet.getText().trim();
			
			if ( defaultRuleSetName != null && defaultRuleSetName.length() != 0 ) {
				
				boolean isDefaultRuleSetChecked = this.availableRuleSetsCheckboxViewer.getChecked(defaultRuleSetName);		
				
				if ( ! isDefaultRuleSetChecked ) {
					
					return this.defaultRuleSetsLabel.getText() + " - This can not be selected if one or more Available Rule Set items are checked and " + defaultRuleSetName + " is not checked.";
					
				}
				
			}
			
		}
		
		if ( this.allowSuppressInvalidIntersectionsCombo.getText().equals(Constants.DROPDOWN_BOOL_TRUE) && 
				this.allowSuppressInvalidIntersectionsTextBox.getText().length() == 0 ) {
			
			return this.allowSuppressInvalidIntersectionsLabel.getText() + " - This can not be True if \nmeasure flag intersections do not exist.";
			
		}
		
		
		return null;
		
	}
	/**
	 * Populates the control with the default/startup values.
	 */
	private void populateControlValues(){
		try{
			//version filters
			refVersionList = roleConfigInput.getRefVersions();
			EditorControlUtil.setText(refVersionsText, StringUtils.arrayListToString(refVersionList, ", "));
			
			//build the "Views" section of the tree.
			TreeItem views = new TreeItem(this.availableViewGroupsAndViewsTree, SWT.NONE);
			views.setText("Views");
			for(String child : roleConfigInput.getViews()){
				EditorControlUtil.addTreeNode(availableViewGroupsAndViewsTree, views, child);
			}
			
			//Build the "Views" section of the tree.
			TreeItem viewGroups = new TreeItem(this.availableViewGroupsAndViewsTree, SWT.NONE);
			viewGroups.setText("View Groups");
			for(String child : roleConfigInput.getViewGroups()){
				EditorControlUtil.addTreeNode(this.availableViewGroupsAndViewsTree, viewGroups, child);
			}
			
			//Add the selected view items.
			EditorControlUtil.addItemsToList(selectedViewGroupsAndViewsList, roleConfigInput.getSelectedViewAndViewGroups());
			//checked list boxes.
			
			//add the rule sets to the list box
			EditorControlUtil.addElements(availableRuleSetsCheckboxViewer, roleConfigInput.getRulesSets());
			//select the user selected rule sets.
			EditorControlUtil.setCheckedElements(availableRuleSetsCheckboxViewer, roleConfigInput.getSelectedRulesSets());
			
			//default rule set combo boxes
			
			java.util.List<String> defaultRuleSetList = new ArrayList<String>();
			defaultRuleSetList.add("");
			defaultRuleSetList.addAll(Arrays.asList(roleConfigInput.getRulesSets()));
			
			EditorControlUtil.addItems(defaultRuleSet, defaultRuleSetList.toArray(new String[0]));
			EditorControlUtil.selectItem(defaultRuleSet, roleConfigInput.getDefaultRulesetName());		
			
			java.util.List<String> planCyclesList = new ArrayList<String>();
			planCyclesList.add("");
			planCyclesList.addAll(Arrays.asList(roleConfigInput.getPlanCycles()));
			
			//plan cycles
			EditorControlUtil.addItems(planCycle, planCyclesList.toArray(new String[0]));
			EditorControlUtil.selectItem(planCycle, roleConfigInput.getPlanCycle());
			
			java.util.List<String> roleList = new ArrayList<String>();
			roleList.add("");
			roleList.addAll(Arrays.asList(roleConfigInput.getRoles()));
			
			//roles
			EditorControlUtil.addItems(roleName, roleList.toArray(new String[0]));
			EditorControlUtil.selectItem(roleName, roleConfigInput.getRole());			
			
			defaultEvalEnabledPlanVersionButton.setSelection(roleConfigInput.isDefaultEvalEnabledWorkingVersion());
			
			mdbSaveOnUowLoadButton.setSelection(roleConfigInput.isMdbSaveWorkingVersionOnUowLoad());
						
			//version filters
			selectedAdminLocks = roleConfigInput.getSelectedAdminLocks();
			if( selectedAdminLocks != null && selectedAdminLocks.length > 0 )
				EditorControlUtil.setText(textAdminLocks, StringUtils.arrayToString(selectedAdminLocks, ","));
			
			//version filters
			versionFilterList = roleConfigInput.getVersionFilters();
			EditorControlUtil.setText(versionFilters, StringUtils.arrayListToString(versionFilterList, ", "));
						
			//visible menus
			EditorControlUtil.setText(visibleMenuTextBox, StringUtils.arrayToString(roleConfigInput.getVisibleMenuItemNames(), ", "));
			
			EditorControlUtil.setText(autoRunOnSaveMenuTextBox, StringUtils.arrayToString(roleConfigInput.getAutoRunOnSaveMenuItemNames(), ", "));
			

			//large cells
			if ( roleConfigInput.getLargeCellCount() != null ) {
				
				EditorControlUtil.setText(largeCellLimitTextBox, roleConfigInput.getLargeCellCount().toString());
				
			}

			//max cells
			if ( roleConfigInput.getMaxCellCount() != null ) {
				
				EditorControlUtil.setText(maxCellLimitTextBox, roleConfigInput.getMaxCellCount().toString());
				
			}
			
			//role filters
			if ( roleConfigInput.getRoleFilters() != null ) {
			
				EditorControlUtil.setText(allowRoleFiltersTextBox, StringUtils.arrayToString(roleConfigInput.getRoleFilters(), ", "));
			
			}
			
			if ( roleConfigInput.isAllowRoleFilter() == null ) {

				allowRoleFilterEllipseButton.setEnabled(false);
				EditorControlUtil.selectItem(allowRoleFilterCombo, Constants.DROPDOWN_GLOBAL_OPTION);
				
			} else {

				allowRoleFilterEllipseButton.setEnabled(roleConfigInput.isAllowRoleFilter());
				EditorControlUtil.selectItem(allowRoleFilterCombo, StringUtil.initCap(roleConfigInput.isAllowRoleFilter().toString()));
				
			}
			//TTN 1735 - multi select role filters
			//PA0003: Enable the Multi-Select combo box if the 'Allow Role Filter' is set to default or true
			if ( roleConfigInput.isAllowRoleFilter() == null || 
					roleConfigInput.isAllowRoleFilter() ) {
				
				multiRoleFilterLabel.setVisible(true);
				multiRoleFilterCombo.setVisible(true);
				
			} 
			//PA0004: Disable the Multi-Select combo box if the 'Allow Role Filter' is set to false
			else {
				
				multiRoleFilterLabel.setVisible(false);
				multiRoleFilterCombo.setVisible(false);
				
			}
			
			//loading multi-select data from isUserFilteredMultiSelect element in paf_planner_config.xml
			//if isUserFilteredMultiSelect is null, then set value to Default
			if ( roleConfigInput.IsUserFilteredMultiSelect() == null ) {
				EditorControlUtil.selectItem(multiRoleFilterCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			} 
			//if isUserFilteredMultiSelect is not null, then set the value
			else {
				EditorControlUtil.selectItem(multiRoleFilterCombo, StringUtil.initCap(roleConfigInput.IsUserFilteredMultiSelect().toString()));
			}
			//end of TTN 1735 - multi select role filters
			
			if ( roleConfigInput.isAllowSuppressInvalidIntersections() == null ) {

				allowSuppressInvalidIntersectionsEllipseButton.setEnabled(false);
				EditorControlUtil.selectItem(allowSuppressInvalidIntersectionsCombo, Constants.DROPDOWN_GLOBAL_OPTION);
				
			} else {

				allowSuppressInvalidIntersectionsEllipseButton.setEnabled(roleConfigInput.isAllowSuppressInvalidIntersections());
				EditorControlUtil.selectItem(allowSuppressInvalidIntersectionsCombo,  StringUtil.initCap(roleConfigInput.isAllowSuppressInvalidIntersections().toString()));
				
			}
					
			//suppress invlaid intersections
			if ( roleConfigInput.getSuppressInvalidIntersections() != null ) {
				
				EditorControlUtil.setText(allowSuppressInvalidIntersectionsTextBox, parseDimSpecArIntoString(roleConfigInput.getSuppressInvalidIntersections()));
			
			}
			
			
			//replicate 
			if ( roleConfigInput.getReplicateEnabled() != null ) {
				EditorControlUtil.selectItem(replicateEnabledCombo, StringUtil.initCap(roleConfigInput.getReplicateEnabled().toString()));
			} else {
				EditorControlUtil.selectItem(replicateEnabledCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}

			//replicate all
			if ( roleConfigInput.getReplicateAllEnabled() != null ) {
				EditorControlUtil.selectItem(replicateAllEnabledCombo, StringUtil.initCap(roleConfigInput.getReplicateAllEnabled().toString()));
			} else {
				EditorControlUtil.selectItem(replicateAllEnabledCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
			//lift 
			if ( roleConfigInput.getLiftEnabled() != null ) {
				EditorControlUtil.selectItem(liftEnabledCombo, StringUtil.initCap(roleConfigInput.getLiftEnabled().toString()));
			} else {
				EditorControlUtil.selectItem(liftEnabledCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}

			//lift all
			if ( roleConfigInput.getLiftAllEnabled() != null ) {
				EditorControlUtil.selectItem(liftAllEnabledCombo, StringUtil.initCap(roleConfigInput.getLiftAllEnabled().toString()));
			} else {
				EditorControlUtil.selectItem(liftAllEnabledCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
			//Filtered Subtotals 
			if ( roleConfigInput.getFilteredSubtotals() != null ) {
				EditorControlUtil.selectItem(filteredSubtotalsCombo, StringUtil.initCap(roleConfigInput.getFilteredSubtotals().toString()));
			} else {
				EditorControlUtil.selectItem(filteredSubtotalsCombo, Constants.DROPDOWN_GLOBAL_OPTION);
			}
			
			//read only measures
			if ( roleConfigInput.getReadOnlyMeasures() != null ) {
				EditorControlUtil.setText(readOnlyMeasuresTextBox, StringUtils.arrayToString(roleConfigInput.getReadOnlyMeasures(), ", "));
			}

		
			//now we are clean...
			isDirty = false;
			
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	

	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		try{
			if(viewer != null && roleConfigurationsNode != null){
				roleConfigurationsNode.createChildren(null);
				viewer.refresh(roleConfigurationsNode);
			}
		} catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * 
	 * Verify event
	 *
	 * @param event
	 */
	private void verifyOnlyPositiveInt(VerifyEvent event) {

		event.doit = EditorControlUtil.isPositiveNumber(event.text);

	}

	/**
	 * 
	 *  Parses a Dim Spec Ar into a comma seperated string
	 *
	 * @param pafDimSpecs dim specs to parse
	 * @return Comma seperated string of values.
	 */
	private String parseDimSpecArIntoString(PafDimSpec[] pafDimSpecs) {

		if (pafDimSpecs == null || pafDimSpecs.length == 0) {
			return "";
		}

		String[] strAr = new String[pafDimSpecs.length];

		int ndx = 0;

		for (PafDimSpec pafDimSpec : pafDimSpecs) {

			if (pafDimSpec.getDimension() != null
					&& pafDimSpec.getExpressionList() != null
					&& pafDimSpec.getExpressionList()[0] != null) {

				strAr[ndx++] = pafDimSpec.getDimension() + "="
						+ pafDimSpec.getExpressionList()[0];

			}

		}

		return StringUtils.arrayToString(strAr, ", ");

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.composite.PaceTreeModule;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.GenericAddRemoveDialog;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.InvalidSpecMemberErrorDialog;
import com.pace.admin.menu.dialogs.internal.InvalidSpecificationMember;
import com.pace.admin.menu.editors.input.AdminLockEditorInput;
import com.pace.admin.menu.editors.internal.SecurityMember;
import com.pace.admin.menu.editors.internal.SelectionType;
import com.pace.admin.menu.nodes.AdminLockNode;
import com.pace.admin.menu.nodes.AdminLocksNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.views.InvalidSpecificationMemberView;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafDimSpec;
import com.pace.base.data.PafMemberList;
import com.pace.base.security.AdminPersistLockDef;
import com.pace.base.utility.StringUtils;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;
import com.swtdesigner.ResourceManager;

/**
 * The User Security Editor allows an admin at assign security roles to database
 * users. A database user has no security defined by default so an admin has to
 * assign security to them. An admin will assign roles to a database user. Each
 * role has a list of hier dimensions and security needs to be defined for each
 * dim. For example, if the role was Planner, and the hier dims were product and
 * location, the security for product might be
 * 
 * @IDESC(DIV09, L0), which would provide that user all of DIV09 and descendants
 *               to level 0. This is a GUI for paf_security.xml. The Hier dims
 *               are populated dynamically for the paf_apps.xml. I used a lot of
 *               dynamic maps to maintain the list of roles and security members
 *               for each hier dim.
 * 
 * @author jmilliron
 * 
 */
public class AdminLockEditor extends EditorPart {
	
	public AdminLockEditor() {

	}
	
	// id of editor
	public static final String ID = "com.pace.admin.menu.editors.AdminLockEditor"; //$NON-NLS-1$
	
	private static Logger logger = Logger.getLogger(AdminLockEditor.class);

	private static final String WARNING_TEXT = "Note: Setting locks at upper levels without including all descendants is not a supported feature at this time.";
	
	private IProject project;
	private TreeViewer viewer;
	private TreeNode treeNode;
	//private PaceTreeModule paceTree;

	// flag used to tell if state is dirty
	private boolean isDirty = false;

	// input for editor
	private AdminLockEditorInput adminLockInput;
	private AdminPersistLockDef currentAdminLock;
	private java.util.List<PafDimSpec> dimSpecList = new ArrayList<PafDimSpec>();
	
	// set of hier dim names
	private java.util.List<String> cachedDimensions = new ArrayList<String>();
	private java.util.List<String> cachedAttributeDimensions = new ArrayList<String>();
	private java.util.List<String> selectedAttributeDimensions = new ArrayList<String>();
	private java.util.List<String> newSelectedAttributeDimensions = new ArrayList<String>();
	private java.util.List<String> allDimensions = new ArrayList<String>();
	private java.util.List<String> adminLockRoleConfigs = new ArrayList<String>();
	
	private Text textAdminLockName;
	// holds all of the hier dim tabs
	private TabFolder dimensionTabFolder;
	private List availableRolesListControl, selectedRolesListControl;
	// map to hold another map of security members keyd on hier dim, but this
	// map is keyd on role name
	private Map<String, java.util.List<SecurityMember>> securityMemberMap = new HashMap<String, java.util.List<SecurityMember>>();
	// map to hold another map of selected tree items keyd on hier dim, but this
	private Map<String, Map<String, String>> secSubMemberListTextMap = new HashMap<String, Map<String, String>>();
	// map to hld the dimension controls (SWT Tree) for hier dims
	private Map<String, PaceTreeModule> dimensionControlTreeMap = new HashMap<String, PaceTreeModule>();
	// map to hold the max number of levels
	private Map<String, Integer> maxTreeDepthMap = new HashMap<String, Integer>();
	// map to hold security member (@IDESC(DIV09, 0) which keys on role
	private Map<String, Map<String, TreeItem>> dimensionMapOfTreeItemMap = new HashMap<String, Map<String, TreeItem>>();
	private Map<String, Button> applyAliasTableMap = new HashMap<String, Button>();
	private Map<String, Combo> aliasTableComboMap = new HashMap<String, Combo>();
	private Map<String, Button> addButtonMap = new HashMap<String, Button>();
	private Map<String, List> securityMemberControlMap = new HashMap<String, List>();
	// map is keyd on hier dim name
	private Map<String, Button> removeButtonMap = new HashMap<String, Button>();
	private Map<String, Button> clearButtonMap = new HashMap<String, Button>();
	// map to selction only buttons, keyed on dim name
	private Map<String, Button> selectionOnlyMap = new HashMap<String, Button>();
	// map to selecitonChildren buttons, keyed on dim name
	private Map<String, Button> selectionChildrenMap = new HashMap<String, Button>();
	// map to justChildren buttons, keyed on dim name
	private Map<String, Button> justChildrenMap = new HashMap<String, Button>();
	// map to selectionDescendants buttons, keyed on dim name
	private Map<String, Button> selectionDescendantsMap = new HashMap<String, Button>();
	// map to justChildren buttons, keyed on dim name
	private Map<String, Button> justDescendantsMap = new HashMap<String, Button>();
	// map to justChildren buttons, keyed on dim name
	private Map<String, Button> justLevelGenerationMap = new HashMap<String, Button>();
	// level gen drop down map. keyd on dim name
	private Map<String, Combo> levelGenerationMap = new HashMap<String, Combo>();
	// level spinner map. keyd on dim name
	private Map<String, Spinner> levelGenSpinnerMap = new HashMap<String, Spinner>();

	private MouseListener tabTreeMouseListener;
	// bold font
	private Font boldFont;

	// standard font
	private Font normalFont;

	private Composite scrolledChildComposite;
	
	private ScrolledComposite scrolledComposite;
	
	private Set<String> hierarchyDimension;

	
	/**
	 * Create contents of the editor part
	 * 
	 * @param parent
	 *            Parent composite
	 */
	@Override
	public void createPartControl(Composite parent) {

		java.util.List<String> hDims = Arrays.asList(DimensionUtil.getHierDimensionNames(project));
        hierarchyDimension = new HashSet<String>(hDims.size() + 1);
        hierarchyDimension.addAll(hDims);
        hierarchyDimension.add(DimensionUtil.getTimeDimensionName(project));
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		parent.setLayout(gridLayout);

		scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL|SWT.RESIZE);
		//scrolledComposite.getVerticalBar().setIncrement(20);
		scrolledComposite.setAlwaysShowScrollBars(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setExpandHorizontal(true);
		final GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_scrolledComposite.heightHint = 600;
		scrolledComposite.setLayoutData(gd_scrolledComposite);

		scrolledChildComposite = new Composite(scrolledComposite, SWT.NONE);
		GridLayout gl_scrolledChildComposite = new GridLayout();
		gl_scrolledChildComposite.marginTop = 10;
		scrolledChildComposite.setLayout(gl_scrolledChildComposite);
		scrolledChildComposite.setSize(484, 339);
		scrolledComposite.setContent(scrolledChildComposite);
		
		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				resizeForm(e);
			}
		});

		final Composite compositeAdminLock = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.TOP, true,	false);
		gridData.heightHint = 540;
		gridData.widthHint = 590;
		compositeAdminLock.setLayoutData(gridData);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 2;
		gridLayout_2.verticalSpacing = 1;
		gridLayout_2.marginTop = 2;
		gridLayout_2.marginHeight = 0;
		compositeAdminLock.setLayout(gridLayout_2);		
		
		Label lblAdminLockName = new Label(compositeAdminLock, SWT.NONE);
		GridData gd_usernameLabel = new GridData(130, 22);
		gd_usernameLabel.verticalAlignment = SWT.TOP;
		gd_usernameLabel.horizontalAlignment = SWT.RIGHT;
		lblAdminLockName.setLayoutData(gd_usernameLabel);
		lblAdminLockName.setText("Admin Lock Name:");
		
		textAdminLockName = new Text(compositeAdminLock, SWT.BORDER);
		textAdminLockName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textAdminLockName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(compositeAdminLock, SWT.NONE);
		
		new Label(compositeAdminLock, SWT.NONE);
		
		final Button btnAddRemoveAttribButton = new Button(compositeAdminLock, SWT.NONE);
		btnAddRemoveAttribButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		btnAddRemoveAttribButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				GenericAddRemoveDialog dlg = new GenericAddRemoveDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"Add/Remove Attribute Dimensions",
						"Select the Attribute Dimension(s) that will be used as member filters in this Admin Lock specification:",
						cachedAttributeDimensions.toArray(new String[0]),
						selectedAttributeDimensions.toArray(new String[0]),
						false);
				
				if( dlg.open() == IDialogConstants.OK_ID ) {
					
					newSelectedAttributeDimensions.clear();
					
					newSelectedAttributeDimensions.addAll(Arrays.asList(dlg.getSelectedItems()));
					
					populateTabFolder(true);
	
					// populate the controls with values
					populateControlValues();
					
					//clear all the tab form controls
					clearAllTabFormControls();
					
					enableAllTabFormControls(true);
					
					populateSecurityForm();
					
					setSelectionControls();
					
					editorModified();
				}
			}
		});
		btnAddRemoveAttribButton.setText("Add/Remove Attributes");

		dimensionTabFolder = new TabFolder(compositeAdminLock, SWT.NONE);
		final GridData gridData_5 = new GridData(SWT.FILL, SWT.FILL, true, false, 2,1);
		gridData_5.horizontalSpan = 5;
		gridData_5.heightHint = 431;
		gridData_5.widthHint = 650;
		dimensionTabFolder.setLayoutData(gridData_5);
		
		final Composite compositeRoleConfig = new Composite(scrolledChildComposite, SWT.NONE);
		compositeRoleConfig.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginHeight = 0;
		gridLayout_4.marginWidth = 0;
		gridLayout_4.horizontalSpacing = 0;
		compositeRoleConfig.setLayout(gridLayout_4);
		
		Group grpRoleConfig = new Group(compositeRoleConfig, SWT.NONE);
		grpRoleConfig.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout gl_grpRoleConfig = new GridLayout(4, false);
		gl_grpRoleConfig.verticalSpacing = 1;
		grpRoleConfig.setLayout(gl_grpRoleConfig);
		grpRoleConfig.setText("Select any number of role configurations to apply this admin lock to:");
	
		final Label rolesLabel = new Label(grpRoleConfig, SWT.NONE);
		final GridData gridData_6 = new GridData(SWT.FILL, SWT.TOP, true,	false);
		gridData_6.heightHint = 15;
		gridData_6.widthHint = 80;
		rolesLabel.setLayoutData(gridData_6);
		rolesLabel.setText("Available Role Configurations:");
		
		new Label(grpRoleConfig, SWT.NONE);
				
		final Label rolesLabel_1 = new Label(grpRoleConfig, SWT.NONE);
		final GridData gridData_6_1 = new GridData(SWT.FILL, SWT.TOP, true, false);
		gridData_6_1.heightHint = 22;
		gridData_6_1.widthHint = 160;
		rolesLabel_1.setLayoutData(gridData_6_1);
		rolesLabel_1.setText("Selected Role Configurations:");
		
		new Label(grpRoleConfig, SWT.NONE);
		
		availableRolesListControl = new List(grpRoleConfig, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		availableRolesListControl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				selectedRolesListControl.deselectAll();
							
				for(String selectedItem : availableRolesListControl.getSelection() ){
				
					EditorControlUtil.addItemToList(selectedRolesListControl, selectedItem);
					EditorControlUtil.selectItem(selectedRolesListControl, selectedItem);
					editorModified();				
					
				}
				
				EditorControlUtil.removeItemFromList(availableRolesListControl, availableRolesListControl.getSelectionIndices());
			}
		});
		availableRolesListControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
						
		Composite compositeRightLeftButtons = new Composite(grpRoleConfig, SWT.NONE);
		compositeRightLeftButtons.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.numColumns = 1;
		gridLayout_5.marginHeight = 0;
		gridLayout_5.marginWidth = 0;
		gridLayout_5.horizontalSpacing = 0;
		compositeRightLeftButtons.setLayout(gridLayout_5);
		
		final Button rightArrowButton = new Button(compositeRightLeftButtons, SWT.ARROW | SWT.RIGHT);
		rightArrowButton.setText(">");
		rightArrowButton.setBounds(20, 10, 20, 20);
		rightArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
							
				//if there are roles to move over, unselect everything in selected roles
				if ( availableRolesListControl.getSelection().length > 0 ) {
					
					selectedRolesListControl.deselectAll();
				}
								
				for(String selectedItem : availableRolesListControl.getSelection() ){
				
					EditorControlUtil.addItemToList(selectedRolesListControl, selectedItem);
					EditorControlUtil.selectItem(selectedRolesListControl, selectedItem);
					editorModified();				
					
				}
				
				EditorControlUtil.removeItemFromList(availableRolesListControl, availableRolesListControl.getSelectionIndices());
								
			}
		});
		
								
		final Button lefttArrowButton = new Button(compositeRightLeftButtons, SWT.ARROW | SWT.LEFT);
		lefttArrowButton.setText("<");
		lefttArrowButton.setBounds(20, 60, 20, 20);
		lefttArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
							
				String[] rolesToRemoveFromSelectedRolesList = selectedRolesListControl.getSelection(); 
				
				for (String itemToAdd : rolesToRemoveFromSelectedRolesList ) {

					EditorControlUtil.addItemToList(availableRolesListControl, itemToAdd);
					editorModified();	
					
				}
				
				EditorControlUtil.removeItemFromList(
						selectedRolesListControl, 
						selectedRolesListControl.getSelectionIndices());
				
				String[] availableRolesListNames = availableRolesListControl.getItems();
				
				if ( availableRolesListNames != null ) {
					
					Arrays.sort(availableRolesListNames);
					
				}
				
				EditorControlUtil.addItemsToList(availableRolesListControl, availableRolesListNames);
				
			}
		});
		
		selectedRolesListControl = new List(grpRoleConfig, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		selectedRolesListControl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				String[] rolesToRemoveFromSelectedRolesList = selectedRolesListControl.getSelection(); 
				
				for (String itemToAdd : rolesToRemoveFromSelectedRolesList ) {

					EditorControlUtil.addItemToList(availableRolesListControl, itemToAdd);
					editorModified();	
					
				}
				
				EditorControlUtil.removeItemFromList(
						selectedRolesListControl, 
						selectedRolesListControl.getSelectionIndices());
				
				String[] availableRolesListNames = availableRolesListControl.getItems();
				
				if ( availableRolesListNames != null ) {
					
					Arrays.sort(availableRolesListNames);
					
				}
				
				EditorControlUtil.addItemsToList(availableRolesListControl, availableRolesListNames);
		}
		});
		selectedRolesListControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
				
		final Composite compositeUpDownButtons = new Composite(grpRoleConfig, SWT.NONE);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.numColumns = 1;
		gridLayout_6.marginHeight = 0;
		gridLayout_6.marginWidth = 0;
		gridLayout_6.horizontalSpacing = 0;
		compositeUpDownButtons.setLayout(gridLayout_6);
						
		final Button upArrowButton = new Button(compositeUpDownButtons, SWT.ARROW | SWT.UP);
		upArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ControlUtil.directionalButtonPressed(selectedRolesListControl, ControlUtil.UP_BUTTON_ID);
				if ( selectedRolesListControl.getSelection().length > 0 ) {
					editorModified();
				}
			}
		});
		upArrowButton.setText("^");
								
		final Button downArrowButton = new Button(compositeUpDownButtons, SWT.ARROW | SWT.DOWN);
		downArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ControlUtil.directionalButtonPressed(selectedRolesListControl, ControlUtil.DOWN_BUTTON_ID);
				if ( selectedRolesListControl.getSelection().length > 0 ) {
					editorModified();
				}
			}
		});
		downArrowButton.setText("down");

		// populate tab folder with hier dims
		populateTabFolder(false);

		clearAllTabFormControls();
		// populate the controls with values
		populateControlValues();
		
		populateSecurityForm();
		
		setSelectionControls();
		// add the control listeners
		addControlListeners();
		
	}


	/**
	 * Populates the tab folder with the controls. The contols include a tree
	 * viewer to display the hier dim tree, 3 check buttons that hold security
	 * member selections (seleciton only, selection and children, and selection
	 * and descendants), a drop down box with Level/Generation, and a spinner
	 * for level and gen number.
	 */
	private void populateTabFolder(boolean reload) {
		if( ! reload ) {
			
			addDimensionTabs(allDimensions);

		} 
		else {
			if(	newSelectedAttributeDimensions.size() != selectedAttributeDimensions.size() 
					|| ! newSelectedAttributeDimensions.contains(selectedAttributeDimensions)
					|| ! selectedAttributeDimensions.contains(newSelectedAttributeDimensions) ) {
				for( TabItem tabItem :  dimensionTabFolder.getItems() ) {
					if( selectedAttributeDimensions.contains(tabItem.getText()) 
							&& ! newSelectedAttributeDimensions.contains(tabItem.getText()) ) {
						allDimensions.remove(tabItem.getText());
						securityMemberMap.remove(tabItem.getText());
						securityMemberControlMap.remove(tabItem.getText());
						dimensionMapOfTreeItemMap.remove(tabItem.getText());
						tabItem.dispose();
					}
				}
				java.util.List<String> newTabs = new ArrayList<String>();
				for( String newTab : newSelectedAttributeDimensions) {
					if( ! selectedAttributeDimensions.contains(newTab) ) {
						newTabs.add(newTab);
					}
				}
				
				addDimensionTabs(newTabs);
				
				allDimensions.clear();
				allDimensions.addAll(cachedDimensions);
				allDimensions.addAll(newSelectedAttributeDimensions);
				selectedAttributeDimensions.clear();
				selectedAttributeDimensions.addAll(newSelectedAttributeDimensions);
			}
		}

	}

	private void addDimensionTabs(java.util.List<String> dimensions) {

		if (dimensions != null && dimensionTabFolder != null
				&& !dimensionTabFolder.isDisposed()) {
			// loop through each dimension and create a tab item. For each tab
			// item, create all the
			// controls
			for (String dimName : dimensions) {
				// if hier dimensions exist and the tab folder is not null and not
				// create new tab item
				final TabItem tabItem = new TabItem(dimensionTabFolder,
						SWT.NONE);

				// set text to hier dimension name
				tabItem.setText(dimName);

				// get paf tree from cached map
				PafSimpleDimTree pafSimpleTree = null;
				try {
					pafSimpleTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
							project, PafProjectUtil.getApplicationName(project),dimName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				if( pafSimpleTree == null ) {
					String outMessage = "Couldn't get cached simple tree. Login into server and cache dimensions";
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
					return;
				}

				// creat a new deposit
				final Composite tabComposite = 
						getDimensionTabFolderComposite(dimensionTabFolder, tabItem, pafSimpleTree);

				// set control to new composite
				tabItem.setControl(tabComposite);
				

				// get tree from cached map
				Tree treeControl = dimensionControlTreeMap.get(dimName).getTree();

				// create swt tree model from paf tree
				createTreeModel(treeControl, pafSimpleTree, dimName, pafSimpleTree.getRootKey());

				// expand top item
				TreeItem top = treeControl.getTopItem();
				if(top != null) {
					treeControl.getTopItem().setExpanded(true);
				}

				// get max depth for level and generation
				Integer maxTreeDept = maxTreeDepthMap.get(tabItem.getText());

				// get spinner just created
				Spinner spinner = levelGenSpinnerMap.get(dimName);

				// if not null and max tree dept not null
				if (spinner != null && maxTreeDept != null) {

					// set spinner to max tree dept
					spinner.setMaximum(maxTreeDept);

				}
			}

			// yeahhhhhh.......lets enable these new form controls
			enableAllTabFormControls(true);
		}
	}
	
	/**
	 * Clears all the form data. First all listeners are removed. Next the form
	 * controls are cleared. Next, unbold all the previously bolded tree itmes.
	 * Finally, add the listeners again.
	 */
	private void clearAllTabFormControls() {

		clearTabFormControl(selectionOnlyMap);
		clearTabFormControl(selectionChildrenMap);
		clearTabFormControl(selectionDescendantsMap);
		clearTabFormControl(justChildrenMap);
		clearTabFormControl(justLevelGenerationMap);
		clearTabFormControl(justDescendantsMap);
		clearTabFormControl(levelGenerationMap);
		clearTabFormControl(levelGenSpinnerMap);

		// unbold all dimension tree itmes
		unboldAllDimensionTreeItems();
	}

	/**
	 * Clears all the controls that are within the values of the map passed in.
	 * Value object is retrieved from the map, then tested and the approiate
	 * cast is applied then control is cleared.
	 * 
	 * @param controlsMap
	 *            Map (hier dim name, control)
	 */
	private void clearTabFormControl(Map controlsMap) {

		// for all values, clear the controls
		for (Object object : controlsMap.values()) {

			// if text, set text to empty string
			if (object instanceof Text) {

				Text txt = (Text) object;

				txt.setText("");

				// if combo, select first (0th) element
			} else if (object instanceof Combo) {

				Combo combo = (Combo) object;

				combo.select(0);

				// if button, un select
			} else if (object instanceof Button) {

				Button button = (Button) object;

				button.setSelection(false);

				// if tree, roll up all children and then expand again
			} else if (object instanceof Tree) {

				Tree tree = (Tree) object;

				// remove any alias information; convo with jim, he thinks
				// should remain enabled
				// updateTreeWithAliases(tree, null);

				for (TreeItem directTreeChild : tree.getItems()) {

					for (TreeItem treeChild : directTreeChild.getItems()) {

						treeChild.setExpanded(false);

					}

					directTreeChild.setExpanded(true);

				}

				// if tree contains any tree itmes, get root and then select and
				// bold
				if (tree.getItems().length > 0) {

					TreeItem topItem = tree.getItem(0);
					tree.setSelection(topItem);
					boldTreeItem(topItem, true);

				}

				// if spinner, select 0
			} else if (object instanceof Spinner) {

				Spinner spinner = (Spinner) object;

				spinner.setSelection(0);

			}

		}

	}

	/**
	 * Enables/disables all tab form controls based on the boolean passed in
	 * 
	 * @param isEnabled
	 *            Enables or disables form controls.
	 */
	private void enableAllTabFormControls(boolean isEnabled) {

		// enable/disable based on flag
		enableAllMapControls(selectionOnlyMap, isEnabled);
		enableAllMapControls(selectionChildrenMap, isEnabled);
		enableAllMapControls(selectionDescendantsMap, isEnabled);
		enableAllMapControls(justChildrenMap, isEnabled);
		enableAllMapControls(justDescendantsMap, isEnabled);
		enableAllMapControls(justLevelGenerationMap, isEnabled);
		enableAllMapControls(addButtonMap, isEnabled);
		enableAllMapControls(removeButtonMap, isEnabled);
		enableAllMapControls(clearButtonMap, isEnabled);

		// if disabling, disable dropdown and spinner
		if (!isEnabled) {

			enableAllMapControls(levelGenerationMap, isEnabled);
			enableAllMapControls(levelGenSpinnerMap, isEnabled);

		}


		if (isEnabled) {

			for (String key : aliasTableComboMap.keySet()) {

				Button applyAliasTableButton = applyAliasTableMap.get(key);

				Combo aliasTableCombo = aliasTableComboMap.get(key);

				aliasTableCombo
						.setEnabled(applyAliasTableButton.getSelection());

			}

		} else {

			enableAllMapControls(aliasTableComboMap, false);

		}

	}

	/**
	 * Enable/disable all map controls. Loop over all values of map and if
	 * instance of control, enable or diable.
	 * 
	 * @param controlsMap
	 *            Map of controls to enable/disable
	 * @param isEnabled
	 *            Flag to enable/disable
	 */
	private void enableAllMapControls(Map controlsMap, boolean isEnabled) {

		// for each value in map
		for (Object object : controlsMap.values()) {

			// if control
			if (object instanceof Control) {

				Control control = (Control) object;

				// enable/diable
				control.setEnabled(isEnabled);

			}

		}

	}

	@Override
	public void setFocus() {

		if ( textAdminLockName != null && ! textAdminLockName.isDisposed()) {
			textAdminLockName.setFocus();
		}

	}

	@Override
	public void doSaveAs() {
		// Do the Save As operation
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		adminLockInput = (AdminLockEditorInput) input;
		currentAdminLock = adminLockInput.getCurrentAdminLock();
		project = adminLockInput.getProject();
		adminLockRoleConfigs = adminLockInput.getAdminLockRoleConfigs();
		cachedAttributeDimensions = adminLockInput.getCachedAttributeDimensions();
		cachedDimensions = adminLockInput.getCachedDimensions();
		if( currentAdminLock != null ) {
			if( adminLockInput.isClone() ) {
				currentAdminLock = (AdminPersistLockDef) currentAdminLock.clone();
			}
			dimSpecList = currentAdminLock.getDimSpecList();
			allDimensions.addAll(getAdminLockDimensions(dimSpecList));
		}
		else {
			allDimensions.addAll(cachedDimensions);
		}

		setSite(site);
		setInput(input);

		// after init, set dirty to false
		setPartName(Constants.MENU_ADMIN_LOCK+ ": " + adminLockInput.getName());
		setDirty(false);
	}

	private java.util.List<String> getAdminLockDimensions(java.util.List<PafDimSpec> dimSpecList) {
		java.util.List<String> dimensions = new ArrayList<String>(cachedDimensions);
		for( PafDimSpec dimSpec : dimSpecList ) {
			String dimension = dimSpec.getDimension();
			if( ! dimensions.contains(dimension) ) {
				dimensions.add(dimension);
				selectedAttributeDimensions.add(dimension);
			}
		}
		return dimensions;
	}


	@Override
	public boolean isDirty() {
		setPartName(Constants.MENU_ADMIN_LOCK+ ": " + textAdminLockName.getText());
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * @return the viewer
	 */
	public TreeViewer getViewer() {
		return this.viewer;
	}

	/**
	 * @param viewer
	 *            the viewer to set
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Build the contents of a tree control and populate it.
	 * 
	 * @param tree
	 *            The tree control to build.
	 */
	private void createTreeModel(Tree tree, PafSimpleDimTree simpleBaseTree, String dimName, String root) {

		logger.debug("Create Tree Model start: " + root);

		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();

		try {

			HashMap treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(simpleBaseTree);

			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(root);

			int maxTreeDepth = rootMember.getPafSimpleDimMemberProps().getLevelNumber();

			//TTN-2476 - grab the map using the dimName and not root node, because they may not be the same.
			maxTreeDepthMap.put(dimName, maxTreeDepth);
			
			//Add a root member node to the dimension tree
			TreeItem rootItem = new TreeItem(tree, SWT.NONE);
			rootItem.setText(rootMember.getKey());
			rootItem.setData(rootMember);
			//TTN-2476 - grab the map using the dimName and not root node, because they may not be the same.
			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimName);
			if (treeItemMap == null) {
				treeItemMap = new HashMap<String, TreeItem>();
			}
			//TTN-2476 - grab the map using the dimName and not root node, because they may not be the same.
			treeItemMap.put(dimName, rootItem);
			dimensionMapOfTreeItemMap.put(dimName, treeItemMap);

			//Add a UOW_ROOT node to the dimension tree
			if( ! DimensionUtil.getVersionDimensionName(project).equals(root) ) {
				if( ! cachedAttributeDimensions.contains(root) ) {
					TreeItem uowItem = new TreeItem(rootItem, SWT.NONE);
					uowItem.setText(PafBaseConstants.UOW_ROOT);
					PafSimpleDimMember uowRootMember = new PafSimpleDimMember();
					uowRootMember.setKey(PafBaseConstants.UOW_ROOT);
					PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
					prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
					prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
					uowRootMember.setPafSimpleDimMemberProps(prop);
					uowItem.setData(uowRootMember);
					treeItemMap.put(uowItem.getText(), uowItem);
					dimensionMapOfTreeItemMap.put(root, treeItemMap);
				}
			}
			else  { // if( DimensionUtil.getVersionDimensionName(project).equals(root) ) {
				TreeItem uowItem = new TreeItem(rootItem, SWT.NONE);
				uowItem.setText(PafBaseConstants.PLAN_VERSION);
				PafSimpleDimMember uowRootMember = new PafSimpleDimMember();
				uowRootMember.setKey(PafBaseConstants.PLAN_VERSION);
				PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
				prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
				prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
				uowRootMember.setPafSimpleDimMemberProps(prop);
				uowItem.setData(uowRootMember);
				treeItemMap.put(uowItem.getText(), uowItem);
				dimensionMapOfTreeItemMap.put(root, treeItemMap);
			}
			
			if( DimensionUtil.getYearDimensionName(project).equals(root) ) {
				TreeItem uowItem = new TreeItem(rootItem, SWT.NONE);
				uowItem.setText(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS);
				PafSimpleDimMember uowRootMember = new PafSimpleDimMember();
				uowRootMember.setKey(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS);
				PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
				prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
				prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
				uowRootMember.setPafSimpleDimMemberProps(prop);
				uowItem.setData(uowRootMember);
				treeItemMap.put(uowItem.getText(), uowItem);
				dimensionMapOfTreeItemMap.put(root, treeItemMap);
			}

			if( adminLockInput.getUserMemberList() != null 
					&& adminLockInput.getUserMemberList().getMemberLists() != null 
					&& adminLockInput.getUserMemberList().getMemberLists().size() > 0 ) {
				for ( Map.Entry<String, PafMemberList> entry : adminLockInput.getUserMemberList().getMemberLists().entrySet() )
				{
					if( entry.getValue().getDimName().equalsIgnoreCase(root) ) {
						TreeItem memberListsItem = treeItemMap.get(Constants.MEMBER_LISTS);
						if( memberListsItem == null ) {
							memberListsItem = new TreeItem(rootItem, SWT.NONE);
							memberListsItem.setText(Constants.MEMBER_LISTS);
							treeItemMap.put(memberListsItem.getText(), memberListsItem);
							dimensionMapOfTreeItemMap.put(root, treeItemMap);
						}
						TreeItem memberItem = new TreeItem(memberListsItem, SWT.NONE);
						memberItem.setText(PafBaseConstants.MEMBERLIST_TOKEN + "(" + entry.getKey() + ")");
						PafSimpleDimMember uowRootMember = new PafSimpleDimMember();
						uowRootMember.setKey(PafBaseConstants.MEMBERLIST_TOKEN + "(" + entry.getKey() + ")");
						PafSimpleDimMemberProps prop = new PafSimpleDimMemberProps();
						prop.setLevelNumber(rootMember.getPafSimpleDimMemberProps().getLevelNumber());
						prop.setGenerationNumber(rootMember.getPafSimpleDimMemberProps().getGenerationNumber());
						uowRootMember.setPafSimpleDimMemberProps(prop);
						memberItem.setData(uowRootMember);
						treeItemMap.put(memberItem.getText(), memberItem);
						dimensionMapOfTreeItemMap.put(root, treeItemMap);
					}
				}
			}

			addTreeNode(treeHashMap, tree, rootItem, rootMember, root);
			
			cachedTreeMap.put(root, tree);


		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds a tree node to a tree.
	 * 
	 * @param treeHashMap
	 *            The hash map containing the tree components.
	 * @param tree
	 *            The tree to add the nodes to.
	 * @param parent
	 *            The parent tree item to have the nodes added to.
	 * @param member
	 *            The paf simple memeber to add to the parent.
	 * @param dimension
	 *            The dimension of this tree.
	 */
	private void addTreeNode(HashMap treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		if (member.getChildKeys().size() > 0 ) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap.get(child);

				TreeItem newItem  = new TreeItem(parent, SWT.NONE);

				newItem.setText(childMember.getKey());

				newItem.setData(childMember);

				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimension);

				if (treeItemMap == null) {

					treeItemMap = new HashMap<String, TreeItem>();

				}

				treeItemMap.put(newItem.getText(), newItem);

				dimensionMapOfTreeItemMap.put(dimension, treeItemMap);
				
				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}


	}

	/**
	 * Creates a new tab folder composite with contorls. After each contorl is
	 * created and added to composite, add to map (dim name, control).
	 * 
	 * @param tabFolder
	 *            Tab folder to create composite for.
	 * @param tabItem
	 *            Tab item to create composite for.
	 * @return
	 */
	private Composite getDimensionTabFolderComposite(TabFolder tabFolder,
			TabItem tabItem, PafSimpleDimTree pafSimpleDimTree) {

		final Composite dimTabComposite = new Composite(tabFolder, SWT.NONE);
		dimTabComposite.setLayout(new GridLayout(3, false));
		dimTabComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		/*******************************
		 * Security Dimension Group	****
		 *******************************/
		Group grpSecDim = new Group(dimTabComposite, SWT.NONE);
		grpSecDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecDim.setLayout(new GridLayout(1, false));
		grpSecDim.setText("Lock Dimension");
		
		Composite subSecDim = new Composite(grpSecDim, SWT.NONE);
		subSecDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		subSecDim.setLayout(new GridLayout(3, false));

		final Button aliasTableCheck = new Button(subSecDim, SWT.CHECK);
		aliasTableCheck.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		aliasTableCheck.setText("View Alias Table");

		applyAliasTableMap.put(tabItem.getText(), aliasTableCheck);

		final Combo aliasTableDropDown = new Combo(subSecDim, SWT.READ_ONLY);
		aliasTableDropDown.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));

		if (pafSimpleDimTree != null && pafSimpleDimTree.getAliasTableNames().size() > 0 ) {

			String[] aliasTableNames = pafSimpleDimTree.getAliasTableNames().toArray(new String[0]);

			if (aliasTableNames != null) {

				aliasTableDropDown.setItems(aliasTableNames);

			}

			// select first item
			aliasTableDropDown.select(0);

		}
		aliasTableComboMap.put(tabItem.getText(), aliasTableDropDown);
		
		//new Label(subSecDim, SWT.NONE);
		
		final Button addButton = new Button(subSecDim, SWT.PUSH );
		addButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		addButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/add.gif"));
		addButton.setToolTipText("Add Tree Item to Security Specification");
		
		addButtonMap.put(tabItem.getText(), addButton);
		
		addButton.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				addNewSecurityMember();
				setSelectionControls();
				editorModified();
			}
		});

		//new Label(subSecDim, SWT.NONE);
		
		final PaceTreeModule dimTree = new PaceTreeModule(grpSecDim, 0, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		dimTree.setLayoutData(gridData_1);
		
		dimTree.getTree().addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				onSelectedDimensionTreeControlChange();
			}
		
		});
		
		dimTree.getTree().addMouseListener(getTabTreeMouseListener());
		
		dimensionControlTreeMap.put(tabItem.getText(), dimTree);
		
		/***********************************
		 * Security Specification Group
		 **********************************/
		Group grpSecSpec = new Group(dimTabComposite, SWT.NONE);
		grpSecSpec.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecSpec.setText("Lock Member Specification");
		grpSecSpec.setLayout(new GridLayout(1, false));
		
		Composite subSecSpec = new Composite(grpSecSpec, SWT.NONE);
		subSecSpec.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false));
		subSecSpec.setLayout(new GridLayout(2, false));

		final Button removeButton = new Button(subSecSpec, SWT.PUSH);
		//removeButton.setLayoutData(new GridData(92, 14));
		//removeButton.setBounds(170, 20, 20, 27);
		removeButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		removeButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/delete.png"));
		removeButton.setToolTipText("Delete Selected Item From Security Specification");
		
		removeButtonMap.put(tabItem.getText(), removeButton);
		
		removeButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeSelectedUserSecurity();
				editorModified();
			}
		});

		final Button clearButton = new Button(subSecSpec, SWT.PUSH);
		//clearButton.setLayoutData(new GridData(92, 14));
		//clearButton.setBounds(190, 20, 21, 27);
		clearButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		clearButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/clear.png"));
		clearButton.setToolTipText("Clear Security Specification");
		
		clearButtonMap.put(tabItem.getText(), clearButton);
		
		clearButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				removeAllUserSecurity();
				editorModified();
			}
		});

		final List securityListControl = new List(grpSecSpec, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_9 = new GridData(SWT.FILL, SWT.FILL, true, true);
		//gridData_9.widthHint = 180;
		//gridData_9.heightHint = 200;
		securityListControl.setLayoutData(gridData_9);
		//securityListControl.setBounds(10, 50, 200, 250);
		securityListControl.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				onSelectedSecurityListControlChange();
			}
		
		});

		securityMemberControlMap.put(tabItem.getText(), securityListControl);
		
		if(secSubMemberListTextMap.get(tabItem.getText()) == null){
			secSubMemberListTextMap.put(tabItem.getText(), new HashMap<String, String>());
		}

		aliasTableCheck.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				// get selection
				boolean isSelected = aliasTableCheck.getSelection();

				// if apply alias is checked, enable drop down, select first
				// element and update tree with aliases
				if (isSelected) {

					aliasTableDropDown.setEnabled(true);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimTree.getTree(), securityListControl, aliasTableDropDown.getText());
				} else {

					aliasTableDropDown.setEnabled(false);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimTree.getTree(), securityListControl, null);

				}
//				securityListControl.deselectAll();
				setSelectionControls();

			}

		});

		aliasTableDropDown.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				updateTreeWithAliases(dimTree.getTree(), securityListControl, aliasTableDropDown.getText());
				setSelectionControls();

			}

		});

		/******************************
		 * Selection Level Group
		 *********************************/
		int startY = 22;
		int yIncrement = 22;
		final Group selectionGroup = new Group(dimTabComposite, SWT.NONE);
		selectionGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
		selectionGroup.setText("Selection");

		if(hierarchyDimension.contains(pafSimpleDimTree.getId())){
			StyledText warning = new StyledText(selectionGroup, SWT.READ_ONLY | SWT.WRAP);
	        warning.setText(WARNING_TEXT); 
			StyleRange bold = new StyleRange();
			bold.start = 0;
			bold.length = 4;
			bold.fontStyle = SWT.BOLD;
			warning.setStyleRange(bold);
	        warning.setBounds(10, startY, 180, 67);
	        startY = startY + 5 + warning.getBounds().height;
		}
		
		final Button selectionButton = new Button(selectionGroup, SWT.RADIO);
		selectionButton.setText(SelectionType.Selection.getDisplayString());
		selectionButton.setBounds(10, startY, 180, 19);
		selectionButton.setSelection(true);
		startY = startY + yIncrement;
		
		selectionButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),false,SelectionType.Selection.getIndex() );
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});

		selectionOnlyMap.put(tabItem.getText(), selectionButton);

		final Button selectionChildrenButton = new Button(selectionGroup,
				SWT.RADIO);
		selectionChildrenButton.setText(SelectionType.SelectionAndChildren.getDisplayString());
		selectionChildrenButton.setBounds(10, startY, 180, 19);
		startY = startY + yIncrement;
		selectionChildrenButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),false,SelectionType.SelectionAndChildren.getIndex());
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});

		selectionChildrenMap.put(tabItem.getText(), selectionChildrenButton);

		final Button justChildrenButton = new Button(selectionGroup,
				SWT.RADIO);
		justChildrenButton.setText(SelectionType.JustChildren.getDisplayString());
		justChildrenButton.setBounds(10, startY, 180, 19);
		startY = startY + yIncrement;
		justChildrenButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),false,SelectionType.JustChildren.getIndex());
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});
		
		justChildrenMap.put(tabItem.getText(), justChildrenButton);
		
		final Button selectionDescendantsButton = new Button(selectionGroup, SWT.RADIO);
		selectionDescendantsButton.setText(SelectionType.SelectionAndDescendants.getDisplayString());
		selectionDescendantsButton.setBounds(10, startY, 180, 19);
		startY = startY + yIncrement;
		selectionDescendantsButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),true,SelectionType.SelectionAndDescendants.getIndex());
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});

		selectionDescendantsMap.put(tabItem.getText(),
				selectionDescendantsButton);

		final Button justDescendantsButton = new Button(selectionGroup,
				SWT.RADIO);
		justDescendantsButton.setText(SelectionType.JustDescendants.getDisplayString());
		justDescendantsButton.setBounds(10, startY, 180, 19);
		startY = startY + yIncrement;
		justDescendantsMap.put(tabItem.getText(), justDescendantsButton);
		justDescendantsButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),true,SelectionType.JustDescendants.getIndex());
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});
		
		final Button levelGenerationButton = new Button(selectionGroup,
				SWT.RADIO);
		levelGenerationButton.setText(SelectionType.JustLevelORGeneration.getDisplayString());
		levelGenerationButton.setBounds(10, startY, 180, 19);
		startY = startY + yIncrement;
		levelGenerationButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),true,SelectionType.JustLevelORGeneration.getIndex());
				updateSecurityMember(((Button)e.getSource()).getSelection());
				editorModified();
			}
		});
		
		justLevelGenerationMap.put(tabItem.getText(), levelGenerationButton);
		
		final Combo levelGenCombo = new Combo(selectionGroup, SWT.READ_ONLY);

		String[] levelGenDropdownValues = new String[] {
				LevelGenerationType.Bottom.toString(),
				LevelGenerationType.Level.toString(),
				LevelGenerationType.Generation.toString() };

		EditorControlUtil.addItems(levelGenCombo, levelGenDropdownValues);
		levelGenCombo.setBounds(10, startY, 101, 21);
		levelGenCombo.setEnabled(false);
		levelGenCombo.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember(true);
				editorModified();
			}
		});

		levelGenerationMap.put(tabItem.getText(), levelGenCombo);

		final Spinner levelGenSpinner = new Spinner(selectionGroup, SWT.BORDER
				| SWT.READ_ONLY);
		// levelGenSpinner.setMaximum(13);
		levelGenSpinner.setBounds(117, startY, 41, 23);
		levelGenSpinner.setEnabled(false);
		levelGenSpinner.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember(true);
				editorModified();
			}
		});

		levelGenSpinnerMap.put(tabItem.getText(), levelGenSpinner);

		return dimTabComposite;
	}

	protected void removeAllUserSecurity() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		unboldDimensionTreeItems(mapKeyDimName);
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && securityMemberList.getItemCount() > 0 ) {
			securityMemberList.removeAll();
			securityMemberMap.remove(mapKeyDimName);
		}
		
		setSelectionControls();
	}

	protected void removeSelectedUserSecurity() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		unboldDimensionTreeItems(mapKeyDimName);
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
	    // try to get security member
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && securityMemberList.getSelectionCount() > 0 ) {
			String[] secList = securityMemberList.getSelection();
			for( String sec : secList ) {
				securityMemberList.remove(sec);
				removeSelectedSecurity(mapKeyDimName, sec);
			}
		}
//		if( securityMemberList.getItemCount() == 0 ) {
//			enableAllMapControls(removeButtonMap, false);
//			enableAllMapControls(clearButtonMap, false);
//		}
		setSelectionControls();
	}

	protected void removeSelectedSecurity( String dimension, String security ) {
		if (securityMemberMap != null && securityMemberMap.size() > 0 ) {
			java.util.List<SecurityMember> secArray = securityMemberMap.get(dimension);
			if( secArray != null  ) {
				SecurityMember secMem = new SecurityMember(dimension, security);
				  for( Iterator< SecurityMember > it = secArray.iterator(); it.hasNext() ; ) {
			           SecurityMember sec = it.next(); 
			           if( sec.equals(secMem) )
			        	   it.remove();
				  }
			}
		}
	}
		
	/**
	 * Applies an alias table to the existing dimension tree.
	 * 
	 * @param dimensionTree
	 *            Dimension tree to have alias applied to
	 * @param aliasTableName
	 *            Alias table name to apply to tree
	 */
	protected void updateTreeWithAliases(Tree dimensionTree, List secMemControl, String aliasTableName) {

		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		Map<String, String> memberListTextMap = this.secSubMemberListTextMap.get(mapKeyDimName);
		String[] listStrs = secMemControl.getItems();

		for (TreeItem childItem : dimensionTree.getItems()) {
			applyAliasToTree(childItem, memberListTextMap, listStrs, aliasTableName);
			securityMemberControlMap.get(dimensionTree.getItems()[0]);
		}

		for(int i = 0; i < listStrs.length; i++){
			if(memberListTextMap.containsKey(listStrs[i])){
				String temp = listStrs[i];
				listStrs[i] = memberListTextMap.get(listStrs[i]);
				memberListTextMap.remove(temp);
				memberListTextMap.put(listStrs[i], temp);
			} else if(listStrs[i].indexOf('(') != -1){
				int endNdx = listStrs[i].indexOf(')');
				if(listStrs[i].indexOf(',') != -1){
					endNdx = listStrs[i].indexOf(',');
				}
				String theText = listStrs[i].substring(listStrs[i].lastIndexOf('(') + 1, endNdx);
				String newText = memberListTextMap.get(theText);
				listStrs[i] = listStrs[i].substring(0, listStrs[i].lastIndexOf('(') + 1) +
								newText +
								listStrs[i].substring(endNdx, listStrs[i].length());
				memberListTextMap.remove(theText);
				memberListTextMap.put(newText, theText);				
			}
		}
		
		secMemControl.setItems(listStrs);
	}

	/**
	 * Updates the treeItem's text with an alias if an alias table name is
	 * defined. If the table name is null, the PafSimpleDimMember.getKey is used
	 * as the text, but if table name is provided, the simple member is searched
	 * for alias keys that match the alias table name and then the alias value
	 * is set as the text.
	 * 
	 * @param treeItem
	 *            text gets updated with key or alias value
	 * @param aliasTableName
	 *            name of alias table
	 */
	private void applyAliasToTree(TreeItem treeItem, Map<String, String> memListTextMap, String[] listStrs, String aliasTableName) {

		// if the tree item has children
		if (treeItem.getItems().length > 0) {

			// apply aliases to the children
			for (TreeItem childItem : treeItem.getItems()) {

				applyAliasToTree(childItem, memListTextMap, listStrs, aliasTableName);

			}

		}

		// get data member
		PafSimpleDimMember member = (PafSimpleDimMember) treeItem.getData();

		if ( member != null ) {
			// if the alias table name is null, use key from member, else use alias
			if (aliasTableName == null) {
	
				treeItem.setText(member.getKey());
	
			} else {
				
				if ( member.getPafSimpleDimMemberProps() != null
						&& member.getPafSimpleDimMemberProps().getAliasKeys().size() > 0) {
	
					String[] aliasKeys = member.getPafSimpleDimMemberProps().getAliasKeys().toArray(new String[0]);
					
					String[] aliasValues = member.getPafSimpleDimMemberProps()
							.getAliasValues().toArray(new String[0]);
	
					for (int i = 0; i < aliasKeys.length; i++) {
						if (aliasKeys[i].equals(aliasTableName)) {
	
							treeItem.setText(aliasValues[i]);
	
						}
					}
	
				}
	
			}
	
			if( memListTextMap.get(member.getKey()) != null){
				memListTextMap.remove(member.getKey());
				memListTextMap.put(member.getKey(), treeItem.getText());
			}
		}
	}

	/**
	 * Add control listeners.
	 * 
	 */
	private void addControlListeners() {

		dimensionTabFolder.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				setSelectionControls();
			}
		});
	}

	protected void onSelectedDimensionTreeControlChange() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && dimensionControlTreeMap.get(mapKeyDimName).getTree().getSelectionCount() == 1 ) { 
			enableAllMapControls(addButtonMap, true);
		}
		else {
			enableAllMapControls(addButtonMap, false);
		}
	}
	
	protected void onSelectedSecurityListControlChange() {

		populateSelectionTypeOptions();
		
		setSelectionControls();

	}

	public void setSelectionControls() {
		
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
		if( securityMemberList.getSelectionCount() == 1 ) {
			enableAllMapControls(removeButtonMap, true);
			enableAllMapControls(selectionOnlyMap, true);
			if( mapKeyDimName.equals(DimensionUtil.getVersionDimensionName(project))
					|| ( securityMemberList.getSelection()[0].equals(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS) 
					&& mapKeyDimName.equals(DimensionUtil.getYearDimensionName(project)) )
					|| securityMemberList.getSelection()[0].contains(PafBaseConstants.MEMBERLIST_TOKEN ) ) {
				enableAllMapControls(selectionChildrenMap,false);
				enableAllMapControls(justChildrenMap,false);
				enableAllMapControls(selectionDescendantsMap,false);
				enableAllMapControls(justDescendantsMap,false);
				enableAllMapControls(justLevelGenerationMap,false);
			}
			else {
				enableAllMapControls(selectionChildrenMap,true);
				enableAllMapControls(justChildrenMap,true);
				enableAllMapControls(selectionDescendantsMap,true);
				enableAllMapControls(justDescendantsMap,true);
				enableAllMapControls(justLevelGenerationMap,true);
			}
			if( selectionDescendantsMap.get(mapKeyDimName).getSelection() 
					|| justDescendantsMap.get(mapKeyDimName).getSelection() 
					|| justLevelGenerationMap.get(mapKeyDimName).getSelection() ) {
				enableAllMapControls(levelGenerationMap, true);
				enableAllMapControls(levelGenSpinnerMap, true);
			}
			else {
				enableAllMapControls(levelGenerationMap, false);
				enableAllMapControls(levelGenSpinnerMap, false);
			}
		}
		else {
			enableAllMapControls(removeButtonMap, false);
			enableAllMapControls(selectionOnlyMap, false);
			enableAllMapControls(selectionChildrenMap, false);
			enableAllMapControls(selectionDescendantsMap, false);
			enableAllMapControls(justChildrenMap, false);
			enableAllMapControls(justDescendantsMap, false);
			enableAllMapControls(justLevelGenerationMap, false);
			enableAllMapControls(levelGenerationMap, false);
			enableAllMapControls(levelGenSpinnerMap, false);
		}
		
		if( securityMemberList.getItemCount() > 0 ) {
			enableAllMapControls(clearButtonMap, true);
		}
		else {
			enableAllMapControls(clearButtonMap, false);
		}
	}

	/**
	 * 
	 * @param selected Is this update the result of a Selected or Unselected event button click.
	 */
	protected void updateSecurityMember(boolean selected) {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];

		String mapKeyDimName = selectedTabItem.getText();

		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() ) {
			List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
			
			if( securityMemberList != null && securityMemberList.getSelection().length == 1) {
			    	
				Map<String,String> memberAliasListMap = this.secSubMemberListTextMap.get(mapKeyDimName); 
				java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
				
				
				String secMember = securityMemberList.getSelection()[0];
				//This member could contain either a member name or member alias
				SecurityMember securityMember = new SecurityMember(mapKeyDimName,secMember);
				//This member will (eventually) only contain the member name
				SecurityMember tempSecurityMember = new SecurityMember(securityMember);
				if(memberAliasListMap != null && memberAliasListMap.containsKey(securityMember.getMember())){
					tempSecurityMember.setMember(memberAliasListMap.get(securityMember.getMember()));
				}
				int index = securityMemberList.getSelectionIndex();
				

				//TTN-2475 - Since the SecurityMember could be a member or alias.  
				//We will just the the tempSecurityMember, which we know is only a member name
				if( securityMemberArray.contains(tempSecurityMember) ){
					securityMemberArray.remove(tempSecurityMember);
				}
				
				//TTN-2475
				//This is an unselect event, so don't add anything new as that will be handled when the select event occurs!
				if(!selected) return;
				
				updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );
				tempSecurityMember.setSelectionType(securityMember.getSelectionType());
				tempSecurityMember.setLevelGenType(securityMember.getLevelGenType());
				tempSecurityMember.setLevelGenNumber(securityMember.getLevelGenNumber());
				
				//This is causing TTN-2475
				//We must do the comparison using the Security Member that we know is only a member name
				if( !securityMemberArray.contains(tempSecurityMember) ) {
					securityMemberArray.add(tempSecurityMember);
				    securityMemberMap.put(mapKeyDimName, securityMemberArray);
				}
					
				securityMemberList.setItem(index, securityMember.toString());
				
				setSelectedSecurityDimension(mapKeyDimName, securityMember.getMember());
				
				boldTreeItem(mapKeyDimName, securityMember.getMember(), true);
				
				if( mapKeyDimName.equals(DimensionUtil.getVersionDimensionName(project))
						|| (securityMember.getMember().equals(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS) 
						&& mapKeyDimName.equals(DimensionUtil.getYearDimensionName(project)) )
						|| securityMember.getMember().contains(PafBaseConstants.MEMBERLIST_TOKEN )) {
					enableAllMapControls(selectionChildrenMap,false);
					enableAllMapControls(justChildrenMap,false);
					enableAllMapControls(selectionDescendantsMap,false);
					enableAllMapControls(justDescendantsMap,false);
					enableAllMapControls(justLevelGenerationMap,false);
				}
				else {
					enableAllMapControls(selectionChildrenMap,true);
					enableAllMapControls(justChildrenMap,true);
					enableAllMapControls(selectionDescendantsMap,true);
					enableAllMapControls(justDescendantsMap,true);
					enableAllMapControls(justLevelGenerationMap,true);
				}
		    }
		}		
	}
	
	protected void populateSelectionTypeOptions() {
		
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		List secMemList = securityMemberControlMap.get(mapKeyDimName);
		
		if ( secMemList.isEnabled() && secMemList.getSelection().length == 1 ) {
			SecurityMember selectedSecMem = new SecurityMember(mapKeyDimName, secMemList.getSelection()[0]);
			
			populateSelectionTypeOptions(selectedSecMem, mapKeyDimName);
			
			setSelectedSecurityDimension(mapKeyDimName, selectedSecMem.getMember());
			
			boldTreeItem(mapKeyDimName, selectedSecMem.getMember(), true);
		}
	}

	protected void populateSelectionTypeOptions(SecurityMember securityMember, String dimension) {
		// if not null, otherwise select selection only
		if (securityMember.getSelectionType() != null) {

			// select correct selection type
			switch (securityMember.getSelectionType()) {

				case Selection:
					selectionOnlyMap.get(dimension).setSelection(true);
					selectionChildrenMap.get(dimension).setSelection(false);
					justChildrenMap.get(dimension).setSelection(false);
					selectionDescendantsMap.get(dimension).setSelection(false);
					justDescendantsMap.get(dimension).setSelection(false);
					justLevelGenerationMap.get(dimension).setSelection(false);
					break;
				case SelectionAndChildren:
					selectionOnlyMap.get(dimension).setSelection(false);
					selectionChildrenMap.get(dimension).setSelection(true);
					justChildrenMap.get(dimension).setSelection(false);
					selectionDescendantsMap.get(dimension).setSelection(false);
					justDescendantsMap.get(dimension).setSelection(false);
					justLevelGenerationMap.get(dimension).setSelection(false);
					break;
				case JustChildren:
					selectionOnlyMap.get(dimension).setSelection(false);
					selectionChildrenMap.get(dimension).setSelection(false);
					justChildrenMap.get(dimension).setSelection(true);
					selectionDescendantsMap.get(dimension).setSelection(false);
					justDescendantsMap.get(dimension).setSelection(false);
					justLevelGenerationMap.get(dimension).setSelection(false);
					break;
				case SelectionAndDescendants:
					selectionOnlyMap.get(dimension).setSelection(false);
					selectionChildrenMap.get(dimension).setSelection(false);
					justChildrenMap.get(dimension).setSelection(false);
					selectionDescendantsMap.get(dimension).setSelection(true);
					justDescendantsMap.get(dimension).setSelection(false);
					justLevelGenerationMap.get(dimension).setSelection(false);
					break;
				case JustDescendants:
					selectionOnlyMap.get(dimension).setSelection(false);
					selectionChildrenMap.get(dimension).setSelection(false);
					justChildrenMap.get(dimension).setSelection(false);
					selectionDescendantsMap.get(dimension).setSelection(false);
					justDescendantsMap.get(dimension).setSelection(true);
					justLevelGenerationMap.get(dimension).setSelection(false);
					break;
				case JustLevelORGeneration:
					selectionOnlyMap.get(dimension).setSelection(false);
					selectionChildrenMap.get(dimension).setSelection(false);
					justChildrenMap.get(dimension).setSelection(false);
					selectionDescendantsMap.get(dimension).setSelection(false);
					justDescendantsMap.get(dimension).setSelection(false);
					justLevelGenerationMap.get(dimension).setSelection(true);
					break;
				
				default:
					break;
	
			}
			
		} else {

			selectionOnlyMap.get(dimension).setSelection(true);
			selectionChildrenMap.get(dimension).setSelection(false);
			selectionDescendantsMap.get(dimension).setSelection(false);
			justChildrenMap.get(dimension).setSelection(false);
			justDescendantsMap.get(dimension).setSelection(false);
			justLevelGenerationMap.get(dimension).setSelection(false);
		}

		if (securityMember.getLevelGenType() != null) {

			Combo levelGenCombo = levelGenerationMap.get(dimension);
			levelGenCombo.setText(securityMember.getLevelGenType().toString());
			
			Spinner levelGenSpinner = levelGenSpinnerMap.get(dimension);
			
			if( dimensionControlTreeMap.get(dimension).getTree().getSelection().length > 0 ) {
				TreeItem selectedTreeItem = dimensionControlTreeMap.get(dimension).getTree().getSelection()[0];
				PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
				
				if( selectedMember.getPafSimpleDimMemberProps() != null ) {
					String levelGenrationItem = levelGenCombo.getItem(levelGenCombo.getSelectionIndex());
					Integer maxTreeDepth = maxTreeDepthMap.get(dimension);
					
					int minLevelGenSpinnerLength = 0;
					int maxLevelGenSpinnerLength = 0;
		
					if (levelGenrationItem.equals(LevelGenerationType.Level.toString())) {
		
						int selectedMemberLevelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
						minLevelGenSpinnerLength = 0;
						maxLevelGenSpinnerLength = selectedMemberLevelNumber;
		
					} else if (levelGenrationItem.equals(LevelGenerationType.Generation.toString())) {
		
						minLevelGenSpinnerLength = selectedMember.getPafSimpleDimMemberProps().getGenerationNumber();
						maxLevelGenSpinnerLength = maxTreeDepth + 1;
		
					}
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
					
					levelGenSpinner.setSelection(securityMember.getLevelGenNumber());
				}
			}
		}
	}
	
	protected void updateSelectionTypeForSecurityMember(SecurityMember securityMember, String dimension) {

		// select correct button
		if (selectionOnlyMap.get(dimension).getEnabled() && selectionOnlyMap.get(dimension).getSelection()) {
			
			securityMember.setSelectionType(SelectionType.Selection);

		} else if (selectionChildrenMap.get(dimension).getEnabled() && selectionChildrenMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.SelectionAndChildren);

		} else if (justChildrenMap.get(dimension).getEnabled() && justChildrenMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.JustChildren);

		} else if (selectionDescendantsMap.get(dimension).getEnabled() && selectionDescendantsMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.SelectionAndDescendants);

		} else if (justDescendantsMap.get(dimension).getEnabled() && justDescendantsMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.JustDescendants);

		} else if (justLevelGenerationMap.get(dimension).getEnabled() && justLevelGenerationMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.JustLevelORGeneration);

		}	
		
		if ( ( selectionDescendantsMap.get(dimension).getEnabled() && selectionDescendantsMap.get(dimension).getSelection() )
				|| (justDescendantsMap.get(dimension).getEnabled() && justDescendantsMap.get(dimension).getSelection())
				|| (justLevelGenerationMap.get(dimension).getEnabled() && justLevelGenerationMap.get(dimension).getSelection()) ){
			if( dimensionControlTreeMap.get(dimension).getTree().getSelection().length > 0 ) {
				TreeItem selectedTreeItem = dimensionControlTreeMap.get(dimension).getTree().getSelection()[0];
			
				PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
	
				if( selectedMember.getPafSimpleDimMemberProps() != null ) {
					
					Combo levelGenCombo = levelGenerationMap.get(dimension);
					Spinner levelGenSpinner = levelGenSpinnerMap.get(dimension);
		
					String levelGenrationItem = levelGenCombo.getItem(levelGenCombo.getSelectionIndex());
		
					int currentLevelGenSpinnerNumber = levelGenSpinner.getSelection();
		
					Integer maxTreeDepth = maxTreeDepthMap.get(dimension);
		
					int minLevelGenSpinnerLength = 0;
		
					int maxLevelGenSpinnerLength = 0;
		
					if (levelGenrationItem.equals(LevelGenerationType.Bottom.toString())) {
		
						securityMember.setLevelGenType(LevelGenerationType.Bottom);
		
						int selectedMemberLevelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
		
						minLevelGenSpinnerLength = 0;
						maxLevelGenSpinnerLength = selectedMemberLevelNumber;
					} else if (levelGenrationItem.equals(LevelGenerationType.Level.toString())) {
		
						securityMember.setLevelGenType(LevelGenerationType.Level);
		
						int selectedMemberLevelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
		
						minLevelGenSpinnerLength = 0;
						maxLevelGenSpinnerLength = selectedMemberLevelNumber;
		
					} else if (levelGenrationItem.equals(LevelGenerationType.Generation.toString())) {
		
						securityMember.setLevelGenType(LevelGenerationType.Generation);
		
						minLevelGenSpinnerLength = selectedMember.getPafSimpleDimMemberProps().getGenerationNumber();
						maxLevelGenSpinnerLength = maxTreeDepth + 1;
		
					}
		
					levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
					levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
		
					if (currentLevelGenSpinnerNumber < minLevelGenSpinnerLength) {
						currentLevelGenSpinnerNumber = minLevelGenSpinnerLength;
					} else if (currentLevelGenSpinnerNumber > maxLevelGenSpinnerLength) {
						currentLevelGenSpinnerNumber = maxLevelGenSpinnerLength;
					}
		
					securityMember.setLevelGenNumber(currentLevelGenSpinnerNumber);
		
					levelGenSpinner.setSelection(currentLevelGenSpinnerNumber);
				}
			}
		}
	}

	/**
	 * Called when any item's selection status changes on the security form.
	 * 
	 */

	protected void addNewSecurityMember() {

		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];

		String mapKeyDimName = selectedTabItem.getText();

		unboldDimensionTreeItems(mapKeyDimName);

		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() 
				&& dimensionControlTreeMap.get(mapKeyDimName).getTree().getSelection().length > 0 ) {

			// get selected tree item name
			TreeItem selectedTreeItem = dimensionControlTreeMap.get(mapKeyDimName).getTree().getSelection()[0];
			
			if( selectedTreeItem.getText().equals(Constants.MEMBER_LISTS) ) 
				return;
			
			PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
			// get selected tree item name
			String selectedTreeItemName = selectedMember.getKey();
			// bold newly selected tree item
			boldTreeItem(mapKeyDimName, selectedTreeItemName, true);
			//Security Member name (no alias)
			SecurityMember securityMember = new SecurityMember();
			securityMember.setDimensionName(mapKeyDimName);
			securityMember.setMember(selectedTreeItemName);

			if( mapKeyDimName.equals(DimensionUtil.getVersionDimensionName(project))
					|| (securityMember.getMember().equals(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS) 
					&& mapKeyDimName.equals(DimensionUtil.getYearDimensionName(project))) 
					|| securityMember.getMember().contains(PafBaseConstants.MEMBERLIST_TOKEN )) {
				selectionOnlyMap.get(mapKeyDimName).setSelection(true);
				selectionChildrenMap.get(mapKeyDimName).setSelection(false);
				justChildrenMap.get(mapKeyDimName).setSelection(false);
				selectionDescendantsMap.get(mapKeyDimName).setSelection(false);
				justDescendantsMap.get(mapKeyDimName).setSelection(false);
				justLevelGenerationMap.get(mapKeyDimName).setSelection(false);
				enableAllMapControls(selectionChildrenMap,false);
				enableAllMapControls(justChildrenMap,false);
				enableAllMapControls(selectionDescendantsMap,false);
				enableAllMapControls(justDescendantsMap,false);
				enableAllMapControls(justLevelGenerationMap,false);
			}
			else {
				enableAllMapControls(selectionChildrenMap,true);
				enableAllMapControls(justChildrenMap,true);
				enableAllMapControls(selectionDescendantsMap,true);
				enableAllMapControls(justDescendantsMap,true);
				enableAllMapControls(justLevelGenerationMap,true);
			}

			if( ! selectionOnlyMap.get(mapKeyDimName).getSelection() 
					&& ! selectionChildrenMap.get(mapKeyDimName).getSelection() 
					&& ! justChildrenMap.get(mapKeyDimName).getSelection() 
					&& ! selectionDescendantsMap.get(mapKeyDimName).getSelection() 
					&& ! justDescendantsMap.get(mapKeyDimName).getSelection()
					&& ! justLevelGenerationMap.get(mapKeyDimName).getSelection() ) {
				selectionOnlyMap.get(mapKeyDimName).setSelection(true);
			}

			updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );
			
			List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
			
			// get security member map
			if (securityMemberMap == null ) {
				securityMemberMap = new HashMap<String,  java.util.List<SecurityMember>>();
			}
			//This list contains the list of selected Member names (no alias)
			java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
			if( securityMemberArray == null ) 
				securityMemberArray = new ArrayList<SecurityMember>();
			Map<String,String> memberAliasListMap = this.secSubMemberListTextMap.get(mapKeyDimName);
			//This string could be the MemberName or the Member Alias.
		    String temp = securityMember.toString().replace(selectedTreeItemName, selectedTreeItem.getText());
		    
	    	boolean foundInList = false;
	    	for( String member : securityMemberList.getItems() ) {
	    		if( member.equals(temp) ) {
	    			foundInList = true;
	    			break;
	    		}
	    	}
	    	
	    	if( ! foundInList ) {
		    	securityMemberArray.add(securityMember);
		    	securityMemberList.add(temp);
		    	memberAliasListMap.put(selectedTreeItem.getText(), selectedTreeItemName);
			    securityMemberMap.put(mapKeyDimName, securityMemberArray);
			    secSubMemberListTextMap.put(mapKeyDimName, memberAliasListMap);
	    	}

	    	securityMemberList.setSelection(new String[] {temp});
	    	
	    	enableAllMapControls(removeButtonMap, true);
			enableAllMapControls(clearButtonMap, true);
		}	
	}

	/**
	 * Populates the security form based ont the role
	 * 
	 * @param roleName
	 *            Role Name
	 */
	protected void populateSecurityForm() {

		TabItem[] tabItems = dimensionTabFolder.getItems();

		// for each hier dim
		for (TabItem tabItem : tabItems) {

			// get map key dimension name
			String mapKeyDimName = tabItem.getText();

			// try go get a security member map from role security map
			// if null, create a new one
			if (securityMemberMap == null) {
				securityMemberMap = new HashMap<String, java.util.List<SecurityMember>>();
			}

			// try to get security members for the specified dimension
			java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
			// if not null, add each security member into the Security Specificaiton List Control
			if (securityMemberArray != null && securityMemberArray.size() > 0 ) {

				// get tree item map
				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(mapKeyDimName);
				for( SecurityMember securityMember : securityMemberArray ) {
					// if security members member is not null
					if (securityMember.getMember() != null) {
	
						// if tree item maps exist
						if (treeItemMap != null) {
	
							TreeItem securityMemberTreeItem = treeItemMap.get(securityMember.getMember());
	
							// if tree item found, get set selection to it
							if (securityMemberTreeItem != null) {
	
								// bold tree item
								boldTreeItem(securityMemberTreeItem, true);
								
							}
							//JIra 1770-AC needs to handle when a security member loaded from XML does not exist in EssBase outline
							List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
							Map<String,String> memberAliasListMap = this.secSubMemberListTextMap.get(mapKeyDimName);
							if( ! Arrays.asList(securityMemberList.getItems()).contains(securityMember.toString()) ){
								securityMemberList.add(securityMember.toString());
								securityMemberList.setSelection(new String[] {securityMember.toString()});
								
								//TTN-2480 Fix for duplicate Alias members.
								if(memberAliasListMap.containsValue(securityMember.toString()))
									securityMemberList.remove(securityMember.toString());
								
								if( ! securityMember.toString().equals(PafBaseConstants.UOW_ROOT) ) {
									if(securityMember.toString().indexOf(',') != -1){
										String temp = securityMember.toString().substring(securityMember.toString().lastIndexOf('(') + 1, securityMember.toString().indexOf(','));
										memberAliasListMap.put(temp, temp);
									} else if (securityMember.toString().indexOf('(') != -1){
										String temp = securityMember.toString().substring(securityMember.toString().lastIndexOf('(') + 1, securityMember.toString().indexOf(')'));
										memberAliasListMap.put(temp, temp);
									} else {
										memberAliasListMap.put(securityMember.toString(), securityMember.toString());
									}
								}
							}
			
							populateSelectionTypeOptions(securityMember, mapKeyDimName );
							
							updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );

						}
					}
				}//end of for loop
			}
		}
	}

	private void enableLevelGenerationControls(String mapKeyDimName,
			boolean isEnabled, int selectionIndex) {

		levelGenerationMap.get(mapKeyDimName).setEnabled(isEnabled);
		levelGenSpinnerMap.get(mapKeyDimName).setEnabled(isEnabled);
		if( currentAdminLock == null ) {
			switch(selectionIndex) {
				case 0:
				case 1:
				case 2:
					levelGenerationMap.get(mapKeyDimName).setText("");
					break;
				case 3:
				case 4:
					levelGenerationMap.get(mapKeyDimName).select(0);
					break;
				case 5:
					levelGenerationMap.get(mapKeyDimName).select(1);
					break;
				default:
					break;
			}
		}
	}

	private MouseListener getTabTreeMouseListener() {

		if (tabTreeMouseListener == null) {

			tabTreeMouseListener = new MouseListener() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void mouseDoubleClick(MouseEvent e) {

					addNewSecurityMember();
					setSelectionControls();
					editorModified();

				}

				@Override
				public void mouseDown(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseUp(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

			};

		}

		return tabTreeMouseListener;
	}
	/**
	 * The tree item should be bolded / unbolded based on bold flag. Then the
	 * parent item should be bolded / unbolded based on if any of it's children
	 * are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded
	 */

	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		Font font = treeItem.getFont();

		// get font data ar for tree item
		FontData[] fontDataAr = font.getFontData();

		if (fontDataAr.length > 0) {

			// get font data
			FontData fontData = fontDataAr[0];

			// if bold is true, set style to bold, else to none
			if (bold) {

				fontData.setStyle(SWT.BOLD);

				if (boldFont == null) {

					boldFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(boldFont);

			} else {

				fontData.setStyle(SWT.NONE);

				if (normalFont == null) {

					normalFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(normalFont);
			}

		}

	}

	/**
	 * Bolds tree item.
	 * 
	 * @param dimensionName
	 * @param treeItemName
	 * @param bold
	 */
	private void boldTreeItem(String dimensionName, String treeItemName,
			boolean bold) {

		if (dimensionMapOfTreeItemMap != null) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);

			if (treeItemMap != null) {

				TreeItem treeItem = treeItemMap.get(treeItemName);

				if (treeItem != null) {

					boldTreeItem(treeItem, bold);
					dimensionControlTreeMap.get(dimensionName).getTree().setSelection(treeItem);

				}

			}

		}

	}

	/**
	 * Unbolds tree item.
	 * 
	 * @param dimensionName
	 */
	private void unboldDimensionTreeItems(String dimensionName) {

		if (dimensionMapOfTreeItemMap != null) {

			// for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
					.get(dimensionName);

			if (treeItemMap != null) {

				for (TreeItem treeItem : treeItemMap.values()) {

					boldTreeItem(treeItem, false);

				}

			}

		}

	}

	/**
	 * Unbolds all tree items.
	 * 
	 */
	private void unboldAllDimensionTreeItems() {

		if (dimensionMapOfTreeItemMap != null) {

			for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
						.get(dimensionName);

				if (treeItemMap != null) {

					for (TreeItem treeItem : treeItemMap.values()) {

						boldTreeItem(treeItem, false);

					}

				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {

		if (boldFont != null) {
			boldFont.dispose();
		}

		if (normalFont != null) {
			normalFont.dispose();
		}

		//Commented for the exception started from Eclipse 4.3, plus this seems no use either.
//		try {
//			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//			if( page != null ) {
//				InvalidSecurityMemberView invalidSecurityMemberView = (InvalidSecurityMemberView) PlatformUI
//						.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(InvalidSecurityMemberView.ID);
//	
//				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().hideView(invalidSecurityMemberView);
//			}
//		} catch (PartInitException e) {
//			e.printStackTrace();
//		}
//
		super.dispose();
	}

	/**
	 * Populates the contorls with values. Called only when editor opens.
	 * 
	 */
	private void populateControlValues() {
		
		if( adminLockInput.isNew() ) {
			textAdminLockName.setText("");
			isDirty = true;
		}
		else if( adminLockInput.isClone()) {
			textAdminLockName.setText("Copy of " + adminLockInput.getName());
			isDirty = true;
		}
		else {
			textAdminLockName.setText( adminLockInput.getName());
			isDirty = false;
		}
			
		// role configurations
		String[] roleConfigNames = adminLockInput.getRoleConfigNames();
		if ( roleConfigNames != null ) {
			EditorControlUtil.addItemsToList(availableRolesListControl, roleConfigNames);
		}
		
		if( ! adminLockInput.isNew() ) {
			
			java.util.List<String> selectedRoleConfigNames = adminLockInput.getAdminLockRoleConfigs();
			
			if( selectedRoleConfigNames != null && selectedRoleConfigNames.size() > 0 ) {
				
				java.util.List<String> roleNameList = new ArrayList<String>(Arrays.asList(roleConfigNames));
				roleNameList.removeAll(selectedRoleConfigNames);
				EditorControlUtil.addItemsToList(availableRolesListControl, roleNameList.toArray(new String[0]));
				EditorControlUtil.addItemsToList(selectedRolesListControl, selectedRoleConfigNames.toArray(new String[0]));
				
			}
		}

		try {

			if( currentAdminLock != null ) {
				java.util.List<PafDimSpec> adminLockSpecs = currentAdminLock.getDimSpecList();
				if( adminLockSpecs != null && adminLockSpecs.size() > 0 ) {
					Set<InvalidSpecificationMember> invalidMemberSet = new LinkedHashSet<InvalidSpecificationMember>();
					for( PafDimSpec pafDimSpec : adminLockSpecs ) {
						String dimensionName = pafDimSpec.getDimension();
						Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);
						// verify security member is still
						// valid member in outline
						if (dimTreeItemMap != null) {
							if (pafDimSpec.getExpressionList() != null
									&& pafDimSpec.getExpressionList().length > 0) {
								String[] secMemStrAry = pafDimSpec.getExpressionList();
								java.util.List<SecurityMember> secMemObjAry = new ArrayList<SecurityMember>();
								for( String secMem : secMemStrAry ) {
									SecurityMember securityMember = new SecurityMember(dimensionName, secMem);
									if (!dimTreeItemMap.containsKey(securityMember.getMember())) {
										InvalidSpecificationMember invalidSpecMember = new InvalidSpecificationMember();
										invalidSpecMember.setDimensionName(securityMember.getDimensionName());
										invalidSpecMember.setMemberName(securityMember.getMember());
										invalidMemberSet.add(invalidSpecMember);
									}

									if( ! secMemObjAry.contains(securityMember)) {
										secMemObjAry.add(securityMember);
										securityMemberMap.put(dimensionName, secMemObjAry);
									}
								}
							}
						}
					}
					if( invalidMemberSet.size() > 0 ) {
						
						Shell shell = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell();

						InvalidSpecMemberErrorDialog invalidSpecMemberErrorDialog = new InvalidSpecMemberErrorDialog(
								shell, invalidMemberSet);
						invalidSpecMemberErrorDialog.open();

						InvalidSpecificationMemberView invalidSpecMemberView;
						try {
							invalidSpecMemberView = (InvalidSpecificationMemberView) PlatformUI
									.getWorkbench().getActiveWorkbenchWindow()
									.getActivePage().showView(
											InvalidSpecificationMemberView.ID);
							invalidSpecMemberView.setInvalidSpecMemberSet(invalidMemberSet);
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return;
					}
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Sets the dirty flag as fires the propery changes, which signals to the
	 * program that the editor is dirty.
	 */
	public void editorModified() {
		boolean wasDirty = isDirty();
		setDirty( true );
		if (!wasDirty) {
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	/**
	 * Implementation of EditorPart.doSave.
	 * 
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if( currentAdminLock == null ) {
			currentAdminLock = new AdminPersistLockDef();
		}
		
		if( textAdminLockName.getText().isEmpty() ) {
	    	GUIUtil.openMessageWindow( "Save Error", "Missing name for the Admin Lock.");
			monitor.setCanceled(true);
	    	return;
		}
		
		//Added code below because new admin locks don't get a name set.
		//Fixed bug created by TTN-2386 below.
		if(adminLockInput.isNew() && currentAdminLock.getName() == null){
			currentAdminLock.setName(textAdminLockName.getText());
		}
		
		//TTN-2386	Admin Lock Renaming Issue related with Role Config
		if( ! adminLockInput.isNew() && ! adminLockInput.isClone() 
				&& ! currentAdminLock.getName().equals(textAdminLockName.getText()) ) {
			java.util.List<String> selectedAdminLockRoleConfigs = Arrays.asList(selectedRolesListControl.getItems());
			boolean isConfirmed = false;
			if( selectedAdminLockRoleConfigs != null && selectedAdminLockRoleConfigs.size() > 0 ) {
				isConfirmed = MessageDialog.openConfirm(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), "Rename Admin Lock Confirmation", 
						"You are renaming an admin lock that's referenced by role configuration(s): '" + StringUtils.arrayListToString(selectedAdminLockRoleConfigs, ",") + "'. Do you want to continue?");
				if( ! isConfirmed ) {
					monitor.setCanceled(true);
			    	return;
				}
			}
			currentAdminLock.setName(textAdminLockName.getText());
		} 
		
		//1. update existing admin lock or add a new admin lock
		if (securityMemberMap != null) {
			Set<InvalidSpecificationMember> invalidMemberSet = new LinkedHashSet<InvalidSpecificationMember>();
			for (String dimensionName : securityMemberControlMap.keySet()) {
				List securityMemberList = securityMemberControlMap.get(dimensionName);
				if( securityMemberList.getItemCount() == 0 ) {
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "Please choose members for all dimensions selected including the attribute dimensions. Else, remove the attribute dimensions.", MessageDialog.ERROR);
					monitor.setCanceled(true);
					return;
				}
				
				Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);
				if (dimTreeItemMap != null) {
					// verify security member is still valid member in outline
					java.util.List<SecurityMember> securityMembeArray = securityMemberMap.get(dimensionName);
					for( SecurityMember securityMember : securityMembeArray ) {
						if (!dimTreeItemMap.containsKey(securityMember.getMember())) { 
							InvalidSpecificationMember invalidSpecMember = new InvalidSpecificationMember();
							invalidSpecMember.setDimensionName(securityMember.getDimensionName());
							invalidSpecMember.setMemberName(securityMember.getMember());
							invalidMemberSet.add(invalidSpecMember);
						}
					}
				}
			}
			if( invalidMemberSet.size() > 0 ) {
				
				Shell shell = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell();

				InvalidSpecMemberErrorDialog invalidSpecMemberErrorDialog = new InvalidSpecMemberErrorDialog(
						shell, invalidMemberSet);
				invalidSpecMemberErrorDialog.open();

				InvalidSpecificationMemberView invalidSpecMemberView;
				try {
					invalidSpecMemberView = (InvalidSpecificationMemberView) PlatformUI
							.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().showView(
									InvalidSpecificationMemberView.ID);
					invalidSpecMemberView.setInvalidSpecMemberSet(invalidMemberSet);
				} catch (PartInitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				monitor.setCanceled(true);
				return;
			}
		}

		if (securityMemberMap != null) {
			
			dimSpecList.clear();
			
			for (String dimensionName : securityMemberMap.keySet()) {
				java.util.List<SecurityMember> securityMembeArray = securityMemberMap.get(dimensionName);
				java.util.List<String> expList = new java.util.ArrayList<String>();
				for( SecurityMember securityMember : securityMembeArray) {
					expList.add(securityMember.toString());
				}
				PafDimSpec pafDimSpec = new PafDimSpec();
				pafDimSpec.setDimension(dimensionName);
				pafDimSpec.setExpressionList(expList.toArray(new String[0]));
				dimSpecList.add(pafDimSpec);
				
			}
			currentAdminLock.setDimSpecList(dimSpecList);
			adminLockInput.setCurrentAdminLock(currentAdminLock);
			
			//2. update role config with the current admin lock
			if( selectedRolesListControl == null || selectedRolesListControl.getItems() == null || selectedRolesListControl.getItems().length == 0 || selectedRolesListControl.getItemCount() == 0 ) {
				if( ! GUIUtil.openMessageWindowOkCancel(Constants.DIALOG_WARNING_HEADING, "No role configuration has been assigned. Continue?") ) {
					monitor.setCanceled(true);
					return;
				}
			}
			
			//Remove admin lock from role config if role config has been removed
			java.util.List<String> selectedAdminLockRoleConfigs = Arrays.asList(selectedRolesListControl.getItems());
			for( String oldRoleConfig : adminLockRoleConfigs ) {
				if( ! selectedAdminLockRoleConfigs.contains(oldRoleConfig) ) {
					adminLockInput.getPlanConfigModelManager().removeAdminLockFromRoleConfig(oldRoleConfig, currentAdminLock.getName());
				}
			}
			
			//Add admin lock to a new role config if a role config has been added
			//06/04/2014 - KRM remove this code because it stops copied AdminLock from updating role config.  Replaced with code below.
			/*			
 			for( String newRoleConfig : selectedAdminLockRoleConfigs ) {
				if( adminLockRoleConfigs.size() == 0 || ! adminLockRoleConfigs.contains(newRoleConfig) ) {
					adminLockInput.getPlanConfigModelManager().addAdminLockToRoleConfig(newRoleConfig, currentAdminLock.getName());
				}
			}
			*/
			
			//TTN-2464
			for( String newRoleConfig : selectedAdminLockRoleConfigs ) {
				Set<String> currentAdminLocks = adminLockInput.getPlanConfigModelManager().getAdminLocksFromRoleConfig(newRoleConfig);
				if(currentAdminLocks == null || currentAdminLocks.size() == 0 || !currentAdminLocks.contains(newRoleConfig) ) {
					adminLockInput.getPlanConfigModelManager().addAdminLockToRoleConfig(newRoleConfig, currentAdminLock.getName());
				}
			}

			adminLockInput.setAdminLockRoleConfigs(selectedAdminLockRoleConfigs);
			adminLockRoleConfigs.clear();
			adminLockRoleConfigs.addAll(selectedAdminLockRoleConfigs);
			try {
				adminLockInput.save();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			setDirty( false );
			
			firePropertyChange(IEditorPart.PROP_DIRTY);
			
			setPartName(Constants.MENU_ADMIN_LOCK + ": " + textAdminLockName.getText());
			
			updateTreeViewer();
			
			adminLockInput.setNew(false);
			adminLockInput.setClone(false);
		}

	}

	/**
	 * Updates the tree viewer control whenever a planner role is added or
	 * modified.
	 */
	private void updateTreeViewer() {
		if (viewer != null && treeNode != null) {
			if( treeNode instanceof AdminLocksNode ) {
				treeNode.createChildren(null);
				viewer.refresh(treeNode);
			}
			else if( treeNode instanceof AdminLockNode ) {
				if( ! treeNode.getName().equals(currentAdminLock.getName()) ) {
					AdminLocksNode node = (AdminLocksNode)treeNode.getParent();
					node.createChildren(null);
					viewer.refresh(node);
				}
				else {
					treeNode.createChildren(null);
					viewer.refresh(treeNode);
				}
			}
		}
	}

	/**
	 * @return the treeNode
	 */
	public TreeNode getTreeNode() {
		return treeNode;
	}

	/**
	 * @param treeNode
	 *            the treeNode to set
	 */
	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	/**
	 * Sets the selected role
	 * TTN-749
	 * @param role
	 */
	public void setSelectedRole(String role) {

		if (role != null && selectedRolesListControl != null
				&& !selectedRolesListControl.isDisposed()) {

			selectedRolesListControl.setSelection(new String[] { role });
		}

	}

	/**
	 * Selects the tab with the specified string as text
	 * TTN-749
	 * @param tab
	 */
	public void setSelectedTab(String tab) {

		if (tab == null || dimensionTabFolder == null) {
			return;
		}

		TabItem[] tabItems = dimensionTabFolder.getItems();

		int index = -1;

		for (int i = 0; i < dimensionTabFolder.getItemCount(); i++) {
			if (tab.startsWith(tabItems[i].getText())) {
				index = i;
			}
		}
		if (index >= 0) {
			dimensionTabFolder.setSelection(index);
		}

	}


	/**
	 * Selects the tab with the specified string as text
	 * TTN-749
	 * @param tab
	 */
	public void setSelectedSecurityDimension(String dim, String selectedSecurity) {

		unboldAllDimensionTreeItems();
		
		if (selectedSecurity == null || dimensionTabFolder == null) {
			return;
		}
		Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dim);
		if (treeItemMap != null) {
			TreeItem securityMemberTreeItem = treeItemMap.get(selectedSecurity);
			if(securityMemberTreeItem == null){
				for(TreeItem item : treeItemMap.values()){
					PafSimpleDimMember mem = (PafSimpleDimMember) item.getData();
					if(mem != null && mem.getPafSimpleDimMemberProps() != null 
							&& mem.getPafSimpleDimMemberProps().getAliasValues().contains(selectedSecurity)){
						securityMemberTreeItem = item;
						break;
					}
				}
			}
//			if (securityMemberTreeItem != null) {
//				dimensionControlTreeMap.get(dim).getTree().setSelection(securityMemberTreeItem);
//				boldTreeItem(securityMemberTreeItem, true);
//			}
		}
	}
	
	public void setSelectedSecurity(String dim, String selectedSecurity) {
		List securityMemberList = securityMemberControlMap.get(dim);
		securityMemberList.setSelection(new String[] {selectedSecurity});
	}
	
	/*
	 * 
	 */
	private void resizeForm(ControlEvent e){
		
		if ( scrolledChildComposite != null && scrolledComposite != null ) {
		
			scrolledComposite.setMinSize(scrolledChildComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			
		}
	}

	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

}

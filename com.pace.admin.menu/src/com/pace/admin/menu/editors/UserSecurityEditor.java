/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.ListErrorMessagesDialog;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.security.PaceUser;
import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.global.composite.FindTreeModule;
import com.pace.admin.menu.dialogs.InvalidMemberErrorDialog;
import com.pace.admin.menu.editors.input.UserSecurityEditorInput;
import com.pace.admin.menu.editors.internal.InvalidSecurityMember;
import com.pace.admin.menu.editors.internal.SecurityMember;
import com.pace.admin.menu.editors.internal.SelectionType;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.views.InvalidSecurityMemberView;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafDimSpec;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafNotAuthenticatedSoapException_Exception;
import com.pace.server.client.PafNotAuthorizedSoapException_Exception;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.ValidateUserSecurityRequest;
import com.pace.server.client.ValidationResponse;
import com.swtdesigner.ResourceManager;
import com.swtdesigner.SWTResourceManager;

/**
 * The User Security Editor allows an admin at assign security roles to database
 * users. A database user has no security defined by default so an admin has to
 * assign security to them. An admin will assign roles to a database user. Each
 * role has a list of hier dimensions and security needs to be defined for each
 * dim. For example, if the role was Planner, and the hier dims were product and
 * location, the security for product might be
 * 
 * @IDESC(DIV09, L0), which would provide that user all of DIV09 and descendants
 *               to level 0. This is a GUI for paf_security.xml. The Hier dims
 *               are populated dynamically for the paf_apps.xml. I used a lot of
 *               dynamic maps to maintain the list of roles and security members
 *               for each hier dim.
 * 
 * @author jmilliron
 * 
 */
public class UserSecurityEditor extends EditorPart {
	public UserSecurityEditor() {
	}
	
	private static Logger logger = Logger.getLogger(UserSecurityEditor.class);

	// logger
	private Combo new_DomainFilterCombo;
	
	private Combo new_SecurityGroupFilterCombo;
	
	private Label usernameLabel;

	private Combo usernameCombo;

	private Button adminCheckBox;
	
	private Map<String, FindTreeModule> findTreeModuleMap = new HashMap<String, FindTreeModule>();

	// set of hier dim names
	private Set<String> dimensionNameSet = new HashSet<String>();

	// map to hold security member (@IDESC(DIV09, 0) which keys on role
	private Map<String, List> securityMemberControlMap = new HashMap<String, List>();
	
	private Map<String, Map<String, String>> secSubMemberListTextMap = new HashMap<String, Map<String, String>>();

	// map to hld the dimension controls (SWT Tree) for hier dims
	private Map<String, Tree> dimensionControlTreeMap = new HashMap<String, Tree>();

	// map to selction only buttons, keyed on dim name
	private Map<String, Button> selectionOnlyMap = new HashMap<String, Button>();

	// map to selecitonChildren buttons, keyed on dim name
	private Map<String, Button> selectionChildrenMap = new HashMap<String, Button>();

	// map to selectionDescendants buttons, keyed on dim name
	private Map<String, Button> selectionDescendantsMap = new HashMap<String, Button>();

	// level gen drop down map. keyd on dim name
	private Map<String, Combo> levelGenerationMap = new HashMap<String, Combo>();

	// level spinner map. keyd on dim name
	private Map<String, Spinner> levelGenSpinnerMap = new HashMap<String, Spinner>();

	private Map<String, Button> applyAliasTableMap = new HashMap<String, Button>();

	private Map<String, Combo> aliasTableComboMap = new HashMap<String, Combo>();

	private Map<String, Button> addButtonMap = new HashMap<String, Button>();

	private Map<String, Button> removeButtonMap = new HashMap<String, Button>();

	private Map<String, Button> clearButtonMap = new HashMap<String, Button>();

	private Map<String, Button> validateButtonMap = new HashMap<String, Button>();
	
	private List availableRolesListControl;
	
	private List selectedRolesListControl;
	
	// holds all of the hier dim tabs
	private TabFolder dimensionTabFolder;

	// dim viewer on tab
	private TreeViewer viewer;

	// flag used to tell if state is dirty
	private boolean isDirty = false;

	// input for editor
	private UserSecurityEditorInput userSecurityInput;

	// map to hold another map of security members keyd on hier dim, but this
	// map is keyd on role name
	private Map<String, Map<String, java.util.List<SecurityMember>>> roleSecurityMap = new HashMap<String, Map<String, java.util.List<SecurityMember>>>();

	// map to hold another map of selected tree items keyd on hier dim, but this
	// map is keyd on hier dim name
	private Map<String, Map<String, TreeItem>> dimensionMapOfTreeItemMap = new HashMap<String, Map<String, TreeItem>>();

	// id of editor
	public static final String ID = "com.pace.admin.menu.editors.UserSecurityEditor"; //$NON-NLS-1$

	// general modify listener
//	private ModifyListener modifyListener;

	// general form selection adapter
	private SelectionAdapter formSelectionAdapter;

	// general selection adaptor for role and tab selection changes
//	private SelectionAdapter roleAndTabSelectionAdapter;

	// general selection adapter for tabs/trees
//	private SelectionAdapter tabTreeSelectionAdapter;

	private MouseListener tabTreeMouseListener;
	
	// map to hold the max number of levels
	private Map<String, Integer> maxTreeDepthMap = new HashMap<String, Integer>();

	// bold font
	private Font boldFont;

	// standard font
	private Font normalFont;

	// tree node
	private TreeNode treeNode;

	//edit user type value label
	private Label edit_UserTypeValueLabel;

	private Button displayAllFilterButton;
	
	private String lastDomainNameSelected = null;
	
	private String lastSecurityGroupSelected = null;
	
	private Composite scrolledChildComposite;
	
	private ScrolledComposite scrolledComposite;

	/**
	 * Create contents of the editor part
	 * 
	 * @param parent
	 *            Parent composite
	 */
	@Override
	public void createPartControl(Composite parent) {

		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		parent.setLayout(gridLayout);

		scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		//scrolledComposite.getVerticalBar().setIncrement(20);
		scrolledComposite.setAlwaysShowScrollBars(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setExpandHorizontal(true);
		final GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_scrolledComposite.heightHint = 600;
		scrolledComposite.setLayoutData(gd_scrolledComposite);

		scrolledChildComposite = new Composite(scrolledComposite, SWT.NONE);
		scrolledChildComposite.setLayout(new GridLayout());
		scrolledChildComposite.setSize(484, 339);
		scrolledComposite.setContent(scrolledChildComposite);
		
		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				resizeForm(e);
			}
		});
		
		final Composite composite = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.CENTER, true,
				false);
		gridData_2.heightHint = 25;
		gridData_2.widthHint = 200;
		composite.setLayoutData(gridData_2);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 2;
		composite.setLayout(gridLayout_1);

		final Label roleInformationCompositeLabel = new Label(composite,
				SWT.NONE);
		roleInformationCompositeLabel.setForeground(SWTResourceManager
				.getColor(0, 0, 255));
		roleInformationCompositeLabel.setText("User Security Configuration");

		final Label roleInformationCompositeSeparator = new Label(composite,
				SWT.HORIZONTAL | SWT.SEPARATOR);
		final GridData gridData_3 = new GridData(SWT.FILL, SWT.CENTER, true,
				false);
		gridData_3.widthHint = 159;
		roleInformationCompositeSeparator.setLayoutData(gridData_3);
		roleInformationCompositeSeparator.setText("Label");

		final Composite composite_1 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true,
				false);
		gridData.heightHint = 111;
		gridData.widthHint = 590;
		composite_1.setLayoutData(gridData);
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginTop = 2;
		gridLayout_2.numColumns = 4;
		gridLayout_2.marginHeight = 0;
		composite_1.setLayout(gridLayout_2);		
		
		if (userSecurityInput.isNew() || userSecurityInput.isClone() ) {

			final Label domainFilterLabel = new Label(composite_1, SWT.NONE);
			domainFilterLabel.setText("Domain Filter:");
	
			new_DomainFilterCombo = new Combo(composite_1, SWT.READ_ONLY);
			new_DomainFilterCombo.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent arg0) {
					
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
							new Runnable() {
												
						public void run() {
						
							populateSecurityGroupFilterCombo();
							
						}
						
					});
		
				}
			});
			
			final GridData gridData_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
			gridData_1.minimumWidth = 70;
			new_DomainFilterCombo.setLayoutData(gridData_1);
	
			final Label filterLabel = new Label(composite_1, SWT.NONE);
			final GridData gridData_4 = new GridData(SWT.RIGHT, SWT.CENTER, false, false);
			filterLabel.setLayoutData(gridData_4);
			filterLabel.setText("Security Group Filter:");
		
			new_SecurityGroupFilterCombo = new Combo(composite_1, SWT.READ_ONLY);
			new_SecurityGroupFilterCombo.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent arg0) {
					
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
							new Runnable() {
												
						public void run() {
							
							populateUserNamesCombo();
							
						}
						
					});
		
					
				}
			});
			new_SecurityGroupFilterCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

//			ServerSession session = SecurityManager.getSession(userSecurityInput.getUrl());
			
			//if (  session != null && session.isMixedAuthMode() ) {
			
			new Label(composite_1, SWT.NONE);
			
			displayAllFilterButton = new Button(composite_1, SWT.CHECK);
			displayAllFilterButton.setToolTipText("When checked, all domains with their security groups are displayed.  When unchecked, only selected security groups and thier domains are displayed.");
			
			displayAllFilterButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
			displayAllFilterButton.setText("Display All Domain / Security Groups");
			
			new Label(composite_1, SWT.NONE);
				
			//}
			
		} else {
		
			final Label edit_UserTypeLabel = new Label(composite_1, SWT.NONE);
			edit_UserTypeLabel.setText("User Type:");

			edit_UserTypeValueLabel = new Label(composite_1, SWT.NONE);
			edit_UserTypeValueLabel.setText("test");
			new Label(composite_1, SWT.NONE);
			new Label(composite_1, SWT.NONE);
			
		}
	
		usernameLabel = new Label(composite_1, SWT.NONE);
		GridData gd_usernameLabel = new GridData(84, 22);
		gd_usernameLabel.verticalAlignment = SWT.TOP;
		gd_usernameLabel.horizontalAlignment = SWT.FILL;
		usernameLabel.setLayoutData(gd_usernameLabel);
		usernameLabel.setText("User Name:");
		usernameCombo = new Combo(composite_1, SWT.READ_ONLY);
		usernameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 3, 1));
		new Label(composite_1, SWT.NONE);

		adminCheckBox = new Button(composite_1, SWT.CHECK);
		adminCheckBox.setText("Testing User");
		new Label(composite_1, SWT.NONE);
		new Label(composite_1, SWT.NONE);

		final Label label = new Label(composite_1, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		final Composite composite_2 = new Composite(scrolledChildComposite, SWT.NONE);
		final GridData gridData_4 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_4.heightHint = 500;
		composite_2.setLayoutData(gridData_4);
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.marginHeight = 0;
		gridLayout_3.numColumns = 3;
		composite_2.setLayout(gridLayout_3);

		final Label rolesLabel = new Label(composite_2, SWT.NONE);
		final GridData gridData_6 = new GridData(SWT.FILL, SWT.FILL, true,
				false);
		gridData_6.heightHint = 13;
		gridData_6.widthHint = 80;
		rolesLabel.setLayoutData(gridData_6);
		rolesLabel.setText("Available Roles");
		new Label(composite_2, SWT.NONE);

		final Label rolesLabel_1 = new Label(composite_2, SWT.NONE);
		final GridData gridData_6_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData_6_1.heightHint = 22;
		gridData_6_1.widthHint = 160;
		rolesLabel_1.setLayoutData(gridData_6_1);
		rolesLabel_1.setText("Selected Roles");

		availableRolesListControl = new List(composite_2, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		availableRolesListControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));

		final Composite composite_5 = new Composite(composite_2, SWT.NONE);
		composite_5.setLayout(new GridLayout());

		final Button rightArrowButton = new Button(composite_5, SWT.ARROW | SWT.RIGHT);
		rightArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
							
				
				//if there are roles to move over, unselect everything in selected roles
				if ( availableRolesListControl.getSelection().length > 0 ) {
					
					selectedRolesListControl.deselectAll();
				}
								
				for(String selectedItem : availableRolesListControl.getSelection() ){
				
					
					EditorControlUtil.addItemToList(selectedRolesListControl, selectedItem);
					EditorControlUtil.selectItem(selectedRolesListControl, selectedItem);
					editorModified();				
					
				}
				
				EditorControlUtil.removeItemFromList(availableRolesListControl, availableRolesListControl.getSelectionIndices());
								
				clearSecurityListControl();
				onSelectedRoleListControlChange();
			}
		});
		
		rightArrowButton.setText("button");

		final Button leftArrowButton = new Button(composite_5, SWT.ARROW | SWT.LEFT);
		
		leftArrowButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
								
				String[] rolesToRemoveFromSelectedRolesList = selectedRolesListControl.getSelection(); 
				
				for (String itemToAdd : rolesToRemoveFromSelectedRolesList ) {

					EditorControlUtil.addItemToList(availableRolesListControl, itemToAdd);
					if( roleSecurityMap.get(itemToAdd) != null )
						roleSecurityMap.get(itemToAdd).clear();
					editorModified();	
					
				}
				
				EditorControlUtil.removeItemFromList(
						selectedRolesListControl, 
						selectedRolesListControl.getSelectionIndices());
				
				String[] availableRolesListNames = availableRolesListControl.getItems();
				
				if ( availableRolesListNames != null ) {
					
					Arrays.sort(availableRolesListNames);
					
				}
				
				EditorControlUtil.addItemsToList(availableRolesListControl, availableRolesListNames);
				
				if ( rolesToRemoveFromSelectedRolesList.length == 1 ) {
					
					enableAllTabFormControls(false);
					
				}
			}
		});
		leftArrowButton.setText("button");

		final Composite composite_3 = new Composite(composite_2, SWT.NONE);
		composite_3.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 2;
		gridLayout_4.marginHeight = 0;
		gridLayout_4.marginWidth = 0;
		gridLayout_4.horizontalSpacing = 0;
		composite_3.setLayout(gridLayout_4);

		selectedRolesListControl = new List(composite_3, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_7 = new GridData(SWT.FILL, SWT.TOP, true, false);
		gridData_7.heightHint = 100;
		selectedRolesListControl.setLayoutData(gridData_7);
		final Composite composite_4 = new Composite(composite_3, SWT.NONE);
		composite_4.setLayout(new GridLayout());

		final Button upArrowButton = new Button(composite_4, SWT.ARROW);
		upArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ControlUtil.directionalButtonPressed(selectedRolesListControl, ControlUtil.UP_BUTTON_ID);
				if ( selectedRolesListControl.getSelection().length > 0 ) {
					editorModified();
				}
			}
		});
		upArrowButton.setText("button");

		final Button downArrowButton = new Button(composite_4, SWT.ARROW | SWT.DOWN);
		downArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ControlUtil.directionalButtonPressed(selectedRolesListControl, ControlUtil.DOWN_BUTTON_ID);
				if ( selectedRolesListControl.getSelection().length > 0 ) {
					editorModified();
				}
			}
		});
		downArrowButton.setText("button");
		
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		gridData_1.heightHint = 500;
		gridData_1.widthHint = 171;

		new Label(composite_2, SWT.NONE);

		dimensionTabFolder = new TabFolder(composite_2, SWT.NONE);
		final GridData gridData_5 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_5.heightHint = 500;
		gridData_5.widthHint = 650;
		dimensionTabFolder.setLayoutData(gridData_5);
		// close editor
//		EditorUtils.closeEditor(this, UserSecurityEditor.class);

		// populate tab folder with hier dims
		populateTabFolder();

		// disable all tab for controls
		enableAllTabFormControls(false);

		// populate the controls with values
		populateControlValues();

		// add the control listeners
		addControlListeners();
		
		setDefaultFilterValues();

		// try to get current user from input
		PafUserSecurity currentPafUser = userSecurityInput
				.getCurrentUserSecurity();

		// if current user exists and if that user has roles already assigned to
		// it
		if (currentPafUser != null && currentPafUser.getRoleNames() != null
				&& currentPafUser.getRoleNames().length > 0) {

			// get current paf roles
			String[] currentPafUserRoles = currentPafUser.getRoleNames();

			// if roles exists
			if (currentPafUserRoles != null) {

				// sort array
				//Arrays.sort(currentPafUserRoles);

				// select the role in the table viewer
				selectedRolesListControl.setSelection(new String[] { currentPafUserRoles[0] });
				
				clearSecurityListControl();
				onSelectedRoleListControlChange();

				// turn off dirty flag. gets turned on when selection changes
				// from previous selection change
//				isDirty = false;

			}

		}

		
	}

	private void setDefaultFilterValues() {

		if ( userSecurityInput.isNew() || userSecurityInput.isClone() ) {
			
			EditorControlUtil.selectItem(new_DomainFilterCombo, PafBaseConstants.Native_Domain_Name);
			
			populateSecurityGroupFilterCombo();
			
		}
		
	}

	/**
	 * Populates the tab folder with the controls. The contols include a tree
	 * viewer to display the hier dim tree, 3 check buttons that hold security
	 * member selections (seleciton only, selection and children, and selection
	 * and descendants), a drop down box with Level/Generation, and a spinner
	 * for level and gen number.
	 */
	private void populateTabFolder() {

		// map(dimension name, cached tree to be used to pouplate swt tree)
		Map<String, PafSimpleDimTree> dimensionSimpleTreeMap = userSecurityInput
				.getDimTreeMaps();

		// list of hier dimensions
		Set<String> dimensionNameKeySet = dimensionSimpleTreeMap.keySet();

		// if hier dimensions exist and the tab folder is not null and not
		// disposed
		if (dimensionNameKeySet != null && dimensionTabFolder != null
				&& !dimensionTabFolder.isDisposed()) {

			// loop through each dimension and create a tab item. For each tab
			// item, create all the
			// controls
			for (String dimName : dimensionNameKeySet) {

				// create new tab item
				final TabItem tabItem = new TabItem(dimensionTabFolder,
						SWT.NONE);

				// set text to hier dimension name
				tabItem.setText(dimName);

				// get paf tree from cached map
				PafSimpleDimTree pafTree = dimensionSimpleTreeMap.get(dimName);

				// creat a new deposit
				final Composite tabComposite = getDimensionTabFolderComposite(
						dimensionTabFolder, tabItem, pafTree);

				// set control to new composite
				tabItem.setControl(tabComposite);

				// get tree from cached map
				Tree treeControl = dimensionControlTreeMap.get(dimName);
				
				findTreeModuleMap.get(dimName).setTree(treeControl);

				// create swt tree model from paf tree
				createTreeModel(treeControl, pafTree, pafTree.getRootKey());

				// expand top item
				TreeItem top = treeControl.getTopItem();
				if(top != null) {
					treeControl.getTopItem().setExpanded(true);
				}

				// get max depth for level and generation
				Integer maxTreeDept = maxTreeDepthMap.get(tabItem.getText());

				// get spinner just created
				Spinner spinner = levelGenSpinnerMap.get(dimName);

				// if not null and max tree dept not null
				if (spinner != null && maxTreeDept != null) {

					// set spinner to max tree dept
					spinner.setMaximum(maxTreeDept);

				}

				// add hier dimension to set of dimension names
				dimensionNameSet.add(dimName);

			}

			// yeahhhhhh.......lets enable these new form controls
			enableAllTabFormControls(true);

		}

	}

	/**
	 * Clears all the form data. First all listeners are removed. Next the form
	 * controls are cleared. Next, unbold all the previously bolded tree itmes.
	 * Finally, add the listeners again.
	 */
	private void clearAllTabFormControls() {

		// clear all security contorls
		clearTabFormControl(securityMemberControlMap);
		clearTabFormControl(dimensionControlTreeMap);
		clearTabFormControl(findTreeModuleMap);
		clearTabFormControl(selectionOnlyMap);
		clearTabFormControl(selectionChildrenMap);
		clearTabFormControl(selectionDescendantsMap);
		clearTabFormControl(levelGenerationMap);
		clearTabFormControl(levelGenSpinnerMap);

		// unbold all dimension tree itmes
		unboldAllDimensionTreeItems();
	}

	/**
	 * Clears all the controls that are within the values of the map passed in.
	 * Value object is retrieved from the map, then tested and the approiate
	 * cast is applied then control is cleared.
	 * 
	 * @param controlsMap
	 *            Map (hier dim name, control)
	 */
	private void clearTabFormControl(Map controlsMap) {

		// for all values, clear the controls
		for (Object object : controlsMap.values()) {

			// if text, set text to empty string
			if (object instanceof Text) {

				Text txt = (Text) object;

				txt.setText("");

				// if combo, select first (0th) element
			} else if (object instanceof Combo) {

				Combo combo = (Combo) object;

				combo.select(0);

				// if button, un select
			} else if (object instanceof Button) {

				Button button = (Button) object;

				button.setSelection(false);

				// if tree, roll up all children and then expand again
			} else if (object instanceof Tree) {

				Tree tree = (Tree) object;

				// remove any alias information; convo with jim, he thinks
				// should remain enabled
				// updateTreeWithAliases(tree, null);

				for (TreeItem directTreeChild : tree.getItems()) {

					for (TreeItem treeChild : directTreeChild.getItems()) {

						treeChild.setExpanded(false);

					}

					directTreeChild.setExpanded(true);

				}

				// if tree contains any tree itmes, get root and then select and
				// bold
				if (tree.getItems().length > 0) {

					TreeItem topItem = tree.getItem(0);
					tree.setSelection(topItem);
					boldTreeItem(topItem, true);

				}

				// if spinner, select 0
			} else if (object instanceof Spinner) {

				Spinner spinner = (Spinner) object;

				spinner.setSelection(0);

			}

		}

	}

	/**
	 * Enables/disables all tab form controls based on the boolean passed in
	 * 
	 * @param isEnabled
	 *            Enables or disables form controls.
	 */
	private void enableAllTabFormControls(boolean isEnabled) {

		// select first tab
		dimensionTabFolder.setSelection(0);

		// enable/disable based on flag
		enableAllMapControls(securityMemberControlMap, isEnabled);
		enableAllMapControls(dimensionControlTreeMap, isEnabled);
		enableAllMapControls(selectionOnlyMap, isEnabled);
		enableAllMapControls(selectionChildrenMap, isEnabled);
		enableAllMapControls(selectionDescendantsMap, isEnabled);
		enableAllMapControls(addButtonMap, isEnabled);
		enableAllMapControls(removeButtonMap, isEnabled);
		enableAllMapControls(clearButtonMap, isEnabled);
		enableAllMapControls(validateButtonMap, isEnabled);
		enableAllMapControls(findTreeModuleMap, isEnabled);

		// if disabling, disable dropdown and spinner
		if (!isEnabled) {

			enableAllMapControls(levelGenerationMap, isEnabled);
			enableAllMapControls(levelGenSpinnerMap, isEnabled);

		}

		enableAllMapControls(applyAliasTableMap, isEnabled);

		if (isEnabled) {

			for (String key : aliasTableComboMap.keySet()) {

				Button applyAliasTableButton = applyAliasTableMap.get(key);

				Combo aliasTableCombo = aliasTableComboMap.get(key);

				aliasTableCombo
						.setEnabled(applyAliasTableButton.getSelection());

			}

		} else {

			enableAllMapControls(aliasTableComboMap, false);

		}

	}

	/**
	 * Enable/disable all map controls. Loop over all values of map and if
	 * instance of control, enable or diable.
	 * 
	 * @param controlsMap
	 *            Map of controls to enable/disable
	 * @param isEnabled
	 *            Flag to enable/disable
	 */
	private void enableAllMapControls(Map controlsMap, boolean isEnabled) {

		// for each value in map
		for (Object object : controlsMap.values()) {

			// if control
			if (object instanceof Control) {

				Control control = (Control) object;

				// enable/diable
				control.setEnabled(isEnabled);

			}

		}

	}

	@Override
	public void setFocus() {

		if (adminCheckBox != null && !adminCheckBox.isDisposed()) {
			adminCheckBox.setFocus();
		}

	}

	@Override
	public void doSaveAs() {
		// Do the Save As operation
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		String usersSecurity = input.getName();

		setSite(site);
		
		setInput(input);

		logger.info("Opening User Security editor for: " + usersSecurity);

		setPartName(Constants.MENU_USER_SECURITY + ": " + usersSecurity);

		userSecurityInput = (UserSecurityEditorInput) input;
		
		// after init, set dirty to false
		setDirty(false);
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * @return the viewer
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * @param viewer
	 *            the viewer to set
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Build the contents of a tree control and populate it.
	 * 
	 * @param tree
	 *            The tree control to build.
	 */
	private void createTreeModel(Tree tree, PafSimpleDimTree simpleBaseTree,
			String root) {

		logger.debug("Create Tree Model start: " + root);

		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();

		try {

			// PafSimpleDimTree pafSimpleTree =
			// userSecurityInput.getPafSimpleTreeTime();

			logger.debug("Converting Tree Into hash start");

			HashMap treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(simpleBaseTree);

			logger.debug("Converting TreeInto hash end");

			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap
					.get(root);

			int maxTreeDepth = rootMember.getPafSimpleDimMemberProps()
					.getLevelNumber();

			logger.info("MAX LEVEL: " + maxTreeDepth);

			maxTreeDepthMap.put(root, maxTreeDepth);

			logger.debug("Alias Tables");

			for (String aliasTableName : simpleBaseTree.getAliasTableNames()) {
				logger.debug("Alias Table: " + aliasTableName);
			}

			logger.debug("Building Tree " + root + " start");

			addTreeNode(treeHashMap, tree, null, rootMember, root);

			cachedTreeMap.put(root, tree);

			logger.debug("Building Tree " + root + " end");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Create Tree Model end");
	}

	/**
	 * Adds a tree node to a tree.
	 * 
	 * @param treeHashMap
	 *            The hash map containing the tree components.
	 * @param tree
	 *            The tree to add the nodes to.
	 * @param parent
	 *            The parent tree item to have the nodes added to.
	 * @param member
	 *            The paf simple memeber to add to the parent.
	 * @param dimension
	 *            The dimension of this tree.
	 */
	private void addTreeNode(HashMap treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		java.util.List<String> aliasKeysList = member.getPafSimpleDimMemberProps().getAliasKeys();
		java.util.List<String> aliasValuesList = member.getPafSimpleDimMemberProps()
				.getAliasValues();

		if (aliasKeysList != null) {
			for (int i = 0; i < aliasKeysList.size(); i++) {
				logger.debug("Alias Key: " + aliasKeysList.get(i) + "; Alias Value: "
						+ aliasValuesList.get(i));
			}
		}

		newItem.setText(member.getKey());

		newItem.setData(member);

		if (member.getChildKeys().size() > 0 ) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap
						.get(child);

				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}

		Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
				.get(dimension);

		if (treeItemMap == null) {

			treeItemMap = new HashMap<String, TreeItem>();

		}

		treeItemMap.put(newItem.getText(), newItem);

		dimensionMapOfTreeItemMap.put(dimension, treeItemMap);

	}

	/**
	 * Creates a new tab folder composite with contorls. After each contorl is
	 * created and added to composite, add to map (dim name, control).
	 * 
	 * @param tabFolder
	 *            Tab folder to create composite for.
	 * @param tabItem
	 *            Tab item to create composite for.
	 * @return
	 */
	private Composite getDimensionTabFolderComposite(TabFolder tabFolder,
			TabItem tabItem, PafSimpleDimTree pafSimpleDimTree) {

		final Composite dimTabComposite = new Composite(tabFolder, SWT.NONE);
		dimTabComposite.setLayout(new GridLayout(3, false));
		dimTabComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		/*******************************
		 * Security Dimension Group	****
		 *******************************/
		Group grpSecDim = new Group(dimTabComposite, SWT.NONE);
		grpSecDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecDim.setLayout(new GridLayout(1, true));
		grpSecDim.setText("Security Dimension");
		
		Composite subSecDim = new Composite(grpSecDim, SWT.NONE);
		subSecDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		subSecDim.setLayout(new GridLayout(3, false));

		final Button aliasTableCheck = new Button(subSecDim, SWT.CHECK);
		aliasTableCheck.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		aliasTableCheck.setText("View Alias Table");

		applyAliasTableMap.put(tabItem.getText(), aliasTableCheck);

		final Combo aliasTableDropDown = new Combo(subSecDim, SWT.READ_ONLY);
		aliasTableDropDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		if (pafSimpleDimTree != null && pafSimpleDimTree.getAliasTableNames().size() > 0 ) {

			String[] aliasTableNames = pafSimpleDimTree.getAliasTableNames().toArray(new String[0]);

			if (aliasTableNames != null) {

				aliasTableDropDown.setItems(aliasTableNames);

			}

			// select first item
			aliasTableDropDown.select(0);

		}

		aliasTableDropDown.setEnabled(false);

		aliasTableComboMap.put(tabItem.getText(), aliasTableDropDown);

		new Label(subSecDim, SWT.NONE);

		final FindTreeModule ftm = new FindTreeModule(subSecDim, SWT.NONE);
		final GridData gridData_ftm = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		ftm.setLayoutData(gridData_ftm);
		
		final Button addButton = new Button(subSecDim, SWT.PUSH );
		addButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		addButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/add.gif"));
		addButton.setToolTipText("Add Tree Item to Security Specification");
		
		addButtonMap.put(tabItem.getText(), addButton);
		
		addButton.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				addNewSecurityMember();
				setSelectionControls();
				editorModified();
			}
		});

		final Tree dimensionTree = new Tree(grpSecDim, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_8 = new GridData(SWT.FILL, SWT.FILL, true, true);
		//gridData_8.widthHint = 100;
		//gridData_8.heightHint = 350;
		dimensionTree.setLayoutData(gridData_8);
		dimensionTree.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				onSelectedDimensionTreeControlChange();
			}
		
		});

		ftm.setTree(dimensionTree);
		dimensionTree.addMouseListener(getTabTreeMouseListener());
		
		dimensionControlTreeMap.put(tabItem.getText(), dimensionTree);
		findTreeModuleMap.put(tabItem.getText(), ftm);
		/***********************************
		 * Security Specification Group
		 **********************************/
		Group grpSecSpec = new Group(dimTabComposite, SWT.NONE);
		grpSecSpec.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSecSpec.setText("Security Specification");
		grpSecSpec.setLayout(new GridLayout(1, false));
		
		Composite subSecSpec = new Composite(grpSecSpec, SWT.NONE);
		subSecSpec.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false));
		subSecSpec.setLayout(new GridLayout(3, false));

		final Button validateButton = new Button(subSecSpec, SWT.PUSH);
		validateButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		//validateButton.setLayoutData(new GridData(92, 14));
		//validateButton.setBounds(150, 20, 20, 27);
		validateButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/validator.gif"));
		validateButton.setToolTipText("Validate (Edited) Security Specification");
		
		validateButtonMap.put(tabItem.getText(), validateButton);
		
		validateButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				validateUserSecurity();
			}
		});

		final Button removeButton = new Button(subSecSpec, SWT.PUSH);
		//removeButton.setLayoutData(new GridData(92, 14));
		//removeButton.setBounds(170, 20, 20, 27);
		removeButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		removeButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/delete.png"));
		removeButton.setToolTipText("Delete Selected Item From Security Specification");
		
		removeButtonMap.put(tabItem.getText(), removeButton);
		
		removeButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeSelectedUserSecurity();
				editorModified();
			}
		});

		final Button clearButton = new Button(subSecSpec, SWT.PUSH);
		//clearButton.setLayoutData(new GridData(92, 14));
		//clearButton.setBounds(190, 20, 21, 27);
		clearButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		clearButton.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/clear.png"));
		clearButton.setToolTipText("Clear Security Specification");
		
		clearButtonMap.put(tabItem.getText(), clearButton);
		
		clearButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				removeAllUserSecurity();
				editorModified();
			}
		});

		final List securityListControl = new List(grpSecSpec, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_9 = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		//gridData_9.widthHint = 180;
		//gridData_9.heightHint = 200;
		securityListControl.setLayoutData(gridData_9);
		//securityListControl.setBounds(10, 50, 200, 250);
		securityListControl.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				onSelectedSecurityListControlChange();
			}
		
		});

		securityMemberControlMap.put(tabItem.getText(), securityListControl);
		
		if(secSubMemberListTextMap.get(tabItem.getText()) == null){
			secSubMemberListTextMap.put(tabItem.getText(), new HashMap<String, String>());
		}

		aliasTableCheck.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				// get selection
				boolean isSelected = aliasTableCheck.getSelection();

				// if apply alias is checked, enable drop down, select first
				// element and update tree with aliases
				if (isSelected) {

					aliasTableDropDown.setEnabled(true);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimensionTree, securityListControl, aliasTableDropDown.getText());

				} else {

					aliasTableDropDown.setEnabled(false);
					aliasTableDropDown.select(0);
					updateTreeWithAliases(dimensionTree, securityListControl, null);

				}

			}

		});

		aliasTableDropDown.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				updateTreeWithAliases(dimensionTree, securityListControl, aliasTableDropDown.getText());

			}

		});

		aliasTableDropDown.addMouseListener(new MouseListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void mouseUp(MouseEvent e) {
				
				updateTreeWithAliases(dimensionTree, securityListControl, aliasTableDropDown.getText());

			}
			
			public void mouseDown(MouseEvent e) {
				
				updateTreeWithAliases(dimensionTree, securityListControl, aliasTableDropDown.getText());

			}
			
			public void mouseDoubleClick(MouseEvent e) {
			
				updateTreeWithAliases(dimensionTree, securityListControl, aliasTableDropDown.getText());

			}

		});
		/******************************
		 * Selection Level Group
		 *********************************/
		final Group selectionGroup = new Group(dimTabComposite, SWT.NONE);
		selectionGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
		selectionGroup.setText("Selection");

		final Button selectionButton = new Button(selectionGroup, SWT.RADIO);
		selectionButton.setText(SelectionType.Selection.getDisplayString());
		selectionButton.setBounds(10, 21, 180, 19);
		selectionButton.setSelection(true);
		
		selectionButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember();
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),false);
				editorModified();
			}
		});

		selectionOnlyMap.put(tabItem.getText(), selectionButton);

		final Button selectionChildrenButton = new Button(selectionGroup,
				SWT.RADIO);
		selectionChildrenButton.setText(SelectionType.SelectionAndChildren.getDisplayString());
		selectionChildrenButton.setBounds(10, 42, 180, 19);
		selectionChildrenButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember();
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),false);
				editorModified();
			}
		});

		selectionChildrenMap.put(tabItem.getText(), selectionChildrenButton);

		final Button selectionDescendantsButton = new Button(selectionGroup,
				SWT.RADIO);
		selectionDescendantsButton.setText(SelectionType.SelectionAndDescendants.getDisplayString());
		selectionDescendantsButton.setBounds(10, 62, 180, 19);
		selectionDescendantsButton.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember();
				enableLevelGenerationControls(dimensionTabFolder.getSelection()[0].getText(),true);
				editorModified();
			}
		});

		selectionDescendantsMap.put(tabItem.getText(),
				selectionDescendantsButton);

		final Combo levelGenCombo = new Combo(selectionGroup, SWT.READ_ONLY);

		String[] levelGenDropdownValues = new String[] {
				LevelGenerationType.Level.toString(),
				LevelGenerationType.Generation.toString() };

		// String[] levelGenDropdownValues = new String[] {
		// LevelGenerationType.Level.toString() };

		EditorControlUtil.addItems(levelGenCombo, levelGenDropdownValues);
		levelGenCombo.setText(LevelGenerationType.Level.toString());
		levelGenCombo.setBounds(10, 87, 81, 21);
		levelGenCombo.setEnabled(false);
		levelGenCombo.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember();
				editorModified();
			}
		});

		levelGenerationMap.put(tabItem.getText(), levelGenCombo);

		final Spinner levelGenSpinner = new Spinner(selectionGroup, SWT.BORDER
				| SWT.READ_ONLY);
		// levelGenSpinner.setMaximum(13);
		levelGenSpinner.setBounds(97, 87, 41, 24);
		levelGenSpinner.setEnabled(false);
		levelGenSpinner.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSecurityMember();
				editorModified();
			}
		});

		levelGenSpinnerMap.put(tabItem.getText(), levelGenSpinner);

		return dimTabComposite;
	}

	protected void removeAllUserSecurity() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		unboldDimensionTreeItems(mapKeyDimName);
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && selectedRolesListControl.getSelection().length == 1 
				&& securityMemberList.getItemCount() > 0 ) {
			securityMemberList.removeAll();
			String roleName = selectedRolesListControl.getSelection()[0];
			Map<String,java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
			securityMemberMap.remove(mapKeyDimName);
		}
		
		setSelectionControls();
	}

	protected void removeSelectedUserSecurity() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		unboldDimensionTreeItems(mapKeyDimName);
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
	    // try to get security member
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && selectedRolesListControl.getSelection().length == 1
				&& securityMemberList.getSelectionCount() > 0 ) {
			String[] secList = securityMemberList.getSelection();
			for( String sec : secList ) {
				securityMemberList.remove(sec);
				removeSelectedSecurity(mapKeyDimName, sec);
			}
		}
		if( securityMemberList.getItemCount() == 0 ) {
			enableAllMapControls(removeButtonMap, false);
			enableAllMapControls(clearButtonMap, false);
			enableAllMapControls(validateButtonMap, false);
		}
		setSelectionControls();
	}

	protected void removeSelectedSecurity( String dimension, String security ) {
		String roleName = selectedRolesListControl.getSelection()[0];
		Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
		if (securityMemberMap != null && securityMemberMap.size() > 0 ) {
			java.util.List<SecurityMember> secArray = securityMemberMap.get(dimension);
			if( secArray != null  ) {
				SecurityMember secMem = new SecurityMember(dimension, security);
				for( Iterator< SecurityMember > it = secArray.iterator(); it.hasNext() ; ) {
			           SecurityMember sec = it.next(); 
			           // TTN-2557 Compare security members.
			           Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimension);
			           if (dimTreeItemMap.containsKey(secMem.getMember())){
			        	   if ( sec.equals(secMem) ) {
			        		   it.remove();
			        	   }
			           } // if security member alias is used , it won't match the key. Then compare the value.
			           else {
			        	   
			        	   Set<Entry<String,TreeItem>> items = dimTreeItemMap.entrySet();
							Iterator itemIter = items.iterator();
							while ( itemIter.hasNext()){
								Map.Entry pairs = (Map.Entry)itemIter.next();
								if (((TreeItem)pairs.getValue()).getText().equals(secMem.getMember())){
									if (sec.getMember().equals(pairs.getKey())){
										it.remove();
										break;
									}
								}
							}
			           }
				  }
			}
		}
	}
		
	protected ValidationResponse validateUserSecurity(String dimension,java.util.List<String> expList) {
		String serverName = null;
		try {
			PafServer server = PafProjectUtil.getProjectServer(userSecurityInput.getProject());
			serverName = server.getName();
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName),false);
			String url = server.getCompleteWSDLService();
			if( SecurityManager.isAuthenticated(url) ) {
				ValidateUserSecurityRequest valUsrSecReq = new ValidateUserSecurityRequest();
				ServerSession serverSession = SecurityManager.getSession(url);
				valUsrSecReq.setClientId(serverSession.getClientId());
				valUsrSecReq.setSessionToken(serverSession.getSecurityToken());
				com.pace.server.client.PafDimSpec clientDimSpec = new com.pace.server.client.PafDimSpec();
				clientDimSpec.setDimension(dimension);
				clientDimSpec.getExpressionList().addAll(expList);
				valUsrSecReq.getSecuritySpecs().add(clientDimSpec);
				PafService service = WebServicesUtil.getPafService(url);
				if( service != null ) {
					return service.validateUserSecurity(valUsrSecReq);
				}
			}
		} catch (PafNotAuthorizedSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafSoapException_Exception e) {
			e.printStackTrace();
		} catch (PafServerNotFound e) {
			logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
			e.printStackTrace();
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch( RuntimeException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	protected void validateUserSecurity() {
		String roleName = selectedRolesListControl.getSelection()[0];
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		unboldDimensionTreeItems(mapKeyDimName);
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		List tempSecurityMemberList = securityMemberList;
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && selectedRolesListControl.getSelection().length == 1
				&& securityMemberList.getItemCount() > 0 ) {
			//validate against dim cache first
			Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(mapKeyDimName);
			if (dimTreeItemMap != null) {
				Set<InvalidSecurityMember> invalidMemberSet = new LinkedHashSet<InvalidSecurityMember>();
				// verify security member is still valid member in outline
				for( String secMem : securityMemberList.getItems() ) {
					//TODO
					boolean memberFound = false;
					SecurityMember securityMember = new SecurityMember(mapKeyDimName, secMem);
					// Remove alias from member name before the comparison. 
					String newMember = securityMember.getMember();
					
					if (!dimTreeItemMap.containsKey(newMember)){
						Set<Entry<String,TreeItem>> items = dimTreeItemMap.entrySet();
						Iterator itemIter = items.iterator();
						while ( itemIter.hasNext()){
							Map.Entry pairs = (Map.Entry)itemIter.next();
							if (((TreeItem)pairs.getValue()).getText().equals(newMember)){
								memberFound = true;
								// Update Security member to replace alias with key
								securityMemberList.remove(secMem);
								securityMember.setMember(pairs.getKey().toString());
								
								secMem = securityMember.toString();
								securityMemberList.add(secMem);
								// Update to replace alias back to key in the member list
								securityMember.setMember(((TreeItem)pairs.getValue()).getText());
								secMem = securityMember.toString();
								break;
							}
						}
						if (!memberFound){
						InvalidSecurityMember invalidSecurityMember = new InvalidSecurityMember();
						invalidSecurityMember.setRoleName(roleName);
						invalidSecurityMember.setDimensionName(securityMember.getDimensionName());
						invalidSecurityMember.setMemberName(securityMember.getMember());
						invalidMemberSet.add(invalidSecurityMember);
						}
						
					}
					
				}
				if ( invalidMemberSet.size() > 0 ) {
	
					Shell shell = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell();
	
					InvalidMemberErrorDialog invalidSecurityMemberErrorDialog = new InvalidMemberErrorDialog(
							shell, invalidMemberSet);
					invalidSecurityMemberErrorDialog.open();
	
					InvalidSecurityMemberView invalidSecurityMemberView;
					try {
						invalidSecurityMemberView = (InvalidSecurityMemberView) PlatformUI
								.getWorkbench().getActiveWorkbenchWindow()
								.getActivePage().showView(
										InvalidSecurityMemberView.ID);
						invalidSecurityMemberView.setInvalidSecurityMemberSet(invalidMemberSet);
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return;
				}
			}
			
			//validate against server then
			ValidationResponse valResp  = validateUserSecurity( mapKeyDimName, Arrays.asList(securityMemberList.getItems()));
			if( valResp != null ) {
				if( ! valResp.isSuccess() ) { //validation failed
					java.util.List<String> errorList = valResp.getValidationErrors();
					if( errorList.size() > 0 ) {
						Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
						ListErrorMessagesDialog userSecurityValidationDialog = new ListErrorMessagesDialog(
								shell, 
								errorList,
								"Invalid User Security Specification",
								"The following are invalid security members for user: " + roleName + ". Please re-configure.");
						userSecurityValidationDialog.open();
					}
				}
				else { //validation succeeded
					GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "The security members for '" + roleName + "/" + mapKeyDimName + "' have been validated.", MessageDialog.INFORMATION);
				}
			}
			else { //service call failed
				GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "There is an issue with server. The security members for '" + roleName + "/" +  mapKeyDimName + "' have NOT been validated.", MessageDialog.INFORMATION);
			}
		}
	}

	/**
	 * Applies an alias table to the existing dimension tree.
	 * 
	 * @param dimensionTree
	 *            Dimension tree to have alias applied to
	 * @param aliasTableName
	 *            Alias table name to apply to tree
	 */
	protected void updateTreeWithAliases(Tree dimensionTree, List secMemControl, String aliasTableName) {

		logger.debug("Going to update tree " + dimensionTree
				+ " with alias table " + aliasTableName);
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		Map<String, String> memberListTextMap = this.secSubMemberListTextMap.get(mapKeyDimName);
		String[] listStrs = secMemControl.getItems();

		for (TreeItem childItem : dimensionTree.getItems()) {

			applyAliasToTree(childItem, memberListTextMap, listStrs, aliasTableName);

		}

		for(int i = 0; i < listStrs.length; i++){
			if(memberListTextMap.containsKey(listStrs[i])){
				String temp = listStrs[i];
				listStrs[i] = memberListTextMap.get(listStrs[i]);
				memberListTextMap.remove(temp);
				memberListTextMap.put(listStrs[i], temp);
			} else if(listStrs[i].indexOf('(') != -1){
				int endNdx = listStrs[i].indexOf(')');
				if(listStrs[i].indexOf(',') != -1){
					endNdx = listStrs[i].indexOf(',');
				}
				
				String theText = listStrs[i].substring(listStrs[i].indexOf('(') + 1, endNdx);
				String newText = memberListTextMap.get(theText);
				listStrs[i] = listStrs[i].substring(0, listStrs[i].indexOf('(') + 1) +
								newText +
								listStrs[i].substring(endNdx, listStrs[i].length()); 
			/*	listStrs[i] = listStrs[i].substring(0, endNdx ) + " (" + newText + ")" +
						
						listStrs[i].substring(endNdx, listStrs[i].length()); */
				memberListTextMap.remove(theText);
				memberListTextMap.put(newText, theText);				
			}
		}
		
		secMemControl.setItems(listStrs);
	}

	/**
	 * Updates the treeItem's text with an alias if an alias table name is
	 * defined. If the table name is null, the PafSimpleDimMember.getKey is used
	 * as the text, but if table name is provided, the simple member is searched
	 * for alias keys that match the alias table name and then the alias value
	 * is set as the text.
	 * 
	 * @param treeItem
	 *            text gets updated with key or alias value
	 * @param aliasTableName
	 *            name of alias table
	 */
	private void applyAliasToTree(TreeItem treeItem, Map<String, String> memListTextMap, String[] listStrs, String aliasTableName) {

		// if the tree item has children
	/*	if (treeItem.getItems().length > 0) {
			
			// apply aliases to the children
			for (TreeItem childItem : treeItem.getItems()) {

				applyAliasToTree(childItem, memListTextMap, listStrs, aliasTableName);

			}

		}*/
	
		// get data member
		PafSimpleDimMember member = (PafSimpleDimMember) treeItem.getData();

		// if the alias table name is null, use key from member, else use alias
		if (aliasTableName == null) {

			treeItem.setText(member.getKey());

		} else {
			
			if (member.getPafSimpleDimMemberProps().getAliasKeys().size() > 0) {

				String[] aliasKeys = member.getPafSimpleDimMemberProps().getAliasKeys().toArray(new String[0]);
				
				String[] aliasValues = member.getPafSimpleDimMemberProps()
						.getAliasValues().toArray(new String[0]);

				for (int i = 0; i < aliasKeys.length; i++) {
					if (aliasKeys[i].equals(aliasTableName)) {

						treeItem.setText(aliasValues[i]);

					}
				}

			}

		}
		
		if(memListTextMap.get(member.getKey()) != null){
			memListTextMap.remove(member.getKey());
			memListTextMap.put(member.getKey(), treeItem.getText());
		}

		
		// if the tree item has children
				if (treeItem.getItems().length > 0) {
					
					// apply aliases to the children
					for (TreeItem childItem : treeItem.getItems()) {

						applyAliasToTree(childItem, memListTextMap, listStrs, aliasTableName);

					}

				}
	}

	/**
	 * Add control listeners.
	 * 
	 */
	private void addControlListeners() {

		// if user input is new, add selection listener to user name dropdown.
		// User name is only eabled
		// for new input.
		if (userSecurityInput.isNew() || userSecurityInput.isClone()) {

			usernameCombo.addSelectionListener(getFormSelectionAdapter());
			
			if ( displayAllFilterButton != null ) {			
				
				displayAllFilterButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent selEvent) {
						
						BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), 
								new Runnable() {
	
							public void run() {
						
								lastDomainNameSelected = null;
								
								lastSecurityGroupSelected = null;
								
								populateDomainFilterCombo();
								
								populateSecurityGroupFilterCombo();
								
								if ( displayAllFilterButton.getSelection() ) {
									
									if ( new_DomainFilterCombo.getItemCount() > 0 ) {
									
										new_DomainFilterCombo.select(0);
									
									}
								} else {
									
									setDefaultFilterValues();
									
								}
								
							}
						
						});
											
					}
				});
			}

		}

		// add form selection adapter to admin check box.
		adminCheckBox.addSelectionListener(getFormSelectionAdapter());
		
		selectedRolesListControl.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {

				clearSecurityListControl();
				onSelectedRoleListControlChange();
			}
		
		});
		
		dimensionTabFolder.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				setSelectionControls();
			}
		});
	}

	protected void onSelectedRoleListControlChange() {

		if ( selectedRolesListControl.getSelection().length == 1 ) {
			
			//clear all the tab form controls
			clearAllTabFormControls();
			
			enableAllTabFormControls(true);
			
			populateSecurityForm(selectedRolesListControl.getSelection()[0]);
			
			setSelectionControls();
			
		} else {
			
			enableAllTabFormControls(false);
			
		}
	}
	
	protected void onSelectedRoleListControlChange(int idx) {

		if ( selectedRolesListControl.getSelection().length == 1 ) {
			
			//clear all the tab form controls
			clearAllTabFormControls();
			
			enableAllTabFormControls(true);
			
			populateSecurityForm(selectedRolesListControl.getItem(idx));
			
			setSelectionControls();
			
		} else {
			
			enableAllTabFormControls(false);
			
		}
	}
	
	protected void onSelectedDimensionTreeControlChange() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && dimensionControlTreeMap.get(mapKeyDimName).getSelectionCount() == 1 ) { 
			enableAllMapControls(addButtonMap, true);
		}
		else {
			enableAllMapControls(addButtonMap, false);
		}
	}
	
	protected void onSelectedSecurityListControlChange() {

		populateSelectionTypeOptions();
		
		setSelectionControls();

	}

	public void setSelectionControls() {
		
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
		
		if( securityMemberList.getSelectionCount() == 1 ) {
			enableAllMapControls(removeButtonMap, true);
			enableAllMapControls(selectionOnlyMap, true);
			enableAllMapControls(selectionChildrenMap, true);
			enableAllMapControls(selectionDescendantsMap, true);
			enableAllMapControls(levelGenerationMap, selectionDescendantsMap.get(mapKeyDimName).getSelection());
			enableAllMapControls(levelGenSpinnerMap, selectionDescendantsMap.get(mapKeyDimName).getSelection());
		}
		else {
			enableAllMapControls(removeButtonMap, false);
			enableAllMapControls(selectionOnlyMap, false);
			enableAllMapControls(selectionChildrenMap, false);
			enableAllMapControls(selectionDescendantsMap, false);
			enableAllMapControls(levelGenerationMap, false);
			enableAllMapControls(levelGenSpinnerMap, false);
		}
		
		if( securityMemberList.getItemCount() > 0 ) {
			enableAllMapControls(clearButtonMap, true);
			enableAllMapControls(validateButtonMap, true);
		}
		else {
			enableAllMapControls(clearButtonMap, false);
			enableAllMapControls(validateButtonMap, false);
		}
	}

	protected void updateSecurityMember() {
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];

		String mapKeyDimName = selectedTabItem.getText();

		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && selectedRolesListControl.getSelection().length == 1 ) {
			List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
			
			if( securityMemberList != null && securityMemberList.getSelection().length == 1) {
			    	
				String secMember = securityMemberList.getSelection()[0];
				int index = securityMemberList.getSelectionIndex();
				SecurityMember securityMember = new SecurityMember(mapKeyDimName,secMember);
				
				String roleName = selectedRolesListControl.getSelection()[0];
				Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
				java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
				
				if( securityMemberArray.contains(securityMember) )
					securityMemberArray.remove(securityMember);
				
				updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );

				if( !securityMemberArray.contains(securityMember) ) {
					securityMemberArray.add(securityMember);
				    securityMemberMap.put(mapKeyDimName, securityMemberArray);
				    roleSecurityMap.put(roleName, securityMemberMap);
				}
					
				securityMemberList.setItem(index, securityMember.toString());
				
				setSelectedSecurityDimension(mapKeyDimName, securityMember.getMember());
				boldTreeItem(mapKeyDimName, securityMember.getMember(), true);
		    }
		}		
	}
	
	protected void populateSelectionTypeOptions() {
		
		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];
		String mapKeyDimName = selectedTabItem.getText();
		List secMemList = securityMemberControlMap.get(mapKeyDimName);
		
		if ( secMemList.isEnabled() && secMemList.getSelection().length == 1 ) {
			SecurityMember selectedSecMem = new SecurityMember(mapKeyDimName, secMemList.getSelection()[0]);
			
			populateSelectionTypeOptions(selectedSecMem, mapKeyDimName);
			
			setSelectedSecurityDimension(mapKeyDimName, selectedSecMem.getMember());
			
			boldTreeItem(mapKeyDimName, selectedSecMem.getMember(), true);
		}
	}

	protected void populateSelectionTypeOptions(SecurityMember securityMember, String dimension) {
		// if not null, otherwise select selection only
		if (securityMember.getSelectionType() != null) {

			// select correct selection type
			switch (securityMember.getSelectionType()) {

			case Selection:
				selectionOnlyMap.get(dimension).setSelection(true);
				selectionChildrenMap.get(dimension).setSelection(
						false);
				selectionDescendantsMap.get(dimension)
						.setSelection(false);
				break;
			case SelectionAndChildren:
				selectionOnlyMap.get(dimension).setSelection(false);
				selectionChildrenMap.get(dimension).setSelection(
						true);
				selectionDescendantsMap.get(dimension)
						.setSelection(false);
				break;
			case SelectionAndDescendants:
				selectionOnlyMap.get(dimension).setSelection(false);
				selectionChildrenMap.get(dimension).setSelection(
						false);
				selectionDescendantsMap.get(dimension)
						.setSelection(true);

				break;

			}

		} else {

			selectionOnlyMap.get(dimension).setSelection(true);
			selectionChildrenMap.get(dimension).setSelection(false);
			selectionDescendantsMap.get(dimension).setSelection(false);
		}

		if (securityMember.getLevelGenType() != null) {

			Combo levelGenCombo = levelGenerationMap.get(dimension);
			levelGenCombo.setText(securityMember.getLevelGenType().toString());
			
			Spinner levelGenSpinner = levelGenSpinnerMap.get(dimension);
			
			TreeItem selectedTreeItem = dimensionControlTreeMap.get(dimension).getSelection()[0];
			PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();
			
			String levelGenrationItem = levelGenCombo.getItem(levelGenCombo.getSelectionIndex());
			Integer maxTreeDepth = maxTreeDepthMap.get(dimension);
			
			int minLevelGenSpinnerLength = 0;
			int maxLevelGenSpinnerLength = 0;

			if (levelGenrationItem.equals(LevelGenerationType.Level.toString())) {

				int selectedMemberLevelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();
				minLevelGenSpinnerLength = 0;
				maxLevelGenSpinnerLength = selectedMemberLevelNumber;

			} else if (levelGenrationItem.equals(LevelGenerationType.Generation.toString())) {

				minLevelGenSpinnerLength = selectedMember.getPafSimpleDimMemberProps().getGenerationNumber();
				maxLevelGenSpinnerLength = maxTreeDepth + 1;

			}
			levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
			levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);
			
			levelGenSpinner.setSelection(securityMember.getLevelGenNumber());

		}
	}
	
	protected void updateSelectionTypeForSecurityMember(SecurityMember securityMember, String dimension) {

		// select correct button
		if (selectionOnlyMap.get(dimension).getSelection()) {
			
			securityMember.setSelectionType(SelectionType.Selection);

		} else if (selectionChildrenMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.SelectionAndChildren);

		} else if (selectionDescendantsMap.get(dimension).getSelection()) {

			securityMember.setSelectionType(SelectionType.SelectionAndDescendants);

			TreeItem selectedTreeItem = dimensionControlTreeMap.get(dimension).getSelection()[0];
			PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem.getData();

			Combo levelGenCombo = levelGenerationMap.get(dimension);
			Spinner levelGenSpinner = levelGenSpinnerMap.get(dimension);

			String levelGenrationItem = levelGenCombo.getItem(levelGenCombo.getSelectionIndex());

			int currentLevelGenSpinnerNumber = levelGenSpinner.getSelection();

			Integer maxTreeDepth = maxTreeDepthMap.get(dimension);

			int minLevelGenSpinnerLength = 0;

			int maxLevelGenSpinnerLength = 0;

			if (levelGenrationItem.equals(LevelGenerationType.Level.toString())) {

				securityMember.setLevelGenType(LevelGenerationType.Level);

				int selectedMemberLevelNumber = selectedMember.getPafSimpleDimMemberProps().getLevelNumber();

				minLevelGenSpinnerLength = 0;
				maxLevelGenSpinnerLength = selectedMemberLevelNumber;

			} else if (levelGenrationItem.equals(LevelGenerationType.Generation.toString())) {

				securityMember.setLevelGenType(LevelGenerationType.Generation);

				minLevelGenSpinnerLength = selectedMember.getPafSimpleDimMemberProps().getGenerationNumber();
				maxLevelGenSpinnerLength = maxTreeDepth + 1;

			}

			levelGenSpinner.setMinimum(minLevelGenSpinnerLength);
			levelGenSpinner.setMaximum(maxLevelGenSpinnerLength);

			if (currentLevelGenSpinnerNumber < minLevelGenSpinnerLength) {
				currentLevelGenSpinnerNumber = minLevelGenSpinnerLength;
			} else if (currentLevelGenSpinnerNumber > maxLevelGenSpinnerLength) {
				currentLevelGenSpinnerNumber = maxLevelGenSpinnerLength;
			}

			securityMember.setLevelGenNumber(currentLevelGenSpinnerNumber);

			levelGenSpinner.setSelection(currentLevelGenSpinnerNumber);
		}
		
	}

	/**
	 * Called when any item's selection status changes on the security form.
	 * 
	 */

	protected void addNewSecurityMember() {

		TabItem selectedTabItem = dimensionTabFolder.getSelection()[0];

		String mapKeyDimName = selectedTabItem.getText();

		unboldDimensionTreeItems(mapKeyDimName);

		// if the dimension tab is enabled
		if (dimensionControlTreeMap.get(mapKeyDimName).isEnabled() && selectedRolesListControl.getSelection().length == 1 ) {

			// get selected tree item name
			TreeItem selectedTreeItem = dimensionControlTreeMap.get(
					mapKeyDimName).getSelection()[0];

			PafSimpleDimMember selectedMember = (PafSimpleDimMember) selectedTreeItem
					.getData();

			// get selected tree item name
			String selectedTreeItemName = selectedMember.getKey();

			// bold newly selected tree item
			boldTreeItem(mapKeyDimName, selectedTreeItemName, true);

			SecurityMember securityMember = new SecurityMember();
			securityMember.setDimensionName(mapKeyDimName);
			securityMember.setMember(selectedTreeItemName);

			if( ! selectionOnlyMap.get(mapKeyDimName).getSelection() 
					&& ! selectionChildrenMap.get(mapKeyDimName).getSelection() 
					&& !selectionDescendantsMap.get(mapKeyDimName).getSelection() ) {
				selectionOnlyMap.get(mapKeyDimName).setSelection(true);
			}

			updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );
			
			List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
			
			//Now let's add it into the Security Specificaiton List Control
			String roleName = selectedRolesListControl.getSelection()[0];
			// log message
			logSecurityMemberMessage(roleName, mapKeyDimName,"Security Form Modified");

			
			// get security member map
			Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
			
			if (securityMemberMap == null ) {
				securityMemberMap = new HashMap<String,  java.util.List<SecurityMember>>();
			}
		    // try to get security member
			java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
			Map<String,String> memberAliasListMap = this.secSubMemberListTextMap.get(mapKeyDimName);
		    //if the dimension has security members already, then add more into it
		    if( securityMemberArray == null ) {
			    securityMemberArray = new ArrayList<SecurityMember>();
		    }
		    
		    String temp = securityMember.toString().replace(selectedTreeItemName, selectedTreeItem.getText());
		    if( ! securityMemberArray.contains(securityMember) ) {
		    	securityMemberArray.add(securityMember);
			    securityMemberMap.put(mapKeyDimName, securityMemberArray);
			    roleSecurityMap.put(roleName, securityMemberMap);
			    
			    
		    	securityMemberList.add(temp);
		    	memberAliasListMap.put(selectedTreeItem.getText(), selectedTreeItemName);

		    }
		    
		    securityMemberList.setSelection(new String[] {temp});
		    
			enableAllMapControls(removeButtonMap, true);
			enableAllMapControls(clearButtonMap, true);
			enableAllMapControls(validateButtonMap, true);
		}	
	}

	private void clearSecurityListControl() {
		TabItem[] tabItems = dimensionTabFolder.getItems();
		// for each hier dim
		for (TabItem tabItem : tabItems) {
			String mapKeyDimName = tabItem.getText();
			unboldDimensionTreeItems(mapKeyDimName);
			securityMemberControlMap.get(mapKeyDimName).removeAll();
		}
	}
	
	/**
	 * Populates the security form based ont the role
	 * 
	 * @param roleName
	 *            Role Name
	 */
	protected void populateSecurityForm(String roleName) {

		TabItem[] tabItems = dimensionTabFolder.getItems();

		// for each hier dim
		for (TabItem tabItem : tabItems) {

			// get map key dimension name
			String mapKeyDimName = tabItem.getText();

			// try go get a security member map from role security map
			Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);

			// if null, create a new one
			if (securityMemberMap == null) {
				securityMemberMap = new HashMap<String, java.util.List<SecurityMember>>();
			}

			// try to get security members for the specified dimension
			java.util.List<SecurityMember> securityMemberArray = securityMemberMap.get(mapKeyDimName);
			// if not null, add each security member into the Security Specificaiton List Control
			if (securityMemberArray != null && securityMemberArray.size() > 0 ) {

				// log
				logSecurityMemberMessage(roleName, mapKeyDimName,"Security Form Being Populated Before");

				// get tree item map
				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(mapKeyDimName);
				for( SecurityMember securityMember : securityMemberArray ) {
					// if security members member is not null
					if (securityMember.getMember() != null) {
	
						// if tree item maps exist
						if (treeItemMap != null) {
	
							TreeItem securityMemberTreeItem = treeItemMap
									.get(securityMember.getMember());
	
							// if tree item found, get set selection to it
							if (securityMemberTreeItem != null) {
	
								// set selction to found tree item
								dimensionControlTreeMap.get(mapKeyDimName)
										.setSelection(securityMemberTreeItem);
								
								findTreeModuleMap.get(mapKeyDimName).setTree(dimensionControlTreeMap.get(mapKeyDimName));
	
								// bold tree item
								boldTreeItem(securityMemberTreeItem, true);
								
							}
							//JIra 1770-AC needs to handle when a security member loaded from XML does not exist in EssBase outline
							List securityMemberList = securityMemberControlMap.get(mapKeyDimName);
							Map<String,String> memberAliasListMap = this.secSubMemberListTextMap.get(mapKeyDimName);
							if( ! Arrays.asList(securityMemberList.getItems()).contains(securityMember.toString()) ){
								securityMemberList.add(securityMember.toString());
								securityMemberList.setSelection(new String[] {securityMember.toString()});
								if(securityMember.toString().indexOf(',') != -1){
									String temp = securityMember.toString().substring(securityMember.toString().lastIndexOf('(') + 1, securityMember.toString().indexOf(','));
									memberAliasListMap.put(temp, temp);
								} else if (securityMember.toString().indexOf('(') != -1){
									String temp = securityMember.toString().substring(securityMember.toString().lastIndexOf('(') + 1, securityMember.toString().indexOf(')'));
									memberAliasListMap.put(temp, temp);
								} else {
									memberAliasListMap.put(securityMember.toString(), securityMember.toString());
								}
							}
			
							populateSelectionTypeOptions(securityMember, mapKeyDimName );
							
							updateSelectionTypeForSecurityMember(securityMember, mapKeyDimName );

							logSecurityMemberMessage(roleName, mapKeyDimName,"Security Form Being Populated After");
						}
					}
				}//end of for loop
			}
		}
	}

	private void enableLevelGenerationControls(String mapKeyDimName,
			boolean isEnabled) {

		levelGenerationMap.get(mapKeyDimName).setEnabled(isEnabled);
		levelGenSpinnerMap.get(mapKeyDimName).setEnabled(isEnabled);

	}

	/**
	 * Used to log info about the model state.
	 * 
	 * @param roleName
	 *            current role
	 * @param mapKeyDimName
	 *            tab selected
	 * @param beginningText
	 *            text within the ----
	 */
	private void logSecurityMemberMessage(String roleName,
			String mapKeyDimName, String beginningText) {
		logger.debug("-------------------" + beginningText
				+ "-----------------------");
		logger.debug("Role Name: " + roleName);
		logger.debug("Tab Name: " + mapKeyDimName);
//		logger.debug("Security Member: "
//				+ securityMemberControlMap.get(mapKeyDimName).getText());
		logger.debug("Tree Item: "
				+ dimensionControlTreeMap.get(mapKeyDimName).getSelection()[0]
						.getText());
		logger.debug("Selection: "
				+ selectionOnlyMap.get(mapKeyDimName).getSelection());
		logger.debug("Selection And Children: "
				+ selectionChildrenMap.get(mapKeyDimName).getSelection());
		logger.debug("Selection And Desc: "
				+ selectionDescendantsMap.get(mapKeyDimName).getSelection());
		logger.debug("Level or Generation: "
				+ levelGenerationMap.get(mapKeyDimName).getItem(
						levelGenerationMap.get(mapKeyDimName)
								.getSelectionIndex()));
		logger.debug("Spinner Number: "
				+ levelGenSpinnerMap.get(mapKeyDimName).getSelection());

	}


	/**
	 * Gets the form selection adapter. If null, creates one.
	 * 
	 * @return SelectionAdapter
	 * 
	 */
	private SelectionAdapter getFormSelectionAdapter() {

		if (formSelectionAdapter == null) {

			formSelectionAdapter = new SelectionAdapter() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent e) {

					editorModified();

				}

			};

		}

		return formSelectionAdapter;
	}

	private MouseListener getTabTreeMouseListener() {

		if (tabTreeMouseListener == null) {

			tabTreeMouseListener = new MouseListener() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void mouseDoubleClick(MouseEvent e) {

					addNewSecurityMember();
					setSelectionControls();
					editorModified();

				}

				@Override
				public void mouseDown(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseUp(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

			};

		}

		return tabTreeMouseListener;
	}
	/**
	 * The tree item should be bolded / unbolded based on bold flag. Then the
	 * parent item should be bolded / unbolded based on if any of it's children
	 * are bolded.
	 * 
	 * @param treeItem
	 *            tree item used to bold/unbold
	 * @param bold
	 *            true/false if item should be bolded
	 */

	private void boldTreeItem(TreeItem treeItem, boolean bold) {

		Font font = treeItem.getFont();

		// get font data ar for tree item
		FontData[] fontDataAr = font.getFontData();

		if (fontDataAr.length > 0) {

			// get font data
			FontData fontData = fontDataAr[0];

			// if bold is true, set style to bold, else to none
			if (bold) {

				fontData.setStyle(SWT.BOLD);

				if (boldFont == null) {

					boldFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(boldFont);

			} else {

				fontData.setStyle(SWT.NONE);

				if (normalFont == null) {

					normalFont = new Font(treeItem.getDisplay(), fontData);

				}

				treeItem.setFont(normalFont);
			}

		}

	}

	/**
	 * Bolds tree item.
	 * 
	 * @param dimensionName
	 * @param treeItemName
	 * @param bold
	 */
	private void boldTreeItem(String dimensionName, String treeItemName,
			boolean bold) {

		if (dimensionMapOfTreeItemMap != null) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
					.get(dimensionName);

			if (treeItemMap != null) {

				TreeItem treeItem = treeItemMap.get(treeItemName);

				if (treeItem != null) {

					boldTreeItem(treeItem, bold);

				}

			}

		}

	}

	/**
	 * Unbolds tree item.
	 * 
	 * @param dimensionName
	 */
	private void unboldDimensionTreeItems(String dimensionName) {

		if (dimensionMapOfTreeItemMap != null) {

			// for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

			Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
					.get(dimensionName);

			if (treeItemMap != null) {

				for (TreeItem treeItem : treeItemMap.values()) {

					boldTreeItem(treeItem, false);

				}

			}

			// }

		}

	}

	/**
	 * Unbolds all tree items.
	 * 
	 */
	private void unboldAllDimensionTreeItems() {

		if (dimensionMapOfTreeItemMap != null) {

			for (String dimensionName : dimensionMapOfTreeItemMap.keySet()) {

				Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap
						.get(dimensionName);

				if (treeItemMap != null) {

					for (TreeItem treeItem : treeItemMap.values()) {

						boldTreeItem(treeItem, false);

					}

				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {

		if (boldFont != null) {
			boldFont.dispose();
		}

		if (normalFont != null) {
			normalFont.dispose();
		}

		//Commented for the exception started from Eclipse 4.3, plus this seems no use either.
//		try {
//			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//			if( page != null ) {
//				InvalidSecurityMemberView invalidSecurityMemberView = (InvalidSecurityMemberView) PlatformUI
//						.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(InvalidSecurityMemberView.ID);
//	
//				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().hideView(invalidSecurityMemberView);
//			}
//		} catch (PartInitException e) {
//			e.printStackTrace();
//		}
//
		super.dispose();
	}

	/**
	 * Populates the contorls with values. Called only when editor opens.
	 * 
	 */
	private void populateControlValues() {
		
		try {

			// roles
			String[] roleNames = userSecurityInput.getRoleNames();

			if (userSecurityInput.isNew() ) {
				
				populateDomainFilterCombo();
			
				EditorControlUtil.addItemsToList(availableRolesListControl, roleNames);
	
			} else {
								
				
				if ( userSecurityInput.isClone() ) {
					
					populateDomainFilterCombo();
					
					EditorControlUtil.addItemsToList(availableRolesListControl, roleNames);
					
				} else {
				
					EditorControlUtil.addItems(usernameCombo,
							new String[] { userSecurityInput.getName() });
					usernameCombo.setEnabled(false);
					usernameCombo.setText(userSecurityInput.getName());
					
				}
								
				PafUserSecurity currentPafUser = userSecurityInput
						.getCurrentUserSecurity();

				if (currentPafUser != null) {

					String domainName = currentPafUser.getDomainName();
					
					if ( domainName == null || domainName.trim().length() == 0 ) {
						
						domainName = PafBaseConstants.Native_Domain_Name;
						
					}

					if ( ! userSecurityInput.isClone() ) {
						//set domain name
						edit_UserTypeValueLabel.setText(domainName);
					}
					
					EditorControlUtil.checkButton(adminCheckBox, currentPafUser
							.getAdmin());

					String[] userRoleNames = currentPafUser.getRoleNames();
					
					EditorControlUtil.addItemsToList(selectedRolesListControl, userRoleNames);
					
					if ( roleNames != null ) {
						
						java.util.List<String> roleNameList = new ArrayList<String>(Arrays.asList(roleNames));
						
						if ( userRoleNames != null ) {
							
							roleNameList.removeAll(Arrays.asList(userRoleNames));
							
						}

						EditorControlUtil.addItemsToList(availableRolesListControl, roleNameList.toArray(new String[0]));
						
					}
		

					Set<InvalidSecurityMember> invalidMemberSet = new LinkedHashSet<InvalidSecurityMember>();
					for (String userRoleName : userRoleNames) {

						PafWorkSpec[] pafWorkSpecAr = currentPafUser
								.getRoleFilters().get(userRoleName);
						
						if (pafWorkSpecAr != null) {
							for (PafWorkSpec pafWorkSpec : pafWorkSpecAr) {

								PafDimSpec[] pafDimSpecAr = pafWorkSpec.getDimSpec();

								if (pafDimSpecAr != null) {

									for (PafDimSpec pafDimSpec : pafDimSpecAr) {
										String dimensionName = pafDimSpec.getDimension();
										Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);
										// verify security member is still
										// valid member in outline
										if (dimTreeItemMap != null) {
											if (pafDimSpec.getExpressionList() != null
													&& pafDimSpec.getExpressionList().length > 0) {
												String[] secMemStrAry = pafDimSpec.getExpressionList();
												java.util.List<SecurityMember> secMemObjAry = new ArrayList<SecurityMember>();
												for( String secMem : secMemStrAry ) {
													SecurityMember securityMember = new SecurityMember(
															dimensionName, secMem);
													//TODO
													if (!dimTreeItemMap
															.containsKey(securityMember
																	.getMember())) {
		
														InvalidSecurityMember invalidSecurityMember = new InvalidSecurityMember();
		
														invalidSecurityMember
																.setRoleName(userRoleName);
														invalidSecurityMember
																.setDimensionName(securityMember
																		.getDimensionName());
														invalidSecurityMember
																.setMemberName(securityMember
																		.getMember());
		
														invalidMemberSet
																.add(invalidSecurityMember);
		
													}
		
													Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap
															.get(userRoleName);
			
													if (securityMemberMap == null) {
			
														securityMemberMap = new HashMap<String, java.util.List<SecurityMember>>();
			
													}
													
													if( ! secMemObjAry.contains(securityMember)) {
														secMemObjAry.add(securityMember);
														securityMemberMap.put(dimensionName, secMemObjAry);
														roleSecurityMap.put(userRoleName, securityMemberMap);
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if (invalidMemberSet.size() > 0) {

						Shell shell = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell();

						InvalidMemberErrorDialog invalidSecurityMemberErrorDialog = new InvalidMemberErrorDialog(
								shell, invalidMemberSet);
						invalidSecurityMemberErrorDialog.open();

						InvalidSecurityMemberView invalidSecurityMemberView = (InvalidSecurityMemberView) PlatformUI
								.getWorkbench().getActiveWorkbenchWindow()
								.getActivePage().showView(
										InvalidSecurityMemberView.ID);

						invalidSecurityMemberView
								.setInvalidSecurityMemberSet(invalidMemberSet);

						setDirty( true );
					}
					else {
						outerloop:
						for (String userRoleName : userRoleNames) {

							PafWorkSpec[] pafWorkSpecAr = currentPafUser
									.getRoleFilters().get(userRoleName);

							if (pafWorkSpecAr != null) {

								for (PafWorkSpec pafWorkSpec : pafWorkSpecAr) {

									PafDimSpec[] pafDimSpecAr = pafWorkSpec.getDimSpec();

									if (pafDimSpecAr != null) {
										//missing dimension
										if( pafDimSpecAr.length < dimensionTabFolder.getItems().length ) {
											java.util.List<String> dimSpecList = new ArrayList<String>();
											for( PafDimSpec dimSpec : pafDimSpecAr) {
												dimSpecList.add(dimSpec.getDimension());
											}
											java.util.List<String> unconfiguredDimList = new ArrayList<String>(securityMemberControlMap.keySet());
											unconfiguredDimList.removeAll(dimSpecList); 
											if ( unconfiguredDimList != null && unconfiguredDimList.size() > 0 ) {
												StringBuffer strBuff = new StringBuffer("The following dimensions for role: " + userRoleName + " have no specification configured:\n");
												for (String unconfiguredDim : unconfiguredDimList ) {
													
													strBuff.append("  -" + unconfiguredDim + "\n");
												}
												GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, strBuff.toString(), MessageDialog.ERROR);
												setDirty( true );
												break outerloop;
											}
										}
										else if( pafDimSpecAr.length == dimensionTabFolder.getItems().length ) {
											for (PafDimSpec pafDimSpec : pafDimSpecAr) {
	
												String dimensionName = pafDimSpec.getDimension();
	
												String[] securityMemAry = pafDimSpec.getExpressionList();
												if ( securityMemAry != null ) {
													if( securityMemAry.length == 0 ) {
														GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "At least 1 security specification needed for '" + userRoleName + "/" + dimensionName + "'.", MessageDialog.ERROR);
														setDirty( true );
														break outerloop;
													}
													else if(securityMemAry.length > 0 ) {
														ValidationResponse valResp  = validateUserSecurity(dimensionName,Arrays.asList(securityMemAry)); 
														if( valResp != null ) {
															if( ! valResp.isSuccess() ) { //validation failed
																java.util.List<String> errorList = valResp.getValidationErrors();
																if( errorList.size() > 0 ) {
																	Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
																	ListErrorMessagesDialog userSecurityValidationDialog = new ListErrorMessagesDialog(
																			shell, 
																			errorList,
																			"Invalid User Security Specification",
																			"The following are invalid security members for " + userRoleName + "/" + dimensionName + ". Please re-configure.");
																	userSecurityValidationDialog.open();
																	setDirty( true );
																	break outerloop;
																}
															}
														}
														else { //service call failed
															GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "There is an issue with server. The security members for '" + userRoleName + "/" +  dimensionName + "' have NOT been validated.", MessageDialog.INFORMATION);
															setDirty( true );
															break outerloop;
														}
													}
												}
											}
										}
									}
								}
							}
							// now we are clean...
							setDirty( false);
						}
					}
				}
			}

			//disable fields if not in mixed mode
			if ( (userSecurityInput.isNew() || userSecurityInput.isClone()) && ! userSecurityInput.isServerInMixedMode() ) {

				new_DomainFilterCombo.setEnabled(false);
				new_SecurityGroupFilterCombo.setEnabled(false);
				displayAllFilterButton.setEnabled(false);
				
			} 
			

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	private void populateDomainFilterCombo() {

		boolean displayAll = false;
		
		if ( displayAllFilterButton != null ) {
			
			displayAll = displayAllFilterButton.getSelection();
			
		}
		
		String[] domainNames = userSecurityInput.getDomainNames(displayAll);
		
		if ( domainNames != null ) {
			
			EditorControlUtil.addItems(new_DomainFilterCombo, domainNames);
			
			new_DomainFilterCombo.select(0);
		}
						
	}
	
	private void populateSecurityGroupFilterCombo() {

		if ( new_DomainFilterCombo != null && new_SecurityGroupFilterCombo != null ) {
		
			String domainName = new_DomainFilterCombo.getText();
									
			if ( domainName != null && ! domainName.equals("")) {
			
				if ( lastDomainNameSelected == null || (lastDomainNameSelected != null && ! lastDomainNameSelected.equals(domainName))) {
				
					boolean displayAll = false;
					
					if ( displayAllFilterButton != null ) {
						
						displayAll = displayAllFilterButton.getSelection();
						
					}
					
					String[] securityGroups = userSecurityInput.getSecurityGroupNames(domainName, displayAll);
					
					if ( securityGroups != null ) {
						
						EditorControlUtil.addItems(new_SecurityGroupFilterCombo, securityGroups);
						
						new_SecurityGroupFilterCombo.select(0);
						
					} else {
						
						new_SecurityGroupFilterCombo.removeAll();
						
					}
					
					lastDomainNameSelected = domainName;
					
				}
								
			} else {
				
				lastDomainNameSelected = null;
				
			}
		
		}
		
		populateUserNamesCombo();
		
	}
		
	private void populateUserNamesCombo() {
		
		if ( new_DomainFilterCombo != null && new_SecurityGroupFilterCombo != null && usernameCombo != null ) {
			
			String domainName = new_DomainFilterCombo.getText();
			
			String securityGroup = new_SecurityGroupFilterCombo.getText();
			
			//if 1st time, or if the last security group does not equal selected security group
			if ( lastSecurityGroupSelected == null || (lastSecurityGroupSelected != null && ! lastSecurityGroupSelected.equals(securityGroup)) ) {
			
				PaceUser[] paceUsers = userSecurityInput.getUnconfiguredPaceUsers(domainName, securityGroup);
				
				if ( paceUsers != null ) {
					
					usernameCombo.setData(paceUsers);
					
					String[] displayNames = new String[paceUsers.length];
					
					for (int i = 0; i < paceUsers.length; i++ ) {
						
						displayNames[i] = paceUsers[i].getDisplayName();
						
					}
				
					EditorControlUtil.addItems(usernameCombo, displayNames);
					
				} else {
					
					usernameCombo.removeAll();
					
				}
				
			}
			
			//if not null and not empty
			if ( securityGroup != null && ! securityGroup.equals("" ) ) {
				lastSecurityGroupSelected = securityGroup;
			} else {
				lastSecurityGroupSelected = null;
			}
			
		}
		
	}
	
	/**
	 * Sets the dirty flag as fires the propery changes, which signals to the
	 * program that the editor is dirty.
	 */
	public void editorModified() {
		boolean wasDirty = isDirty();
		setDirty( true );
		if (!wasDirty) {
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	/**
	 * Implementation of EditorPart.doSave.
	 * 
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if (isDirty) {
	
			try {
				String missingField = isInputValid();
				if (missingField != null) {
					GUIUtil.openMessageWindow(this.getTitle(),
							"The following fields are required:\n-"
									+ missingField, SWT.ICON_INFORMATION);
					
					monitor.setCanceled(true);
					
					return;
				}
				
				java.util.List<String> unconfiguredRolesList = new ArrayList<String>(Arrays.asList(selectedRolesListControl.getItems()));
				
				if ( roleSecurityMap != null ) {
					
					unconfiguredRolesList.removeAll(roleSecurityMap.keySet());
					
				}				
				
				if ( unconfiguredRolesList != null && unconfiguredRolesList.size() > 0 ) {
					
					StringBuffer strBuff = new StringBuffer("The following roles have been selected but not configured:\n");
					
					for (String unconfiguredRole : unconfiguredRolesList ) {
						
						strBuff.append("  -" + unconfiguredRole + "\n");
					}
					
					GUIUtil.openMessageWindow(this.getTitle(), strBuff.toString(), SWT.ICON_INFORMATION);
					
					monitor.setCanceled(true);
					
					return;
					
				}

				
				PafUserSecurity securityUser = userSecurityInput
						.getCurrentUserSecurity();

				if (securityUser == null) {

					securityUser = new PafUserSecurity();

				}
			
				if ( userSecurityInput.isNew() || userSecurityInput.isClone() ) {
					
					String displayName = usernameCombo.getText();
					
					PaceUser[] paceUsers = (PaceUser[]) usernameCombo.getData();
					
					String userName = null;
					
					if ( paceUsers != null)  {
						
						for (PaceUser paceUser : paceUsers ) {
							
							if ( paceUser.getDisplayName().equals(displayName)) {
								
								userName = paceUser.getUserName();
								
								break;
								
							}
							
						}
						
						
					}
					
					if ( userName == null ) {
					
						securityUser.setUserName(displayName);
						
					} else {
						
						securityUser.setUserName(userName);
						
					}
					
					if ( ! userName.equals(displayName)) {
						
						securityUser.setDisplayName(displayName);
						
					}
					
					
					if ( new_DomainFilterCombo != null ) {
						
						String domainName = new_DomainFilterCombo.getText();
						
						if ( domainName != null && ! domainName.equals("") && ! domainName.equalsIgnoreCase(PafBaseConstants.Native_Domain_Name)) {
							
							securityUser.setDomainName(domainName);
							
						}
						
					}
															
				} 
				
				securityUser.setAdmin(adminCheckBox.getSelection());
				
				/*check against local dimension cache
				 * 
				 */
				Set<InvalidSecurityMember> invalidMemberSet = new LinkedHashSet<InvalidSecurityMember>();
				for (String roleName : selectedRolesListControl.getItems()) {
					Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
					if( securityMemberMap.size() < dimensionTabFolder.getItems().length ) {
						java.util.List<String> unconfiguredDimList = new ArrayList<String>(securityMemberControlMap.keySet());
						unconfiguredDimList.removeAll(securityMemberMap.keySet()); 
						if ( unconfiguredDimList != null && unconfiguredDimList.size() > 0 ) {
							StringBuffer strBuff = new StringBuffer("The following dimensions for role: " + roleName + " have no specification configured:\n");
							for (String unconfiguredDim : unconfiguredDimList ) {
								strBuff.append("  -" + unconfiguredDim + "\n");
							}
							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, strBuff.toString(), MessageDialog.ERROR);
							return;
						}
					}
					if (securityMemberMap != null) {
						for (String dimensionName : securityMemberMap.keySet()) {
							java.util.List<SecurityMember> securityMembeArray = securityMemberMap.get(dimensionName);
							if( securityMembeArray.size() == 0 ) {
								GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "At least 1 security specification needed for '" + roleName + "/" + dimensionName + "'.", MessageDialog.ERROR);
								return;
							}
							
							Map<String, TreeItem> dimTreeItemMap = dimensionMapOfTreeItemMap.get(dimensionName);
							if (dimTreeItemMap != null) {
								// verify security member is still valid member in outline
								for( SecurityMember securityMember : securityMembeArray ) {
									//TODO
									if (!dimTreeItemMap.containsKey(securityMember.getMember())) {
										InvalidSecurityMember invalidSecurityMember = new InvalidSecurityMember();
										invalidSecurityMember.setRoleName(roleName);
										invalidSecurityMember.setDimensionName(securityMember.getDimensionName());
										invalidSecurityMember.setMemberName(securityMember.getMember());
										invalidMemberSet.add(invalidSecurityMember);
									}
								}
							}
						}
					}
				}
				if( invalidMemberSet.size() > 0 ) {
					
					Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	
					InvalidMemberErrorDialog invalidSecurityMemberErrorDialog = new InvalidMemberErrorDialog(
							shell, invalidMemberSet);
					invalidSecurityMemberErrorDialog.open();
	
					InvalidSecurityMemberView invalidSecurityMemberView;
					try {
						invalidSecurityMemberView = (InvalidSecurityMemberView) PlatformUI
								.getWorkbench().getActiveWorkbenchWindow()
								.getActivePage().showView(
										InvalidSecurityMemberView.ID);
						invalidSecurityMemberView.setInvalidSecurityMemberSet(invalidMemberSet);
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return;
				}
				
				LinkedHashMap<String, PafWorkSpec[]> pafWorkSpecMap = new LinkedHashMap<String, PafWorkSpec[]>();

				for (String roleName : selectedRolesListControl.getItems()) {

					PafWorkSpec[] pafWorkSpecAr = new PafWorkSpec[1];

					pafWorkSpecAr[0] = new PafWorkSpec();

					pafWorkSpecAr[0].setName(roleName);

					Map<String, java.util.List<SecurityMember>> securityMemberMap = roleSecurityMap.get(roleName);
					if (securityMemberMap != null) {
						PafDimSpec[] pafDimSpecAr = new PafDimSpec[securityMemberMap.size()];
						int pafDimSpecNdx = 0;
						for (String dimensionName : securityMemberMap.keySet()) {
							java.util.List<SecurityMember> securityMembeArray = securityMemberMap.get(dimensionName);
							java.util.List<String> expList = new java.util.ArrayList<String>();
							for( SecurityMember securityMember : securityMembeArray) {
								expList.add(securityMember.toString());
								logger.info("Saving: " + securityMember);
							}
							ValidationResponse valResp  = validateUserSecurity( dimensionName, expList );
							if( valResp != null ) {
								if( ! valResp.isSuccess() ) { //validation failed
									java.util.List<String> errorList = valResp.getValidationErrors();
									if( errorList.size() > 0 ) {
										Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
										ListErrorMessagesDialog userSecurityValidationDialog = new ListErrorMessagesDialog(
												shell, 
												errorList,
												"Invalid User Security Specification",
												"The following are invalid security members for " + roleName + "/" + dimensionName + ". Please re-configure.");
										userSecurityValidationDialog.open();
										return;
									}
								}
							}
							else { //service call failed
								GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, "There is an issue with server. The security members for '" + roleName + "/" +  dimensionName + "' have NOT been validated.", MessageDialog.INFORMATION);
								return;
							}
							PafDimSpec pafDimSpec = new PafDimSpec();
							pafDimSpec.setDimension(dimensionName);
							pafDimSpec.setExpressionList(expList.toArray(new String[0]));
							pafDimSpecAr[pafDimSpecNdx++] = pafDimSpec;
							
						}
						pafWorkSpecAr[0].setDimSpec(pafDimSpecAr);
					}

					pafWorkSpecMap.put(roleName, pafWorkSpecAr);
				}

				securityUser.setRoleFilters(pafWorkSpecMap);


				boolean wasNewOrCloned = userSecurityInput.isNew() || userSecurityInput.isClone();
				
				userSecurityInput.setCurrentUserSecurity(securityUser);

				userSecurityInput.save();

				setDirty( false );
				
				if ( wasNewOrCloned ) {
					
					if ( usernameCombo != null ) {
						
						usernameCombo.setEnabled(false);
						
					}
					
					if ( displayAllFilterButton != null ) {
						
						displayAllFilterButton.setEnabled(false);
						
					}
					
					if ( new_DomainFilterCombo != null ) {
						
						new_DomainFilterCombo.setEnabled(false);
						
					}
					
					if ( new_SecurityGroupFilterCombo != null ) {

						new_SecurityGroupFilterCombo.setEnabled(false);
						
					}
					
				}
				
				firePropertyChange(IEditorPart.PROP_DIRTY);
				setPartName(Constants.MENU_USER_SECURITY + ": "
						+ userSecurityInput.getName());
				
				
				updateTreeViewer();

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage());
			}

		}
	}

	/**
	 * Validates the control input values.
	 * 
	 * @return The label associated with the control if an invalid value is
	 *         found, or null if the entries are valid.
	 */
	private String isInputValid() {

		if (userSecurityInput.isNew() || userSecurityInput.isClone() ) {

			if (usernameCombo.getText() == null
					|| usernameCombo.getText().length() == 0) {
				return usernameLabel.getText();
			}

		}

		return null;
	}

	/**
	 * Updates the tree viewer control whenever a planner role is added or
	 * modified.
	 */
	private void updateTreeViewer() {

		try {
			if (viewer != null && treeNode != null) {
				
				Map<String, Boolean> expandedStateMap = new HashMap<String, Boolean>();
				
				java.util.List objectList = treeNode.getChildren();
				
				if ( objectList != null ) {
					
					for (Object object : objectList) {
						
						if ( object instanceof TreeNode ) {
							
							TreeNode tmpTreeNode = (TreeNode) object;
							
							boolean isTreeNodeExpanded = viewer.getExpandedState(tmpTreeNode);
							
							expandedStateMap.put(tmpTreeNode.getName(), isTreeNodeExpanded);
							
						}
						
					}
					
				}
				
				treeNode.createChildren(null);
				viewer.refresh(treeNode);
				
				objectList = treeNode.getChildren();
				
				if ( objectList != null ) {
					
					for (Object object : objectList) {
						
						if ( object instanceof TreeNode ) {
							
							TreeNode tmpTreeNode = (TreeNode) object;
							
							if ( expandedStateMap.containsKey(tmpTreeNode.getName())) {
								
								viewer.setExpandedState(tmpTreeNode, expandedStateMap.get(tmpTreeNode.getName()));
								
							}
																									
							
						}
						
					}
					
				}
				

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * @return the treeNode
	 */
	public TreeNode getTreeNode() {
		return treeNode;
	}

	/**
	 * @param treeNode
	 *            the treeNode to set
	 */
	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	/**
	 * Sets the selected role
	 * TTN-749
	 * @param role
	 */
	public void setSelectedRole(String role) {

		if (role != null && selectedRolesListControl != null
				&& !selectedRolesListControl.isDisposed()) {

			selectedRolesListControl.setSelection(new String[] { role });
			clearSecurityListControl();
			onSelectedRoleListControlChange();
		}

	}

	/**
	 * Selects the tab with the specified string as text
	 * TTN-749
	 * @param tab
	 */
	public void setSelectedTab(String tab) {

		if (tab == null || dimensionTabFolder == null) {
			return;
		}

		TabItem[] tabItems = dimensionTabFolder.getItems();

		int index = -1;

		for (int i = 0; i < dimensionTabFolder.getItemCount(); i++) {
			if (tab.startsWith(tabItems[i].getText())) {
				index = i;
			}
		}
		if (index >= 0) {
			dimensionTabFolder.setSelection(index);
		}

	}

	/**
	 * Selects the tab with the specified string as text
	 * TTN-749
	 * @param tab
	 */
	public void setSelectedSecurityDimension(String dim, String selectedSecurity) {

		unboldAllDimensionTreeItems();
		
		if (selectedSecurity == null || dimensionTabFolder == null) {
			return;
		}
		Map<String, TreeItem> treeItemMap = dimensionMapOfTreeItemMap.get(dim);
		if (treeItemMap != null) {
			
			TreeItem securityMemberTreeItem = treeItemMap.get(selectedSecurity);
			
			if(securityMemberTreeItem == null){
				for(TreeItem item : treeItemMap.values()){
					PafSimpleDimMember mem = (PafSimpleDimMember) item.getData();
					if(mem.getPafSimpleDimMemberProps().getAliasValues().contains(selectedSecurity)){
						securityMemberTreeItem = item;
						break;
					}
				}
			}
			
			if (securityMemberTreeItem != null) {
				dimensionControlTreeMap.get(dim).setSelection(securityMemberTreeItem);
				boldTreeItem(securityMemberTreeItem, true);
			}
		}
	}
	
	public void setSelectedSecurity(String dim, String selectedSecurity) {
		List securityMemberList = securityMemberControlMap.get(dim);
		securityMemberList.setSelection(new String[] {selectedSecurity});
	}
	
	/*
	 * 
	 */
	private void resizeForm(ControlEvent e){
		
		if ( scrolledChildComposite != null && scrolledComposite != null ) {
		
			scrolledComposite.setMinSize(scrolledChildComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			
		}
	}

	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.input.SeasonInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.data.ExpOpCode;
import com.pace.base.utility.StringUtils;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

/**
 * Implementation of EditorPart, used to add or modify Seasons.
 * 
 * @author kmoos
 * @version x.xx
 */
public class SeasonEditor extends EditorPart {
	/**
	 * 
	 */
	public static final String ID = "com.pace.admin.menu.editors.season";
	private static final String YEARS = "Year";
	private static final String PLANNABLE = "Plannable";
	private static final String NONPLANNABLE = "Read-Only";
	private static final String NONAVAILABLE = "Not Loaded";
	private static final String AVAILABILITY = "Availability";
	private static final String[] PROPS = { YEARS, AVAILABILITY };
	private static final String[] YEAR_AVAILABILITY = { NONAVAILABLE, NONPLANNABLE, PLANNABLE };
	private static final int NUMBER_OF_LEVELS = 13;
	private static final int SELECTION = 0;
	private static final int SELECTION_CHILDREN = 1;
	private static final int SELECTION_DESCENTENTS = 2;
	private static Logger logger = Logger.getLogger(SeasonEditor.class);
	private Tree periods;
	private Label timePeriodLabel;
	private Combo planCycle;
	private Label planCycleLabel;
	private Label yearLabel;
	private Combo selectionLevel;
	private Text seasonId;
	private Label seasonIdLabel;
	private Button isOpen;
	private Button selectionButton;
	private Button selectionChildrenButton;
	private Button selectionDecendenantsButton;
	private Group selectionGroup;
	private SeasonInput seasonInput;
	private boolean isDirty;
	private ExpOpCode memberSelectionType;
	private TreeViewer viewer;
	private SeasonsNode seasonsNode;
	private Map<String, Boolean> selectYears = new TreeMap<String, Boolean>();
	private Map<String, Boolean> planYears = new TreeMap<String, Boolean>();

	// TTN 1595 - Multiyear support
	private Table years;
	// private CheckboxTableViewer yearsCheckboxViewer;
	private TableViewer yearsTableViewer;
	private List<SeasonYear> seasonYearList = new ArrayList<SeasonYear>();

	/**
	 * Constructor.
	 */
	public SeasonEditor() {
		super();
	}

	/**
	 * Implementation of EditorPart.doSave.
	 * 
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if (isDirty) {

			seasonId.setText(seasonId.getText().trim());

			boolean selYrFound = false, planYrFound = false;
			for (SeasonYear sy : seasonYearList) {
				if (sy.getPlanYear() != NONAVAILABLE) {
					selYrFound = true;
					break;
				}
			}
			for (SeasonYear sy : seasonYearList) {
				if (sy.getPlanYear() == PLANNABLE) {
					planYrFound = true;
					break;
				}
			}
			String outMessage = "";
			if (!selYrFound) {
				outMessage = "No years available within the season. At least one year must be available in the Season definition.";
				GUIUtil.openMessageWindow("Validation Error(s)",outMessage, SWT.ICON_ERROR);
				monitor.setCanceled(true);
				return;
			}
			if (!planYrFound) {
				outMessage = "The edited Season does not contain any Plannable Years. The entire Season will be READ ONLY. Please verify that this is valid.";
				if (!GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage)) {// cancel button was clicked
					monitor.setCanceled(true);
					return;
				}
			}

			try {
				String missingField = isInputValid();
				if (missingField != null) {
					GUIUtil.openMessageWindow(getTitle(),
							"The following fields are required:\n-"
									+ missingField, SWT.ICON_INFORMATION);

					monitor.setCanceled(true);
					return;
				}

				if ((seasonInput.isNew() || seasonInput.isCopy())
						&& seasonInput.itemExists(seasonId.getText())) {
					if (!GUIUtil
							.askUserAQuestion(
									getTitle(),
									Constants.MENU_SEASON
											+ ": '"
											+ seasonId.getText()
											+ "' already exists.\nReplace existing item?")) {

						monitor.setCanceled(true);
						return;
					}
				}
				// set the season id
				seasonInput.setSeasonId(seasonId.getText().trim());
				// set the plan cycle
				seasonInput.setPlanCycle(planCycle.getText().trim());
				// set the selected years
				List<String> selYrs = new ArrayList<String>(selectYears.size());
				List<String> planYrs = new ArrayList<String>(planYears.size());
				for( String yr : seasonInput.getYears()) {
					if( selectYears.get(yr) != null && selectYears.get(yr) == true) {
						selYrs.add(yr);
					}
					if( planYears.get(yr) != null && planYears.get(yr) == true ) {
						planYrs.add(yr);
					}
				}
				seasonInput.setSelectedYears(selYrs.toArray(new String[0]));
				// set the plannable years
				seasonInput.setPlannableYears(planYrs.toArray(new String[0]));
				
				// set the open status
				seasonInput.setOpenStatus(isOpen.getSelection());
				// set the select level, if needed
				if (selectionLevel.getEnabled()
						&& selectionLevel.getText() != null
						&& selectionLevel.getText().length() > 0)
					seasonInput.setTimePeriodLevel(new Integer(selectionLevel
							.getText()));
				// set the time period seleciton type
				seasonInput.setMemberSelectionType(memberSelectionType);
				// set the time period memeber
				seasonInput.setTimePeriod(periods.getSelection()[0].getText()
						.trim());

				seasonInput.save();

				isDirty = false;

				firePropertyChange(IEditorPart.PROP_DIRTY);
				setPartName(Constants.MENU_SEASON + ": "
						+ getEditorInput().getName());
				// Refresh the tree nodes.
				updateTreeViewer();
			} catch (Exception e) {
				logger.error(e.getMessage());
				return;
			}
		}
	}

	/**
	 * Sets the dirty flag as fires the propery changes, which signals to the
	 * program that the editor is dirty.
	 */
	public void editorModified() {
		boolean wasDirty = isDirty();
		isDirty = true;
		if (!wasDirty)
			firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	/**
	 * Implementation of EditorPart.doSaveAs.
	 */
	@Override
	public void doSaveAs() {
	}

	/**
	 * Implementation of EditorPart.init.
	 * 
	 * @param site
	 * @param input
	 * @throws org.eclipse.ui.PartInitException
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		isDirty = false;
		setSite(site);
		setInput(input);
		setPartName(Constants.MENU_SEASON + ": " + input.getName());
		seasonInput = (SeasonInput) getEditorInput();

	}

	private boolean loadSeasonYears() {
		boolean modified = false;
		try {
			// parse out the time string seleciton
			seasonInput.parseTimeString();
			// build the year, and time simpletrees.
			seasonInput.buildSimpleTrees();
		} catch (Exception e) {
			logger.debug("No outline found and server is not running");
		}

		// load available years
		if (seasonInput.getSelectedYears() != null) {
			for (String year : seasonInput.getSelectedYears()) {
				selectYears.put(year, true);
			}
		}
		List<String> invalidSelectedYearMember = new ArrayList<String>(), invalidPlanYearMember = new ArrayList<String>();
		// load plannable years
		if (seasonInput.getPlannableYears() != null) {
			for (String year : seasonInput.getPlannableYears()) {
				if (selectYears.get(year) == null) {
					invalidPlanYearMember.add(year);
					modified = true;
				} else {
					planYears.put(year, true);
				}
			}
		}
		if (seasonInput.getYears() != null) {
			// load years for years TableViewer
			for (String year : seasonInput.getYears()) {
				SeasonYear sy = new SeasonYear();
				sy.setYear(year);
				if (selectYears.get(year) == null
						&& planYears.get(year) == null) {
					sy.planYear = NONAVAILABLE;
				} else if (selectYears.get(year) != null
						&& planYears.get(year) == null) {
					sy.planYear = NONPLANNABLE;
				} else if (selectYears.get(year) != null
						&& planYears.get(year) != null) {
					sy.planYear = PLANNABLE;
				}
				seasonYearList.add(sy);
			}
			// validate years
			Set<String> yearMems = new HashSet<String>();
			Set<String> yearSelMems = new HashSet<String>();
			Set<String> yearPlanMems = new HashSet<String>();
			yearMems.addAll(Arrays.asList(seasonInput.getYears()));
			if (seasonInput.getSelectedYears() != null) {
				yearSelMems
						.addAll(Arrays.asList(seasonInput.getSelectedYears()));
			}
			if (seasonInput.getPlannableYears() != null) {
				yearPlanMems.addAll(Arrays.asList(seasonInput
						.getPlannableYears()));
			}
			if (yearSelMems != null && yearSelMems.size() != 0) {
				Iterator<String> it = yearSelMems.iterator();
				while (it.hasNext()) {
					String yrSelMem = it.next();
					if (!yearMems.contains(yrSelMem)) {
						invalidSelectedYearMember.add(yrSelMem);;
						it.remove();
						modified = true;
					}
				}
			}
			if (yearPlanMems != null && yearPlanMems.size() != 0) {
				Iterator<String> it = yearPlanMems.iterator();
				while (it.hasNext()) {
					String yrPlanMem = it.next();
					if (!yearMems.contains(yrPlanMem)) {
						if( !invalidPlanYearMember.contains(yrPlanMem) ) {
							invalidPlanYearMember.add(yrPlanMem);
						}
						it.remove();
						modified = true;
					}
				}
			}

			if (invalidSelectedYearMember.size() != 0
					&& invalidPlanYearMember.size() == 0) {
				GUIUtil.openMessageWindow("Validation Error(s)",
						"The previously selected Year members: "
								+ StringUtils.arrayListToString(invalidSelectedYearMember, ",")
								+ " are no longer valid.", SWT.ICON_ERROR);
			} 
			else if (invalidSelectedYearMember.size() == 0
					&& invalidPlanYearMember.size() != 0) {
				GUIUtil.openMessageWindow("Validation Error(s)",
						"The previously selected Plannable Year members: "
								+ StringUtils.arrayListToString(invalidPlanYearMember, ",")
								+ " are no longer valid.", SWT.ICON_ERROR);
			} 
			else if (invalidSelectedYearMember.size() != 0
					&& invalidPlanYearMember.size() != 0) {
				GUIUtil.openMessageWindow("Validation Error(s)",
						"The previously selected Year members: "
								+ StringUtils.arrayListToString(invalidSelectedYearMember, ",")
								+ " and Plannable Year members: "
								+ StringUtils.arrayListToString(invalidPlanYearMember, ",")
								+ " are no longer valid.", SWT.ICON_ERROR);
			}
		}
		return modified;
	}

	/**
	 * Implementation of EditorPart.isDirty.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isDirty() {
		setPartName(Constants.MENU_SEASON + ": " + seasonId.getText());
		return isDirty;
	}

	/**
	 * Implementation of EditorPart.isSaveAsAllowed.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * Implementation of EditorPart.createPartControl.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		boolean yearModified = loadSeasonYears();
		// build the time dimension tree control.
		Composite container = new Composite(parent, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		seasonIdLabel = new Label(container, SWT.NONE);
		seasonIdLabel.setAlignment(SWT.RIGHT);
		seasonIdLabel.setText("Season Id");

		seasonId = new Text(container, SWT.BORDER);
		seasonId.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		seasonId.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);

		planCycleLabel = new Label(container, SWT.NONE);
		planCycleLabel.setAlignment(SWT.RIGHT);
		planCycleLabel.setText("Plan Cycle");

		planCycle = new Combo(container, SWT.READ_ONLY);
		planCycle.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.CENTER, true,
				false);
		gridData_2.widthHint = 190;
		planCycle.setLayoutData(gridData_2);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		isOpen = new Button(container, SWT.CHECK);
		isOpen.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				editorModified();
			}
		});
		isOpen.setText("Open Status");
		new Label(container, SWT.NONE);

		yearLabel = new Label(container, SWT.NONE);
		yearLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false,
				1, 1));
		EditorControlUtil.setText(yearLabel, seasonInput.getYearDimName());
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		years = new Table(container, SWT.SINGLE | SWT.FULL_SELECTION
				| SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gd_years = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_years.heightHint = 115;
		years.setLinesVisible(true);
		years.setHeaderVisible(true);
		years.setLayoutData(gd_years);
		yearsTableViewer = new TableViewer(years);
		yearsTableViewer.setContentProvider(new SeasonYearContentProvider());
		yearsTableViewer.setLabelProvider(new SeasonYearLabelProvider());
		yearsTableViewer.setColumnProperties(PROPS);
		years.setHeaderVisible(true);
		years.setLinesVisible(true);
		final TableViewerColumn tcYears = new TableViewerColumn(
				yearsTableViewer, SWT.CENTER);
		tcYears.getColumn().setText(YEARS);
		tcYears.getColumn().setWidth(66);
		tcYears.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				SeasonYear p = (SeasonYear) element;
				return p.getYear();
			}

		});
		final TableViewerColumn tcYearAvail = new TableViewerColumn(
				yearsTableViewer, SWT.CENTER);
		tcYearAvail.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				SeasonYear p = (SeasonYear) element;
				return p.getPlanYear();
			}

		});
		tcYearAvail.getColumn().setText(AVAILABILITY);
		tcYearAvail.getColumn().setWidth(150);
		YearEditingSupport yearEditingSupport = new YearEditingSupport(
				yearsTableViewer);
		tcYearAvail.setEditingSupport(yearEditingSupport);
		yearsTableViewer.setInput(seasonYearList);

		new Label(container, SWT.NONE);

		timePeriodLabel = new Label(container, SWT.NONE);
		timePeriodLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false,
				false, 1, 1));
		EditorControlUtil
				.setText(timePeriodLabel, seasonInput.getTimeDimName());
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		periods = new Tree(container, SWT.BORDER);
		periods.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				editorModified();
			}
		});
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true,
				true, 2, 1);
		gridData_1.heightHint = 217;
		gridData_1.widthHint = 218;
		periods.setLayoutData(gridData_1);

		selectionGroup = new Group(container, SWT.NONE);
		selectionGroup.setText("Selection");
		final GridData gridData_4 = new GridData(SWT.RIGHT, SWT.TOP, false,
				true);
		gridData_4.heightHint = 116;
		gridData_4.widthHint = 149;
		selectionGroup.setLayoutData(gridData_4);
		selectionGroup.setLayout(new GridLayout());

		selectionButton = new Button(selectionGroup, SWT.RADIO);
		selectionButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				selectionButtonClicked(SELECTION);
			}
		});
		selectionButton.setText("Selection");

		selectionChildrenButton = new Button(selectionGroup, SWT.RADIO);
		selectionChildrenButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				selectionButtonClicked(SELECTION_CHILDREN);
			}
		});
		selectionChildrenButton.setText("Selection && Children");

		selectionDecendenantsButton = new Button(selectionGroup, SWT.RADIO);
		selectionDecendenantsButton
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(final SelectionEvent e) {
						selectionButtonClicked(SELECTION_DESCENTENTS);
					}
				});
		selectionDecendenantsButton.setText("Selection && Descendants");

		final Composite selectionComposite = new Composite(selectionGroup,
				SWT.NONE);
		final GridData selectionCompositeGridData = new GridData(SWT.LEFT,
				SWT.TOP, true, true);
		selectionCompositeGridData.widthHint = 136;
		selectionCompositeGridData.heightHint = 29;
		selectionComposite.setLayoutData(selectionCompositeGridData);
		final GridLayout selectionCompositeGridLayout = new GridLayout();
		selectionCompositeGridLayout.marginWidth = 0;
		selectionCompositeGridLayout.marginHeight = 0;
		selectionCompositeGridLayout.numColumns = 2;
		selectionComposite.setLayout(selectionCompositeGridLayout);

		final Label selectionLevelLabel = new Label(selectionComposite,
				SWT.NONE);
		selectionLevelLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				false, true));
		selectionLevelLabel.setText("Level");

		selectionLevel = new Combo(selectionComposite, SWT.READ_ONLY);
		selectionLevel.setTextLimit(2);
		selectionLevel.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		selectionLevel.setEnabled(false);
		final GridData gridData_5 = new GridData(SWT.LEFT, SWT.CENTER, true,
				true);
		gridData_5.heightHint = 33;
		gridData_5.widthHint = 43;
		selectionLevel.setLayoutData(gridData_5);

//		EditorUtils.closeEditor(this, SeasonEditor.class);

		populateControlValues();

		isDirty = false;

		if ((seasonInput != null && (seasonInput.isCopy() || seasonInput.isNew() )) || yearModified) {

			editorModified();

		}

	}

	/**
	 * Default control to set focus to when the editor gets focus.
	 */
	@Override
	public void setFocus() {
		if (seasonId != null && !seasonId.isDisposed()) {
			seasonId.setFocus();
		}
	}

	/**
	 * Gets the seasons node.
	 * 
	 * @return A seasons node.
	 */
	public SeasonsNode getSeasonsNode() {
		return seasonsNode;
	}

	/**
	 * Sets the seasons node.
	 * 
	 * @param seasonsNode
	 *            The seasons node.
	 */
	public void setSeasonsNode(SeasonsNode seasonsNode) {
		this.seasonsNode = seasonsNode;
	}

	/**
	 * Gets the tree viewer.
	 * 
	 * @return The tree viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the tree viewer.
	 * 
	 * @param viewer
	 *            The tree viewer.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Automatically generated method: toString
	 * 
	 * @return String
	 */
	public String toString() {
		return super.toString();
	}

	/**
	 * Populates the control with the default/startup values.
	 */
	private void populateControlValues() {
		try {
			// set the plan cycle.
			EditorControlUtil.addItems(planCycle, seasonInput.getPlanCycles());
			// select the version - if it's on the list.
			EditorControlUtil.selectItem(planCycle, seasonInput.getPlanCycle());

			// set the read only season id.
			EditorControlUtil.setText(seasonId, seasonInput.getSeasonId());
			createTreeModel(periods);// , new String[]
										// {seasonInput.getTimeDimName()});

			// set the user selected item.
			if (seasonInput.getTimePeriod() != null
					&& periods.getItemCount() > 0) {
				setTreeItem(periods, seasonInput.getTimePeriod(),
						periods.getItem(0));
				if (periods.getSelectionCount() == 0
						&& seasonInput.getTimePeriod() != null) {
					GUIUtil.openMessageWindow("Validation Error(s)",
							"The previously selected Time member: "
									+ seasonInput.getTimePeriod()
									+ " are no longer valid.", SWT.ICON_ERROR);
				}
			}

			// set the isopen checkbox
			isOpen.setSelection(seasonInput.isOpenStatus());

			// populate the level drop down box.
			String[] levelNumbers = new String[NUMBER_OF_LEVELS + 1];
			for (int i = 0; i < levelNumbers.length; i++) {
				levelNumbers[i] = new Integer(i).toString();
			}
			EditorControlUtil.addItems(selectionLevel, levelNumbers);

			memberSelectionType = seasonInput.getMemberSelectionType();
			// set the selection combo box selection.
			if (memberSelectionType == null) {
				selectionButton.setSelection(true);
			} else if (memberSelectionType.equals(ExpOpCode.CHILDREN)
					|| memberSelectionType.equals(ExpOpCode.ICHILDREN)) {
				selectionChildrenButton.setSelection(true);
			} else if (memberSelectionType.equals(ExpOpCode.IDESC)) {
				selectionDecendenantsButton.setSelection(true);
				// enable the combo
				selectionLevel.setEnabled(true);
				// set the value in the combo box.
				if (seasonInput.getTimePeriodLevel() != null) {
					EditorControlUtil.selectItem(selectionLevel, Integer
							.valueOf(seasonInput.getTimePeriodLevel())
							.toString());
				}
			}
			// if the cache and server are not present, then throw a message and
			// disable the controls.
			if (!seasonInput.isServerRunning()) {
				GUIUtil.openMessageWindow(
						getTitle(),
						"No default server or dimension tree could be found. Or server is not running.",
						SWT.ICON_WARNING);
				setControlStatus(false);
			}

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}
		
	}

	private class SeasonYear {
		private String year;
		private String planYear;

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public String getPlanYear() {
			return planYear;
		}

		public void setPlanYear(String planYear) {
			this.planYear = planYear;
		}

	}

	private final class YearEditingSupport extends EditingSupport {
		private ComboBoxViewerCellEditor cellEditor = null;

		public YearEditingSupport(TableViewer viewer) {
			super(viewer);
			cellEditor = new ComboBoxViewerCellEditor((Composite) getViewer()
					.getControl(), SWT.READ_ONLY);
			cellEditor.setLabelProvider(new LabelProvider());
			cellEditor.setContenProvider(new ArrayContentProvider());
			cellEditor.setInput(YEAR_AVAILABILITY);
			cellEditor.addListener(new ICellEditorListener() {

				@Override
				public void applyEditorValue() {
					editorModified();

				}

				@Override
				public void cancelEditor() {
					isDirty = false;
				}

				@Override
				public void editorValueChanged(boolean oldValidState,
						boolean newValidState) {

				}
			});
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return cellEditor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			if (element instanceof SeasonYear) {
				SeasonYear sy = (SeasonYear) element;
				return sy.getPlanYear();
			}
			return null;
		}

		@Override
		protected void setValue(Object element, Object value) {
			if (element instanceof SeasonYear && value instanceof String) {
				SeasonYear sy = (SeasonYear) element;
				String newPY = (String) value;
				/*
				 * only set new value if it differs from old one
				 */
				if (!sy.getPlanYear().equals(newPY)) {
					if( newPY.equals(PLANNABLE) ) {
						planYears.put(sy.getYear(), true);
						selectYears.put(sy.getYear(), true);
					}
					else if( newPY.equals(NONPLANNABLE) ) {
						planYears.remove(sy.getYear());
						selectYears.put(sy.getYear(), true);
					}
					else if( newPY.equals(NONAVAILABLE) ) {
						planYears.remove(sy.getYear());
						selectYears.remove(sy.getYear());
					}
					sy.setPlanYear(newPY);
					yearsTableViewer.refresh();
				}
			}
		}
	}

	private class SeasonYearContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof List) {

				List<SeasonYear> seasonYearList = (ArrayList<SeasonYear>) inputElement;

				return seasonYearList.toArray();
			}
			return null;
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	private class SeasonYearLabelProvider extends LabelProvider implements ITableLabelProvider {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java
		 * .lang.Object, int)
		 */
		public Image getColumnImage(Object element, int colIndex) {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.
		 * lang.Object, int)
		 */
		public String getColumnText(Object element, int columnNdx) {

			if (element instanceof SeasonYear) {

				SeasonYear sy = (SeasonYear) element;
				switch (columnNdx) {
				case 0:
					return sy.getYear();
				case 1:
					return sy.getPlanYear();
				}
			}
			return null;
		}
	}

	/**
	 * Enables or disables the control states.
	 * 
	 * @param enable
	 *            , to enable; false to disable
	 */
	private void setControlStatus(boolean enable) {
		seasonId.setEnabled(enable);
		planCycle.setEnabled(enable);
		years.setEnabled(enable);
		isOpen.setEnabled(enable);
		periods.setEnabled(enable);
		selectionGroup.setEnabled(enable);
	}

	/**
	 * Sets a variable when a selection button is selected.
	 * 
	 * @param button
	 *            The selection button that was selected.
	 */
	private void selectionButtonClicked(int button) {
		if (button == SELECTION) {
			selectionLevel.setEnabled(false);
			memberSelectionType = null;
		} else if (button == SELECTION_CHILDREN) {
			selectionLevel.setEnabled(false);
			memberSelectionType = ExpOpCode.ICHILDREN;
		} else {
			selectionLevel.setEnabled(true);
			memberSelectionType = ExpOpCode.IDESC;
		}
		editorModified();
	}

	/**
	 * Selects the tree item in a tree.
	 * 
	 * @param tree
	 *            The tree to search.
	 * @param textToSelect
	 *            The text to select.
	 * @param item
	 *            The tree item to search.
	 */
	private void setTreeItem(Tree tree, String textToSelect, TreeItem item) {
		if (item != null) {

			if (item.getText().equalsIgnoreCase(textToSelect)) {
				tree.setSelection(item);
				return;
			} else {
				TreeItem[] items = item.getItems();

				if (items != null && items.length != 0) {
					for (TreeItem treeItem : items) {
						if (treeItem.getText().equalsIgnoreCase(textToSelect)) {
							tree.setSelection(treeItem);
							return;
						} else {
							setTreeItem(tree, textToSelect, treeItem);
						}
					}
				}
			}
		}
	}

	/**
	 * Build the contents of a tree control and populate it.
	 * 
	 * @param tree
	 *            The tree control to build.
	 */
	private void createTreeModel(Tree tree) {
		logger.debug("Create Tree Model start");

		Map<String, Tree> cachedTreeMap = new HashMap<String, Tree>();

		try {

			PafSimpleDimTree pafSimpleTree = seasonInput.getPafSimpleTreeTime();

			logger.debug("Converting TreeInto hash start");

			HashMap treeHashMap = DimensionTreeUtility
					.convertTreeIntoHashMap(pafSimpleTree);

			logger.debug("Converting TreeInto hash end");

			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap
					.get(seasonInput.getTimeDimName());

			logger.info("MAX LEVEL: "
					+ rootMember.getPafSimpleDimMemberProps().getLevelNumber());

			logger.debug("Building Tree start");

			addTreeNode(treeHashMap, tree, null, rootMember,
					seasonInput.getTimeDimName());

			cachedTreeMap.put(seasonInput.getTimeDimName(), tree);

			logger.debug("Building Tree end");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.debug("Create Tree Model end");
	}

	/**
	 * Adds a tree node to a tree.
	 * 
	 * @param treeHashMap
	 *            The hash map containing the tree components.
	 * @param tree
	 *            The tree to add the nodes to.
	 * @param parent
	 *            The parent tree item to have the nodes added to.
	 * @param member
	 *            The paf simple memeber to add to the parent.
	 * @param dimension
	 *            The dimension of this tree.
	 */
	private void addTreeNode(HashMap treeHashMap, Tree tree, TreeItem parent,
			PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent == null) {

			newItem = new TreeItem(tree, SWT.NONE);

		} else {

			newItem = new TreeItem(parent, SWT.NONE);

		}

		newItem.setText(member.getKey());

		if (member.getChildKeys().size() > 0) {

			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap
						.get(child);

				addTreeNode(treeHashMap, tree, newItem, childMember, dimension);

			}
		}
	}

	/**
	 * Validates the control input values.
	 * 
	 * @return The label associated with the control if an invalid value is
	 *         found, or null if the entries are valid.
	 */
	private String isInputValid() {
		if (seasonId.getText() == null
				|| seasonId.getText().trim().length() == 0) {
			return seasonIdLabel.getText();
		}
		if (planCycle == null || planCycle.getText().length() == 0) {
			return planCycleLabel.getText();
		}

		if (periods == null || periods.getSelectionCount() == 0) {
			return timePeriodLabel.getText();
		}

		if (!selectionButton.getSelection()
				&& !selectionChildrenButton.getSelection()
				&& !selectionDecendenantsButton.getSelection()) {
			return selectionGroup.getText();
		}

		return null;
	}

	/**
	 * Updates the tree viewer control whenever a planner role is added or
	 * modified.
	 */
	private void updateTreeViewer() {
		try {
			if (viewer != null && seasonsNode != null) {
				seasonsNode.createChildren(null);
				viewer.refresh(seasonsNode);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}

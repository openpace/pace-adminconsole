/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.editors.input.PlanCycleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;

/**
 * Implementation of EditorPart, used to add or modify Plan Cycles.
 * @author kmoos
 * @version x.xx
 */
public class PlanCycleEditor extends EditorPart {
	/**
	 * Unique id for the editor.
	 */
	public static final String ID = "com.pace.admin.menu.editors.plancycle";
	private static Logger logger = Logger.getLogger(PlanCycleEditor.class);
	private boolean isDirty;
	private TreeViewer viewer;
	private PlanCycleInput planCycleInput;
	private PlanCyclesNode planCyclesNode;
	private Combo planCycleVersion;
	private Label versionLabel;
	private Text planCycleName;
	private Label nameLabel;
	
	/**
	 * Constructor.
	 */
	public PlanCycleEditor() {
		super();
	}

	/**
	 * Implementation of EditorPart.doSave.
	 * @param monitor IProgressMonitor.
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if(isDirty){
			try{
				
				//trim text
				planCycleName.setText(planCycleName.getText().trim());
				
				String missingField = isInputValid();
				if(missingField != null){
					GUIUtil.openMessageWindow(
							this.getTitle(),
							"The following fields are required:\n-" + missingField,
							SWT.ICON_INFORMATION);
					
					monitor.setCanceled(true);
					
					return;
				}
				
				if((planCycleInput.isNew() || planCycleInput.isCopy()) && planCycleInput.itemExists(planCycleName.getText())){
					
					if (! GUIUtil.askUserAQuestion(
			    			this.getTitle(),
			    			Constants.MENU_ROLES_PROCESS_PLANCYCLE + ": '" + planCycleName.getText() + "' already exists.\nReplace existing item?")) {
						
						monitor.setCanceled(true);
						
						return;
												
					}
				}
				
				//set the plan cycle name.
				planCycleInput.setPlanCycleName(planCycleName.getText().trim());
				//set the plan cycle version.
				planCycleInput.setPlanCycleVersion(planCycleVersion.getText().trim());
				//do save.
				planCycleInput.save();
												
				//reset the dirty flag.
				isDirty = false;
				//fire the property change flag
				firePropertyChange(IEditorPart.PROP_DIRTY);
				//update the tab name.
				setPartName(Constants.MENU_ROLES_PROCESS_PLANCYCLE + ": " + this.getEditorInput().getName());
				//update the tree viewer.
				updateTreeViewer();
			} catch(Exception e){
				logger.error(e.getMessage());
				return;
			}
		}
	}

	/**
	 * Implementation of EditorPart.doSaveAs.
	 */
	@Override
	public void doSaveAs() {
		// Not Allowed.
	}
	
	/**
	 * Implementation of EditorPart.init.
	 * @param input
	 * @param site
	 * @throws org.eclipse.ui.PartInitException
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(Constants.MENU_ROLES_PROCESS_PLANCYCLE + ": " + input.getName());
		planCycleInput = (PlanCycleInput) input;
		isDirty = false;
	}

	/**
	 * Implementation of EditorPart.isDirty.
	 * @return boolean
	 */
	@Override
	public boolean isDirty() {
		setPartName(Constants.MENU_ROLES_PROCESS_PLANCYCLE + ": " + planCycleName.getText());
		return isDirty;
	}

	/**
	 * Implementation of EditorPart.isSaveAsAllowed.
	 * @return boolean
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	   }

	/**
	 * Implementation of EditorPart.createPartControl.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(final DisposeEvent e) {
				logger.debug("PlanCycleEditor disposed.");
			}
		});

		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);

		nameLabel = new Label(container, SWT.NONE);
		nameLabel.setLayoutData(new GridData());
		nameLabel.setText("Name");

		planCycleName = new Text(container, SWT.BORDER);
		planCycleName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	
		planCycleName.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});

		versionLabel = new Label(container, SWT.NONE);
		versionLabel.setLayoutData(new GridData());
		EditorControlUtil.setText(versionLabel, planCycleInput.getVersionDimName());
		
		planCycleVersion = new Combo(container, SWT.READ_ONLY);
		planCycleVersion.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		//create the listners.
		planCycleVersion.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		
//		EditorUtils.closeEditor(this, PlanCycleEditor.class);
		populateControlValues();
		
		//TTN-2381
		isDirty = false;
		
		//if copy, flag system to start as dirty
		if ( planCycleInput != null && (planCycleInput.isCopy() || planCycleInput.isNew())) {
			
			editorModified();
			
		}
		
	}
	
	/**
	 * Default control to set focus to when the editor gets focus.
	 */
	@Override
	public void setFocus() {
		if (planCycleName != null && !planCycleName.isDisposed()) {
			planCycleName.setFocus();
		}
	}


	/**
	 * Gets the tree viewer.
	 * @return The tree viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the tree viewer.
	 * @param viewer The tree viewer.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Gets the Plan Cycles Node.
	 * @return A PlanCyclesNode
	 */
	public PlanCyclesNode getPlanCyclesNode() {
		return planCyclesNode;
	}

	/**
	 * Sets the Plan Cycles Node
	 * @param planCyclesNode The PlanCyclesNode.
	 */
	public void setPlanCyclesNode(PlanCyclesNode planCyclesNode) {
		this.planCyclesNode = planCyclesNode;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	/**
	 * Populates the control with the default/startup values.
	 */
	private void populateControlValues(){
		//set the original text.
		EditorControlUtil.setText(planCycleName, planCycleInput.getPlanCycleName());
		//set the list of avail. versions.
		EditorControlUtil.addItems(planCycleVersion, planCycleInput.getPlanCycleVersions());
		//select the version
		EditorControlUtil.selectItem(planCycleVersion, planCycleInput.getPlanCycleVersion());
	}
	
	/**
	 * Validates the control input values.
	 * @return The label associated with the control if an invalid value is found,
	 * or null if the entries are valid. 
	 */
	private String isInputValid(){
		if(planCycleName.getText() == null || 
				planCycleName.getText().trim().length() == 0){
			return nameLabel.getText();
		}
				
		if(planCycleVersion == null || 
				planCycleVersion.getText().length() == 0){
			return versionLabel.getText();
		} 
		
		return null;
	}
	
	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		try{
			if(viewer != null && planCyclesNode != null){
				planCyclesNode.createChildren(null);
				viewer.refresh(planCyclesNode);
			}
			
			if(planCyclesNode.getParent() != null && 
					planCyclesNode.getParent().getChildren() != null){
				for(Object node : planCyclesNode.getParent().getChildren()){
					if(node instanceof RoleConfigurationsNode){
						RoleConfigurationsNode roleConfigsNode = 
							(RoleConfigurationsNode) node;
						roleConfigsNode.createChildren(null);
						viewer.refresh(roleConfigsNode);
						break;
					}
				}
			}
			
		} catch(Exception e){
			logger.error(e.getMessage());
		}
	}
}
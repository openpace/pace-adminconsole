/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.internal;

import java.util.StringTokenizer;

import org.springframework.util.StringUtils;

import com.pace.admin.global.constants.SecurityMemberConsants;
import com.pace.admin.global.enums.LevelGenerationType;
import com.pace.base.PafBaseConstants;

public class SecurityMember {
	
	private String dimensionName;
	
	private String member;
	
	private SelectionType selectionType;
	
	private LevelGenerationType levelGenType;
	
	private int levelGenNumber;
	
	public SecurityMember() {
		
	}
	
	/**
	 * Copy Constructor.
	 * @param copyMe
	 */
	public SecurityMember(SecurityMember copyMe) {
		this.dimensionName = copyMe.getDimensionName();
		this.member = copyMe.getMember();
		this.levelGenNumber = copyMe.getLevelGenNumber();
		this.levelGenType = copyMe.getLevelGenType();
		this.selectionType = copyMe.getSelectionType();
	}
	
	public SecurityMember(String dimensionName, String existingFullSecurityMember) {
						
		if ( dimensionName != null && existingFullSecurityMember != null ) {
			
			this.dimensionName = dimensionName;
			
			if ( existingFullSecurityMember.startsWith("@") 
					&& ! existingFullSecurityMember.equals(PafBaseConstants.UOW_ROOT) 
					&& ! existingFullSecurityMember.equals(PafBaseConstants.PLAN_VERSION) 
					&& ! existingFullSecurityMember.equals(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS)
					&& ! existingFullSecurityMember.contains((PafBaseConstants.MEMBERLIST_TOKEN)) ) {
				
				if ( existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.ICHILD)) {
					
					selectionType = SelectionType.SelectionAndChildren;
					
				} else if ( existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.IDESC)) {
					
					selectionType = SelectionType.SelectionAndDescendants;
					
				} else if ( existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.CHILDREN)) {
					
					selectionType = SelectionType.JustChildren;
					
				} else if ( existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.DESC)) {
					
					selectionType = SelectionType.JustDescendants;
					
				} else if ( existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.LEVEL) || existingFullSecurityMember.startsWith("@" + SecurityMemberConsants.GENERATION ) ) {
					
					selectionType = SelectionType.JustLevelORGeneration;
					
				}
				
				
				int ndxLeftP = existingFullSecurityMember.indexOf('(') + 1;
				int ndxRightP = existingFullSecurityMember.lastIndexOf(')');
				
				String memberWithNum = existingFullSecurityMember.substring(ndxLeftP, ndxRightP);
				
				StringTokenizer strTokenizer = new StringTokenizer(memberWithNum, ",");
				
				member = strTokenizer.nextToken().trim();
				
			
				if ( strTokenizer.hasMoreTokens() ) {
					
					String combinedLevelGenWithNum = strTokenizer.nextToken().trim();
										
					if ( combinedLevelGenWithNum.length() > 1) {
					
						String levelGen = combinedLevelGenWithNum.substring(0, 1);
						
						int numberLevelGenLength = combinedLevelGenWithNum.length();
						
						String levelGenNum = combinedLevelGenWithNum.substring(1, numberLevelGenLength);
						
						if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.LEVEL_IDNT)) {
							
							levelGenType = LevelGenerationType.Level;
							
						} else if ( levelGen.equalsIgnoreCase(SecurityMemberConsants.GENERATION_IDNT)) {
							
							levelGenType = LevelGenerationType.Generation;
						}
						
						if ( levelGenNum.matches("[0123456789]*")) {
							
							levelGenNumber = Integer.parseInt(levelGenNum);
							
						}
										
						
					} else {
						
						levelGenType = LevelGenerationType.Level;
						
						if ( combinedLevelGenWithNum.matches("[0123456789]*")) { 
							 
							levelGenNumber = Integer.parseInt(combinedLevelGenWithNum);
							
						} else {
						
							levelGenNumber = 0;
							
						}						
						
					}					
					
				} else {
					
					//TTN-2490 - Changed to Bottom as it was Level
					levelGenType = LevelGenerationType.Bottom;
					levelGenNumber = 0;
				}
					
				
				
			} else {
				
				member = existingFullSecurityMember.trim();
				selectionType = SelectionType.Selection;
				levelGenType = LevelGenerationType.Level;
				levelGenNumber = 0;
				
				
			}
			
			//System.out.println(this);
		}
		
	}
	
	
	/** This method removes the Alias name of the member and returns the basic security member.
	 * @param existingSecurityMember
	 * @return String securityMember without the Alias
	 */
	public String removeAliasFromMember(String existingSecurityMember, boolean isMemberWithSelection){
		// Check if the security member has an alias along with the member name.
		int numAlias = StringUtils.countOccurrencesOf(existingSecurityMember,")");
		if (numAlias == 2) {
			int ndxLeftP = existingSecurityMember.indexOf('(') + 1;
			int ndxRightP = existingSecurityMember.indexOf(')');
		
			// Remove the alias after getting the alias indexes.
			String memberWithNum = existingSecurityMember.substring(ndxLeftP, ndxRightP);
			String subMember = existingSecurityMember.substring(ndxLeftP, ndxRightP);
			int ndxLeftPointer = memberWithNum.indexOf('(');
			subMember = subMember.substring(ndxLeftPointer, memberWithNum.length());
		
			subMember = subMember.concat(")");
			subMember = " "+subMember;
			String newMember = existingSecurityMember.replace(subMember,"");
			ndxLeftPointer = newMember.indexOf("(");
			if (isMemberWithSelection){
				return newMember;
			}
			else { // remove the selections and just return the member
				newMember = newMember.substring(ndxLeftPointer+1, newMember.length()-1);
				if(StringUtils.countOccurrencesOf(newMember, ",") > 0){
					newMember = newMember.substring(0,newMember.indexOf(','));
				}
				
				return newMember;
			}
		}
		else{ // If no alias exxists in the member, just return back the member as is.
			return existingSecurityMember;
			
		}
	}

	public boolean hasAlias(String existingSecurityMember){
		// Check if alias name exists in the member name.
		int numAlias = StringUtils.countOccurrencesOf(existingSecurityMember,")");
		if(numAlias >1)
			return true;
		else
			return false;
	}
	/**
	 * @return the dimensionName
	 */
	public String getDimensionName() {
		return dimensionName;
	}

	/**
	 * @param dimensionName the dimensionName to set
	 */
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	/**
	 * @return the levelGenNumber
	 */
	public int getLevelGenNumber() {
		return levelGenNumber;
	}

	/**
	 * @param levelGenNumber the levelGenNumber to set
	 */
	public void setLevelGenNumber(int levelGenNumber) {
		this.levelGenNumber = levelGenNumber;
	}

	
	/**
	 * @return the levelGenType
	 */
	public LevelGenerationType getLevelGenType() {
		return levelGenType;
	}

	/**
	 * @param levelGenType the levelGenType to set
	 */
	public void setLevelGenType(LevelGenerationType levelGenType) {
		this.levelGenType = levelGenType;
	}

	/**
	 * @return the member
	 */
	public String getMember() {
		return member;
	}

	/**
	 * @param member the member to set
	 */
	public void setMember(String member) {
		this.member = member;
	}

	/**
	 * @return the selectionType
	 */
	public SelectionType getSelectionType() {
		return selectionType;
	}

	/**
	 * @param selectionType the selectionType to set
	 */
	public void setSelectionType(SelectionType selectionType) {
		this.selectionType = selectionType;
	}
	
	/**
	 * 
	 */
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer();
		
		if ( selectionType!= null 
				&& (selectionType.equals(SelectionType.SelectionAndChildren) 
						|| selectionType.equals(SelectionType.SelectionAndDescendants)
						|| selectionType.equals(SelectionType.JustChildren)
						|| selectionType.equals(SelectionType.JustDescendants)
						|| selectionType.equals(SelectionType.JustLevelORGeneration))) {
		
			strBuff.append("@");
			
			switch (selectionType) {
			
			case JustChildren:
				
				strBuff.append(SecurityMemberConsants.CHILDREN + "(" + member);
				
				break;
				
			case SelectionAndChildren:
				
				strBuff.append(SecurityMemberConsants.ICHILD + "(" + member);
				
				break;
			
			case JustDescendants:
				
				switch (levelGenType){
				
				case Bottom:
					strBuff.append(SecurityMemberConsants.DESC + "(" + member );
					break;
					
				case Level:
					strBuff.append(SecurityMemberConsants.DESC + "(" + member + ", ");
					strBuff.append(SecurityMemberConsants.LEVEL_IDNT + levelGenNumber);
					
					break;
				case Generation:
					strBuff.append(SecurityMemberConsants.DESC + "(" + member + ", ");
					strBuff.append(SecurityMemberConsants.GENERATION_IDNT + levelGenNumber);
					break;
				}			
								
				break;
				
			case SelectionAndDescendants:
				
				switch (levelGenType){
				
				case Bottom:
					strBuff.append(SecurityMemberConsants.IDESC + "(" + member );
					break;
					
				case Level:
					
					strBuff.append(SecurityMemberConsants.IDESC + "(" + member + ", ");
					strBuff.append(SecurityMemberConsants.LEVEL_IDNT + levelGenNumber);
					
					break;
				case Generation:
					strBuff.append(SecurityMemberConsants.IDESC + "(" + member + ", ");
					strBuff.append(SecurityMemberConsants.GENERATION_IDNT + levelGenNumber);
					break;
				}			
								
				break;
			
			case JustLevelORGeneration:
				
				switch (levelGenType){
				
				case Bottom:
					strBuff.append(SecurityMemberConsants.LEVEL + "(" + member );
					break;
					
				case Level:
					
					strBuff.append(SecurityMemberConsants.LEVEL + "(" + member + ", ");
					strBuff.append(levelGenNumber);
					
					break;
				case Generation:
					strBuff.append(SecurityMemberConsants.GENERATION + "(" + member + ", ");
					strBuff.append( levelGenNumber);
					break;
				}			
								
				break;
			default:
				break;
			
			}
			
			
			strBuff.append(")");
			
		} else {
			
			strBuff.append(member);
			
		}
						
		
		return strBuff.toString();
		
	}
	
	public boolean equals(Object obj) {
		SecurityMember theOther = (SecurityMember)obj;
		if( ! this.dimensionName.equals(theOther.dimensionName ))
			return false;
		if( ! this.member.equals(theOther.member) )
				return false;
		return true;
	}
	
	public static void main(String[] args ) {
		
		String[] ar = {"single_member", "@IDESC(totprod, L0)", "@IDESC(totprod, G2)", "@ICHILD(totprod, L4)", "@ICHILD(totprod, g3)"};
		
		//String[] ar = {"single_member", "@IDESC(totprod, 0)", "@IDESC(totprod,2)", "@ICHILD(totprod,4)", "@ICHILD(totprod, g3)"};
		
		for (String s : ar)  {
		
			SecurityMember securityMember = new SecurityMember("Product", s);
		
			System.out.println(securityMember);
		}
		
		
		
		
	}

}

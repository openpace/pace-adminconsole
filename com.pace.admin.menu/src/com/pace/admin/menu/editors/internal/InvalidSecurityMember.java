/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.internal;

import com.pace.admin.menu.dialogs.internal.InvalidSpecificationMember;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class InvalidSecurityMember extends InvalidSpecificationMember{

	private String roleName;
	
	/**
	 * Default Constructor 
	 */
	public InvalidSecurityMember() {
		
	}
	
	/**
	 * @param roleName
	 * @param dimensionName
	 * @param memberName
	 */
	public InvalidSecurityMember(String roleName, String dimensionName, String memberName) {
	
		super(dimensionName, memberName);
		this.roleName = roleName;
		
	}
	
	/**
	 * @return Returns the roleName.
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * @param roleName The roleName to set.
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}

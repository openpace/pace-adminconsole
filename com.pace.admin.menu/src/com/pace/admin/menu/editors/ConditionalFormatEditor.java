/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.composite.PaceTreeModule;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.GenericAddRemoveDialog;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.editors.input.ConditionalFormatEditorInput;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.nodes.ConditionalFormatsNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.GenFormat;
import com.pace.base.format.LevelFormat;
import com.pace.base.format.MemberFormat;
import com.pace.base.format.PafFormat;
import com.pace.base.view.ConditionalFormatDimension;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

public class ConditionalFormatEditor extends EditorPart {

	public static final String ID = "com.pace.admin.menu.editors.ConditionalFormatEditor";
	private static Logger logger = Logger.getLogger(ConditionalFormatEditor.class);
	private static final String LEVEL_NODE = "Level";
	private static final String GENERATION_NODE = "Generation";
	private static final String MEMBERS_NODE = "Members";
	private static final String PLAN_VERSION = PafBaseConstants.PLAN_VERSION;
	private static final String PLAN_YEARS = PafBaseConstants.VIEW_TOKEN_PLAN_YEARS;
	
	private enum FormatType {
		Level, Generation, Member;
	}

	private IProject project;
	private TreeViewer viewer;
	private TreeNode treeNode;
	
	private ConditionalFormatEditorInput condFormatEditorInput;
	private ConditionalFormat conditionalFormat;
	
	private List<String> allDimensions = new ArrayList<String>();
	private List<String> cachedDimensions = new ArrayList<String>();
	private List<String> cachedAttributeDimensions = new ArrayList<String>();
	private List<String> selectedAttributeDimensions = new ArrayList<String>();
	private List<String> newSelectedAttributeDimensions = new ArrayList<String>();
	private Map<String, List<Integer>> dimLevelMap = new TreeMap<String, List<Integer>>();
	private Map<String, List<Integer>> dimGenMap = new TreeMap<String, List<Integer>>();
	private Map<String, PafSimpleDimTree> dimTreeMap = new TreeMap<String, PafSimpleDimTree>(); //A list of dimensions along with their member list
	
	private ConditionalFormatDimension primaryDimension;
	private List<ConditionalFormatDimension> secondaryDimensions = new ArrayList<ConditionalFormatDimension>();
	private Map<Integer, LevelFormat> levelFormats;
	private Map<Integer, GenFormat> genFormats;
	private Map<String, MemberFormat> memberFormats;
	
	private Text txtFormatName;
	private TabFolder dimensionTabFolder;
	private Map<String, Button> dimPrimDimButtonMap = new HashMap<String, Button>();
	private Map<String, Button> dimClearnAllButtonMap = new HashMap<String, Button>();
	private Map<String, Combo> dimCondStyleComboMap = new HashMap<String, Combo>();
	private Map<String, TabItem> dimTabMap = new HashMap<String, TabItem>();
	private Map<String, PaceTreeModule> dimensionControlTreeMap = new HashMap<String, PaceTreeModule>();
	
	private boolean isDirty;
	
	static {
	}

	public ConditionalFormatEditor() {
		super();
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * @return the treeNode
	 */
	public TreeNode getTreeNode() {
		return treeNode;
	}

	/**
	 * @param treeNode
	 *            the treeNode to set
	 */
	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		if( txtFormatName.getText().isEmpty() ) {
	    	GUIUtil.openMessageWindow( "Save Error", "Missing name for the conditional format.");
			monitor.setCanceled(true);
	    	return;
		}
		
    	if( secondaryDimensions == null ) {
    		secondaryDimensions = new ArrayList<ConditionalFormatDimension>();
    	}
    	else 
    		secondaryDimensions.clear();
    	
		
		boolean foundPrimary = false, foundStyleOnPrimary = false, notAllCheckedItemsHaveStylesOnPrimary = false;

		
		for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
		{
			levelFormats = new HashMap<Integer, LevelFormat>();
			genFormats = new HashMap<Integer, GenFormat>();
			memberFormats = new HashMap<String, MemberFormat>();
			
			if( entry.getValue().getSelection() ) {
		    	
		    	foundPrimary = true;
	    	
		    	if( isStyleFound(dimensionControlTreeMap.get(entry.getKey()).getTree().getItem(0)) ) {

		    		foundStyleOnPrimary = true;
		    		
		    	}
		    	
	    		if( notAllCheckedItemsHaveStyles(dimensionControlTreeMap.get(entry.getKey()).getTree().getItem(0))) {
	    			notAllCheckedItemsHaveStylesOnPrimary = true;
	    			break;
	    		}

	    		//iterate through dimension tree items and save it to data cache
    	    	saveDimensionTree(
    	    			dimensionControlTreeMap.get(
	    	    			entry.getKey()).getTree().getItems()[0], 
	    	    			entry.getKey(), 
	    	    			entry.getValue().getSelection());
    	    	
    	    	if( primaryDimension == null ) {
    	    		primaryDimension = new ConditionalFormatDimension();
    	    	}
				primaryDimension.setName(entry.getKey());
    			primaryDimension.setPrimaryDimension(true);
    			
    			if( levelFormats.size() == 0 ) {
    				primaryDimension.setLevelFormats(null);
    			}
    			else {
    				primaryDimension.setLevelFormats(levelFormats);
    			}
    			if( genFormats.size() == 0 ) {
    				primaryDimension.setGenFormats(null);
    			}
    			else {
    				primaryDimension.setGenFormats(genFormats);
    			}
    			if( memberFormats.size() == 0 ) {
    				primaryDimension.setMemberFormats(null);
    			}
    			else {
    				primaryDimension.setMemberFormats(memberFormats);
    			}
    			
		    }
		    else {
    			//iterate through dimension tree items and save it to data cache
    	    	saveDimensionTree(
    	    			dimensionControlTreeMap.get(
    	    					entry.getKey()).getTree().getItems()[0], 
    	    					entry.getKey(), 
    	    					entry.getValue().getSelection());
    	    	if( levelFormats.size() != 0 || genFormats.size() != 0 ||  memberFormats.size() != 0 ) {
	    			ConditionalFormatDimension	secondaryDimension = new ConditionalFormatDimension();
	    			secondaryDimension.setName(entry.getKey());
	       			if( levelFormats.size() == 0 ) {
	       				secondaryDimension.setLevelFormats(null);
	    			}
	    			else {
	    				secondaryDimension.setLevelFormats(levelFormats);
	    			}
	    			if( genFormats.size() == 0 ) {
	    				secondaryDimension.setGenFormats(null);
	    			}
	    			else {
	    				secondaryDimension.setGenFormats(genFormats);
	    			}
	    			if( memberFormats.size() == 0 ) {
	    				secondaryDimension.setMemberFormats(null);
	    			}
	    			else {
	    				secondaryDimension.setMemberFormats(memberFormats);
	    			}
	    			secondaryDimensions.add(secondaryDimension);
    	    	}
		    }

		}
	   	if( ! foundPrimary ) 
    	{
	    	GUIUtil.openMessageWindow( "Save Error", "No primary dimension is found.");
			monitor.setCanceled(true);
	    	return;
    	}
    	
    	if( ! foundStyleOnPrimary ) 
    	{
	    	GUIUtil.openMessageWindow( "Save Error", "At least 1 format is required.");
			monitor.setCanceled(true);
	    	return;
    	}
    	
    	if( notAllCheckedItemsHaveStylesOnPrimary ) 
    	{
	    	GUIUtil.openMessageWindow( "Save Error", "Some members are missing conditional styles.");
			monitor.setCanceled(true);
	    	return;
    	}
    	
		conditionalFormat.setPrimaryDimension(primaryDimension);
		conditionalFormat.setSecondaryDimensions(secondaryDimensions);
		
		conditionalFormat.setName(txtFormatName.getText());
		condFormatEditorInput.setConditionalFormat(conditionalFormat);
	 	
		if( condFormatEditorInput.getModelManager().findDuplicateConditionalFormat(conditionalFormat) !=null ) {
	    	GUIUtil.openMessageWindow( "Save Error", "Duplicate conditional format name exists.");
			monitor.setCanceled(true);
	    	return;
		}
		
		try {
			condFormatEditorInput.save();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		setPartName(Constants.CONDITIONAL_FORMATS + ": " + txtFormatName.getText());
		
		//Refresh the tree nodes.
		updateTreeViewer();
		
		condFormatEditorInput.setNew(false);
		condFormatEditorInput.setCopy(false);
		isDirty = false;
		firePropertyChange(IEditorPart.PROP_DIRTY);
		
	}

	private void saveDimensionTree(TreeItem treeItem, String dimension, boolean isPrimary) {
		
		if (treeItem != null) {
			if(  treeItem.getData() != null ) {
				if( treeItem.getData() instanceof LevelFormat ) {
					LevelFormat levelFormat = (LevelFormat)treeItem.getData();
					levelFormats.put(levelFormat.getLevel(), levelFormat);
				}
				else if( treeItem.getData() instanceof GenFormat ) {
					GenFormat genFormat = (GenFormat)treeItem.getData();
					genFormats.put(genFormat.getGeneration(), genFormat);
				}
				else if( treeItem.getData() instanceof MemberFormat ) {
					MemberFormat memberFormat = (MemberFormat)treeItem.getData();
					memberFormats.put(memberFormat.getMemberName(), memberFormat);
				}
				return;
			}	
			else{
		        TreeItem[] items = treeItem.getItems();
				if (items != null && items.length != 0) {
					for (TreeItem item : items) {
						if( treeItem.getData() != null ) {
							if( treeItem.getData() instanceof LevelFormat ) {
								LevelFormat levelFormat = (LevelFormat)treeItem.getData();
								levelFormats.put(levelFormat.getLevel(), levelFormat);
							}
							else if( treeItem.getData() instanceof GenFormat ) {
								GenFormat genFormat = (GenFormat)treeItem.getData();
								genFormats.put(genFormat.getGeneration(), genFormat);
							}
							else if( treeItem.getData() instanceof MemberFormat ) {
								MemberFormat memberFormat = (MemberFormat)treeItem.getData();
								memberFormats.put(memberFormat.getMemberName(), memberFormat);
							}
							return;
						}	
						else {
							saveDimensionTree(item, dimension, isPrimary);
						}
					}
		        }	
	    	}
	    }
		
	}

	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		if (viewer != null && treeNode != null) {
			if( treeNode instanceof ConditionalFormatsNode ) {
				treeNode.createChildren(null);
				viewer.refresh(treeNode);
			}
			else if( treeNode instanceof ConditionalFormatNode ) {
				if( ! treeNode.getName().equals(conditionalFormat.getName()) ) {
					ConditionalFormatsNode node = (ConditionalFormatsNode)treeNode.getParent();
					node.createChildren(null);
					viewer.refresh(node);
				}
				else {
					treeNode.createChildren(null);
					viewer.refresh(treeNode);
				}
			}
		}
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	@Override
	public void doSaveAs() {

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		condFormatEditorInput = (ConditionalFormatEditorInput) input;
		project = condFormatEditorInput.getProject();
		cachedAttributeDimensions = condFormatEditorInput.getCachedAttributeDimensions();
		cachedDimensions = condFormatEditorInput.getCachedDimensions();

		if( condFormatEditorInput.isNew() ) {
			conditionalFormat = new ConditionalFormat();
			allDimensions.addAll(cachedDimensions);
		}
		else {
			conditionalFormat = condFormatEditorInput.getConditionalFormat();
			if( conditionalFormat != null ) {
				if( condFormatEditorInput.isCopy() ) {
					conditionalFormat = conditionalFormat.clone();
				}
				primaryDimension = conditionalFormat.getPrimaryDimension();
				
				secondaryDimensions = conditionalFormat.getSecondaryDimensions();
				allDimensions.addAll(getConditionalFormatDimensions());
			}
		}
		
		setSite(site);
		setInput(input);

		setPartName(Constants.CONDITIONAL_FORMATS + ": " + condFormatEditorInput.getName());
		isDirty = false;
	}

	private List<String> getConditionalFormatDimensions() {
		java.util.List<String> dimensions = new ArrayList<String>(cachedDimensions);
		String dimension = "";
		if( primaryDimension != null  ) {
			dimension = primaryDimension.getName();
		}
		if( ! dimensions.contains(dimension) ) {
			dimensions.add(dimension);
			selectedAttributeDimensions.add(dimension);
		}
		if( secondaryDimensions != null && secondaryDimensions.size() > 0 ) {
			for( ConditionalFormatDimension condFormDim : secondaryDimensions ) {
				dimension = condFormDim.getName();
				if( ! dimensions.contains(dimension) ) {
					dimensions.add(dimension);
					selectedAttributeDimensions.add(dimension);
				}
			}
		}
		return dimensions;
	}

	private void loadDimTreeMap(List<String> dimensions) throws Exception {
		
		for( String dimName : dimensions ) {
			
			int dimensionMaxLevel = 0;
			int dimensionMaxGeneration = 1;
			
			PafSimpleDimTree pafSimpleTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
					project, PafProjectUtil.getApplicationName(project),dimName);
			
			java.util.List<PafSimpleDimMember> memberObjectsList = pafSimpleTree.getMemberObjects();

			PafApplicationDef pafAppDef = PafApplicationUtil.getPafApp(this.project);
			
			//by default, search for dimension name member.
			String dimensionMemberToSearch = dimName;
			
			//if measure dim and measure root is different than measure dim name, set dimension member to measure root
			if ( dimName.equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureDim()) &&
					pafAppDef.getMdbDef().getMeasureRoot() != null 
					&& ! pafAppDef.getMdbDef().getMeasureDim().equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureRoot())) {
				
				dimensionMemberToSearch = pafAppDef.getMdbDef().getMeasureRoot();
				
			}
			
			// for each member object, find the dimension max level
			for (PafSimpleDimMember member : memberObjectsList) {					
				
				if (member.getKey().equalsIgnoreCase(dimensionMemberToSearch)) {

					dimensionMaxLevel = member.getPafSimpleDimMemberProps()
							.getLevelNumber();
					dimensionMaxGeneration = dimensionMaxLevel + 1;
					
					break;
				}
			}
			List<Integer> levelNumbers = new ArrayList<Integer>();
			List<Integer> genNumbers = new ArrayList<Integer>();
			// loop throught all levels and add level with tag to level numbers
			// array list
			for (int i = 0; i <= dimensionMaxLevel; i++) {

				levelNumbers.add(i);

			}

			for (int i = 1; i <= dimensionMaxGeneration; i++) {

				genNumbers.add(i);

			}
			dimLevelMap.put(dimName, levelNumbers);
			dimGenMap.put(dimName, genNumbers);
			dimTreeMap.put(dimName, pafSimpleTree);
		}
		
	}

	@Override
	public boolean isDirty() {
		setPartName(Constants.CONDITIONAL_FORMATS + ": " + txtFormatName.getText());
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		
		Composite compositeName = new Composite(container, SWT.NONE);
		GridData gd_compositeName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_compositeName.widthHint = 459;
		compositeName.setLayoutData(gd_compositeName);
		compositeName.setLayout(new GridLayout(2, false));
		
		Label lblFormatName = new Label(compositeName, SWT.CENTER);
		lblFormatName.setAlignment(SWT.LEFT);
		GridData gd_lblFormatName = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_lblFormatName.heightHint = 24;
		gd_lblFormatName.widthHint = 98;
		lblFormatName.setLayoutData(gd_lblFormatName);
		lblFormatName.setText("Format Name:");
		
		txtFormatName = new Text(compositeName,SWT.BORDER);
		GridData gd_txtFormatName = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtFormatName.widthHint = 249;
		txtFormatName.setLayoutData(gd_txtFormatName);
		txtFormatName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				setPartName(Constants.CONDITIONAL_FORMATS + ": " + txtFormatName.getText());
				editorModified();
			}
		});
		
		Button btnAddRemoveAttributes = new Button(container, SWT.NONE);
		btnAddRemoveAttributes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				GenericAddRemoveDialog dlg = new GenericAddRemoveDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"Add/Remove Attribute Dimensions",
						"Select the Attribute Dimension(s) that will be used as member filters in this Conditional Format:",
						cachedAttributeDimensions.toArray(new String[0]),
						selectedAttributeDimensions.toArray(new String[0]),
						false);
				
				if( dlg.open() == IDialogConstants.OK_ID ) {
					
					newSelectedAttributeDimensions.clear();
					
					newSelectedAttributeDimensions.addAll(Arrays.asList(dlg.getSelectedItems()));
					
					try {
						loadDimTreeMap(newSelectedAttributeDimensions);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					populateTabFolder(true);
	
					// populate the controls with values
					populateControlValues();
					
					editorModified();
				}
			}			
		});
		btnAddRemoveAttributes.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnAddRemoveAttributes.setText("Add/Remove Attributes");
		
		dimensionTabFolder = new TabFolder(container, SWT.FILL);
		GridData gd_dimensionTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_dimensionTabFolder.widthHint = 420;
		dimensionTabFolder.setLayoutData(gd_dimensionTabFolder);
		
		populateTabFolder(false);

		// populate the controls with values
		populateControlValues();
		
	}
	
	private void populateControlValues() {
		if( condFormatEditorInput.isNew() ) {
			txtFormatName.setText("");
			for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
			{
		    	entry.getValue().setSelection(false);
		    	clearDimensionTree(dimensionControlTreeMap.get(entry.getKey()).getTree().getItem(0), true);
			}	
			isDirty = true;
		}
		else if( condFormatEditorInput.isCopy() ) {
			txtFormatName.setText("Copy of " + condFormatEditorInput.getName());
			isDirty = true;
		}
		else {
			txtFormatName.setText( condFormatEditorInput.getName() );
			isDirty = false;
		}
		
	}

	private void populateTabFolder(boolean reload) {
		if( ! reload ) {
			
			try {
				loadDimTreeMap(allDimensions);
			} catch (Exception e) {
		    	GUIUtil.openMessageWindow( "Error", "Unable to load dimension tree if the Pace Server isn't running and there are no cached dimensions.");
		    	return;
			}
			
			addDimensionTabs(allDimensions);

		} 
		else {
			if(	newSelectedAttributeDimensions.size() != selectedAttributeDimensions.size() 
					|| ! newSelectedAttributeDimensions.contains(selectedAttributeDimensions)
					|| ! selectedAttributeDimensions.contains(newSelectedAttributeDimensions) ) {
				boolean bPrimaryAttribRemoved = false;
				for( TabItem tabItem :  dimensionTabFolder.getItems() ) {
					String tabString = tabItem.getText();
					int index = tabItem.getText().indexOf("(");
					if( index > 1 ) {
						tabString = getMemberNameFromItem(tabItem.getText());
						bPrimaryAttribRemoved = true;
					}
					if( selectedAttributeDimensions.contains(tabString) 
							&& ! newSelectedAttributeDimensions.contains(tabString) ) {
						allDimensions.remove(tabString);
						dimPrimDimButtonMap.remove(tabString);
						tabItem.dispose();
					}
				}
				java.util.List<String> newTabs = new ArrayList<String>();
				for( String newTab : newSelectedAttributeDimensions) {
					if( ! selectedAttributeDimensions.contains(newTab) ) {
						newTabs.add(newTab);
					}
				}
				
				addDimensionTabs(newTabs);
				
				if( bPrimaryAttribRemoved ) {
					for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
					{
				    	entry.getValue().setEnabled(true);
					}	
				}
				allDimensions.clear();
				allDimensions.addAll(cachedDimensions);
				allDimensions.addAll(newSelectedAttributeDimensions);
				selectedAttributeDimensions.clear();
				selectedAttributeDimensions.addAll(newSelectedAttributeDimensions);
			}
		}

	}
	
	private void addDimensionTabs(java.util.List<String> dimensions) {
					
		// if hier dimensions exist and the tab folder is not null and not
		// disposed
		if (dimensions != null && dimensionTabFolder != null
				&& !dimensionTabFolder.isDisposed()) {

			// loop through each dimension and create a tab item. For each tab
			// item, create all the
			// controls
			for (String dimName : dimensions) {

				// create new tab item
				final TabItem tabItem = new TabItem(dimensionTabFolder,	SWT.NONE);

				// set text to hier dimension name
				tabItem.setText(dimName);

				PafSimpleDimTree pafSimpleTree = null;
				try {
					pafSimpleTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
							project, PafProjectUtil.getApplicationName(project),dimName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				if( pafSimpleTree == null ) {
					String outMessage = "Couldn't get cached simple tree. Login into server and cache dimensions";
					GUIUtil.openMessageWindow(Constants.DIALOG_WARNING_HEADING, outMessage, SWT.ICON_WARNING);
				}
				

				// creat a new deposit
				Composite tabComposite = getDimensionTabFolderComposite(dimensionTabFolder, tabItem);

				// set control to new composite
				tabItem.setControl(tabComposite);

				// get tree from cached map
				Tree treeControl = dimensionControlTreeMap.get(dimName).getTree();
				
				TreeItem rootItem = new TreeItem(treeControl,SWT.READ_ONLY);
				rootItem.setText(dimName);
				rootItem.setGrayed(true);
				// create swt tree model from paf tree
				createTreeModel(treeControl, rootItem, dimName);

				updateFormControls(tabItem);
				
				dimTabMap.put(dimName, tabItem);
				
				rootItem.setExpanded(true);
				
			}
		}

	}
	
	private void createTreeModel(Tree tree, TreeItem rootItem, String dimension) {

		logger.debug("Create Tree Model start: " + dimension);

		try {

			addLevelTree(tree, rootItem, dimension);
			
			addGenerationTree(tree, rootItem, dimension);
			
			PafSimpleDimTree pafSimpleTree = dimTreeMap.get(dimension);
			HashMap<String, PafSimpleDimMember> treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(pafSimpleTree);
			PafSimpleDimMember rootMember = (PafSimpleDimMember) treeHashMap.get(dimension);
			addDimensionTree(treeHashMap, tree, rootItem, rootMember, dimension);
			
			logger.debug("Building Tree " + dimension + " end");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Create Tree Model end");
	}

	private void addLevelTree(Tree dimensionTree, TreeItem rootItem, String dimension) {

		// if dimension exist in the generation format
		if (dimLevelMap.containsKey(dimension)) {

			// get the level numbers. Number of levels per dimension
			List<Integer> levelMembers = dimLevelMap.get(dimension);

			// create the tree root item
			TreeItem levelItem = new TreeItem(rootItem, SWT.READ_ONLY);
			// set the root's name
			levelItem.setText(LEVEL_NODE);
			levelItem.setGrayed(true);

			// loop through all lever numbers to populate the root item with
			// levels
			for (Integer levelMember : levelMembers) {

				// create level tree item off of root item
				TreeItem newItem = new TreeItem(levelItem, SWT.NONE);
				// set the new item to the level number (for example L1 or G2)
				// is a global style defined for this dimension and level
				LevelFormat format = (LevelFormat) findConditionalFormatForMember(dimension, FormatType.Level, levelMember.toString());
				if( format != null ) {
					newItem.setData(format);
					if( format.getFormatName() != null )
						newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT + levelMember.toString() + " (" + format.getFormatName() + ")");
					else
						newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT + levelMember.toString());
					newItem.setChecked(true);
				}
				else {
					newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT + levelMember.toString());
					newItem.setChecked(false);
				}
			}

//			levelItem.setExpanded(true);
		}

	}
	
	private void addGenerationTree(Tree dimensionTree, TreeItem rootItem, String dimension) {

		// if dimension exist in the generation format
		if (dimGenMap.containsKey(dimension)) {

			// get the level numbers. Number of levels per dimension
			List<Integer> genMembers = dimGenMap.get(dimension);

			// create the tree root item
			TreeItem genItem = new TreeItem(rootItem, SWT.READ_ONLY);
			// set the root's name
			genItem.setText(GENERATION_NODE);
			genItem.setGrayed(true);

			// loop through all lever numbers to populate the root item with
			// levels
			for (Integer genMember : genMembers) {

				// create level tree item off of root item
				TreeItem newItem = new TreeItem(genItem, SWT.NONE);
				// set the new item to the level number (for example L1 or G2)
				// is a global style defined for this dimension and level
				GenFormat format = (GenFormat) findConditionalFormatForMember(dimension, FormatType.Generation, genMember.toString());
				if( format != null ) {
					newItem.setData(format);
					if( format.getFormatName() != null )
						newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT + genMember.toString() + " (" + format.getFormatName() + ")");
					else
						newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT + genMember.toString());
					newItem.setChecked(true);
				}
				else {
					newItem.setText(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT + genMember.toString());
					newItem.setChecked(false);
				}
			}

//			genItem.setExpanded(true);
		}

	}
	
	private void addDimensionTree(HashMap<String, PafSimpleDimMember> treeHashMap, Tree tree, TreeItem parent,	PafSimpleDimMember member, String dimension) {

		TreeItem newItem = null;

		if (parent.getText().equals(dimension)) {

			newItem = new TreeItem(parent, SWT.READ_ONLY|SWT.Deactivate);
			newItem.setText(MEMBERS_NODE);
			newItem.setGrayed(true);
			
			if( DimensionUtil.getVersionDimensionName(project).equals(dimension)) {
				TreeItem planVersion = new TreeItem(newItem, SWT.NONE);
				planVersion.setText(PLAN_VERSION);
				MemberFormat format = (MemberFormat) findConditionalFormatForMember(dimension, FormatType.Member, PLAN_VERSION);
				if( format != null ) {
					planVersion.setData(format);
					if( format.getFormatName() != null )
						planVersion.setText(PLAN_VERSION + " (" + format.getFormatName() + ")");
					else
						planVersion.setText(PLAN_VERSION);
					planVersion.setChecked(true);
				}
				else {
					planVersion.setText(PLAN_VERSION);
					planVersion.setChecked(false);
				}
			}
			else if( DimensionUtil.getYearDimensionName(project).equals(dimension)) {
				TreeItem planYears = new TreeItem(newItem, SWT.NONE);
				planYears.setText(PLAN_YEARS);
				MemberFormat format = (MemberFormat) findConditionalFormatForMember(dimension, FormatType.Member, PLAN_YEARS);
				if( format != null ) {
					planYears.setData(format);
					if( format.getFormatName() != null )
						planYears.setText(PLAN_YEARS + " (" + format.getFormatName() + ")");
					else
						planYears.setText(PLAN_YEARS);
					planYears.setChecked(true);
				}
				else {
					planYears.setText(PLAN_YEARS);
					planYears.setChecked(false);
				}
			}

		} else {

			newItem = new TreeItem(parent, SWT.NONE);
			
			MemberFormat format = (MemberFormat) findConditionalFormatForMember(dimension, FormatType.Member, member.getKey());
			if( format != null ) {
				newItem.setData(format);
				if( format.getFormatName() != null )
					newItem.setText(member.getKey() + " (" + format.getFormatName() + ")");
				else
					newItem.setText(member.getKey());
				newItem.setChecked(true);
			}
			else {
				newItem.setText(member.getKey());
				newItem.setChecked(false);
			}
		}
		
		if (member.getChildKeys().size() > 0 ) {
	
			String[] children = member.getChildKeys().toArray(new String[0]);

			for (String child : children) {

				PafSimpleDimMember childMember = (PafSimpleDimMember) treeHashMap
						.get(child);

				addDimensionTree(treeHashMap, tree, newItem, childMember, dimension);

			}
		}

//		newItem.setExpanded(true);
		
	}

	private PafFormat findConditionalFormatForMember(String dimensionName,	FormatType formatType, String member) {

		if( primaryDimension != null && primaryDimension.getName().equals(dimensionName)) {

			if (formatType.equals(FormatType.Level)) {
	
				return primaryDimension.getLevelFormat(Integer.parseInt(member));
	
			} else if (formatType.equals(FormatType.Generation)) {
	
				return primaryDimension.getGenFormat(Integer.parseInt(member));
	
			} else if (formatType.equals(FormatType.Member)) {
	
				return primaryDimension.getMemberFormat(member);
	
			}
			
		}
		else {
			if( secondaryDimensions != null && secondaryDimensions.size() > 0 ) {
				
				for( ConditionalFormatDimension  secondaryDimension : secondaryDimensions ) {
					
					if( secondaryDimension != null && secondaryDimension.getName().equals(dimensionName)) {
						
						if (formatType.equals(FormatType.Level) ) {
							
							return secondaryDimension.getLevelFormat(Integer.parseInt(member));
			
						} else if (formatType.equals(FormatType.Generation)) {
				
							return secondaryDimension.getGenFormat(Integer.parseInt(member));
				
						} else if (formatType.equals(FormatType.Member)) {
				
							return secondaryDimension.getMemberFormat(member);
				
						}
					}
				}
				
			}
		}
		return null;
	}
	
	private Composite getDimensionTabFolderComposite(TabFolder tabFolder, final TabItem tabItem) {

		final Composite dimTabComposite = new Composite(tabFolder, SWT.NONE);
		dimTabComposite.setLayout(new GridLayout(2, false));
		dimTabComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		Group grpFormatDim = new Group(dimTabComposite, SWT.NONE);
		grpFormatDim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpFormatDim.setLayout(new GridLayout(1, true));
		grpFormatDim.setText("Select a Style for Dimension: " + tabItem.getText());
		
		final PaceTreeModule dimTree = new PaceTreeModule(grpFormatDim, 0, SWT.CHECK| SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		dimTree.setLayoutData(gridData_1);
		
		dimensionControlTreeMap.put(tabItem.getText(), dimTree);		

		Group grpSelection = new Group(dimTabComposite, SWT.NONE);
		grpSelection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpSelection.setLayout(new GridLayout(1, true));
		
		Composite compSelection = new Composite(grpSelection, SWT.NONE);
		compSelection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		compSelection.setLayout(new GridLayout(1,true));

		final Button btnPrimaryDimension = new Button(compSelection, SWT.CHECK);
		btnPrimaryDimension.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		btnPrimaryDimension.setText("Primary Dimension");

		dimPrimDimButtonMap.put(tabItem.getText(), btnPrimaryDimension);
		
		final Label lblCondStyle = new Label(compSelection, SWT.READ_ONLY);
		lblCondStyle.setText("Conditional Style");
		
		final Combo cmbConditionalStyle = new Combo(compSelection, SWT.READ_ONLY);
		cmbConditionalStyle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if( condFormatEditorInput.getCondStyles() != null && condFormatEditorInput.getCondStyles().length > 0 ) {
			cmbConditionalStyle.setItems(condFormatEditorInput.getCondStyles());
			cmbConditionalStyle.select(0);
		}
		
		dimCondStyleComboMap.put(tabItem.getText(), cmbConditionalStyle);
		
		cmbConditionalStyle.setEnabled(false);
		
		btnPrimaryDimension.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				// get selection
				boolean isSelected = btnPrimaryDimension.getSelection();
				if( isSelected ) { //check primary dimension
					//disable primary buttons on other tabs
					for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
					{
					    if( ! entry.getKey().equals(tabItem.getText()) )
					    {
					    	entry.getValue().setEnabled(false);
					    }
					}	
					if( dimTree.getTree().getSelection().length > 0 ) {
						TreeItem item = dimTree.getTree().getSelection()[0];
						if( item.getChecked() ) {
	                		cmbConditionalStyle.setEnabled(true);
 
	                		item.setData(parseFormatFromItem(item.getText(), cmbConditionalStyle.getText()));
                			item.setText(item.getText() + " (" + cmbConditionalStyle.getText() + ")");
							
						}
					}
					tabItem.setText(getMemberNameFromItem(tabItem.getText()) + " (P)");
					
				}
				else { //uncheck primary dimension
					if( isItemFoundChecked(dimTree.getTree().getItems()[0]) ) {
						if(	GUIUtil.askUserAQuestion(
			    			"Conditional Format Editor",
							"Removing the primary specification from this dimension will reset all conditional style selections.  Are you sure?")) 
						{
					    	//strip tab name
							tabItem.setText(getMemberNameFromItem(tabItem.getText()));
							//enable primary buttons on other tabs
							for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
							{
							    if( ! entry.getKey().equals(getMemberNameFromItem(tabItem.getText()) ) )
							    {
							    	entry.getValue().setEnabled(true);
							    }
							}	
					    	clearDimensionTree(dimTree.getTree().getItem(0), false);
						}
						else {
							btnPrimaryDimension.setSelection(true);
						}
			    	}
					else {
				    	//strip tab name
						tabItem.setText(getMemberNameFromItem(tabItem.getText()));
						//enable primary buttons on other tabs
						for ( Map.Entry<String, Button> entry : dimPrimDimButtonMap.entrySet() )
						{
						    if( ! entry.getKey().equals(getMemberNameFromItem(tabItem.getText()) ) )
						    {
						    	entry.getValue().setEnabled(true);
						    }
						}	
					}
				}
				
				editorModified();
			}

		});

		final Label lable_1 = new Label(compSelection, SWT.NONE);
		GridData gd_usernameLabel = new GridData(84, 22);
		gd_usernameLabel.verticalAlignment = SWT.FILL;
		gd_usernameLabel.horizontalAlignment = SWT.FILL;
		lable_1.setLayoutData(gd_usernameLabel);

		final Button btnClearAll = new Button(compSelection, SWT.PUSH);
		btnClearAll.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		btnClearAll.setText("Clear All");
		btnClearAll.setEnabled(false);
		
		dimClearnAllButtonMap.put(tabItem.getText(), btnClearAll);
		
		btnClearAll.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearDimensionTree(dimTree.getTree().getItem(0), true);
                editorModified();
			}
		});
		
		cmbConditionalStyle.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				if( dimTree.getTree().getSelection()[0].getChecked() ) {
					dimTree.getTree().getSelection()[0].setData(
							parseFormatFromItem(getMemberNameFromItem(dimTree.getTree().getSelection()[0].getText()), cmbConditionalStyle.getText()));
					dimTree.getTree().getSelection()[0].setText(
							getMemberNameFromItem(dimTree.getTree().getSelection()[0].getText()) + " (" + cmbConditionalStyle.getText() + ")");
					editorModified();
				}
			}

		});
		
		dimTree.getTree().addListener(SWT.Selection, new Listener() {
	        public void handleEvent(Event event) {
                TreeItem item = (TreeItem) event.item;
	            if (event.detail == SWT.CHECK) {
	                boolean checked = item.getChecked();
	                if( item.getText().equals(getMemberNameFromItem(tabItem.getText())) || item.getText().equals(LEVEL_NODE) 
	                		|| item.getText().equals(GENERATION_NODE) || item.getText().equals(MEMBERS_NODE) ) {
	                	item.setChecked(false);
    					cmbConditionalStyle.setEnabled(false);
	                }
	                else {
	                	if( checked ) {
	                		dimTree.getTree().setSelection(item);
	                		//list item includes style name
	                		if( btnPrimaryDimension.getSelection() ) 
	                		{
		                		if( item.getText().indexOf(cmbConditionalStyle.getText()) == -1 ) { //if no style is set
	
		                			item.setData(parseFormatFromItem(item.getText(), cmbConditionalStyle.getText()));
		                			item.setText(item.getText() + " (" + cmbConditionalStyle.getText() + ")");
			                		
		                		}
		    					cmbConditionalStyle.setEnabled(true);
	                		}
	                		else {
	                			item.setData(parseFormatFromItem(item.getText(), null));
	                		}
    						btnClearAll.setEnabled(true);
	                	}
	                	else {
	                		item.setText(getMemberNameFromItem(item.getText()));
	                		item.setData(null);
	    					cmbConditionalStyle.setEnabled(false);
	    					if( ! isItemFoundChecked(dimTree.getTree().getItems()[0]) ) {
	    						btnClearAll.setEnabled(false);
	    					}
	    					else {
	    						btnClearAll.setEnabled(true);
	    					}
	    					
	                	}
	                }
	                
	                editorModified();
 	            }
	            else {//only regular selection, not check selection
	                boolean checked = item.getChecked();
	                if( item.getText().equals(getMemberNameFromItem(tabItem.getText())) || item.getText().equals(LEVEL_NODE) 
	                		|| item.getText().equals(GENERATION_NODE) || item.getText().equals(MEMBERS_NODE) ) {
    					cmbConditionalStyle.setEnabled(false);
	                }
	                else {
	                	if( checked && btnPrimaryDimension.getSelection() )  {
	                		cmbConditionalStyle.setEnabled(true);
	                		if( findStyleFromItem(item.getText()) ) {
		                		if( item.getText().indexOf(cmbConditionalStyle.getText()) != -1 ) { //if style is set
			    					cmbConditionalStyle.setText(getStyleNameFromTreeItem(item.getText())); //update combo box
		                		}
		                		else {//if a different style, change to the selected style
		                			item.setData(parseFormatFromItem(parseIdentFromString(item.getText()), cmbConditionalStyle.getText()));
		                			item.setText(parseIdentFromString(item.getText()) + " (" + cmbConditionalStyle.getText() + ")");
			    	                editorModified();
		                		}
	                		}
	                		else {//if no style is set
	                			item.setData(parseFormatFromItem(item.getText(), cmbConditionalStyle.getText()));
	                			item.setText(item.getText() + " (" + cmbConditionalStyle.getText() + ")");
		    	                editorModified();
	                		}
	                	}
	                	else {
	       					cmbConditionalStyle.setEnabled(false);
	                	}
	                }
	            }
	        }
	    });

		return dimTabComposite;
	}
		
	private PafFormat parseFormatFromItem(String item, String style) {
		String ident = parseIdentFromString(item);
		int levelGenNum = 0;
		boolean isMemberNode = false;
		try {
			levelGenNum = parseIntFromString(item);
		}
		catch(NumberFormatException nfe) {
			isMemberNode = true;
		}
		if (ident.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT) && ! isMemberNode) {
			LevelFormat levelFormat = new LevelFormat(levelGenNum, style);
			return levelFormat;
		} else if (ident.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT) && ! isMemberNode) {
			GenFormat genFormat = new GenFormat(levelGenNum, style);
			return genFormat;
		}
		else {
			MemberFormat memberFormat = new MemberFormat(item, style );
			return memberFormat;
		}
	}

	/**
	 * 
	 * @param strLevel
	 * @return
	 */
	private int parseIntFromString(String strLevel) throws NumberFormatException {

		return Integer.parseInt(strLevel.substring(1, strLevel.length()));

	}
	
	private String parseIdentFromString(String strMember) {

		return strMember.substring(0, 1);

	}

	protected String getMemberNameFromItem(String item) {
		int index = item.indexOf("(");
		if( index > 1 ) {
			return  item.substring(0, index-1);
		}
		else 
			return item;
	}

	protected boolean findStyleFromItem(String item) {
		int index = item.indexOf("(");
		if( index > 1 ) {
			return  true;
		}
		else 
			return false;
	}
	
	protected String getStyleNameFromTreeItem(String treeItem) {
		int index1 = treeItem.indexOf("(");
		int index2= treeItem.indexOf(")");
		if( index1 > 1 && index2 > 1 ) {
	 		String member = treeItem.substring(index1+1,index2);
			return member;
		}
		return null;
	}

	private boolean isItemFoundChecked(TreeItem treeItem) {
		TreeItem[] items = treeItem.getItems();
		for (TreeItem item : items) {
			boolean checked = item.getChecked();
			if( checked ) {
				 return true;
			}
			if( isItemFoundChecked(item) == true )
				return true;
		}
		return false;
	}

	private boolean isStyleFound(TreeItem treeItem) {
		TreeItem[] items = treeItem.getItems();
		for (TreeItem item : items) {
			boolean checked = item.getChecked();
			if( checked && findStyleFromItem(item.getText())) {
				 return true;
			}
			if( isStyleFound(item) == true )
				return true;
		}
		return false;
	}

	private boolean notAllCheckedItemsHaveStyles(TreeItem treeItem) {
		TreeItem[] items = treeItem.getItems();
		for (TreeItem item : items) {
			boolean checked = item.getChecked();
			if( checked && ! findStyleFromItem(item.getText())) {
				 return true;
			}
			if( notAllCheckedItemsHaveStyles(item) == true )
				return true;
		}
		return false;
	}

	private void clearDimensionTree(TreeItem treeItem, boolean removeAll ) {
		if (treeItem != null) {
			if( treeItem.getChecked() ) {
				treeItem.setText(getMemberNameFromItem(treeItem.getText()));
				if( removeAll ) {
					treeItem.setChecked(false);
					treeItem.setData(null);
				}
				else {
					if( treeItem.getData() instanceof LevelFormat ) {
						LevelFormat levelFormat = (LevelFormat)treeItem.getData();
						levelFormat.setFormatName(null);
						treeItem.setData(levelFormat);
					}
					else if( treeItem.getData() instanceof GenFormat ) {
						GenFormat genFormat = (GenFormat)treeItem.getData();
						genFormat.setFormatName(null);
						treeItem.setData(genFormat);
					}
					else if( treeItem.getData() instanceof MemberFormat ) {
						MemberFormat memberFormat = (MemberFormat)treeItem.getData();
						memberFormat.setFormatName(null);
						treeItem.setData(memberFormat);
					}
				}
 				return;
			}
		    else {
		        TreeItem[] items = treeItem.getItems();
				if (items != null && items.length != 0) {
					for (TreeItem item : items) {
						if( treeItem.getChecked() ) {
								treeItem.setText(getMemberNameFromItem(treeItem.getText()));
								if( removeAll ) {
									treeItem.setChecked(false);
									treeItem.setData(null);
								}
								else {
									if( treeItem.getData() instanceof LevelFormat ) {
										LevelFormat levelFormat = (LevelFormat)treeItem.getData();
										levelFormat.setFormatName(null);
										treeItem.setData(levelFormat);
									}
									else if( treeItem.getData() instanceof GenFormat ) {
										GenFormat genFormat = (GenFormat)treeItem.getData();
										genFormat.setFormatName(null);
										treeItem.setData(genFormat);
									}
									else if( treeItem.getData() instanceof MemberFormat ) {
										MemberFormat memberFormat = (MemberFormat)treeItem.getData();
										memberFormat.setFormatName(null);
										treeItem.setData(memberFormat);
									}
								}
				  				return;
						}
						else {
							clearDimensionTree(item, removeAll);
						}
		            }
		        }	
	    	}
	    }
	}
	
	public void updateFormControls(final TabItem tabItem) {
		if( dimensionControlTreeMap.get(tabItem.getText()) != null 
				&& dimensionControlTreeMap.get(tabItem.getText()).getTree().getItems() != null
				&& dimensionControlTreeMap.get(tabItem.getText()).getTree().getItems().length > 0 ) {
			if( ! isItemFoundChecked(dimensionControlTreeMap.get(tabItem.getText()).getTree().getItems()[0]) ) {
				dimClearnAllButtonMap.get(tabItem.getText()).setEnabled(false);
			}
			else {
				dimClearnAllButtonMap.get(tabItem.getText()).setEnabled(true);
			}
		}

		if( conditionalFormat != null && conditionalFormat.getPrimaryDimension() != null 
				&& conditionalFormat.getPrimaryDimension().getName() != null ) {
			if( conditionalFormat.getPrimaryDimension().getName().equals(tabItem.getText())) {
				
				dimPrimDimButtonMap.get(tabItem.getText()).setSelection(true);
				dimensionTabFolder.setSelection(tabItem);
				
				tabItem.setText(getMemberNameFromItem(tabItem.getText()) + " (P)");
				
			}
			else {
				
				dimPrimDimButtonMap.get(tabItem.getText()).setEnabled(false);
				tabItem.setText(getMemberNameFromItem(tabItem.getText()));
				
			}
			
		}
		
	}
	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void setFocus() {
		txtFormatName.setFocus();
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.menu.dialogs.ViewDialog;
import com.pace.admin.menu.dialogs.ViewSectionDialog;
import com.pace.admin.menu.editors.AdminLockEditor;
import com.pace.admin.menu.editors.ConditionalFormatEditor;
import com.pace.admin.menu.editors.ConditionalStyleEditor;
import com.pace.admin.menu.editors.PlanCycleEditor;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.ProjectSettingsEditor;
import com.pace.admin.menu.editors.RoleConfigurationEditor;
import com.pace.admin.menu.editors.SeasonEditor;
import com.pace.admin.menu.editors.UserSecurityEditor;
import com.pace.admin.menu.editors.ViewGroupEditor;

/**
 * Common functions used with editors.
* @author kmoos
* @version x.xx
 */
public class EditorUtils {
	private static Logger logger = Logger.getLogger(EditorUtils.class);
	
	private final static String VIEW_DIALOG = ViewDialog.class.getName(); // "com.pace.admin.menu.dialogs.ViewDialog";
	private final static String VIEW_SECTION_DIALOG = ViewSectionDialog.class.getName(); // "com.pace.admin.menu.dialogs.ViewSectionDialog";
	
	private final static String ADMIN_LOCK_EDITOR = AdminLockEditor.class.getName(); // AdminLockEditor.class.getTypeName(); //    "com.pace.admin.menu.editors.AdminLockEditor";
	private final static String CONDITIONAL_FORMAT_EDITOR = ConditionalFormatEditor.class.getName(); // "com.pace.admin.menu.editors.ConditionalFormatEditor";
	private final static String CONDITIONAL_STYLE_EDITOR = ConditionalStyleEditor.class.getName(); // "com.pace.admin.menu.editors.ConditionalStyleEditor";
	private final static String PLAN_CYCLE_EDITOR = PlanCycleEditor.class.getName(); // "com.pace.admin.menu.editors.plancycle";
	private final static String PLANNER_ROLE_EDITOR = PlannerRoleEditor.class.getName(); // "com.pace.admin.menu.editors.plannerrole";
	private final static String PRINT_STYLE_EDITOR = PrintStyleEditor.class.getName(); // "com.pace.admin.menu.editors.PrintStyleEditor";
	private final static String PROJECT_SETTINGS_EDITOR = ProjectSettingsEditor.class.getName(); // "com.pace.admin.menu.editors.ProjectSettingsEditor";
	private final static String ROLE_CONFIGURATION_EDITOR = RoleConfigurationEditor.class.getName(); // "com.pace.admin.menu.editors.roleconfiguration";
	private final static String SEASON_EDITOR = SeasonEditor.class.getName(); // "com.pace.admin.menu.editors.SeasonEditor";
	private final static String USER_SECURITY_EDITOR = UserSecurityEditor.class.getName(); // "com.pace.admin.menu.editors.UserSecurityEditor";
	private final static String VIEW_GROUP_EDITOR = ViewGroupEditor.class.getName(); // "com.pace.admin.menu.editors.ViewGroupEditor";
	
	private final static String[] ADMIN_LOCK_EDITOR_DEPENDENCIES = {ROLE_CONFIGURATION_EDITOR, }; 
	private final static String[] CONDITIONAL_FORMAT_EDITOR_DEPENDENCIES = {CONDITIONAL_STYLE_EDITOR}; 
	private final static String[] CONDITIONAL_STYLE_EDITOR_DEPENDENCIES = {}; 
	private final static String[] PLAN_CYCLE_EDITOR_DEPENDENCIES = {ROLE_CONFIGURATION_EDITOR, SEASON_EDITOR }; 
	private final static String[] PLANNER_ROLE_EDITOR_DEPENDENCIES = {ROLE_CONFIGURATION_EDITOR, SEASON_EDITOR }; 
	private final static String[] PRINT_STYLE_EDITOR_DEPENDENCIES = {}; 
	private final static String[] PROJECT_SETTINGS_EDITOR_DEPENDENCIES = {}; 
	private final static String[] ROLE_CONFIGURATION_EDITOR_DEPENDENCIES = {PLAN_CYCLE_EDITOR, PLANNER_ROLE_EDITOR, VIEW_GROUP_EDITOR}; 
	private final static String[] SEASON_EDITOR_DEPENDENCIES = {PLANNER_ROLE_EDITOR, PLAN_CYCLE_EDITOR}; 
	private final static String[] USER_SECURITY_EDITOR_DEPENDENCIES = {PLANNER_ROLE_EDITOR}; 
	private final static String[] VIEW_GROUP_EDITOR_DEPENDENCIES = {ROLE_CONFIGURATION_EDITOR}; 
	
	private final static String[] VIEW_DIALOG_DEPENDENCIES = {ROLE_CONFIGURATION_EDITOR, PRINT_STYLE_EDITOR, VIEW_GROUP_EDITOR };
	private final static String[] VIEW_SECTION_DIALOG_DEPENDENCIES = {};
	
	// Map of conflicting editors
	private static Map<String, List<String>> editorConflicts = new HashMap<String, List<String>>();
	
	/**
	 * Get data structure of editor dependencies
	 * @return
	 */
	private static Map<String, List<String>> getEditorConflicts() {
		return editorConflicts;
	}
	
	/**
	 * Set data structure of editor dependencies 
	 * @param dependenciesMap
	 */
	private static void setEditorConflicts(Map<String, List<String>> dependenciesMap) {
		editorConflicts = dependenciesMap;
	}
			
	/**
	 * Closes the specified editorPart if another editor of editorClass is already open.
	 * @param editorPart The editor to close.
	 * @param editorClass The type of editor to search for.
	 */
	public static void closeEditor(EditorPart editorPart, Class editorClass){
		IWorkbenchPage page = editorPart.getSite().getWorkbenchWindow().getActivePage();
		//Get all the current editors.
		IEditorReference editorReferences[] = page.getEditorReferences();
		if(editorReferences != null && editorReferences.length > 0){
			for (IEditorReference editorReference : editorReferences) {
				
				IEditorPart editor = editorReference.getEditor(false);
				
				try{
					if (Class.forName(editorClass.getName()).isInstance(editor)){
						boolean status = page.closeEditor(editorPart, true);
						if(! status)
							logger.debug("Could not close editor: " + editorPart.getTitle());
						else
							logger.debug("Closed editor: " + editorPart.getTitle());
						return;
					}
				}catch(Exception ex){
					logger.error(ex.getMessage());
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Closes any open editors of type editorClass.
	 * @param page The IWorkbenchPage.
	 * @param editorClass The editor to find.
	 */
	public static void closeDuplicateEditorType(IWorkbenchPage page, Class editorClass){
		//Get all the current editors.
		IEditorReference[] editorReferences = page.getEditorReferences();
		if(editorReferences != null && editorReferences.length > 0){
			for (IEditorReference editorReference : editorReferences) {
				
				IEditorPart editor = editorReference.getEditor(false);
				
				try{
					if (Class.forName(editorClass.getName()).isInstance(editor)){
												
						boolean status = page.closeEditor(editor, true);
						if(! status)
							logger.debug("Could not close duplicate editor type: " + editorReference.getTitle());
						else
							logger.debug("Closed duplicate editor type: " + editorReference.getTitle());
					}
				}catch(Exception ex){
					logger.error(ex.getMessage());
					ex.printStackTrace();
				}
			}
		}
	}

	
	/**
	 * Closes any open editors of type editorClass.
	 * @param page The IWorkbenchPage.
	 * @param editorClass The editor to find.
	 */
	public static void closeEditorType(IWorkbenchPage page, Class editorClass, boolean save){
		//Get all the current editors.
		IEditorReference[] editorReferences = page.getEditorReferences();
		
		if(editorReferences != null && editorReferences.length > 0){
			
			for (IEditorReference editorReference : editorReferences) {
				
				IEditorPart editor = editorReference.getEditor(false);
				
				try{
					if (Class.forName(editorClass.getName()).isInstance(editor)){
						boolean status = page.closeEditor(editor, save);
						if(! status) {
							logger.debug("Could not close editor type: " + editorReference.getTitle());
						} else {
							logger.debug("Closed editor type: " + editorReference.getTitle());
						}
					}
				} catch(Exception ex){
					logger.error(ex.getMessage());
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Notifies user if they are opening multiple editors that may reference the same object. 
	 * @return 
	 * @throws PartInitException 
	 * 
	 */
	public static boolean checkForEditorConflicts(IWorkbenchPage page, Class editorClass) throws PartInitException {
		
		if (page != null && editorClass != null) {
			String editorClassName = editorClass.getName();
			// String editorClassType = editorClass.getTypeName();
			// String editorClassSimpleName = editorClass.getSimpleName();
			logger.debug("Editor: " + editorClassName);
			
			// create map of editor dependencies 
			initializeEditorConflicts();
			
			// Map<String, List<String>> temp = getEditorDependencies();
			List<String> editorDependenciesKeySet = new ArrayList<String>(editorConflicts.keySet());
			
			for (String string : editorDependenciesKeySet) {
				
				if (string.equals(editorClassName)) {
					
					// get all dependencies for current editor
					List<String> dependencies =  editorConflicts.get(string);
					
					if (dependencies != null && dependencies.size() > 0) {
						
						//Get all open editors.
						IEditorReference[] editorReferences = page.getEditorReferences();
						
						if(editorReferences != null && editorReferences.length > 0){
							
							// Check for any editor dependencies that are currently open
							for (IEditorReference editorReference : editorReferences) {
								
								for (String currentString : dependencies) {
									
									String editorName = editorReference.getPart(false).getClass().getName();
									
									if (editorName.equalsIgnoreCase(currentString)) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	private static void initializeEditorConflicts() {	
		Map<String, List<String>> map = new HashMap<String,List<String>>();
		map.put(ADMIN_LOCK_EDITOR, Arrays.asList(ADMIN_LOCK_EDITOR_DEPENDENCIES));
		map.put(CONDITIONAL_STYLE_EDITOR, Arrays.asList(CONDITIONAL_STYLE_EDITOR_DEPENDENCIES));
		map.put(CONDITIONAL_FORMAT_EDITOR, Arrays.asList(CONDITIONAL_FORMAT_EDITOR_DEPENDENCIES));
		map.put(PLAN_CYCLE_EDITOR, Arrays.asList(PLAN_CYCLE_EDITOR_DEPENDENCIES));
		map.put(PLANNER_ROLE_EDITOR, Arrays.asList(PLANNER_ROLE_EDITOR_DEPENDENCIES));
		map.put(PRINT_STYLE_EDITOR, Arrays.asList(PRINT_STYLE_EDITOR_DEPENDENCIES));
		map.put(PROJECT_SETTINGS_EDITOR, Arrays.asList(PROJECT_SETTINGS_EDITOR_DEPENDENCIES));
		map.put(ROLE_CONFIGURATION_EDITOR, Arrays.asList(ROLE_CONFIGURATION_EDITOR_DEPENDENCIES));
		map.put(SEASON_EDITOR, Arrays.asList(SEASON_EDITOR_DEPENDENCIES));
		map.put(USER_SECURITY_EDITOR, Arrays.asList(USER_SECURITY_EDITOR_DEPENDENCIES));
		map.put(VIEW_GROUP_EDITOR , Arrays.asList(VIEW_GROUP_EDITOR_DEPENDENCIES));
		map.put(VIEW_DIALOG, Arrays.asList(VIEW_DIALOG_DEPENDENCIES));
		map.put(VIEW_SECTION_DIALOG, Arrays.asList(VIEW_SECTION_DIALOG_DEPENDENCIES));
		setEditorConflicts(map);
	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.util.ColorsUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.menu.dialogs.Week53SelelctorDialog;
import com.pace.admin.menu.editors.input.ProjectSettingsEditorInput;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.app.AppColors;
import com.pace.base.app.AppSettings;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.utility.StringUtils;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.swtdesigner.SWTResourceManager;

public class ProjectSettingsEditor extends EditorPart {
	
	private static Logger logger = Logger.getLogger(ProjectSettingsEditor.class);
	public static final String ID = "com.pace.admin.menu.editors.ProjectSettingsEditor";
	
	private IProject project;
	private Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	private AppSettings appSettings;
	private PafApplicationDef pafApplicationDef;
	private ProjectSettingsEditorInput projectSettingsInput;
	private boolean isDirty = false;

	//Color Selection Tab
	private AppColors appColors;
	private Text userLockColorText;
	private Text protectedColorText;
	private Text systemLockColorText;
	private Text nonplannableProtectedColorText;
	private Text forwardplannableProtectedColorText;
	private Text noteColorText;
	
	//Week 53 tab
	private Set<String> week53Years, week53Members;
	private TreeViewer viewer;
	private CheckboxTableViewer yearsCheckboxViewer;
	private Button btnSelectWeek;
	private String[] yearMembers;
	private Text wk53Text;
	Group grpYearSelections, grpWk53Selection; 
	Table years;
	
	public ProjectSettingsEditor() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		projectSettingsInput = (ProjectSettingsEditorInput)input;
		
		this.project = projectSettingsInput.getProject();
		pafApplicationDef = PafApplicationUtil.getPafApp(project);
		if( pafApplicationDef != null ) {
			appSettings = pafApplicationDef.getAppSettings();
			
			week53Years = appSettings.getWeek53Years();	
			week53Members = appSettings.getWeek53Members();
			projectSettingsInput.setWeek53MembersSelected(week53Members);
			
			appColors = appSettings.getAppColors();
			if(appColors == null) {
				appColors = new AppColors();
			}
		}
	}
	
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(final DisposeEvent e) {
				logger.debug("ProjectSettingsEditor disposed.");
			}
		});
		container.setLayout(new GridLayout(1, false));
		
		//Color Selection Tab
		final TabFolder tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tabFolder.getSelection()[0].getText().equalsIgnoreCase("Week 53 Setup")) {
					validateWeek53YearsAndMembers();
					populateControlValues();
				}
				if( isDirty ) {
					updateWeek53YearSelection();
				}
			}
		});
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TabItem tabColorSelection = new TabItem(tabFolder, SWT.NONE);
		tabColorSelection.setText("Color Selection");
		
		Composite compositeColorTab = new Composite(tabFolder, SWT.NONE);
		compositeColorTab.setBackgroundMode(SWT.INHERIT_FORCE);
		compositeColorTab.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		tabColorSelection.setControl(compositeColorTab);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 45;
		gridLayout.verticalSpacing = 10;
		gridLayout.marginRight = 5;
		gridLayout.marginLeft = 5;
		compositeColorTab.setLayout(gridLayout);


		// Create a text labels
		Label selectUsersToLabel;
		selectUsersToLabel = new Label(compositeColorTab, SWT.NONE);
		selectUsersToLabel.setText("Select the colors:");

		final Composite composite = new Composite(compositeColorTab, SWT.NONE);
		final GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd_composite.heightHint = 375;
		gd_composite.widthHint = 312;
		composite.setLayoutData(gd_composite);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.makeColumnsEqualWidth = true;
		gridLayout_1.numColumns = 2;
		composite.setLayout(gridLayout_1);

		final Label userLockColorLabel = new Label(composite, SWT.NONE);
		userLockColorLabel.setText("User Lock Color");
		new Label(composite, SWT.NONE);

		userLockColorText = new Text(composite, SWT.BORDER);
		userLockColorText.setTabs(1);
		userLockColorText.setEditable(false);
		final GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_text.widthHint = 63;
		userLockColorText.setLayoutData(gd_text);
		userLockColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button userLockColorButton = new Button(composite, SWT.NONE);
		userLockColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {userLockColorText.getBackground().getRGB()});
		        dlg.setRGB(userLockColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					userLockColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setUserLockColor(stringColor);
					editorModified();
		        }
			}
		});
		userLockColorButton.setText("...");
		userLockColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				userLockColorButton.setFocus();
			}
		});

		final Label protectedColorLabel = new Label(composite, SWT.NONE);
		protectedColorLabel.setText("Rule Protection Color");
		new Label(composite, SWT.NONE);

		protectedColorText = new Text(composite, SWT.BORDER);
		protectedColorText.setEditable(false);
		final GridData gd_text_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_text_1.widthHint = 64;
		protectedColorText.setLayoutData(gd_text_1);
		protectedColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button protectedColorButton = new Button(composite, SWT.NONE);
		protectedColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {protectedColorText.getBackground().getRGB()});
		        dlg.setRGB(protectedColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					protectedColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setProtectedColor(stringColor);
					editorModified();
		        }
			}
		});
		protectedColorButton.setText("...");
		protectedColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				protectedColorButton.setFocus();
			}
		});

		final Label systemLockColorLabel = new Label(composite, SWT.NONE);
		systemLockColorLabel.setText("User Change Color");
		new Label(composite, SWT.NONE);

		systemLockColorText = new Text(composite, SWT.BORDER);
		systemLockColorText.setEditable(false);
		systemLockColorText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		systemLockColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button systemLockColorButton = new Button(composite, SWT.NONE);
		systemLockColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {systemLockColorText.getBackground().getRGB()});
		        dlg.setRGB(systemLockColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					systemLockColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setSystemLockColor(stringColor);
					editorModified();
		        }
			}
		});
		systemLockColorButton.setText("...");
		systemLockColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				systemLockColorButton.setFocus();
			}
		});

		final Label nonplannableProtectedColorLabel = new Label(composite, SWT.NONE);
		nonplannableProtectedColorLabel.setText("Non-plannable Protection Color");
		new Label(composite, SWT.NONE);

		nonplannableProtectedColorText = new Text(composite, SWT.BORDER);
		nonplannableProtectedColorText.setEditable(false);
		nonplannableProtectedColorText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		nonplannableProtectedColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button nonplannableProtectedColorButton = new Button(composite, SWT.NONE);
		nonplannableProtectedColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {nonplannableProtectedColorText.getBackground().getRGB()});
		        dlg.setRGB(nonplannableProtectedColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					nonplannableProtectedColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setNonPlannableProtectedColor(stringColor);
					editorModified();
		        }
			}
		});
		nonplannableProtectedColorButton.setText("...");
		nonplannableProtectedColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				nonplannableProtectedColorButton.setFocus();
			}
		});

		final Label forwardplannableProtectedColorLabel = new Label(composite, SWT.NONE);
		forwardplannableProtectedColorLabel.setText("Elapsed Period Protection Color");
		new Label(composite, SWT.NONE);

		forwardplannableProtectedColorText = new Text(composite, SWT.BORDER);
		forwardplannableProtectedColorText.setEditable(false);
		forwardplannableProtectedColorText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		forwardplannableProtectedColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button forwardplannableProtectedColorButton = new Button(composite, SWT.NONE);
		forwardplannableProtectedColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {forwardplannableProtectedColorText.getBackground().getRGB()});
		        dlg.setRGB(forwardplannableProtectedColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					forwardplannableProtectedColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setForwardPlannableProtectedColor(stringColor);
					editorModified();
		        }
			}
		});
		forwardplannableProtectedColorButton.setText("...");
		forwardplannableProtectedColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				forwardplannableProtectedColorButton.setFocus();
			}
		});

		final Label noteColorLabel = new Label(composite, SWT.NONE);
		noteColorLabel.setText("Note Color");
		new Label(composite, SWT.NONE);

		noteColorText = new Text(composite, SWT.BORDER);
		noteColorText.setEditable(false);
		noteColorText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		noteColorText.setCursor(new Cursor(null, SWT.CURSOR_ARROW));

		final Button noteColorButton = new Button(composite, SWT.NONE);
		noteColorButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				ColorDialog dlg = new ColorDialog(shell);
				dlg.setRGBs(new RGB[] {noteColorText.getBackground().getRGB()});
		        dlg.setRGB(noteColorText.getBackground().getRGB());
		        RGB rgb = dlg.open();
		        if( rgb != null ) {
					Color selectedColor = new Color(shell.getDisplay(), rgb);
					noteColorText.setBackground(selectedColor);			
					String stringColor = ColorsUtil.colorToHexString(selectedColor);
					appColors.setNoteColor(stringColor);
					editorModified();
		        }
			}
		});
		noteColorButton.setText("...");
		
		final Button resetColorsButton = new Button(composite, SWT.NONE);
		resetColorsButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		resetColorsButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				appColors = new AppColors();
				initColors();
				editorModified();
			}
		});
		resetColorsButton.setText("Reset Colors");
		new Label(composite, SWT.NONE);
		noteColorText.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				noteColorButton.setFocus();
			}
		});
		
		if( appColors != null )
			initColors();

		//Week 53 Tab
		TabItem tabWk53Selection = new TabItem(tabFolder, SWT.NONE);
		tabWk53Selection.setText("Week 53 Setup");

		Composite compositeWk53Tab = new Composite(tabFolder, SWT.NONE);
		tabWk53Selection.setControl(compositeWk53Tab);
		compositeWk53Tab.setLayout(new GridLayout(1, false));
		
		grpYearSelections = new Group(compositeWk53Tab, SWT.NONE);
		GridData gd_grpYearSelections = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_grpYearSelections.widthHint = 368;
		gd_grpYearSelections.heightHint = 154;
		grpYearSelections.setLayoutData(gd_grpYearSelections);
		grpYearSelections.setText("Year Selection");
		
		yearsCheckboxViewer = CheckboxTableViewer.newCheckList(grpYearSelections, SWT.BORDER);
		years = yearsCheckboxViewer.getTable();
		years.setBounds(10, 28, 354, 134);
		years.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		years.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		grpWk53Selection = new Group(compositeWk53Tab, SWT.NONE);
		GridData gd_grpWk53Selection = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_grpWk53Selection.heightHint = 50;
		gd_grpWk53Selection.widthHint = 366;
		grpWk53Selection.setLayoutData(gd_grpWk53Selection);
		grpWk53Selection.setText("Week 53 Selection");
		
		btnSelectWeek = new Button(grpWk53Selection, SWT.NONE);
		btnSelectWeek.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Week53SelelctorDialog wk53Dlg = new Week53SelelctorDialog(shell, projectSettingsInput);
				int id = wk53Dlg.open();
				if( id == IDialogConstants.OK_ID ) {
					Set<String> week53s =  projectSettingsInput.getWeek53MembersSelected();
					String week53 = "";
					if( week53s.size() > 0 ) {
						week53 = StringUtils.setToSortedString(week53s, ",");
					}
					setWeek53Members(week53s);
					wk53Text.setText(week53);
					if( projectSettingsInput.isSelectorModified() ) {
						editorModified();
					}
				}
			}
		});
		btnSelectWeek.setText("Select Week 53 Member(s):");
		btnSelectWeek.setBounds(10, 31, 187, 25);
		
		wk53Text = new Text(grpWk53Selection, SWT.BORDER | SWT.READ_ONLY);
		wk53Text.setBounds(203, 31, 159, 25);

	}

	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	/**
	 * Gets the tree viewer.
	 * @return The tree viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the tree viewer.
	 * @param viewer The tree viewer.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
	
	private void validateWeek53YearsAndMembers() {
		//load years and time dimensions
		if( years.getItemCount() == 0 ) {
			projectSettingsInput.buildSimpleTree();
		}
		
		//load years
		yearMembers = projectSettingsInput.getYears();
		if( yearMembers == null ) {
			String outMessage = "Couldn't get cached dimension for 'Years'. Login into server and cache dimensions";
			GUIUtil.openMessageWindow("Validation Error(s)", outMessage, SWT.ICON_WARNING);
			return;
		}
		
		//load years members if theya are not loaded
		if( years.getItemCount() == 0 ) {
			EditorControlUtil.addElements(yearsCheckboxViewer, yearMembers);
		}
		
		//validation: if Previously selected member(s) in Year Dimension no longer exist
		List<String> invalidYearMember = new ArrayList<String>(), invalidWk53Member = new ArrayList<String>();
		Set<String> yrMems = new HashSet<String>(Arrays.asList(yearMembers));
		if( week53Years != null && week53Years.size() > 0 ) {
			Iterator<String> it = week53Years.iterator();
			while(it.hasNext()) {
				String wk53Yr = it.next();
				if( ! yrMems.contains(wk53Yr) ) {
					invalidYearMember.add(wk53Yr);
					it.remove();
					editorModified();
				}
			}
		}
		
		//load Time dimension
		PafSimpleDimTree pafSimpleTree = projectSettingsInput.getPafSimpleTreeTime();
		if( pafSimpleTree == null ) {
			String outMessage = "Couldn't get cached dimension for 'Time'. Login into server and cache dimensions";
			GUIUtil.openMessageWindow("Validation Error(s)", outMessage, SWT.ICON_WARNING);
			return;
		}
		
		//validation: if Previously selected member(s) in Time Dimension no longer exist
		logger.debug("Converting TreeInto hash start");
		HashMap<String, PafSimpleDimMember> treeHashMap = DimensionTreeUtility.convertTreeIntoHashMap(pafSimpleTree);
		Set<String> timeMems = treeHashMap.keySet();
		if( week53Members != null && week53Members.size() != 0 ) {
			Iterator<String> it = week53Members.iterator();
			while(it.hasNext()) {
				String wk53Mem = it.next();
				if( ! timeMems.contains(wk53Mem) ) {
					invalidWk53Member.add(wk53Mem);
					it.remove();
					editorModified();
				}
			}
		}
		projectSettingsInput.setWeek53MembersSelected(week53Members);
		
		if( invalidYearMember.size() != 0 && invalidWk53Member.size() == 0 ) {
			GUIUtil.openMessageWindow(
					"Validation Error(s)", 
					"The previously selected Year members: " + StringUtils.arrayListToString(invalidYearMember,",") + " are no longer valid.",
					SWT.ICON_WARNING);
		}
		else if( invalidYearMember.size() == 0 && invalidWk53Member.size() != 0 ) {
			GUIUtil.openMessageWindow(
					"Validation Error(s)",
					"The previously selected Time members: " + StringUtils.arrayListToString(invalidWk53Member,",")  + " are no longer valid.",
					SWT.ICON_WARNING);
		}
		else if( invalidYearMember.size() != 0 && invalidWk53Member.size() != 0 ) {
			GUIUtil.openMessageWindow(
					"Validation Error(s)",
					"The previously selected Year members: " + StringUtils.arrayListToString(invalidYearMember,",") + " and Time members: " + StringUtils.arrayListToString(invalidWk53Member,",") + " are no longer valid.",
					SWT.ICON_WARNING);
		}
		
	}

	private void populateControlValues(){
		//set the week 53 years 
		if( week53Years != null ) {
			EditorControlUtil.setCheckedElements(yearsCheckboxViewer, week53Years.toArray());
		}
		
		//set the week 53 members
		if( week53Members != null && week53Members.size() > 0 ) {
			List<String> week53Ary = new ArrayList<String>();
			week53Ary.addAll(week53Members);
			if( week53Ary.size() > 0 ) {
				String week53 = StringUtils.setToSortedString(week53Members,",");
				EditorControlUtil.setText(wk53Text, week53);
			}
		}	
	}
	
	void initColors(){
		
		String clr = appColors.getUserLockColor();
		int r = Integer.parseInt(clr.substring(0, 2), 16);
		int g = Integer.parseInt(clr.substring(2, 4), 16);
		int b = Integer.parseInt(clr.substring(4, 6), 16);
		userLockColorText.setBackground(SWTResourceManager.getColor(r, g, b));
		
		clr = appColors.getSystemLockColor();
		r = Integer.parseInt(clr.substring(0, 2), 16);
		g = Integer.parseInt(clr.substring(2, 4), 16);
		b = Integer.parseInt(clr.substring(4, 6), 16);
		systemLockColorText.setBackground(SWTResourceManager.getColor(r, g, b));
		
		clr = appColors.getProtectedColor();
		r = Integer.parseInt(clr.substring(0, 2), 16);
		g = Integer.parseInt(clr.substring(2, 4), 16);
		b = Integer.parseInt(clr.substring(4, 6), 16);
		protectedColorText.setBackground(SWTResourceManager.getColor(r, g, b));
		
		clr = appColors.getNonPlannableProtectedColor();
		r = Integer.parseInt(clr.substring(0, 2), 16);
		g = Integer.parseInt(clr.substring(2, 4), 16);
		b = Integer.parseInt(clr.substring(4, 6), 16);
		nonplannableProtectedColorText.setBackground(SWTResourceManager.getColor(r, g, b));
		
		clr = appColors.getForwardPlannableProtectedColor();
		r = Integer.parseInt(clr.substring(0, 2), 16);
		g = Integer.parseInt(clr.substring(2, 4), 16);
		b = Integer.parseInt(clr.substring(4, 6), 16);
		forwardplannableProtectedColorText.setBackground(SWTResourceManager.getColor(r, g, b));
		
		clr = appColors.getNoteColor();
		r = Integer.parseInt(clr.substring(0, 2), 16);
		g = Integer.parseInt(clr.substring(2, 4), 16);
		b = Integer.parseInt(clr.substring(4, 6), 16);		
		noteColorText.setBackground(SWTResourceManager.getColor(r,g,b));
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}
	
	public Set<String> getWeek53Years() {
		return week53Years;
	}

	public void setWeek53Years(Set<String> week53Years) {
		this.week53Years = week53Years;
	}

	public Set<String> getWeek53Members() {
		return week53Members;
	}

	public void setWeek53Members(Set<String> week53Members) {
		this.week53Members = week53Members;
	}
	
	private void updateWeek53YearSelection() {
		String[] selItems = new String[yearsCheckboxViewer.getCheckedElements().length];
		for(int i = 0; i < selItems.length; i++){
			selItems[i] = yearsCheckboxViewer.getCheckedElements()[i].toString().trim();
		}
		Set<String> week53YearsSelected = new HashSet<String>();
		week53YearsSelected.addAll(Arrays.asList(selItems));
		setWeek53Years(week53YearsSelected);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		if(isDirty){
			updateWeek53YearSelection();
			
			String outMessage = null;
			Set<String> wk53Mems = projectSettingsInput.getWeek53MembersSelected();
			if( wk53Mems != null && week53Years != null ) {
				//No Year dimension members have been selected, but at least one Week has
				if( wk53Mems.size() ==0  && week53Years.size() != 0 ) {
					outMessage = "No 'Week 53' members have been selected. With this current configuration no Week 53 planning can occur. Are you sure you want to apply these changes?";
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
				//No Time dimension members have been selected, but at least one Year has
				else if( wk53Mems.size() != 0 && week53Years.size() == 0 ) {
					outMessage = "No Year selections have been made. With this current configuration no Week 53 planning can occur. Are you sure you want to apply these changes?";
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
				//No member selections have been made (in either Time or Year dimensions)
				else if( wk53Mems.size() ==0 && week53Years.size() == 0 ) {
					outMessage = "There are no Year or Week 53 selections. With this current configuration, no Week 53 planning can occur. Are you sure you want to apply these changes?";
					if( ! GUIUtil.openMessageWindowOkCancel("Validation Error(s)", outMessage) ) {//cancel button was clicked
						return;
					}
				}
				//set the selected week 53 members
				appSettings.setWeek53Members(wk53Mems);
			}
			
			//save the selected week 53 years and members
			if( week53Years != null ) {
				appSettings.setWeek53Years(week53Years);
			}
			//save app colors
			appSettings.setAppColors(appColors);
			
			pafApplicationDef.setAppSettings(appSettings);
			PafApplicationUtil.setPafApps(project, pafApplicationDef);
			
			isDirty = false;
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.util.ControlUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.menu.editors.input.PlannerRoleInput;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.RoleConfigurationsNode;
import com.pace.admin.menu.nodes.SecurityNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.admin.menu.nodes.UserSecurityDomainNode;

/**
 * Implementation of EditorPart, used to add or modify Planner Roles.
 * @author kmoos
 * @version x.xx
 */
public class PlannerRoleEditor extends EditorPart {
	private Combo readOnlyCombo;
	/**
	 * Unique id for the editor.
	 */
	public static final String ID = "com.pace.admin.menu.editors.plannerrole";
	/**
	 * THe Editor it.
	 */
	public static final String EDITOR_ID = "Role Editor";
	private static Logger logger = Logger.getLogger(PlannerRoleEditor.class);
	private Table table;
	private Text roleName;
	private Label roleNameLabel;
	private Combo planType;
	private Label planTypeLabel;
	private PlannerRoleInput plannerRoleInput;
	private CheckboxTableViewer seasonIdCheckboxTableViewer;
	private Label seasonIdsLabel;
	private Button selAllButton;
	private Button deselAllButton;
	private Button upArrowButton; 
	private Button downArrowButton; 
	private boolean isDirty;
	private TreeViewer viewer;
	private PlannerRolesNode plannerRolesNode;	
	private Composite upDownComposite;
	
	/**
	 * Constructor
	 */
	public PlannerRoleEditor() {
		super();
	}

	/**
	 * Implementation of EditorPart.doSave.
	 * @param monitor
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		
		if(isDirty){
			
			roleName.setText(roleName.getText().trim());
			
			try{
				String missingField = isInputValid();
				if(missingField != null){
					GUIUtil.openMessageWindow(
							this.getTitle(),
							"The following fields are required:\n-" + missingField,
							SWT.ICON_INFORMATION);
					
					monitor.setCanceled(true);
					
					return;
				}
				
				if((plannerRoleInput.isNew() || plannerRoleInput.isCopy()) && plannerRoleInput.itemExists(roleName.getText())){
					if (! GUIUtil.askUserAQuestion(
							this.getTitle(),
							"Role: '" + roleName.getText() + "' already exists.\nReplace existing item?")) {
						
						monitor.setCanceled(true);
						
						return;
					}
				}
				
				plannerRoleInput.setRoleName(roleName.getText().trim());
				plannerRoleInput.setPlanType(planType.getText().trim());
				plannerRoleInput.setReadOnly(Boolean.parseBoolean(readOnlyCombo.getText()));
				
				String[] selItems = new String[seasonIdCheckboxTableViewer.getCheckedElements().length];
				for(int i = 0; i < selItems.length; i++){
					selItems[i] = seasonIdCheckboxTableViewer.getCheckedElements()[i].toString().trim();
				}
				
				plannerRoleInput.setSelectedSeasons(selItems);
				plannerRoleInput.save();
				
				isDirty = false;
				firePropertyChange(IEditorPart.PROP_DIRTY);
				setPartName(Constants.MENU_ROLE + ": " + this.getEditorInput().getName());
				updateTreeViewer();
			} catch(Exception e){
				logger.error(e.getLocalizedMessage());
			}
		}
	}

	/**
	 * Implementation of EditorPart.doSaveAs.
	 */
	@Override
	public void doSaveAs() {
	}

	/**
	 * Implementation of EditorPart.init.
	 * @param site
	 * @param input
	 * @throws org.eclipse.ui.PartInitException
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(Constants.MENU_ROLE + ": " + input.getName());
		plannerRoleInput = (PlannerRoleInput) input;
		isDirty = false;
	}

	/**
	 * Implementation of EditorPart.isDirty.
	 * @return boolean
	 */
	@Override
	public boolean isDirty() {
		setPartName(Constants.MENU_ROLE + ": " + roleName.getText());
		return isDirty;
	}

	/**
	 * Sets the dirty flag as fires the propery changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}
	
	/**
	 * Implementation of EditorPart.isSaveAsAllowed.
	 * @return boolean
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	/**
	 * Implementation of EditorPart.createPartControl.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		roleNameLabel = new Label(container, SWT.NONE);
		roleNameLabel.setText("Role Name");

		roleName = new Text(container, SWT.BORDER);
		roleName.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		roleName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);

		planTypeLabel = new Label(container, SWT.NONE);
		EditorControlUtil.setText(planTypeLabel, plannerRoleInput.getPlanTypeDimName());

		planType = new Combo(container, SWT.READ_ONLY);
		planType.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		planType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);

		final Label readOnlyLabel = new Label(container, SWT.NONE);
		readOnlyLabel.setText("Read Only");

		readOnlyCombo = new Combo(container, SWT.READ_ONLY);
		readOnlyCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		readOnlyCombo.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				editorModified();
			}
		});
		new Label(container, SWT.NONE);
		
		seasonIdsLabel = new Label(container, SWT.NONE);
		seasonIdsLabel.setText(Constants.MENU_SEASONS);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
		
		
		
		seasonIdCheckboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER);
		table = seasonIdCheckboxTableViewer.getTable();
		table.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				seasonSelectionChanged();
				editorModified();
			}
		});
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		upDownComposite = new Composite(container, SWT.NONE);
		upDownComposite.setLayout(new GridLayout(1, false));
		
				deselAllButton = new Button(container, SWT.NONE);
				deselAllButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(final SelectionEvent e) {
						seasonIdCheckboxTableViewer.setAllChecked(false);
						editorModified();
					}
				});
				deselAllButton.setText("Deselect All");

		upArrowButton = new Button(upDownComposite, SWT.ARROW);
		upArrowButton.setToolTipText("Move selected item up.");
		upArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				
				//user pressed up button
				ControlUtil.directionalButtonPressed(seasonIdCheckboxTableViewer, ControlUtil.UP_BUTTON_ID);
				seasonSelectionChanged();
				editorModified();
			}
		});
		upArrowButton.setText("button");
		upArrowButton.setEnabled(false);

		downArrowButton = new Button(upDownComposite, SWT.ARROW | SWT.DOWN);
		downArrowButton.setToolTipText("Move selected item down.");
		downArrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				
				//user pressed down button
				ControlUtil.directionalButtonPressed(seasonIdCheckboxTableViewer, ControlUtil.DOWN_BUTTON_ID);
				seasonSelectionChanged();
				editorModified();
			}
		});
		downArrowButton.setText("button");
		downArrowButton.setEnabled(false);
		
		
		
		selAllButton = new Button(container, SWT.NONE);
		selAllButton.setLayoutData(new GridData(67, SWT.DEFAULT));
		selAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				seasonIdCheckboxTableViewer.setAllChecked(true);
				editorModified();
			}
		});
		selAllButton.setText("Select All");
		new Label(container, SWT.NONE);
		
//		EditorUtils.closeEditor(this, PlannerRoleEditor.class);
		populateControlValues();
		
		//TTN-2381
		isDirty = false;
		
		if ( plannerRoleInput != null && ( plannerRoleInput.isNew() || plannerRoleInput.isCopy() ) ) { // && isInputValid() == null ) {
			
			editorModified();
			
		}
		
	}
	
	protected void seasonSelectionChanged() {
		
		updateUpButtonStatus();
		updateDownButtonStatus();
		
	}	
	
	protected void updateDownButtonStatus() {
		
		Table table = seasonIdCheckboxTableViewer.getTable();
		
		TableItem[] selectedItems = table.getSelection();
	
		int[] selItemIdx = table.getSelectionIndices();
		
		boolean invalidDownButtonSelection = false;
		
		if ( selectedItems.length == 1) {
			
			int selectedItemNdx = selItemIdx[0];
			
			if(selectedItemNdx == table.getItemCount() - 1){
				invalidDownButtonSelection = true;
			} 
				
		} else {
			
			invalidDownButtonSelection = true;
			
		}
		
		downArrowButton.setEnabled(! invalidDownButtonSelection);
		
	}

	protected void updateUpButtonStatus() {
		
		Table table = seasonIdCheckboxTableViewer.getTable();
		
		TableItem[] selectedItems = table.getSelection();
	
		int[] selItemIdx = table.getSelectionIndices();
		
		boolean invalidUpButtonSelection = false;
		
		if ( selectedItems.length == 1) {
			
			int selectedItemNdx = selItemIdx[0];
			
			if(selectedItemNdx == 0){
				invalidUpButtonSelection = true;
			} 
			
		} else {
			
			invalidUpButtonSelection = true;
			
		}
		
		upArrowButton.setEnabled(! invalidUpButtonSelection);
				
	}
	
	/**
	 * Default control to set focus to when the editor gets focus.
	 */
	@Override
	public void setFocus() {
		if (roleName != null && !roleName.isDisposed()) {
			roleName.setFocus();
		}
	}

	/**
	 * Gets the planner roles node.
	 * @return The planner roles node.
	 */
	public PlannerRolesNode getPlannerRolesNode() {
		return plannerRolesNode;
	}

	/**
	 * Set the planner roles node.
	 * @param plannerRolesNode The planner roles node.
	 */
	public void setPlannerRolesNode(PlannerRolesNode plannerRolesNode) {
		this.plannerRolesNode = plannerRolesNode;
	}

	/**
	 * Gets the tree viewer.
	 * @return The tree viewer.
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the tree viewer.
	 * @param viewer The tree viewer.
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}
	
	/**
	 * Populates the control with the default/startup values.
	 */
	private void populateControlValues(){
		EditorControlUtil.setText(roleName, plannerRoleInput.getRoleName());
		String[] planTypes = plannerRoleInput.getPlanTypes();
		EditorControlUtil.addItems(planType, planTypes);
		EditorControlUtil.selectItem(planType, plannerRoleInput.getPlanType());
		EditorControlUtil.addItems(readOnlyCombo, new String[] { Constants.DROPDOWN_BOOL_FALSE, Constants.DROPDOWN_BOOL_TRUE });
		EditorControlUtil.selectItem(readOnlyCombo, StringUtil.initCap(Boolean.toString(plannerRoleInput.isReadOnly())));
		EditorControlUtil.addElements(seasonIdCheckboxTableViewer, plannerRoleInput.getAllSeasons());
		EditorControlUtil.setCheckedElements(seasonIdCheckboxTableViewer, plannerRoleInput.getSelectedSeasons());
		
		//if the cache and server are not present, then throw a message and disable the controls.
		if( planTypes == null ) {
			GUIUtil.openMessageWindow(
					this.getTitle(),
					"No default server or dimension tree could be found. Or server is not running.",
					SWT.ICON_WARNING);
			setControlStatus(false);
		}
//		if(! plannerRoleInput.isServerRunning()){
//			GUIUtil.openMessageWindow(
//					this.getTitle(),
//					"This role cannot be modified because the server is not running.\nPlease start the server and reopen this role.",
//					SWT.ICON_WARNING);
//			setControlStatus(false);
//		}
		
	}
	
	/**
	 * Enables or disables the control states.
	 */
	private void setControlStatus(boolean enable){
		roleName.setEnabled(enable);
		planType.setEnabled(enable);
		selAllButton.setEnabled(enable);
		deselAllButton.setEnabled(enable);
		seasonIdCheckboxTableViewer.getTable().setEnabled(enable);
		readOnlyCombo.setEnabled(enable);
	}
	
	/**
	 * Validates the control input values.
	 * @return The label associated with the control 
	 * if an invalid value is found, or null if the entries are valid. 
	 */
	private String isInputValid(){
		if(roleName.getText() == null || 
				roleName.getText().trim().length() == 0){
			return roleNameLabel.getText();
		}
		
		if(planType == null || 
				planType.getText().length() == 0){
			return planTypeLabel.getText();
		}
	
		if(seasonIdCheckboxTableViewer == null ||
				seasonIdCheckboxTableViewer.getCheckedElements() == null ||
				seasonIdCheckboxTableViewer.getCheckedElements().length == 0){
			return seasonIdsLabel.getText();
		}
	
		return null;
	}
	
	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		try{
			
			if(viewer != null && plannerRolesNode != null){
				
				plannerRolesNode.createChildren(null);
				viewer.refresh(plannerRolesNode);
				
				//refresh the menu view for role configs.  Updates the label.
				if(plannerRolesNode.getParent() != null && 
						plannerRolesNode.getParent().getChildren() != null){
					for(Object node : plannerRolesNode.getParent().getChildren()){
						if(node instanceof RoleConfigurationsNode){
							RoleConfigurationsNode roleConfigsNode = 
								(RoleConfigurationsNode) node;
							roleConfigsNode.createChildren(null);
							viewer.refresh(roleConfigsNode);
							break;
						}
					}
				}

				
				ITreeNode projNode = plannerRolesNode.getProjectNode();
				//refresh the menu view for user security.  Updates the label.
				if ( projNode != null && projNode instanceof ProjectNode) {
										
					ProjectNode projectNode = (ProjectNode) projNode;
					
					if ( projectNode != null ) {
						
						for (Object projectChildNode : projectNode.getChildren()) {
					
							if ( projectChildNode instanceof SecurityNode ) {
								
								SecurityNode securityNode = (SecurityNode) projectChildNode;
								
								for(Object folder : securityNode.getChildren()){
									ITreeNode folderNode = (ITreeNode) folder;
									for(Object subFolder : folderNode.getChildren()){
										if(subFolder instanceof UserSecurityDomainNode){
											TreeNode temp = (TreeNode) subFolder;
											temp.createChildren(null);
											viewer.refresh(temp);
										}
									}
									break;
								}
							}
						}
					}
				}
	
			}
		} catch(Exception e){
			logger.error(e.getMessage());
		}
	}
}
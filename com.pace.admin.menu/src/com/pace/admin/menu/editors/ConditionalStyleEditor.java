/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.pace.admin.global.composite.ImageCombo;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.util.ColorsUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.input.ConditionalStyleEditorInput;
import com.pace.admin.menu.nodes.ConditionalFormatNode;
import com.pace.admin.menu.nodes.ConditionalStyleNode;
import com.pace.admin.menu.nodes.ConditionalStylesNode;
import com.pace.admin.menu.nodes.StylesFormatsNode;
import com.pace.admin.menu.nodes.TreeNode;
import com.pace.base.format.ColorScale;
import com.pace.base.format.ColorScaleColorNumType;
import com.pace.base.format.ColorScaleCriteria;
import com.pace.base.format.CriteriaType;
import com.pace.base.format.CriteriaTypeValues;
import com.pace.base.format.DataBarAppearance;
import com.pace.base.format.DataBarAppearanceBarDirectionType;
import com.pace.base.format.DataBarAppearanceBorderType;
import com.pace.base.format.DataBarAppearanceFillType;
import com.pace.base.format.DataBarCriteria;
import com.pace.base.format.DataBars;
import com.pace.base.format.IPaceConditionalStyle;
import com.pace.base.format.IconCriteria;
import com.pace.base.format.IconStyle;
import com.pace.base.format.IconStyleType;
import com.pace.base.utility.GUIDUtil;
import com.pace.base.utility.StringUtils;
import com.swtdesigner.ResourceManager;
import com.swtdesigner.SWTResourceManager;


public class ConditionalStyleEditor extends EditorPart {

	public static final String ID = "com.pace.admin.menu.editors.ConditionalStyleEditor";
	private static Logger logger = Logger.getLogger(ConditionalStyleEditor.class);

	private IProject project = null;
	private TreeViewer viewer;
	private TreeNode treeNode;
	
	private IPaceConditionalStyle condStyle;
	private ConditionalStyleEditorInput condStyleEditorInput;
	private boolean isDirty;
	
	private TabFolder tabFolder;
	private Text textStyleName;
	private Combo comboStyleType;
	private enum StyleType {
		ColorScale(0, "Color Scale"), Icon(1, "Icon"), DataBars(2, "Data Bars");
		
		int index;
		String displayText;
		StyleType(int index, String displayText) {
			this.index = index;
			this.displayText = displayText;
		}
	}
	
	private Map<Integer, List<Control>> tabControlMap = new HashMap<Integer, List<Control>>();
	
	private TabItem tabColorScale, tabIcon, tabDataBars;
	
	//Color Scale tab
	private Label lblMidScale;
	private Combo comboNumOfColors, comboMinType, comboMidType, comboMaxType;
	private Text textMinValue,textMidValue, textMaxValue, txtMinColor, txtMidColor, txtMaxColor;
	private Button btnMidColor;
	//Icon tab
	private ImageCombo imgCombo;
	private Label lblIcon_1, lblIcon_2, lblIcon_3, lblIcon_4, lblIcon_5; 
	private Combo cmbComparive1,  cmbComparive2,  cmbComparive3,  cmbComparive4, cmbIconType1, cmbIconType2, cmbIconType3, cmbIconType4;
	private Text textIconValue1, textIconValue2, textIconValue3, textIconValue4;
	//Bar Direction tab
	private Combo cmbMinBarType, cmbMaxBarType, cmbFill, cmbBorder, cmbBarDirection;
	private Text txtMinBarValue,txtMaxBarValue, txtFillColor, txtBorderColor;
	
	private DecimalFormat df = new DecimalFormat("#");

	public ConditionalStyleEditor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * @return the treeNode
	 */
	public TreeNode getTreeNode() {
		return treeNode;
	}

	/**
	 * @param treeNode
	 *            the treeNode to set
	 */
	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		df.setMaximumFractionDigits(2);

		condStyleEditorInput = (ConditionalStyleEditorInput) input;
		project = condStyleEditorInput.getProject();
		condStyle = condStyleEditorInput.getConditionalStyle();
		if( condStyle != null ) {
			if( condStyleEditorInput.isCopy() ) {
				condStyle = (IPaceConditionalStyle) condStyle.clone();
			}
		}
		setSite(site);
		setInput(input);
		
		setPartName(Constants.CONDITIONAL_STYLES + ": " + condStyleEditorInput.getName());

		isDirty = false;
	}

	@Override
	public void createPartControl(Composite parent) {

		Composite container = new Composite(parent, SWT.RESIZE);
		GridLayout gl_container = new GridLayout(4, false);
		gl_container.marginTop = 10;
		container.setLayout(gl_container);
		
		Label lblStyleName = new Label(container, SWT.NONE);
		lblStyleName.setAlignment(SWT.CENTER);
		GridData gd_lblStyleName = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_lblStyleName.widthHint = 120;
		lblStyleName.setLayoutData(gd_lblStyleName);
		lblStyleName.setText("Style Name:");
		
		textStyleName = new Text(container, SWT.BORDER);
		GridData gd_textStyleName = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd_textStyleName.widthHint = 281;
		textStyleName.setLayoutData(gd_textStyleName);
		textStyleName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		
		Label lblNewLabel_4 = new Label(container, SWT.NONE);
		lblNewLabel_4.setAlignment(SWT.CENTER);
		GridData gd_lblNewLabel_4 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_lblNewLabel_4.widthHint = 120;
		lblNewLabel_4.setLayoutData(gd_lblNewLabel_4);
		lblNewLabel_4.setText("Type of Style:");
		
		comboStyleType = new Combo(container, SWT.READ_ONLY);
		comboStyleType.setItems(new String[] {"Color Scale", "Icon", "Data Bars"});
		GridData gd_comboStyleType = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gd_comboStyleType.widthHint = 265;
		comboStyleType.setLayoutData(gd_comboStyleType);
		comboStyleType.setText("Color Scale");
		
		comboStyleType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				showHideTabs(comboStyleType.getSelectionIndex());
				editorModified();
				
//				enableTabControls();
			}
		});
		
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
		tabFolder = new TabFolder(container, SWT.RESIZE);
		GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 2);
		gd_tabFolder.heightHint = 500;
		tabFolder.setLayoutData(gd_tabFolder);
		
		loadConditionalStyle();
		
		enableFormControls();
		
		populateControlValues();
		
	}
	
	private void showHideTabs(int index) {
		switch(index) {
			case 0:
				if( tabColorScale == null || tabColorScale.isDisposed() )
					createColorScaleTab();
				if( tabIcon != null && ! tabIcon.isDisposed() )
					tabIcon.dispose();
				if( tabDataBars != null && ! tabDataBars.isDisposed() )
					tabDataBars.dispose();
				break;
			case 1:
				if( tabIcon == null || tabIcon.isDisposed() )
					createIconTab();
				if( tabColorScale != null && ! tabColorScale.isDisposed() )
					tabColorScale.dispose();
				if( tabDataBars != null && ! tabDataBars.isDisposed() )
					tabDataBars.dispose();
				break;
			case 2:
				if( tabDataBars == null || tabDataBars.isDisposed() )
					createDataBarsTab();
				if( tabColorScale != null && ! tabColorScale.isDisposed() )
					tabColorScale.dispose();
				if( tabIcon != null && ! tabIcon.isDisposed() )
					tabIcon.dispose();
				break;
			default:
				break;
		}
	}
	
	private void createColorScaleTab() {
		//Color Scale Tab
		List<Control> colorScaleControls = new ArrayList<Control>();
		
		tabColorScale = new TabItem(tabFolder, SWT.NONE);
		tabColorScale.setText("Color Scale");
		
		Composite compositeTabColorScale = new Composite(tabFolder, SWT.BORDER);
		tabColorScale.setControl(compositeTabColorScale);
		GridLayout gl_compositeTabColorScale = new GridLayout(7, false);
		gl_compositeTabColorScale.marginTop = 10;
		gl_compositeTabColorScale.marginLeft = 10;
		compositeTabColorScale.setLayout(gl_compositeTabColorScale);
		
		//row 1
		Label lblNumberOfColors = new Label(compositeTabColorScale, SWT.NONE);
		lblNumberOfColors.setAlignment(SWT.CENTER);
		lblNumberOfColors.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblNumberOfColors.setText("Number of colors:");
		
		comboNumOfColors = new Combo(compositeTabColorScale, SWT.READ_ONLY);
		GridData gd_comboNumOfColors = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_comboNumOfColors.widthHint = 100;
		comboNumOfColors.setLayoutData(gd_comboNumOfColors);
		comboNumOfColors.setItems(new String[] {"2-color", "3-color"});
		comboNumOfColors.setText("2-color");
		colorScaleControls.add(comboNumOfColors);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		
		//row 2
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		
		//row 3
		new Label(compositeTabColorScale, SWT.NONE);
		
		Label lblMinScale = new Label(compositeTabColorScale, SWT.CENTER);
		GridData gd_lblMinScale = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblMinScale.widthHint = 100;
		lblMinScale.setLayoutData(gd_lblMinScale);
		lblMinScale.setAlignment(SWT.LEFT);
		lblMinScale.setText("Minimium");
		
		lblMidScale = new Label(compositeTabColorScale, SWT.NONE);
		GridData gd_lblMidScale = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblMidScale.widthHint = 100;
		lblMidScale.setLayoutData(gd_lblMidScale);
		lblMidScale.setText("Midpoint");
		lblMidScale.setVisible(false);
		
		Label lblMaxScale = new Label(compositeTabColorScale, SWT.CENTER);
		lblMaxScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblMaxScale.setText("Maximum");
		lblMaxScale.setAlignment(SWT.LEFT);
		
		//row 4
		Label lblColorScaleType = new Label(compositeTabColorScale, SWT.NONE);
		lblColorScaleType.setAlignment(SWT.CENTER);
		lblColorScaleType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblColorScaleType.setText("Type:");
		
		comboMinType = new Combo(compositeTabColorScale, SWT.READ_ONLY);
		comboMinType.setItems(new String[] {"Lowest Value", "Number", "Percent", "Percentile"});
		GridData gd_comboMinType = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_comboMinType.widthHint = 100;
		comboMinType.setLayoutData(gd_comboMinType);
		comboMinType.setText("Lowest Value");
		colorScaleControls.add(comboMinType);
		
		comboMidType = new Combo(compositeTabColorScale, SWT.READ_ONLY);
		comboMidType.setItems(new String[] {"Number", "Percent", "Percentile"});
		comboMidType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		comboMidType.setText("Percentile");
		comboMidType.setVisible(false);
		colorScaleControls.add(comboMidType);
		
		comboMaxType = new Combo(compositeTabColorScale, SWT.READ_ONLY);
		comboMaxType.setItems(new String[] {"Highest Value", "Number", "Percent", "Percentile"});
		GridData gd_comboMaxPoint = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_comboMaxPoint.widthHint = 100;
		comboMaxType.setLayoutData(gd_comboMaxPoint);
		comboMaxType.setText("Highest Value");
		colorScaleControls.add(comboMaxType);

		//row 5
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		
		//row 6
		Label lblColorScaleValue = new Label(compositeTabColorScale, SWT.NONE);
		lblColorScaleValue.setAlignment(SWT.CENTER);
		lblColorScaleValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblColorScaleValue.setText("Value:");
		
		textMinValue = new Text(compositeTabColorScale, SWT.BORDER);
		textMinValue.setEnabled(false);
		GridData gd_textMinValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_textMinValue.widthHint = 100;
		textMinValue.setLayoutData(gd_textMinValue);
		colorScaleControls.add(textMinValue);
		
		textMinValue.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		// Minimum Value verify listener
		textMinValue.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(comboMinType, e);
			}
		});
		
		comboMinType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				enableColorScaleMinimumControls();
				
				if( comboMinType.getSelectionIndex() != 0 ) { //"Lowest Value"
					textMinValue.setText("0");
				}
				editorModified();
			}
		});
		
		textMidValue = new Text(compositeTabColorScale, SWT.BORDER);
		textMidValue.setText("50");
		GridData gd_textMidpointValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_textMidpointValue.widthHint = 100;
		textMidValue.setLayoutData(gd_textMidpointValue);
		textMidValue.setVisible(false);
		colorScaleControls.add(textMidValue);

		textMidValue.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		// Midpoint Value verify listener
		textMidValue.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(comboMidType, e);
			}
		});
		
		comboMidType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableColorScaleMidPointControls();
				if( comboMidType.getSelectionIndex() == 0 ) //Number
					textMidValue.setText("0");
				else if( comboMidType.getSelectionIndex() == 1 ) //Percent
					textMidValue.setText("50");
				else if( comboMidType.getSelectionIndex() == 2 ) //Percentile
					textMidValue.setText("50");
				editorModified();
			}
		});

		textMaxValue = new Text(compositeTabColorScale, SWT.BORDER);
		textMaxValue.setEnabled(false);
		GridData gd_textMaxValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_textMaxValue.widthHint = 100;
		textMaxValue.setLayoutData(gd_textMaxValue);
		colorScaleControls.add(textMaxValue);

		textMaxValue.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		// Maximum Value verify listener
		textMaxValue.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(comboMaxType, e);
			}
		});
		
		comboMaxType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				enableColorScaleMaximumControls();
				
				if( comboMaxType.getSelectionIndex() == 1 )//Number
					textMaxValue.setText("0");
				else if( comboMaxType.getSelectionIndex() == 2 ) //Percent
					textMaxValue.setText("100");
				else if( comboMaxType.getSelectionIndex() == 3 ) //Percentile
					textMaxValue.setText("90");
				editorModified();
			}
		});

		//row 7
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		
		//row 8
		Label lblColorScaleColor = new Label(compositeTabColorScale, SWT.NONE);
		lblColorScaleColor.setAlignment(SWT.CENTER);
		lblColorScaleColor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblColorScaleColor.setText("Color:");
		
		txtMinColor = new Text(compositeTabColorScale, SWT.READ_ONLY);
		GridData gd_txtMinColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMinColor.widthHint = 100;
		txtMinColor.setLayoutData(gd_txtMinColor);
		txtMinColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), new RGB(246, 67, 70)));			
	
		final Button btnMinColor = new Button(compositeTabColorScale, SWT.NONE);
		GridData gd_btnMinColor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnMinColor.widthHint = 20;
		btnMinColor.setLayoutData(gd_btnMinColor);
		colorScaleControls.add(btnMinColor);
		
		btnMinColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
		       ColorDialog dlg = new ColorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		        // Set the selected color in the dialog from
		        // user's selected color
				dlg.setRGBs(new RGB[] {txtMinColor.getBackground().getRGB()});
		        dlg.setRGB(txtMinColor.getBackground().getRGB());

		        // Change the title bar text
		        dlg.setText("Choose a Color");

		        // Open the dialog and retrieve the selected color
		        RGB rgb = dlg.open();
		        if (rgb != null) {
		          // Dispose the old color, create the
		          // new one, and set into the label
		          txtMinColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), rgb));			
		          
		        }
		        
				editorModified();
			}
		});
		btnMinColor.setText("...");
		
		txtMidColor = new Text(compositeTabColorScale, SWT.READ_ONLY);
		txtMidColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), new RGB(255,235,132)));			
		txtMidColor.setEditable(false);
		GridData gd_txtMidColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMidColor.widthHint = 100;
		txtMidColor.setLayoutData(gd_txtMidColor);
		txtMidColor.setVisible(false);
		
		btnMidColor = new Button(compositeTabColorScale, SWT.NONE);
		GridData gd_btnMidColor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnMidColor.widthHint = 20;
		btnMidColor.setLayoutData(gd_btnMidColor);
		btnMidColor.setVisible(false);
		colorScaleControls.add(btnMidColor);
		
		btnMidColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
		       ColorDialog dlg = new ColorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		        // Set the selected color in the dialog from
		        // user's selected color
				dlg.setRGBs(new RGB[] {txtMidColor.getBackground().getRGB()});
		        dlg.setRGB(txtMidColor.getBackground().getRGB());

		        // Change the title bar text
		        dlg.setText("Choose a Color");

		        // Open the dialog and retrieve the selected color
		        RGB rgb = dlg.open();
		        if (rgb != null) {
		          // Dispose the old color, create the
		          // new one, and set into the label
		          txtMidColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), rgb));			
		          
		        }
				editorModified();
			}
		});
		btnMidColor.setText("...");
		
		txtMaxColor = new Text(compositeTabColorScale, SWT.READ_ONLY);
		txtMaxColor.setEditable(false);
		txtMaxColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), new RGB(99, 190, 123)));			
		GridData gd_txtMaxColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMaxColor.widthHint = 100;
		txtMaxColor.setLayoutData(gd_txtMaxColor);
		
		final Button btnMaxColor = new Button(compositeTabColorScale, SWT.NONE);
		GridData gd_btnMaxColor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnMaxColor.widthHint = 20;
		btnMaxColor.setLayoutData(gd_btnMaxColor);
		colorScaleControls.add(btnMaxColor);
		
		btnMaxColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ColorDialog dlg = new ColorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		        // Set the selected color in the dialog from
		        // user's selected color
				dlg.setRGBs(new RGB[] {txtMaxColor.getBackground().getRGB()});
		        dlg.setRGB(txtMaxColor.getBackground().getRGB());

		        // Change the title bar text
		        dlg.setText("Choose a Color");

		        // Open the dialog and retrieve the selected color
		        RGB rgb = dlg.open();
		        if (rgb != null) {
		          // Dispose the old color, create the
		          // new one, and set into the label
		          txtMaxColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), rgb));			
		          
		        }
		        
				editorModified();
			}
		});
		btnMaxColor.setText("...");
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		new Label(compositeTabColorScale, SWT.NONE);
		
		comboNumOfColors.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableColorScaleMidPointControls();
				if( comboMidType.getText().equals(CriteriaType.Percent.toString())
								|| comboMidType.getText().equals(CriteriaType.Percentile.toString() ))  {
					textMidValue.setText("50");
				}
				editorModified();
			}
		});
		
		tabControlMap.put(StyleType.ColorScale.index, colorScaleControls);
		
	}
	
	private void createIconTab() {
		//Icon Tab
		List<Control> iconControls = new ArrayList<Control>();
		
		tabIcon = new TabItem(tabFolder, SWT.NONE);
		tabIcon.setText("Icon");
		
		Composite compositeTabMargins = new Composite(tabFolder, SWT.BORDER);
		tabIcon.setControl(compositeTabMargins);
		GridLayout gl_compositeTabMargins = new GridLayout(4, false);
		gl_compositeTabMargins.marginTop = 10;
		gl_compositeTabMargins.marginLeft = 10;
		compositeTabMargins.setLayout(gl_compositeTabMargins);
		
		Label lblIconStyle = new Label(compositeTabMargins, SWT.NONE);
		lblIconStyle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIconStyle.setText("Icon Style:");
		
		imgCombo = new ImageCombo(compositeTabMargins, SWT.BORDER );
		imgCombo.setVisibleItemCount(7);
		imgCombo.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.NORMAL));
		imgCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/ThreeColorStopLightStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/FourColorStopLightStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/ThreeColorArrowStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/FourColorArrowStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/FiveColorArrowsStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/IndicatorCheckBangStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/IndicatorThreeFlagsStrip.png"), "");
		imgCombo.add(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/IndicatorCheckBangCircleStrip.png"), "");
		iconControls.add(imgCombo);
		
		new Label(compositeTabMargins, SWT.NONE);
		new Label(compositeTabMargins, SWT.NONE);
		new Label(compositeTabMargins, SWT.NONE);
		new Label(compositeTabMargins, SWT.NONE);
		
		Label lblIcon = new Label(compositeTabMargins, SWT.NONE);
		lblIcon.setAlignment(SWT.CENTER);
		lblIcon.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIcon.setText("Icon");
		new Label(compositeTabMargins, SWT.NONE);
		
		Label lblIconValue = new Label(compositeTabMargins, SWT.NONE);
		lblIconValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIconValue.setText("Value");
		
		Label lblIconType = new Label(compositeTabMargins, SWT.NONE);
		lblIconType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIconType.setText("Type");
		
		lblIcon_1 = new Label(compositeTabMargins, SWT.NONE);
		lblIcon_1.setAlignment(SWT.CENTER);
		lblIcon_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		cmbComparive1 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbComparive1.setItems(new String[] {">=", ">"});
		cmbComparive1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbComparive1.setText(">=");
		iconControls.add(cmbComparive1);
		
		cmbComparive1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue1 = new Text(compositeTabMargins, SWT.BORDER);
		textIconValue1.setText("67");
		GridData gd_textIconValue1 = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_textIconValue1.widthHint = 120;
		textIconValue1.setLayoutData(gd_textIconValue1);
		iconControls.add(textIconValue1);
		
		cmbIconType1 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		GridData gd_cmbIconType1 = new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1);
		gd_cmbIconType1.widthHint = 120;
		cmbIconType1.setLayoutData(gd_cmbIconType1);
		cmbIconType1.setItems(new String[] {"Number", "Percent", "Percentile"});
		cmbIconType1.setText("Percent");
		iconControls.add(cmbIconType1);
		
		cmbIconType1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue1.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textIconValue1.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbIconType1, e);
			}
		});
		
		lblIcon_2 = new Label(compositeTabMargins, SWT.NONE);
		lblIcon_2.setAlignment(SWT.CENTER);
		lblIcon_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		cmbComparive2 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbComparive2.setItems(new String[] {">=", ">"});
		cmbComparive2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbComparive2.setText(">=");
		iconControls.add(cmbComparive2);
		
		cmbComparive2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue2 = new Text(compositeTabMargins, SWT.BORDER);
		textIconValue2.setText("33");
		textIconValue2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		iconControls.add(textIconValue2);
		
		cmbIconType2 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbIconType2.setItems(new String[] {"Number", "Percent", "Percentile"});
		cmbIconType2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbIconType2.setText("Percent");
		iconControls.add(cmbIconType2);
		
		cmbIconType2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue2.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textIconValue2.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbIconType2, e);
			}
		});
		
		lblIcon_3 = new Label(compositeTabMargins, SWT.NONE);
		lblIcon_3.setAlignment(SWT.CENTER);
		lblIcon_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		cmbComparive3 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbComparive3.setItems(new String[] {">=", ">"});
		cmbComparive3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbComparive3.setText(">=");
		iconControls.add(cmbComparive3);
		
		cmbComparive3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue3 = new Text(compositeTabMargins, SWT.BORDER);
		textIconValue3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		iconControls.add(textIconValue3);
		
		cmbIconType3 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbIconType3.setItems(new String[] {"Number", "Percent", "Percentile"});
		cmbIconType3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbIconType3.setText("Percent");
		iconControls.add(cmbIconType3);
		
		cmbIconType3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue3.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textIconValue3.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbIconType3, e);
			}
		});
		
		lblIcon_4 = new Label(compositeTabMargins, SWT.NONE);
		lblIcon_4.setImage(null);
		lblIcon_4.setAlignment(SWT.CENTER);
		lblIcon_4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		cmbComparive4 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbComparive4.setItems(new String[] {">=", ">"});
		cmbComparive4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbComparive4.setText(">=");
		iconControls.add(cmbComparive4);
		
		cmbComparive4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		textIconValue4 = new Text(compositeTabMargins, SWT.BORDER);
		textIconValue4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		iconControls.add(textIconValue4);
		
		cmbIconType4 = new Combo(compositeTabMargins, SWT.READ_ONLY);
		cmbIconType4.setItems(new String[] {"Number", "Percent", "Percentile"});
		cmbIconType4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbIconType4.setText("Percent");
		iconControls.add(cmbIconType4);
		
		cmbIconType4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		tabControlMap.put(StyleType.Icon.index, iconControls);
		
		textIconValue4.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		textIconValue4.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbIconType4, e);
			}
		});
		lblIcon_5 = new Label(compositeTabMargins, SWT.NONE);
		lblIcon_5.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIcon_5.setImage(null);
		lblIcon_5.setAlignment(SWT.CENTER);
		new Label(compositeTabMargins, SWT.NONE);
		new Label(compositeTabMargins, SWT.NONE);
		new Label(compositeTabMargins, SWT.NONE);
		
		//set default selection for Icon Style Combo
		imgCombo.select(0);
		lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Green.png"));
		lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Yellow.png"));
		lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Red.png"));
		cmbComparive3.setVisible(false);
		textIconValue3.setVisible(false);
		cmbIconType3.setVisible(false);
		lblIcon_4.setVisible(false);
		cmbComparive4.setVisible(false);
		textIconValue4.setVisible(false);
		cmbIconType4.setVisible(false);
		lblIcon_5.setVisible(false);
		
		imgCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				updateIconControls();
				
				switch( imgCombo.getSelectionIndex() ) {
					case 0: //3 color stop light
						textIconValue1.setText("67");
						textIconValue2.setText("33");
						break;
					case 1: //4 color stop light
						textIconValue1.setText("75");
						textIconValue2.setText("50");
						textIconValue3.setText("25");
						break;
					case 2: //3 color arrow
						textIconValue1.setText("67");
						textIconValue2.setText("33");
						break;
					case 3: //4 color arrow
						textIconValue1.setText("75");
						textIconValue2.setText("50");
						textIconValue3.setText("25");
						break;
					case 4: //5 color arrow
						textIconValue1.setText("80");
						textIconValue2.setText("60");
						textIconValue3.setText("40");
						textIconValue4.setText("20");
						break;
					case 5: //3 color indicator check
						textIconValue1.setText("67");
						textIconValue2.setText("33");
						break;
					case 6: //3 color indicator flag
						textIconValue1.setText("67");
						textIconValue2.setText("33");
						break;
					case 7: //3 color indicator circle
						textIconValue1.setText("67");
						textIconValue2.setText("33");
						break;
					default:
						break;
				}
				
				editorModified();
			}
		});
		
	}

	private void createDataBarsTab() {

		//Data Bars Tab
		List<Control> dataBarsControls = new ArrayList<Control>();
		
		tabDataBars = new TabItem(tabFolder, SWT.NONE);
		tabDataBars.setText("Data Bars");
		
		Composite composite = new Composite(tabFolder, SWT.BORDER);
		tabDataBars.setControl(composite);
		composite.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		GridLayout gl_composite = new GridLayout(4, false);
		gl_composite.marginRight = 5;
		gl_composite.marginLeft = 10;
		gl_composite.marginTop = 10;
		gl_composite.horizontalSpacing = 3;
		composite.setLayout(gl_composite);
		new Label(composite, SWT.NONE);
		
		Label lblMinBar = new Label(composite, SWT.CENTER);
		lblMinBar.setAlignment(SWT.LEFT);
		lblMinBar.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		GridData gd_lblMinBar = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblMinBar.widthHint = 120;
		lblMinBar.setLayoutData(gd_lblMinBar);
		lblMinBar.setText("Minimum");
		
		Label lblMaxBar = new Label(composite, SWT.CENTER);
		lblMaxBar.setAlignment(SWT.LEFT);
		lblMaxBar.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		GridData gd_lblMaxBar = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblMaxBar.widthHint = 120;
		lblMaxBar.setLayoutData(gd_lblMaxBar);
		lblMaxBar.setText("Maximum");
		
		Label labelBarType = new Label(composite, SWT.NONE);
		labelBarType.setAlignment(SWT.CENTER);
		labelBarType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		labelBarType.setText("Type:");
		labelBarType.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		
		cmbMinBarType = new Combo(composite, SWT.READ_ONLY);
		cmbMinBarType.setItems(new String[] {"Automatic", "Lowest Value", "Number", "Percent", "Percentile"});
		GridData gd_cmbMinBarType = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbMinBarType.widthHint = 160;
		cmbMinBarType.setLayoutData(gd_cmbMinBarType);
		cmbMinBarType.setText("Automatic");
		dataBarsControls.add(cmbMinBarType);
		
		cmbMaxBarType = new Combo(composite, SWT.READ_ONLY);
		cmbMaxBarType.setItems(new String[] {"Automatic", "Highest Value", "Number", "Percent", "Percentile"});
		GridData gd_cmbMaxBarType = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_cmbMaxBarType.widthHint = 160;
		cmbMaxBarType.setLayoutData(gd_cmbMaxBarType);
		cmbMaxBarType.setText("Automatic");
		dataBarsControls.add(cmbMaxBarType);
		
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		Label lblBarValue = new Label(composite, SWT.NONE);
		lblBarValue.setAlignment(SWT.CENTER);
		lblBarValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblBarValue.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblBarValue.setText("Value:");
		
		txtMinBarValue = new Text(composite, SWT.BORDER);
		txtMinBarValue.setEnabled(false);
		GridData gd_txtMinBarValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMinBarValue.widthHint = 120;
		txtMinBarValue.setLayoutData(gd_txtMinBarValue);
		dataBarsControls.add(txtMinBarValue);
		
		txtMaxBarValue = new Text(composite, SWT.BORDER);
		txtMaxBarValue.setEnabled(false);
		GridData gd_txtMaxBarValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_txtMaxBarValue.widthHint = 120;
		txtMaxBarValue.setLayoutData(gd_txtMaxBarValue);
		dataBarsControls.add(txtMaxBarValue);
		
		cmbMinBarType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				enableDataBarsMinControls();
				
				if( cmbMinBarType.getSelectionIndex() != 0 && cmbMinBarType.getSelectionIndex() != 1) { 
					txtMinBarValue.setText("0");
				}
				
				editorModified();
			}
		});
		
		txtMinBarValue.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		txtMinBarValue.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbMinBarType, e);
			}
		});
		
		cmbMaxBarType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableDataBarsMaxControls();

				if( cmbMaxBarType.getSelectionIndex() == 2 ) //"Number"
					txtMaxBarValue.setText("0");
				else if( cmbMaxBarType.getSelectionIndex() == 3 ) //"Percent"
					txtMaxBarValue.setText("100");
				else if( cmbMaxBarType.getSelectionIndex() == 4 ) //"Percentile"
					txtMaxBarValue.setText("90");
				
				editorModified();
			}
		});
		
		txtMaxBarValue.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorModified();
			}
		});
		txtMaxBarValue.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyValueText(cmbMaxBarType,e);
			}
		});
		
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		Label lblBarAppearance = new Label(composite, SWT.NONE);
		lblBarAppearance.setAlignment(SWT.CENTER);
		lblBarAppearance.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblBarAppearance.setFont(SWTResourceManager.getFont("Tahoma", 10, SWT.NORMAL));
		lblBarAppearance.setText("Bar Appearance:");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		new Label(composite, SWT.NONE);
		Label lblFill = new Label(composite, SWT.NONE);
		GridData gd_lblFill = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblFill.widthHint = 120;
		lblFill.setLayoutData(gd_lblFill);
		lblFill.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblFill.setText("Fill");
		
		Label lblColorMinBar = new Label(composite, SWT.NONE);
		GridData gd_lblColorMinBar = new GridData(SWT.FILL, SWT.CENTER, false, false,1, 1);
		gd_lblColorMinBar.widthHint = 100;
		lblColorMinBar.setLayoutData(gd_lblColorMinBar);
		lblColorMinBar.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblColorMinBar.setText("Color");
		new Label(composite, SWT.NONE);
		
		new Label(composite, SWT.NONE);
		cmbFill = new Combo(composite, SWT.READ_ONLY);
		cmbFill.setItems(new String[] {"Gradient", "Solid"});
		GridData gd_cmbFill = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbFill.widthHint = 120;
		cmbFill.setLayoutData(gd_cmbFill);
		cmbFill.setText("Solid");
		dataBarsControls.add(cmbFill);
		
		cmbFill.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		txtFillColor = new Text(composite, SWT.READ_ONLY);
		txtFillColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), new RGB(99, 142, 198)));			
		txtFillColor.setEditable(false);
		GridData gd_txtFillColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtFillColor.widthHint = 160;
		txtFillColor.setLayoutData(gd_txtFillColor);
		
		Button btnFillColor = new Button(composite, SWT.NONE);
		GridData gd_btnFillColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_btnFillColor.widthHint = 20;
		btnFillColor.setLayoutData(gd_btnFillColor);
		btnFillColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
		       ColorDialog dlg = new ColorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		        // Set the selected color in the dialog from
		        // user's selected color
				dlg.setRGBs(new RGB[] {txtFillColor.getBackground().getRGB()});
		        dlg.setRGB(txtFillColor.getBackground().getRGB());

		        // Change the title bar text
		        dlg.setText("Choose a Color");

		        // Open the dialog and retrieve the selected color
		        RGB rgb = dlg.open();
		        if (rgb != null) {
		          // Dispose the old color, create the
		          // new one, and set into the label
		        	txtFillColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), rgb));			
		          
		        }
		        
				editorModified();
			}
		});
		btnFillColor.setText("...");
		dataBarsControls.add(btnFillColor);
		new Label(composite, SWT.NONE);
		
		btnFillColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		Label lblBorder = new Label(composite, SWT.NONE);
		GridData gd_lblBorder = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblBorder.widthHint = 120;
		lblBorder.setLayoutData(gd_lblBorder);
		lblBorder.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblBorder.setText("Border");
		
		Label lblColorMaxBar = new Label(composite, SWT.NONE);
		GridData gd_lblColorMaxBar = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblColorMaxBar.widthHint = 100;
		lblColorMaxBar.setLayoutData(gd_lblColorMaxBar);
		lblColorMaxBar.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblColorMaxBar.setText("Color");
		new Label(composite, SWT.NONE);
		
		new Label(composite, SWT.NONE);
		cmbBorder = new Combo(composite, SWT.READ_ONLY);
		cmbBorder.setItems(new String[] {"None", "Solid"});
		GridData gd_cmbBorder = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbBorder.widthHint = 120;
		cmbBorder.setLayoutData(gd_cmbBorder);
		cmbBorder.setText("None");
		dataBarsControls.add(cmbBorder);
		
		cmbBorder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		txtBorderColor = new Text(composite, SWT.READ_ONLY);
		txtBorderColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), new RGB(0, 0, 0)));	
		txtBorderColor.setEditable(false);
		GridData gd_txtBorderColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtBorderColor.widthHint = 160;
		txtBorderColor.setLayoutData(gd_txtBorderColor);
		
		Button btnBorderColor = new Button(composite, SWT.NONE);
		GridData gd_btnBorderColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_btnBorderColor.widthHint = 20;
		btnBorderColor.setLayoutData(gd_btnBorderColor);
		btnBorderColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
		       ColorDialog dlg = new ColorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		        // Set the selected color in the dialog from
		        // user's selected color
				dlg.setRGBs(new RGB[] {txtBorderColor.getBackground().getRGB()});
		        dlg.setRGB(txtBorderColor.getBackground().getRGB());

		        // Change the title bar text
		        dlg.setText("Choose a Color");

		        // Open the dialog and retrieve the selected color
		        RGB rgb = dlg.open();
		        if (rgb != null) {
		          // Dispose the old color, create the
		          // new one, and set into the label
		        	txtBorderColor.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(), rgb));			
		          
		        }
		        
				editorModified();
			}
		});
		btnBorderColor.setText("...");
		dataBarsControls.add(btnBorderColor);
		new Label(composite, SWT.NONE);
		
		btnBorderColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		Label lblBarDirection = new Label(composite, SWT.NONE);
		GridData gd_lblBarDirection = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblBarDirection.widthHint = 120;
		lblBarDirection.setLayoutData(gd_lblBarDirection);
		lblBarDirection.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		lblBarDirection.setText("Bar Direction");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		cmbBarDirection = new Combo(composite, SWT.READ_ONLY);
		cmbBarDirection.setItems(new String[] {"Context", "Right-to-Left", "Left-to-Right"});
		GridData gd_cmbBarDirection = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbBarDirection.widthHint = 120;
		cmbBarDirection.setLayoutData(gd_cmbBarDirection);
		cmbBarDirection.setText("Context");
		dataBarsControls.add(cmbBarDirection);
		
		cmbBarDirection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorModified();
			}
		});
		
		tabControlMap.put(StyleType.DataBars.index, dataBarsControls);
		
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
//		tabFolder.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				
//				enableTabControls();
//				
//			}
//		});
//		
		new Label(composite, SWT.NONE);		
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
	}

	protected void enableDataBarsMaxControls() {
		if( cmbMaxBarType.getText().equals(CriteriaType.Automatic.getDisplayText()) 
				|| cmbMaxBarType.getText().equals(CriteriaType.Highest.getDisplayText()) ) { 
			txtMaxBarValue.setEnabled(false);
			txtMaxBarValue.setText("");
		}
		else  { 
			txtMaxBarValue.setEnabled(true);
		}
	}

	private void enableDataBarsMinControls() {
		if( cmbMinBarType.getText().equals(CriteriaType.Automatic.getDisplayText()) 
				|| cmbMinBarType.getText().equals(CriteriaType.Lowest.getDisplayText()) ) { 
			txtMinBarValue.setEnabled(false);
			txtMinBarValue.setText("");
		}
		else  { 
			txtMinBarValue.setEnabled(true);
		}
	}

	protected void enableTabControls() {
		if( tabFolder.getSelectionIndex() == comboStyleType.getSelectionIndex() ) {
			for( Control control : tabControlMap.get(tabFolder.getSelectionIndex()) ) {
				control.setEnabled(true);
			}
			textMinValue.setEnabled( ! comboMinType.getText().equals("Lowest Value") );
			textMaxValue.setEnabled( ! comboMaxType.getText().equals("Highest Value") );
			txtMinBarValue.setEnabled( ! cmbMinBarType.getText().equals("Lowest Value") && ! cmbMinBarType.getText().equals("Automatic"));
			txtMaxBarValue.setEnabled( ! cmbMaxBarType.getText().equals("Highest Value") && ! cmbMinBarType.getText().equals("Automatic"));
		}
		else {
			for( Control control : tabControlMap.get(tabFolder.getSelectionIndex()) ) {
				control.setEnabled(false);
			}
		}
	}

	protected void enableColorScaleMinimumControls() {
		if( comboMinType.getText().equals(CriteriaType.Lowest.getDisplayText())) { //"Lowest Value"
			textMinValue.setEnabled(false);
			textMinValue.setText("");
		}
		else {
			textMinValue.setEnabled(true);
		}
	}

	protected void enableColorScaleMaximumControls() {
		if( comboMaxType.getText().equals(CriteriaType.Highest.getDisplayText()) ) { //"Highest Value"
			textMaxValue.setEnabled(false);
			textMaxValue.setText("");
		}
		else {
			textMaxValue.setEnabled(true);
		}
	}

	protected void enableColorScaleMidPointControls() {
		if( comboNumOfColors.getText().equals(ColorScaleColorNumType.Two_Color.getDisplayText()) ) {
			lblMidScale.setVisible(false);
			comboMidType.setVisible(false);
			textMidValue.setVisible(false);
			txtMidColor.setVisible(false);
			btnMidColor.setVisible(false);
		}
		else { //if( comboNumOfColors.getSelectionIndex() == 1 ) {
			lblMidScale.setVisible(true);
			comboMidType.setVisible(true);
			textMidValue.setVisible(true);
			txtMidColor.setVisible(true);
			btnMidColor.setVisible(true);
		}
	}

	protected void updateIconControls() {
		switch( imgCombo.getSelectionIndex() ) {
			case 0: //3 color stop light
				//hide 4 & 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Green.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Yellow.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Red.png"));
				cmbComparive3.setVisible(false);
				textIconValue3.setVisible(false);
				cmbIconType3.setVisible(false);
				lblIcon_4.setVisible(false);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 1: //4 color stop light
				//hide 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Green.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Yellow.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Red.png"));
				lblIcon_4.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Black.png"));
				cmbComparive3.setVisible(true);
				textIconValue3.setVisible(true);
				cmbIconType3.setVisible(true);
				lblIcon_4.setVisible(true);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 2: //3 color arrow
				//hide 4 & 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenArrow.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/YellowArrow.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/RedArrow.png"));
				cmbComparive3.setVisible(false);
				textIconValue3.setVisible(false);
				cmbIconType3.setVisible(false);
				lblIcon_4.setVisible(false);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 3: //4 color arrow
				//hide 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenArrow.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/UpYellowArrow.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/DownYellowArrow.png"));
				lblIcon_4.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/RedArrow.png"));
				cmbComparive3.setVisible(true);
				textIconValue3.setVisible(true);
				cmbIconType3.setVisible(true);
				lblIcon_4.setVisible(true);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 4: //5 color arrow
				//show all
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenArrow.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/UpYellowArrow.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/YellowSideArrow.png"));
				lblIcon_4.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/DownYellowArrow.png"));
				lblIcon_5.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/RedArrow.png"));
				cmbComparive3.setVisible(true);
				textIconValue3.setVisible(true);
				cmbIconType3.setVisible(true);
				lblIcon_4.setVisible(true);
				cmbComparive4.setVisible(true);
				textIconValue4.setVisible(true);
				cmbIconType4.setVisible(true);
				lblIcon_5.setVisible(true);
				break;
			case 5: //3 color indicator check
				//hide 4 & 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenCheck.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/YellowExclamationPoint.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Red-x.png"));
				cmbComparive3.setVisible(false);
				textIconValue3.setVisible(false);
				cmbIconType3.setVisible(false);
				lblIcon_4.setVisible(false);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 6: //3 color indicator flag
				//hide 4 & 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenFlag.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/YellowFlag.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/RedFlag.png"));
				cmbComparive3.setVisible(false);
				textIconValue3.setVisible(false);
				cmbIconType3.setVisible(false);
				lblIcon_4.setVisible(false);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			case 7: //3 color indicator circle
				//hide 4 & 5
				lblIcon_1.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/GreenCheckCircle.png"));
				lblIcon_2.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/YellowExclmationCircle.png"));
				lblIcon_3.setImage(ResourceManager.getPluginImage(MenuPlugin.getDefault(), "icons/Red-x-Circle.png"));
				cmbComparive3.setVisible(false);
				textIconValue3.setVisible(false);
				cmbIconType3.setVisible(false);
				lblIcon_4.setVisible(false);
				cmbComparive4.setVisible(false);
				textIconValue4.setVisible(false);
				cmbIconType4.setVisible(false);
				lblIcon_5.setVisible(false);
				break;
			default:
				break;
		}
	}

	/**
	 * Populates the control with the default/startup values.
	 */
	/**
	 * 
	 */
	private void populateControlValues() {
		if( condStyleEditorInput.isNew() ) {
			textStyleName.setText("");
			comboStyleType.select(0);
			isDirty = true;
		}
		else if( condStyleEditorInput.isCopy() ) {
			textStyleName.setText("Copy of " + condStyle.getName());
			comboStyleType.select(getStyleTypeFromConditionaStyle().index);
			isDirty = true;
		}
		else {
			if( condStyle != null ) {
				textStyleName.setText(condStyle.getName());
				comboStyleType.select(getStyleTypeFromConditionaStyle().index);
			}
			isDirty = false;
		}
		
	}
	
	private StyleType getStyleTypeFromConditionaStyle() {
		if( condStyle instanceof ColorScale ) {
			return StyleType.ColorScale; 
		}
		else if( condStyle instanceof IconStyle ) {
			return StyleType.Icon;
		}
		else if( condStyle instanceof DataBars ) {
			return StyleType.DataBars;
		}
		return null;
	}
	
	public void loadConditionalStyle() {
		if( condStyle != null ) {
			if( condStyle instanceof ColorScale ) {
				
				comboStyleType.select(StyleType.ColorScale.index);
				showHideTabs(StyleType.ColorScale.index);
				
				if( ((ColorScale) condStyle).getScaleType() != null ) {
					if( ((ColorScale) condStyle).getScaleType().equals(ColorScaleColorNumType.Two_Color) ) {
						
						comboNumOfColors.setText(ColorScaleColorNumType.Two_Color.getDisplayText());
						
					}
					else {
						comboNumOfColors.setText(ColorScaleColorNumType.Three_Color.getDisplayText());
						
						ColorScaleCriteria midCriteria = ((ColorScale) condStyle).getMidpoint();
						if( midCriteria != null && midCriteria.getCriteriaTypeValue().equals(CriteriaTypeValues.Midpoint) ) {
							comboMidType.setText(midCriteria.getCriteriaType().toString());
							textMidValue.setText(df.format(midCriteria.getCriteriaValue()) );
							
							if( midCriteria.getCriteriaHexColor() != null ) {
								txtMidColor.setBackground(ColorsUtil.hexStringToColor(
										PlatformUI.getWorkbench().getDisplay(), 
										midCriteria.getCriteriaHexColor()) );
							}
						}
					}
					
					ColorScaleCriteria minCriteria = ((ColorScale) condStyle).getMinimum();
					if( minCriteria != null && minCriteria.getCriteriaTypeValue().equals(CriteriaTypeValues.Minimum) ) {
						if( minCriteria.getCriteriaType().equals(CriteriaType.Lowest) ) {
							comboMinType.setText(CriteriaType.Lowest.getDisplayText());
						}
						else {
							comboMinType.setText(minCriteria.getCriteriaType().toString());
							textMinValue.setText(df.format(minCriteria.getCriteriaValue()) );
						}
						
						if( minCriteria.getCriteriaHexColor() != null ) {
							txtMinColor.setBackground(ColorsUtil.hexStringToColor(
									PlatformUI.getWorkbench().getDisplay(), 
									minCriteria.getCriteriaHexColor()) );
						}
					}
					
					ColorScaleCriteria maxCriteria = ((ColorScale) condStyle).getMaximum();
					if( maxCriteria != null && maxCriteria.getCriteriaTypeValue().equals(CriteriaTypeValues.Maximum) ) {
						if( maxCriteria.getCriteriaType().equals(CriteriaType.Highest) ) {
							comboMaxType.setText(CriteriaType.Highest.getDisplayText());
						}
						else {
							comboMaxType.setText(maxCriteria.getCriteriaType().toString());
							textMaxValue.setText(df.format(maxCriteria.getCriteriaValue()) );
						}
						
						if( maxCriteria.getCriteriaHexColor() != null ) {
							txtMaxColor.setBackground(ColorsUtil.hexStringToColor(
									PlatformUI.getWorkbench().getDisplay(), 
									maxCriteria.getCriteriaHexColor()) );
						}
					}
				}
			}
			else if( condStyle instanceof IconStyle ) {
				
				comboStyleType.select(StyleType.Icon.index);
				showHideTabs(StyleType.Icon.index);
				
//				private ImageCombo imgCombo;
//				private Combo cmbComparive1,  cmbComparive2,  cmbComparive3,  cmbComparive4, 
//				private Combo mbIconType1, cmbIconType2, cmbIconType3, cmbIconType4;
//				private Text textIconValue1, textIconValue2, textIconValue3, textIconValue4;
				IconStyleType iconType = ((IconStyle) condStyle).getIconStyle();
				if( iconType != null ) {
					if( iconType.equals(IconStyleType.ThreeColorArrows) ) {
						imgCombo.select(0);
					}
					else if( iconType.equals(IconStyleType.FourColorStoplight) ) {
						imgCombo.select(1);
					}
					else if( iconType.equals(IconStyleType.ThreeColorArrows) ) {
						imgCombo.select(2);
					}
					else if( iconType.equals(IconStyleType.FourColorArrows) ) {
						imgCombo.select(3);
					}
					else if( iconType.equals(IconStyleType.FiveColorArrows) ) {
						imgCombo.select(4);
					}
					else if( iconType.equals(IconStyleType.IndicatorCheckBang) ) {
						imgCombo.select(5);
					}
					else if( iconType.equals(IconStyleType.IndicatorThreeFlags) ) {
						imgCombo.select(6);
					}
					else if( iconType.equals(IconStyleType.IndicatorCheckBangCircle) ) {
						imgCombo.select(7);
					}
					
					IconCriteria iconCriteria1 = ((IconStyle) condStyle).getCriteriaOne();
					if( iconCriteria1 != null ) {
						cmbIconType1.setText(iconCriteria1.getCriteriaType().toString());
						textIconValue1.setText(df.format(iconCriteria1.getCriteriaValue()));
						cmbComparive1.setText(iconCriteria1.getCriteriaOperator());
					}
					
					IconCriteria iconCriteria2 = ((IconStyle) condStyle).getCriteriaTwo();
					if( iconCriteria2 != null ) {
						cmbIconType2.setText(iconCriteria2.getCriteriaType().toString());
						textIconValue2.setText(df.format(iconCriteria2.getCriteriaValue()));
						cmbComparive2.setText(iconCriteria2.getCriteriaOperator());
					}
					
					if( ((IconStyle) condStyle).getIconStyle().equals(IconStyleType.FourColorStoplight) 
							|| ((IconStyle) condStyle).getIconStyle().equals(IconStyleType.FourColorArrows) ) { 
						
						IconCriteria iconCriteria3 = ((IconStyle) condStyle).getCriteriaThree();
						if( iconCriteria3 != null ) {
							cmbIconType3.setText(iconCriteria3.getCriteriaType().toString());
							textIconValue3.setText(df.format(iconCriteria3.getCriteriaValue()));
							cmbComparive3.setText(iconCriteria3.getCriteriaOperator());
						}
						
					}
					else if( ((IconStyle) condStyle).getIconStyle().equals(IconStyleType.FiveColorArrows) ) {
						
						IconCriteria iconCriteria3 = ((IconStyle) condStyle).getCriteriaThree();
						if( iconCriteria3 != null ) {
							cmbIconType3.setText(iconCriteria3.getCriteriaType().toString());
							textIconValue3.setText(df.format(iconCriteria3.getCriteriaValue()));
							cmbComparive3.setText(iconCriteria3.getCriteriaOperator());
						}
						
						IconCriteria iconCriteria4 = ((IconStyle) condStyle).getCriteriaFour();
						if( iconCriteria4 != null ) {
							cmbIconType4.setText(iconCriteria4.getCriteriaType().toString());
							textIconValue4.setText(df.format(iconCriteria4.getCriteriaValue()));
							cmbComparive4.setText(iconCriteria4.getCriteriaOperator());
						}
						
					}
				}
			}
			else if( condStyle instanceof DataBars ) {
				
				comboStyleType.select(StyleType.DataBars.index);
				showHideTabs(StyleType.DataBars.index);
				
				DataBarCriteria minCriteria = ((DataBars) condStyle).getMinimum();
				if( minCriteria != null && minCriteria.getCriteriaTypeValue().equals(CriteriaTypeValues.Minimum) ) {
					if( minCriteria.getCriteriaType().equals(CriteriaType.Lowest) )
						cmbMinBarType.setText(CriteriaType.Lowest.getDisplayText()); 
					else
						cmbMinBarType.setText(minCriteria.getCriteriaType().toString());
					if( minCriteria.getCriteriaType().equals(CriteriaType.Lowest)
							|| minCriteria.getCriteriaType().equals(CriteriaType.Automatic) ) {
						txtMinBarValue.setText("");
					}
					else {
						txtMinBarValue.setText(df.format(minCriteria.getCriteriaValue()));
					}
				}
				
				DataBarCriteria maxCriteria = ((DataBars) condStyle).getMaximum();
				if( maxCriteria != null && maxCriteria.getCriteriaTypeValue().equals(CriteriaTypeValues.Maximum) ) {
					if( maxCriteria.getCriteriaType().equals(CriteriaType.Highest) )
						cmbMaxBarType.setText(CriteriaType.Highest.getDisplayText()); 
					else
						cmbMaxBarType.setText(maxCriteria.getCriteriaType().toString());
					if( minCriteria.getCriteriaType().equals(CriteriaType.Highest)
							|| minCriteria.getCriteriaType().equals(CriteriaType.Automatic) ) {
						txtMinBarValue.setText("");
					}
					else {
						txtMaxBarValue.setText(df.format(maxCriteria.getCriteriaValue()));
					}
				}
				
				DataBarAppearance barAppearance = ((DataBars) condStyle).getAppearance();
				if( barAppearance != null ) {
					cmbFill.setText(barAppearance.getFillType().toString());
					txtFillColor.setBackground(ColorsUtil.hexStringToColor(
							PlatformUI.getWorkbench().getDisplay(), 
							barAppearance.getFillHexColor()) );
					cmbBorder.setText(barAppearance.getBorderType().toString());
					txtBorderColor.setBackground(ColorsUtil.hexStringToColor(
							PlatformUI.getWorkbench().getDisplay(), 
							barAppearance.getBorderHexColor()));
					cmbBarDirection.setText(barAppearance.getBarDirection().toString());
					if( barAppearance.getBarDirection().equals(DataBarAppearanceBarDirectionType.Right_To_Left) )
						cmbBarDirection.setText(DataBarAppearanceBarDirectionType.Right_To_Left.getDisplayText());
					else if( barAppearance.getBarDirection().equals(DataBarAppearanceBarDirectionType.Left_To_Right ) )
						cmbBarDirection.setText(DataBarAppearanceBarDirectionType.Left_To_Right.getDisplayText());
					else
						cmbBarDirection.setText(barAppearance.getBarDirection().toString());
				}
			}
		}
		else {
			comboStyleType.select(StyleType.ColorScale.index);
			showHideTabs(StyleType.ColorScale.index);
		}
	}
	
	private void enableFormControls() {
		if( tabColorScale != null && ! tabColorScale.isDisposed() ) {
			enableColorScaleMinimumControls();
			enableColorScaleMidPointControls();
			enableColorScaleMaximumControls();
		}
		else if( tabIcon != null && ! tabIcon.isDisposed() ) {
			updateIconControls();
		}
		else if( tabDataBars != null && ! tabDataBars.isDisposed() ) {
			enableDataBarsMinControls();
			enableDataBarsMaxControls();
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		if( textStyleName.getText().isEmpty() ) {
	    	GUIUtil.openMessageWindow( "Save Error", "Missing name for the conditional style.");
			monitor.setCanceled(true);
	    	return;
		}
		
		//TTN-2393	Add warning when renaming conditional style that is used by a conditional format
		if( ! condStyleEditorInput.isNew() && ! condStyleEditorInput.isCopy() 
				&& ! condStyle.getName().equals(textStyleName.getText()) ) {
			ConditionalFormatModelManager condFormatModelManager = new ConditionalFormatModelManager(project);
			Set<String> condFormats = condFormatModelManager.findConditionalFormatsWithStyle(condStyle.getName());
			boolean isConfirmed = false;
			if( condFormats != null && condFormats.size() > 0 ) {
				isConfirmed = MessageDialog.openConfirm(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), "Rename Conditional Style Confirmation", 
						"You are renaming a conditional style that's used by conditional format(s): '" + StringUtils.setToSortedString(condFormats, ",") + "'. Do you want to continue?");
				if( ! isConfirmed ) {
					monitor.setCanceled(true);
			    	return;
				}
			}
		}
		
		try {
			
			saveConditionalStyle();
			
			if( condStyleEditorInput.getCondStyleModelManager().findDuplicateConditionalStyle(condStyle) !=null ) {
		    	GUIUtil.openMessageWindow( "Save Error", "Duplicate conditional style name exists.");
				monitor.setCanceled(true);
		    	return;
			}
			
			condStyleEditorInput.setConditionalStyle(condStyle);
			condStyleEditorInput.save();
			
			setPartName(Constants.CONDITIONAL_FORMATS + ": " + textStyleName.getText());
			
			updateTreeViewer();
			
			condStyleEditorInput.setNew(false);
			condStyleEditorInput.setCopy(false);
			isDirty = false;
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}	
		catch(Exception e){
			logger.error(e.getMessage());
			monitor.setCanceled(true);
			return;
		}

	}

	private void saveConditionalStyle() {
		
		if( comboStyleType.getText().equals(StyleType.ColorScale.displayText)) {

			condStyle = new ColorScale(GUIDUtil.getGUID());

//			private Combo comboNumOfColor, comboMinType, comboMidType, comboMaxType;
//			private Text textMinValue,textMidpointValue, textMaxValue, txtMinColor, txtMidColor, txtMaxColor;
			
			if( comboNumOfColors.getText().equals(ColorScaleColorNumType.Two_Color.getDisplayText())) {
				
				((ColorScale) condStyle).setScaleType(ColorScaleColorNumType.Two_Color);
				
			}
			else {
				
				((ColorScale) condStyle).setScaleType(ColorScaleColorNumType.Three_Color);
				
				ColorScaleCriteria midCriteria = new ColorScaleCriteria();
				midCriteria.setCriteriaType(CriteriaType.valueOf(comboMidType.getText()));
				midCriteria.setCriteriaTypeValue(CriteriaTypeValues.Midpoint);
				if( textMidValue.getText() != null && ! textMidValue.getText().isEmpty() )
					midCriteria.setCriteriaValue(Double.parseDouble(textMidValue.getText()));
				midCriteria.setCriteriaHexColor(ColorsUtil.colorToHexString(txtMidColor.getBackground()));
				((ColorScale) condStyle).setMidpoint(midCriteria);
			}
			
			ColorScaleCriteria minCriteria = new ColorScaleCriteria();
			if( comboMinType.getText().equals(CriteriaType.Lowest.getDisplayText())) 
				minCriteria.setCriteriaType(CriteriaType.Lowest);
			else
				minCriteria.setCriteriaType(CriteriaType.valueOf(comboMinType.getText()));
			minCriteria.setCriteriaTypeValue(CriteriaTypeValues.Minimum);
			if( textMinValue.getText() != null && ! textMinValue.getText().isEmpty() )
				minCriteria.setCriteriaValue(Double.parseDouble(textMinValue.getText()));
			minCriteria.setCriteriaHexColor(ColorsUtil.colorToHexString(txtMinColor.getBackground()));
			((ColorScale) condStyle).setMinimum(minCriteria);
			
			ColorScaleCriteria maxCriteria = new ColorScaleCriteria();
			if( comboMaxType.getText().equals(CriteriaType.Highest.getDisplayText())) 
				maxCriteria.setCriteriaType(CriteriaType.Highest);
			else
				maxCriteria.setCriteriaType(CriteriaType.valueOf(comboMaxType.getText()));
			maxCriteria.setCriteriaTypeValue(CriteriaTypeValues.Maximum);
			if( textMaxValue.getText() != null && ! textMaxValue.getText().isEmpty() )
				maxCriteria.setCriteriaValue(Double.parseDouble(textMaxValue.getText()));
			maxCriteria.setCriteriaHexColor(ColorsUtil.colorToHexString(txtMaxColor.getBackground()));
			((ColorScale) condStyle).setMaximum(maxCriteria);
			
		}
		else if( comboStyleType.getText().equals(StyleType.Icon.displayText ))  {
			
			condStyle = new IconStyle(GUIDUtil.getGUID());
			
			switch( imgCombo.getSelectionIndex() ) {
				case 0:
					((IconStyle) condStyle).setIconStyle(IconStyleType.ThreeColorStoplight);
					break;
				case 1:
					((IconStyle) condStyle).setIconStyle(IconStyleType.FourColorStoplight);
					break;
				case 2:
					((IconStyle) condStyle).setIconStyle(IconStyleType.ThreeColorArrows);
					break;
				case 3:
					((IconStyle) condStyle).setIconStyle(IconStyleType.FourColorArrows);
					break;
				case 4:
					((IconStyle) condStyle).setIconStyle(IconStyleType.FiveColorArrows);
					break;
				case 5:
					((IconStyle) condStyle).setIconStyle(IconStyleType.IndicatorCheckBang);
					break;
				case 6:
					((IconStyle) condStyle).setIconStyle(IconStyleType.IndicatorThreeFlags);
					break;
				case 7:
					((IconStyle) condStyle).setIconStyle(IconStyleType.IndicatorCheckBangCircle);
					break;
				default:
					break;
			}
			
//			private ImageCombo imgCombo;
//			private Combo cmbComparive1,  cmbComparive2,  cmbComparive3,  cmbComparive4, 
//			private Combo mbIconType1, cmbIconType2, cmbIconType3, cmbIconType4;
//			private Text textIconValue1, textIconValue2, textIconValue3, textIconValue4;
			
			//by default 3-color
			IconCriteria iconCriteria1 = new IconCriteria();
			iconCriteria1.setCriteriaType(CriteriaType.valueOf(cmbIconType1.getText()));
			iconCriteria1.setCriteriaValue(Double.parseDouble(textIconValue1.getText()));
			iconCriteria1.setCriteriaOperator(cmbComparive1.getText());
			((IconStyle) condStyle).setCriteriaOne(iconCriteria1);
				
			IconCriteria iconCriteria2 = new IconCriteria();
			iconCriteria2.setCriteriaType(CriteriaType.valueOf(cmbIconType2.getText()));
			iconCriteria2.setCriteriaValue(Double.parseDouble(textIconValue2.getText()));
			iconCriteria2.setCriteriaOperator(cmbComparive2.getText());
			((IconStyle) condStyle).setCriteriaTwo(iconCriteria2);
				
			if( imgCombo.getSelectionIndex() == 1 //4 color stop light
					|| imgCombo.getSelectionIndex() == 3 ) { //4 color arrow
				
				IconCriteria iconCriteria3 = new IconCriteria();
				iconCriteria3.setCriteriaType(CriteriaType.valueOf(cmbIconType3.getText()));
				iconCriteria3.setCriteriaValue(Double.parseDouble(textIconValue3.getText()));
				iconCriteria3.setCriteriaOperator(cmbComparive3.getText());
				((IconStyle) condStyle).setCriteriaThree(iconCriteria3);
				
			}
			else if( imgCombo.getSelectionIndex() == 4 ) {//5 color arrow
				
				IconCriteria iconCriteria3 = new IconCriteria();
				iconCriteria3.setCriteriaType(CriteriaType.valueOf(cmbIconType3.getText()));
				iconCriteria3.setCriteriaValue(Double.parseDouble(textIconValue3.getText()));
				iconCriteria3.setCriteriaOperator(cmbComparive3.getText());
				((IconStyle) condStyle).setCriteriaThree(iconCriteria3);
					
				IconCriteria iconCriteria4 = new IconCriteria();
				iconCriteria4.setCriteriaType(CriteriaType.valueOf(cmbIconType4.getText()));
				iconCriteria4.setCriteriaValue(Double.parseDouble(textIconValue4.getText()));
				iconCriteria4.setCriteriaOperator(cmbComparive4.getText());
				((IconStyle) condStyle).setCriteriaFour(iconCriteria4);
				
			}
			
		}
		else if( comboStyleType.getText().equals(StyleType.DataBars.displayText ) ) {
			
			condStyle = new DataBars(GUIDUtil.getGUID());
			
//			private Combo cmbMinBarType, cmbMaxBarType, cmbFill, cmbBorder, cmbBarDirection;
//			private Text txtMinBarValue,txtMaxBarValue, txtFillColor, txtBorderColor;
			
			DataBarCriteria minCriteria = new DataBarCriteria();
			if( cmbMinBarType.getText().equals(CriteriaType.Lowest.getDisplayText())) 
				minCriteria.setCriteriaType(CriteriaType.Lowest);
			else
				minCriteria.setCriteriaType(CriteriaType.valueOf(cmbMinBarType.getText()));
			minCriteria.setCriteriaTypeValue(CriteriaTypeValues.Minimum);
			if( txtMinBarValue.getText() != null && ! txtMinBarValue.getText().isEmpty() )
				minCriteria.setCriteriaValue(Double.parseDouble(txtMinBarValue.getText()));
			((DataBars) condStyle).setMinimum(minCriteria);
			
			DataBarCriteria maxCriteria = new DataBarCriteria();
			if( cmbMaxBarType.getText().equals(CriteriaType.Highest.getDisplayText())) 
				maxCriteria.setCriteriaType(CriteriaType.Highest);
			else
				maxCriteria.setCriteriaType(CriteriaType.valueOf(cmbMaxBarType.getText()));
			maxCriteria.setCriteriaTypeValue(CriteriaTypeValues.Maximum);
			if( txtMaxBarValue.getText() != null && ! txtMaxBarValue.getText().isEmpty() )
				maxCriteria.setCriteriaValue(Double.parseDouble(txtMaxBarValue.getText()));
			((DataBars) condStyle).setMaximum(maxCriteria);
			
			DataBarAppearance barAppearance = new DataBarAppearance();
			barAppearance.setFillType(DataBarAppearanceFillType.valueOf(cmbFill.getText()));
			barAppearance.setFillHexColor(ColorsUtil.colorToHexString(txtFillColor.getBackground()));
			barAppearance.setBorderType(DataBarAppearanceBorderType.valueOf(cmbBorder.getText()));
			barAppearance.setBorderHexColor(ColorsUtil.colorToHexString(txtBorderColor.getBackground()));
			if( cmbBarDirection.getText().equals(DataBarAppearanceBarDirectionType.Right_To_Left.getDisplayText()) )
				barAppearance.setBarDirection(DataBarAppearanceBarDirectionType.Right_To_Left);
			else if( cmbBarDirection.getText().equals(DataBarAppearanceBarDirectionType.Left_To_Right.getDisplayText()) )
				barAppearance.setBarDirection(DataBarAppearanceBarDirectionType.Left_To_Right);
			else
				barAppearance.setBarDirection(DataBarAppearanceBarDirectionType.valueOf(cmbBarDirection.getText()));
			((DataBars) condStyle).setAppearance(barAppearance);
			
		}
		
		if( condStyleEditorInput.getConditionalStyle() != null && ! condStyleEditorInput.isCopy() ) 
			condStyle.setGuid(condStyleEditorInput.getConditionalStyle().getGuid());
		
		condStyle.setName(textStyleName.getText());
		
	}

	/**
	 * Updates the tree viewer control whenever a 
	 * planner role is added or modified.
	 */
	private void updateTreeViewer(){
		if (viewer != null && treeNode != null ) {
			if( treeNode instanceof ConditionalStylesNode ) {
				treeNode.createChildren(null);
				viewer.refresh(treeNode);
			}
			else if( treeNode instanceof ConditionalStyleNode ) {
				if( condStyleEditorInput.isCopy() ) {
					ConditionalStylesNode tmpNode = (ConditionalStylesNode)treeNode.getParent();
					tmpNode.createChildren(null);
					viewer.refresh(tmpNode);
				}
				else {
					//renamed
					if( ! treeNode.getName().equals(condStyle.getName()) ) {
						StylesFormatsNode tmpNode = null;
						if(treeNode.getParent() instanceof ConditionalStylesNode){
							tmpNode = (StylesFormatsNode)treeNode.getParent().getParent().getParent();
						}else if(treeNode.getParent() instanceof ConditionalFormatNode){
							tmpNode = (StylesFormatsNode)treeNode.getParent().getParent().getParent().getParent();
						}
						tmpNode.createChildren(null);
						viewer.refresh(tmpNode);
					}
					else {
						treeNode.createChildren(null);
						viewer.refresh(treeNode);
					}
				}
			}
		}
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDirty() {
		setPartName(Constants.CONDITIONAL_STYLES + ": " + textStyleName.getText());
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * 
	 * Verify event
	 * 
	 * @param event
	 */
	private void verifyValueText(Combo combo, VerifyEvent event) {
		if( event.widget instanceof Text && ((Text) event.widget).isEnabled() ) {
			String currentText = ((Text)event.widget).getText();
			String text =  currentText.substring(0, event.start) + event.text + currentText.substring(event.end);
			if( combo.getText().equals("Number") )
				event.doit = EditorControlUtil.isNumeric(text) 
					|| (text != null && text.isEmpty());		// Allow user to delete/replace leading digit (TTN-2313)
			else if( combo.getText().equals("Percent") || combo.getText().equals("Percentile") ) {
				event.doit = EditorControlUtil.isValidPercentNumber(text)  
						|| (text != null && text.isEmpty());	// Allow user to delete/replace leading digit (TTN-2313)
			}
		}
	}
	
	/**
	 * Sets the dirty flag as fires the property changes, which signals
	 * to the program that the editor is dirty.
	 */
	public void editorModified() {
	      boolean wasDirty = isDirty();
	      isDirty = true;
	      if (!wasDirty)
	         firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void setFocus() {
		textStyleName.setFocus();
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

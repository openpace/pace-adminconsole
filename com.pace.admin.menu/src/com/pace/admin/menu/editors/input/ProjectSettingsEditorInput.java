/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.server.client.PafSimpleDimTree;

public class ProjectSettingsEditorInput implements IEditorInput {
	private IProject project;
	protected String key;
	private SeasonInput seasonInput;
	private Set<String> week53MembersSelected = new TreeSet<String>();
	private boolean selectorModified;
	
	public boolean isSelectorModified() {
		return selectorModified;
	}

	public void setSelectorModified(boolean selectorModified) {
		this.selectorModified = selectorModified;
	}

	public Set<String> getWeek53MembersSelected() {
		return week53MembersSelected;
	}

	public void setWeek53MembersSelected(Set<String> week53MembersSelected) {
		this.week53MembersSelected = week53MembersSelected;
	}


	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	public ProjectSettingsEditorInput(IProject project) {
		this.project = project;
		key = project.getName();
		seasonInput = new SeasonInput(project);
	}
	
	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.key;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return this.key;
	}
	
	public void  buildSimpleTree() {
		seasonInput.buildSimpleTrees();
	}
	
	public String[] getYears(){
		return seasonInput.getYears();
	}
	
	public PafSimpleDimTree getPafSimpleTreeTime() {
		return seasonInput.getPafSimpleTreeTime();
	}
	
	public String getTimeDimName(){
		if( seasonInput.getMdbDef() != null  )
			return seasonInput.getMdbDef().getTimeDim();
		else 
			return null;
	}
	
	public boolean isServerRunning(){
		return seasonInput.isServerRunning();
	}
}

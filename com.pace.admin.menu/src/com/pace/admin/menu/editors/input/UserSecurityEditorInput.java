/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.global.security.PaceUser;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.webservices.DomainFilter;
import com.pace.admin.menu.exceptions.InvalidUserSecurityException;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafSimpleDimTree;

/**
 * IEditorInput implementation for the Role configuration.
 * 
 * @author javaj
 * @version x.xx
 */
public class UserSecurityEditorInput implements IEditorInput {
	
	private Logger logger = Logger.getLogger(UserSecurityEditorInput.class);

	private boolean isNew;
	
	private boolean isClone;

	private String key;

	private UserSecurityModelManager userSecurityModelManager = null;

	private PlannerRoleModelManager plannerRoleModelManager = null;

	private PafUserSecurity currentUserSecurity = null;

	private IProject project;
	
	private PafServer pafServer;
	
	private IFile pafSecurityFile;

	private Map<String, PafSimpleDimTree> dimTreeMaps;

	private PafApplicationDef pafApplicationDef = null;
		
	private boolean isAuthenticated;
	
	private String url;
	
	Map<String, Set<String>> completeDomainSecurityGroupMap = null;
	
	Map<String, Set<String>> filteredDomainSecurityGroupMap = null;
	
	Map<String, Map<String, Set<PaceUser>>> cachedDomainUserSecurityGroupMap = new HashMap<String, Map<String, Set<PaceUser>>>();
	
	Map<String, Set<String>> cachedDomainPaceSecurityGroups = new TreeMap<String, Set<String>>(String.CASE_INSENSITIVE_ORDER);

	/**
	 * Constructor.
	 * 
	 * @param key
	 *            The text value of the planner role.
	 * @param model
	 *            The planner role model manager.
	 * @param project
	 *            The current IProject.
	 * @param isNew
	 *            boolean, is this a new Planner Role.
	 * @throws InvalidUserSecurityException 
	 */
	public UserSecurityEditorInput(String item, IProject project, PafServer pafServer, boolean isNew ) throws InvalidUserSecurityException {
		this(item, project, pafServer, isNew, false);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param key
	 *            The text value of the planner role.
	 * @param model
	 *            The planner role model manager.
	 * @param project
	 *            The current IProject.
	 * @param isNew
	 *            boolean, is this a new Planner Role.
	 * @param isClone
	 * 			  boolean, clone user
	 * @throws InvalidUserSecurityException 
	 */
	public UserSecurityEditorInput(String item, IProject project, PafServer pafServer, boolean isNew, boolean isClone) throws InvalidUserSecurityException {

		super();

		this.setNew(isNew);
		this.setClone(isClone);

		this.key = item;
		
		this.project = project;
		this.pafServer = pafServer;
		
		userSecurityModelManager = new UserSecurityModelManager(project);
		
		plannerRoleModelManager = new PlannerRoleModelManager(project);

		dimTreeMaps = new LinkedHashMap<String, PafSimpleDimTree>();

		this.pafSecurityFile = PafApplicationUtil.getPafSecurityFile(project);
		
		if ( isNew() || isClone() ) {
						
			try {
				
				login();
																			
			} catch (ServerNotRunningException e) {
				
				throw new RuntimeException(e.getMessage());
				
			} 
			
			if( ! isAuthenticated() ) {
				throw new RuntimeException("Couldn't get cached dimensions. Login into server and cache dimensions.");
			}
			//try to cache the domain user security groups with suer name
			if ( pafServer != null && pafServer.getCompleteWSDLService() != null ) {
			
				try {
					
					cachedDomainUserSecurityGroupMap = SecurityManager.getCachedUserNamesBySecurityGroups(pafServer.getCompleteWSDLService());
					
					//try to populate the pace security groups
					if ( cachedDomainUserSecurityGroupMap != null ) {
						
						for (String domainName : cachedDomainUserSecurityGroupMap.keySet()) {
							
							Set<String> paceSecurityGroups = new TreeSet<String>();
							
							if ( cachedDomainUserSecurityGroupMap.get(domainName) != null ) {
								
								paceSecurityGroups.addAll(cachedDomainUserSecurityGroupMap.get(domainName).keySet());
								
							}
							
							cachedDomainPaceSecurityGroups.put(domainName, paceSecurityGroups);
							
							
						}
						
					}
					
				} catch (PafException e) {
					logger.error(e.getMessage());
				}
			}
		}
		
		populateDimensionMap();
		
		if ( isClone() ) {			
			
			currentUserSecurity = (PafUserSecurity) userSecurityModelManager.getItem(key);
			
			//if user should be cloned and the existing user isn't null
			if ( currentUserSecurity != null ) {
				
				PafUserSecurity clonedUserSecurity = (PafUserSecurity) currentUserSecurity.clone();
				
				clonedUserSecurity.setUserName(null);
				clonedUserSecurity.setDomainName(null);
				clonedUserSecurity.setDisplayName(null);
				
				currentUserSecurity = clonedUserSecurity;
				
			} else {
				
				throw new InvalidUserSecurityException("Security for user '" + key + "' no longer exists.");
				
			}

			
		} else if ( !isNew() ){
			
			if ( userSecurityModelManager.contains(key)) {

				currentUserSecurity = (PafUserSecurity) userSecurityModelManager.getItem(key);
				
			} else {
		
				throw new InvalidUserSecurityException("Security for user '" + key + "' no longer exists.");
				
			}						
		}
		
		if (currentUserSecurity == null) {
			currentUserSecurity = new PafUserSecurity();
		}
	}

	private void login() throws ServerNotRunningException {
		if (pafServer != null ) {

			url = pafServer.getCompleteWSDLService();
			
			if (url != null ) {
				if( ! SecurityManager.isAuthenticated(url) ) {
				
					ServerView.authWithLoginDialogIfNotAuthed(pafServer,false);
					if ( SecurityManager.isAuthenticated(url) ) {
						
						setAuthenticated(true);
					}
				}
				else {
					
					setAuthenticated(true);
					
				}
			}
		}
	}
	
	public String[] getDomainNames(boolean allDomains) {
	
		Set<String> domainNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
			
		domainNameSet.add(PafBaseConstants.Native_Domain_Name);
		
		Map<String, Set<String>> domainSecurityMap = null;
		
		if ( allDomains ) {
			
			domainSecurityMap = getCompleteSecurityGroupMap();
			
		} else {
			
			domainSecurityMap = cachedDomainPaceSecurityGroups;
			
		}
		
		if ( domainSecurityMap != null ) {
			
			domainNameSet.addAll(domainSecurityMap.keySet());
			
		}
		
		if ( domainNameSet != null && domainNameSet.size() > 0 ) {
			
			return domainNameSet.toArray(new String[0]);
						
		} else {
			
			return null;
			
		}
	
	}	

	/**
	 * 
	 * @param domainName
	 * @param allSecurityGroups
	 * @return
	 */
	public String[] getSecurityGroupNames(String domainName, boolean allSecurityGroups) {
		
		Set<String> securityGroupNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
						
		Map<String, Set<String>> domainSecurityMap = null;
		
		if ( allSecurityGroups ) {
			
			domainSecurityMap = getCompleteSecurityGroupMap();
			
		} else {
			
			domainSecurityMap = cachedDomainPaceSecurityGroups;
			
		}
		
		if ( domainSecurityMap != null ) {
			
			if ( domainSecurityMap.containsKey(domainName) ) {
				
				Set<String> domainSecurityNameSet = domainSecurityMap.get(domainName);
				
				if ( domainSecurityNameSet != null ) {
					
					securityGroupNameSet.addAll(domainSecurityNameSet);
					
					//if domain name is native, add native to set
					if ( ! domainName.equals(PafBaseConstants.Native_Domain_Name) &&
							securityGroupNameSet.contains(PafBaseConstants.Native_Domain_Name) ) {

						securityGroupNameSet.remove(PafBaseConstants.Native_Domain_Name);
						
					}
					
				}
				
			}
			
		}
			
			
		if ( domainName.equals(PafBaseConstants.Native_Domain_Name)) {
			
			securityGroupNameSet.add(PafBaseConstants.Native_Domain_Name);
			
		}
				
		if ( securityGroupNameSet != null && securityGroupNameSet.size() > 0 ) {
			
			return securityGroupNameSet.toArray(new String[0]);
						
		} else {
			
			return null;
			
		}
	}

	private Map<String, Set<String>> getCompleteSecurityGroupMap() {
		
			
		if ( completeDomainSecurityGroupMap == null ) {
		
			try {
				
				login();
				
			} catch (ServerNotRunningException e1) {
	
				logger.error(e1.getMessage());
				e1.printStackTrace();
			}
			
			if ( url != null && SecurityManager.isAuthenticated(url)  ) {

				try {
					
					completeDomainSecurityGroupMap = WebServicesUtil.createDomainSecurityGroupMap(SecurityManager.getDomainSecurityGroupsFromServer(url));				
									
				} catch (PafException e) {
					
					logger.error(e.getMessage());
					
					e.printStackTrace();
					
				}			
			}
		
		}
		
		return completeDomainSecurityGroupMap;
		
	}
		
	/*
	private String[] getConfiguredUserNameAr() throws ServerNotRunningException  {

		String[] configuredUserNames = null;
		
		if ( url != null && SecurityManager.isAuthenticated(url)) {
			
			PafUserSecurity[] usersWithSecurity = PafApplicationUtil
					.getPafSecurityUsers(project);
	
			String[] pafDBUserNames = null;
								
				pafDBUserNames = SecurityManager.getNativeDbUserNamesFromServer(url);
				
				Set<String> usersSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
				if ( pafDBUserNames != null && pafDBUserNames.length > 0 ) {
					
					for (String pafDBUserName : pafDBUserNames) {
						
						usersSet.add(pafDBUserName);
						
					}
					
					if ( usersWithSecurity != null ) {
						
						for (PafUserSecurity userWithSecurity : usersWithSecurity ) {
							
							usersSet.remove(userWithSecurity.getUserName());
							
						}
						
					}
					
					//unconfiguredUserNames = usersSet.toArray(new String[0]);
					
				} 
				
			}
		
		return configuredUserNames;
		
	}
	*/

	private void populateDimensionMap() {

		String[] hierDims = DimensionUtil.getHierDimensionNames(project);

		if (hierDims != null) {

			PafApplicationDef[] pafAppDefAr = PafApplicationUtil
					.getPafApps(project);

			if (pafAppDefAr != null && pafAppDefAr.length > 0) {

				pafApplicationDef = pafAppDefAr[0];

				for (String hierDim : hierDims) {

					PafSimpleDimTree hierTree = null;

					try {

						hierTree = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
								project, pafApplicationDef.getAppId(), hierDim);
						
					} catch (Exception e) {
					
						throw new RuntimeException(e.getMessage());
						
					}

					if (hierTree != null) {

						dimTreeMaps.put(hierDim, hierTree);

					} 

				}

			}
		}

	}

	/**
	 * Saves the current EditorInput.
	 * 
	 * @throws Exception
	 */
	public void save() throws Exception {

		if (isNew() || isClone()) {
		
			userSecurityModelManager.add(currentUserSecurity.getKey(),
					currentUserSecurity);
					
		} else if (!key.equalsIgnoreCase(currentUserSecurity.getKey())) {

			userSecurityModelManager.replace(key, currentUserSecurity);
			
		}
		
		try {		
			
			userSecurityModelManager.save();
			key = currentUserSecurity.getKey();
			setNew(false);
			setClone(false);
			
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Is this a new EditorInput.
	 * 
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * 
	 * @param isNew
	 *            true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	/**
	 * Searches the model to see if an planner role already exists.
	 * 
	 * @param key
	 *            The planner role name to search for.
	 * @return true if the planner role already exists, false if not.
	 */
	public boolean itemExists(String key) {
		return userSecurityModelManager.contains(key);
	}

	/**
	 * Is the Paf Server running.
	 * 
	 * @return true if the paf server is running, false if not.
	 */
	// Not called anywhere in AC. Commenting as per TTN - 1843
//	public boolean isServerRunning() {
//		return serverRunning;
//	}

	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {		
		
		if ( isNew() ) {
			
			return key;
		
		} else if ( isClone() ) {
			
			return Constants.COPY_TO + key;
			
		}
		
		return currentUserSecurity.getDisplayName();
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return getName();
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}

	/**
	 * Automatically generated method: toString
	 * 
	 * @return String
	 */
	public String toString() {
		return super.toString();
	}

	public String[] getRoleNames() {
		return plannerRoleModelManager.getKeys();
	}

	/**
	 * @return the pafApplicationDef
	 */
	public PafApplicationDef getPafApplicationDef() {
		return pafApplicationDef;
	}

	/**
	 * @param pafApplicationDef
	 *            the pafApplicationDef to set
	 */
	public void setPafApplicationDef(PafApplicationDef pafApplicationDef) {
		this.pafApplicationDef = pafApplicationDef;
	}

	/**
	 * @return the dimTreeMaps
	 */
	public Map<String, PafSimpleDimTree> getDimTreeMaps() {
		return dimTreeMaps;
	}

	/**
	 * @return the unconfiguredUserNames
	 */
	public PaceUser[] getUnconfiguredPaceUsers(String domainName, String securityGroup) {
		
		PaceUser[] unconfiguredPaceUsers = null;
		
		if ( url != null && SecurityManager.isAuthenticated(url) && domainName != null && securityGroup != null ) {
			
			try {
				
				//if cached map doesn't exists, or if cached map doesn't contain key domain name, or if map doesn't contain security group for domain.
				if ( ! cachedDomainUserSecurityGroupMap.containsKey(domainName) || 
						( cachedDomainUserSecurityGroupMap.containsKey(domainName) && ! cachedDomainUserSecurityGroupMap.get(domainName).containsKey(securityGroup))) {
					
					DomainFilter df = new DomainFilter();			
					
					df.setDomainName(domainName);
					
					df.getSecurityGroupSet().add(securityGroup);
					
//					cachedDomainUserSecurityGroupMap
					Map<String, Map<String, Set<PaceUser>>> domainUserSecurityGroupMap = SecurityManager.getUserNamesBySecurityGroups(url, new DomainFilter[] { df });
										
					//if map contains domain and security group
					if ( domainUserSecurityGroupMap != null && domainUserSecurityGroupMap.containsKey(domainName) ) { 
					
						//get new user security map
						Map<String, Set<PaceUser>> newUserSecurityGroupMap = domainUserSecurityGroupMap.get(domainName);
						
						//get cached map
						Map<String, Set<PaceUser>> cachedUserSecurityGroupMap = cachedDomainUserSecurityGroupMap.get(domainName);
						
						//if the cached map is null, replace with new user security group
						if ( cachedUserSecurityGroupMap == null ) {
							
							cachedUserSecurityGroupMap = newUserSecurityGroupMap;
							
						} else {
						
							//if new map contains security group, add to cache
							if ( newUserSecurityGroupMap.containsKey(securityGroup) ) {
								
								for (String newSecurityGroup : newUserSecurityGroupMap.keySet() ) {
								
									cachedUserSecurityGroupMap.put(securityGroup, newUserSecurityGroupMap.get(newSecurityGroup));								
									
								}	
								
							//else add security gruop to cache but with a null value.
							} else {
								
								cachedUserSecurityGroupMap.put(securityGroup, null);
								
							}
							
						}						
							
						//readd cache to map
						cachedDomainUserSecurityGroupMap.put(domainName, cachedUserSecurityGroupMap);
						
					}
				} 												
				
				//if cache contains the domain
				if ( cachedDomainUserSecurityGroupMap.containsKey(domainName) ) {
					
					Map<String, Set<PaceUser>> userSecurityGroupMap = cachedDomainUserSecurityGroupMap.get(domainName);
					
					//if user security group map contains the security group
					if ( userSecurityGroupMap != null && userSecurityGroupMap.containsKey(securityGroup) ) {
						
						Set<PaceUser> paceUserSet = userSecurityGroupMap.get(securityGroup);
						
						if ( paceUserSet != null && paceUserSet.size() > 0 ) {
							
							PafUserSecurity[] usersWithSecurity = PafApplicationUtil.getPafSecurityUsers(project);
							
							if ( usersWithSecurity != null ) {
								
								for (PafUserSecurity userWithSecurity : usersWithSecurity ) {
									
									String userWithSecurityDomainName = userWithSecurity.getDomainName();
									
									//if user with security domain name is null or blank, set default
									if ( userWithSecurityDomainName == null || userWithSecurityDomainName.equals("") ) {
										
										userWithSecurityDomainName = PafBaseConstants.Native_Domain_Name;
										
									}
									
									//if domain name equals users domain name, remove from pace user set
									if ( domainName.equalsIgnoreCase(userWithSecurityDomainName)) {
										
										paceUserSet.remove(new PaceUser(userWithSecurity.getUserName()));
										
									}
									
								}
								
							}
							
							if ( paceUserSet.size() > 0 ) {
								
								unconfiguredPaceUsers = paceUserSet.toArray(new PaceUser[0]); 
																								
							}
							
						}
						
					}
					
				}
			} catch (PafException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
		}	
		
		return unconfiguredPaceUsers;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the currentUserSecurity
	 */
	public PafUserSecurity getCurrentUserSecurity() {
		return currentUserSecurity;
	}

	/**
	 * @param currentUserSecurity the currentUserSecurity to set
	 */
	public void setCurrentUserSecurity(PafUserSecurity currentUserSecurity) {
		this.currentUserSecurity = currentUserSecurity;
	}

	/**
	 * @return the isAuthenticated
	 */
	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	/**
	 * @param isAuthenticated the isAuthenticated to set
	 */
	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @return Returns the pafSecurityFile.
	 */
	public IFile getPafSecurityFile() {
		return pafSecurityFile;
	}

	/**
	 * @param pafSecurityFile The pafSecurityFile to set.
	 */
	public void setPafSecurityFile(IFile pafSecurityFile) {
		this.pafSecurityFile = pafSecurityFile;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @return the isClone
	 */
	public boolean isClone() {
		return isClone;
	}

	/**
	 * @param isClone the isClone to set
	 */
	public void setClone(boolean isClone) {
		this.isClone = isClone;
	}

	public boolean isServerInMixedMode() {
		
		boolean isMixedMode = false;
		
		if ( url != null ) {
			
			ServerSession session = SecurityManager.getSession(url);
			
			isMixedMode = session.isMixedAuthMode();			
			
		}
		
		return isMixedMode;
	}


}
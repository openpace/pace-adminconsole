/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.MdbDefModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PafUserModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.app.MdbDef;
import com.pace.base.app.PafPlannerRole;
import com.pace.base.app.Season;
import com.pace.server.client.PafSimpleDimTree;

/**
* IEditorInput implementation for the Role configuration.
* @author kmoos
* @version	x.xx
*/
public class PlannerRoleInput implements IEditorInput {
	
	private final static Logger logger = Logger.getLogger(PlannerRoleInput.class);
	
	private boolean isNew;
	private boolean isCopy;
//	private boolean serverRunning;
	private String key;
	private PlannerRoleModelManager plannerRoleModel = null;
	private SeasonModelManager seasonsModelManager = null;
	private MdbDef mdbDef = null;
	private PafPlannerRole currentRole = null;
	private IProject project;
	
	/**
	 * Constructor.
	 * @param item The text value of the planner role.
	 * @param model The planner role model manager.
	 * @param seasonModel The seasons model manager.
	 * @param project The current IProject.
	 * @param isNew boolean, is this a new Planner Role.
	 */
	public PlannerRoleInput(String item, ModelManager model, ModelManager seasonModel, 
			IProject project, boolean isNew) {
		this(item, model, seasonModel, project, isNew, false);
	}
	
	/**
	 * Constructor.
	 * @param item The text value of the planner role.
	 * @param model The planner role model manager.
	 * @param seasonModel The seasons model manager.
	 * @param project The current IProject.
	 * @param isNew boolean, is this a new Planner Role.
	 * @param isCopy boolean, is this a copy of a Planner Role.
	 */
	public PlannerRoleInput(String item, ModelManager model, ModelManager seasonModel, 
			IProject project, boolean isNew, boolean isCopy) {
		super();
		
		this.setNew(isNew);
		this.key = item;
		this.plannerRoleModel = (PlannerRoleModelManager) model;
		this.seasonsModelManager = (SeasonModelManager) seasonModel;
		this.mdbDef = new MdbDefModelManager(project).getMdbDef();
		this.project = project;
		this.isCopy = isCopy;
		
		this.currentRole = (PafPlannerRole) plannerRoleModel.getItem(key);
		
		if ( isCopy() ) {
			
			if ( this.currentRole != null)  {
				
				this.currentRole = this.currentRole.clone();
				this.currentRole.setRoleName(Constants.COPY_TO + this.currentRole.getRoleName());
				
			}
			
		}
		
		if(currentRole == null){
			currentRole = new PafPlannerRole();
		}	
	}
	
	/**
	 * Returns the role name.
	 * @return String value containing the current planner role name.
	 */
	public String getRoleName() {
		return currentRole.getRoleName();
	}
	
	/**
	 * Sets a role name in the current planner role.
	 * @param name The role name.
	 */
	public void setRoleName(String name) {
		currentRole.setRoleName(name);
	}
	
	public boolean isReadOnly() {
		return currentRole.isReadOnly();
	}
	
	public void setReadOnly(boolean isReadOnly) {
		currentRole.setReadOnly(isReadOnly);
	}
	
	/**
	 * Gets the plan type for the Role.
	 * @return The string value plan type for the current planner role.
	 */
	public String getPlanType(){
		return currentRole.getPlanType();
	}
	
	/**
	 * Gets an array of all available plan types.
	 * @return An array of plan types.
	 * @throws Exception 
	 */
	public String[] getPlanTypes() {
		try {
			PafSimpleDimTree pafSimpleTree = null;
				pafSimpleTree = DimensionTreeUtility.getDimensionTree(
						project, PafProjectUtil.getApplicationName(project),
						mdbDef.getPlanTypeDim(),
						false);
			
//			serverRunning = true;
			
			//Strip off the dimension root, if it's present.
			if(pafSimpleTree != null && pafSimpleTree.getTraversedMembers().size() > 0){
				if(pafSimpleTree.getTraversedMembers().get(0).equalsIgnoreCase(mdbDef.getPlanTypeDim())){
					String[] planTypes = new String[pafSimpleTree.getTraversedMembers().size() - 1];
				
					System.arraycopy(pafSimpleTree.getTraversedMembers().toArray(new String[0]),
							1,
							planTypes,
							0,
							planTypes.length);
						
					return planTypes;
					
				} else{
					return pafSimpleTree.getTraversedMembers().toArray(new String[0]);
				}
			}
			else{
				return null;
			}
		}catch(Exception e){
			
			logger.error(e.getMessage());
			
			return null;
		}
	}
	
	/**
	 * Sets the plan type of the planner role.
	 * @param planType
	 */
	public void setPlanType(String planType){
		currentRole.setPlanType(planType);
	}
	
	/**
	 * Gets the name of the plan type dimension.
	 * @return The name of the plan type dimension.
	 */
	public String getPlanTypeDimName(){
		return mdbDef.getPlanTypeDim();
	}
	
	/**
	 * Gets an array of the currently selected seasons for the planner role.
	 * @return A string array of the currently selected season for the planner role.
	 */
	public String[] getSelectedSeasons(){
		return currentRole.getSeasonIds();
	}
	
	/**
	 * Sets the seasons of the current planner role.
	 * @param seasons The array of seasons.
	 */
	public void setSelectedSeasons(String[] seasons){
		currentRole.setSeasonIds(seasons);
	}
	
	/**
	 * Gets all the available seasons.
	 * @return An array of all of the available seasons.
	 */
	@SuppressWarnings("unchecked")
	public String[] getAllSeasons(){
		
		ArrayList<String> allSeasons = new ArrayList<String>();
		
		//get the currently selected seasons (in order) 
		String[] selSeasons = getSelectedSeasons();
		if(selSeasons != null && selSeasons.length > 0){
			for(String s : selSeasons){
				allSeasons.add(s);
			}
		}
			
		Collection<Season> seasons = seasonsModelManager.getModel().values();
		ArrayList<String> seasonIds = new ArrayList<String>();
		
		//build an array of season ids.
		for(Season s : seasons){
			seasonIds.add(s.getId());
		}
		
		//remove any dead seasons.
		for(String s : seasonIds){
			if(!allSeasons.contains(s)){
				allSeasons.remove(s);
			}
		}
		
		//add any unselected seasons.
		for(Season season : seasons){
			if(!allSeasons.contains(season.getId())){
				allSeasons.add(season.getId());
			} 
		}
		
		
		return allSeasons.toArray(new String[allSeasons.size()]);
	}
	
	
	
	/**
	 * Saves the current EditorInput.
	 * @throws Exception
	 */
	public void save() throws Exception{
		
		//if new or copy, add to role model
		if(isNew() || isCopy()){
			plannerRoleModel.add(currentRole.getRoleName(), currentRole);
		} else if(! key.equalsIgnoreCase(currentRole.getRoleName())){
			PafPlannerConfigModelManager roleConfigModelManager = new PafPlannerConfigModelManager(project);
			roleConfigModelManager.renameRoleName(key, currentRole.getRoleName());
			
			PafUserModelManager pafUserModelManager = new PafUserModelManager(project);
			pafUserModelManager.renameRoleNames(key, currentRole.getRoleName());
		}
		try{
			plannerRoleModel.save();
			key = currentRole.getRoleName();
			setNew(false);
			setCopy(false);
		} catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	/**
	 * Searches the model to see if an planner role already exists.
	 * @param key The planner role name to search for.
	 * @return true if the planner role already exists, false if not.
	 */
	public boolean itemExists(String key){
		return plannerRoleModel.contains(key);
	}
	
	/**
	 * Is the Paf Server running.
	 * @return true if the paf server is running, false if not.
	 */
//	public boolean isServerRunning() {
//		return serverRunning;
//	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * @return the isCopy
	 */
	public boolean isCopy() {
		return isCopy;
	}

	/**
	 * @param isCopy the isCopy to set
	 */
	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}

}
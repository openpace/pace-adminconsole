/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.base.ui.IPafMapModelManager;
import com.pace.base.ui.PrintStyle;

public abstract class PrintStyleInput implements IEditorInput {

	private final static Logger logger = Logger.getLogger(PrintStyleInput.class);
	
	private boolean isNew;
	private boolean isCopy;
	protected String key;
	protected IPafMapModelManager model;
	protected IProject project;
	protected PrintStyle printStyle;
	
	public PrintStyleInput(String item, IPafMapModelManager model, IProject project) {
		// TODO Auto-generated constructor stub
		super();
		this.key = item;
		this.setModel(model);
		this.project = project;
	}
	public PrintStyleInput(String item, IPafMapModelManager model, IProject project, boolean isNew) {
		// TODO Auto-generated constructor stub
		super();
		this.key = item;
		this.setModel(model);
		this.project = project;
		this.setNew(isNew);
	}

	/**
	 * Constructor.
	 * @param item The text value of the role configuration.
	 * @param model The role configuration model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new Planner Role.
	 * @param isCopy isCopy boolean, is this a copy of a Planner Role.
	 */
	public PrintStyleInput(String item, IPafMapModelManager model, IProject project, boolean isNew, boolean isCopy) {
		super();
		this.key = item;
		this.setModel(model);
		this.project = project;
		this.setNew(isNew);
		this.setCopy(isCopy );
	}

	public IPafMapModelManager getModel() {
		return model;
	}

	public void setModel(IPafMapModelManager model) {
		this.model = model;
	}
	public abstract void save() throws Exception;

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public boolean isCopy() {
		return isCopy;
	}

	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return key;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub
		key = name; 
	}

	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getToolTipText() {
		// TODO Auto-generated method stub
		return key;
	}

	public String getPrintStyleName() {
		return printStyle.getName();
	}
	
	public void setPrintStyleName(String name) {
		printStyle.setName(name);
	}
	
	public boolean isPortrait()  {
		if( printStyle.getPortrait() != null )
			return printStyle.getPortrait();
		return false;
	}
	
	public void setPortrait(boolean isPortrait) {
		printStyle.setPortrait(isPortrait);
	}
	
	public boolean isLandscape() {
		if( printStyle.getLandscape() != null )
			return printStyle.getLandscape();
		return false;
	}
	
	public void setLandscape(boolean isLandscape) {
		printStyle.setLandscape(isLandscape);
	}
	
	public boolean isAdjustTo() {
		if( printStyle.getAdjustTo() != null )
			return printStyle.getAdjustTo();
		return false;
	}
	
	public void setAdjustTo(boolean isAdjustTo) {
		printStyle.setAdjustTo(isAdjustTo);
	}
	
	public int getPercentNormalSize() {
		if( printStyle.getPercentNormalSize() != null )
			return printStyle.getPercentNormalSize();
		return 100;
	}
	
	public void setPercentNormalSize(int percentNormalSize) {
		printStyle.setPercentNormalSize(percentNormalSize);
	}
	public boolean isFitTo() {
		if( printStyle.getFitTo() != null )
			return printStyle.getFitTo();
		return false;
	}
	public void setFitTo(boolean isFitTo) {
		printStyle.setFitTo(isFitTo);
	}
	public int getPageWide() {
		if( printStyle.getPageWide() != null )
			return printStyle.getPageWide();
		return 1;
	}
	public void setPageWide(int pageWide) {
		printStyle.setPageWide(pageWide);
	}
	public int getPageTall() {
		if( printStyle.getPageTall() != null )
			return printStyle.getPageTall();
		return 1;
	}
	public void setPageTall(int pageTall) {
		printStyle.setPageTall(pageTall);
	}
	public String getPaperSize() {
		if( printStyle.getPaperSize() != null )
			return printStyle.getPaperSize();
		return "Letter";
	}
	public void setPaperSize(String paperSize) {
		printStyle.setPaperSize(paperSize);
	}
	public String getFirstPageNumber() {
		return printStyle.getFirstPageNumber();
	}
	public void setFirstPageNumber(String firstPageNumber) {
		printStyle.setFirstPageNumber(firstPageNumber);
	}
	public float getHeader() {
		return printStyle.getHeader();
	}
	public void setHeader(float header) {
		printStyle.setHeader(header);
	}
	public float getTop() {
		return printStyle.getTop();
	}
	public void setTop(float top) {
		printStyle.setTop(top);
	}
	public float getLeft() {
		return printStyle.getLeft();
	}
	public void setLeft(float left) {
		printStyle.setLeft(left);
	}
	public float getRight() {
		return printStyle.getRight();
	}
	public void setRight(float right) {
		printStyle.setRight(right);
	}
	public float getBottom() {
		return printStyle.getBottom();
	}
	public void setBottom(float bottom) {
		printStyle.setBottom(bottom);
	}
	public float getFooter() {
		return printStyle.getFooter();
	}
	public void setFooter(float footer) {
		printStyle.setFooter(footer);
	}
	public boolean getCenterHorizontally() {
		return printStyle.getCenterHorizontally();
	}
	public void setCenterHorizontally(boolean centerHorizontally) {
		printStyle.setCenterHorizontally(centerHorizontally);
	}
	public boolean getCenterVertically() {
		return printStyle.getCenterVertically();
	}
	public void setCenterVertically(Boolean centerVertically) {
		printStyle.setCenterVertically(centerVertically);
	}
	public String getHeaderText() {
		return printStyle.getHeaderText();
	}
	public void setHeaderText(String headerText) {
		printStyle.setHeaderText(headerText);
	}
	public String getFooterText() {
		return printStyle.getFooterText();
	}
	public void setFooterText(String footerText) {
		printStyle.setFooterText(footerText);
	}
	public boolean getDiffOddAndEvenPages() {
		return printStyle.getDiffOddAndEvenPages();
	}
	public void setDiffOddAndEvenPages(boolean diffOddAndEvenPages) {
		printStyle.setDiffOddAndEvenPages(diffOddAndEvenPages);
	}
	public boolean getDiffFirstPage() {
		return printStyle.getDiffFirstPage();
	}
	public void setDiffFirstPage(boolean diffFirstPage) {
		printStyle.setDiffFirstPage(diffFirstPage);
	}
	public boolean getScaleWithDocument() {
		return printStyle.getScaleWithDocument();
	}
	public void setScaleWithDocument(boolean scaleWithDocument) {
		printStyle.setScaleWithDocument(scaleWithDocument);
	}
	public boolean getAlignWithPageMargin() {
		return printStyle.getAlignWithPageMargin();
	}
	public void setAlignWithPageMargin(boolean alignWithPageMargin) {
		printStyle.setAlignWithPageMargin(alignWithPageMargin);
	}
	public void setEntireSheet(boolean entireSheet) {
		printStyle.setEntireView(entireSheet);
	}
	public boolean getEntireSheet() {
		return printStyle.getEntireView();
	}
	public void setUserSelection(boolean userSelection) {
		printStyle.setUserSelection(userSelection);
	}
	public boolean getUserSelection() {
		return printStyle.getUserSelection();
	}
	public void setUserSelectionText(String userSelectionText) {
		printStyle.setUserSelectionText(userSelectionText);
	}
	public String getUserSelectionText() {
		return printStyle.getUserSelectionText();
	}
	public String getRowsToRepeatAtTop() {
		return printStyle.getRowsToRepeatAtTop();
	}
	public void setRowsToRepeatAtTop(String rowsToRepeatAtTop) {
		printStyle.setRowsToRepeatAtTop(rowsToRepeatAtTop);
	}
	public String getColsToRepeatAtLeft() {
		return printStyle.getColsToRepeatAtLeft();
	}
	public void setColsToRepeatAtLeft(String colsToRepeatAtLeft) {
		printStyle.setColsToRepeatAtLeft(colsToRepeatAtLeft);
	}
	public boolean getGridlines() {
		return printStyle.getGridlines();
	}
	public void setGridlines(boolean gridlines) {
		printStyle.setGridlines(gridlines);
	}
	public boolean getBlackAndWhite() {
		return printStyle.getBlackAndWhite();
	}
	public void setBlackAndWhite(boolean blackAndWhite) {
		printStyle.setBlackAndWhite(blackAndWhite);
	}
	public boolean getDraftQuality() {
		return printStyle.getDraftQuality();
	}
	public void setDraftQuality(boolean draftQuality) {
		printStyle.setDraftQuality(draftQuality);
	}
	public boolean getRowAndColHeadings() {
		return printStyle.getRowAndColHeadings();
	}
	public void setRowAndColHeadings(Boolean rowAndColHeadings) {
		printStyle.setRowAndColHeadings(rowAndColHeadings);
	}
	public String getComment() {
		return printStyle.getComment();
	}
	public void setComment(String comment) {
		printStyle.setComment(comment);
	}
	public String getCellErrorsAs() {
		return printStyle.getCellErrorsAs();
	}
	public void setCellErrorsAs(String cellErrorsAs) {
		printStyle.setCellErrorsAs(cellErrorsAs);
	}
	public boolean getDownThenOver() {
		return printStyle.getDownThenOver();
	}
	public void setDownThenOver(Boolean downThenOver) {
		printStyle.setDownThenOver(downThenOver);
	}
	public boolean getOverThenDown() {
		return printStyle.getOverThenDown();
	}
	public void setOverThenDown(Boolean overThenDown) {
		printStyle.setOverThenDown(overThenDown);
	}
	
}

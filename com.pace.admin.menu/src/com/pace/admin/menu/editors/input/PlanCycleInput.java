/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.Assert;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.MdbDefModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.model.managers.VersionDefModelManager;
import com.pace.base.app.MdbDef;
import com.pace.base.app.PlanCycle;
import com.pace.base.app.VersionDef;

/**
* IEditorInput implementation for the Plan Cycle.
* @author kmoos
* @version x.xx
*/
public class PlanCycleInput implements IEditorInput {
	
	private static final Logger logger = Logger.getLogger(PlanCycleInput.class);
	
	private boolean isNew;
	private boolean isCopy;
	private String key;
	private IProject project = null;
	private PlanCycleModelManager planCycleModel = null;
	private VersionDefModelManager versionDefModel = null;
	private MdbDefModelManager mdbDefModelManager = null;
	private MdbDef mdbDef = null;
	private PlanCycle currentPlanCycle = null;
	
	/**
	 * IEditorInput implementation for the plan cycle configuration.
	 * @param item The text value of the plan cycle.
	 * @param planCycleModel Plan Cycle model manager.
	 * @param versionDefModel Version Def model manager.
	 * @param project The current IProject.
	 * @param isNew boolean, is this a new plan cycle.
	 */
	public PlanCycleInput(String item, ModelManager planCycleModel, ModelManager versionDefModel,
			IProject project, boolean isNew){
		this(item, planCycleModel, versionDefModel, project, isNew, false);
	}
	
	/**
	 * IEditorInput implementation for the plan cycle configuration.
	 * @param item The text value of the plan cycle.
	 * @param planCycleModel Plan Cycle model manager.
	 * @param versionDefModel Version Def model manager.
	 * @param project The current IProject.
	 * @param isNew boolean, is this a new plan cycle.
	 */
	public PlanCycleInput(String item, ModelManager planCycleModel, ModelManager versionDefModel,
			IProject project, boolean isNew, boolean isCopy){
		super();
		Assert.isNotNull(item);Assert.isNotNull(planCycleModel);
		Assert.isNotNull(versionDefModel);Assert.isNotNull(project);
		
		this.setNew(isNew);
		this.setCopy(isCopy);
		this.key = item;
		this.project = project;
		this.planCycleModel = (PlanCycleModelManager) planCycleModel;
		this.versionDefModel = (VersionDefModelManager) versionDefModel;
		this.mdbDefModelManager = new MdbDefModelManager(project);
		this.mdbDef = this.mdbDefModelManager.getMdbDef();
				
		//Set the inital plan cycle.
		currentPlanCycle = (PlanCycle) planCycleModel.getItem(item);
		
		//if copy, try to clone
		if ( isCopy() ) {
			
			currentPlanCycle = (PlanCycle) currentPlanCycle.clone();
			
			if ( currentPlanCycle != null ) {
				
				currentPlanCycle.setLabel(Constants.COPY_TO + currentPlanCycle.getLabel());
				
			}			
			
		}
		
		//if current plan cycle is null, create empty plan cycle
		if(currentPlanCycle == null) {
			
			currentPlanCycle = new PlanCycle("", "");
			
		}
		
	}
	
	/**
	 * Gets the plan cycle name.
	 * @return A string with the plan cycle name.
	 */
	public String getPlanCycleName(){
		return currentPlanCycle.getLabel();
	}
	
	/**
	 * Gets the current version.
	 * @return A string with the current version.
	 */
	public String getPlanCycleVersion() {
		return currentPlanCycle.getVersion();
	}

	/**
	 * Sets the version.
	 * @param planCycleVersion The version.
	 */
	public void setPlanCycleVersion(String planCycleVersion) {
		currentPlanCycle.setVersion(planCycleVersion);
	}

	/**
	 * Sets the plan cycle name.
	 * @param planCycleName The plan cycle.
	 */
	public void setPlanCycleName(String planCycleName) {
		currentPlanCycle.setLabel(planCycleName);
	}
	
	/**
	 * Gets the dimension name of the verison dimension.
	 * @return The version dimension name.
	 */
	public String getVersionDimName(){
		return mdbDef.getVersionDim();
	}
	
	/**
	 * Gets all of the available versions.
	 * @return A string array of all of the available versions.
	 */
	@SuppressWarnings("unchecked")
	public String[] getPlanCycleVersions(){
		Collection<VersionDef> versionDefs = versionDefModel.getModel().values();
		
		String[] ret = new String[versionDefs.size()];
		int i = 0;
		for(VersionDef versionDef : versionDefs){
			ret[i] = versionDef.getName();
			i++;
		}
		return ret;
	}
	
	/**
	 * Saves the current EditorInput.
	 * @throws Exception
	 */
	public void save() throws Exception{
		if(isNew() || isCopy() ){
			try{
				planCycleModel.add(currentPlanCycle.getLabel(), currentPlanCycle);
			} catch(Exception e){
				throw e;
			}
		} else if(! key.equalsIgnoreCase(currentPlanCycle.getLabel())){
			SeasonModelManager seasonsModelManager = new SeasonModelManager(project);
			seasonsModelManager.renamePlanCycle(key, currentPlanCycle.getLabel());
			
			PafPlannerConfigModelManager roleConfigModelManager = new PafPlannerConfigModelManager(project);
			roleConfigModelManager.renamePlanCycle(key, currentPlanCycle.getLabel());
		}
		try{
			planCycleModel.save();
			this.key = currentPlanCycle.getLabel();
			setNew(false);
			setCopy(false);
		} catch(Exception e){
			
			logger.error(e.getMessage());			
			throw e;
			
		}
	}
	
	/**
	 * Searches the model to see if an plan cycle already exists.
	 * @param key The plan cycle name to search for.
	 * @return true if the plan cycle already exists, false if not.
	 */
	public boolean itemExists(String key){
		return planCycleModel.contains(key);
	}
	
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}

	public boolean isCopy() {
		return isCopy;
	}

	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	
	
}
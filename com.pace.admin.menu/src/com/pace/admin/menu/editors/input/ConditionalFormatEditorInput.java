/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.global.model.managers.ConditionalStyleModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafMdbProps;

/**
* IEditorInput implementation for the Seasons.
* @author ihuang
* @version	x.xx
*/
public class ConditionalFormatEditorInput implements IEditorInput {
	
	private final static Logger logger = Logger.getLogger(ConditionalFormatEditorInput.class);
	
	private IProject project;
	private String key;
	private boolean isNew;
	private boolean isCopy;
	
	private ConditionalFormatModelManager modelManager;
	public ConditionalFormatModelManager getModelManager() {
		return modelManager;
	}
	public void setModelManager(ConditionalFormatModelManager modelManager) {
		this.modelManager = modelManager;
	}

	private ConditionalFormat conditionalFormat; //current conditional format used for Editor
	
	private ConditionalStyleModelManager condStyleModelManager;
	private String[] condStyles; //a list of conditional styles

	private PafMdbProps cachedPafMdbProps;
	private List<String> cachedDimensions = new ArrayList<String>();
	private List<String> cachedAttributeDimensions = new ArrayList<String>();
	
	public ConditionalFormatEditorInput(String item, ModelManager model, IProject project) throws Exception {
		this(item, model, project, false, false);
	}
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 * @throws Exception 
	 */
	public ConditionalFormatEditorInput(String item, ModelManager model, IProject project, boolean isNew) throws Exception {
		this(item, model, project, isNew, false);
	}
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 * @param isCopy isCopy boolean, is this a copy season. 
	 * @throws Exception 
	 */
	public ConditionalFormatEditorInput(String item, ModelManager model, IProject project, boolean isNew, boolean isCopy) throws Exception {
		
		super();
		this.key = item;
		this.modelManager = (ConditionalFormatModelManager) model;
		this.project = project;
		this.setNew(isNew);
		this.setCopy(isCopy);
		this.conditionalFormat = (ConditionalFormat)modelManager.getItem(key);

		condStyleModelManager = new ConditionalStyleModelManager(project);
		setCondStyles(condStyleModelManager.getNames());

		try {
			loadDimensions();
		} catch (Exception e) {
			throw e;
		}
		
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	public ConditionalStyleModelManager getCondStyleModelManager() {
		return condStyleModelManager;
	}

	public void setCondStyleModelManager(
			ConditionalStyleModelManager condStyleModelManager) {
		this.condStyleModelManager = condStyleModelManager;
	}

	public ConditionalFormat getConditionalFormat() {
		return (ConditionalFormat) modelManager.getItem(key);
	}

	public void setConditionalFormat(ConditionalFormat conditionalFormat) {
		this.conditionalFormat = conditionalFormat;
	}

	public ConditionalFormat getConditionalFormat(String formatName) {
		return (ConditionalFormat) modelManager.getItem(formatName);
	}

	public String[] getCondStyles() {
		return condStyles;
	}
	public void setCondStyles(String[] condStyles) {
		this.condStyles = condStyles;
	}

	public List<String> getCachedAttributeDimensions() {
		return cachedAttributeDimensions;
	}
	public void setCachedAttributeDimensions(List<String> cachedAttributeDimensions) {
		this.cachedAttributeDimensions = cachedAttributeDimensions;
	}

	/**
	 * Save the current SeasonModelManager.
	 * @exception java.lang.Exception
	 */
	public void save() throws Exception {

		if(! isNew() && ! isCopy() && conditionalFormat != null && ! key.equalsIgnoreCase(conditionalFormat.getName())){
			modelManager.renameConditionalFormat(key, conditionalFormat.getName());
		}
		else {
			modelManager.add(conditionalFormat.getName(), conditionalFormat);
		}
		
		try{
			this.key = conditionalFormat.getName();
			modelManager.save();
		} catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
		
	/**
	 * @return the isCopy
	 */
	public boolean isCopy() {
		return isCopy;
	}

	/**
	 * @param isCopy the isCopy to set
	 */
	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	private void loadDimensions() throws Exception {
		
		cachedDimensions.addAll(Arrays.asList(PafProjectUtil.getProjectDimensions(project)));
		cachedDimensions.remove("PlanType");
		
		try {
			PafServer projServer = PafProjectUtil.getProjectServer(project);
			
			cachedPafMdbProps = DimensionTreeUtility.getCachedMdbProps(projServer, PafProjectUtil.getApplicationName(project));
			if( cachedPafMdbProps == null ) {
				cachedPafMdbProps = DimensionTreeUtility.getMdbProps(
						PafProjectUtil.getProjectServer(project), 
						PafProjectUtil.getApplicationName(project));
			}
			if ( cachedPafMdbProps != null ) {
				if( cachedPafMdbProps.getCachedAttributeDims().size() > 0 ) {
					cachedAttributeDimensions = cachedPafMdbProps.getCachedAttributeDims();
				}
			}
		} catch (ServerNotRunningException sne ) {
			
			logger.error(" Unable to edit Conditional Formats if the Pace Server isn't running and there are no cached dimensions.");
			throw sne;
			
		} catch (Exception e) {
			logger.error("Cannot load cached dimensions, please reload your dimension cache.");
			throw e;
		}
		
	}
	
	public List<String> getCachedDimensions() {
		return cachedDimensions;
	}

	public void setCachedDimensions(List<String> cachedDimensions) {
		this.cachedDimensions = cachedDimensions;
	}

//	public Map<String, List<Integer>> getDimLevelMap() {
//		return dimLevelMap;
//	}
//
//	public void setDimLevelMap(Map<String, List<Integer>> dimLevelMap) {
//		this.dimLevelMap = dimLevelMap;
//	}
//
//	public Map<String, List<Integer>> getDimGenMap() {
//		return dimGenMap;
//	}
//
//	public void setDimGenMap(Map<String, List<Integer>> dimGenMap) {
//		this.dimGenMap = dimGenMap;
//	}
//
//	public Map<String, PafSimpleDimTree> getDimTreeMap() {
//		return dimTreeMap;
//	}
//
//	public void setDimTreeMap(Map<String, PafSimpleDimTree> dimTreeMap) {
//		this.dimTreeMap = dimTreeMap;
//	}

	
}
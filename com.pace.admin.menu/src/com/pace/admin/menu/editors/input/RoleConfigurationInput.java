/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.model.managers.AdminLockModelManager;
import com.pace.admin.global.model.managers.CustomMenuDefModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.RuleSetModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.nodes.AdminLockNode;
import com.pace.admin.menu.providers.CustomMenuDefLabelProvider;
import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.CustomMenuDef;
import com.pace.base.comm.DataFilterSpec;
import com.pace.base.comm.PafPlannerConfig;
import com.pace.base.comm.UserFilterSpec;
import com.pace.base.security.AdminPersistLockDef;

/**
* IEditorInput implementation for the Planner Role configuration.
* @author kmoos
* @version x.xx
*/
public class RoleConfigurationInput implements IEditorInput {
	private boolean isNew;
	private boolean isCopy;	
	private String key;
	private PafPlannerConfigModelManager roleConfigModelManager = null;
	private ViewModelManager viewModelManager = null;
	private PafViewGroupModelManager pafViewTreeModelManager = null;
	private CustomMenuDefModelManager customMenuDefModelManager = null;
	private RuleSetModelManager ruleSetModelManager = null;
	private PlanCycleModelManager planCycleModel = null;
	private PlannerRoleModelManager plannerRoleModel = null;
	private PafPlannerConfig currentConfig = null;
	private CustomMenuDefLabelProvider customMenuDefLabelProvider;
	private IProject project;
	private String[] readOnlyMeasures;
	
	/**
	 * Constructor.
	 * @param item The text value of the role configuration.
	 * @param model The role configuration model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new Planner Role.
	 */
	public RoleConfigurationInput(String item, ModelManager model, IProject project, boolean isNew) {
		this(item, model, project, isNew, false);
	}
	
	/**
	 * Constructor.
	 * @param item The text value of the role configuration.
	 * @param model The role configuration model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new Planner Role.
	 * @param isCopy isNew boolean, is this a copy of a Planner Role.
	 */
	public RoleConfigurationInput(String item, ModelManager model, IProject project, boolean isNew, boolean isCopy) {
		super();
		this.key = item;
		this.setNew(isNew);
		this.project = project;
		this.setCopy(isCopy);
		this.roleConfigModelManager = (PafPlannerConfigModelManager) model;
		this.viewModelManager = new ViewModelManager(project);
		this.pafViewTreeModelManager = new PafViewGroupModelManager(project);
		this.customMenuDefModelManager = new CustomMenuDefModelManager(project);
		this.ruleSetModelManager = new RuleSetModelManager(project);
		this.planCycleModel = new PlanCycleModelManager(project);
		this.plannerRoleModel = new PlannerRoleModelManager(project);
		this.customMenuDefLabelProvider =  new CustomMenuDefLabelProvider();
		
		
		this.currentConfig = (PafPlannerConfig) roleConfigModelManager.getItem(key);
		
		if ( isCopy() ) {
			
			if ( this.currentConfig != null ) {
				
				this.currentConfig = this.currentConfig.clone();
								
			}
			
		}
		
		if(currentConfig == null) {
			currentConfig = new PafPlannerConfig();
		}
	}
	
	/**
	 *	Returns all of the available views.
	 * @return A string array of all the available views.
	 */
	public String[] getViews(){
	
		String[] viewNames = viewModelManager.getKeys();
		
		if ( viewNames != null ) {
		
			Arrays.sort(viewNames);
			
		}
		
		return viewNames;
		
	}
		
	/**
	 *	Returns all of the available view groups.
	 * @return A string array of all the available view groups.
	 */
	public String[] getViewGroups(){
		
		String[] viewGroups = pafViewTreeModelManager.getKeys();
		
		if ( viewGroups != null ) {
			
			Arrays.sort(viewGroups);
			
		}
		
		return viewGroups;
	}
	
	/**
	 *	Returns all of the currently selected views and view groups.
	 * @return A string array of all the currently selected views and view groups.
	 */
	public String[] getSelectedViewAndViewGroups(){
		return currentConfig.getViewTreeItemNames();
	}
	
	/**
	 *	Sets the user selected views and view groups.
	 * @param items The string array of selected views and view groups.
	 */
	public void setSelectedViewAndViewGroups(String[] items){
		currentConfig.setViewTreeItemNames(items);
	}
	
	/**
	 * 
	 * Returns the list of visible menu item names.
	 *
	 * @return	A string array of all the visible menu item names
	 */
	public String[] getVisibleMenuItemNames() {
		
		if(currentConfig == null) {
			return null;
		}
		
		return currentConfig.getMenuItemNames();
		
	}
	
	/**
	 * 
	 * Sets the visible menu item names.
	 *
	 * @param visibleMenuItemNames A string array of new visible menu item names
	 */
	public void setVisibleMenuItemNames(String[] visibleMenuItemNames) {
		
		currentConfig.setMenuItemNames(visibleMenuItemNames);
		
	}
	
	/**
	 * 
	 * Returns the list of auto run on save menu item names.
	 *
	 * @return	A string array of all the  auto run on save menu item names
	 */
	public String[] getAutoRunOnSaveMenuItemNames() {
		
		if(currentConfig == null) {
			return null;
		}
		
		return currentConfig.getAutoRunOnSaveMenuItemNames();
		
	}
	
	/**
	 * 
	 * Sets the auto run on save menu item names.
	 *
	 * @param autoRunOnSaveMenuItemNames A string array of new auto run on save menu item names
	 */
	public void setAutoRunOnSaveMenuItemNames(String[] autoRunOnSaveMenuItemNames) {
		
		currentConfig.setAutoRunOnSaveMenuItemNames(autoRunOnSaveMenuItemNames);
		
	}
	
	/**
	 *	Returns all of the currently selected Custom Menu Defs.
	 * @return A string array of all the currently selected Custom Menu Defs.
	 */
	public Object[] getSelectedCustomMenuDefs(){
		if(currentConfig.getMenuItemNames() == null)
			return null;
		
		Object[] items = new Object[currentConfig.getMenuItemNames().length];
		int i = 0;
		for(String customMenu : currentConfig.getMenuItemNames()){
			CustomMenuDef menu = (CustomMenuDef) customMenuDefModelManager.getItem(customMenu);
			if(menu != null){
				items[i] = menu;
			}else{
				items[i] = null;
			}
			i++;
		}
		return items;
	}
	
	/**
	 *	Sets the user seleted custom menus.
	 * @param menus The string array of user seleted custom menus.
	 */
	public void setSelectedCustomMenuDefs(Object[] menus){
		String[] selItems = new String[menus.length];
		for(int i = 0; i < selItems.length; i++){
			selItems[i] = customMenuDefLabelProvider.getKey(menus[i]);
		}
		
		if(selItems != null && selItems.length > 0)
			currentConfig.setMenuItemNames(selItems);
	}
	
	/**
	 *	Returns all of the Custom Menu Defs.
	 * @return A object array of all the Custom Menu Defs.
	 */
	@SuppressWarnings("unchecked")
	public Object[] getCustomMenuDefs(){
		
		return (CustomMenuDef[]) customMenuDefModelManager.getModel().
			values().toArray(new CustomMenuDef[0]);
	}
	
	
	/**
	 * 
	 *  Returns all the custom menu defs.
	 *
	 * @return An array of all the custom menu defs
	 */
	public String[] getCustomMenuDefNames() {
		return (String[]) customMenuDefModelManager.getModel().
		keySet().toArray(new String [0]);
	}
	
	/**
	 *	Returns the current default rule set.
	 * @return A string value consisting of the current default rule set.
	 */
	public String getDefaultRulesetName(){
		return currentConfig.getDefaultRulesetName();
	}
	
	/**
	 *	Sets the user selected default rule set.
	 * @param ruleSetName The string value of the Rule Set name.
	 */
	public void setDefaultRulesetName(String ruleSetName){
		currentConfig.setDefaultRulesetName(ruleSetName);
	}
	
	/**
	 *	Returns all of the selected rule sets.
	 * @return A string array of all the selected rule sets.
	 */
	public String[] getSelectedRulesSets(){
		return currentConfig.getRuleSetNames();
	}
	
	/**
	 *	Sets the user selected rule sets.
	 * @param selectedRuleSets The string array of Rule Sets to set.
	 */
	public void setSelectedRuleSets(String[] selectedRuleSets){
		currentConfig.setRuleSetNames(selectedRuleSets);
	}
	
	/**
	 *	Returns all of the available rule sets.
	 * @return A string array of all the available rule sets
	 */
	public String[] getRulesSets(){
		return ruleSetModelManager.getKeys();
	}
	
	/**
	 * 
	 *	Returns the large cell count
	 *
	 * @return An Integer for large cell count
	 */
	public Integer getLargeCellCount() {
		
		return currentConfig.getUowSizeLarge();
	}
	
	/**
	 * 
	 *	Sets the large cell count.
	 *
	 * @param maxCellCount The Integer for the large cell count
	 */
	public void setLargeCellCount(Integer maxCellCount) {
		currentConfig.setUowSizeLarge(maxCellCount);
	}
	
	/**
	 * 
	 *	Returns the max cell count
	 *
	 * @return An Integer for max cell count
	 */
	public Integer getMaxCellCount() {
		
		return currentConfig.getUowSizeMax();
	}
	
	/**
	 * 
	 *	Sets the max cell count.
	 *
	 * @param maxCellCount The Integer for the max cell count
	 */
	public void setMaxCellCount(Integer maxCellCount) {
		currentConfig.setUowSizeMax(maxCellCount);
	}
	
	/**
	 *	Returns the current plan cycle.
	 * @return A string value with the current plan cycle.
	 */
	public String getPlanCycle(){
		return currentConfig.getCycle();
	}
	
	/**
	 *	Sets the user selected plan cycle.
	 * @param planCycle The plan cycle string value.
	 */
	public void setPlanCycle(String planCycle){
		currentConfig.setCycle(planCycle);
	}
	
	/**
	 *	Returns all the avail plan cycles.
	 * @return A string array of all the available plan cycles.
	 */
	public String[] getPlanCycles(){
		return planCycleModel.getKeys();
	}
	
	/**
	 *	Returns user role.
	 * @return A string value with the users role.
	 */
	public String getRole(){
		return currentConfig.getRole();
	}

	/**
	 *	Sets user role.
	 * @param role A string value with the new user role.
	 */
	public void setRole(String role){
		currentConfig.setRole(role);
	}
	
	/**
	 *	Returns all of the available user roles.
	 * @return A string array of all the available user roles.
	 */
	public String[] getRoles(){
		return plannerRoleModel.getKeys();
	}
	
	/**
	 * Sets the list of version filters.
	 * @param versionFilters An array list of version filters.
	 */
	public void setVersionFilters(ArrayList<String> versionFilters){
		//only set the value if it's not null and > 0.
		if(versionFilters != null && versionFilters.size() > 0){
			String[] str = new String[versionFilters.size()];
			for(int i = 0; i < str.length; i++){
				str[i] = versionFilters.get(i);
			}
			currentConfig.setVersionFilter(str);
		}
		else{
			currentConfig.setVersionFilter(null);
		}
			
	}
	
	/**
	 * Gets the list of version filters.
	 * @return An arraylist of all the selected version filters.
	 */
	public ArrayList<String> getVersionFilters(){
		//check for a null, if so return null
		if(currentConfig.getVersionFilter() == null)
			return null;
		//init the array list
		ArrayList<String> versionFilterList = new ArrayList<String>();
		//create the arraylist from the array.
		for(String str : currentConfig.getVersionFilter()){
			versionFilterList.add(str);
		}
		//return the array list.
		return versionFilterList;
	}

	/**
	 * Sets the list of reference version.
	 * @param refVersions An array list of reference versions.
	 */
	public void setRefVersions(ArrayList<String> refVersions){
		//only set the value if it's not null and > 0.
		if(refVersions != null && refVersions.size() > 0){
			String[] str = new String[refVersions.size()];
			for(int i = 0; i < str.length; i++){
				str[i] = refVersions.get(i);
			}
			currentConfig.setDefaultEvalRefVersions(str);
		}
		else{
			currentConfig.setDefaultEvalRefVersions(null);
		}
			
	}
	
	/**
	 * Gets the list of reference versions.
	 * @return An arraylist of all the selected reference versions.
	 */
	public ArrayList<String> getRefVersions(){
		//check for a null, if so return null
		if(currentConfig.getDefaultEvalRefVersions() == null)
			return null;
		//init the array list
		ArrayList<String> refVersionList = new ArrayList<String>();
		//create the arraylist from the array.
		for(String str : currentConfig.getDefaultEvalRefVersions()){
			refVersionList.add(str);
		}
		//return the array list.
		return refVersionList;
	}
	
	public String[] getCachedAdminLocks() {
		return new AdminLockModelManager(project).getKeys();
	}
	
	public String[] getSelectedAdminLocks() {
		return currentConfig.getAdminPersistLockNames();
	}
	
	public void setSelectedAdminLocks(String[] adminLocks) {
		currentConfig.setAdminPersistLockNames(adminLocks);
	}
	/**
	 *  Returns true if default eval plan version eval is enabled
	 *
	 * @return true/false
	 */
	public boolean isDefaultEvalEnabledWorkingVersion() {
		return currentConfig.isDefaultEvalEnabledWorkingVersion();
	}
	
	/**
	 *  Sets the default eval plan version eval
	 *
	 * @param isDefaultEvalEnabledWorkingVersion true/false
	 */
	public void setDefaultEvalEnabledWorkingVersion(boolean isDefaultEvalEnabledWorkingVersion) {
		
		currentConfig.setDefaultEvalEnabledWorkingVersion(isDefaultEvalEnabledWorkingVersion);
		
	}
	
	/**
	 *  Gets the mdb save on uow load
	 *
	 * @return
	 */
	public boolean isMdbSaveWorkingVersionOnUowLoad() {
		return currentConfig.isMdbSaveWorkingVersionOnUowLoad();
	}
	
	/**
	 *  Sets the mdb save on uow load
	 *
	 * @param isMdbSaveOnUowLoad
	 */
	public void setMdbSaveWorkingVersionOnUowLoad(boolean isMdbSaveWorkingVersionOnUowLoad) {
		currentConfig.setMdbSaveWorkingVersionOnUowLoad(isMdbSaveWorkingVersionOnUowLoad);
	}
	/**
	 * 
	 *  Sets the role filters names.
	 *
	 * @param roleFilters An array of role filter names
	 */
	public void setRoleFilters(String[] roleFilters) {
						
		UserFilterSpec userFilterSpec = new UserFilterSpec();
		
		userFilterSpec.setAttrDimNames(roleFilters);
		
		currentConfig.setUserFilterSpec(userFilterSpec);
		
	}
	
	/**
	 * 
	 * Returns an array of role filters names.
	 *
	 * @return A string array of role filters names.
	 */
	public String[] getRoleFilters() {
		
		if ( currentConfig.getUserFilterSpec() == null ) {
			return null;
		}
		
		return currentConfig.getUserFilterSpec().getAttrDimNames();
	
	}
	
	/**
	 * 
	 * Is role filter allowed
	 *
	 * @return true/false if role filter is allowed
	 */
	public Boolean isAllowRoleFilter() {
	
		//return currentConfig.isUserFilteredUow();
		return currentConfig.getIsUserFilteredUow();
	}
	
	/**
	 * 
	 *  Set if role filter is allowed
	 *
	 * @param isAllowRoleFilter true/false
	 */
	public void setAllowRoleFilter(Boolean isAllowRoleFilter) {
		
		currentConfig.setIsUserFilteredUow(isAllowRoleFilter);
		
	}
	
	/**
	 * 
	 * Is role filter allowed
	 *
	 * @return true/false if role filter is allowed
	 */
	public Boolean IsUserFilteredMultiSelect() {
	
		//return currentConfig.isUserFilteredUow();
		return currentConfig.getIsUserFilteredMultiSelect();
	}
	
	/**
	 * 
	 *  Set if role filter is allowed
	 *
	 * @param isAllowRoleFilter true/false
	 */
	public void setUserFilteredMultiSelect(Boolean isUserFilteredMultiSelect) {
		
		currentConfig.setIsUserFilteredMultiSelect(isUserFilteredMultiSelect);
		
	}
	
	/**
	 * 
	 * Returns true/fals based on allow suppress invalid inserstecions
	 *
	 * @return Returns true/fals based on allow suppress invalid inserstecions
	 */
	public Boolean isAllowSuppressInvalidIntersections() {
		
		//return currentConfig.isDataFilteredUow();
		return currentConfig.getIsDataFilteredUow();
		
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param isAllowSuppressInvalidIntersections
	 */
	public void setAllowSuppressInvalidIntersections(Boolean isAllowSuppressInvalidIntersections) {
		currentConfig.setIsDataFilteredUow(isAllowSuppressInvalidIntersections);
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @return
	 */
	public PafDimSpec[] getSuppressInvalidIntersections() {
		
		if ( currentConfig.getDataFilterSpec() == null || currentConfig.getDataFilterSpec().getDimSpec() == null) {
			return null;
		}
					
		return currentConfig.getDataFilterSpec().getDimSpec();
	}
	
	/**
	 * 
	 * Creates a new Data Filter Spec and sets the dim spec from the argument.
	 *
	 * @param pafDimSpecs An array of paf dim specs
	 */
	public void setSuppressInvalidIntersections(PafDimSpec[] pafDimSpecs) {
	
		DataFilterSpec dataFilterSpec = new DataFilterSpec();
		
		dataFilterSpec.setDimSpec(pafDimSpecs);
		
		currentConfig.setDataFilterSpec(dataFilterSpec);
		
	}
	
	/**
	 * 
	 *  Test if replicate is enabled
	 *
	 * @return True/False/Null
	 */
	public Boolean getReplicateEnabled() {
		
		if ( currentConfig == null) {
			return null;
		}
		
		return currentConfig.getReplicateEnabled();
	}
	
	/**
	 * 
	 * Sets if replicate is enabled/disabled or turned off with null
	 *
	 * @param replicateEnabled True/False/Null
	 */
	public void setReplicateEnabled(Boolean replicateEnabled) {
		
		currentConfig.setReplicateEnabled(replicateEnabled);
		
	}

	/**
	 * 
	 *  Test if replicate all is enabled
	 *
	 * @return True/False/Null
	 */
	public Boolean getReplicateAllEnabled() {
		
		if ( currentConfig == null) {
			return null;
		}
		
		return currentConfig.getReplicateAllEnabled();
	}

	/**
	 * 
	 * Sets if replicate all is enabled/disabled or turned off with null
	 *
	 * @param replicateAllEnabled True/False/Null
	 */
	public void setReplicateAllEnabled(Boolean replicateAllEnabled) {
		
		currentConfig.setReplicateAllEnabled(replicateAllEnabled);
		
	}
	
	/**
	 * 
	 *  Test if Lift is enabled
	 *
	 * @return True/False/Null
	 */
	public Boolean getLiftEnabled() {
		
		if ( currentConfig == null) {
			return null;
		}
		
		return currentConfig.getLiftEnabled();
	}
	
	/**
	 * 
	 * Sets if Lift is enabled/disabled or turned off with null
	 *
	 * @param replicateEnabled True/False/Null
	 */
	public void setLiftEnabled(Boolean liftEnabled) {
		
		currentConfig.setLiftEnabled(liftEnabled);
		
	}

	/**
	 * 
	 *  Test if lift all is enabled
	 *
	 * @return True/False/Null
	 */
	public Boolean getLiftAllEnabled() {
		
		if ( currentConfig == null) {
			return null;
		}
		
		return currentConfig.getLiftAllEnabled();
	}

	/**
	 * 
	 * Sets if replicate all is enabled/disabled or turned off with null
	 *
	 * @param replicateAllEnabled True/False/Null
	 */
	public void setLiftAllEnabled(Boolean liftAllEnabled) {
		
		currentConfig.setLiftAllEnabled(liftAllEnabled);
		
	}

	/**
	 * 
	 * Sets if Lift is enabled/disabled or turned off with null
	 *
	 * @param replicateEnabled True/False/Null
	 */
	public void setFilteredSubtotals(Boolean filteredSubs) {
		
		currentConfig.setIsFilteredSubtotals(filteredSubs);
		
	}

	/**
	 * 
	 *  Test if FIltered Subtotals is enabled
	 *
	 * @return True/False/Null
	 */
	public Boolean getFilteredSubtotals() {
		
		if ( currentConfig == null) {
			return null;
		}
		
		return currentConfig.getIsFilteredSubtotals();
	}
	
	/**
	 * Returns a custom menu def label provider.
	 * @return The custom menu def label provider.
	 */
	public CustomMenuDefLabelProvider getCustomMenuDefLabelProvider(){
		return customMenuDefLabelProvider;
	}
	
	/**
	 * Save the current RoleConfigurationModelManager.
	 * @exception java.lang.Exception
	 */
	public void save() throws Exception {
		
		String role = currentConfig.getRole();
		String planCycle = currentConfig.getCycle();
		
		String generatedKey = roleConfigModelManager.getKey(role, planCycle);
		
		if(isNew() || isCopy()){
			
			roleConfigModelManager.add(generatedKey, currentConfig);
			
		} 
		
		try{
			
			roleConfigModelManager.save();
					
			this.key = generatedKey;
			
			setNew(false);
			setCopy(false);
		} catch(Exception e){
			throw e;
		}
		
	}
		
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	/**
	 * Searches the model to see if an plan cycle already exists.
	 * @param role The role name.
	 * @param cycle The plan cycle name.
	 * @return true if the role configuration already exists, false if not.
	 */
	public boolean itemExists(String role, String cycle){
		
		return roleConfigModelManager.contains(roleConfigModelManager.getKey(role, cycle));

	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}
	
	public String getKey(String role, String planCycle) {
		
		return this.roleConfigModelManager.getKey(role, planCycle);
		
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project The project to set.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * @return the isCopy
	 */
	public boolean isCopy() {
		return isCopy;
	}

	/**
	 * @param isCopy the isCopy to set
	 */
	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	/**
	 * @return the readOnlyMeasures
	 */
	public String[] getReadOnlyMeasures() {
		return currentConfig.getReadOnlyMeasures();
	}

	/**
	 * @param readOnlyMeasures the readOnlyMeasures to set
	 */
	public void setReadOnlyMeasures(String[] readOnlyMeasures) {
		currentConfig.setReadOnlyMeasures(readOnlyMeasures);
	}
	
	
	
}
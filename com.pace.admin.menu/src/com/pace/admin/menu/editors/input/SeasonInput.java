/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.MdbDefModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.app.MdbDef;
import com.pace.base.app.Season;
import com.pace.base.data.ExpOpCode;
import com.pace.base.data.ExpOperation;
import com.pace.server.client.PafSimpleDimTree;

/**
* IEditorInput implementation for the Seasons.
* @author kmoos
* @version	x.xx
*/
public class SeasonInput implements IEditorInput {
	
	private final static Logger logger = Logger.getLogger(SeasonInput.class);
	
	private SeasonModelManager seasonsModelManager;
	private MdbDefModelManager mdbDefModelManager;
	private IProject project;
	private MdbDef mdbDef;
	private boolean isNew;
	private boolean isCopy;
	private boolean serverRunning;
	private String key;
	private Season currentSeason;
	private PafSimpleDimTree pafSimpleTreeYear;
	private PafSimpleDimTree pafSimpleTreeTime;
	private ExpOpCode memberExpCode;
	private String[] timePeriodParms;
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 */
	public SeasonInput(String item, ModelManager model, IProject project, boolean isNew) {
		this(item, model, project, isNew, false);
	}
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 * @param isCopy isCopy boolean, is this a copy season. 
	 */
	public SeasonInput(String item, ModelManager model, IProject project, boolean isNew, boolean isCopy) {
		
		super();
		this.key = item;
		this.setNew(isNew);
		this.seasonsModelManager = (SeasonModelManager) model;
		this.mdbDefModelManager = new MdbDefModelManager(project);
		this.mdbDef = this.mdbDefModelManager.getMdbDef();
		this.project = project;
		this.setCopy(isCopy);
		
		currentSeason = (Season) seasonsModelManager.getItem(item);
		
		//if copy, clone current season and added copy to the id
		if ( isCopy() ) {
			
			if ( currentSeason != null ) {
				
				currentSeason = currentSeason.clone();
				
				if ( currentSeason.getId() != null ) {
					
					currentSeason.setId(Constants.COPY_TO + currentSeason.getId());
					
				}
				
			}
			
		}
		
		if(currentSeason == null) {
			currentSeason = new Season();
		}
		
		//parse out the time string seleciton
//		parseTimeString();
		//build the year, and time simpletrees.
//		buildSimpleTrees();
		
	}

	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 * @param isCopy isCopy boolean, is this a copy season. 
	 */
	public SeasonInput(IProject project) {
		this.project = project;
		seasonsModelManager = new SeasonModelManager(project);
		mdbDefModelManager = new MdbDefModelManager(project);
		mdbDef = mdbDefModelManager.getMdbDef();
	}

	/**
	 * Save the current SeasonModelManager.
	 * @exception java.lang.Exception
	 */
	public void save() throws Exception {

		currentSeason.setTimePeriod(getMemberWithSelection());
		if(isNew() || isCopy()){
			seasonsModelManager.add(currentSeason.getId(), currentSeason);
		} else if(! key.equalsIgnoreCase(currentSeason.getId())){
			PlannerRoleModelManager plannerRoleModelManager = new PlannerRoleModelManager(project);
			plannerRoleModelManager.renameSeasonId(key, currentSeason.getId());
		}
		
		try{
			seasonsModelManager.save();
			this.key = currentSeason.getId();
			setNew(false);
			setCopy(false);
		} catch(Exception e){
			throw e;
		}
	}
	
	public MdbDef getMdbDef() {
		return mdbDef;
	}

	public void setMdbDef(MdbDef mdbDef) {
		this.mdbDef = mdbDef;
	}

	/**
	 * Gets the open status value.
	 * @return The open status.
	 */
	public boolean isOpenStatus() {
		return currentSeason.isOpen();
	}

	/**
	 * Sets the open status.
	 * @param openStatus true if open, false if not.
	 */
	public void setOpenStatus(boolean openStatus) {
		currentSeason.setOpen(openStatus);
	}

	/**
	 * Gets the plan cycle for the current season.
	 * @return The plan cycle for the current season.
	 */
	public String getPlanCycle() {
		return currentSeason.getPlanCycle();
	}
	
	/**
	 * Gets all of the available plan cycles.
	 * @return All the available plan cycles.
	 */
	public String[] getPlanCycles(){
		PlanCycleModelManager manager = new PlanCycleModelManager(project);
		return manager.getKeys();
	}

	/**
	 * Sets the plan cycle for the current season.
	 * @param planCycle The plan cycle.
	 */
	public void setPlanCycle(String planCycle) {
		currentSeason.setPlanCycle(planCycle);
	}

	/**
	 * Gets the name of the time dimension.
	 * @return The name of the time diemension.
	 */
	public String getTimeDimName(){
		return mdbDef.getTimeDim();
	}
	
	/**
	 * Gets the time periods simple tree.
	 * @return A paf simple tree for the time dimension.
	 */
	public PafSimpleDimTree getPafSimpleTreeTime() {
		return pafSimpleTreeTime;
	}
	
	/**
	 * Gets the time periods for the current season.
	 * @return The time period value.
	 */
	public String getTimePeriod() {
		if(timePeriodParms != null && timePeriodParms.length > 0){
			return timePeriodParms[0];
		}else{
			return null;
		}
	}

	/**
	 * Sets the time period for the current season.
	 * @param timePeriod The time period.
	 */
	public void setTimePeriod(String timePeriod) {
		if(timePeriod != null){
			timePeriodParms[0] = timePeriod;
		} else {
			timePeriodParms[0] = null;
		}
	}

	/**
	 * Gets the level of the time period selection.
	 * @return The level, or null if no level is present.
	 */
	public Integer getTimePeriodLevel() {
		if(timePeriodParms != null && timePeriodParms[1] != null && timePeriodParms.length > 0){
			return Integer.valueOf(timePeriodParms[1]);
		}else{
			return null;
		}
	}

	/**
	 * Sets the time period level.
	 * @param timePeriodLevel The time period level.
	 */
	public void setTimePeriodLevel(int timePeriodLevel) {
		if(timePeriodParms != null && timePeriodParms.length > 0){
			timePeriodParms[1] = Integer.toString(timePeriodLevel);
		}else{
			timePeriodParms[1] = null;
		}
	}

	/**
	 * Gets the member selection type for the time selection.
	 * @return The member selection type (selection, ICHILD, IDESC)
	 */
	public ExpOpCode getMemberSelectionType() {
		return memberExpCode;
	}

	/**
	 * Sets the member selection type.
	 * @param memberSelectionType The member selection type for the time period.
	 */
	public void setMemberSelectionType(ExpOpCode memberSelectionType) {
		memberExpCode = memberSelectionType;
	}
	
	/**
	 * Gets the name of the year dimension.
	 * @return The name of the year dimension.
	 */
	public String getYearDimName(){
		return mdbDef.getYearDim();
	}
	
	/**
	 * Gets all of the available years.
	 * @return All of the available years.
	 */
	public String[] getYears() {
		try {
			//Strip off the dimension root, if it's present.
			if(pafSimpleTreeYear != null && pafSimpleTreeYear.getTraversedMembers().size() > 0){
				if(pafSimpleTreeYear.getTraversedMembers().get(0).equalsIgnoreCase(mdbDef.getYearDim())){
					
					// updated to use a hashmap for Years to avoid duplicate members from Years dimension to show up in Year editior when there are shared Years in the outline.
					HashMap<String,String> yearsMap = new HashMap<String,String>();
					List<String> years = new ArrayList<String>();
					// Modified the for to include the last year from the Years dimension TTN-2477.
					for(int i=1;i<pafSimpleTreeYear.getTraversedMembers().size();i++)
					{
						if(!yearsMap.containsKey(pafSimpleTreeYear.getTraversedMembers().get(i))) 
						{
						yearsMap.put(pafSimpleTreeYear.getTraversedMembers().get(i), pafSimpleTreeYear.getTraversedMembers().get(i));
						years.add(pafSimpleTreeYear.getTraversedMembers().get(i));
						}
						
					}
				
					return years.toArray(new String[0]);
				} else{
					return pafSimpleTreeYear.getTraversedMembers().toArray(new String[0]);
				}
			}
			else{
				return null;
			}
		}catch(Exception e){
			
			logger.error(e.getMessage());
			
			return null;
		}
	}
	
	/**
	 * Gets the selected year for the current season.
	 * @return The selected year.
	 */
	public String getYear() {
		return currentSeason.getYear();
	}

	/**
	 * Sets the selected year for the current season.
	 * @param year - The selected year.
	 */
	public void setYear(String year) {
		currentSeason.setYear(year);
	}

	/**
	 * Gets the selected years for the current season.
	 * @return The selected years.
	 */
	public String[] getSelectedYears() {
		return currentSeason.getYears();
	}

	/**
	 * Sets the selected years for the current season.
	 * @param years - The selected years.
	 */
	public void setSelectedYears(String[] years) {
		currentSeason.setYears(years);
	}

	/**
	 * Gets the plannable years for the current season.
	 * @return The plannable years.
	 */
	public String[] getPlannableYears() {
		return currentSeason.getPlannableYears();
	}

	/**
	 * Sets the plannable years for the current season.
	 * @param year - The plannable years.
	 */
	public void setPlannableYears(String[] years) {
		currentSeason.setPlannableYears(years);
	}

	/**
	 * Gets the season id for the current season.
	 * @return The season id.
	 */
	public String getSeasonId() {
		return currentSeason.getId();
	}
	
	/**
	 * Sets the season id.
	 * @param id The season id.
	 */
	public void setSeasonId(String id){
		currentSeason.setId(id);
	}
	
	/**
	 * Gets the stats of the PAF Server.
	 * @return true if the PAF server is running, false if not.
	 */
	public boolean isServerRunning(){
		return serverRunning;
	}
	
	/**
	 * Searches the model to see if an season already exists.
	 * @param key The season name to search for.
	 * @return true if the season already exists, false if not.
	 */
	public boolean itemExists(String key){
		return seasonsModelManager.contains(key);
	}
	
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
		
	/**
	 * @return the isCopy
	 */
	public boolean isCopy() {
		return isCopy;
	}

	/**
	 * @param isCopy the isCopy to set
	 */
	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	/**
	 * Parses the time string into an array (@IDESC(foo, 2), S1, <@>ICHILD(IDESC)
	 */
	public void parseTimeString(){
		//init a 2 dim array.  pos 0 is for the member, and pos 1 is for the level - if needed.
		timePeriodParms = new String[2];
		//create a member exp operation
		ExpOperation memberExp;
		
		if(currentSeason.getTimePeriod() != null && currentSeason.getTimePeriod().indexOf("@") > -1){
			//if the time period is not null, and contains the "@" symbol. then parse
			//the expression into the array. (ICHILD or IDESC)
			memberExp = new ExpOperation(currentSeason.getTimePeriod());
			for(int i =0; i < memberExp.getParms().length; i++){
				timePeriodParms[i] = memberExp.getParms()[i];
			}
			memberExpCode = memberExp.getOpCode();
		} else if(currentSeason.getTimePeriod() != null && currentSeason.getTimePeriod().indexOf("@") == - 1){
			//we have a selection (! IDESC && ! ICHILD)
			timePeriodParms[0] = currentSeason.getTimePeriod();
			timePeriodParms[1] = null;
			memberExpCode = null;
		} else {
			//we have nothing so just null out the array (this is the case if 
			//the user is creating a new season).
			timePeriodParms[0] = null;
			timePeriodParms[1] = null;
			memberExpCode = null;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private String getMemberWithSelection(){
		if(memberExpCode == null){
			//selection only.
			return timePeriodParms[0];
		}else {
			if(timePeriodParms[1] == null){
				//no level
				return '@' + memberExpCode.toString() + '(' + timePeriodParms[0] + ')';
			} else{
				//level
				return '@' + memberExpCode.toString() + '(' + timePeriodParms[0] + ',' + 
					timePeriodParms[1] + ')';
			}
		}
	}
	
	/**
	 * Builds the year and time simple trees if the server or cache is available.
	 * If the server and cache are not available, then the serverRunning property is
	 * set to false.
	 */
	public void buildSimpleTrees(){
		try{
			pafSimpleTreeYear = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
					project, PafProjectUtil.getApplicationName(project),
					mdbDef.getYearDim(), 
					false);
			
			pafSimpleTreeTime = (PafSimpleDimTree) DimensionTreeUtility.getDimensionTree(
					project, PafProjectUtil.getApplicationName(project), 
					mdbDef.getTimeDim(), 
					false);
			
			serverRunning = true;
			
		} catch(ServerNotRunningException ex){
			logger.error("Server not running.");
			serverRunning = false;
		} catch(Exception ex){
			logger.error("Couldn't get cached simple tree. Login into server and cache dimensions.");
			serverRunning = false;
		}
	}
	
}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.model.managers.PafViewGroupModelManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.base.view.PafViewGroup;
import com.pace.base.view.PafViewGroupItem;

/**
 * A view group editor's input
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ViewGroupEditorInput implements IEditorInput {
	
	private boolean isNew;
	private String key;
	private IProject project = null;	
	private PafViewGroupModelManager viewGroupModelManager = null;
	private ViewModelManager viewModelManager = null;
	private String[] viewGroupItems = null;
	
	
	public ViewGroupEditorInput(String key, IProject project, boolean isNew) {

		this.key = key;
		this.project = project;
		this.isNew = isNew;
		
		viewGroupModelManager = new PafViewGroupModelManager(project);
		viewModelManager = new ViewModelManager(project);
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	public String getName() {
		return key;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	public String getToolTipText() {
		return key;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	/**
	 * 
	 *	Get's every view name
	 *
	 * @return array of view names
	 */
	public String[] getViewNames() {
		
		String[] viewNames = null;
		
		if ( viewModelManager != null ) {
			
			viewNames = viewModelManager.getKeys();
			
		}
		
		return viewNames;
	}
	
	/**
	 * 
	 *	Get's every View Group name
	 *
	 * @return array of view group names
	 */
	public String[] getViewGroupNames() {
		
		String[] viewGroupNames = null;
		
		if ( viewGroupModelManager != null ) {
			
			viewGroupNames = viewGroupModelManager.getSortedKeys();
			
		}
		
		return viewGroupNames;
	}

	/**
	 * @return Returns the isNew.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * @param isNew The isNew to set.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	/**
	 * @return Returns the project.
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * @param project The project to set.
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
	
	/**
	 * 
	 *	Get's a view group
	 *
	 * @param viewGroupName
	 * @return
	 */
	public PafViewGroup getViewGroup(String viewGroupName) {
		
		PafViewGroup pafViewGroup = null;
		
		if ( viewGroupModelManager != null && viewGroupModelManager.contains(viewGroupName)) {
			
			pafViewGroup = (PafViewGroup) viewGroupModelManager.getItem(viewGroupName);		
		}
		
		return pafViewGroup;
		
	}
	
	/**
	 * 
	 *	Set's the view group items.
	 *
	 * @param items
	 */
	public void setViewGroupItems(String[] items) {
		this.viewGroupItems  = items;
	}
	
	public void save() {
		
		if (viewGroupModelManager != null ) {
		
			PafViewGroup pafViewGroup = null;
			
			if ( isNew() ) {
				
				pafViewGroup = new PafViewGroup();
				
				pafViewGroup.setName(this.key);											
				
				pafViewGroup.setPafViewGroupItems(createPafViewGroupItems());
				
				viewGroupModelManager.add(this.key, pafViewGroup);
				
			} else if ( viewGroupModelManager.contains(this.key) ) {
					
					pafViewGroup = (PafViewGroup) viewGroupModelManager
							.getItem(this.key);
								
					pafViewGroup.setPafViewGroupItems(createPafViewGroupItems());	
					
					viewGroupModelManager.replace(this.key, pafViewGroup);
			} 		
		
			viewGroupModelManager.save();
			
			setNew(false);

		}
		
	}
	
	private PafViewGroupItem[] createPafViewGroupItems() {
		
		List<PafViewGroupItem> pafViewGroupItemList = new ArrayList<PafViewGroupItem>();
		
		if (viewGroupItems != null) {
						
			for (String viewGroupItem : viewGroupItems) {

				PafViewGroupItem newViewGroupItem = new PafViewGroupItem();
				newViewGroupItem.setName(viewGroupItem);
				newViewGroupItem.setViewGroup(viewGroupModelManager.contains(viewGroupItem));
				
				pafViewGroupItemList.add(newViewGroupItem);

			}
					
		}
		
		return pafViewGroupItemList.toArray(new PafViewGroupItem[0]);
		
	}

	public boolean validViewGroupName(String text) {
				
		for (String viewName : getViewNames() ) {
			
			if ( viewName.equalsIgnoreCase(text)) {
				return false;
			}
			
		}
		
		for (String viewGroupName : getViewGroupNames() ) {
			
			if ( viewGroupName.equalsIgnoreCase(text)) {
				return false;
			}
			
		}
		
		
		return true;
	}

	/**
	 * @return Returns the key.
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key The key to set.
	 */
	public void setKey(String key) {
		this.key = key;
	}
	}

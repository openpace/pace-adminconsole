/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.ui.PrintStyle;
import com.pace.base.ui.PrintStyles;
import com.pace.base.utility.GUIDUtil;

public class GlobalPrintStyleInput extends PrintStyleInput {
	private final static Logger logger = Logger.getLogger(GlobalPrintStyleInput.class);

	public GlobalPrintStyleInput(String item, PrintStyles model, IProject project) {
		this(item, model, project, false);
		// TODO Auto-generated constructor stub
	}
	public GlobalPrintStyleInput(String item, PrintStyles model, IProject project, boolean isNew) {
		this(item, model, project, isNew, false);
		// TODO Auto-generated constructor stub
	}

	public GlobalPrintStyleInput(String item, PrintStyles model, IProject project, boolean isNew, boolean isCopy) {
		super(item, model, project, isNew, isCopy);
		// TODO Auto-generated constructor stub
		if( isNew() ) {
			printStyle = ((PrintStyle)((PrintStyles)model).getDefaultPrintStyle()).clone();
			printStyle.setGUID(GUIDUtil.getGUID());
			printStyle.setName("");
			printStyle.setDefaultStyle(false);
		}
		else{
			printStyle = (PrintStyle) ((PrintStyles)model).getPrintStyleByName(item);
			//if copy, try to clone
			if ( isCopy()) {
				printStyle = (PrintStyle) printStyle.clone();
				if(printStyle != null) {
					String copyName = Constants.COPY_TO + printStyle.getName();
					printStyle.setGUID(GUIDUtil.getGUID());
					printStyle.setName(copyName);
					if( printStyle.getDefaultStyle() ) //only 1 default, don't want to copy default property
						printStyle.setDefaultStyle(false);
				}
			}			
		}
		if(printStyle == null) {
			printStyle = new PrintStyle();
		}
	}
	
	@Override
	public void save() throws Exception {
		if(isNew() || isCopy() ){
			String printStyleName = printStyle.getName().trim();
			if ( printStyleName.isEmpty() ) {
		    	GUIUtil.openMessageWindow( "Save Error", "Please enter print style name.");
				return;
			}
			else if( ((PrintStyles)model).findDuplicatePrintStyle(printStyle) != null ) {
		    	GUIUtil.openMessageWindow( "Save Error",
			    		"Print style '" + printStyleName+ "' already exists. Please use a different name.");
				return;
			}
			if( isNew() ) {
				//if there is no default print style, then set this one as default print style
				if( ! ((PrintStyles)model).findDefaultPrintStyle() ) {
					printStyle.setDefaultStyle(true);
				}
				//if there is default print style already, then turn the default flag off for this print style
				else {
					printStyle.setDefaultStyle(false);
				}
			}
			try{
				((PrintStyles)model).add(printStyle.getGUID(), printStyle);
			} catch(Exception e){
				throw e;
			}
		} 
		else { //modifying on an existing print style
			if( printStyle.getDefaultStyle() ) {
				String outMessage = "'" + printStyle.getName() + "' is a default print style and it has been changed.";
				ViewModelManager viewModel = new ViewModelManager(project);
				List<PafAdminConsoleView> listViews = viewModel.findPrintStyleInViewsByGUID(printStyle.getGUID());
				//if find views that contain this print style, then pop up a warning message
				if( listViews != null && listViews.size() > 0 ) {
					String viewNames = "";
					for( PafAdminConsoleView view : listViews ) {
						viewNames += "'" + view.getName() + "', ";
					}
					outMessage +=  " This style is currently associated with views: " + viewNames + ".";  
				}
				GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
			}
		}
		try {
			model.save();
		} catch(Exception e){
			logger.error(e.getMessage());			
			throw e;
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.base.PafBaseConstants;
import com.pace.base.ui.DefaultPrintSettings;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.ui.PrintStyle;
import com.pace.base.utility.GUIDUtil;

public class ViewPrintSettingsInput extends PrintStyleInput {
	private final static Logger logger = Logger.getLogger(ViewPrintSettingsInput.class);

	public ViewPrintSettingsInput(String item, ViewModelManager model,	IProject project) {
		// TODO Auto-generated constructor stub
		super(item,model,project);
		
		PafAdminConsoleView view = (PafAdminConsoleView)model.getItem(key);
		printStyle = view.getPrintStyle();
		if(printStyle == null) {
			printStyle = new PrintStyle();
			printStyle = DefaultPrintSettings.getInstance().getDefaultPrintSettings();
			printStyle.setGUID(GUIDUtil.getGUID());
			printStyle.setName(PafBaseConstants.EMBEDED_PRINT_SETTINGS);
			printStyle.setDefaultStyle(false);
		}
	}

	public String getPrintStyleName() {
		return PafBaseConstants.EMBEDED_PRINT_SETTINGS;
	}
	
	@Override
	public void save() throws Exception {
	//TODO
	//Use the ViewModelManager to save	
		PafAdminConsoleView view = (PafAdminConsoleView)model.getItem(key);
		view.setPrintStyle(printStyle);
		try {
			model.save();
		} catch(Exception e){
			logger.error(e.getMessage());			
			throw e;
		}
	}
}

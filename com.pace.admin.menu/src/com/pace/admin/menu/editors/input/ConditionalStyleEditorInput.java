/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.editors.input;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.pace.admin.global.model.managers.ConditionalStyleModelManager;
import com.pace.admin.global.model.managers.ModelManager;
import com.pace.base.format.IPaceConditionalStyle;

/**
* IEditorInput implementation for the Seasons.
* @author ihuang
* @version	x.xx
*/
public class ConditionalStyleEditorInput implements IEditorInput {
	
	private final static Logger logger = Logger.getLogger(ConditionalStyleEditorInput.class);
	
	private IProject project;
	private String key;
	private boolean isNew;
	private boolean isCopy;
	
	private ConditionalStyleModelManager condStyleModelManager;
	public ConditionalStyleModelManager getCondStyleModelManager() {
		return condStyleModelManager;
	}

	public void setCondStyleModelManager(
			ConditionalStyleModelManager condStyleModelManager) {
		this.condStyleModelManager = condStyleModelManager;
	}

	private IPaceConditionalStyle conditionalStyle;
	
	public IPaceConditionalStyle getConditionalStyle() {
		return conditionalStyle;
	}

	public void setConditionalStyle(IPaceConditionalStyle conditionalStyle) {
		this.conditionalStyle = conditionalStyle;
	}

	public IPaceConditionalStyle getConditionalStyle(String styleName) {
		return condStyleModelManager.getConditionalStyleByName(styleName);
	}

	public void addConditionalStyle(IPaceConditionalStyle conditionalStyle) {
		condStyleModelManager.addConditionalStyle(conditionalStyle);
	}
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 */
	public ConditionalStyleEditorInput(String item, ModelManager model, IProject project) {
		this(item, model, project, false, false);
	}
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 */
	public ConditionalStyleEditorInput(String item, ModelManager model, IProject project, boolean isNew) {
		this(item, model, project, isNew, false);
	}
	
	/**
	 * 
	 * Season Input for Season Editor Input
	 * 
	 * @param item The text value of the season.
	 * @param model The season model manager.
	 * @param project The current IProject.
	 * @param isNew isNew boolean, is this a new season.
	 * @param isCopy isCopy boolean, is this a copy season. 
	 */
	public ConditionalStyleEditorInput(String item, ModelManager model, IProject project, boolean isNew, boolean isCopy) {
		
		super();
		this.key = item;
		this.condStyleModelManager = (ConditionalStyleModelManager) model;
		this.project = project;
		this.setNew(isNew);
		this.setCopy(isCopy);
		this.conditionalStyle = this.condStyleModelManager.getConditionalStyleByName(item);
		
	}

	/**
	 * Save the current SeasonModelManager.
	 * @exception java.lang.Exception
	 */
	public void save() throws Exception {
		if(! isNew() && ! isCopy() && conditionalStyle != null && ! key.equalsIgnoreCase(conditionalStyle.getName())){
			condStyleModelManager.renameConditionalStyle(key, conditionalStyle.getName());
		}
		
		condStyleModelManager.add(conditionalStyle.getGuid(), conditionalStyle);
		
		try{
			this.key = conditionalStyle.getName();
			condStyleModelManager.save();
		} catch(Exception e){
			throw e;
		}
	}
	
	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	
	/**
	 * Is this a new EditorInput.
	 * @return true if the EditorInput is new, false if not.
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * Sets the status of this EditorInput.
	 * @param isNew true if this is new, false if not.
	 */
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	/**
	 * @return
	 */
	public boolean exists() {
		return false;
	}

	/**
	 * @return
	 */
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * @return
	 */
	public String getName() {
		return key;
	}

	/**
	 * @return
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * @return
	 */
	public String getToolTipText() {
		return key;
	}

	/**
	 * @param adapter
	 * @return
	 */
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
		
	/**
	 * @return the isCopy
	 */
	public boolean isCopy() {
		return isCopy;
	}

	/**
	 * @param isCopy the isCopy to set
	 */
	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	
}
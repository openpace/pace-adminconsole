/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.menu.project.ProjectSource;
import com.pace.admin.menu.wizards.FileSelectionWizardPage;
import com.pace.admin.menu.wizards.GenericSelectionOptionsWizardPage;
import com.pace.admin.menu.wizards.ProjectDataSelectionPage;
import com.pace.admin.menu.wizards.ProjectErrorsDialog;
import com.pace.admin.menu.wizards.ProjectExportTypeSelectionPage;
import com.pace.admin.menu.wizards.ProjectNameSelectionWizardPage;
import com.pace.admin.menu.wizards.ProjectSourceOptionsWizardPage;
import com.pace.base.project.ProjectDataError;

/**
 * A wizard utility that creates pages and other wizard type things.
 * 
 * @author jmilliron
 *
 */
public class ProjectWizardUtil {
	
	public static String getTemplatePath() {
		return Constants.ADMIN_CONSOLE_HOME + Constants.TEMPLATES_DIR;
	}
	
	
	public static List<File> getTemplates(){
		
		List<File> templates = new ArrayList<File>();
		
		File dir = new File(getTemplatePath());
		
		if(dir != null && dir.listFiles() != null){
			
			for (File fromFile : dir.listFiles()){
				
				if(fromFile.isFile()){
				
					if(fromFile.getName().endsWith("." + Constants.PAF_EXT)){
						
						templates.add(fromFile);
						
					}
					
				}
				
			}
			
		}
		
		return templates;
		
	}
	
	/**
	 * 
	 * @param projectSourceTypeList
	 * @return
	 */
	public static ProjectSourceOptionsWizardPage getNewProjectOptionsWizardPage(List<ProjectSource> projectSourceList, List<ProjectSource> checkedItemsList) {
						
		return new ProjectSourceOptionsWizardPage("newProjectSourceOptions", 
				"New Project Selection", 
				"Please select an option to create a project from.", 
				projectSourceList,
				checkedItemsList);
		
	}
	
	
	/**
	 * 
	 * @param projectSourceTypeList
	 * @return
	 */
	public static ProjectSourceOptionsWizardPage getImportProjectOptionsWizardPage(List<ProjectSource> projectSourceList, List<ProjectSource> checkedItemsList) {
						
		return new ProjectSourceOptionsWizardPage("importProjectSourceOptions", 
				"Import Project Data Selection", 
				"Please select an option to import pace project data from.", 
				projectSourceList, 
				checkedItemsList);
		
	}
	
	
	
	
	/**
	 * 
	 * @return
	 */
	public static FileSelectionWizardPage getExcelFileSelectionWizardPage() {		
		
		
		return new FileSelectionWizardPage("excelFileSelection", 
				"Excel File Selection", 
				"Please select an existing Pace Excel workbook.",
				"Excel workbook:",
				"Select Excel file:",
				Constants.EXCEL_FILE_EXTENSION_LIST);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public static FileSelectionWizardPage getPaceArchiveFileSelectionWizardPage() {
		
		List<String> pafFileExtension = new ArrayList<String>(Arrays.asList("paf","zip"));
				
		return new FileSelectionWizardPage("pafFileSelection", 
				"Pace Archive File Selection", 
				"Please select an existing Pace archive file.", 
				"Pace Archive:", 
				"Select a Pace archive file:", 
				pafFileExtension);
		
	}
	
	/**
	 * 
	 * @param serverNameList
	 * @return
	 */
	public static GenericSelectionOptionsWizardPage getServerSelectionWizardPage(String[] serverNameList, String[] initallyCheckedItems) {
		
		return new GenericSelectionOptionsWizardPage("serverSelection", 
				"Pace Server Selection", 
				"Select a Pace Server that holds the data source defintion that points to the desired Essbase Outline.", 
				"Servers:", 
				serverNameList, 
				initallyCheckedItems, 
				false);		
		
	}
	
	/**
	 * 
	 * @param templateNameList
	 * @return
	 */
	public static GenericSelectionOptionsWizardPage getTemplateSelectionWizardPage(String[] templateNameList, String[] initallyCheckedItems) {
		
		return new GenericSelectionOptionsWizardPage("templateSelection", 
				"Template Selection", 
				"Select a template to create the project from.", 
				"Templates:", 
				templateNameList, 
				initallyCheckedItems, 
				false);		
		
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @return
	 */
	public static ProjectNameSelectionWizardPage getNewProjectNameSelectionWizardPage(String[] currentProjects) {
		
		return new ProjectNameSelectionWizardPage("newProjectNameSelection", 
				"Pace Project Name Selection", 
				"Creates a new Pace project", 
				false, 
				currentProjects);
		
	}

	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @return
	 */
	public static ProjectNameSelectionWizardPage getImportProjectNameSelectionWizardPage(String[] currentProjects) {
			
			return new ProjectNameSelectionWizardPage("importProjectNameSelection", 
					"Pace Project Name Selection", 
					"Imports Pace data into a new or existing Pace project.",
					true, 
					currentProjects);
			
	}
	
	public static ProjectDataSelectionPage getProjectImportDataSelectionWizardPage() {
		
		return new ProjectDataSelectionPage("importDataSelection", 
				"Project Data Selection", 
				"Select the project data to import.  If all are checked, all project data will be imported.",
				true);
		
	}
	
	
	public static ProjectErrorsDialog getProjectImportErrorsWizardPage(Shell parentShell, List<ProjectDataError> projectDataErrors) {
		
		return new ProjectErrorsDialog(parentShell,
				"Project Import Errors", 
				"The following is a list of errors contained in the project you are trying to import. Please correct these errors before trying to import your project again.",
				projectDataErrors);
		
	}
	
	public static ProjectErrorsDialog getProjectExportErrorsWizardPage(Shell parentShell, List<ProjectDataError> projectDataErrors) {
		
		return new ProjectErrorsDialog(parentShell,
				"Project Export Errors", 
				"The following is a list of errors contained in the project you are trying to export. Please correct these errors before trying to export your project again.",
				projectDataErrors);
		
	}
	
	public static ProjectDataSelectionPage getProjectExportDataSelectionWizardPage() {
		
		return new ProjectDataSelectionPage("exportDataSelection",
				"Project Data Selection", 
				"Select the project data to export.  If all are checked, all project data will be exported.",
				false);
		
	}
	
	/**
	 * 
	 * @param project name array
	 * @return
	 */
	public static GenericSelectionOptionsWizardPage getExportProjectSelectionWizardPage(String[] projectNameList, String[] initallyCheckedItems) {
		
		return new GenericSelectionOptionsWizardPage("exportProjectSelection",
				"Project Selection", "Select a project to export from.", 
				"Projects:", 
				projectNameList,
				initallyCheckedItems,
				false);		
		
	}
	
	/**
	 * 
	 * @param project name array
	 * @return
	 */
	public static ProjectExportTypeSelectionPage getProjectExportTypeSelectionPage(String[] projectNameList, List<String> templateNameList) {		
		
		List<String> pafFileExtension = new ArrayList<String>(Arrays.asList("paf"));
		
		return new ProjectExportTypeSelectionPage("projectExportTypeSelectionPage", 
				"Export Type Selection", 
				"Select the destination(s) for exported files.", 
				projectNameList, 
				templateNameList,
				Constants.EXCEL_FILE_EXTENSION_LIST,
				pafFileExtension);		
		
	}
	

	/**
	 * 
	 * @param serverNameList
	 * @return
	 */
	public static GenericSelectionOptionsWizardPage getDataSourceSelectionWizardPage(String[] dsNameList, String[] initallyCheckedItems) {
		
		return new GenericSelectionOptionsWizardPage("dataSourceSelection", 
				"Data Source Selection", 
				"Select the data source that points to the desired Essbase Outline.", 
				"DataSources:", 
				dsNameList, 
				initallyCheckedItems, 
				false);		
		
	}

	
	
	/**
	 * 
	 * @param serverNameList
	 * @return
	 */
	public static GenericSelectionOptionsWizardPage getDimensionSelectionWizardPage(String[] dimNameList, String[] initallyCheckedItems) {
		
		return new GenericSelectionOptionsWizardPage("dimensionSelection", 
				"Dimension Selection", 
				"Select dimensions from Essbase.", 
				"Dimensions:", 
				dimNameList, 
				initallyCheckedItems, 
				false);		
		
	}
}

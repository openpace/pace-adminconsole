/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.util;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.TreeNode;

public class ViewerUtil {
	
	
	public static IProject getProjectNode(ISelection iSelection){
		
		if ( iSelection != null ) {
			
			IStructuredSelection selection = (IStructuredSelection) iSelection;
			
			
			if ( ! selection.isEmpty() ) {
			
				Object object = selection.getFirstElement();
				
				
				if (object != null) {

					if(object instanceof IProject){
						return (IProject) object;
					}else if(object instanceof IFolder){
						return ((IFolder)object).getProject();
					}else if(object instanceof IFile){
						return ((IFile)object).getProject();
					}else {
						TreeNode node = (TreeNode) object;
						
						if(node.getProject() != null){
							
							return node.getProject();
							
						} 
						
					}
					
				} 
				
			}
			
		} 
			
		
		return null;
		
	}
	
	
	public static IProject getProjectNode(TreeNode treeNode){
		
		IProject iProject = null;
		
		if ( treeNode != null ) {
		
			iProject = treeNode.getProject();
			
			if ( iProject == null && treeNode.getParent() != null ) {
				
				iProject = getProjectNode((TreeNode) treeNode.getParent());
								
			} 			
			
		}		
		
		return iProject;
	}
	
	/**
	 * Fixes bug TTN-1459
	 */
	public static ProjectNode getProjectNode(String projectName){
		ProjectNode projectNode = null;
		
		for(TreeItem item : MenuPlugin.getDefault().getMenuView().getViewer().getTree().getItems()){
			if(item.getData() instanceof ProjectNode){
				ProjectNode p = (ProjectNode) item.getData();
				if(p.getName().equals(projectName)){
					return (ProjectNode) item.getData();
				}
			}
		}
		
		return projectNode;
	}
	


	
	
//	public static ProjectNode getProjectNode2(String projectName){
//		ProjectNode projectNode = null;
//		
//		for(TreeItem item : MenuPlugin.getDefault().getMenuView().getViewer().getTree().getItems()){
//			if(item.getText().equals(projectName)){
//				if(item.getData() instanceof ProjectNode){
//					return (ProjectNode) item.getData();
//				}
//			}
//		}
//		
//		return projectNode;
//	}
	
	public static Map<String, IProject> getCurrentProjects(){
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot rootWorkspace = workspace.getRoot();
		
//		File workspaceRoot = new File(rootWorkspace.getLocation().toString());
//		
//		//hold a set of project names
//		Set<String> newProjectSet = new HashSet<String>();
//				
//		//if workpace root is not null and is in fact a dir
//		if ( workspaceRoot != null && workspaceRoot.isDirectory() ) {
//		
//			//list files in workspace folders
//			for (File workspaceFolder : workspaceRoot.listFiles()	) {
//			
//				//if workspace file is a dir and doesn't start with a .
//				if ( workspaceFolder.isDirectory() && ! workspaceFolder.getName().equals(Constants.META_DATA_FOLDER_NAME)) {
//				
//					//add existing project work project set
//					newProjectSet.add(workspaceFolder.getName());
//					
//				}
//				
//			}
//						
//		}
		
		//create project map with key as string and value as project
		Map<String, IProject> projectMap = new TreeMap<String, IProject>(String.CASE_INSENSITIVE_ORDER);
	
		
		//populate ordered project map
		for (IProject project : rootWorkspace.getProjects() ) {
			
			projectMap.put(project.getName(), project);
			
		}
		
//		newProjectSet.clear();
		return projectMap;
		
	}
	
	/**
	 * Creates a new IProject in the workspace.
	 * @param projectName name of the new project.
	 * @return
	 */
	public static IProject createNewProject(String projectName) throws CoreException {
		
		
		IProject newProject = null;
		
		//create new project
		newProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		
		newProject.create(null);
	
		//open new project
		newProject.open(null);
	
		//create conf folder
		IFolder confFolder = newProject.getFolder(Constants.CONF_DIR);
		
		confFolder.create(true, true, null);
			
		return newProject;
			
	}
	
	/**
	 * Get the IProject or creates it if the project is not available.
	 * @param createNewProject creates the project if the project is not 
	 * @param projectName
	 * @return
	 */
	public static IProject getProject(boolean createNewProject, String projectName)  throws CoreException {
		
		
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		
		if(! project.exists()){
			
			//create a new pace project.
			project = ViewerUtil.createNewProject(projectName);

		}
		
		
		return project;
	}
	
	
	/**
	 * Deletes the IProject
	 * @param project project to delete.
	 */
	public static void deleteProject(IProject project){
		
		if ( project != null && project.exists() ) {
			
			try {
				
				project.delete(true, null);
				
			} catch (CoreException e) {
				
				e.printStackTrace();
				
			}
			
		}
		
	}
	
	 public IContainer getProject(IResource resource) {
		  IContainer container = null;
		  if (resource != null) {
			  int type = resource.getType();
			  switch (type) {
			  case IResource.FILE :
			  container = resource.getParent();
			  break;
			  case IResource.FOLDER :
			  container = resource.getProject();
			  break;
			  case IResource.PROJECT :
			  container = (IContainer) resource;
			  break;
			  case IResource.ROOT :
			  break;
			  }
		  }
		  return container;
	 }
		 
	
}

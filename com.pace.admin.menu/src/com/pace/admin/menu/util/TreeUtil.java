/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.util;

import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class TreeUtil {

	public static int getTreeItemLevel(TreeItem item) {

		int levelIndex = 0;

		levelIndex = findTreeItemLevel(item, levelIndex);

		return levelIndex;

	}
	
	public static int findTreeItemLevel(TreeItem item, int levelIndex) {

		if (item.getParentItem() != null) {
			levelIndex++;
			levelIndex = findTreeItemLevel(item.getParentItem(), levelIndex);
		}

		return levelIndex;
	}
	
	public static TreeItem getParentOfTree(TreeItem item) {

		TreeItem parent = null;

		if (item.getParentItem() != null) {

			parent = getParentOfTree(item.getParentItem());

		} else {

			parent = item;

		}

		return parent;

	}
	
	public static void doubleClickSelectionAction(Tree tree) {

		TreeItem[] selectedItems = tree.getSelection();

		if (selectedItems != null) {

			if (selectedItems.length == 1) {

				selectedItems[0].setExpanded(!selectedItems[0].getExpanded());

			}

		}
	}
	
}

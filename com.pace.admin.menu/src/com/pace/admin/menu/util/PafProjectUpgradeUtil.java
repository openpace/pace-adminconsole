/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.menu.actions.migration.PafCustomFunctionsMigrationAction;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.base.migration.MigrationAction;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafMdbProps;

public class PafProjectUpgradeUtil {

	private static final Logger logger = Logger.getLogger(PafProjectUpgradeUtil.class);
	
	private static Map<IProject, Boolean> upgradeStatus = new ConcurrentHashMap<IProject, Boolean>();
	
	/**
	 * 
	 * Converts a project into the new format
	 *
	 * @param project current project
	 */
	public static void autoConvertProject(IProject project, String confDir) {


		Boolean status = upgradeStatus.get(project);
		
		if(status != null && status){
			return;
		}
		
		
		List<String> cachedAttributeDims = null;
		try {
			PafServer projServer = PafProjectUtil.getProjectServer(project);

			PafMdbProps cachedMdbProps = DimensionTreeUtility.getCachedMdbProps(projServer, PafProjectUtil.getApplicationName(project));
			if ( cachedMdbProps != null && cachedMdbProps.getCachedAttributeDims().size() > 0 ) {
				cachedAttributeDims = cachedMdbProps.getCachedAttributeDims();
			}
		} catch (PafServerNotFound e1) {

			logger.error("Project Server Not Found: " + e1.getMessage());
		}


		XMLPaceProject pp = null;

		try {

			pp = new XMLPaceProject(confDir, true, cachedAttributeDims);

		} catch (InvalidPaceProjectInputException e2) {

			logger.error("Invalid Pace Project: " + e2.getMessage());

		} catch (PaceProjectCreationException e2) {

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e2.printStackTrace(pw);
			logger.error(sw.toString().toUpperCase());
			logger.error("Problem creating Pace Project: " + e2.getMessage());

		}

		//standard migration actions
		MigrationAction pafCustomFunctionsMigration = new PafCustomFunctionsMigrationAction(project, pp); 
		pafCustomFunctionsMigration.run();

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot rootWorkspace = workspace.getRoot();

		try {
			rootWorkspace.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e1) {
			logger.error("Problem refreshing workspace: " + e1.getMessage());
		}
		
		upgradeStatus.put(project, true);
	}
	
}

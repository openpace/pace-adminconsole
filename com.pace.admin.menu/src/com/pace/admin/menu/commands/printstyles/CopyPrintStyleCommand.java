/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.ui.PrintStyles;

public class CopyPrintStyleCommand extends AbstractHandler {

	public final static String ID = "com.pace.admin.menu.commands.printstyles.CopyPrintStyleCommand";
	private static final Logger logger = Logger.getLogger(CopyPrintStyleCommand.class);

	public CopyPrintStyleCommand() {
		// TODO Auto-generated constructor stub
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		logger.info("Begin CopyPrintStyleCommand.execute()");
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//convert the item to a PrintStylesNode.
			PrintStyleNode node = (PrintStyleNode) ((IStructuredSelection) selection).getFirstElement();
			//Get a PrintStylesNode (parent node)
			PrintStylesNode printStylesNode = (PrintStylesNode) node.getParent();
			//current project
			IProject project = ViewerUtil.getProjectNode(printStylesNode);
			//active page
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			//look for any open editors and close them.
			EditorUtils.closeDuplicateEditorType(page, PrintStyleEditor.class);
			//create the PrintStylesManager. 
			PrintStyles model = new PrintStylesManager(project);
			//using season model manager, create the input for the SeasonEditor.
			GlobalPrintStyleInput input = new GlobalPrintStyleInput(node.getName(), model, project, false, true);
			try {
				//Open the new editor.
				PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
				//set the viewer.
				printStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
				printStyleEditor.setProject(project);
			} catch (PartInitException e) {
				logger.error(e.getMessage());					
				throw new ExecutionException(e.getMessage(), e);					
			}
			
		}
		logger.info("End CopyPrintStyleCommand.execute()");
		return null;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.ViewPrintSettingsInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.ViewNode;
import com.pace.admin.menu.util.ViewerUtil;

public class ModifyPrintSettingsCommand extends AbstractHandler {
	public final static String ID = "com.pace.admin.menu.commands.printstyles.ModifyPrintSettingsCommand";
	private static final Logger logger = Logger.getLogger(ModifyPrintSettingsCommand.class);

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
		//look for any open editors and close them.
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	    	EditorUtils.closeDuplicateEditorType(page, PrintStyleEditor.class);		
			//create a PlanCycleModelManager.
	    	ViewNode node = (ViewNode) ((IStructuredSelection) selection).getFirstElement();
			//Get a PrintStylesNode (parent node)
			if ( node instanceof ViewNode ) {
				//cast
				ViewNode viewNode = (ViewNode) node;
				IProject project = ViewerUtil.getProjectNode(viewNode);
				ViewModelManager model = new ViewModelManager(project);
				if ( model.contains(viewNode.getName())) {
					//get list of open editors
					EditorUtils.closeDuplicateEditorType(page, PrintStyleEditor.class);
					//loop over open editors
					ViewPrintSettingsInput input = new ViewPrintSettingsInput(viewNode.getName(), model, project);
					try {
						//open the editor
						PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
						//set the viewer.
						printStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
						printStyleEditor.setProject(project);
						
					} catch (PartInitException e) {
						// handle error
						logger.error(e.getMessage());
					}
				}
			}
		}
		return null;
	}
}

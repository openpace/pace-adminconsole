/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.dialogs.RenameDialog;
import com.pace.admin.menu.dialogs.RenamePrintStyleDialog;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.ui.PrintStyles;

public class RenamePrintStyleCommand extends AbstractHandler {
	public final static String ID = "com.pace.admin.menu.actions.printstyles.RenamePrintStyleAction";
	private static Logger logger = Logger.getLogger(RenamePrintStyleCommand.class);
	private final static int OK_ID = 0;
	private PrintStyles modelManager;
	private String messageBoxTitle = "Rename Print Style";

	public Object execute(ExecutionEvent event) throws ExecutionException {
		//if user wants to delete a default print style
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
			
			//refresh menu's viewer
			PrintStyleNode node = (PrintStyleNode) ((IStructuredSelection) selection).getFirstElement();
			
			//Get a PrintStylesNode (parent node)
			PrintStylesNode printStylesNode = (PrintStylesNode) node.getParent();
			
			//current project
			IProject project = ViewerUtil.getProjectNode(printStylesNode);
			modelManager = new PrintStylesManager(project);
			String oldName = node.getName();
			RenamePrintStyleDialog dialog = new RenamePrintStyleDialog(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					oldName, modelManager,
					"Old Print Style Name:", "New Print Style Name:", messageBoxTitle);
							
			int rc = dialog.open();
			//if user clicks ok
			if ( rc == OK_ID ) {
				
				//get new print style name
				String newPrintStyleName = dialog.getNewName();
				
				logger.info("Old Print Style Name: " + oldName + "; New Print Style Name: " + newPrintStyleName);
			
				//rename view section name
				modelManager.renamePrintStyle(oldName, newPrintStyleName);
				
				//refresh tree
				printStylesNode.createChildren(null);
				//update the menu plug in.
				MenuPlugin.getDefault().getMenuView().getViewer().refresh(printStylesNode);
				
				//refresh editor view
		    	//using the current window, get the active page.
		    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		    	if( page.getActiveEditor() != null && page.getActiveEditor().getEditorInput() instanceof GlobalPrintStyleInput 
		    			&& page.getActiveEditor().getEditorInput().getName().equals(oldName) ) {
					EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PrintStyleEditor.class);
					GlobalPrintStyleInput input = new GlobalPrintStyleInput(newPrintStyleName, modelManager, project);
					try {
						PrintStyleEditor printStyleEditor = (PrintStyleEditor) page.openEditor(input, PrintStyleEditor.ID);
						printStyleEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
						//set the parent PrintStylesNode (for updating purposes)
						printStyleEditor.setProject(project);
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
			}
		}
		return null;
	}

}

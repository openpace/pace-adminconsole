/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.printstyles;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PrintStyleEditor;
import com.pace.admin.menu.editors.input.GlobalPrintStyleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.ui.PrintStyle;
import com.pace.base.ui.PrintStyles;


public class DeletePrintStyleCommand extends AbstractHandler {

	public final static String ID = "com.pace.admin.menu.commands.printstyles.DeletePrintStyleCommand";
	private static final Logger logger = Logger.getLogger(DeletePrintStyleCommand.class);
	private String printStyleName;
	private PrintStyles modelManager;
	private String messageBoxTitle = "Delete Print Style";
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//if user wants to delete a default print style
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			PrintStyleNode node = (PrintStyleNode) ((IStructuredSelection) selection).getFirstElement();
			printStyleName = node.getName();
			//Get a PrintStylesNode (parent node)
			PrintStylesNode printStylesNode = (PrintStylesNode) node.getParent();
			//current project
			IProject project = ViewerUtil.getProjectNode(printStylesNode);
			modelManager = new PrintStylesManager(project);
	        PrintStyle currentPrintStyle = modelManager.getPrintStyleByName(printStyleName);
    		//1. first find if the print style is a default print style
    		if( currentPrintStyle.getDefaultStyle() ) {
		    	GUIUtil.openMessageWindow( messageBoxTitle, "'" + 
		    		printStyleName+ "' is currently set as the Default Print Style.  Please set another style as the Default before deleting.");
		    	return null;
    		}
    		//2. if not a default print style, then find any view that contain this print styles
			ViewModelManager viewModel = new ViewModelManager(project);
			List<PafAdminConsoleView> listViews = viewModel.findPrintStyleInViewsByGUID(currentPrintStyle.getGUID());
			//if find views that contain this print style, then pop up a warning message
			if( listViews != null && listViews.size() > 0 ) {
				String viewNames = "";
				for( PafAdminConsoleView view : listViews ) {
					viewNames += "'" + view.getName() + "', ";
				}
				//3. if user says Not to delete the associated print styles
				if ( ! GUIUtil.askUserAQuestion( messageBoxTitle,
					"Are you sure you want to delete the print style: '" + printStyleName 
					+ "'? This Print Style is associated with Views: " + viewNames + " Deleting this print style will cause the Default Print Style to be applied to these views.  Do you want to continue?")) {
					return null;
				}
			
 				deletePrintStyle();
			}
			//5. if find views that contain this print style, pop up a confirmation message
			else {
 	       		String message = "Are you sure you want to delete the print style: '" + printStyleName + "'?";
	    		//confirm with user
	    		boolean isConfirmed = MessageDialog.openConfirm(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Confirm", message);
	    		//if confirmed
	    		if ( isConfirmed ) {
	    			deletePrintStyle();

			    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			    	if( page.getActiveEditor() != null && page.getActiveEditor().getEditorInput() instanceof GlobalPrintStyleInput 
			    			&& page.getActiveEditor().getEditorInput().getName().equals(printStyleName) ) {
			    		EditorUtils.closeDuplicateEditorType(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), PrintStyleEditor.class);
			    		
			    	}
	    		}
			}
	    }
		return null;
	}

	private void deletePrintStyle() {
		
	    //remove the item from the plan cycle model manager.
		modelManager.remove(modelManager.getGUIDByName(printStyleName));
		modelManager.save();
		
		ISelection selection = MenuPlugin.getDefault().getMenuView().getViewer().getSelection();
	    //rebuilds the print preferences tree.
       	if ( selection instanceof IStructuredSelection) {
    		Object objectNode = ((IStructuredSelection) selection).getFirstElement();
    		if ( objectNode instanceof PrintStyleNode ) {
    			PrintStyleNode printStyleNode = (PrintStyleNode) objectNode;
    			PrintStylesNode parentNode = (PrintStylesNode) printStyleNode.getParent();			
    			parentNode.createChildren(null);
    			MenuPlugin.getDefault().getMenuView().getViewer().refresh();
    		}
       	}
	}

}

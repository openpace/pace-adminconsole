/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.printstyles;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.nodes.PrintStyleNode;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.base.ui.PrintStyles;


public class SetDefaultPrintStyleCommand extends AbstractHandler {
	public final static String ID = "com.pace.admin.menu.commands.printstyles.SetDefaultPrintStyleCommand";
	private static final Logger logger = Logger.getLogger(SetDefaultPrintStyleCommand.class);
	private PrintStyles printStylesModel;

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			PrintStyleNode printStyleNode = (PrintStyleNode) ((IStructuredSelection) selection).getFirstElement();
			PrintStylesNode printStylesNode = (PrintStylesNode) printStyleNode.getParent();
			IProject project = ViewerUtil.getProjectNode(printStylesNode);
    		ViewModelManager viewModel = new ViewModelManager(project);
    		viewModel.resetDefaultPrintStyleInViews(printStyleNode.getName());
    		printStylesModel = new PrintStylesManager(project);
			//if can't find any existing default print style, then simply set the current one as default
			printStylesModel.setDefaultPrintStyle(printStyleNode.getName(), true);
			ProjectNode projectNode = ViewerUtil.getProjectNode(printStyleNode.getProject().getName());
			if(projectNode != null){
				for (Object obj : projectNode.getChildren()) {
					if ( obj instanceof PrintStylesNode) {
						//get the high level node.
						PrintStylesNode printPrefsNode = (PrintStylesNode)obj;
						//re-create children
						printPrefsNode.createChildren(null);
						//update the menu plug in.
						MenuPlugin.getDefault().getMenuView().getViewer().refresh(obj);
					}	
				}
			}	
		}
		return null;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.commands.projectsettings;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.ProjectSettingsEditor;
import com.pace.admin.menu.editors.input.ProjectSettingsEditorInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PrintStylesNode;
import com.pace.admin.menu.nodes.ProjectNode;
import com.pace.admin.menu.nodes.ProjectSettingsWizardNode;
import com.pace.admin.menu.util.ViewerUtil;

public class ProjectSettingsCommand extends AbstractHandler {

	public final static String ID = "com.pace.admin.menu.commands.ProjectSettingsCommand";
	private static final Logger logger = Logger.getLogger(ProjectSettingsCommand.class);
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		logger.info("Begin ProjectSettingsCommand.execute()");
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ( selection != null && selection instanceof IStructuredSelection ) {
			//refresh menu's viewer
			//current project
			ProjectSettingsWizardNode node = (ProjectSettingsWizardNode) ((IStructuredSelection) selection).getFirstElement();
			//current project
			IProject project = ViewerUtil.getProjectNode(node);
			//active page
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			//look for any open editors and close them.
			EditorUtils.closeDuplicateEditorType(page, ProjectSettingsEditor.class);
			//using season model manager, create the input for the SeasonEditor.
			ProjectSettingsEditorInput input = new ProjectSettingsEditorInput(project);
			try {
				//Open the new editor.
				ProjectSettingsEditor projSettingsEditor = (ProjectSettingsEditor) page.openEditor(input, ProjectSettingsEditor.ID);
				//set the viewer.
				projSettingsEditor.setProject(project);
				projSettingsEditor.setViewer(MenuPlugin.getDefault().getMenuView().getViewer());
			} catch (PartInitException e) {
				logger.error(e.getMessage());					
				throw new ExecutionException(e.getMessage(), e);					
			}
			
		}
		logger.info("End ProjectSettingsCommand.execute()");
		return null;
	}

}

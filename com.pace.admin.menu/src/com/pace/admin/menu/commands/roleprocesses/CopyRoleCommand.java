/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.commands.roleprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PlannerRoleEditor;
import com.pace.admin.menu.editors.input.PlannerRoleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlannerRoleNode;
import com.pace.admin.menu.nodes.PlannerRolesNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.views.MenuView;

/**
 * @author jmilliron
 *
 */
public class CopyRoleCommand extends AbstractHandler {

	private static final Logger logger = Logger.getLogger(CopyRoleCommand.class);
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		logger.debug("Begin CopyRoleCommand.execute()");
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);

		if ( selection != null && selection instanceof IStructuredSelection ) {

			//refresh menu's viewer
			MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

			if ( menuView != null ) {
				
				//convert the node to a PlannerRolesNode.
				PlannerRoleNode node = (PlannerRoleNode) ((IStructuredSelection) selection).getFirstElement(); 
				//Create a PlannerRolesNode (parent).
				PlannerRolesNode plannerRolesNode = (PlannerRolesNode) node.getParent(); 
				
				//current project
				IProject project = ViewerUtil.getProjectNode(node);		
				//active page
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				
				//look for any open editors and close them.
				EditorUtils.closeDuplicateEditorType(page, PlannerRoleEditor.class);
				//Create a PlannerRoleModelManager.
				PlannerRoleModelManager model = new PlannerRoleModelManager(project);
				//Create a SeasonModelManager.
				SeasonModelManager seasonsModel = new SeasonModelManager(project);
				//using the two model manager, create the input for the PlannerRoleEditor.
				PlannerRoleInput input = new PlannerRoleInput(node.getName(), model, seasonsModel, project, false, true);
				
				try {
					PlannerRoleEditor plannerRoleEditor = (PlannerRoleEditor) 
						page.openEditor(input, PlannerRoleEditor.ID);
					//set the viewer.
					plannerRoleEditor.setViewer(menuView.getViewer());
					//set the parent plannerRolesNode (for updating purposes)
					plannerRoleEditor.setPlannerRolesNode(plannerRolesNode);
					
				} catch (PartInitException e) {
					// handle error
					logger.error(e.getMessage());
				}
				
			}
			
		}
		
		logger.debug("End CopyRoleCommand.execute()");
		
		return null;
	}

}

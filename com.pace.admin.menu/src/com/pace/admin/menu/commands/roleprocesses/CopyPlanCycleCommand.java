/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.commands.roleprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.PlanCycleModelManager;
import com.pace.admin.global.model.managers.VersionDefModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.PlanCycleEditor;
import com.pace.admin.menu.editors.input.PlanCycleInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.PlanCycleNode;
import com.pace.admin.menu.nodes.PlanCyclesNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.views.MenuView;

/**
 * @author jmilliron
 *
 */
public class CopyPlanCycleCommand extends AbstractHandler {
	
	private static final Logger logger = Logger.getLogger(CopyPlanCycleCommand.class);

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		logger.info("Begin CopyPlanCycleCommand.execute()");
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);

		if ( selection != null && selection instanceof IStructuredSelection ) {
			
			PlanCycleNode planCycleNode = (PlanCycleNode) ((IStructuredSelection) selection).getFirstElement();
			
			if ( planCycleNode != null ) {
			
				//refresh menu's viewer
				MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

				if ( menuView != null ) {
								
					String planCycleName = planCycleNode.getName();
					
					IProject project = ViewerUtil.getProjectNode(planCycleNode);
					
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					
					//look for any open editors and close them.
			    	EditorUtils.closeDuplicateEditorType(page, PlanCycleEditor.class);		
					//create a PlanCycleModelManager.
					PlanCycleModelManager model = new PlanCycleModelManager(project);
					//Create a version def model manager.
					VersionDefModelManager versionDefModel = new VersionDefModelManager(project, true);
					//using the two model manager, create the input for the PlanCycleEditor.
					PlanCycleInput input = new PlanCycleInput(planCycleName, model, versionDefModel, project, false, true);
					
					try {
						//Open the new editor.
						PlanCycleEditor planCycleEditor = (PlanCycleEditor) 
							page.openEditor(input, PlanCycleEditor.ID);
						//set the viewer.
						planCycleEditor.setViewer(menuView.getViewer());
																		
						//set the parent plan cycles node (for updating purposes)
						planCycleEditor.setPlanCyclesNode((PlanCyclesNode) planCycleNode.getParent());
						
					} catch (PartInitException e) {
						logger.error(e.getMessage());
						throw new ExecutionException(e.getMessage(), e);
					}     
				
				
				}
			}		
			
		}
		
		logger.info("End CopyPlanCycleCommand.execute()");
		
		return null;
		
	}

}

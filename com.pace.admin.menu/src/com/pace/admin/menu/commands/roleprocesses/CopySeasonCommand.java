/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.commands.roleprocesses;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.admin.menu.editors.SeasonEditor;
import com.pace.admin.menu.editors.input.SeasonInput;
import com.pace.admin.menu.editors.util.EditorUtils;
import com.pace.admin.menu.nodes.SeasonNode;
import com.pace.admin.menu.nodes.SeasonsNode;
import com.pace.admin.menu.util.ViewerUtil;
import com.pace.admin.menu.views.MenuView;

/**
 * @author jmilliron
 *
 */
public class CopySeasonCommand extends AbstractHandler {

	private static final Logger logger = Logger.getLogger(CopySeasonCommand.class);
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		logger.info("Begin CopySeasonCommand.execute()");
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);

		if ( selection != null && selection instanceof IStructuredSelection ) {
		
			//refresh menu's viewer
			MenuView menuView = (MenuView) MenuPlugin.getDefault().getMenuView();

			if ( menuView != null ) {
			
				//convert the item to a SeasonNode.
				SeasonNode node = (SeasonNode) ((IStructuredSelection) selection).getFirstElement();
				
				//Create a SeasonsNode (parent node)
				SeasonsNode seasonsNode = (SeasonsNode) node.getParent();

				//current project
				IProject project = ViewerUtil.getProjectNode(seasonsNode);
				
				//active page
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				
				//look for any open editors and close them.
				EditorUtils.closeDuplicateEditorType(page, SeasonEditor.class);
				
				//create the season model manager. 
				SeasonModelManager model = new SeasonModelManager(project);
				//using season model manager, create the input for the SeasonEditor.
				SeasonInput input = new SeasonInput(node.getName(), model, project, false, true);
				
				try {
					//Open the new editor.
					SeasonEditor seasonEditor = (SeasonEditor) page.openEditor(input, SeasonEditor.ID);
					//set the viewer.
					seasonEditor.setViewer(menuView.getViewer());
					//set the parent SeasonsNode (for updating purposes)
					seasonEditor.setSeasonsNode(seasonsNode);
				} catch (PartInitException e) {
					logger.error(e.getMessage());					
					throw new ExecutionException(e.getMessage(), e);					
				}
				
			}
			
		}
		
		logger.info("End CopySeasonCommand.execute()");
		
		return null;
		
	}

}

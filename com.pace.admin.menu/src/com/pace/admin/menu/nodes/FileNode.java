/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import com.pace.admin.global.interfaces.ITreeNode;

public class FileNode extends TreeNode
{
	private File fFile; /* actual data object */
	
	public FileNode(ITreeNode parent, File file)
	{
		super(parent);
		fFile = file;
	}
	
	public String getName() {		
		return "FILE: " + fFile.getName();
	}
 
	@Override
	public void createChildren(List children) {
	}
}

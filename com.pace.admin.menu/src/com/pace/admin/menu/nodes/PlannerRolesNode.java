/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;

/**
* PlannerRolesNode(parent) extenstion of TreeNode
* @author kmoos
* @version	x.xx
*
*/
public class PlannerRolesNode extends ModelTreeNode {
	
	/**
	 * Constructor
	 * @param parent The parent TreeNode.
	 * @param project The IProject.
	 */
	public PlannerRolesNode(ITreeNode parent, IProject project) {
		super(parent, project, new PlannerRoleModelManager(project));
	}
	
	/**
	 * Implementation of TreeNode.createChildren()
	 * @param children
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {
		super.createChildren(children);
		
		if(modelManager == null){
			modelManager = new PlannerRoleModelManager(project);
			modelKeys = modelManager.getKeys();
		}
		
		logger.debug("Loading Planner Role Node");
		for (String plannerRole : modelKeys) {

			fChildren.add(new PlannerRoleNode(this, plannerRole));
		}
		
		modelManager = null;
	}
	
	/**
	 * Implementation of TreeNode.getImage()
	 * @return Image
	 */
	public Image getImage() {
		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	/**
	 * Implementation of TreeNode.getName()
	 * @return String
	 */
	public String getName() {
		return Constants.MENU_ROLES;
	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
}

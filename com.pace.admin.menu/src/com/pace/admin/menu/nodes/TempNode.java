/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;
import java.util.ArrayList;

import org.eclipse.swt.widgets.TreeItem;

import com.pace.admin.menu.dialogs.ViewSectionTuplesDialog2;

public class TempNode {
	
	private TempNode parent;
	private String text;
	private ArrayList<TempNode> children;
	private boolean expanded;
	private Object data;
	private Object userSelectMultipleDataItem;
	private Object memberTagNode;
	
	public TempNode(TempNode parent, TreeItem item) {
		
		this.parent = parent;
		this.text = item.getText();
		this.expanded = item.getExpanded();
		this.data = item.getData();
		this.userSelectMultipleDataItem = item.getData(ViewSectionTuplesDialog2.USER_SELECTION_MULTIPLE);
		this.memberTagNode = item.getData(ViewSectionTuplesDialog2.MEMBER_TAG_KEY);
		
	}
	
	public TempNode(String text) {

		this.text = text;

	}
	
	
	public TempNode(TempNode parent, String text) {
		
		this(text);
		this.parent = parent;
		if ( this.parent != null ) {
			this.parent.addChild(this);
		}
		
		
	}
		
	public TempNode getParent() {
		return parent;
	}

	public void setParent(TempNode parent) {
		this.parent = parent;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
		
	public ArrayList<TempNode> getChildren() {
		return children;
	}
	
	public void addChild(TempNode item) {
		
		if ( children == null) {
			
			children = new ArrayList<TempNode>();
			
		}
		
		children.add(item);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TempNode node1 = new TempNode(null, "Node 1");
		
		TempNode node2 = new TempNode(node1, "Node 2");
		TempNode node3 = new TempNode(node1, "Node 3");
		TempNode node4 = new TempNode(node1, "Node 4");
		
		/*node1.addChild(node2);
		node1.addChild(node3);
		node1.addChild(node4);*/
		
		TempNode node5 = new TempNode(node4, "Node 5");
		TempNode node6 = new TempNode(node4, "Node 6");
		TempNode node7 = new TempNode(node4, "Node 7");
		
		/*node4.addChild(node5);
		node4.addChild(node6);
		node4.addChild(node7);*/
		
		TempNode node8 = new TempNode(node7, "Node 8");
		
		//node7.addChild(node8);
		
		printFromRootNode(node1);
		
		node3 = null;
		node8 = null;
		
		TempNode newNode = node1.getChildren().get(1);
		
		System.out.println("\n\n" + newNode);
		
		
		System.out.println("After");
		printFromRootNode(node1);
			

	}

	public static void printFromRootNode(TempNode node) {
		
		//TestNode parent = node.getParent();
		
		System.out.println(node);
		
		if ( node.getChildren() != null && node.getChildren().size() > 0 ) {
			for (TempNode child : node.getChildren()) {
				printFromRootNode(child);
			}
			
		}
		
	}
	
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("======================\n");
		strBuff.append("Node Name: " + this.text);
		strBuff.append("\nExpanded: " + this.expanded);
		if ( this.children != null) {
			for (TempNode child : children) {
				strBuff.append("\n\n" + child);
			}
		}			
		
		return strBuff.toString();
	}
	
	public int numberOfChildren() {
		
		if (children == null) {
			return 0;
		} else {
			return children.size();
		}
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @return Returns the userSelectMultipleDataItem.
	 */
	public Object getUserSelectMultipleDataItem() {
		return userSelectMultipleDataItem;
	}

	/**
	 * @param userSelectMultipleDataItem The userSelectMultipleDataItem to set.
	 */
	public void setUserSelectMultipleDataItem(Object userSelectMultipleDataItem) {
		this.userSelectMultipleDataItem = userSelectMultipleDataItem;
	}

	public Object getMemberTagNode() {
		return memberTagNode;
	}

	public void setMemberTagNode(Object memberTagNode) {
		this.memberTagNode = memberTagNode;
	}



}

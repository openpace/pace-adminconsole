/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PrintStylesManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.ui.PrintStyles;

public class PrintStyleNode extends TreeNode {
	private String name;
	protected Image image;

	public PrintStyleNode(ITreeNode parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	public PrintStyleNode(ITreeNode parent, String name) {
		super(parent);
		// TODO Auto-generated constructor stub
		this.name = name;
	}

	public String getName() {		
		return name;
	}
 
	public Image getImage() {
		return image;
	}

	public void createChildren(List children) {
	}
}

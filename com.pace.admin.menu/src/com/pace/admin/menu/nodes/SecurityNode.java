/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;

public class SecurityNode extends TreeNode {

	public SecurityNode(ITreeNode parent, IProject project) {
		super(parent);
		this.project = project;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {

		logger.debug("Creating Children of Security Nodes");
		
		clearChildren();
	
		fChildren.add(new UsersSecurityNode(this, project));
		//Disable admin locks, however allow it to be enabled using a magic file.
		try
		{
			File f = new File(Constants.ADMIN_CONSOLE_CONF_DIRECTORY, Constants.ENABLE_DEVELOPMENT_FEATURES_FLAG);
			if(f.exists()){
				logger.debug("Admin Locks are enabled via development flag.");
				fChildren.add(new AdminLocksNode(this, project));
			} else {
				logger.debug("Admin Locks are disabled.");
			}
		}catch(Exception e)
		{
			logger.debug("Admin Locks are disabled.");
		}
	}

	@Override
	public boolean hasChildren() {

		return true;
	}
	
	
	public Image getImage() {

		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	public String getName() {

		return "Security";
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.global.model.managers.SeasonModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.Season;
import com.pace.base.comm.PafPlannerConfig;

/**
* PlanCycleNode extenstion of TreeNode
* @author kmoos
* @version	x.xx
*
*/
public class PlanCycleNode extends TreeNode {
	protected static Image image;
	private String name;
	
	/**
	 * Constructor
	 * @param parent The parent tree node.
	 * @param name The name of the node to create.
	 */
	public PlanCycleNode(ITreeNode parent, String name) {
		super(parent);
		this.name = name;
	}

	/**
	 * Implementation of TreeNode.createChildren()
	 * @param children
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void createChildren(List children) {
	}

	/**
	 * Implementation of TreeNode.getName()
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Implementation of TreeNode.getImage()
	 * @return Image
	 */
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "wi0064-16.gif");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		return image;
	}	
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	@SuppressWarnings("rawtypes")
	public List<IRemoval> getDependants(IProject project){
		
		ArrayList<IRemoval> dependants = new ArrayList<IRemoval>();
		
		SeasonsNode seasonsNode = null;
		List seasonNodes = null;
		
		RoleConfigurationsNode roleConfigurationsNode = null;
		List configurationNodes = null;
		
		TreeNode parent = (TreeNode) getParent();
		TreeNode grandparent = (TreeNode) parent.getParent();
		
		for (Object objectNode : grandparent.getChildren()) {
			if(objectNode instanceof RoleConfigurationsNode){
				roleConfigurationsNode = (RoleConfigurationsNode) objectNode;
			}
			else if(objectNode instanceof SeasonsNode){
				seasonsNode = (SeasonsNode) objectNode;
			}
		}
		
		seasonNodes = seasonsNode.getChildren();
		configurationNodes = roleConfigurationsNode.getChildren();

		
		PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
		
		for(Object objectNode : configurationNodes){
			RoleConfigurationNode configurationNode = (RoleConfigurationNode) objectNode;
			PafPlannerConfig pafPlannerConfig = (PafPlannerConfig) pafPlannerConfigModelManager.getItem(configurationNode.getName());
			if(pafPlannerConfig.getCycle() != null && pafPlannerConfig.getCycle().equalsIgnoreCase(name)){
				dependants.add(new RoleConfigurationRemoval(pafPlannerConfig.getRole() + " - " + pafPlannerConfig.getCycle(), pafPlannerConfigModelManager));
			}
		}
		
		SeasonModelManager seasonModelManager = new SeasonModelManager(project);
		PlannerRoleModelManager plannerRoleModelManager = new PlannerRoleModelManager(project);
		
		for(Object objectNode : seasonNodes){
			SeasonNode seasonNode = (SeasonNode) objectNode;
			Season season = (Season) seasonModelManager.getItem(seasonNode.getName());
			if(season.getPlanCycle().equalsIgnoreCase(name)){
				dependants.add(new SeasonRemoval(seasonNode.getName(), seasonModelManager));
				dependants.addAll(seasonNode.getDependants(project, plannerRoleModelManager));
			}
		}

		return dependants;
	}
}

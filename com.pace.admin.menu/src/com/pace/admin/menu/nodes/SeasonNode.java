/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.PafPlannerRole;

/**
* SeasonNode extenstion of TreeNode
* @author kmoos
* @version	x.xx
*
*/
public class SeasonNode extends TreeNode {
	protected static Image image;
	private String name;
	
	/**
	 * Constructor
	 * @param parent The parent tree node.
	 * @param name The name of the node to create.
	 */
	public SeasonNode(ITreeNode parent, String name) {
		super(parent);
		this.name = name;
	}

	/**
	 * Implementation of TreeNode.createChildren()
	 * @param children
	 */
	@Override
	public void createChildren(List children) {
	}

	/**
	 * Implementation of TreeNode.getName()
	 * @return String
	 */
	public String getName() {
		return name; 
	}

	/**
	 * Implementation of TreeNode.getImage()
	 * @return Image
	 */
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "wi0064-16.gif");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		return image;
	}	
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
	
	public List<IRemoval> getDependants(IProject project, PlannerRoleModelManager roleModelManager){
		
		ArrayList<IRemoval> dependants = new ArrayList<IRemoval>();
		
		PlannerRolesNode plannerRolesNode = null;
		List roleNodes = null;
		
		TreeNode parent = (TreeNode) getParent();
		TreeNode grandparent = (TreeNode) parent.getParent();
		
		for (Object objectNode : grandparent.getChildren()) {
			if(objectNode instanceof PlannerRolesNode){
				plannerRolesNode = (PlannerRolesNode) objectNode;
				break;
			}
		}
		
		roleNodes = plannerRolesNode.getChildren();
		
		PlannerRoleModelManager plannerRoleModelManager = roleModelManager;
		if(roleModelManager == null){
			plannerRoleModelManager = new PlannerRoleModelManager(project);
		}

		
		for (Object objectNode : roleNodes) {
			PlannerRoleNode roleNode = (PlannerRoleNode) objectNode;
			PafPlannerRole plannerRole = (PafPlannerRole) plannerRoleModelManager.getItem(roleNode.getName());
			
			String [] seasonNames = plannerRole.getSeasonIds();
			
			if ( seasonNames != null ) {
				
				for (String seasonName : seasonNames) {
					if(seasonName.equalsIgnoreCase(name)){
						dependants.add(new SeasonFromRoleRemoval(name, plannerRole, plannerRoleModelManager));
					}
				}
			}
		}
		
		return dependants;
		
	}
}
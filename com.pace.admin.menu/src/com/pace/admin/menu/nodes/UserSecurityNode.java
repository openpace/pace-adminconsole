/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;

public class UserSecurityNode extends TreeNode {
	
	private PafUserSecurity pafUserSecurity;
	
	private HashMap<String, Image> imageMap = null;
	
	public UserSecurityNode(ITreeNode parent, IProject project, PafUserSecurity PafUserSecurity) {
		super(parent);
		this.project = project;
		this.pafUserSecurity = PafUserSecurity;		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void createChildren(List children) {
		
		clearChildren();
		
		if ( pafUserSecurity != null ) 
		{
			logger.debug("Creating Children of  User Security Node: " + pafUserSecurity.getUserName());
			
			Map<String, PafWorkSpec[]> roleMap = PafApplicationUtil.getPafSecurityUserRoles(project, pafUserSecurity.getUserName(), pafUserSecurity.getDomainName());
			
			if ( roleMap != null) {				
				
				Set<String> roleNameSet = new LinkedHashSet<String>();
				
				for ( String roleName : roleMap.keySet()) {
					
					roleNameSet.add(roleName);
					
				}
					
				for (String roleName : roleNameSet ) {
									
					fChildren.add(new UserSecurityRoleNode(this, project, roleName, roleMap.get(roleName) ));
					
				}			
				
			}	
				
		}
		
	}

	@Override
	public boolean hasChildren() {

		return pafUserSecurity != null;
	}
	
	/**
	 * Gets the name to display
	 */
	public String getName() {
		
		return pafUserSecurity.getDisplayName();
		
	}

	public Image getImage() {

		
		if ( imageMap == null ) {
			imageMap = new HashMap<String, Image>();
		}
		
		String imageName = "icons" + File.separator + "user.png";
		
		if ( /*PafUserSecurity.getAdmin() != null &&*/ pafUserSecurity.getAdmin() ) {
			
			imageName = "icons" + File.separator + "user.png";
			
		}			
		
		Image image = imageMap.get(imageName);
				
		if ( image == null ) {
						
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor(imageName);
			
			if (imageDesc != null ) {
				image = imageDesc.createImage();
				imageMap.put(imageName, image);
			}
		}
		
		return image;
		
		
	
		 
	}

	public String getKey() {
		
		if ( pafUserSecurity != null ) {
			
			return pafUserSecurity.getKey();
			
		}
		
		return null;
		
	}
	
}

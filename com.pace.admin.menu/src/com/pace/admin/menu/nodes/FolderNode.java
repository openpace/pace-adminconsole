/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import com.pace.admin.global.interfaces.ITreeNode;

public class FolderNode extends TreeNode
{
	private File fFolder; /* actual data object */
	
	public FolderNode(File folder) {
		this(null, folder);
	}
	
	public FolderNode(ITreeNode parent, File folder) {
		super(parent);
		fFolder = folder;
	}
 
	public String getName() {		
		return "FOLDER: " + fFolder.getName();
	}
 	
	@SuppressWarnings("unchecked")
	public void createChildren(List children)
	{			
		clearChildren();
		
		File[] childFiles = fFolder.listFiles();
		for(int i=0; i<childFiles.length; i++)
		{
			File childFile = childFiles[i];
			if( childFile.isDirectory() )
				children.add(new FolderNode(this, childFile));
			else
				children.add(new FileNode(this, childFile));
		}
	}	
}

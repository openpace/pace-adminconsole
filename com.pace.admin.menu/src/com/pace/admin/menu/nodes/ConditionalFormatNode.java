/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.ConditionalFormatModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.PafBaseConstants;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.GenFormat;
import com.pace.base.format.LevelFormat;
import com.pace.base.format.MemberFormat;
import com.pace.base.view.ConditionalFormatDimension;

public class ConditionalFormatNode extends TreeNode {
	
	private static Image image;
	private String name;
	private ConditionalFormatModelManager modelManager;
	private ConditionalFormat conditionalFormat;

	public ConditionalFormatNode(ITreeNode parent, IProject project, String formatName, ConditionalFormatModelManager modelManager) {
		super(parent);
		this.project = project;
		this.name = formatName;
		this.modelManager = modelManager;
		this.conditionalFormat = (ConditionalFormat) modelManager.getItem(name);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {
		clearChildren();
	
		if(modelManager == null){
			modelManager = new ConditionalFormatModelManager(project);
			conditionalFormat = (ConditionalFormat) modelManager.getItem(name);
		}
		
		if( conditionalFormat != null ) {
			ConditionalFormatDimension condFormatDimension = conditionalFormat.getPrimaryDimension();
			if( condFormatDimension != null ) {
				Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
				Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
				Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
				for ( Map.Entry<Integer, LevelFormat> entry : levelFormats.entrySet() ) {
					fChildren.add(new ConditionalStyleNode(this, project, PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT + entry.getValue().getLevel() + " (" + entry.getValue().getFormatName() + ")"));
				}
				for ( Map.Entry<Integer, GenFormat> entry : genFormats.entrySet() ) {
					
					fChildren.add(new ConditionalStyleNode(this, project, PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT + entry.getValue().getGeneration() + " (" + entry.getValue().getFormatName() + ")"));
				}
				for ( Map.Entry<String, MemberFormat> entry : memberFormats.entrySet() ) {
					fChildren.add(new ConditionalStyleNode(this, project, entry.getKey() + " (" + entry.getValue().getFormatName() + ")"));
				}

			}
		}
		
	}

	@Override
	public boolean hasChildren() {

		return conditionalFormat != null;
	}

	public String getName() {
		return name;
	}
	
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "conditional_format.png");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		
		return image;
		
	}	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.PafDimSpec;
import com.pace.base.app.PafWorkSpec;

public class UserSecurityRoleNode extends TreeNode {
	
	private PafWorkSpec[] pafWorkSpecAr = null;
	
	private String name = null;
	
	private static Image image;
	
	public UserSecurityRoleNode(ITreeNode parent, IProject project, String name, PafWorkSpec[] pafWorkSpecAr) {
		super(parent);
		this.project = project;
		this.pafWorkSpecAr = pafWorkSpecAr;
		this.name = name;
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void createChildren(List children) {
	
		logger.debug("Creating Children of  User Security Role Node: " + name);
		
		if ( pafWorkSpecAr != null) {
		
			clearChildren();
			
			for ( PafWorkSpec pafWorkSpec : pafWorkSpecAr ) {
				
				if ( pafWorkSpec.getDimSpec() != null ) {
					
					for ( PafDimSpec pafDimSpec : pafWorkSpec.getDimSpec()) {
						
						fChildren.add(new UserSecurityDimensionNode(this, project, pafDimSpec));
						
					}
					
					
				}
				
			}
		}
	}

	@Override
	public boolean hasChildren() {

		return pafWorkSpecAr != null;
	}
	

	public String getName() {
		return name;
	}
	
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "role.png");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		
		return image;
		
	}	
}

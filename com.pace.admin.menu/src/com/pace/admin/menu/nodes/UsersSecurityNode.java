/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafUserSecurity;

public class UsersSecurityNode extends TreeNode {

	private PafUserSecurity[] pafUsers; 
	
	public UsersSecurityNode(ITreeNode parent, IProject project) {
		super(parent);
		this.project = project;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {

		clearChildren();
		
		if(children == null){
			pafUsers = PafApplicationUtil.getPafSecurityUsers(project);
		}
		

		if (pafUsers != null) {
			
			logger.debug("Creating Children of  User Security Nodes");

			Map<String, Map<String, PafUserSecurity>> domainUserSecurityMap = new TreeMap<String, Map<String, PafUserSecurity>>(String.CASE_INSENSITIVE_ORDER);
			
			for (PafUserSecurity pafUser : pafUsers) {
								
				String domainName = pafUser.getDomainName();
				
				if ( domainName == null || domainName.equals("")) {
					
					domainName = PafBaseConstants.Native_Domain_Name;
					
				}
				
				Map<String, PafUserSecurity> pafUserSecurityMap = null;
				
				if ( domainUserSecurityMap.containsKey(domainName) ) {
					
					pafUserSecurityMap = domainUserSecurityMap.get(domainName);
					
				} 
				
				if ( pafUserSecurityMap == null ) {
					
					pafUserSecurityMap = new TreeMap<String, PafUserSecurity>(String.CASE_INSENSITIVE_ORDER);
					
				}
				
				pafUserSecurityMap.put(pafUser.getUserName(), pafUser);
								
				domainUserSecurityMap.put(domainName, pafUserSecurityMap);
			}
			
			List<String> domainNameList = new ArrayList<String>();
			
			if ( domainUserSecurityMap.keySet().size() > 0 ) {
				
				domainNameList.addAll(domainUserSecurityMap.keySet());
				
				if (domainNameList.contains(PafBaseConstants.Native_Domain_Name)) {
		
					domainNameList.remove(PafBaseConstants.Native_Domain_Name);
					
					domainNameList.add(0, PafBaseConstants.Native_Domain_Name);
					
				}
			
			}
			
			
			for (String domainName : domainNameList) {
				

				List<PafUserSecurity> orderedUserSecurityList = new ArrayList<PafUserSecurity>();
				
				if ( domainUserSecurityMap.get(domainName) != null ) {
					
					orderedUserSecurityList.addAll(domainUserSecurityMap.get(domainName).values());
					
				}
				
				fChildren.add(new UserSecurityDomainNode(this, project, domainName, orderedUserSecurityList));
				
				
			}

		}

	}
	
	@Override
	public boolean hasChildren() {

		if(pafUsers == null){
			pafUsers = PafApplicationUtil.getPafSecurityUsers(project);
		}
		
		return pafUsers != null && pafUsers.length > 0;
	}

	public Image getImage() {

		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	public String getName() {

		return "User Security";
	}

}

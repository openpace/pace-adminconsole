/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE.SharedImages;

import com.pace.admin.global.interfaces.ITreeNode;

/**
 * 
 * Main node for a project.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ProjectNode extends TreeNode {

	private boolean isBuilt;
	
	public ProjectNode(ITreeNode parent, IProject project) {
		super(parent);
		this.project = project;
		this.isBuilt = false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {

		if(!this.isBuilt){
			logger.debug("Loading Dummy Node");
			children.add(new DummyNode(this, project));
		} else {
			logger.debug("Loading Views");
			ViewsNode viewsNode = new ViewsNode(this, project);
			children.add(viewsNode);
			logger.debug("Loading View Sections");
			children.add(new ViewSectionsNode(this, project));
			logger.debug("Loading View Groups");
			children.add(new ViewGroupsNode(this, project, viewsNode.getViewModelManager()));
			logger.debug("Loading Security Nodes");
			children.add(new SecurityNode(this, project));
			logger.debug("Loading Roles/Processes");
			children.add(new RolesProcessesNode(this, project));
			logger.debug("Loading Styles and Formats");
			children.add(new StylesFormatsNode(this, project));
			logger.debug("Loading Print Styles");
			children.add(new PrintStylesNode(this, project));
			logger.debug("Loading Project Settings");
			children.add(new ProjectSettingsWizardNode(this));
			logger.debug("Loading User Selections");
			children.add(new UserSelectionDialogNode(this));
			logger.debug("Loading Dynamic Member");
			children.add(new DynamicMembersDialogNode(this));
			logger.debug("Loading Member Tags");
			children.add(new MemberTagDialogNode(this));
		}
	}

	public Image getImage() {
		return PlatformUI.getWorkbench().getSharedImages().getImage(SharedImages.IMG_OBJ_PROJECT);
	}

	public String getName() {
		
		return project.getName();
		
	}
	
	@Override
	public boolean hasChildren() {

		return true;
	}

	public void buildTree1(){
		//Don't rebuild the structure if it's built
		if(this.isBuilt) return;
		
		clearChildren();
		createChildren(fChildren);
		this.isBuilt = true;

	}

	/**
	 * Returns true if the project has loaded it's nodes.
	 * @return
	 */
	public boolean isBuilt() {
		return isBuilt;
	}

	public void setBuilt(boolean isBuilt) {
		
		if(this.isBuilt != isBuilt){
			this.isBuilt = isBuilt;
			clearChildren();
			createChildren(fChildren);
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ViewGroupItemNode extends TreeNode {

	//private  PafViewGroupItem pafViewGroupItem;
	private  Object pafViewGroupItem;
	
//	public ViewGroupItemNode(ITreeNode parent, IProject project, PafViewGroupItem pafViewTreeItem) {
//		super(parent, project);
//		this.pafViewGroupItem = pafViewTreeItem;
//
//	}
	
	public ViewGroupItemNode(ITreeNode parent, IProject project, Object pafViewTreeItem) {
		super(parent, project);
		this.pafViewGroupItem = pafViewTreeItem;

	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#createChildren(java.util.List)
	 */
	@Override
	public void createChildren(List children) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.ITreeNode#getName()
	 */
	public String getName() {
		if (pafViewGroupItem instanceof ViewGroupNode) {
			
			ViewGroupNode obj = (ViewGroupNode) pafViewGroupItem;
			
			return obj.getName();
			
		} else{
			//ViewSectionNode obj = (ViewSectionNode) pafViewGroupItem;
			ViewNode obj = (ViewNode) pafViewGroupItem;
			
			return obj.getName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#getImage()
	 */
	@Override
	public Image getImage() {
		
		Image image = null;
		
		ImageDescriptor imageDesc = null;
		
		if (pafViewGroupItem instanceof ViewGroupNode) {
			
			ViewGroupNode obj = (ViewGroupNode) pafViewGroupItem;
			
			if(obj != null) {
				imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "viewGroup2.ico");
			}
			else{
				imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "wi0064-16.gif");
			}	
		} else {
			
			imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "wi0064-16.gif");
			
		}			
		
		if (imageDesc != null ) {
			image = imageDesc.createImage();
		}
		
		
		return image;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.base.app.PafUserSecurity;

public class UserSecurityDomainNode extends TreeNode {

	private String domainName;
	private List<PafUserSecurity> orderedUserSecurityList;
	
	public UserSecurityDomainNode(ITreeNode parent, IProject project, String domainName, List<PafUserSecurity> orderedUserSecurityList) {
		super(parent);
		this.project = project;
		this.domainName = domainName;
		this.orderedUserSecurityList = orderedUserSecurityList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {

		clearChildren();

		if ( orderedUserSecurityList != null ) {
			
			logger.debug("Creating Children of  User Security Domain Node: " + domainName);
		
			for (Object object : orderedUserSecurityList ) {
				
				if ( object instanceof PafUserSecurity ) {
					
					PafUserSecurity pafUser = (PafUserSecurity) object;
					
					fChildren.add(new UserSecurityNode(this, project, pafUser));					
					
				}
				
				
			}
			
			
		}	

	}

	@Override
	public boolean hasChildren() {

		return orderedUserSecurityList != null && orderedUserSecurityList.size() > 0;
	}
	
	public Image getImage() {

		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	public String getName() {

		return domainName;
	}

}

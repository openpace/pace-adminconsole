/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.interfaces.ITreeNode;

public class ProjectSettingsWizardNode extends TreeNode {

//	private final static String PROJECT_SETTINGS_WIZARD_NAME = "Project Settings Wizard";
	
	public ProjectSettingsWizardNode(ITreeNode parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	public ProjectSettingsWizardNode(ITreeNode parent, IProject project) {
		super(parent, project);
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return "Project Settings Wizard";
	}

	public void createChildren(List children) {
		// TODO Auto-generated method stub

	}

	public Image getImage() {
		
		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_TOOL_NEW_WIZARD);
	}
}

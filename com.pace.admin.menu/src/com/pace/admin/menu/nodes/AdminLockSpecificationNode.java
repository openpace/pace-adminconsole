/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.PafDimSpec;
import com.pace.base.utility.StringUtils;

public class AdminLockSpecificationNode extends TreeNode {
	
	private PafDimSpec pafDimSpec;

	private static Image image;
	
	public AdminLockSpecificationNode(ITreeNode parent, IProject project) {
		super(parent);
		this.project = project;
	}
	
	public AdminLockSpecificationNode(ITreeNode parent, IProject project, PafDimSpec pafDimSpec) {
		super(parent);
		this.project = project;
		this.pafDimSpec = pafDimSpec;
		createChildren(null);
	}

	public String getName() {
		if ( pafDimSpec != null) {
			
			String[] expressionList = pafDimSpec.getExpressionList(); 
			if( expressionList != null && expressionList.length > 0 ) 
				return pafDimSpec.getDimension() + " - " + StringUtils.arrayToString(expressionList, ",");
		}
		return	"Admin Lock Specificaiton 1";
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void createChildren(List children) {
	}
	
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "file_obj.gif");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		return image;
	}	
}

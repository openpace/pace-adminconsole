/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.ViewModelManager;

public class ViewsNode extends ModelTreeNode {

	public ViewsNode(ITreeNode parent, IProject project) {
		super(parent, project, new ViewModelManager(project));
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {

		super.createChildren(children);
		
		if(modelManager == null){
			modelManager = new ViewModelManager(project);
			modelKeys = modelManager.getKeys();
		}
		
		logger.debug("Creating View Nodes");
		
		clearChildren();

		for (String viewName : modelKeys) {

			fChildren.add(new ViewNode(this, project, viewName));
		}
	}
	

	public Image getImage() {

		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	public String getName() {

		return Constants.MENU_VIEWS;
	}

	public ViewModelManager getViewModelManager() {
		return (ViewModelManager) modelManager;
	}

}

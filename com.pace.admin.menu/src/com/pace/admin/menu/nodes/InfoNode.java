/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;

public class InfoNode extends TreeNode {

	private String labelName;
	
	public InfoNode(ITreeNode parent, String labelName) {
		super(parent);
		this.labelName = labelName;
	}

	@Override
	public void createChildren(List children) {
		
		fChildren = children;

	}
	
	public Image getImage() {
		return null; 
	}

	public String getName() {
		return labelName;
	}

}

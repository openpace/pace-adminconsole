/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;

/**
 * Dynamic Members Dialog Node
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DynamicMembersDialogNode extends TreeNode {
	
	/**
	 * Constructor
	 * 
	 * @param parent
	 */
	public DynamicMembersDialogNode(ITreeNode parent) {
		super(parent);
	}

	/**
	 * returns name of node.
	 */
	public String getName() {		
		return Constants.DYNAMIC_MEMBERS_WIZARD_NAME;
	}
 
	/**
	 * returns image for node
	 */
	public Image getImage() {
		
		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_TOOL_NEW_WIZARD);
	}
	
	//used to create children, but no children for this node
	public void createChildren(List children) {		
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.nodes;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.SeasonModelManager;

/**
 * @author fskrgic
 *
 */
public class SeasonRemoval implements IRemoval {

	
	private String name;
	private SeasonModelManager manager;
	
	/**
	 * 
	 */
	public SeasonRemoval(String seasonName, SeasonModelManager seasonModelManager) {

		name = seasonName;
		manager = seasonModelManager;
		
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.IRemoval#removalDescription()
	 */
	public String removalDescription() {

		return "Deletion of season: " + name;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.IRemoval#remove()
	 */
	public void remove() {
		manager.remove(name);

	}
	
	public Object getModelManager(){
		return manager;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.base.view.PafViewGroup;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ViewGroupsNode extends TreeNode {

	private ViewModelManager viewManager;
	
	public ViewGroupsNode(ITreeNode parent, IProject project, ViewModelManager viewManager) {
		super(parent);
		this.project = project;
		this.viewManager = viewManager;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#createChildren(java.util.List)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void createChildren(List children) {
		
		logger.debug("Creating View Group Nodes");
		
		clearChildren();

		if(children == null){
			viewManager = null;
		}
		
		if(viewManager == null){
			viewManager = new ViewModelManager(project);
		}
		
		Map<String, PafViewGroup> modelManager = viewManager.getViewGroupModel();

		String[] keys = modelManager.keySet().toArray(new String[modelManager.keySet().size()]);
		
		if( keys != null && keys.length > 0 ) {
			Arrays.sort(keys, String.CASE_INSENSITIVE_ORDER);
			
			for (String key :  keys) {
				
				PafViewGroup pafViewGroup = (PafViewGroup) modelManager.get(key);
							
				fChildren.add(new ViewGroupNode(this, 
						project, 
						pafViewGroup.getName(),
						modelManager,
						viewManager));
				
			}
		}
		else {
			ConsoleWriter.writeMessage("Invalid Pace Project: " + project.getName());
			
		}
		
		viewManager = null;
		modelManager = null;
		
	}
	
	@Override
	public boolean hasChildren() {

		return true;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.ITreeNode#getName()
	 */
	public String getName() {
		return Constants.MENU_VIEW_GROUPS;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#getImage()
	 */
	@Override
	public Image getImage() {
		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}
}

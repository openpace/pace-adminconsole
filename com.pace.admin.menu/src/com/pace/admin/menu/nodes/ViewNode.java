/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;

public class ViewNode extends TreeNode {
	
	private String name;
	protected static Image image;
	private List<String> ViewSections;
	
	public ViewNode(ITreeNode parent, IProject project, String name) {
		super(parent);
		this.project = project;
		this.name = name;
		this.ViewSections = new ArrayList<String>();
	}
	
	public ViewNode(ITreeNode parent,IProject project, String name, List<String> viewSections) {
		super(parent);
		this.project = project;
		this.name = name;
		this.ViewSections = viewSections;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {
		
		clearChildren();
		
		List<String> viewSections = getViewSections();
		
		if(viewSections != null && viewSections.size() > 0){
		
			for(String viewSection : getViewSections()){
				fChildren.add(new ViewSectionNode(this, project, viewSection));
			}
			
		}
	}
	
	@Override
	public boolean hasChildren() {

		try {
			return ViewSections != null && ViewSections.size() > 0;
		} catch (Exception e){
			return false;
		}
	}

	public String getName() {
		return name; 
	}

	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "wi0064-16.gif");
			//ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons\\31-Document_16x16.png");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		
		return image;
		
	}

	public void setViewSections(List<String> viewSections) {
		ViewSections = viewSections;
	}

	public List<String> getViewSections() {
		return ViewSections;
	}	
	
}

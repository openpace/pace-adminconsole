/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;

public class NumericFormatWizardNode extends TreeNode {
	private String name;
	
	public NumericFormatWizardNode(ITreeNode parent) {
		super(parent);
	}

	public NumericFormatWizardNode(ITreeNode parent, String name) {
		super(parent);
		this.name = name;
	}

	public String getName() {		
//		return Constants.NUMERIC_FORMATTING_WIZARD_NAME;
		return name;
	}
 
	public Image getImage() {
		
		return MenuPlugin.getImageDescriptor("icons" + File.separator + "number.png").createImage();
	}
	
	public void createChildren(List children) {		
	}

}
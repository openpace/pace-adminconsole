/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;

import com.pace.admin.global.interfaces.ITreeNode;

public class RootNode extends TreeNode {

	public RootNode(ITreeNode parent) {
		super(parent);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addProject(ProjectNode project) {
		
		if ( fChildren == null ) {
			fChildren = new ArrayList();
		}
		fChildren.add(project);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void createChildren(List children) {
		// TODO Auto-generated method stub

	}

	public String getName() {
		return "Paf Projects";
	}

	public List<IProject> getChildProjects(){
		List<IProject> projects = new ArrayList<IProject>();
		
		for(Object child : getChildren()){
			if(child instanceof ProjectNode){
				projects.add(((ProjectNode)child).getProject());
			}
		}
		return projects;
	}
	
}

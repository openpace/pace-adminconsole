/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.PafPlannerConfigModelManager;
import com.pace.admin.global.model.managers.UserSecurityModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.comm.PafPlannerConfig;

/**
* PlannerRoleNode extenstion of TreeNode
* @author kmoos
* @version	x.xx
*
*/
public class PlannerRoleNode extends TreeNode {
	protected static Image image;
	private String name;

	/**
	 * Constructor
	 * @param parent The parent tree node.
	 * @param name The name of the node to create.
	 */
	public PlannerRoleNode(ITreeNode parent, String name) {
		super(parent);
		this.name = name;
	}

	/**
	 * Implementation of TreeNode.createChildren()
	 * @param children
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void createChildren(List children) {
	}

	/**
	 * Implementation of TreeNode.getName()
	 * @return String
	 */
	public String getName() {
		return name; 
	}

	/**
	 * Implementation of TreeNode.getImage()
	 * @return Image
	 */
	public Image getImage() {

		if ( image == null ) {
			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons" + File.separator + "role.png");
			if (imageDesc != null ) {
				image = imageDesc.createImage();
			}
		}
		return image;
	}	
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
	
	@SuppressWarnings("rawtypes")
	public List<IRemoval> getDependants(IProject project){
		
		ArrayList<IRemoval> dependants = new ArrayList<IRemoval>();
		
		UsersSecurityNode usersSecurityNode = null;
		RoleConfigurationsNode roleConfigurationsNode = null;
		
		TreeNode plannerRoleNodes = (TreeNode) getParent();
		TreeNode rolesProcessesNode = (TreeNode) plannerRoleNodes.getParent();
		TreeNode projectNode = (TreeNode) rolesProcessesNode.getParent();
		
		for (Object objectNode : projectNode.getChildren()) {
			if(objectNode instanceof SecurityNode){
				for(Object node : ((ITreeNode) objectNode).getChildren()){
					usersSecurityNode = (UsersSecurityNode) node;
					break;
				}
			}
		}

		for (Object objectNode : rolesProcessesNode.getChildren()) {
			if(objectNode instanceof RoleConfigurationsNode){
				roleConfigurationsNode = (RoleConfigurationsNode) objectNode;
				break;
			}
		}
		
		if(usersSecurityNode == null || roleConfigurationsNode == null){
			return null;
		}
		
		List securityNodes = usersSecurityNode.getChildren();
		List configurationNodes = roleConfigurationsNode.getChildren();

		UserSecurityModelManager userSecurityModelManager = new UserSecurityModelManager(project);
		
		for(Object domainNameSecurityNode : securityNodes){
				
			if ( domainNameSecurityNode instanceof UserSecurityDomainNode ) {
			
				for (Object userSecurityNode : ((UserSecurityDomainNode) domainNameSecurityNode).getChildren()) {
				
					if ( userSecurityNode instanceof UserSecurityNode ) {
						
						UserSecurityNode securityNode = (UserSecurityNode) userSecurityNode;
						
						if ( userSecurityModelManager.contains(securityNode.getKey())) {
						
							PafUserSecurity pafUserSecurity = (PafUserSecurity) userSecurityModelManager.getItem(securityNode.getKey());
														
							String [] roleNames = pafUserSecurity.getRoleNames();
							
							if ( roleNames != null ) {
								
								for (String roleName : roleNames) {
									
									if(roleName.equalsIgnoreCase(name)){
										
										dependants.add(new RoleFromSecurityRemoval(name, pafUserSecurity, userSecurityModelManager));
										
									}
								}
								
							}
							
						}
					}
				}
				
			}
			
		}
		
		
		PafPlannerConfigModelManager pafPlannerConfigModelManager = new PafPlannerConfigModelManager(project);
		
		for(Object objectNode : configurationNodes){
			RoleConfigurationNode configurationNode = (RoleConfigurationNode) objectNode;
			PafPlannerConfig pafPlannerConfig = (PafPlannerConfig) pafPlannerConfigModelManager.getItem(configurationNode.getName());
			if(pafPlannerConfig.getRole().equalsIgnoreCase(name)){
				
				/*
				String role = pafPlannerConfig.getRole();
				if(role == null || role.trim().equalsIgnoreCase("")) {
					role = PafPlannerConfigModelManager.NO_ROLE;
				}
				
				
				String cycle = pafPlannerConfig.getCycle();
				if(cycle == null || cycle.trim().equalsIgnoreCase("")) {
					cycle = PafPlannerConfigModelManager.NO_CYCLE;
				}
				*/
				
				String genKey = pafPlannerConfigModelManager.getKey(pafPlannerConfig.getRole(), pafPlannerConfig.getCycle());
				
				dependants.add(new RoleConfigurationRemoval(genKey, pafPlannerConfigModelManager));
			}
		}
		
		return dependants;
	}
}

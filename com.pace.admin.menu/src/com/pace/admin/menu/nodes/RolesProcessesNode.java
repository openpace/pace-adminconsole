/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.interfaces.ITreeNode;

/**
* RolesProcessesNode(parent) extenstion of TreeNode
* @author kmoos
* @version	x.xx
*
*/
public class RolesProcessesNode extends TreeNode {
	/**
	 * Constructor
	 * @param parent The parent TreeNode.
	 * @param project The IProject.
	 */
	public RolesProcessesNode(ITreeNode parent, IProject project) {
		super(parent);
		this.project = project;
	}

	/**
	 * Implementation of TreeNode.createChildren()
	 * Creates all the nodes for the Role & Processes.
	 * @param children
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {
		
		clearChildren();
		
		logger.debug("Loading Plan Cycles Node");
		fChildren.add(new PlanCyclesNode(this, project));
		logger.debug("Loading Seasons Node");
		fChildren.add(new SeasonsNode(this, project));
		logger.debug("Loading Planner Roles Node");
		fChildren.add(new PlannerRolesNode(this, project));
		logger.debug("Loading Role Configurations Node");
		fChildren.add(new RoleConfigurationsNode(this, project));
	}

	@Override
	public boolean hasChildren() {

		return true;
	}
	
	/**
	 * Implementation of TreeNode.getImage()
	 * @return Image
	 */
	public Image getImage() {
		return PlatformUI.getWorkbench().getSharedImages().getImage(
				ISharedImages.IMG_OBJ_FOLDER);
	}

	/**
	 * Implementation of TreeNode.getName()
	 * @return String
	 */
	public String getName() {
		return Constants.MENU_ROLES_PROCESS;
	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
}

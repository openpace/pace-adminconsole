/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.ViewModelManager;
import com.pace.admin.menu.MenuPlugin;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.view.PafViewGroup;
import com.pace.base.view.PafViewGroupItem;

/**
 * View Group Node
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ViewGroupNode extends TreeNode {

	private String viewGroupName;
	private Map<String, PafViewGroup> viewGroupModel;
	private ViewModelManager viewModelManager;

	/**
	 * @param parent
	 * @param project
	 * @param viewGroupName
	 * @param viewGroupModelManager
	 * @param viewModelManager
	 */
	public ViewGroupNode(ITreeNode parent, IProject project, String viewGroupName,
			Map<String, PafViewGroup>  viewGroupModel, ViewModelManager viewModelManager) {
		super(parent);
		this.project = project;
		this.viewGroupName = viewGroupName;
		this.viewGroupModel = viewGroupModel;
		this.viewModelManager = viewModelManager;
	}
	
	

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#createChildren(java.util.List)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void createChildren(List children) {
		
		logger.debug("Creating Children Nodes for View Group: " + viewGroupName);
		
		clearChildren();
		
		if(children == null){
			viewModelManager = null;
			viewGroupModel = null;
		}
		
		if(viewModelManager == null) {
			viewModelManager = new ViewModelManager(project);
			viewGroupModel = viewModelManager.getViewGroupModel();
		}
		
		TreeMap<String, PafAdminConsoleView> modelMap = (TreeMap<String, PafAdminConsoleView>) viewModelManager.getModel();
	
		PafViewGroup pafViewGroup = (PafViewGroup) viewGroupModel.get(viewGroupName);
		
		if ( pafViewGroup != null) {
		
			if ( pafViewGroup.getPafViewGroupItems() != null ) {
				
			
				for (PafViewGroupItem pafViewGroupItem : pafViewGroup.getPafViewGroupItems()) {
							
					if(pafViewGroupItem.isViewGroup()){
						
						fChildren.add(new ViewGroupNode(this, 
								project, 
								pafViewGroupItem.getName(),
								viewGroupModel,
								viewModelManager));
						
					} else {
						
						if(modelMap != null && pafViewGroupItem != null && pafViewGroupItem.getName() != null){
							//get model from map
							PafAdminConsoleView model = modelMap.get(pafViewGroupItem.getName());
							
							List<String> s = new ArrayList<String>();
							//get the set of view section names
							if(model != null && model.getViewSectionNames() != null &&
									model.getViewSectionNames().size() > 0){
								Set<String> currentViewSectionNames = model.getViewSectionNames();
								//currently, just get the first one
								String currentModelViewSectionName = currentViewSectionNames.toArray(new String[0])[0];
								//create the list and add the item.
								s.add(currentModelViewSectionName);
							}
							//create the node.
							fChildren.add(new ViewNode(this, project, pafViewGroupItem.getName(), s));
						}
					}
					
				}
			
			}
			
		}		

	}

	@Override
	public boolean hasChildren() {

		try {
			PafViewGroup pafViewGroup = (PafViewGroup) viewGroupModel.get(viewGroupName);
			return pafViewGroup.getPafViewGroupItems().length > 0;
		} catch (Exception e){
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.ITreeNode#getName()
	 */
	public String getName() {
				
		return viewGroupName;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.nodes.TreeNode#getImage()
	 */
	@Override
	public Image getImage() {
		
		return null;
		
	}

	@Override
	public ImageDescriptor getImageDescriptor(Object object) {		
		return MenuPlugin.getImageDescriptor("icons" + File.separator + "viewGroup2.png");
	}
	
	

}

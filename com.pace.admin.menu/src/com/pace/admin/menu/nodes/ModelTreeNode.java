/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.List;

import org.eclipse.core.resources.IProject;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.global.model.managers.ModelManager;

/**
 * Extension of a TreeNode that's bound to a ModelManager
 * @author kmoos
 *
 */
public abstract class ModelTreeNode extends TreeNode {

	protected ModelManager modelManager;
	protected String[] modelKeys;
	
	public ModelTreeNode(ITreeNode parent, IProject project, ModelManager modelManager) {
		super(parent);
		this.project = project;
		this.modelManager = modelManager;
		this.modelKeys = modelManager.getKeys();
	}

	public abstract String getName();

	@SuppressWarnings("rawtypes")
	@Override
	/**
	 * If children are null, then the modelManager, and modelKeys are nulled.
	 * This method also calls clearChildren();
	 */
	public void createChildren(List children) {
		if(children == null){
			modelManager = null;
			modelKeys = null;
		}
		clearChildren();
	}
	
	@Override
	public final boolean hasChildren() {

		return modelKeys != null && modelKeys.length > 0;
	}

}

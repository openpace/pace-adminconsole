/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.IWorkbenchAdapter;

import com.pace.admin.global.interfaces.ITreeNode;


/* Default or adapter implementation for ITreeNode */
public abstract class TreeNode implements ITreeNode, IWorkbenchAdapter {
	
	protected static Logger logger = Logger.getLogger(TreeNode.class);
	
	protected IProject project;
	
	protected ITreeNode fParent;

	protected List fChildren;
	
	/**
	 * Constructor.  Does not call createChildren()
	 * @param parent
	 */
	public TreeNode(ITreeNode parent) {
		fParent = parent;
	}
	
	/**
	 * Constructor.  Note that this Constructor call createChildren() and will build the nodes before needed.
	 * @param parent
	 * @param project
	 */
	public TreeNode(ITreeNode parent, IProject project) {
		this(parent);
		this.project = project;
		this.fChildren = new ArrayList();
		createChildren(fChildren);
	}
	

	public Image getImage() {
		return null; 
	}

	/**
	 * Called by the framework.  If this returns true then the node is expandable, 
	 * if false the node will not show the expander.
	 * Subclasses can (and should) Override
	 */
	public boolean hasChildren() {

		if ( fChildren != null ) {
			return fChildren.size() > 0;
		}
			
		return false;
	}

	public ITreeNode getParent() {
		return fParent;
	}

	/**
	 * Called by the framework to get the child nodes of the current node.  
	 * Returns the children nodes of the current node.
	 * In general one should not call this directly.
	 */
	public List getChildren() {
		if (fChildren != null)
			return fChildren;

		fChildren = new ArrayList();
		createChildren(fChildren);

		return fChildren;
	}

	/**
	 * Removes all the child nodes of the current node.
	 */
	protected void clearChildren() {
		if ( fChildren != null ) {
			fChildren.clear();
		} else {
			fChildren = new ArrayList();
		}
	}
	
	/**
	 * Creates the list of children that will be returned to the framework when getChildren() is called.
	 * @param children
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public abstract void createChildren(List children);

	/**
	 * Gets the project associated with the nodes
	 * @return
	 * @since TTN-1439 add the if/else to fix issue where grandchild nodes would have project set to null;
	 */
	public IProject getProject() {
		if(project != null){
			return project;
		} else {
			return ((TreeNode) getParent()).getProject();
		}
	}

	/**
	 * Sets the project associated to the nodex.
	 * @param project
	 */
	public void setProject(IProject project) {
		this.project = project;
	}

	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public ImageDescriptor getImageDescriptor(Object object) {
		
		Image image = getImage();
		
		if ( image != null ) {
		
			return ImageDescriptor.createFromImage(image);
			
		} else {
			
			return null;
			
		}
	}

	public String getLabel(Object o) {
		return getName();
	}

	public Object getParent(Object o) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 * @return the first ITreeNode that matches the IProjectName
	 */
	public ITreeNode getProjectNode(){
		try {
			return getProjectRoot(this);
		} catch(Exception ex){
			return null;
		}
	}
	
	private ITreeNode getProjectRoot(ITreeNode node){
		ITreeNode parent = node.getParent();
		if(parent != null){
			if(!getProject().getName().equalsIgnoreCase(parent.getName())){
				return getProjectRoot(node.getParent());
			} else {
				return parent;
			}
		}
		return node;
	}
	
}

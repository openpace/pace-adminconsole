/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.menu.nodes;

import java.util.ArrayList;

import com.pace.admin.global.interfaces.IRemoval;
import com.pace.admin.global.model.managers.PlannerRoleModelManager;
import com.pace.base.app.PafPlannerRole;

/**
 * @author fskrgic
 *
 */
public class SeasonFromRoleRemoval implements IRemoval {

	private String name;
	private PafPlannerRole role;
	private PlannerRoleModelManager manager;
	
	/**
	 * 
	 */
	public SeasonFromRoleRemoval(String seasonName, PafPlannerRole plannerRole, PlannerRoleModelManager plannerRoleModelManager) {

		this.name = seasonName;
		this.role = plannerRole;
		this.manager = plannerRoleModelManager;
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.IRemoval#removalDescription()
	 */
	public String removalDescription() {

		return "Removal of season: " + name + " from role: " + role.getRoleName();
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.global.interfaces.IRemoval#remove()
	 */
	public void remove() {
		
		ArrayList<String> newSeasons = new ArrayList<String>();
		
		String [] seasons = role.getSeasonIds();
		
		if ( seasons != null ) {
			
			for (String season : seasons) {
				if( ! season.equalsIgnoreCase(name) ){
					newSeasons.add(season);
				}
			}
			
		}
		
		if ( newSeasons.size() == 0 ) {
			role.setSeasonIds(null);
		} else {
			role.setSeasonIds(newSeasons.toArray(new String[0]));	
		}
		
		if ( this.manager != null ) {
			
			this.manager.replace(role.getRoleName(), role);
			
			this.manager.save();
		}
		
	}
	
	public Object getModelManager(){
		return manager;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.nodes;

import java.io.File;
import java.util.List;

import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.admin.menu.MenuPlugin;

public class GlobalStylesWizardNode extends TreeNode {
	
	protected static Image image;
	private String name;

	public GlobalStylesWizardNode(ITreeNode parent) {
		super(parent);
	}

	public GlobalStylesWizardNode(ITreeNode parent, String name) {
		super(parent);
		this.name = name;
	}

	public String getName() {		
//		return Constants.GLOBAL_SYTLES_WIZARD_NAME;
		return name;
	}
 
	public Image getImage() {
//		if ( image == null ) {
//			ImageDescriptor imageDesc = MenuPlugin.getImageDescriptor("icons\\wi0064-16.gif");
//			if (imageDesc != null ) {
//				image = imageDesc.createImage();
//			}
//		}
//		return image;
		return MenuPlugin.getImageDescriptor("icons" + File.separator + "global_style.png").createImage();
		
//		return PlatformUI.getWorkbench().getSharedImages().getImage(
//				ISharedImages.IMG_TOOL_NEW_WIZARD);
	}
	
	public void createChildren(List children) {		
	}

}
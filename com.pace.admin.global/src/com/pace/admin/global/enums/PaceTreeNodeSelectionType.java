/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.enums;

public enum PaceTreeNodeSelectionType {
	
	Selection, //Selection only: e.g. @UOW_ROOT
	IChildren, //Selection and Children: @ICHILDREN(@UOW_ROOT)
	Children, //Children only: @CHILDREN(@UOW_ROOT) 
	IDescendants, //Selection and Descendants at bottom, @IDESC(@UOW_ROOT)
	IDescLevel, // Selection and Descendants only with level: @IDESC(@UOW_ROOT,Lx)
	IDescGen, //Selection and Descendants only with generation: @IGEN(@UOW_ROOT,Gx)
	Descendants, //Descendants at bottom, @DESC(@UOW_ROOT)
	DescLevel, //Descendants only with level: @DESC(@UOW_ROOT,Lx)
	DescGen, // Descendants only with generation: @GEN(@UOW_ROOT,Gx)
	Members, //Members: e.g. @MEMBERS(@UOW_ROOT)
	ILevel, // Just Level at bottom: e.g. @LEVEL(@UOW_ROOT)
	Level, // Just Level: e.g. @LEVEL(@UOW_ROOT,Lx)
	Generation, //Just Generation: e.g. @GEN(@UOW_ROOT,Gx)
	None, 
	LevelGen,
	OffsetMember,//Offset Member: e.g. @OFFSET_MEMBERS(@UOW_ROOT,x,x) 
	OffsetMemberRange, //Offset Member Range: e.g. @OFFSET_MEMBERS(@UOW_ROOT,x,y)
}

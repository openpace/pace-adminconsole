/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;

import com.pace.admin.global.GlobalPlugin;
import com.pace.admin.global.model.PaceTree;
import com.pace.admin.global.model.PaceTreeNode;
import com.swtdesigner.ResourceManager;

public class FindTreeModule extends Composite {

    private Text text;
    private Button search;
    private Tree tree;
    private TreeViewer viewer;
     
	/**
	 * @return the viewer
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * @param viewer
	 *            the viewer to set
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}

	public TreeViewer getTreeViewer(){
		return this.viewer;
	}
	
	public void setTreeViewer(TreeViewer viewer){
		this.viewer = viewer;
	}
    /**
     * Constructs a new instance of this class given its parent
     * and a style value describing its behavior and appearance.
     * <p>
     * The style value is either one of the style constants defined in
     * class <code>SWT</code> which is applicable to instances of this
     * class, or must be built by <em>bitwise OR</em>'ing together 
     * (that is, using the <code>int</code> "|" operator) two or more
     * of those <code>SWT</code> style constants. The class description
     * lists the style constants that are applicable to the class.
     * Style bits are also inherited from superclasses.
     * </p>
     *
     * @param parent a composite control which will be the parent of the new instance (cannot be null)
     * @param style the style of control to construct
     *
     * @exception IllegalArgumentException <ul>
     *    <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
     * </ul>
     * @exception SWTException <ul>
     *    <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the parent</li>
     *    <li>ERROR_INVALID_SUBCLASS - if this class is not an allowed subclass</li>
     * </ul>
     *
     * @see SWT#DROP_DOWN
     * @see SWT#READ_ONLY
     * @see SWT#SIMPLE
     * @see Widget#checkSubclass
     * @see Widget#getStyle
     */
	public FindTreeModule(Composite parent, int style) {
        super(parent, style = checkStyle(style));
        
 		GridLayout gl_container = new GridLayout(4, false);
		this.setLayout(gl_container);      

        this.text = new Text(this, SWT.BORDER);
		GridData gd_textStyleName = new GridData(SWT.FILL, SWT.CENTER, true, false);
		text.setLayoutData(gd_textStyleName);
		text.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if(text.getText().length() > 0){
					search.setEnabled(true);
					if(e.character  == '\r'){
						SearchTree();
					}
				} else {
					search.setEnabled(false);
				}
			}
			
		});

        this.search = new Button(this, SWT.PUSH );
        GridData searchGrid = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        searchGrid.minimumWidth = 55;
        search.setLayoutData(searchGrid);
        search.setImage(ResourceManager.getPluginImage(GlobalPlugin.getDefault(), "icons/search.png"));
        search.setToolTipText("Search the Tree for Text");
        search.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				SearchTree();
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
								
			}

        });
        search.setEnabled(false);
    }
    
    private void SearchTree(){
    	
    	TreeItem[] selection = tree.getSelection();
    	TreeItem theSel = null;
    	if(selection.length == 1){
    		theSel = selection[0];
    	}
    	
    	List<TreeItem> items = new ArrayList<TreeItem>();
    	for(TreeItem leaves : tree.getItems()){
    		items.add(leaves);
    		getChildren(leaves, items);
    	}
    	
    	String temp = text.getText();
    	int ndx = 0;
    	if(theSel != null){
    		ndx = items.indexOf(theSel) + 1;
    	}
    	TreeItem foundItem = null;
    	for(int i = ndx; i < items.size(); i++){
    		TreeItem leaf = items.get(i);
    		if(leaf.getText().toLowerCase().contains(temp.toLowerCase())){
    			foundItem = leaf;
    			break;
    		}
    	}
    	if(foundItem == null){
    		for(int j = 0; j < ndx; j++){
        		TreeItem leaf = items.get(j);
        		if(leaf.getText().toLowerCase().contains(temp.toLowerCase())){
        			foundItem = leaf;
        			break;
        		}    			
    		}
    	}
    	
    	if(foundItem != null){
    		tree.setSelection(foundItem);
    		tree.showSelection();
    		
    		if(viewer != null){
    			StructuredSelection sel = (StructuredSelection) viewer.getSelection();
    			PaceTree paceTree = (PaceTree) viewer.getInput();
    			PaceTreeNode ptn = paceTree.findTreeNode((PaceTreeNode)sel.getFirstElement(), foundItem.getText());
				viewer.setSelection(new StructuredSelection(ptn), true);
			}
    	}
    	
    	//if(foundItem != null && theSel != null && foundItem.getParentItem() != theSel.getParentItem()){
    		//theSel.getParentItem().setExpanded(false);
    	//}
    }
    
    private void getChildren(TreeItem item, List<TreeItem> items){
    	for(TreeItem leaf : item.getItems()){
    		if(!leaf.getText().isEmpty()){
    			items.add(leaf);
    			getChildren(leaf, items);
    		}
    	}
    }

    static int checkStyle(int style) {
        int mask = SWT.BORDER | SWT.READ_ONLY | SWT.FLAT | SWT.LEFT_TO_RIGHT | SWT.RIGHT_TO_LEFT;
        return style & mask;
    }
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;

public class PaceTreeModule extends Composite {

    private Tree tree;
    private FindTreeModule findModule;
     
	/**
	 * @return the viewer
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * @param viewer
	 *            the viewer to set
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}
	
	/**
	 * 
	 * @return
	 */
	public FindTreeModule getFindModule() {
		return this.findModule;		
	}
	
	/**
	 * 
	 * @param findModule
	 */
	public void setFindModule(FindTreeModule findModule){
		this.findModule = findModule;
	}


    /**
     * Constructs a new instance of this class given its parent
     * and a style value describing its behavior and appearance.
     * <p>
     * The style value is either one of the style constants defined in
     * class <code>SWT</code> which is applicable to instances of this
     * class, or must be built by <em>bitwise OR</em>'ing together 
     * (that is, using the <code>int</code> "|" operator) two or more
     * of those <code>SWT</code> style constants. The class description
     * lists the style constants that are applicable to the class.
     * Style bits are also inherited from superclasses.
     * </p>
     *
     * @param parent a composite control which will be the parent of the new instance (cannot be null)
     * @param style the style of control to construct
     *
     * @exception IllegalArgumentException <ul>
     *    <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
     * </ul>
     * @exception SWTException <ul>
     *    <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the parent</li>
     *    <li>ERROR_INVALID_SUBCLASS - if this class is not an allowed subclass</li>
     * </ul>
     *
     * @see SWT#DROP_DOWN
     * @see SWT#READ_ONLY
     * @see SWT#SIMPLE
     * @see Widget#checkSubclass
     * @see Widget#getStyle
     */
	public PaceTreeModule(Composite parent, int style, int treeStyle) {
        super(parent, style = checkStyle(style));
        
 		GridLayout gl_container = new GridLayout(1, false);
		this.setLayout(gl_container);
				
		findModule = new FindTreeModule(this, SWT.NONE);
		final GridData gridData_ftm = new GridData(SWT.FILL, SWT.TOP, true, false);
		findModule.setLayoutData(gridData_ftm);
		
		tree = new Tree(this, treeStyle);
		GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		tree.setLayoutData(gridData_1);
		
		findModule.setTree(tree);
		
		final Menu menu = new Menu(tree);
	    tree.setMenu(menu);
	    menu.addMenuListener(new MenuListener()
	    {
			@Override
			public void menuHidden(MenuEvent e) {				
			}

			@Override
			public void menuShown(MenuEvent e) {
				// TODO Auto-generated method stub
	            MenuItem[] items = menu.getItems();
	            for (int i = 0; i < items.length; i++)
	            {
	                items[i].dispose();
	            }
	            MenuItem newItem = new MenuItem(menu, SWT.NONE);
	            newItem.setText("Expand all");
	            newItem.addSelectionListener(new SelectionListener()
	            {

					@Override
					public void widgetSelected(SelectionEvent e) {
				    	List<TreeItem> items = new ArrayList<TreeItem>();
				    	for(TreeItem leaf : tree.getItems()){
				    		items.add(leaf);
				    		getChildren(leaf, items);
				    	}
				    	
			    		for(TreeItem item : items){
			        		item.setExpanded(true);
			    		}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {	
					}
	            });
	            
	            
	            MenuItem newItem1 = new MenuItem(menu, SWT.NONE);
	            newItem1.setText("Collapse all");
	            newItem1.addSelectionListener(new SelectionListener()
	            {

					@Override
					public void widgetSelected(SelectionEvent e) {
				    	List<TreeItem> items = new ArrayList<TreeItem>();
				    	for(TreeItem leaf : tree.getItems()){
				    		items.add(leaf);
				    		getChildren(leaf, items);
				    	}
				    	
			    		for(TreeItem item : items){
			        		item.setExpanded(false);
			    		}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {						
					}
	            });
			}
	    });
    }
	
    private void getChildren(TreeItem item, List<TreeItem> items){
    	for(TreeItem leaf : item.getItems()){
    		if(!leaf.getText().isEmpty()){
    			items.add(leaf);
    			getChildren(leaf, items);
    		}
    	}
    }
    
    static int checkStyle(int style) {
        int mask = SWT.BORDER | SWT.READ_ONLY | SWT.FLAT | SWT.LEFT_TO_RIGHT | SWT.RIGHT_TO_LEFT;
        return style & mask;
    }
}

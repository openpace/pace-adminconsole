/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.PafException;

/**
 * Thrown when invalid dimensions occur.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class InvalidDimensionsException extends PafException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6363450449694182819L;
	
	private List<String> invalidDimensionList = new ArrayList<String>();
	
	/**
	 * Creates the exception with a list of invalid dimensions
	 * @param dimNameList invalid dimension list
	 */
	public InvalidDimensionsException(List<String> dimNameList) {
		
		if ( dimNameList != null)  {
			
			invalidDimensionList.addAll(dimNameList);
			
		}
		
	}

	/**
	 * @return the invalidDimensionList
	 */
	public List<String> getInvalidDimensionList() {
		return new ArrayList<String>(invalidDimensionList);
	}
	
}

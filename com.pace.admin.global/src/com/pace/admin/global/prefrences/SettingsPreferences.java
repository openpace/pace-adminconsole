/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.prefrences;

import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.swt.graphics.Rectangle;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class SettingsPreferences {
private Preferences preferences;
	
	public SettingsPreferences(String node){
		
		preferences = ConfigurationScope.INSTANCE.getNode(node);
		try {
			preferences.sync();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public String getFileLocation(){
		return preferences.absolutePath();
	}
	
	public String getStringSetting(String key){
		String def = "";
		return preferences.get(key, def);
	}
	
	public boolean getBooleanSetting(String key){
		boolean def = false;
		return preferences.getBoolean(key, def);
	}
	
	public String getStringSetting(org.eclipse.swt.widgets.Control  textBox){
		String s =  getStringSetting(textBox.getData().toString());
		if(s == null){
			return "";
		}
		return s;
	}
	
	public boolean getBooleanSetting(org.eclipse.swt.widgets.Control  textBox){
		return  getBooleanSetting(textBox.getData().toString());
	}
	
	public void saveBooleanSetting(String key, boolean value){
		preferences.putBoolean(key, value);
		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStringSetting(String key, String value){
		
		preferences.put(key, value);
		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		
	}
	
	public void saveStringSetting(org.eclipse.swt.widgets.Text text){
		saveStringSetting(text, text.getText());
	}
	
	
	public void saveStringSetting(org.eclipse.swt.widgets.Control control, String value){
		saveStringSetting(control.getData().toString(), value);
	};
	
	public void saveBooleanSetting(org.eclipse.swt.widgets.Control control, boolean value){
		saveBooleanSetting(control.getData().toString(), value);
	}
	
	public void saveSetting(String key, Rectangle rectangle){
		saveStringSetting(key, String.format("%s,%s,%s,%s", rectangle.x, rectangle.y, rectangle.width, rectangle.height));
	}
	
	public Rectangle getSetting(String key){
		String value = getStringSetting(key);
		String[] values = value.split(",");
		
		return new Rectangle(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]), Integer.parseInt(values[3]));
	}
}

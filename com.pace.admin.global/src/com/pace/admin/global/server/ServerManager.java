/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.server;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.ui.console.MessageConsole;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.jms.client.HornetQConnectionFactory;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.jobs.RestartServerJob;
import com.pace.admin.global.jobs.ServerRefreshJob;
import com.pace.admin.global.jobs.StartServerJob;
import com.pace.admin.global.jobs.StopServerJob;
import com.pace.admin.global.util.ConsoleUtil;
import com.pace.base.ui.PafServer;

/**
 * Manages all aspects of paf servers. start, stop,  etc.
 * 
 * @author JMilliron
 *
 */
public class ServerManager implements Observer {
	
	private static final Logger logger = Logger.getLogger(ServerManager.class);

	private Map<PafServer, MessageConsumer> serverMessageConsumerMap = new ConcurrentHashMap<PafServer, MessageConsumer>();
	
	//	private Map<PafServer, ServerStatus> serverStatusMap = Collections.synchronizedMap(new HashMap<PafServer, ServerStatus>());
	private Map<PafServer, ServerStatus> serverStatusMap = new ConcurrentHashMap<PafServer, ServerStatus>();
	
	private Map<MessageConsumer, Connection> connectionMap = new ConcurrentHashMap<MessageConsumer, Connection>();
		
	private static ServerManager instance;
	
	private ServerManager() {
	}
	
	/**
	 * Singleton.  This allows only one instance to be used throughout the application.
	 * 
	 * @return only instance of the ServerManager
	 */
	public static ServerManager getInstance() {
		
		if ( instance == null ) {
			instance = new ServerManager();
		}
		
		return instance;
	}
	
	/**
	 * Starts server.
	 * 
	 * @param pafServer server to start
	 */
	public void startServer(PafServer pafServer, String LinuxLogin, String LinuxLoginPasswrod ) {
		
		if ( pafServer != null && ! ServerMonitor.getInstance().isServerRunning(pafServer) ) {
			
			StartServerJob startServerJob = new StartServerJob(pafServer, LinuxLogin, LinuxLoginPasswrod);
			
			startServerJob.setPriority(Job.LONG);

			// add a finished job listener
			startServerJob.addJobChangeListener(new JobChangeAdapter() {

				// called when job is finished
				public void done(IJobChangeEvent event) {

					logger.info("Start Server Job has finished");
					
					//refresh servers
					(new ServerRefreshJob()).schedule(); 
					
				}
				
				
			});
			
			
			startServerJob.schedule();
			
		}
		
	}
	
	/**
	 * Stops server.
	 * 
	 * @param pafServer server to stop
	 */
	public void stopServer(PafServer pafServer, String LinuxLogin, String LinuxLoginPasswrod ) {
		
		if ( pafServer != null && ServerMonitor.getInstance().isServerRunning(pafServer)  ) {
			
			
			StopServerJob stopServerJob = new StopServerJob(pafServer, LinuxLogin, LinuxLoginPasswrod);

			stopServerJob.setPriority(Job.LONG);
			// add a finished job listener
			stopServerJob.addJobChangeListener(new JobChangeAdapter() {

				// called when job is finished
				public void done(IJobChangeEvent event) {

					logger.info("Stop Server Job has finished");
			
					//refresh servers
					(new ServerRefreshJob()).schedule(); 
					
				}
			});
			
			stopServerJob.schedule();
			
		}
		
	}
	
	public void restartServer(PafServer pafServer, String LinuxLogin, String LinuxLoginPasswrod ) {
	
		if ( pafServer != null && ServerMonitor.getInstance().isServerRunning(pafServer)  ) {
			
			RestartServerJob restartServerJob = new RestartServerJob(pafServer, LinuxLogin, LinuxLoginPasswrod);
			
			restartServerJob.setPriority(Job.LONG);
			
			restartServerJob.schedule();
			
		}
				
	}
	
	/**
	 * This class implements Observer so whenever the Observerable class notifies observers
	 * of changes, this method will be called.  If object is a ServerEvent, then the event 
	 * is processed.
	 */
	@Override
	public void update(Observable o, Object object) {
		
		logger.debug("start update(" + o + ", " + object + ")");
		
		if ( object != null && object instanceof ServerEvent) {
			
			ServerEvent se = (ServerEvent) object;
			
			final PafServer ps = se.getPafServer();
			
			ServerStatus ss = se.getServerStatus();
			
			if( ss != null ) {
				boolean hasServerStatusChanged = false;
				
				//if server status map doesn't have status for server
				//or
				//server status map has key but value doens't match
				if ( ! serverStatusMap.containsKey(ps) 
						|| ( serverStatusMap.containsKey(ps) 
								&& serverStatusMap.get(ps) != null 
								&& ! serverStatusMap.get(ps).equals(ss) ) ) {
					
					//update value
					serverStatusMap.put(ps, ss);
					
					hasServerStatusChanged = true;
					
				}
				
				//if running, open the console window
				if ( ss.equals(ServerStatus.Running) ) {
					
					final MessageConsole messageConsole = ConsoleUtil.findConsole(ps.getName());

					// add to message consumer map if not already present
					if ( hasServerStatusChanged && ! serverMessageConsumerMap.containsKey(ps)) {
						
						MessageConsumer messageConsumer = getMessageConsumer(ps, messageConsole);
						
						if ( messageConsumer != null ) {
													
							serverMessageConsumerMap.put(ps, messageConsumer);
						}
						else {
							
							String outMessage = "JMS connection failed, logging for server: '" + ps.getName() + "' will be disabled";
							logger.error(outMessage);
							ConsoleWriter.writeMessage(outMessage);
							
						}						
						
					}
					
				//if not running and currently in message consumer map, remove map entry
				} 
				else { 
					
					if ( hasServerStatusChanged && serverMessageConsumerMap.containsKey(ps) ) {
						
						MessageConsumer mc = serverMessageConsumerMap.remove(ps);
						
						if ( connectionMap.containsKey(mc) ) {
							
							Connection connection = connectionMap.remove(mc);
							
							try {
								connection.close();
							} catch (JMSException e) {
								//do nothing
							}
							
						}
											
					}
				}
			}
		}
		
		logger.debug("end update()");

	}


	/**
	 * Creates a Message Consumer to listen to Servers jms messaging.
	 * 
	 * @param ps paf server
	 * @param messageConsole console to write log messages to
	 * @return Message consumer that listens for server log messages
	 */
	private MessageConsumer getMessageConsumer(PafServer ps, MessageConsole messageConsole) {

		MessageConsumer messageConsumer = null;

		Connection connection = null;

		try {
			logger.debug( "Start creating JMS connection for server: '" + ps.getName() + "' at " + Calendar.getInstance().getTime());
//			System.out.println( "Start creating JMS connection for server: '" + ps.getName() + "' at " + Calendar.getInstance().getTime());
			Topic topic = HornetQJMSClient.createTopic(Constants.SERVER_LOG_TOPIC);

			Map<String, Object> connectorParams = new HashMap<String, Object>();
			
			Integer jmsMessagingPort = ps.getJmsMessagingPort();
			
			//if null, use default messaging port number
			if ( jmsMessagingPort == null ) {
				
				jmsMessagingPort = Constants.DEFAULT_SERVER_JMS_MESSAGING_PORT;
				
			}
				
			connectorParams
					.put(org.hornetq.core.remoting.impl.netty.TransportConstants.PORT_PROP_NAME,
							jmsMessagingPort.toString());
			connectorParams
					.put(org.hornetq.core.remoting.impl.netty.TransportConstants.HOST_PROP_NAME,
							ps.getHost());

			TransportConfiguration transportConfiguration = new TransportConfiguration(
					NettyConnectorFactory.class.getName(), connectorParams);
			
			HornetQConnectionFactory cf = (HornetQConnectionFactory) HornetQJMSClient.createConnectionFactoryWithoutHA(
					JMSFactoryType.CF,	transportConfiguration);

			connection = cf.createConnection();
			
			Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);

			messageConsumer = session.createConsumer(topic);

			connection.start();
			
			messageConsumer.setMessageListener(new JMSServerMessageListener(messageConsole) );
			
			connectionMap.put(messageConsumer, connection);
			
		} catch (JMSException je) {
			
			logger.error(je.getMessage());
			
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
					//do nothing
				}
			}
			
		}

		logger.debug( "End creating JMS connection for server: '" + ps.getName() + "' at " + Calendar.getInstance().getTime());
//		System.out.println( "End creating JMS connection for server: '" + ps.getName() + "' at " + Calendar.getInstance().getTime());
		return messageConsumer;
	}

	/**
	 * Closes all JMS listeners.
	 */
	public void closeJMSListeners() {

		if ( connectionMap != null && connectionMap.size() > 0 	) {
			
			for ( Connection conn : connectionMap.values() ) {
				
				if ( conn != null ) {
					
					try {
						conn.close();
					} catch (JMSException e) {
						logger.error(e.getMessage());
					}
					
				}
				
			}
			
		}
	
	}

}

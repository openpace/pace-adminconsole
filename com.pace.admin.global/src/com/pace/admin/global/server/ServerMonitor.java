/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.server;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.ui.PafServer;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.ApplicationStateResponse;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.RunningState;

/**
 * Server Monitor to monitor the status of a server. If a server goes from not started to running, a server
 * status event will be sent out.  Or, if a server goes from running to not started, then a server status event
 * will be sent out.  Also can send out application states.
 * 
 * @author JMilliron
 *
 */
public class ServerMonitor extends Observable {

	private static final Logger logger = Logger.getLogger(ServerMonitor.class);
	private static ServerMonitor instance;
	
	private Map<String, PafServer> serverMap = new ConcurrentHashMap<String, PafServer>();
	private Map<PafServer, Boolean> serverRunningMap = new ConcurrentHashMap<PafServer, Boolean>();
	private Map<PafServer, List<ApplicationState>> appStatesMap = new ConcurrentHashMap<PafServer, List<ApplicationState>>();
	
	private ServerMonitor() {		
	}
	
	/**
	 * Singleton
	 * 
	 * @return only instance of Server Monitor
	 */
	public static ServerMonitor getInstance() {
		
		if ( instance == null ) {
			instance = new ServerMonitor();
		}
		
		return instance;
		
	}
	
	/**
	 * Adds Server to monitor
	 * 
	 * @param server server to monitor
	 */
	public void addServer(PafServer server) {
		if ( server != null && server.getName() != null ) {
			serverMap.put(server.getName(), server);
			addServerApplicationMap(server);
		}
	}
	
	/**
	 * Adds Server to monitor
	 * 
	 * @param server server to monitor
	 */
	public void initServer(PafServer server) {
		if ( server != null && server.getName() != null ) {
			serverMap.put(server.getName(), server);
			initServerApplicationMap(server);
		}
	}
	
	private void initServerApplicationMap(PafServer pafServer) {
		if( pafServer != null ) {
			updateServerRunningState(pafServer, false);
		}
	}
	
	/**
	 * Removes server from monitoring
	 * 
	 * @param server server to remove
	 */
	public boolean isServerRunning(PafServer server) {
		if( server != null ) {
			if(serverRunningMap.containsKey(server)){
				return serverRunningMap.get(server);
			}
		}
		return false;
			
	}

	/**
	 * Removes server from monitoring
	 * 
	 * @param server server to remove
	 */
	public boolean isServerRunning(String url) {
		try {
			return isServerRunning( PafServerUtil.getServerFromUrl(url) );
		} catch (PafServerNotFound e) {
			e.printStackTrace();
		}
		return false;
	}

	private void updateServerRunningState(PafServer pafServer, Boolean status) {
		if( pafServer != null && status != null )
			serverRunningMap.put(pafServer, status);
	}
	
	/**
	 * Removes server from monitoring
	 * 
	 * @param server server to remove
	 */
	public void removeServer(PafServer server) {
		if ( server != null && server.getName() != null && serverMap.containsKey(server.getName()) ) {
			serverMap.remove(server.getName());
			serverRunningMap.remove(server);
			removeApplicationState(server);
		}
	}

	private void addServerApplicationMap(PafServer pafServer) {
		if( pafServer != null ) {
			
			if( !pafServer.isDisabled() && pafServer.isTheServerRunning() ) {
				//String url = pafServer.getCompleteWSDLService();
				updateServerRunningState(pafServer, true);
				WebServicesUtil.getPafServerAck(pafServer);
				addApplicationState(pafServer);
			}
			else {
				updateServerRunningState(pafServer, false);
			}
		}
	}
	
	public 	ApplicationState getApplicationState(PafServer pafServer) {
		ApplicationState appState = null;
		if( ServerMonitor.getInstance().isServerRunning(pafServer) ) {
			if (appStatesMap.containsKey(pafServer)) {
				appState = appStatesMap.get(pafServer).get(0);
			} 
//			else {
//				appState = addApplicationState(pafServer);
//			}
		}
		return appState;
	}

	public ApplicationState addApplicationState(PafServer pafServer) {
		ApplicationState appState = null;
		if( pafServer != null ) {
			logger.debug("Start calling getApplicationState for server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime() );
//			System.out.println("Start calling getApplicationState for server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime() );
			String url = pafServer.getCompleteWSDLService();
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ) {
				try {
					//get application state
					ApplicationStateResponse appStateResponse = service.getApplicationState(null);
					
					if( appStateResponse != null ) {
//						WebServicesUtil.loadPafServerAckMap(pafServer.getCompleteWSDLService());
						List<ApplicationState> listAppStates = appStateResponse.getAppStates();
						if( listAppStates != null && listAppStates.size() > 0 ) {
							
							//add applicationStateMap
							updateApplicationState(pafServer, listAppStates);
							
							appState = listAppStates.get(0);
							
							if ( appState.getCurrentRunState() != null ) {
								
								RunningState runningState = appState.getCurrentRunState();
																								
								final ApplicationEvent ae = new ApplicationEvent(pafServer, appState.getApplicationId(), com.pace.base.RunningState.valueOf(runningState.toString().toUpperCase()));
								
								//notify observers of application event to refresh application states
								Display.getDefault().asyncExec(new Runnable() {
									
									@Override
									public void run() {
												
										//enable observers to be notified
										setChanged();
										//notify observers of server event
										notifyObservers(ae);
										
									}
								});			
								
							}
							
						}
					}
					else { //appStateResponse == null
						
						logger.warn("ApplicationStateResponse return from getApplicationState Service Call is null.");
						updateServerRunningState(pafServer, false);
						WebServicesUtil.removePafServerAck(url);
						WebServicesUtil.removePafService(url);
						removeApplicationState(pafServer);
						
					}
					
				} catch (PafSoapException_Exception e) {
					
					logger.error("PafSoapException: getApplicationState Service Call failed.");
					updateServerRunningState(pafServer, false);
					WebServicesUtil.removePafServerAck(url);
					WebServicesUtil.removePafService(url);
					removeApplicationState(pafServer);
					
				} catch(Exception e) {
					
					logger.error("getApplicationState Service Call failed.");
					updateServerRunningState(pafServer, false);
					WebServicesUtil.removePafServerAck(url);
					WebServicesUtil.removePafService(url);
					removeApplicationState(pafServer);
					
				}
			}
			logger.debug("End calling getApplicationState for server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime() );
//			System.out.println("End calling getApplicationState for server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime() );
		}
		return appState;
	}
	
	private void updateApplicationState(PafServer pafServer, List<ApplicationState> appStates) {
		if( pafServer != null && appStates != null )
			appStatesMap.put(pafServer, appStates);
	}

	public void removeApplicationState(PafServer server) {
		if ( server != null && server.getName() != null  ) {
			appStatesMap.remove(server);
		}
	}
	
//	//no longer used by ServerRefreshJob
//	public void refreshAllServerStatus() {	
//		Iterator<PafServer> it = serverMap.values().iterator();
//		while(it.hasNext()){
//			final PafServer pafServer = it.next();
//		
//			//check if server changed from NotRunning to Running
//			if ( ! serverRunningMap.get(pafServer) ) {
//				checkServerConnection(pafServer);
//			}
//			
//			ServerStatus ss = null;
//			if ( serverRunningMap.get(pafServer) ) {
//				ss = ServerStatus.Running;
//			} else {
//				ss = ServerStatus.NotRunning;
//			}
//			//create server event
//			final ServerEvent se = new ServerEvent(pafServer, ss);
//
//			//notify observers of server event to refresh servers
//			Display.getDefault().asyncExec(new Runnable() {
//				
//				@Override
//				public void run() {
//							
//					//enable observers to be notified
//					setChanged();
//					//notify observers of server event
//					notifyObservers(se);
//					
//				}
//			});			
//		}
//	}

	//run by ServerRefreshJob
	public void refreshAllServerStatus(PafServer pafServer) {	
			
		ServerStatus ss = null;
		if(pafServer.isDisabled()){
			ss = ServerStatus.Disabled;
		} else if ( serverRunningMap.get(pafServer) ) {
			ss = ServerStatus.Running;
		} else {
			ss = ServerStatus.NotRunning;
		}
		//create server event
		final ServerEvent se = new ServerEvent(pafServer, ss);

		//notify observers of server event to refresh servers
		Display.getDefault().asyncExec(new Runnable() {
			
			@Override
			public void run() {
						
				//enable observers to be notified
				setChanged();
				//notify observers of server event
				notifyObservers(se);
				
			}
		});	
		
	}

	//run by ServerRefreshJob
	/**
	 * Creates app states that get send to observers
	 */
	public void refreshAllServerAndApplicationStates() {
		Iterator<PafServer> it = serverMap.values().iterator();
		while(it.hasNext()){
			PafServer pafServer = it.next();
			if(pafServer.isDisabled()) continue;
			if( serverRunningMap.get(pafServer) ) {
				
				addApplicationState(pafServer);
				
			}
			else { //server not running before
				
				checkServerConnection(pafServer);
				refreshAllServerStatus(pafServer);
				
			}
		}
	}
	
	//Check if server changed status from NotRunning to Running
	private void checkServerConnection(PafServer pafServer) {
		
		logger.debug("Start calling http connection for server " + pafServer.getName() + "." );
		
		if( pafServer.isTheServerRunning() ) {
			updateServerRunningState(pafServer, true);
			WebServicesUtil.getPafServerAck(pafServer.getCompleteWSDLService());
			addApplicationState(pafServer);
		
//			//create server event
//			ServerStatus ss = null;
//			if ( serverRunningMap.get(pafServer) ) {
//				ss = ServerStatus.Running;
//			} else {
//				ss = ServerStatus.NotRunning;
//			}
//			final ServerEvent se = new ServerEvent(pafServer, ss);
//	
//			//notify observers of server event to refresh servers
//			Display.getDefault().asyncExec(new Runnable() {
//				
//				@Override
//				public void run() {
//							
//					//enable observers to be notified
//					setChanged();
//					//notify observers of server event
//					notifyObservers(se);
//					
//				}
//			});			
		}

	}

	@Override
	public String toString() {
		return "ServerMonitor [serverMap=" + serverMap + "]";
	}
//	
//	/**
//	 * Creates ServerEvents for each server monitoring and notifies all Observers
//	 */
//	public void refresh() {	
//		Iterator<PafServer> it = serverMap.values().iterator();
//		while(it.hasNext()){
//			final PafServer pafServer = it.next();
//		
//			ServerStatus ss = null;
//			
//			if ( pafServer.isTheServerRunning() ) {
//				ss = ServerStatus.Running;
//			} else {
//				ss = ServerStatus.NotRunning;
//			}
//			updateServerStatus(pafServer, ss);
//			//create server event
//			final ServerEvent se = new ServerEvent(pafServer, ss);
//
//			//notify observers of server event to refresh servers
//			Display.getDefault().asyncExec(new Runnable() {
//				
//				@Override
//				public void run() {
//							
//					//enable observers to be notified
//					setChanged();
//					//notify observers of server event
//					notifyObservers(se);
//					
//				}
//			});			
//		}
//	}
//	
//	/**
//	 * Creates app states that get send to observers
//	 */
//	public void refreshAppStates() {
//		
//		Iterator<PafServer> it = serverMap.values().iterator();
//		while(it.hasNext()){
//			PafServer pafServer = it.next();
//			String url = pafServer.getCompleteWSDLService();
//			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
//			
//			if ( pafServerAck == null ) {
//				
//				WebServicesUtil.loadPafServerAckMap(url);
//				pafServerAck = WebServicesUtil.getPafServerAck(url);
////				pafServerAck = WebServicesUtil.getPafServerAck(url, true);
//				
//			}
//			
//			if( pafServerAck != null ) {
//				PafService service = WebServicesUtil.getPafService(url);
//				
//				if( service != null ) {
//					
//					ApplicationStateRequest appStateRequest = new ApplicationStateRequest();
//					
//					appStateRequest.setClientId(pafServerAck.getClientId());
//					try {
//						
//						//get application state
//						ApplicationStateResponse appStateResponse = service.getApplicationState(appStateRequest);
//						
//						if( appStateResponse != null ) {
//							
//							List<ApplicationState> listAppStates = appStateResponse.getAppStates();
//							
//							if( listAppStates != null && listAppStates.size() > 0 ) {
//								
//								ApplicationState appState = listAppStates.get(0);
//								
//								if ( appState.getCurrentRunState() != null ) {
//									
//									RunningState runningState = appState.getCurrentRunState();
//																									
//									final ApplicationEvent ae = new ApplicationEvent(pafServer, appState.getApplicationId(), com.pace.base.RunningState.valueOf(runningState.toString().toUpperCase()));
//									
//									logger.debug("Created " + ae);
//									
//									//System.out.println("Created " + ae);
//									Display.getDefault().asyncExec(new Runnable() {
//										
//										@Override
//										public void run() {
//													
//											//enable observers to be notified
//											setChanged();
//											//notify observers of server event
//											notifyObservers(ae);
//											
//										}
//									});			
//									
//								}
//								
//							}
//						}
//						
//					} catch (PafSoapException_Exception e) {
//						
//						logger.error(e.getMessage());
//						
//					} catch(Exception e) {
//						logger.error(e.getMessage());
//					}
//				}
//			}
//		}
//	}
//
//	public void refreshAllServerStatusAndUpdateAcks() {
//		Iterator<PafServer> it = serverMap.values().iterator();
//		while(it.hasNext()){
//			PafServer pafServer = it.next();
//			refreshServerStatusAndUpdateAck( pafServer ); 
//		}
//	}
	
//	public void updateServerStatus(PafServer pafServer, ServerStatus status) {
//		if( pafServer != null && status != null )
//			serverStatusMap.put(pafServer, status);
//	}
	
//	public void refreshServerAckAndServiceAck(PafServer pafServer) {
//		if( pafServer != null ) {
//			String url = pafServer.getCompleteWSDLService();
//			if( serverRunningMap.get(pafServer)) {
//				WebServicesUtil.loadPafServerAckMap(url);
//				updateApplicationState(pafServer);
//			}
//			else {
//				WebServicesUtil.resetPafServerAckMap(url);
//				removeApplicationState(pafServer);
//			}
//		}
//	}
	
//	public void getServerAndApplicationStates() {
//		Iterator<PafServer> it = serverMap.values().iterator();
//		while(it.hasNext()){
//			PafServer pafServer = it.next();
//			String url = pafServer.getCompleteWSDLService();
//			if( url != null ) {
//				PafService service = WebServicesUtil.getPafService(url);
//				if( service != null ) {
//					try {
//						//get application state
//						ApplicationStateResponse appStateResponse = service.getApplicationState(null);
//						
//						if( appStateResponse != null ) {
//							updateServerStatus(pafServer, ServerStatus.Running );
//							updateApplicationState(pafServer, appStateResponse.getAppStates());
//							continue;
//						}
//						
//					} catch (PafSoapException_Exception e) {
//						
//						logger.error(e.getMessage());
//						
//					} catch(Exception e) {
//						logger.error(e.getMessage());
//					}
//				}
//			}
//			updateServerStatus(pafServer, ServerStatus.NotRunning );
//		}
//	}
//	
	
}

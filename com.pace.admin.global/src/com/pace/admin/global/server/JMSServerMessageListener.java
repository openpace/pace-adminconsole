/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.server;

import java.io.IOException;
import java.util.Calendar;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.hornetq.jms.client.HornetQObjectMessage;

import com.pace.admin.global.constants.Constants;

/**
 * JMS Server Message Listener.  Used to monitor jms log messages on server.
 * 
 * @author JMilliron
 *
 */
public class JMSServerMessageListener implements MessageListener {
		
	private static final Logger logger = Logger.getLogger(JMSServerMessageListener.class);
	
//	private PafServer pafServer = null;
	
	private MessageConsoleStream msgStream = null;
				
	/**
	 * Creates listener
	 * 
	 * @param pafServer server to monitor for log messages
	 * @param messageConsole message console to write log messages to
	 */
	public JMSServerMessageListener(MessageConsole messageConsole) {
		super();
//		this.pafServer = pafServer;
		if ( messageConsole != null ) {
			msgStream = messageConsole.newMessageStream();
		}
	}

	@Override
	public void onMessage(Message message) {
		
		if ( message instanceof HornetQObjectMessage) {
		
			HornetQObjectMessage hQMessage = (HornetQObjectMessage) message;
			try {
				
				//if log4j log event, cast and then write to console
				if ( hQMessage.getObject() instanceof org.apache.log4j.spi.LoggingEvent ) {
	            	
	            	org.apache.log4j.spi.LoggingEvent logEvent = (org.apache.log4j.spi.LoggingEvent) hQMessage.getObject();
	            	
	            	try {
						msgStream.write(Constants.CONSOLE_SIMPLE_DATE_FORMAT.format(Calendar.getInstance().getTime()) + " - " + logEvent.getLevel()+ " - " + logEvent.getRenderedMessage() + "\n");
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
	            	
	            }
				
			
			} catch (JMSException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.server;

import com.pace.base.ui.PafServer;

/**
 * Wraps a PafServer and server status.
 * 
 * @author JMilliron
 *
 */
public class ServerEvent {

	private final PafServer pafServer;
	
	private final ServerStatus serverStatus;

	/**
	 * Creates a server event.
	 * 
	 * @param pafServer server tied to status
	 * @param serverStatus status of server ( i.e. running, not running )
	 */
	public ServerEvent(PafServer pafServer, ServerStatus serverStatus) {
		super();
		this.pafServer = pafServer;
		this.serverStatus = serverStatus;
	}

	public PafServer getPafServer() {
		return pafServer;
	}

	public ServerStatus getServerStatus() {
		return serverStatus;
	}

	@Override
	public String toString() {
		return "ServerEvent [pafServer=" + pafServer + ", serverStatus="
				+ serverStatus + "]";
	}	
	
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.server;

import com.pace.base.RunningState;
import com.pace.base.ui.PafServer;

/**
 * Application event with server, app id and app state. 
 * 
 * @author JMilliron
 *
 */
public class ApplicationEvent {

	private final PafServer pafServer;
	
	private final String appId;
	
	private final RunningState runningState;

	/**
	 * Application Event
	 * 
	 * @param pafServer server
	 * @param appId application id
	 * @param runningState app state
	 */
	public ApplicationEvent(PafServer pafServer, String appId, RunningState runningState) {
		super();
		this.pafServer = pafServer;
		this.appId = appId;
		this.runningState = runningState;
	}

	/**
	 * @return the pafServer
	 */
	public PafServer getPafServer() {
		return pafServer;
	}

	/**
	 * @return the runningState
	 */
	public RunningState getRunningState() {
		return runningState;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appId == null) ? 0 : appId.hashCode());
		result = prime * result
				+ ((pafServer == null) ? 0 : pafServer.hashCode());
		result = prime * result
				+ ((runningState == null) ? 0 : runningState.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationEvent other = (ApplicationEvent) obj;
		if (appId == null) {
			if (other.appId != null)
				return false;
		} else if (!appId.equals(other.appId))
			return false;
		if (pafServer == null) {
			if (other.pafServer != null)
				return false;
		} else if (!pafServer.equals(other.pafServer))
			return false;
		if (runningState != other.runningState)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ApplicationEvent [pafServer=" + pafServer + ", appId=" + appId
				+ ", runningState=" + runningState + "]";
	}
		
}

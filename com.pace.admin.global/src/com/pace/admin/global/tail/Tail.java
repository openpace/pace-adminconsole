/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.tail;

// Import the Java classes
import java.util.*;
import java.io.*;

import org.eclipse.ui.console.MessageConsoleStream;

/**
 * Implements console-based log file tailing, or more specifically, tail following:
 * it is somewhat equivalent to the unix command "tail -f"
 */
public class Tail implements LogFileTailerListener
{
  /**
   * The log file tailer
   */
  private LogFileTailer tailer;
  
  private static Set<String> filesWritingToConsole = new HashSet<String>();
  
  private MessageConsoleStream messageConsoleStream = null;

  /**
   * Creates a new Tail instance to follow the specified file
   */
  public Tail( String filename, MessageConsoleStream messageConsoleStream )
  {
	  
	if ( ! filesWritingToConsole.contains(filename)) {  
	  
	File fileToTail = new File(filename); 
	
	this.messageConsoleStream = messageConsoleStream;
	
	if ( fileToTail.exists() && fileToTail.isFile() && fileToTail.canRead()) {
				
		tailer = new LogFileTailer( fileToTail, 1000, false );
		tailer.addLogFileTailerListener( this );
		tailer.start();
		filesWritingToConsole.add(filename);
		
	}
	}
	
  }

  /**
   * A new line has been added to the tailed log file
   * 
   * @param line   The new line that has been added to the tailed log file
   */
  public void newLogFileLine(String line)
  {
    if ( messageConsoleStream != null ) {
    	messageConsoleStream.println(line);
    }
  }

}
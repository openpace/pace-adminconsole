/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

/**
 * @author JMilliron
 *
 */
public class UploadProjectJob extends Job {

	private static final Logger logger = Logger.getLogger(UploadProjectJob.class);
	
	protected IProject project;
	
	protected PafServer pafServer;
	
	protected boolean refreshConfiguration;
	
	protected boolean refreshCube;
	
	protected boolean succesfullyUploaded;

	protected Set<ProjectElementId> filterSet;
	
	public UploadProjectJob(IProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube) {
		this("Upload Project Job", project, pafServer, refreshConfiguration, refreshCube, null);
	}
	
	public UploadProjectJob(IProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube, Set<ProjectElementId> filterSet) {
		this("Upload Project Job", project, pafServer, refreshConfiguration, refreshCube, filterSet);
	}
	
	public UploadProjectJob(String name, IProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube, Set<ProjectElementId> filterSet) {
		super(name);
		this.project = project;
		this.pafServer = pafServer;
		this.refreshConfiguration = refreshConfiguration;
		this.refreshCube = refreshCube;
		this.filterSet = filterSet;
	}
	
	public String getProjectName() {
		
		if ( project != null ) {
			return project.getName();
		}
		
		return null;
	}
	
	public String getServerName() {
		
		if ( pafServer != null ) {
			return pafServer.getName();
		}
		
		return null;
	}
	
	/**
	 * @return the filterSet
	 */
	public Set<ProjectElementId> getFilterSet() {
		return filterSet;
	}

	/**
	 * @param filterSet the filterSet to set
	 */
	public void setFilterSet(Set<ProjectElementId> filterSet) {
		this.filterSet = filterSet;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		logger.info("Running Job: " + this.getName());
		try {
		
		succesfullyUploaded = WebServicesUtil.uploadProjectToServer(project, pafServer, refreshConfiguration, refreshCube, filterSet);
		} catch(Exception e) {
			logger.info("Service Error: " + e.getMessage());
		}
		
		logger.info(this.getName() + " successfully executed: " + succesfullyUploaded);

		return Status.OK_STATUS;
	}

	public boolean isSuccesfullyUploaded() {
		return succesfullyUploaded;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.inputs.UploadProjectInput;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;

/**
 * Upload Project Job Change Adapter.  Called whenever the upload project job finishes.
 * 
 * @author JMilliron
 *
 */
public class UploadProjectJobChangeAdapter extends JobChangeAdapter {
	
	private static final Logger logger = Logger.getLogger(UploadProjectJobChangeAdapter.class);
	
	private UploadProjectInput input = null;

	public UploadProjectJobChangeAdapter(UploadProjectInput input) {
		super();
		this.input = input;
	}

	@Override
	public void done(IJobChangeEvent event) {
			
			super.done(event);
			
			if ( event.getJob() != null && event.getJob() instanceof UploadProjectJob ) {
			
				final UploadProjectJob job = (UploadProjectJob) event.getJob();
							
				if ( event.getResult().equals(Status.OK_STATUS)) {
				
					final boolean isJobSuccessful = job.isSuccesfullyUploaded();
					
					Display.getDefault().asyncExec(new Runnable() {
	
						@Override
						public void run() {
																					
							String outMessage = null;
																					
							if ( isJobSuccessful ) {
							
								// Remove existing PafServerAck map entry so that server node
								// info gets refreshed (TTN-2659).
								String url = job.pafServer.getCompleteWSDLService();
								WebServicesUtil.removePafServerAck(url);
								
								ServerMonitor.getInstance().refreshAllServerAndApplicationStates();
								
								if ( input != null && input.getSuccessfulMessage() != null ) {
									
									outMessage = input.getSuccessfulMessage();
									
								} else {
								
									outMessage = "Project '" + job.getProjectName() + "' was successfully uploaded to server '" + job.getServerName() + "'.";
									
								}
																
								logger.info(outMessage);
								
								if ( input == null || ! input.isSlientMode() ) {
									GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
								}
								
							} else {
							
								if ( input != null && input.getUnsuccessfulMessage() != null ) {
									
									outMessage = input.getUnsuccessfulMessage();
									
								} else {
								
									outMessage = "Project '" + job.getProjectName() + "' was not successfully uploaded to server '" + job.getServerName() + "'.  There was a problem uploading one or more files.  Check the admin console and server logs for more information.";
									
								}
								
								logger.error(outMessage);
								
								GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, SWT.ICON_ERROR);
																
							}

							ConsoleWriter.writeMessage(outMessage);
														
						}
						
					});
						
					
					
				}				
				
			}
		
	}			
	
}

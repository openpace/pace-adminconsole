/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.jobs;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

public class UploadProjectToServerGroupJob extends UploadProjectJob {

	private static final Logger logger = Logger.getLogger(UploadProjectToServerGroupJob.class);
	
	private String groupName;
	
	private List<PafServer> pafServers;
	
	private Map<String, Boolean> results;
	
	public UploadProjectToServerGroupJob(IProject project, List<PafServer> pafServers, String groupName, boolean refreshConfiguration, boolean refreshCube) {
		this(project, pafServers, groupName, refreshConfiguration, refreshCube, null);
	}
	
	public UploadProjectToServerGroupJob(IProject project,  List<PafServer> pafServers, String groupName, boolean refreshConfiguration, boolean refreshCube, Set<ProjectElementId> filterSet) {
		super("Upload Project To Server Group Job", project, null, refreshConfiguration, refreshCube, filterSet);
		this.pafServers = pafServers;
		this.groupName = groupName;
	}


	public String getGroupName() {
		
		if ( groupName != null ) {
			return groupName;
		}
		
		return null;
	}
	
	public List<PafServer> getServers() {
		
		if ( pafServers != null ) {
			return pafServers;
		}
		
		return null;
	}

	
	public Map<String, Boolean> getResults() {
		return results;
	}


	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		logger.info("Running Job: " + this.getName());
		
		try {
		
			results = WebServicesUtil.uploadProjectToServerGroup(project, pafServers, refreshConfiguration, refreshCube, groupName, filterSet);
			
		} catch(Exception e) {
			logger.info("Service Error: " + e.getMessage());
		}
		
		succesfullyUploaded = ImportExportUtil.getResults(results.values());

		logger.info(this.getName() + " successfully executed: " + succesfullyUploaded);
		
		return Status.OK_STATUS;

	}
}

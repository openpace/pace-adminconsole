/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.jobs;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;

/**
 * Refreshes the Server Monitor to see if any server changes have occured.  Typically 
 * this would be if a server was stopped or started 
 * 
 * @author JMilliron
 *
 */
public class ServerRefreshJob extends Job {
	private static final Logger logger = Logger.getLogger(ServerRefreshJob.class);

	public ServerRefreshJob() {
		super(Constants.REFRESHING_SERVERS);
	}
	
	public ServerRefreshJob(String name) {
        super(name);
    }
 
    @Override
    protected IStatus run(IProgressMonitor monitor) {
		logger.debug( "Start refreshing server view at " + Calendar.getInstance().getTime());
//		System.out.println( "Start refreshing server view at " + Calendar.getInstance().getTime());
        
    	//alert server monitor to refresh
	   	ServerMonitor.getInstance().refreshAllServerAndApplicationStates();
//    	ServerMonitor.getInstance().refreshAllServerStatus();
	   	
		logger.debug( "End crefreshing server view at " + Calendar.getInstance().getTime());
//		System.out.println( "End refreshing server view at " + Calendar.getInstance().getTime());
        return Status.OK_STATUS;
    }

}

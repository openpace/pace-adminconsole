/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.inputs.UploadProjectToServerGroupInput;
import com.pace.admin.global.util.ImportExportUtil;

/**
 * Upload Project Job Change Adapter.  Called whenever the upload project job finishes.
 * 
 * @author JMilliron
 *
 */
public class UploadProjectToServerGroupJobChangeAdapter extends JobChangeAdapter {
	
	private UploadProjectToServerGroupInput input = null;

	public UploadProjectToServerGroupJobChangeAdapter(UploadProjectToServerGroupInput input) {
		super();
		this.input = input;
	}

	@Override
	public void done(IJobChangeEvent event) {
			
			super.done(event);
			
			if ( event.getJob() != null && event.getJob() instanceof UploadProjectToServerGroupJob ) {
			
				final UploadProjectToServerGroupJob job = (UploadProjectToServerGroupJob) event.getJob();
							
				if ( event.getResult().equals(Status.OK_STATUS)) {
				
					final boolean isJobSuccessful = job.isSuccesfullyUploaded();
					
					Display.getDefault().asyncExec(new Runnable() {
	
						@Override
						public void run() {
																					
							String outMessage = null;
							String messagePrefix = "Project '" + job.getProjectName() + "' was not successfully uploaded to one or more servers in the '" + job.getGroupName() + "' server group.\r\n\r\n";
							if ( input.getUnsuccessfulMessage() != null && !isJobSuccessful ) {
								messagePrefix = input.getUnsuccessfulMessage();
							}
							String messageEnding = "\r\nCheck the admin console and server logs for more information.";
							if(isJobSuccessful){
								messagePrefix = "Project '" + job.getProjectName() + "' was successfully uploaded to server group '" + job.getGroupName() + "'. \r\n\r\n";
								messageEnding = "";
							}
							
							outMessage = messagePrefix +
									ImportExportUtil.parseUploadResults(job.getResults()) + 
										messageEnding;
							
							if ( input.getSuccessfulMessage() != null && isJobSuccessful ) {
								
								outMessage = input.getSuccessfulMessage();
								
							}
						
							ImportExportUtil.showProcessCompleteDialog(isJobSuccessful, outMessage);
							
							
							ConsoleWriter.writeMessage(outMessage);
														
						}
						
					});
				}				
			}
	}			
	
}

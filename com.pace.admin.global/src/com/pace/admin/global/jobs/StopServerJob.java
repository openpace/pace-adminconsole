/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.base.server.ServerPlatform;
import com.pace.base.ui.PafServer;

/**
 * Stops a server using the service bat file
 * 
 * @author JMilliron
 *
 */
public class StopServerJob extends ServerJob {

	private static final Logger logger = Logger.getLogger(StopServerJob.class);
	
	public StopServerJob(PafServer pafServer, String linuxUsrName, String linuxPasswd) {
		super("Stopping Server", pafServer, linuxUsrName, linuxPasswd);
		setStopServer(true);
		String outMessage = "Stopping server '" + pafServer.getName() + "'";
		logger.info(outMessage);
		ConsoleWriter.writeMessage(outMessage);
	}

	@Override
	protected String getCommandToExecute() {
		
		String shutdownCommand = null;
		
		if ( pafServer != null && pafServer.getShutdownFile() != null && 
				! pafServer.getShutdownFile().trim().equals("")) {
			shutdownCommand = pafServer.getShutdownFile();
		} else {
			if (pafServer != null) {
				if( pafServer.getServerPlatform() == null || pafServer.getServerPlatform().equals(ServerPlatform.Windows)) {
					shutdownCommand = Constants.ADMIN_CONSOLE_CONF_DIRECTORY + Constants.SERVICE_STOP_BAT;
				}
				else if( pafServer.getServerPlatform().equals(ServerPlatform.Linux) ) {
					shutdownCommand = Constants.ADMIN_CONSOLE_CONF_DIRECTORY + Constants.SERVICE_STOP_LINUX_BAT;
				}
			}
		}
		
		logger.info("StopServerJob command: " + shutdownCommand);
		
		return shutdownCommand;
		
	}

}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.project.ProjectMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafApplicationUtil;

/**
 * Download Project Job Change Adapter.  Called whenever the download project job finishes.
 * 
 * @author JMilliron
 *
 */
public class DownloadProjectJobChangeAdapter extends JobChangeAdapter {
	
	private static final Logger logger = Logger.getLogger(DownloadProjectJobChangeAdapter.class);
	
	private boolean silentMode;
	
	public DownloadProjectJobChangeAdapter(boolean silentMode) {
		this.silentMode = silentMode;
	}

	@Override
	public void done(IJobChangeEvent event) {
			
			super.done(event);
			
			if ( event.getJob() != null && event.getJob() instanceof DownloadProjectJob ) {
			
				final DownloadProjectJob job = (DownloadProjectJob) event.getJob();
							
				if ( event.getResult().equals(Status.OK_STATUS)) {
				
					final boolean isJobSuccessful = job.isSuccesfullyDownloaded();
										
					Display.getDefault().asyncExec(new Runnable() {
	
						@Override
						public void run() {
							
							String outMessage = null;
							
							if ( isJobSuccessful ) {
														
								//tell observers to refresh their ui
								ProjectMonitor.getInstance().refreshProject(job.getProject());
								int result =0;
								
								if ( ! silentMode && job.getProject() != null && job.getInitialPafApplicationDef() != null ) {
								
									// ask user if they want to update application id
									// TTN-2429 Check the return value to see if the user clicked on Yes or cancel
									result = GUIUtil.promptUserAboutAppId(job.getInitialPafApplicationDef(), job.getProject());
								
								}
								
								// display this message only if the user clicked on Yes.
								if(result==0)
									try {
										logger.info("Server project: " + job.getProject().getDescription().getName() + ".");
										outMessage = "The Project '" + PafApplicationUtil.getPafApp(job.getProject()).getAppId() + "' on server '" + job.getServerName() + "', was successfully downloaded to local project '" + job.getProjectName() + "'." ;
									} catch (CoreException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								
								logger.info(outMessage);
								
								if ( ! silentMode ) {
									GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, SWT.ICON_INFORMATION);
								}
								
							} else {
							
								outMessage = "Project '" + job.getProjectName() + "' was not successfully downloaded from server '" + job.getServerName() + "'.  There was a problem downloading one or more files.  Check the admin console and server logs for more information.";
								logger.error(outMessage);
								GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, SWT.ICON_ERROR);
								
							}
							
						}
						
					});
						
				}				
				else {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							String outMessage = "Server Error. Downloading project was not completed";
							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, SWT.ICON_ERROR);
						}
					});
				}
				
			}
			
	}			
	
}

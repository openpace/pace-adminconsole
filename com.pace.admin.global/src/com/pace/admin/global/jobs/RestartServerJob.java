/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;

import com.pace.base.ui.PafServer;

/**
 * 
 * Stops and then starts a server.
 * 
 * @author JMilliron
 *
 */
public class RestartServerJob extends Job {
	
	private PafServer pafServer;
	private String linuxUsrName;
	private String linuxPasswd;
	
	public RestartServerJob(PafServer pafServer, String linuxUsrName, String linuxPasswd) {
		super("Restarting Server");
		this.linuxUsrName = linuxUsrName;
		this.linuxPasswd = linuxPasswd;
		this.pafServer = pafServer;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		StopServerJob stopJob = new StopServerJob(pafServer,linuxUsrName, linuxPasswd);
						
		stopJob.addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void done(IJobChangeEvent event) {
				
				StartServerJob startJob = new StartServerJob(pafServer, linuxUsrName, linuxPasswd);
				
				startJob.addJobChangeListener(new JobChangeAdapter() {

					// called when job is finished
					public void done(IJobChangeEvent event) {

						//refresh servers
						(new ServerRefreshJob()).schedule(); 
						
					}
				});
				
				startJob.schedule();
				
			}
			
		});
		
		stopJob.schedule();
		
		return Status.OK_STATUS;
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.jobs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.StreamGobbler;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.server.ServerPlatform;
import com.pace.base.ui.PafServer;

/**
 * Abstract ServerJob that will be extended by StartServerJob and StopServerJob
 * 
 * @author JMilliron
 *
 */
public abstract class ServerJob extends Job {

	private static final Logger logger = Logger.getLogger(ServerJob.class);
	
	protected PafServer pafServer;
	protected String linuxUsrName;
	protected String linuxPasswd;
	
	protected boolean isStartServer;
	
	protected boolean isStopServer;
	
	/**
	 * Creates a server job.
	 * @param name name of server job
	 * @param pafServer paf server to perform job on
	 */
	public ServerJob(String name, PafServer pafServer, String linuxUsrName, String linuxPasswd) {
		super(name);
		this.pafServer = pafServer;	
		this.linuxUsrName = linuxUsrName;
		this.linuxPasswd = linuxPasswd;
	}
	
	protected abstract String getCommandToExecute();
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		IStatus status = null;
		
		Thread th = new Thread();

		int sleepCnt = 0;

		int maxSleepCnt = pafServer.getServerStartupTimeoutInMilliseconds().intValue() / Constants.SLEEP_TIME;
					
		try {
			
			executeCommand(getCommandToExecute());
			
			while ( true && (isStartServer || isStopServer) ) {
				
				try {
		
					logger.debug("Start Sleeping for: " + Constants.SLEEP_TIME);
					th.sleep(Constants.SLEEP_TIME);
					logger.debug("Done Sleeping");
					sleepCnt++;
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		
				boolean serverRunning = ServerMonitor.getInstance().isServerRunning(pafServer);
				//if started
				if (isStartServer && serverRunning) {
					status = Status.OK_STATUS;									
					break;
				//if stopped
				} else if (isStopServer && ! serverRunning) {
						status = Status.OK_STATUS;									
						break;
				} else if ( sleepCnt > maxSleepCnt ) {
					status = new Status(Status.INFO,null,"Timeout");
					break;
				}
					
			}
			
		} catch (IOException e1) {
			
			e1.printStackTrace();
			
			logger.error(e1.getMessage());
			
			status = Status.OK_STATUS;
			
		}
		
		if ( status == null ) {
			status = new Status(Status.ERROR,null,"Unknown Error");
		}
		
		return status;
	}
	
	
	
	public boolean isStartServer() {
		return isStartServer;
	}

	public void setStartServer(boolean isStartServer) {
		this.isStartServer = isStartServer;
	}

	public boolean isStopServer() {
		return isStopServer;
	}

	public void setStopServer(boolean isStopServer) {
		this.isStopServer = isStopServer;
	}

	/**
	 * Executes a command to start or stop the servers service
	 * 
	 * @param serverServiceExecutable
	 * @throws IOException
	 */
	protected void executeCommand(String serverServiceExecutable) throws IOException {

		List<String> commandList = new ArrayList<String>();
		String serverServiceExeString = getServerCommand(serverServiceExecutable);

		if (pafServer != null) {

			if( pafServer.getServerPlatform() == null || pafServer.getServerPlatform().equals(ServerPlatform.Windows)) {
//				if (pafServer.getOsServiceName() == null || pafServer.getOsServiceName().trim().equals("") ) {
//			
//					pafServer.setOsServiceName(Constants.DEFAULT_SERVER_SERVICE_NAME);
//				}
								
				commandList.add("\"" + getServerCommand(serverServiceExecutable) + "\"" );
				commandList.add(pafServer.getHost());
				commandList.add(pafServer.getOsServiceName());
			}
			
			else if( pafServer.getServerPlatform().equals(ServerPlatform.Linux) ) {
				
				commandList.add("\"" + getServerCommand(serverServiceExecutable) + "\"" );
				commandList.add(linuxUsrName);
				commandList.add(linuxPasswd);
				commandList.add(pafServer.getHost());
				commandList.add(pafServer.getOsServiceName());
			}
		}
		

		String launchInfoMsg = "About to launch command: " + commandList;		
		logger.info(launchInfoMsg);		
		ConsoleWriter.writeMessage(launchInfoMsg);
	
		ProcessBuilder builder = new ProcessBuilder(commandList);
		builder.redirectErrorStream(true);
		
		File serverServiceFile = new File(serverServiceExeString);
				
		if ( serverServiceFile.getParentFile() != null && serverServiceFile.getParentFile().exists() ) {
		
			//TTN-1565: AC - Change working dir to script path when executing startup or shutdown script
			builder.directory(serverServiceFile.getParentFile());
			
			String workingDirMsg = "Working directory: " + builder.directory().toString();		
			logger.info(workingDirMsg);		
			ConsoleWriter.writeMessage(workingDirMsg);
		
		} else {
			
			logger.error("Parent directory '" + serverServiceFile.getParent() + "' doesn't exists.");
		}
		
		
		Process process = builder.start();
				
		StreamGobbler sg = new StreamGobbler(null, process.getInputStream());
		sg.setConsoleLoggingEnabled(true);
		sg.start();
		
		// log
		logger.info("Executing process : " + process.toString());
		
	}

	/**
	 * Gets the server command
	 * 
	 * @return the server command
	 */
	private String getServerCommand(String serverServiceCommand) throws FileNotFoundException {
		
		// get ref to service file
		final File serverServiceFile = new File(serverServiceCommand);

		// if service file does not exist, error then return
		if (! serverServiceFile.exists() || ! serverServiceFile.isFile() ) {

			Display.getDefault().asyncExec(new Runnable() {

				public void run() {

					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING,
							"Could not find server service file: "
									+ serverServiceFile.toString(), MessageDialog.ERROR);
				}
			});

			throw new FileNotFoundException(serverServiceCommand);
			
		}
		
		return serverServiceCommand;
	}

	
}

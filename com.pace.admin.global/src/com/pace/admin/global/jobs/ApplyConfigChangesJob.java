/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.ui.PafServer;
import com.pace.server.client.LoadApplicationRequest;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafSuccessResponse;

/**
 * Apply's a configuration on the server.
 * 
 * @author JMilliron
 *
 */
public class ApplyConfigChangesJob extends Job {
	
	private static final Logger logger = Logger.getLogger(ApplyConfigChangesJob.class);
	
	private PafServer server = null;
	
	private String appId = null;

	/**
	 * Creates a job that will reload a configuraiton on the server.
	 * @param server server app is running
	 * @param appId app id to send to server
	 */
	public ApplyConfigChangesJob(PafServer server, String appId) {
		super(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
		this.server = server;
		this.appId = appId;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
	
		boolean error = false;
		
		if ( server != null && appId != null) {		
			
			String url = server.getCompleteWSDLService();
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
			PafService service = WebServicesUtil.getPafService(url);
			
			if( pafServerAck != null && service != null ) {
			
				LoadApplicationRequest loadAppRequest = new LoadApplicationRequest();
				loadAppRequest.getAppIds().add(appId);
				loadAppRequest.setClientId(pafServerAck.getClientId());
	
				try {
					
					PafSuccessResponse pafSuccuessResponse = service.loadApplication(loadAppRequest);
					
					if( pafSuccuessResponse == null ) {
						error = true;
					} else {
						if( pafSuccuessResponse.isSuccess() ) {
							error = false;
						} else {
							error = true;
						}
					}
				} catch (PafSoapException_Exception e) {
					logger.error(e.getMessage());
					error = true;
				}
			}
			else {
				logger.error("Server is not up or service is not available");
				error = true;
			}
		}
		
		final boolean finalError = error;
		
		Display.getDefault().asyncExec(new Runnable() {
			
			@Override
			public void run() {
		
				String outMessage = null;
				
				if( finalError ) {
					outMessage = Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION + " on server '" + server.getName() + "' was NOT successful.";
					logger.error(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, MessageDialog.ERROR);
				} else { 
					outMessage = Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION + " on server '" + server.getName() + "' was successful.";
					logger.info(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, MessageDialog.INFORMATION);						
				}
		
				ConsoleWriter.writeMessage(outMessage);
			}
		});			
		
		return Status.OK_STATUS;
		
	}

}

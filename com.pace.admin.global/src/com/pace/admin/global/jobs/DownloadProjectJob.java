/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.ui.PafServer;

/**
 * Downloads a project to the workspace.
 * 
 * @author JMilliron
 *
 */
public class DownloadProjectJob extends Job {

	private static final Logger logger = Logger.getLogger(DownloadProjectJob.class);
	
	private IProject project;
	
	private PafServer pafServer;
	
	private boolean succesfullyDownloaded;
	
	private PafApplicationDef initialPafApplicationDef;
	
	/**
	 * Creates job
	 * 
	 * @param project project to download to
	 * @param pafServer server to download from
	 */
	public DownloadProjectJob(IProject project, PafServer pafServer) {
		super("Download Project Job");
		this.project = project;
		this.pafServer = pafServer;
	}
	
	/**
	 * Gets Project
	 * 
	 * @return project
	 */
	public IProject getProject() {
		return project;
	}

	/**
	 * Gets Project Name.
	 * 
	 * @return project name.  returns null if project is null.
	 */
	public String getProjectName() {
		
		if ( project != null ) {
			return project.getName();
		}
		
		return null;
	}
	
	/**
	 * Gets Sever Name.
	 * 
	 * @return server name.  returns null if server is null.
	 */
	public String getServerName() {
		
		if ( pafServer != null ) {
			return pafServer.getName();
		}
		
		return null;
	}
	
	/**
	 * @return the initialPafApplicationDef
	 */
	public PafApplicationDef getInitialPafApplicationDef() {
		return initialPafApplicationDef;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		logger.info("Running Job: " + this.getName());
		
		//TODO: refactor this to one helper util
		initialPafApplicationDef = PafApplicationUtil.getPafApp(project);
			
		succesfullyDownloaded = WebServicesUtil.downloadProjectFromServer(project, pafServer);	
				
		if( succesfullyDownloaded )
			return Status.OK_STATUS;
		else
			return Status.CANCEL_STATUS;
	}

	public boolean isSuccesfullyDownloaded() {
		return succesfullyDownloaded;
	}

}

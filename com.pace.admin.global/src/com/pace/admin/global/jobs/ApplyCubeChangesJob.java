/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.jobs;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafSuccessResponse;
import com.pace.server.client.StartApplicationRequest;

/**
 * 
 * Starts/Restarts application on server
 * 
 * @author JMilliron
 *
 */
public class ApplyCubeChangesJob extends Job {

private static final Logger logger = Logger.getLogger(ApplyCubeChangesJob.class);
	
	private PafServer server = null;
	
	private String appId = null;

	/**
	 * Creates a job that will start/restart application on server
	 * @param server server app is running
	 * @param appId app id to send to server
	 */
	public ApplyCubeChangesJob(PafServer server, String appId) {
		super(Constants.START_OR_RESTART_APPLICATION);
		this.server = server;
		this.appId = appId;
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		boolean error = false;
		
		if ( server != null && appId != null ) {
			
			String url = server.getCompleteWSDLService();
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
			PafService service = null;
			long timeout = server.getServerStartupTimeoutInMilliseconds();
			//TTN-2562, if the user is going to restart the project then look at the Server Startup Timeout
			//However, we will ignore timeouts less then the CFX default of 60 seconds.
			if(timeout > 60000){
				service = WebServicesUtil.getPafService(url, timeout);
			} else {
				service = WebServicesUtil.getPafService(url);
			}
			
			
			if( pafServerAck != null && service != null ) {
				StartApplicationRequest appRequest = new StartApplicationRequest();
				appRequest.getAppIds().add(appId);
				appRequest.setClientId(pafServerAck.getClientId());
				
				try {
					
					PafSuccessResponse pafSuccuessResponse = service.startApplication(appRequest);
					
					if( pafSuccuessResponse == null ) {
						error = true;
					} else {
						if( pafSuccuessResponse.isSuccess() ) {
							error = false;
						} else {
							error = true;
						}
					}
				} catch (PafSoapException_Exception e) {
					logger.error(e.getMessage());
					error = true;
				}				
			}
			else {
				logger.error("Server is not up or service is not available");
				error = true;
			}
		}
		
		final boolean finalError = error;
		
		Display.getDefault().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				
				String outMessage = null;
				
				if( finalError ) {
					outMessage = Constants.START_OR_RESTART_APPLICATION + " on server '" + server.getName() + "' was NOT successful.";
					logger.error(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, outMessage, MessageDialog.ERROR);
				} else {						
//					ServerMonitor.getInstance().refreshAppStates();
					ServerMonitor.getInstance().refreshAllServerAndApplicationStates();
					outMessage = Constants.START_OR_RESTART_APPLICATION + " on server '" + server.getName() + "' was successful.";
					logger.info(outMessage);
					GUIUtil.openMessageWindow(Constants.DIALOG_INFO_HEADING, outMessage, MessageDialog.INFORMATION);						
				}
				
				ConsoleWriter.writeMessage(outMessage);
				
			}
		});
		
		
		return Status.OK_STATUS;
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.List;

import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.base.PafException;

public class MemberListReorderInput {

	List<PaceTreeNode> members = null;
	
	public MemberListReorderInput()  {
		this.members = new ArrayList<PaceTreeNode>();
		
	}
	
	public MemberListReorderInput(List<PaceTreeNode> members) throws PafException {
		this.members = members;
		
	}
	
	public void addMember(PaceTreeNode newMember){
		if(newMember != null && members != null){
			members.add(newMember);
		}
	}
	
	public void addMember(int index, PaceTreeNode newMember){
		if(newMember != null && members != null){
			members.add(index, newMember);
		}
	}
	
	public void addMembers(List<PaceTreeNode> newMembers){
		if(newMembers != null && members != null){
			members.addAll(newMembers);
		}
	}
	
	public List<PaceTreeNode> getMembers() {
		return members;
	}
	
	public List<PaceTreeNode> getMembers(int[] index) {
		List<PaceTreeNode> temp = new ArrayList<PaceTreeNode>();
		
		for(int i : index){
			temp.add(members.get(i));
		}
		
		return temp;
	}
	
	public void removeMember(int index){
		members.remove(index);
	}
	
	public void removeMembers(int[] index){
		List<PaceTreeNode> temp = getMembers(index);
		if(temp != null && temp.size() > 0){
			for(PaceTreeNode node : temp){
				members.remove(node);
			}
		}
	}
	
	/**
	 * @param aliasTable the aliasTable to set
	 */
	public void setAliasTableToDisplay(String aliasTableName) {

		for (PaceTreeNode node : members) {
			
			if ( node != null ) {
			
				node.setAliasTable(aliasTableName);
				
			}
			
		}
		
	}
	
	public PaceTreeNode reorderNode(){
		PaceTreeNode root = members.get(0);
		root.getProperties().setSelectionType(PaceTreeNodeSelectionType.Members);
		root.clearMembers();
		
		for(int i = 1; i < members.size(); i++){
			PaceTreeNode temp = members.get(i);
			temp.getProperties().setSelectionType(PaceTreeNodeSelectionType.Members);
			
			if(temp.getAdditionalPaceTreeNodeList() != null && temp.getAdditionalPaceTreeNodeList().size() > 0){
				temp.clearMembers();
			}
			
			root.addMember(members.get(i));
		}
		
		return root;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.model.managers.DynamicMembersModelManager;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.model.managers.UserSelectionModelManager;
import com.pace.admin.global.util.MemberTagUtil;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.util.PafSimpleDimTreeUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.app.DynamicMemberDef;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafUserSelection;

/**
 * 
 * A tree structure that is used in views.  Each tree has one root node.  Each root node can have
 * 0 or more children.
 * 
 * Typically this is used to display the dimension tree and extra nodes such as UOW_ROOT, Blank, etc.
 * 
 * @author jmilliron
 * @version 1.0
 */
public class PaceTree implements Cloneable {
	
	private static final Logger logger = Logger.getLogger(PaceTree.class);

	protected PaceTreeNode rootNode = new PaceTreeNode(PaceTreeNodeType.Root.toString(), new PaceTreeNodeProperties(PaceTreeNodeType.Root.toString(), 0, 0, null, PaceTreeNodeType.Root), null);

	//list of alias table names
	protected List<String> aliasTableList = null;
	
	//holds the depth of each dimension tree
	protected Map<String, Integer> dimensionTreeHeightMap = new HashMap<String, Integer>();

	//input for tree
	protected PaceTreeInput input;
	

	/**
	 * 
	 * Constructor.  
	 * 
	 * @param input			input for tree
	 * @throws PafException	thrown if exception occurs while building tree
	 */
	public PaceTree(PaceTreeInput input) throws PafException {
		
		//set input
		this.input = input;
		
		//if input is not null
		if ( input != null)  {
						
			aliasTableList = new ArrayList<String>();
			
			//add None as first item
			aliasTableList.add("None");
			
			if ( input.getAliasTables() != null ) {
				
				aliasTableList.addAll(input.getAliasTables());
				
			}
			
			//if dim tree needs to be built
			if ( input.isBuildDimensionTrees() ) {
				
				buildDimensionTree();
				
			}
		
		} 
		
	}
		
	/**
	 * 
	 *  Reads user selections from the model manager and then returns them as a list
	 *
	 * @return list of user selections
	 */
	protected List<PafUserSelection> getUserSelectionList() {
		
		List<PafUserSelection> userSelectionList = new ArrayList<PafUserSelection>(); 
		
		UserSelectionModelManager userSelModelManager = new UserSelectionModelManager(input.getProject());
		
		//if model manager is not empty
		if ( ! userSelModelManager.isEmpty() ) {
		
			for (String userSelectionName : userSelModelManager.getKeys() ) {
				
				userSelectionList.add((PafUserSelection) userSelModelManager.getItem(userSelectionName));
				
			}
		
		}
		
		return userSelectionList;
		
	}
	
	/**
	 * 
	 *  Reads dynamic member defs from the model manager and then returns them as a list
	 *
	 * @return list of dynamic member defs
	 */
	protected List<DynamicMemberDef> getDynamicMemberDefList() {
		
		List<DynamicMemberDef> dynamicMemberDefList = new ArrayList<DynamicMemberDef>();
		
		DynamicMembersModelManager dynamicMembersModelManager = new DynamicMembersModelManager(input.getProject());
		
		if ( ! dynamicMembersModelManager.isEmpty() ) {
			
			for (String key : dynamicMembersModelManager.getKeys() ) {
				
				dynamicMemberDefList.add((DynamicMemberDef) dynamicMembersModelManager.getItem(key));
				
			}
			
		}
		
		return dynamicMemberDefList;
		
	}
	
	/**
	 * 
	 * Builds a dimension tree from the input.  Each dimension subtree will be  a child of the Root Node. 
	 *  
	 *
	 * @throws PafException thrown if exception happens when building the tree
	 */
	private void buildDimensionTree() throws PafException {		
		
		//get dimension tree map.  key is dim name, value is simple dim tree
		Map<String, PafSimpleDimTree> dimTreeMap = input.getPafSimpleDimTreeMap();
					
		PafApplicationDef pafAppDef = PafApplicationUtil.getPafApp(input.getProject());
		if ( dimTreeMap != null ) {
			
			//holds the index for number of trees; ex: if page axis has 3 dims, this will be 1-3
			int treeIndex = 1;
			
			for (String axisDim : input.getAxisDims() ) {
				
				//get tree from map
				PafSimpleDimTree pafSimpleDimTree = dimTreeMap.get(axisDim);
				
				if ( pafSimpleDimTree != null ) {					
					
					//get dimension name
					String dimensionName = pafSimpleDimTree.getId();
										
					//convert tree into hash map.  key is member name, value is simple dim member
					Map<String, PafSimpleDimMember> pafSimpleDimMemberMap = PafSimpleDimTreeUtil.convertTreeIntoHashMap(pafSimpleDimTree);
					
					//get the root dimension node
					PafSimpleDimMember rootChildPafSimpleDimMember = pafSimpleDimMemberMap.get(dimensionName);
					
					//if measure dim and measure root is different than measure dim name
					if ( rootChildPafSimpleDimMember == null && axisDim.equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureDim()) &&
							pafAppDef.getMdbDef().getMeasureRoot() != null && ! pafAppDef.getMdbDef().getMeasureDim().equalsIgnoreCase(pafAppDef.getMdbDef().getMeasureRoot())) {
						
						String topLevelMeasure = pafAppDef.getMdbDef().getMeasureRoot();
						
						PafSimpleDimMember topLevelMeasureDimMember = pafSimpleDimMemberMap.get(topLevelMeasure);
						
						rootChildPafSimpleDimMember = new PafSimpleDimMember();
						rootChildPafSimpleDimMember.setKey(dimensionName);
						rootChildPafSimpleDimMember.getChildKeys().add(topLevelMeasure);
						
						PafSimpleDimMemberProps rootChildPafSimpleDimMemberProps = new PafSimpleDimMemberProps();
						rootChildPafSimpleDimMemberProps.setGenerationNumber(topLevelMeasureDimMember.getPafSimpleDimMemberProps().getGenerationNumber() - 1 );
						rootChildPafSimpleDimMemberProps.setLevelNumber(topLevelMeasureDimMember.getPafSimpleDimMemberProps().getLevelNumber() + 1 );
						rootChildPafSimpleDimMemberProps.getAliasKeys().addAll(topLevelMeasureDimMember.getPafSimpleDimMemberProps().getAliasKeys());
						
						int numberOfAliases = rootChildPafSimpleDimMemberProps.getAliasKeys().size();
						
						List<String> aliasValueList = new ArrayList<String>();
						
						for (int i = 0; i < numberOfAliases; i++) {
							
							aliasValueList.add(axisDim);
							
						}
						
						rootChildPafSimpleDimMemberProps.getAliasValues().addAll(aliasValueList);
												
						rootChildPafSimpleDimMember.setPafSimpleDimMemberProps(rootChildPafSimpleDimMemberProps);						
						
						rootChildPafSimpleDimMember.setParentKey(axisDim);
						
						pafSimpleDimMemberMap.put(dimensionName, rootChildPafSimpleDimMember);
						
					}
					
					//get root child properties
					PaceTreeNodeProperties rootChildNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, rootChildPafSimpleDimMember, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.None);
										
					//change the root pace node type to text only if the dim name is found in the non selectable dim set from paf apps.
					if (input.getNonSelectableBaseTreeDimensionMembers().contains(dimensionName)) {
						
						rootChildNodeProperties.setType(PaceTreeNodeType.TextOnly);
						
					}
					
					//dimension root node
					PaceTreeNode rootChildNode = new PaceTreeNode(dimensionName, rootChildNodeProperties, rootNode);
											
					//add dim name, height of tree
					dimensionTreeHeightMap.put(dimensionName, pafSimpleDimMemberMap.get(rootChildNode.getName()).getPafSimpleDimMemberProps().getLevelNumber());
					
					//BEGIN NFR-143
					//if not a page tree and not the version dim and not an attribute dimension, just add UOW_ROOT
					if ( ! dimensionName.equalsIgnoreCase(pafAppDef.getMdbDef().getVersionDim()) && ! input.getAttributeDims().contains(dimensionName)) {
						PaceTreeNodeProperties nodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.UOWRoot, PaceTreeNodeSelectionType.None);
						new PaceTreeNode(PafBaseConstants.UOW_ROOT, nodeProperties, rootChildNode);
						if( dimensionName.equalsIgnoreCase(pafAppDef.getMdbDef().getYearDim()) ) {
							nodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.MultiYearSingleMember, PaceTreeNodeSelectionType.None);
							new PaceTreeNode(PafBaseConstants.VIEW_TOKEN_FIRST_NONPLAN_YEAR, nodeProperties, rootChildNode);
							new PaceTreeNode(PafBaseConstants.VIEW_TOKEN_FIRST_PLAN_YEAR, nodeProperties, rootChildNode);
						}
						else if( dimensionName.equalsIgnoreCase(pafAppDef.getMdbDef().getTimeDim() ) ) {
							nodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.MultiYearSingleMember, PaceTreeNodeSelectionType.None);
							new PaceTreeNode(PafBaseConstants.VIEW_TOKEN_FIRST_PLAN_PERIOD, nodeProperties, rootChildNode);
						}
					} 
					
					//if version dim add dynamic members
					if ( dimensionName.equalsIgnoreCase(pafAppDef.getMdbDef().getVersionDim())) {
						
						List<DynamicMemberDef> dynamicMemberDefList = getDynamicMemberDefList();

						PaceTreeNodeProperties dynamicMembersNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.TextOnly, PaceTreeNodeSelectionType.None);
						PaceTreeNode dynamicMembersNode = new PaceTreeNode(Constants.DYNAMIC_MEMBERS_NODE_NAME, dynamicMembersNodeProperties, rootChildNode);
												
						for (DynamicMemberDef dynamicMemberDef : dynamicMemberDefList) {							
							
							if ( dynamicMemberDef.getDimension().equals(pafAppDef.getMdbDef().getVersionDim())) {
							
								for (String dynamicMember : dynamicMemberDef.getMemberSpecs()) {
								
									PaceTreeNodeProperties dynamicMemberNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.DynamicMember, PaceTreeNodeSelectionType.Selection);
									new PaceTreeNode(dynamicMember, dynamicMemberNodeProperties, dynamicMembersNode);
								
								}
								
							}
													
						}
												
					}
				
					
					//if not page, add paf blank
					if ( ! input.isPageAxis() ) {
						if( dimensionName.equalsIgnoreCase(pafAppDef.getMdbDef().getYearDim()) ) {
							PaceTreeNodeProperties nodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.MultiYearMultiMember, PaceTreeNodeSelectionType.None);
							new PaceTreeNode(PafBaseConstants.VIEW_TOKEN_NONPLAN_YEARS, nodeProperties, rootChildNode);
							new PaceTreeNode(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS, nodeProperties, rootChildNode);
						}
						PaceTreeNodeProperties pafBlankNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.Blank, PaceTreeNodeSelectionType.None);
						new PaceTreeNode(PafBaseConstants.PAF_BLANK, pafBlankNodeProperties, rootChildNode);
					}	
				
					if ( getUserSelectionList().size() > 0 ) {						
												
						PaceTreeNode userSelectionsNode = null;
					
						for (PafUserSelection userSelection : getUserSelectionList()) {
	
								if ( ! userSelection.getDimension().equalsIgnoreCase(dimensionName)) {
									continue;
								}
								
								//if page, add only single select user selections
								if ( input.isPageAxis() && userSelection.isMultiples()	) {
									continue;							
								} 
					
								//if user selection node is null on 1st valid user selection, create new node
								if ( userSelectionsNode == null ) {
						
									PaceTreeNodeProperties userSelectionsNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.TextOnly, PaceTreeNodeSelectionType.None);
									userSelectionsNode = new PaceTreeNode(Constants.USER_SELECTIONS_NODE_NAME, userSelectionsNodeProperties, rootChildNode);
									
								}
								
								PaceTreeNodeProperties userSelectionNodeProperties = null;
								
								if ( userSelection.isMultiples()) {
									userSelectionNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.UserSelMulti, PaceTreeNodeSelectionType.Selection);
								} else {
									userSelectionNodeProperties = new PaceTreeNodeProperties(dimensionName, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.UserSelSingle, PaceTreeNodeSelectionType.Selection);
								}
								
								new PaceTreeNode(PafBaseConstants.USER_SEL_TAG + "(" + userSelection.getId() + ")", userSelectionNodeProperties, userSelectionsNode);
								
							}
					
					}
					//END NFR-143
					
					//builds pace tree starting with root dim node
					buildPaceTree(rootChildNode, dimensionName, rootChildPafSimpleDimMember.getChildKeys(), pafSimpleDimMemberMap, input.getAxisDims().size(), treeIndex);
																			
					//increment tree index
					treeIndex++;			
					
				}				
																
			}
			
			//if is building on column or row axis
			if ( input.isColumnAxis() || input.isRowAxis() ) {
				//create member tag model manager to read in member tag model
				MemberTagModelManager memberTagModelManager = new MemberTagModelManager(input.getProject());
															
				//get filtered member tag defs
				MemberTagDef[] memberTagDefs = memberTagModelManager.getMemberTags(input.getAllDims().toArray(new String[0]), true);
				
				if ( memberTagDefs != null ) {
												
					PafAxis currentAxis = null;
					
					//get current axis for row or column
					if ( input.isRowAxis() ) {
						
						currentAxis = new PafAxis(PafAxis.ROW);
						
					} else if ( input.isColumnAxis() ) {
						
						currentAxis = new PafAxis(PafAxis.COL);
					} 
															
					PaceTreeNodeProperties memberTagsNodeProperties = new PaceTreeNodeProperties(null, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.TextOnly, PaceTreeNodeSelectionType.None);
					PaceTreeNode memberTagsNode = new PaceTreeNode(Constants.MEMBER_TAGS_NODE_NAME, memberTagsNodeProperties, rootNode);
					
					//go over member tags and find out which ones are valid for the current axis
					for (MemberTagDef memberTagDef : memberTagDefs ) {
						
						//if valid, create a new member tag tree item node
						if ( MemberTagUtil.isMemberTagValidForAxis(input.getViewSection(), memberTagDef, currentAxis)) {
							
							PaceTreeNodeProperties memberTagNodeProperties = new PaceTreeNodeProperties(null, input.getAxisDims().size(), treeIndex, null, PaceTreeNodeType.MemberTag, PaceTreeNodeSelectionType.Selection);
							new PaceTreeNode(memberTagDef.getName(), memberTagNodeProperties, memberTagsNode);							
							
						}
							
															
					}
					
					//if no memember tag children exists, remove node
					if ( ! memberTagsNode.hasChildren()) {
						
						memberTagsNode.remove();
						
					}
					
				}
				
				
			}
			
		}
				
		
	}


	/**
	 * 
	 *  Recursively builds a dimension sub tree starting with dimension node.
	 *
	 * @param parentNode				parent node
	 * @param dimensionName				name of dimension {Measures, Product, etc }
	 * @param nodesList					children names of parent node
	 * @param pafSimpleDimMemberMap		map key = member name, value = simple dim member
	 * @param axisDimCnt				number of dimensions on axis
	 * @param treeIndex					axis index of dimension
	 */
	private void buildPaceTree(PaceTreeNode parentNode, String dimensionName, List<String> nodesList, Map<String, PafSimpleDimMember> pafSimpleDimMemberMap, int axisDimCnt, int treeIndex) {
		
		for (String node : nodesList ) {
			
			PafSimpleDimMember pafSimpleDimMember = pafSimpleDimMemberMap.get(node);
			
			PaceTreeNodeProperties paceNodeProperties = new PaceTreeNodeProperties(dimensionName, axisDimCnt, treeIndex, pafSimpleDimMember, PaceTreeNodeType.PaceMember, PaceTreeNodeSelectionType.None);
			
			PaceTreeNode paceNode = new PaceTreeNode(node, paceNodeProperties, parentNode);
						
			buildPaceTree(paceNode, dimensionName, pafSimpleDimMember.getChildKeys(), pafSimpleDimMemberMap, axisDimCnt, treeIndex);		
			
			if ( ! paceNode.hasChildren()) {
				
				paceNode.getProperties().setType(PaceTreeNodeType.PaceMemberLevel0);
				
			}
			
		}
		
	}
	
	/**
	 * 
	 * Finds a tree node using member name within a pace tree.  The search
	 * starts with the root node.
	 *
	 * @param memberName 	Member name to search for
	 * @return				If memberName was found in the tree, the found node is returned
	 */
	public PaceTreeNode findTreeNode(String memberName) {
			
		return findTreeNode(rootNode, memberName);
		
	}

	/**
	 * 
	 *  Finds a tree node using member name within a pace tree.  The search
	 *  starts with the node passed in.
	 *
	 * @param searchFromNode	Node to start search from
	 * @param memberName		Member name to search for
	 * @return 					If memberName was found in the tree, the found node is returned
	 */
	public PaceTreeNode findTreeNode(PaceTreeNode searchFromNode, String memberName) {
		
		PaceTreeNode foundNode = null;
		
		if ( searchFromNode != null && memberName != null ) {
		
			if ( searchFromNode.getName().equalsIgnoreCase(memberName)) {
				
				foundNode = searchFromNode;
				
			} else {
				
				for (PaceTreeNode childNode : searchFromNode.getChildren()) {
					
					foundNode = findTreeNode(childNode, memberName);
					
					if ( foundNode != null ) {
						break;
					}
					
				}
				
				
			}
				
		}
		
		return foundNode;
	}

	/**
	 * @return the rootNode
	 */
	public PaceTreeNode getRootNode() {
		return rootNode;
	}
	
	/**
	 * @return the aliasTableList
	 */
	public List<String> getAliasTableList() {
		return aliasTableList;
	}

	/**
	 * 
	 *  Specifies if PaceTree is a page axis tree
	 *
	 * @return true if Page tree
	 */
	public boolean isPageTree() {
		
		if (input == null ) {
			return false;
		}
		
		return input.isPageAxis();
		
	}
	
	/**
	 * 
	 *  Specifies if PaceTree is a column axis tree
	 *
	 * @return true if Column tree
	 */
	public boolean isColumnTree() {
		
		if (input == null ) {
			return false;
		}
		
		return input.isColumnAxis();
		
	}
	
	/**
	 * 
	 *  Specifies if PaceTree is a row axis tree
	 *
	 * @return true if Row tree
	 */	
	public boolean isRowTree() {
		
		if (input == null ) {
			return false;
		}
		
		return input.isRowAxis();
		
	}
	
	/**
	 * 
	 * Sets alias table name for root node.  This will push the change down to all children of root node
	 * and their children until all nodes have new alias table
	 *
	 * @param aliasTable
	 */
	public void setAliasTableToDisplay(String aliasTable) {
		
		if ( rootNode != null ) {
			
			rootNode.setAliasTable(aliasTable);
			
		}
		
	}


	/**
	 * @return the dimensionTreeHeightMap
	 */
	public Map<String, Integer> getDimensionTreeHeightMap() {
		
		return new HashMap<String, Integer>(dimensionTreeHeightMap);
	}

	@Override
	public PaceTree clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		PaceTree paceTree =  (PaceTree) super.clone();
		
		paceTree.rootNode = rootNode;
		paceTree.aliasTableList = aliasTableList;
		paceTree.dimensionTreeHeightMap = dimensionTreeHeightMap;
		//paceTree.input = input;
		
		return paceTree;
	}

	
   // TTN-2513 Method added to get the dialogInput .
	public PaceTreeInput getInput() {
		return input;
	}
	
}

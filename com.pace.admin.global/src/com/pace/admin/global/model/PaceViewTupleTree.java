/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.exceptions.InvalidStateException;
import com.pace.admin.global.util.PaceTreeUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.app.DynamicMemberDef;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafUserSelection;
import com.pace.base.view.ViewTuple;

/**
 * View Tuple Tree that is used as the model for PaceTreeContentProvider and PaceTreeLabelProvider.
 *
 * @version	x.xx
 * @author JavaJ
 *
 */
public class PaceViewTupleTree extends PaceTree {

	private static final Logger logger = Logger.getLogger(PaceViewTupleTree.class);
	
	/**
	 * Constructor - create a pace view tuple tree
	 * 
	 * @param input			pace tree input
	 * @throws PafException 
	 */
	public PaceViewTupleTree(PaceTreeInput input) throws PafException {
		
		this(input, null);
						
	}
	
	/**
	 * Constructor - creates a pace view tuple tree from existing view tuples
	 * 
	 * @param input
	 * @param viewTuples
	 * @throws PafException
	 */
	public PaceViewTupleTree(PaceTreeInput input, ViewTuple[] viewTuples) throws PafException {

		super(input);
		
		/* Logic to create the PaceTreeNode structure */
		if ( viewTuples != null ) {
						
			List<PaceTreeNode> nodeList = new ArrayList<PaceTreeNode>();

			logger.debug("Starting to create pace tree tuple node strcuture from view tuples");
			
			//loop over cloned view tuples and create pace tree node and tree from view tuples
			for (ViewTuple vt : viewTuples.clone() ) {					
								
				PaceTreeNode newNode = createTupleNodeStructure(rootNode, vt, 0);
				
				if ( nodeList.size() == 0 || (! nodeList.get(nodeList.size() -1).getName().equals(newNode.getName()))) {
				
					nodeList.add(newNode);
					
				}
									
			}
			
			logger.debug("Finished creating pace tree tuple node strcuture from view tuples");
		
		}
				
	}
	
	/**
	 * 
	 * @param node
	 * @return
	 */
	private int retrieveSymmetricGroupNum(PaceTreeNode node){
		
		if(node.getViewTuple() == null && node.hasChildren()){
			PaceTreeNode lastChild = node.getChildren().get(node.getChildren().size()-1);
			if(lastChild.getViewTuple() == null)
				return retrieveSymmetricGroupNum(lastChild);
			else 
				if( lastChild.getViewTuple().getSymetricGroupNo() != null 
					&& lastChild.getViewTuple().getSymetricGroupNo().length > 0 )
					return lastChild.getViewTuple().getSymetricGroupNo(0);
			//int existingGroupNum = grandChild.getViewTuple().getSymetricGroupNo(0);
			//return existingGroupNum;
		}
		
		return -1;
	}
	
	/**
	 * 
	 *  Creates a PaceTreeNode from existing ViewTuple.  This method is recursively called as it
	 *  builds PaceTreeNodes from the ViewTuple.  
	 *  
	 *  A ViewTuple has an array of member names.  For example, view tuples could have a member def array as such:
	 *  
	 *  1st Tuple
	 *    SLS_DLR
	 *    WF
	 *    WK1
	 *    
	 *  2nd Tuple
	 *    SLS_DLR
	 *    WF
	 *    WK2
	 *    
	 *  3rd Tuple
	 *    SLS_DLR
	 *    WP
	 *    WK1
	 *    
	 *   This method creates a structure as such for hte viewer:
	 *   
	 *   SLS_DLR
	 *   	WF
	 *   		WK1
	 *   		WK2
	 *		WP
	 *			WK1
	 *
	 *    In this case, the Root node has one child SLS_DLR, SLS_DLR has two children WF and WP, etc.   
	 *
	 * @param parent Parent to use when creating new node
	 * @param vt	 ViewTuple, used for it's member definitions
	 * @param ndx	 Current member definition index.  
	 * @return		 Newly creating PaceTreeNode
	 */
	private PaceTreeNode createTupleNodeStructure(PaceTreeNode parent, ViewTuple vt, int ndx) {

		PaceTreeNode node = null;
		int existingGroupNum = retrieveSymmetricGroupNum(parent);
		
		int tempGroupNum = 0;
		if(vt != null && vt.getSymetricGroupNo() != null &&  vt.getSymetricGroupNo().length > 0 ){
			tempGroupNum = vt.getSymetricGroupNo(0);
		}
		
		//if parent already has child and child is pafblank, use getName instead of getDisplayName
		if ( vt.getMemberDefs().length != (ndx + 1) && parent.hasChildren() 
				&& parent.getChildren().get(parent.getChildren().size()-1).isType(PaceTreeNodeType.Blank) 
				&& parent.getChildren().get(parent.getChildren().size()-1).getName().equals(vt.getMemberDefs()[ndx]) ) {
			
			node = parent.getChildren().get(parent.getChildren().size()-1);
			
		//if parent already has existing node at specified dimension ndx, reuse
		//only allow grouping of view tuples that have the same parent display name and symmetic group number (TTN-1961, TTN-2308)
		} else if ( vt.getMemberDefs().length != (ndx + 1) && parent.hasChildren() 
				&& parent.getChildren().get(parent.getChildren().size()-1).getDisplayName().equals(vt.getMemberDefs()[ndx])
				&& tempGroupNum == existingGroupNum
				) {
			
			node = parent.getChildren().get(parent.getChildren().size()-1);
			
		}
		else {
			
			String dimensionName = input.getAxisDims().get(ndx);
						
			//get current member from member def and member def index
			String memberWithSelection = vt.getMemberDefs()[ndx]; 
			
			//create a member selection from existing member from tuple
			PaceMemberSelection memberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(memberWithSelection);
			
			//get members as array
			String[] members = memberSelection.getMembers();
									
			PaceTreeNodeType memberTreeNodeType = null;
						
			int memberNdx = 0;
			
			//for each member
			for (String member : members) {
			
				PafSimpleDimMember pafSimpleDimMember = null;
				
				if ( input.getPafSimpleDimTreeMap() != null && input.getPafSimpleDimTreeMap().containsKey(dimensionName) ) {
					
					PafSimpleDimTree simpleDimTree = input.getPafSimpleDimTreeMap().get(dimensionName); 
					
					pafSimpleDimMember = PaceTreeUtil.getPafSimpleDimMember(simpleDimTree, member);
					
				} else {
					
					pafSimpleDimMember = null;
					
				}
				
				String userMemberName = member;
				
				//if userselection
				if ( memberSelection.isUserSelection() || member.startsWith(PaceMemberSelection.USER_SEL_TAG)) {
															
					if ( member.startsWith(PaceMemberSelection.USER_SEL_TAG) ) {
						
						PaceMemberSelection userPaceMemberSel = PaceMemberSelection.createMemberSelectionFromExistingMember(member);
																
						userMemberName = userPaceMemberSel.getMembers()[0];
						
					} 
					
					List<PafUserSelection> userSelectionList = getUserSelectionList();
					
					for (PafUserSelection userSelection : userSelectionList) {
						
						if (userSelection.getDimension().equals(dimensionName) && userSelection.getId().equals(userMemberName)) {
							
							if ( userSelection.isMultiples()) {
								
								memberTreeNodeType = PaceTreeNodeType.UserSelMulti;			
								
							} else {
								
								memberTreeNodeType = PaceTreeNodeType.UserSelSingle;
								
							}
							
							break;
							
						}						
						
					}		
					
					/*
					//if user selection is not found, remove user selection status and treat as text only
					if ( ! isUserSelectionFound ) {
					
						memberSelection.setUserSelection(false);
						
					}
					*/										
					
				//if member tag
				} else if ( vt.isMemberTag() ) {
					
					memberTreeNodeType = PaceTreeNodeType.MemberTag;
					
				//if blank
				} else if ( member.equals(PafBaseConstants.PAF_BLANK)) {
					
					memberTreeNodeType = PaceTreeNodeType.Blank;
					
				//if uow root
				} else if ( member.equals(PafBaseConstants.UOW_ROOT)) {
					
					memberTreeNodeType = PaceTreeNodeType.UOWRoot;
										
				} else if( memberSelection.isMultiYearSingleMember(member)) {
					
					memberTreeNodeType = PaceTreeNodeType.MultiYearSingleMember;
										
				} else if( memberSelection.isMultiYearMultipleMember(member)) {
					
					memberTreeNodeType = PaceTreeNodeType.MultiYearMultiMember;
										
				} else {
															
					List<DynamicMemberDef> dynamicMemberDefList = getDynamicMemberDefList();
					
					//loop and see if it's a dynamic member					
					for (DynamicMemberDef dynamicMemberDef : dynamicMemberDefList) {							
						
						if ( dynamicMemberDef.getDimension().equals(dimensionName)) {
						
							for (String dynamicMember : dynamicMemberDef.getMemberSpecs()) {
								
								if ( member.equals(dynamicMember)) {
									
									memberTreeNodeType = PaceTreeNodeType.DynamicMember;
									
									break;
								}
								
							}
						}
					}					
				
					if ( pafSimpleDimMember != null) {					
						
						if ( pafSimpleDimMember.getPafSimpleDimMemberProps().getLevelNumber() == 0 ) {
							
							memberTreeNodeType = PaceTreeNodeType.PaceMemberLevel0;
							
						} else {
						
							memberTreeNodeType = PaceTreeNodeType.PaceMember;
							
						}						
						
					} 					
														
				}
				
				//if null, set default to Text Only
				if ( memberTreeNodeType == null ) {
					memberTreeNodeType = PaceTreeNodeType.TextOnly;
				}
									
				
				String paceTreeNodeName = member;
				
				//if a user selection, use the @USER_SEL(<member name>) instead of just member name
				if ( memberTreeNodeType.equals(PaceTreeNodeType.UserSelMulti) || memberTreeNodeType.equals(PaceTreeNodeType.UserSelSingle) ) {
					
					//if 1st member item, use memberSelection, otherwise create new Member Selection from user selection member
					if ( memberNdx == 0 ) {
						
						paceTreeNodeName = memberSelection.getMemberWithUserSelection();
						
					} else {
					
						paceTreeNodeName = PaceMemberSelection.createMemberSelectionFromExistingMember(member).getMemberWithUserSelection();
						
					}
					
				//if user selection but no longer a valid user selection, set pace tree node as passed in						
				} else if ( memberSelection.isUserSelection() && memberTreeNodeType.equals(PaceTreeNodeType.TextOnly)) {
					
					paceTreeNodeName = memberSelection.getMemberWithUserSelection();
					
				}
				
				int levelGenNumber = memberSelection.getLevelGenNumber();
				
				if ( node == null ) {
					if( memberSelection.getSelectionType().equals(PaceTreeNodeSelectionType.OffsetMember) ||  memberSelection.getSelectionType().equals(PaceTreeNodeSelectionType.OffsetMemberRange) ) {
						node = new PaceTreeNode(paceTreeNodeName, 
								new PaceTreeNodeProperties(dimensionName, vt.getMemberDefs().length, (ndx+1), pafSimpleDimMember, PaceTreeNodeType.PaceMemberLevel0, memberSelection.getSelectionType(), true, memberSelection.getOffsetMinNumber(), memberSelection.getOffsetMaxNumber(), true), 
								parent);
					}
					else {
						node = new PaceTreeNode(paceTreeNodeName, 
								new PaceTreeNodeProperties(dimensionName, vt.getMemberDefs().length, (ndx+1), pafSimpleDimMember, memberTreeNodeType, memberSelection.getSelectionType(), true, levelGenNumber, true), 
								parent);
					}
					//set ViewTuple
					node.setViewTuple(vt);
					
				} else {
					PaceTreeNode additionalNode = null;
					if( memberSelection.getSelectionType().equals(PaceTreeNodeSelectionType.OffsetMember) ||  memberSelection.getSelectionType().equals(PaceTreeNodeSelectionType.OffsetMemberRange) ) {
						additionalNode = new PaceTreeNode(paceTreeNodeName, 
								new PaceTreeNodeProperties(dimensionName, vt.getMemberDefs().length, (ndx+1), pafSimpleDimMember, PaceTreeNodeType.PaceMemberLevel0, memberSelection.getSelectionType(), true, memberSelection.getOffsetMinNumber(), memberSelection.getOffsetMaxNumber(), true), 
								null);
					}
					else {
						additionalNode = new PaceTreeNode(paceTreeNodeName, 
								new PaceTreeNodeProperties(dimensionName, vt.getMemberDefs().length, (ndx+1), pafSimpleDimMember, memberTreeNodeType, memberSelection.getSelectionType(), true, levelGenNumber, true), 
								null);
					}
					additionalNode.setParentNode(node.getParentNode());
					
					node.addMember(additionalNode);
					
				}	
				
				memberNdx++;
										
			}		
			
		}
		
		//increment index and if the new ndx value doesn't = the number of member defs in array, call itself
		if ( ++ndx != vt.getMemberDefs().length ) {
			
			createTupleNodeStructure(node, vt, ndx);
			
		}
		
		return node;
		
	}
	
	/**
	 * 
	 * Validates the Pace View Tuple tree. 
	 *
	 * @throws InvalidStateException
	 */
	public void validate() throws InvalidStateException {
		
		PaceTreeAxis paceTreeAxis = PaceTreeAxis.Undefined;
		
		//get pace tree axis
		if ( isColumnTree() ) {
			paceTreeAxis = PaceTreeAxis.Column;
		} else if ( isRowTree() ) {
			paceTreeAxis = PaceTreeAxis.Row;
		}
						
		//if root doesn't have any children
		if ( rootNode.getChildren().size() == 0 ) {
			
			throw new InvalidStateException("Incomplete " + paceTreeAxis.toString() + " tree.  At lease one fully defined member must exist.");
			
		}
		
		//loop children and verify all leaf nodes are view tuples
		for (PaceTreeNode rootChildNode : rootNode.getChildren()) {
		
			List<PaceTreeNode> bottomLevelChildNodes = PaceTreeUtil.getAllBottomLevelPaceTreeNodes(rootChildNode);
			
			for (PaceTreeNode  bottomLevelChildNode: bottomLevelChildNodes) {
				
				if ( ! bottomLevelChildNode.isViewTuple() ) {
																	
					throw new InvalidStateException("Incomplete " + paceTreeAxis.toString() + " tree.  Check member " + bottomLevelChildNode.getDisplayName(), bottomLevelChildNode);
									
				}
				
			}
			
		}		
				
	}
	
	/**
	 *  Creates a list of View tuples from the PaceTreeNode's within the tree.
	 *
	 * @return list of view tuples
	 * @throws InvalidStateException
	 */
	public List<ViewTuple> getViewTuples() throws InvalidStateException {
		
		validate();
		
		createSymmetricGroupNums(rootNode);
		
		rootNode.setAliasTable(null);
		
		List<ViewTuple> viewTuples = new ArrayList<ViewTuple>();
		
		List<PaceTreeNode> bottomLevelNodes = PaceTreeUtil.getAllBottomLevelPaceTreeNodes(rootNode);
		
		for (PaceTreeNode paceTreeNode : bottomLevelNodes) {
			
			logger.debug(paceTreeNode.getDisplayName());
			
			ViewTuple vt = paceTreeNode.getViewTuple();
			
			if ( isColumnTree() ) {
				vt.setAxis(PafAxis.COL);
			} else if ( isRowTree() ) {
				vt.setAxis(PafAxis.ROW);
			}
			
			viewTuples.add(vt);
			
			logger.debug("\t" + paceTreeNode.toString());
			logger.debug("\t" + Arrays.asList(vt.getMemberDefs()));
			
		}
		
		return viewTuples;
	}
		
	/**
	 * Sets the symmetric group number for node.
	 *
	 * @param node node to start adding the symmetric number to
	 */
	private void createSymmetricGroupNums(PaceTreeNode node) {

		int nodeItemIndex = 0;
						
		for (PaceTreeNode childNode : node.getChildren() ) {
			
			//if childNode is a tuple, set group number and increment by 1
			if ( childNode.isViewTuple() ) {

				childNode.setSymetricGroupNumber(null);
				
			} else {				

				childNode.setSymetricGroupNumber(nodeItemIndex++);
		
				logger.debug("Node: " + node.getName() + "; Symmetric Group Number: " + nodeItemIndex);
				
				createSymmetricGroupNums(childNode);
								
			}
			
		}
		
	}
		
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.enums.PaceTreeAxis;
import com.pace.admin.global.util.PafApplicationUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.base.app.PafApplicationDef;
import com.pace.server.client.PafSimpleDimTree;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PaceTreeInput {
		
	private static final Logger logger = Logger.getLogger(PaceTreeInput.class);
	
	private IProject project;
	
	private PafViewSectionUI viewSection;
	
	private List<String> aliasTables;
	
	private PaceTreeAxis paceTreeAxis;
	
	private List<String> axisDims;
	
	private List<String> baseDims;
	
	private List<String> attributeDims;
	
	private Map<String, PafSimpleDimTree> pafSimpleDimTreeMap;
	
	private boolean buildDimensionTrees;
	
	private Set<String> nonSelectableBaseTreeDimensionMembers;
	
	public PaceTreeInput(IProject project, PafViewSectionUI viewSection, List<String> aliasTables, PaceTreeAxis paceTreeAxis, List<String> axisDims, List<String> baseDims, List<String> attributeDims, Map<String, PafSimpleDimTree> pafSimpleDimTreeMap, boolean buildDimensionTrees) {
		
		//TODO: project and viewsection can not be null
		this.project = project;		
		this.viewSection = viewSection;
				
		if ( aliasTables == null ) {
			this.aliasTables = new ArrayList<String>();
		} else {
			this.aliasTables = aliasTables;
		}
		
		this.paceTreeAxis = paceTreeAxis;
						
		if ( axisDims == null ) {
			this.axisDims = new ArrayList<String>();
		} else {
			this.axisDims = axisDims;
		}
		
		if ( baseDims == null ) {
			this.baseDims = new ArrayList<String>();
		} else {
			this.baseDims = baseDims;
		}
		
		if ( attributeDims == null ) {
			this.attributeDims = new ArrayList<String>();
		} else {
			this.attributeDims = attributeDims;	
		}
				
		this.pafSimpleDimTreeMap = pafSimpleDimTreeMap;
		this.buildDimensionTrees = buildDimensionTrees;
		
		PafApplicationDef pafAppDef = PafApplicationUtil.getPafApp(this.project);
		
		nonSelectableBaseTreeDimensionMembers = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
		nonSelectableBaseTreeDimensionMembers.add(pafAppDef.getMdbDef().getYearDim());
		nonSelectableBaseTreeDimensionMembers.add(pafAppDef.getMdbDef().getVersionDim());
		nonSelectableBaseTreeDimensionMembers.add(pafAppDef.getMdbDef().getMeasureDim());
		nonSelectableBaseTreeDimensionMembers.add(pafAppDef.getMdbDef().getPlanTypeDim());
				
	}

	/**
	 * @return the aliasTables
	 */
	public List<String> getAliasTables() {
		return aliasTables;
	}

	/**
	 * @param aliasTables the aliasTables to set
	 */
	public void setAliasTables(List<String> aliasTables) {
		this.aliasTables = aliasTables;
	}

	/**
	 * @return the axisDims
	 */
	public List<String> getAxisDims() {
		return axisDims;
	}

	/**
	 * @param axisDims the axisDims to set
	 */
	public void setAxisDims(List<String> axisDims) {
		this.axisDims = axisDims;
	}

	/**
	 * @return the attributeDims
	 */
	public List<String> getAttributeDims() {
		return attributeDims;
	}

	/**
	 * @param attributeDims the attributeDims to set
	 */
	public void setAttributeDims(List<String> attributeDims) {
		this.attributeDims = attributeDims;
	}

	/**
	 * @return the pafSimpleDimTreeMap
	 */
	public Map<String, PafSimpleDimTree> getPafSimpleDimTreeMap() {
		return pafSimpleDimTreeMap;
	}

	/**
	 * @param pafSimpleDimTreeMap the pafSimpleDimTreeMap to set
	 */
	public void setPafSimpleDimTreeMap(
			Map<String, PafSimpleDimTree> pafSimpleDimTreeMap) {
		this.pafSimpleDimTreeMap = pafSimpleDimTreeMap;
	}

	/**
	 * @return true if the page axis
	 */
	public boolean isPageAxis() {
		
		if ( paceTreeAxis != null && paceTreeAxis.equals(PaceTreeAxis.Page)) {
			
			return true;
			
		}
		
		return false;
	}
	
	/**
	 * @return true if the column axis
	 */
	public boolean isColumnAxis() {
		
		if ( paceTreeAxis != null && paceTreeAxis.equals(PaceTreeAxis.Column)) {
			
			return true;
			
		}
		
		return false;
	}

	/**
	 * @return true if the row axis
	 */
	public boolean isRowAxis() {
		
		if ( paceTreeAxis != null && paceTreeAxis.equals(PaceTreeAxis.Row)) {
			
			return true;
			
		}
		
		return false;
	}

	/**
	 * @return the project
	 */
	public IProject getProject() {
		return project;
	}

	public List<String> getAllDims() {
		
		List<String> allDims = new ArrayList<String>();
		
		allDims.addAll(this.baseDims);
		allDims.addAll(this.attributeDims);
		
		return allDims;
		
	}

	/**
	 * @return the viewSection
	 */
	public PafViewSectionUI getViewSection() {
		return viewSection;
	}

	/**
	 * @return the buildDimensionTrees
	 */
	public boolean isBuildDimensionTrees() {
		return buildDimensionTrees;
	}

	/**
	 * @param buildDimensionTrees the buildDimensionTrees to set
	 */
	public void setBuildDimensionTrees(boolean buildDimensionTrees) {
		this.buildDimensionTrees = buildDimensionTrees;
	}

	/**
	 * @return the nonSelectableBaseTreeDimensionMembers
	 */
	public Set<String> getNonSelectableBaseTreeDimensionMembers() {
		return new TreeSet<String>(nonSelectableBaseTreeDimensionMembers);
	}

	
	
}

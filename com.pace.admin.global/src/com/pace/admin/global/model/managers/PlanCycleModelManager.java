/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.global.model.managers;

import java.util.Collection;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PlanCycle;
import com.pace.base.app.Season;

/**
* The PlanCycleModelManager handles the model objects for the PlanCycle.
* @author kmoos
* @version x.xx
 */
public class PlanCycleModelManager extends ModelManager {
	private static Logger logger = Logger.getLogger(PlanCycleModelManager.class);
	
	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public PlanCycleModelManager(IProject project) {
		super(project);
		this.load();
	}

	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	public void load() {

		// create empty model with case insensitive keys
		model = new TreeMap<String, PlanCycle>(
				String.CASE_INSENSITIVE_ORDER);

		// create an nulled paf view array
		PlanCycle[] pafPlanCycles = null;

		try {
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));

			pafPlanCycles = appDef[0].getPlanCycles();
			
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
					+ PafBaseConstants.FN_ApplicationMetaData);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}

		if (pafPlanCycles != null) {

			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (PlanCycle pafPlanCycle : pafPlanCycles) {
				model.put(pafPlanCycle.getLabel(), pafPlanCycle);
			}
		}
		logger.debug(model);
	}

	/**
	 * Saves the model objects
	 */
	@SuppressWarnings("unchecked")
	public void save(){
		try{
			
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
			
			Collection<PlanCycle> pafPlanCycle = model.values();

			PlanCycle[] pafPlanCycles = (PlanCycle[]) pafPlanCycle.toArray(new PlanCycle[0]);
			
			appDef[0].setPlanCycles(pafPlanCycles);
			
			ACPafXStream.exportObjectToXml(appDef, getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
			
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Renames a plan cycle
	 * @param oldPlanCycleName The old plan cycle process name.
	 * @param newPlanCycleName The new plan cycle process name.
	 */
	public void renamePlanCycle(String oldPlanCycleName, String newPlanCycleName) {

		PlanCycle[] planCycles = (PlanCycle[]) model.values().toArray(new PlanCycle[0]);
		boolean change = false;

		for (PlanCycle planCycle : planCycles) {
			if (planCycle.getLabel().equals(oldPlanCycleName)) {
				planCycle.setLabel(newPlanCycleName);
				change = true;
			}
		}
		
		if (change) {
			save();
			load();
		}
	}
	
	/**
	 * Find duplicates in array of plan cycles
	 * @param newSeasonName
	 * @return
	 */
	public boolean findDulicatePlanCycleName(String newPlanCycleName) {
		// create an nulled paf view array
		PlanCycle[] pafPlanCycles = null;

		try {
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
				getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
			pafPlanCycles = appDef[0].getPlanCycles();
			
			for (PlanCycle planCycle : pafPlanCycles) {
				if (planCycle.getLabel().equals(newPlanCycleName)) {
					return false;
				}
			}
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
				+ PafBaseConstants.FN_ApplicationMetaData);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}
		return true;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}
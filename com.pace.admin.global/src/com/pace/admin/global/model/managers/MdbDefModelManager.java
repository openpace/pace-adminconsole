/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.MdbDef;
import com.pace.base.app.PafApplicationDef;

/**
* The MdbDefModelManager handles the model objects for the mdbDef.
* @author kmoos
* @version x.xx
*/
public class MdbDefModelManager extends ModelManager {
	private static Logger logger = Logger.getLogger(MdbDefModelManager.class);

	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public MdbDefModelManager(IProject project) {
		super(project);
		this.load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		//create empty model with case insensitive keys
		model = new TreeMap<String, MdbDef>(
				String.CASE_INSENSITIVE_ORDER);

		// create an nulled paf view array
		MdbDef mdbDef = null;

		try {
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));

			mdbDef = appDef[0].getMdbDef();
			
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_ApplicationMetaData);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		// if there are paf views...
		if (mdbDef != null) {
			model.put(mdbDef.getDataSourceId(), mdbDef);
		}
		logger.debug(model);
	}

	/**
	 * Saves the model to file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		try {
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
			
			appDef[0].setMdbDef((MdbDef) model.values().iterator().next());
			
			ACPafXStream.exportObjectToXml(appDef, getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
		
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
					+ PafBaseConstants.FN_ApplicationMetaData);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}
	}
	
	/**
	 * Gets the MdbDef.
	 * @return An MdbDef
	 */
	public MdbDef getMdbDef() {
		return (MdbDef) model.values().iterator().next();
	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
}

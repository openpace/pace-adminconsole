/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.comm.CustomMenuDef;

/**
 * The CustomMenuDefModelManager handles the model objects for custom menus.
* @author kmoos
* @version x.xx
 */
public class CustomMenuDefModelManager extends ModelManager {
	private static Logger logger = Logger.getLogger(CustomMenuDefModelManager.class);
	
	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public CustomMenuDefModelManager(IProject project) {
		super(project);
		this.load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	public void load() {

		// create empty model with case insensitive keys
		model = new TreeMap<String, CustomMenuDef>(
				String.CASE_INSENSITIVE_ORDER);

		CustomMenuDef[] appDef = null;
		
		try {
			appDef = (CustomMenuDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_CustomMenus));
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
					+ PafBaseConstants.FN_CustomMenus);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}

		// if there are paf views...
		if (appDef != null) {

			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (CustomMenuDef customMenuDef : appDef) {
				model.put(customMenuDef.getKey(), customMenuDef);
			}
		}
		logger.debug(model);
	}


	/**
	 * Saves the model to file.
	 */
	@Override
	public void save() {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.global.model.managers;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.GenFormat;
import com.pace.base.format.LevelFormat;
import com.pace.base.format.MemberFormat;
import com.pace.base.view.ConditionalFormatDimension;

/**
 * The GenerationFormattingModelManager handles the model objects for Hierarchy formatting. 
 *  
 * @author ihuang
 *
 */
public class ConditionalFormatModelManager extends ModelManager {

	private static Logger logger = Logger.getLogger(ConditionalFormatModelManager.class);
	
	/**
	 * Construtor
	 * 
	 * @param projectDir The project directory for the current project
	 */
	public ConditionalFormatModelManager(IProject project) {
		super(project);
		load();
		
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
		
	/**
	 * Loads the model from file.
	 */
	@SuppressWarnings("unchecked")
	public void load() {

		logger.info("Loading Conditional Formats file");
		
		try {
			model = new TreeMap<String, ConditionalFormat>();
			
			model = (TreeMap<String, ConditionalFormat>) ACPafXStream.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_ConditionalFormats));
			
		} catch (PafConfigFileNotFoundException pex) {
			PafErrHandler.handleException(pex);
			logger.debug("Conditional Formats file not found.  Creating an empty model.");
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		logger.info("Successfully loaded Conditional Formats file");
		
		
	}
	
	/**
	 * Renames all the plan cycles in the array of seasons.
	 * @param oldPlanCycleName The old plan cycle name.
	 * @param newPlanCycleName The new plan cycle name.
	 */
	@SuppressWarnings("unchecked")
	public void renameConditionalFormat(String oldName, String newName){
		ConditionalFormat oldCondFormat = (ConditionalFormat) model.get(oldName);
		if( oldCondFormat != null ) {
			ConditionalFormat newCondFormat = oldCondFormat.clone();
			newCondFormat.setName(newName);
			model.remove(oldName);
			model.put(newName, newCondFormat);
		}
	}

	public ConditionalFormat findDuplicateConditionalFormat( ConditionalFormat condFormat ) {
		Iterator<Entry<String,ConditionalFormat>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,ConditionalFormat> entry = it.next();
		    ConditionalFormat tmpFormat = (ConditionalFormat)  entry.getValue();
			if ( ! tmpFormat.equals(condFormat) 
					&& tmpFormat.getName().equalsIgnoreCase(condFormat.getName())) {
				return tmpFormat;
			} 
		}
		return null;
	}

	public ConditionalFormat findConditionalFormatWithStyle(String styleName) {
		Iterator<Entry<String,ConditionalFormat>> itFormat = model.entrySet().iterator();
		while (itFormat.hasNext()) {
		    Entry<String,ConditionalFormat> formatEntry = itFormat.next();
		    ConditionalFormat condFormat = (ConditionalFormat)  formatEntry.getValue();
			if( condFormat != null ) {
				ConditionalFormatDimension condFormatDimension = condFormat.getPrimaryDimension();
				Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
				Iterator<Entry<Integer,LevelFormat>> itLevel = levelFormats.entrySet().iterator();
				while (itLevel.hasNext()) {
				    Entry<Integer,LevelFormat> entry = itLevel.next();
					if( entry.getValue().getFormatName().equals(styleName) ) {
						return condFormat;
					}
				}
				Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
				Iterator<Entry<Integer,GenFormat>> itGenFormat = genFormats.entrySet().iterator();
				while (itGenFormat.hasNext()) {
				    Entry<Integer,GenFormat> entry = itGenFormat.next();
					if( entry.getValue().getFormatName().equals(styleName) )
						return condFormat;
				}
				Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
				Iterator<Entry<String,MemberFormat>> itMemFormat = memberFormats.entrySet().iterator();
				while (itMemFormat.hasNext()) {
				    Entry<String,MemberFormat> entry = itMemFormat.next();
					if( entry.getValue().getFormatName().equals(styleName)  )
						return condFormat;
				}
			}
		}
		return null;
	}
	
	public Set<String> findConditionalFormatsWithStyle(String styleName) {
		Set<String> tmpCondFormats = new HashSet<String>();
		Iterator<Entry<String,ConditionalFormat>> itFormat = model.entrySet().iterator();
		while (itFormat.hasNext()) {
		    Entry<String,ConditionalFormat> formatEntry = itFormat.next();
		    ConditionalFormat condFormat = (ConditionalFormat)  formatEntry.getValue();
			if( condFormat != null ) {
				ConditionalFormatDimension condFormatDimension = condFormat.getPrimaryDimension();
				Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
				Iterator<Entry<Integer,LevelFormat>> itLevel = levelFormats.entrySet().iterator();
				while (itLevel.hasNext()) {
				    Entry<Integer,LevelFormat> entry = itLevel.next();
					if( entry.getValue().getFormatName().equals(styleName) ) {
						tmpCondFormats.add(condFormat.getName());
					}
				}
				Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
				Iterator<Entry<Integer,GenFormat>> itGenFormat = genFormats.entrySet().iterator();
				while (itGenFormat.hasNext()) {
				    Entry<Integer,GenFormat> entry = itGenFormat.next();
					if( entry.getValue().getFormatName().equals(styleName) ) {
						tmpCondFormats.add(condFormat.getName());
					}
				}
				Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
				Iterator<Entry<String,MemberFormat>> itMemFormat = memberFormats.entrySet().iterator();
				while (itMemFormat.hasNext()) {
				    Entry<String,MemberFormat> entry = itMemFormat.next();
					if( entry.getValue().getFormatName().equals(styleName) ) {
						tmpCondFormats.add(condFormat.getName());
					}
				}
			}
		}
		return tmpCondFormats;
	}
	
	public int findStyleCountInConditionalFormat(String formatName, String styleName) {
		ConditionalFormat conditionalFormat = (ConditionalFormat) model.get(formatName);
		int count = 0;
		if( formatName != null ) {
			ConditionalFormatDimension condFormatDimension = conditionalFormat.getPrimaryDimension();
			Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
			Iterator<Entry<Integer,LevelFormat>> itLevelFormat = levelFormats.entrySet().iterator();
			while (itLevelFormat.hasNext()) {
			    Entry<Integer,LevelFormat> entry = itLevelFormat.next();
			    if( styleName != null ) {
					if( entry.getValue().getFormatName().equals(styleName) ) {
						count++;
					}
			    }
				else{
					count++;
				}
			}
			Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
			Iterator<Entry<Integer,GenFormat>> itGenFormat = genFormats.entrySet().iterator();
			while (itGenFormat.hasNext()) {
			    Entry<Integer,GenFormat> entry = itGenFormat.next();
			    if( styleName != null ) {
					if( entry.getValue().getFormatName().equals(styleName) ) {
						count++;
					}
			    }
				else{
					count++;
				}
			}
			Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
			Iterator<Entry<String,MemberFormat>> itMemFormat = memberFormats.entrySet().iterator();
			while (itMemFormat.hasNext()) {
			    Entry<String,MemberFormat> entry = itMemFormat.next();
			    if( styleName != null ) {
					if( entry.getValue().getFormatName().equals(styleName) ) {
						count++;
					}
			    }
				else{
					count++;
				}
			}
		}
		return count;
	}
	
	public void removeStyleInConditionalFormats(String styleName) {
		Iterator<Entry<String,ConditionalFormat>> itFormat = model.entrySet().iterator();
		while (itFormat.hasNext()) {
		    Entry<String,ConditionalFormat> formatEntry = itFormat.next();
		    ConditionalFormat condFormat = (ConditionalFormat)  formatEntry.getValue();
			if( condFormat != null ) {
				ConditionalFormatDimension condFormatDimension = condFormat.getPrimaryDimension();
				Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
				Iterator<Entry<Integer,LevelFormat>> itLevel = levelFormats.entrySet().iterator();
				while (itLevel.hasNext()) {
				    Entry<Integer,LevelFormat> entry = itLevel.next();
					if( entry.getValue().getFormatName().equals(styleName) )
						itLevel.remove();
				}
				Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
				Iterator<Entry<Integer,GenFormat>> itGenFormat = genFormats.entrySet().iterator();
				while (itGenFormat.hasNext()) {
				    Entry<Integer,GenFormat> entry = itGenFormat.next();
					if( entry.getValue().getFormatName().equals(styleName) )
						itGenFormat.remove();
				}
				Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
				Iterator<Entry<String,MemberFormat>> itMemFormat = memberFormats.entrySet().iterator();
				while (itMemFormat.hasNext()) {
				    Entry<String,MemberFormat> entry = itMemFormat.next();
					if( entry.getValue().getFormatName().equals(styleName)  )
						itMemFormat.remove();
				}
			}
		}
		save();
	}
	
	@SuppressWarnings("unchecked")
	public void renameStyleInConditionalFormats(String oldName, String newName){
		Iterator<Entry<String,ConditionalFormat>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,ConditionalFormat> entry = it.next();
		    ConditionalFormat conditionalFormat  = entry.getValue();
			ConditionalFormatDimension condFormatDimension = conditionalFormat.getPrimaryDimension();
			Map<Integer, LevelFormat> levelFormats = condFormatDimension.getLevelFormats();
			Map<Integer, GenFormat> genFormats = condFormatDimension.getGenFormats();
			Map<String, MemberFormat> memberFormats = condFormatDimension.getMemberFormats();
			for ( Map.Entry<Integer, LevelFormat> entryFormat : levelFormats.entrySet() ) {
				if( entryFormat.getValue().getFormatName().equals(oldName ))
					entryFormat.getValue().setFormatName(newName);
			}
			for ( Map.Entry<Integer, GenFormat> entryFormat : genFormats.entrySet() ) {
				if( entryFormat.getValue().getFormatName().equals(oldName ))
					entryFormat.getValue().setFormatName(newName);
			}
			for ( Map.Entry<String, MemberFormat> entryFormat : memberFormats.entrySet() ) {
				if( entryFormat.getValue().getFormatName().equals(oldName ))
					entryFormat.getValue().setFormatName(newName);
			}
		}
	}

	/**
	 * Saves the model to file.
	 */
	public void save() {

		ACPafXStream.exportObjectToXml(model, getIFileFromProject(PafBaseConstants.FN_ConditionalFormats));

	}
		
}
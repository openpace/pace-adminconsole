/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.model.managers;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.security.AdminPersistLockDef;

/**
 * @author jmilliron
 * 
 */
public class AdminLockModelManager extends ModelManager {

	public AdminLockModelManager(IProject iProject) {
		super(iProject);
		load();

	}

	private static Logger logger = Logger
			.getLogger(AdminLockModelManager.class);

	@SuppressWarnings("unchecked")
	public void load() {

		IFile adminLockFile = getIFileFromProject(PafBaseConstants.FN_AdminLocks);

		model = new TreeMap<String, AdminPersistLockDef>(String.CASE_INSENSITIVE_ORDER);
		try {
			model = (TreeMap<String, AdminPersistLockDef>) ACPafXStream
					.importObjectFromXml(adminLockFile);
		} 
		catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ adminLockFile.getLocation());
			model = new TreeMap<String, AdminPersistLockDef>(String.CASE_INSENSITIVE_ORDER);
		} 
		catch (Exception ex) {
			logger.error(ex.getMessage());
		}

	}

	public void save() {
		
		ACPafXStream.exportObjectToXml(model, getIFileFromProject(PafBaseConstants.FN_AdminLocks));

	}

	/**
	 * Renames  Admin Lock 
	 * @param oldPName The old Admin Lock name.
	 * @param newName The new Admin Lock name.
	 */
	@SuppressWarnings("unchecked")
	public void renameAdminLock(String oldName, String newName){
		Iterator<Entry<String,AdminPersistLockDef>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,AdminPersistLockDef> entry = it.next();
			if(entry.getKey().equalsIgnoreCase(oldName)){
				AdminPersistLockDef newAdminLock = entry.getValue().clone();
				newAdminLock.setName(newName);
				model.remove(oldName);
				model.put(newName, newAdminLock);
				break;
			}
		}
	}

	public AdminPersistLockDef findAdminLockByName( String adminLockName ) {
		Iterator<Entry<String,AdminPersistLockDef>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,AdminPersistLockDef> entry = it.next();
		    AdminPersistLockDef tmpAdminLock = (AdminPersistLockDef)  entry.getValue();
			if ( tmpAdminLock.getName().equals(adminLockName) ) {
				return tmpAdminLock;
			} 
		}
		return null;
	}
	
	public AdminPersistLockDef findDuplicateAdminLock( AdminPersistLockDef adminLock ) {
		Iterator<Entry<String,AdminPersistLockDef>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,AdminPersistLockDef> entry = it.next();
		    AdminPersistLockDef tmpAdminLock = (AdminPersistLockDef)  entry.getValue();
			if ( ! tmpAdminLock.equals(adminLock) 
					&& tmpAdminLock.getName().equalsIgnoreCase(adminLock.getName())) {
				return tmpAdminLock;
			} 
		}
		return null;
	}
}
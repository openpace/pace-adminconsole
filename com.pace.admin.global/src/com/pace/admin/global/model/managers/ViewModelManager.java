/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.base.ViewPrintState;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.ui.PrintStyle;
import com.pace.base.utility.PafImportExportUtility;
import com.pace.base.utility.PafXStream;
import com.pace.base.view.PafView;
import com.pace.base.view.PafViewGroup;
import com.thoughtworks.xstream.XStream;

/**
 * @author jmilliron
 * 
 */
public class ViewModelManager extends ModelManager {

	private Map<String, PafViewGroup> viewGroupModel;
	
	public ViewModelManager(IProject project) {
		super(project);
		load();
	}

	private static Logger logger = Logger.getLogger(ViewModelManager.class);

	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	public void load() {

		// create empty model with case insensitive keys
		model = new TreeMap<String, PafAdminConsoleView>(
				String.CASE_INSENSITIVE_ORDER);

		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		
		
		
		//
		
		//create an nulled paf view array
		//removed - krm 1/18/2010
		//PafView[] pafViews = PafImportExportUtility.importViews(iConfFolder.getLocation().toString(), true);		
		
		//try to refresh the directory to one depth
		
		
		
		List<PafView> pafViews = null;
		try {
			
			XMLPaceProject pp = new XMLPaceProject(
					iConfFolder.getLocation().toString(), 
					new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.Views)),
							false);
			
			pafViews = pp.getViews();
			viewGroupModel = pp.getViewGroups();
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);


		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		} catch (InvalidPaceProjectInputException e) {

			logger.error("Invalid Pace Project: " + e.getMessage());
			
		} catch (PaceProjectCreationException e) {

			logger.error("Problem creating Pace Project: " + e.getMessage());
			
		}
	
		// if there are paf views...
		if (pafViews != null) {

			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (PafView pafView : pafViews) {

				PafAdminConsoleView pafAdminConsoleView = new PafAdminConsoleView(
						pafView);

				try{
				
					model.put(pafAdminConsoleView.getName(), pafAdminConsoleView);
					
				} catch (Exception e) {

					logger.error("Problem refreshing local folder: " + e.getMessage());
					
				}

			}

		}

	}

	/**
	 * Renames a view.
	 * @param oldViewName old view name.
	 * @param newViewName new view name.
	 */
	@SuppressWarnings("unchecked")
	public void renameView(String oldViewName, String newViewName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		ArrayList<String> saveItems = new ArrayList<String>();
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				//if the current key == the old view name.
				if ( key.equalsIgnoreCase(oldViewName) ) {
					
					//get a temporary PafAdminConsoleView object for the current key 
					PafAdminConsoleView pafAdminConsoleView = (PafAdminConsoleView) model.get(key);
					//set the new name.
					pafAdminConsoleView.setName(newViewName);
					//remove old object from map and put the new one in.
					remove(key, true, false);
					//new the temporary object in the map.
					model.put(newViewName, pafAdminConsoleView);
					//add the object in the array list of items to be saved to disk.
					if(!saveItems.contains(newViewName)){
						saveItems.add(newViewName);
					}
					
				} 
			}		
			
		}
		
		save(saveItems);
		load();
	}
	
	/**
	 * Renames a all occurrences of a view section.
	 * @param oldViewSectionName Old view section.
	 * @param newViewSectionName New view section name.
	 */
	public void renameViewSection(String oldViewSectionName, String newViewSectionName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		ArrayList<String> saveItems = new ArrayList<String>();
		boolean wasChanged = false;
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				PafAdminConsoleView pafAdminConsoleView = (PafAdminConsoleView) model.get(key);
				
				//is the old view section in the current map of view sections.
				if(pafAdminConsoleView.getViewSectionNames().contains(oldViewSectionName)){
					//get a set of view section.
					Set<String> viewSections = pafAdminConsoleView.getViewSectionNames();
					//get and iterator.
					Iterator<String> it = viewSections.iterator();
					//create a temporary array list.
					ArrayList<String> tempViewSections = new ArrayList<String>();
					while(it.hasNext()){
						String s = it.next();
						//does the old view section == the current item.
						if(s.equalsIgnoreCase(oldViewSectionName)){
							//if so modify the item.
							tempViewSections.add(newViewSectionName);
							if(!saveItems.contains(key)){
								saveItems.add(key);
							}
						} else{
							//if not then just add the current item and continue.
							tempViewSections.add(s);
						}
					}
					//clear the map.
					viewSections.clear();
					//add the temporary array list.
					viewSections.addAll(tempViewSections);
					//set the view sections in the PafAdminConsoleView
					pafAdminConsoleView.setViewSectionNames(viewSections);
					//the map was modified so we need to save to disk.
					wasChanged = true;
				}
				
			}		
			
		}
		if(wasChanged){
			save(saveItems);
			load();
		}
	}
	
	
	/**
	 * Renames a all occurrences of a view section.
	 * @param oldViewSectionName Old view section.
	 * @param newViewSectionName New view section name.
	 */
	public void removeViewSection(String oldViewSectionName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		ArrayList<String> saveItems = new ArrayList<String>();
		boolean wasChanged = false;
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				PafAdminConsoleView pafAdminConsoleView = (PafAdminConsoleView) model.get(key);
				
				//is the old view section in the current map of view sections.
				if(pafAdminConsoleView.getViewSectionNames().contains(oldViewSectionName)){
					//get a set of view section.
					Set<String> viewSections = pafAdminConsoleView.getViewSectionNames();
					//get and iterator.
					Iterator<String> it = viewSections.iterator();
					//create a temporary array list.
					ArrayList<String> tempViewSections = new ArrayList<String>();
					while(it.hasNext()){
						String s = it.next();
						//does the old view section == the current item.
						if(s.equalsIgnoreCase(oldViewSectionName)){
							//if so modify the item.
							//tempViewSections.add(newViewSectionName);
							if(!saveItems.contains(key)){
								saveItems.add(key);
							}
						} else{
							//if not then just add the current item and continue.
							tempViewSections.add(s);
						}
					}
					//clear the map.
					viewSections.clear();
					//add the temporary array list.
					viewSections.addAll(tempViewSections);
					//set the view sections in the PafAdminConsoleView
					pafAdminConsoleView.setViewSectionNames(viewSections);
					//the map was modified so we need to save to disk.
					wasChanged = true;
				}
				
			}		
			
		}
		if(wasChanged){
			save(saveItems);
			load();
		}
	}
	
	/**
	 * Saves only the items in the list.
	 * @param items items to save.
	 */
	public void save(ArrayList<String> items){

		// get the model keys
		String[] keys = getKeys();

		// if there are view model items
		if (keys != null) {

			// create an empty PafView array with a size = to the number of items to save.
			PafView[] pafViews = new PafView[items.size()];

			// index to iterate through the keys
			int pafViewsNdx = 0;

			// for each key in keys, get the paf admin console view object and
			// add to the paf views array by converting it to the paf view object.
			for (String key : keys) {
				
				if(items.contains(key)){
					
					PafAdminConsoleView pafAdminConsoleView = (PafAdminConsoleView) getItem(key);
	
					pafViews[pafViewsNdx] = pafAdminConsoleView.getPafView();
	
					// set the view section names from the admin console view object.
					pafViews[pafViewsNdx++].setViewSectionNames(pafAdminConsoleView
							.getViewSectionNames().toArray(new String[0]));
					
				}
			}

			// turn off references
			PafXStream.setMode(XStream.NO_REFERENCES);

			// export object to file system
			//ACPafXStream.exportObjectToXml(pafViews, getIFileFromProject(Constants.VIEWS_FILE));
			
			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
			
			//PafImportExportUtility.exportView(pafViews, iConfFolder.getLocation().toString());
			PafImportExportUtility.exportViews(pafViews, iConfFolder.getLocation().toString(), false);
			
			//try to refresh the directory to one depth
			try {
				
				iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

			} catch (CoreException e) {
				
				logger.error("Problem refreshing local folder: " + e.getMessage());
				
			}
			
		}
	}
	
	
	
	/**
	 * Saves the model objects
	 */

	public void save() {

		// get the model keys
		String[] keys = getKeys();

		// if there are view model items
		if (keys != null) {

			// create an empty PafView array with a size = to the number of keys
			PafView[] pafViews = new PafView[keys.length];

			// index to iterate through the keys
			int pafViewsNdx = 0;

			// for each key in keys, get the paf admin console view object and
			// add to
			// the paf views array by converting it to the paf view object.
			for (String key : keys) {

				PafAdminConsoleView pafAdminConsoleView = (PafAdminConsoleView) getItem(key);

				pafViews[pafViewsNdx] = pafAdminConsoleView.getPafView();

				// set the view section names from the admin console view
				// object.
				pafViews[pafViewsNdx++].setViewSectionNames(pafAdminConsoleView
						.getViewSectionNames().toArray(new String[0]));
			}

			// turn off references
			PafXStream.setMode(XStream.NO_REFERENCES);

			// export object to file system
			//ACPafXStream.exportObjectToXml(pafViews, getIFileFromProject(Constants.VIEWS_FILE));
			
			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
			
			PafImportExportUtility.exportViews(pafViews, iConfFolder.getLocation().toString(), true);
			
//			try to refresh the directory to one depth
			try {
				
				iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

			} catch (CoreException e) {
				
				logger.error("Problem refreshing local folder: " + e.getMessage());
				
			}
			
		}

	}

	public void remove(String key, boolean removeFromFileSystem, boolean cascade) {
		
		model.remove(key);
		
		if(removeFromFileSystem){
			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
			PafImportExportUtility.deleteViews(new String[] {key}, iConfFolder.getLocation().toString());
		}
		
		if(cascade){
			// Deletes key from role configurations
			PafPlannerConfigModelManager ppcmm = new PafPlannerConfigModelManager(project);
			ppcmm.deleteViewInRoleConfigurations(key);
			ppcmm.save();
			
			
			// deletes key from all views in all view groups
			PafViewGroupModelManager viewGroupMM = new PafViewGroupModelManager(project);
			viewGroupMM.deleteViewInViewGroups(key);
			viewGroupMM.save();
		}
		
	}
	
	
	public void remove(String key) {

		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		super.remove(key);
		PafImportExportUtility.deleteViews(new String[] {key}, iConfFolder.getLocation().toString());
		
		// Deletes key from role configurations
		PafPlannerConfigModelManager ppcmm = new PafPlannerConfigModelManager(project);
		ppcmm.deleteViewInRoleConfigurations(key);
		ppcmm.save();
		
		
		// deletes key from all views in all view groups
		PafViewGroupModelManager viewGroupMM = new PafViewGroupModelManager(project);
		viewGroupMM.deleteViewInViewGroups(key);
		viewGroupMM.save();
		

	}
	
	public List<PafAdminConsoleView> findPrintStyleInViewsByGUID(String guid) {
		List<PafAdminConsoleView> pafViews = new ArrayList<PafAdminConsoleView>();
		for (String key : super.getKeys()) {
			PafAdminConsoleView view = (PafAdminConsoleView)getItem(key);
			if( view != null ) {
				if( view.getGlobalPrintStyleGUID() != null && view.getGlobalPrintStyleGUID().equalsIgnoreCase(guid) )
					pafViews.add(view);
			}
		}
		return pafViews;
	}

	public void renamePrintStyleInViews(String oldName, String newName ) {
		for (String key : super.getKeys()) {
			PafAdminConsoleView view = (PafAdminConsoleView)getItem(key);
			if( view != null ) {
				PrintStyle printStyle = view.getPrintStyle();
				if( printStyle != null && printStyle.getName().equalsIgnoreCase(oldName) )
					printStyle.setName(newName);
			}
		}
		save();
		load();
	}
	
	public void resetDefaultPrintStyleInViews(String newDefaultPrintName ) {
		for (String key : super.getKeys()) {
			PafAdminConsoleView view = (PafAdminConsoleView)getItem(key);
			if( view != null && view.getViewPrintState() == ViewPrintState.GLOBAL ) {
				PrintStyle printStyle = view.getPrintStyle();
				if( printStyle != null && printStyle.getName().equalsIgnoreCase(newDefaultPrintName) ) {
					view.setGlobalPrintStyleGUID(null);
					view.setViewPrintState(ViewPrintState.DEFAULT);
				}
			}
		}
		save();
		load();
	}
	
	public Map<String, PafViewGroup> getViewGroupModel() {
		return viewGroupModel;
	}

}
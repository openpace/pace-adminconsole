/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.model.managers;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafPlannerRole;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;

/**
 * @author jmilliron
 * 
 */
public class UserSecurityModelManager extends ModelManager {

	public UserSecurityModelManager(IProject iProject) {
		super(iProject);
		load();

	}

	private static Logger logger = Logger
			.getLogger(UserSecurityModelManager.class);

	@SuppressWarnings("unchecked")
	public void load() {

		PafUserSecurity[] pafUsers = null;

		IFile securityFile = getIFileFromProject(PafBaseConstants.FN_SecurityMetaData);

		try {
			pafUsers = (PafUserSecurity[]) ACPafXStream
					.importObjectFromXml(securityFile);
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ securityFile.getLocation());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		// create a new model with Generic Tree Map
		model = new TreeMap<String, PafUserSecurity>(
				String.CASE_INSENSITIVE_ORDER);

		// if users exist
		if (pafUsers != null) {

			// populate model from user []
			for (PafUserSecurity pafUser : pafUsers) {

				model.put(pafUser.getKey(), pafUser);

			}

		}
	}

	public void save() {
		
		try{
			
			PafUserSecurity[] pafUsers = (PafUserSecurity[]) model.values().toArray(new PafUserSecurity[0]);
			
			ACPafXStream.exportObjectToXml(pafUsers, getIFileFromProject(PafBaseConstants.FN_SecurityMetaData));
			
		} catch(Exception e){
			
			logger.error(e.getLocalizedMessage());
			
		}

	}
	
	/**
	 * Renames a planner role inside all the associated paf security role filters.
	 * @param oldPlannerRoleName The old planner role.
	 * @param newPlannerRoleName The new planner role.
	 */
	@SuppressWarnings("unchecked")
	public void renamePlannerRole(String oldPlannerRoleName, String newPlannerRoleName){
		PafUserSecurity[] pafUserSecurityList = (PafUserSecurity[]) model.values().toArray(new PafUserSecurity[0]);
		boolean change = false;
		
		for(PafUserSecurity pafUserSecurity : pafUserSecurityList){
			
			LinkedHashMap<String, PafWorkSpec[]> roleFilters = pafUserSecurity.getRoleFilters();
			LinkedHashMap<String, PafWorkSpec[]> newRoleFilters = new LinkedHashMap<String, PafWorkSpec[]>();
			
			if (roleFilters != null) {
				
				for(Entry<String, PafWorkSpec[]> entry : roleFilters.entrySet()) {
					String plannerRoleName  = entry.getKey();
					PafWorkSpec[] pafWorkSpecs = entry.getValue();
					
					// role filter contains role you're trying to replace
					if (plannerRoleName.equalsIgnoreCase(oldPlannerRoleName)) {

						// rename PafWorkSpec that references old name
						for(PafWorkSpec pafWorkSpec : pafWorkSpecs) {
							if (pafWorkSpec.getName().equals(oldPlannerRoleName)) {
								pafWorkSpec.setName(newPlannerRoleName);
							}
						}
						
						newRoleFilters.put(newPlannerRoleName, pafWorkSpecs);
						change = true;
					
					// entry does not contain the role you're trying to replace -- keep original entry
					} else {
						newRoleFilters.put(plannerRoleName, pafWorkSpecs);
					}
				}
				
				pafUserSecurity.setRoleFilters(newRoleFilters);						
			}
		}
		if(change){
			save();
			load();
		}
	}

}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;

/**
* The PafUserModelManager handles the model objects for the paf user.
* @author kmoos
* @version x.xx
*/
public class PafUserModelManager extends ModelManager {
	private static Logger logger = Logger.getLogger(PafUserModelManager.class);

	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public PafUserModelManager(IProject project) {
		super(project);
		this.load();
	}

	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		// create empty model with case insensitive keys
		model = new TreeMap<String, PafUserSecurity>(String.CASE_INSENSITIVE_ORDER);
	
		// create an nulled paf view array
		PafUserSecurity[] pafUsers = null;
	
		try {
			pafUsers = (PafUserSecurity[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_SecurityMetaData));
	
			
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_SecurityMetaData);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}
	
		// if there are paf views...
		if (pafUsers != null) {
	
			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (PafUserSecurity PafUserSecurity : pafUsers) {
				model.put(PafUserSecurity.getUserName(), PafUserSecurity);
			}
		}
		logger.debug(model);
	}

	/**
	 * Saves the model to file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		try{
			PafUserSecurity[] pafUsers = (PafUserSecurity[]) model.values().toArray(new PafUserSecurity[0]);
			ACPafXStream.exportObjectToXml(pafUsers, getIFileFromProject(PafBaseConstants.FN_SecurityMetaData));
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
		}
	}

	/**
	 * Renames the role name within the paf user configuration.
	 * @param oldRoleName The role name to update.
	 * @param newRoleName The new role name.
	 */
	@SuppressWarnings("unchecked")
	public void renameRoleNames(String oldRoleName, String newRoleName){
		PafUserSecurity[] pafUsers = (PafUserSecurity[]) model.values().toArray(new PafUserSecurity[0]);
		boolean change = false;

		for(PafUserSecurity PafUserSecurity : pafUsers){
			
			if(PafUserSecurity.getRoleFilters().containsKey(oldRoleName)){
												
				//TTN-1218 Start
				
				Map<String, PafWorkSpec[]> newRowFilterMap = new LinkedHashMap<String, PafWorkSpec[]>(); 
				
				List<String> roleNameOrderedList = new ArrayList<String>();
				
				for (String roleName : PafUserSecurity.getRoleFilters().keySet()) {
					
					roleNameOrderedList.add(roleName);
					
				}
				
				for (String roleName : roleNameOrderedList) {
													
					PafWorkSpec[] pafWorkSpec = PafUserSecurity.getRoleFilters().get(roleName);
					
					if ( roleName.equalsIgnoreCase(oldRoleName) ) {
					
						newRowFilterMap.put(newRoleName, pafWorkSpec);
						
					} else {
						
						newRowFilterMap.put(roleName, pafWorkSpec);
						
					}
				
					PafUserSecurity.getRoleFilters().remove(roleName);
					
				}
				
				PafUserSecurity.getRoleFilters().putAll(newRowFilterMap);
				
				//TTN-1218 End
				
				change = true;
				
			}
		}
		if(change){
			save();
		}
	}
	
	/**
	 * Removes a role from the paf user configuration.
	 * @param roleName The role to remove.
	 */
	@SuppressWarnings("unchecked")
	public void removeRoles(String roleName){
		PafUserSecurity[] pafUsers = (PafUserSecurity[]) model.values().toArray(new PafUserSecurity[0]);
		boolean change = false;

		for(PafUserSecurity PafUserSecurity : pafUsers){
			if(PafUserSecurity.getRoleFilters().containsKey(roleName)){
				//remove the old one.
				PafUserSecurity.getRoleFilters().remove(roleName);
				//set the change flag.
				change = true;
			}
		}
		if(change){
			save();
		}
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.PafImportExportUtility;
import com.pace.base.view.PafViewGroup;
import com.pace.base.view.PafViewGroupItem;

/**
* The PafViewGroupModelManager handles the model objects for the PafViewGroup.
* @author kmoos
* @version x.xx
*
*/
public class PafViewGroupModelManager extends ModelManager {

	private static Logger logger = Logger
		.getLogger(PafViewGroupModelManager.class);

	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public PafViewGroupModelManager(IProject project) {
		super(project);
		this.load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
	
		try {
			
			XMLPaceProject pp = new XMLPaceProject(
					iConfFolder.getLocation().toString(), 
					new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.Views)),
							false);
			
			model = pp.getViewGroups();


		}  catch (InvalidPaceProjectInputException e) {

			logger.error("Invalid Pace Project: " + e.getMessage());
			
		} catch (PaceProjectCreationException e) {

			logger.error("Problem creating Pace Project: " + e.getMessage());
			
		}
		
		//model = PafImportExportUtility.importViewGroups(iConfFolder.getLocation().toString(), true);
		
		try {
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		}

	}

	/**
	 * Saves the model to file.
	 */
	@Override
	public void save() {
		
		
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		PafImportExportUtility.exportViewGroups(model, iConfFolder.getLocation().toString());
				
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	public void renameViewGroupName(String oldViewGroupName, String newViewGroupName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				PafViewGroup pafViewGroup = (PafViewGroup) model.get(key);
				
				updateViewGroupReferences(pafViewGroup, oldViewGroupName, newViewGroupName);
				
				if ( key.equalsIgnoreCase(oldViewGroupName) ) {
					
					//remove old object from map
					model.remove(key);
					
					key = newViewGroupName;			
					
				} 
				
				model.put(key, pafViewGroup);
				
			}		
			
		}
		
		save();
		load();
		
	}
	
	@SuppressWarnings("unchecked")
	private void deleteViewGroupNameReferences(String deletedViewGroupName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				PafViewGroup pafViewGroup = (PafViewGroup) model.get(key);
									
				if ( pafViewGroup != null ) {
					
					ArrayList<PafViewGroupItem> pafViewGroupItemList = new ArrayList<PafViewGroupItem>();
					
					PafViewGroupItem[] pafViewGroupItems = pafViewGroup.getPafViewGroupItems();
					
					if ( pafViewGroupItems != null ) {
						
						for ( PafViewGroupItem pafViewGroupItem : pafViewGroupItems) {
							
							if ( pafViewGroupItem.isViewGroup() && pafViewGroupItem.getName().equalsIgnoreCase(deletedViewGroupName))
								
								continue;
							
							pafViewGroupItemList.add(pafViewGroupItem);
							
						}
						
						pafViewGroup.setPafViewGroupItems(pafViewGroupItemList.toArray(new PafViewGroupItem[0]));
						
						
					}
					model.put(key, pafViewGroup);
					
				}
				
			}		
			
		}
		
		save();
		load();
		
		// Deletes key from role configurations
		PafPlannerConfigModelManager ppcmm = new PafPlannerConfigModelManager(project);
		ppcmm.deleteViewInRoleConfigurations(deletedViewGroupName);
		ppcmm.save();
		
	}

	private void updateViewGroupReferences(PafViewGroup pafViewGroup, String oldViewGroupName, String newViewGroupName) {
		
		if ( pafViewGroup.getPafViewGroupItems() != null && pafViewGroup.getPafViewGroupItems().length > 0 ) {
			
			for (PafViewGroupItem pafViewGroupItem : pafViewGroup.getPafViewGroupItems() ) {
				
				if ( pafViewGroupItem.isViewGroup() && pafViewGroupItem.getName().equalsIgnoreCase(oldViewGroupName)) {
					
					pafViewGroupItem.setName(newViewGroupName);
					
				}
								
			}
			
		} 
		
		if ( pafViewGroup.getName().equalsIgnoreCase(oldViewGroupName)) {
			
			pafViewGroup.setName(newViewGroupName);
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.model.ModelManager#remove(java.lang.String)
	 */
	@Override
	public void remove(String key) {
		
		super.remove(key);
		
		deleteViewGroupNameReferences(key);
		
	}
	
	/**
	 * Deletes deletedViewName from all views in all view groups
	 * @param deletedViewName
	 */
	public void deleteViewInViewGroups(String deletedViewName){
		
		String [] stringKeys = getKeys(); 
		
		if( stringKeys == null || deletedViewName == null){
			return;
		}
		
		for (String stringKey : stringKeys) {			
				
				PafViewGroup pafViewGroup = (PafViewGroup) getItem(stringKey);
									
				if ( pafViewGroup != null ) {
					
					ArrayList<PafViewGroupItem> pafViewGroupItemList = new ArrayList<PafViewGroupItem>();
					
					PafViewGroupItem[] pafViewGroupItems = pafViewGroup.getPafViewGroupItems();
					
					if ( pafViewGroupItems != null ) {
						
						for ( PafViewGroupItem pafViewGroupItem : pafViewGroupItems)
							
							if ( !pafViewGroupItem.getName().equalsIgnoreCase(deletedViewName)){
								
								pafViewGroupItemList.add(pafViewGroupItem);
								
							}
								
						
						pafViewGroup.setPafViewGroupItems(pafViewGroupItemList.toArray(new PafViewGroupItem[0]));

					}
					
					replace(stringKey, pafViewGroup);
					
				}		
			
		}

	}
	
	/**
	 * Renames a view within a view group.
	 * @param oldViewName Old view name.
	 * @param newViewName New view name.
	 */
	public void renameViewInViewGroups(String oldViewName, String newViewName){
		
		String [] stringKeys = getKeys(); 
		
		if( stringKeys == null || oldViewName == null){
			return;
		}
		
		for (String stringKey : stringKeys) {			
				
				PafViewGroup pafViewGroup = (PafViewGroup) getItem(stringKey);
									
				if ( pafViewGroup != null ) {
					
					ArrayList<PafViewGroupItem> pafViewGroupItemList = new ArrayList<PafViewGroupItem>();
					
					PafViewGroupItem[] pafViewGroupItems = pafViewGroup.getPafViewGroupItems();
					
					if ( pafViewGroupItems != null ) {
						
						for ( PafViewGroupItem pafViewGroupItem : pafViewGroupItems)
							if ( !pafViewGroupItem.getName().equalsIgnoreCase(oldViewName)){
								pafViewGroupItemList.add(pafViewGroupItem);
								
							} else{
								pafViewGroupItem.setName(newViewName);
								pafViewGroupItemList.add(pafViewGroupItem);
							}
						
						pafViewGroup.setPafViewGroupItems(pafViewGroupItemList.toArray(new PafViewGroupItem[0]));

					}
					replace(stringKey, pafViewGroup);
					
				}		
		}
		save();
		load();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

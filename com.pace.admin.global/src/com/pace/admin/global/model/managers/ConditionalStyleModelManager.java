/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.format.IPaceConditionalStyle;

/**
 * @author ihuang
 *
 */
public class ConditionalStyleModelManager extends ModelManager {

	public ConditionalStyleModelManager(IProject project) {
		super(project);		
		load();
		
	}

	private static Logger logger = Logger.getLogger(ConditionalStyleModelManager.class);
	
	@SuppressWarnings("unchecked")
	public void load() {

		model = new TreeMap<String, IPaceConditionalStyle>();
		
		try {
			model = (TreeMap<String, IPaceConditionalStyle>) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_ConditionalStyles));
			
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_ConditionalStyles);	
		} catch (Exception ex) { logger.error(ex.getMessage()); }

	}
	
	public void save() {

		ACPafXStream.exportObjectToXml(model, getIFileFromProject(PafBaseConstants.FN_ConditionalStyles));

	}
	
	public IPaceConditionalStyle getConditionalStyleByGUID( String guid ) {
		return (IPaceConditionalStyle)model.get(guid);
	}
	
	public String getNameByGUID( String guid ) {
		return getConditionalStyleByGUID(guid).getName();
	}
	
	public String getGUIDByName( String name ) {
		return getConditionalStyleByName(name).getGuid();
	}
	
	public String[] getNames() {
		int i = 0;
		String[] names = new String[model.size()];
		
		for (String guid : getKeys()) {
			IPaceConditionalStyle style = (IPaceConditionalStyle) model.get(guid);
			String name = style.getName();
			names[i++] = name;
		}
		
		return names;
	}
	
	public void addConditionalStyle(IPaceConditionalStyle conditionalStyle) {
		model.put(conditionalStyle.getGuid(), conditionalStyle);
		
	}
	
	public IPaceConditionalStyle getConditionalStyleByName(String styleName) {
		Set<Map.Entry<String, IPaceConditionalStyle>> set = model.entrySet();

	    for (Map.Entry<String, IPaceConditionalStyle> dm : set) {
			
			if ( dm.getValue().getName().equals(styleName)) {
				return dm.getValue();
			} 
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void renameConditionalStyle(String oldName, String newName){
		Iterator<Entry<String,IPaceConditionalStyle>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,IPaceConditionalStyle> entry = it.next();
		    String guid = entry.getKey();
			String name = getNameByGUID(entry.getKey());
			if(name.equalsIgnoreCase(oldName)){
				IPaceConditionalStyle newCondFormat = (IPaceConditionalStyle) entry.getValue().clone();
				newCondFormat.setName(newName);
				model.remove(guid);
				model.put(guid, newCondFormat);
				break;
			}
		}
		
		ConditionalFormatModelManager modelManager = new ConditionalFormatModelManager(project);
		modelManager.renameStyleInConditionalFormats(oldName, newName);
		modelManager.save();
	}

	public IPaceConditionalStyle findDuplicateConditionalStyle( IPaceConditionalStyle condStyle ) {
		Iterator<Entry<String,IPaceConditionalStyle>> it = model.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<String,IPaceConditionalStyle> entry = it.next();
		    IPaceConditionalStyle tmpStyle = (IPaceConditionalStyle)  entry.getValue();
			if ( ! entry.getKey().equals(condStyle.getGuid()) 
					&& tmpStyle.getName().equalsIgnoreCase(condStyle.getName())) {
				return tmpStyle;
			} 
		}
		return null;
	}

}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.rules.RuleSet;

/**
* The RuleSetModelManager handles the model objects for the RuleSet.
* @author kmoos
* @version x.xx
*
*/
public class RuleSetModelManager extends ModelManager {

	private static Logger logger = Logger.getLogger(RuleSetModelManager.class);

	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public RuleSetModelManager(IProject project) {
		super(project);
		this.load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
	//	 create empty model with case insensitive keys
		model = new TreeMap<String, RuleSet>(
				String.CASE_INSENSITIVE_ORDER);
	
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		Collection<RuleSet> ruleSets = null;//PafImportExportUtility.importRuleSets(iConfFolder.getLocation().toString(), true);
		
		XMLPaceProject pp = null;
		try {
			
			pp = new XMLPaceProject(
					iConfFolder.getLocation().toString(), 
					new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.ApplicationDef)),
							false);
			
			ruleSets = pp.getRuleSets().values();
			
		} catch (InvalidPaceProjectInputException e1) {
			
			logger.error("Invalid Pace Project: " + e1.getMessage());
			
		} catch (PaceProjectCreationException e1) {

			logger.error("Problem creating Pace Project: " + e1.getMessage());
		}
		
		
		//try to refresh the directory to one depth
		try {
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		}
		

		/*		
		try {
			ruleSets = (List<RuleSet>) ACPafXStream.importObjectFromXml(
					getIFileFromProject(Constants.PAF_RULES_FILE));
			
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ Constants.PAF_RULES_FILE);
		} catch (Exception ex) {
			logger.error(ex.getMessage()); 
		}
		*/
		
		if (ruleSets != null) {
			for (RuleSet ruleSet : ruleSets) {
				model.put(ruleSet.getName(), ruleSet);
			}
		}
		logger.debug(model);
	}

	/**
	 * Not Implemented.
	 */
	@Override
	public void save() {
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

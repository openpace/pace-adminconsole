/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafPlannerRole;

/**
* The PlannerRoleModelManager handles the model objects for the PafPlannerRole.
* @author kmoos
* @version x.xx
*/
public class PlannerRoleModelManager extends ModelManager {

	private static Logger logger = Logger
		.getLogger(PlannerRoleModelManager.class);
	
	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public PlannerRoleModelManager(IProject project) {
		super(project);
		this.load();
	}

	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		// create empty model with case insensitive keys
		model = new TreeMap<String, PafPlannerRole>(
				String.CASE_INSENSITIVE_ORDER);

		// create an nulled paf view array
		PafPlannerRole[] pafPlannerRoles = null;

		try {
			pafPlannerRoles = (PafPlannerRole[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_RoleMetaData));

			
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_RoleMetaData);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		if (pafPlannerRoles != null) {

			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (PafPlannerRole pafRole : pafPlannerRoles) {
				model.put(pafRole.getRoleName(), pafRole);
			}
		}
		logger.debug(model);
	}

	/**
	 * Saves the model to file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		try{
			PafPlannerRole[] pafPlannerRoles = (PafPlannerRole[]) model.values().toArray(new PafPlannerRole[0]);
			
			ACPafXStream.exportObjectToXml(pafPlannerRoles, getIFileFromProject(PafBaseConstants.FN_RoleMetaData));
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Renames a seasonid inside all the associated PafPlannerRoles.
	 * @param oldSeasonId The old season id.
	 * @param newSeasonId The new season id.
	 */
	@SuppressWarnings("unchecked")
	public void renameSeasonId(String oldSeasonId, String newSeasonId){
		PafPlannerRole[] pafPlannerRoles = (PafPlannerRole[]) model.values().toArray(new PafPlannerRole[0]);
		boolean change = false;
		
		for(PafPlannerRole pafPlannerRole : pafPlannerRoles){
			
			if ( pafPlannerRole.getSeasonIds() != null ) {
				
				String[] seasonIds = pafPlannerRole.getSeasonIds();

				for(int i = 0; i < seasonIds.length; i++){
					if(seasonIds[i].equalsIgnoreCase(oldSeasonId)){
						seasonIds[i] = newSeasonId;
						change = true;
					}
				}
				
				if (change) {
					pafPlannerRole.setSeasonIds(seasonIds);
				}
				
			}
		}
		if(change){
			save();
			load();
		}
	}
	
	/**
	 * Deletes all occurrences of a season id in the PafPlannerRoles.
	 * @param seasonId The season id to remove.
	 */
	@SuppressWarnings("unchecked")
	public void deleteSeasonId(String seasonId){
		PafPlannerRole[] pafPlannerRoles = (PafPlannerRole[]) model.values().toArray(new PafPlannerRole[0]);
				
		for(PafPlannerRole pafPlannerRole : pafPlannerRoles){
			
			ArrayList temp = new ArrayList();
			
			if ( pafPlannerRole.getSeasonIds() != null ) {
				for(int i = 0; i < pafPlannerRole.getSeasonIds().length; i++){
					
					if(! pafPlannerRole.getSeasonIds()[i].equalsIgnoreCase(seasonId)){
						temp.add(pafPlannerRole.getSeasonIds()[i]);
					} 
				}
			}
			
			if ( temp.size() == 0 ) {
			
				pafPlannerRole.setSeasonIds(null);
				
			} else {
				pafPlannerRole.setSeasonIds((String[]) temp.toArray(new String[0]));
			}
			
		}
		
		save();
		
	}
	
	// TTN-2466 - Rename planner role
	/**
	 * Renames planner role.
	 * @param oldPlannerRoleName
	 * @param newPlannerRoleName
	 */
	public void renamePlannerRole(String oldPlannerRoleName, String newPlannerRoleName) {
		PafPlannerRole[] pafPlannerRoles = (PafPlannerRole[]) model.values().toArray(new PafPlannerRole[0]);
		boolean change = false;
		
		for(PafPlannerRole pafPlannerRole : pafPlannerRoles){
			
			if ( pafPlannerRole.getRoleName() != null && pafPlannerRole.getRoleName().equals(oldPlannerRoleName)) {
				change = true;
				pafPlannerRole.setRoleName(newPlannerRoleName);
			}
		}
		if(change){
			save();
		}
		
		save();
		load();
	}
	
	/**
	 * Finds duplicate planner role names in paf planner roles.
	 * @param newRoleName
	 * @return
	 */
	public boolean findDulicatePlannerRoleName(String newRoleName) {
		PafPlannerRole[] pafPlannerRoles = (PafPlannerRole[]) model.values().toArray(new PafPlannerRole[0]);
		for(PafPlannerRole pafPlannerRole : pafPlannerRoles){
			if (pafPlannerRole.getRoleName().equals(newRoleName)) {
				return false;
			}
			
		}
		return true;
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.db.membertags.MemberTagDef;
import com.thoughtworks.xstream.XStream;

/**
 * Manages Member Tags
 *
 * @version 1.00
 * @author jmilliron
 *
 */
public class MemberTagModelManager extends ModelManager {

	private static Logger logger = Logger.getLogger(MemberTagModelManager.class);
	
	/**
	 * 
	 * @param project Current project
	 */
	public MemberTagModelManager(IProject project) {
		
		super(project);
		load();
		
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.model.ModelManager#load()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {

		MemberTagDef[] memberTagDefAr = null;

		//try to import the dynamic member definitions
		try {
			memberTagDefAr = (MemberTagDef[]) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_MemberTagMetaData));
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_MemberTagMetaData);
		} catch (Exception ex) { 
			logger.error(ex.getMessage()); 
		}
		
		//create empty model
		model = new TreeMap<String, MemberTagDef>(String.CASE_INSENSITIVE_ORDER);
		
		//if array is not null, push array values into model
		if ( memberTagDefAr != null ) {
			
			for (MemberTagDef memberTagDef : memberTagDefAr ) {
				
				model.put(memberTagDef.getName(), memberTagDef);
				
			}
			
		}		

	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.model.ModelManager#save()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		
		ACPafXStream.getXStream().setMode(XStream.NO_REFERENCES);
		
		//write to disk
		ACPafXStream.exportObjectToXml(model.values().toArray(new MemberTagDef[0]), getIFileFromProject(PafBaseConstants.FN_MemberTagMetaData));

	}
	
	/**
	 * 
	 *  Creates an array of Member Tags and returns it.
	 * @param haveSorted 			sort member tags by in alpha key order
	 * @return array of Member Tags
	 */
	public MemberTagDef[] getMemberTags(boolean haveSorted ) {
		
		MemberTagDef[] memberTags = null;
		
		if ( model != null && model.size() > 0 ) {
						
			memberTags = new MemberTagDef[model.size()];
			
			int ndx = 0;
			
			String[] keys = null;
			
			if ( haveSorted ) {
				
				keys = getSortedKeys();
				
			} else {
				keys = getKeys();
			}
			
			if ( keys != null ) {
			
				for ( String key : keys ) {
					
					memberTags[ndx++] = (MemberTagDef) getItem(key);
									
				}
				
			}
			
			
		}		
		
		return memberTags;
		
	}
	
	/**
	 * 
	 * Gets a filtered array of member tags.
	 *
	 * @param dimFilterAr
	 * @param haveSorted
	 * @return Array of filtered member tags
	 */
	public MemberTagDef[] getMemberTags(String[] dimFilterAr, boolean haveSorted ) {
		
		MemberTagDef[] memberTagDefs = getMemberTags(haveSorted);
						
		if ( memberTagDefs != null && dimFilterAr != null ) {
		
			List<MemberTagDef> filteredMemberTagList = new ArrayList<MemberTagDef>();
			
			Set<String> dimFilterSet = new HashSet<String>(Arrays.asList(dimFilterAr));
			
			MT_LOOP:
			for ( MemberTagDef memberTagDef : memberTagDefs ) {
				
				if ( memberTagDef.getDims() != null && memberTagDef.getDims().length <= dimFilterSet.size() ) {
					
					for (String memberTagDefDim : memberTagDef.getDims() ) {
						
						if ( ! dimFilterSet.contains(memberTagDefDim) ) {
							
							//contine to next member tag def
							continue MT_LOOP;							
						}						
					}
					
					filteredMemberTagList.add(memberTagDef);
										
				}
				
			}
			
			if ( filteredMemberTagList.size() > 0 ) {
				
				memberTagDefs = filteredMemberTagList.toArray(new MemberTagDef[0]);
				
			} else {
				
				memberTagDefs = null;
				
			}		
			
		}
		
		return memberTagDefs;
		
	}

	@Override
	/**
	 * Removes the member tag references from the view section and them removes member tag from model map.
	 */
	public void remove(String key) {
	
		ViewSectionModelManager viewSectionModelManager = new ViewSectionModelManager(project);		
		
		//remove member tag reference from all view sections
		viewSectionModelManager.removeMemberTagReferences(key);
		
		super.remove(key);
	}


	
	
}

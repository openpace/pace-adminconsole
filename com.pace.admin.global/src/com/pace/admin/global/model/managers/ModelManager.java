/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.base.ui.IPafMapModelManager;

/**
 * @author jmilliron
 * 
 */
public abstract class ModelManager implements IPafMapModelManager {

	protected IProject project;

	protected Map model = null;
	
	private static Logger logger = Logger.getLogger(ModelManager.class);
	
	public ModelManager(IProject project) {
		this.project = project;
	}

	/**
	 * Saves only the items in the list.
	 * @param items items to save.
	 */
	//public abstract void save(ArrayList<String> items);
	public abstract void save();
	public abstract void load();
	
	@SuppressWarnings("unchecked")
	public void add(String key, Object object) {

		model.put(key, object);

	}

	public void remove(String key) {

		if (model.containsKey(key)) {
			logger.debug("Removing key: " + key + " from model");
			model.remove(key);
		} else {
			logger.debug("Can't remove key: " + key
					+ " from model because view does not exists.");
		}

	}
	
	public Set<String> getKeySet() {
		
		Set<String> keySet = new HashSet<String>();
		
		if ( model != null ) {
			
			keySet = model.keySet();
			
		}
		
		return keySet;
		
	}

	public String[] getKeys() {
		
		String[] keys = null;
		
		if ( model != null ) {
			
			keys = new String[model.keySet().size()];
	
			int i = 0;
	
			for (Object key : model.keySet()) {
				keys[i++] = (String) key;
			}

		}
		
		return keys;

	}

	public Object getItem(String key) {

		return model.get(key);

	}

	public int getIndex(String key) {

		int index = 0;

		for (Object keyValue : model.keySet()) {
			if (keyValue.equals(key)) {
				break;
			} else {
				index++;
			}
		}

		return index;

	}

	public int size() {
	
		return model.size();
		
	}

	public boolean contains(String arg0) {
	
		return model.containsKey(arg0);
		
	}
	
	public boolean containsIgnoreCase(String arg0) {
		
		Object[] objAr = model.keySet().toArray();
		
		if ( objAr != null ) {
			
			for (Object obj : objAr) {
				
				if ( obj instanceof String) {
					
					String key = (String) obj;
					
					if ( key.equalsIgnoreCase(arg0)) {
						
						return true;
						
					}
					
				}
				
			}
			
		}
		
		return false;
		
	}
	
	@SuppressWarnings("unchecked")
	public void replace(String key, Object obj) {
		
		if ( model.containsKey(key)) {
			
			model.put(key, obj);
			
		}		
		
	}

	public void setModel(Map model) {
		this.model = model;
	}
	
	public Map getModel() {
		return this.model;
	}
	
	public IFile getIFileFromProject(String fileName) {
		
		IFolder confFolder = project.getFolder(Constants.CONF_DIR);

		return confFolder.getFile(fileName);
		
	}
	
	public String[] getSortedKeys() {
		
		String[] sortedKeys = this.getKeys();
		
		if ( sortedKeys != null ) {
			
			Arrays.sort(sortedKeys);
			
		}
		
		return sortedKeys;
		
	}
	
	public boolean isEmpty() {
				
		boolean isModelEmpty = true;
		
		if ( model != null && model.size() > 0 ) {
			
			isModelEmpty = false;
			
		}		
		
		return isModelEmpty;
		
	}
}

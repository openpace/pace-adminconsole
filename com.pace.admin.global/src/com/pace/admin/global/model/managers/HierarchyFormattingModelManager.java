/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.global.model.managers;


import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.view.HierarchyFormat;

/**
 * The GenerationFormattingModelManager handles the model objects for Hierarchy formatting. 
 *  
 * @author jmilliron
 *
 */
public class HierarchyFormattingModelManager extends ModelManager {

	private static Logger logger = Logger.getLogger(HierarchyFormattingModelManager.class);
	
	/**
	 * Construtor
	 * 
	 * @param projectDir The project directory for the current project
	 */
	public HierarchyFormattingModelManager(IProject project) {
		super(project);
		load();
		
	}
	
	public IFile getInputFile() {
		
		IFolder confFolder = project.getFolder(Constants.CONF_DIR);

		return confFolder.getFile(PafBaseConstants.FN_HierarchyFormats);
		
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
		
	/**
	 * Loads the model from file.
	 */
	@SuppressWarnings("unchecked")
	public void load() {

		logger.info("Loading Hierarchy Formats file");
		model = new TreeMap<String, HierarchyFormat>(String.CASE_INSENSITIVE_ORDER);
		
		try {
			
			model = (Map<String, HierarchyFormat>) ACPafXStream.importObjectFromXml(getInputFile());
			
		} catch (PafConfigFileNotFoundException pex) {
			PafErrHandler.handleException(pex);
			logger.debug("Hierarchy Formats file not found.  Creating an empty model.");
			model = new TreeMap<String, HierarchyFormat>(String.CASE_INSENSITIVE_ORDER);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		logger.info("Successfully loaded Hierarchy Formats file");
		
		
	}
	
	/**
	 * Saves the model to file.
	 */
	public void save() {

		ACPafXStream.exportObjectToXml(model, getInputFile());

	}
		
}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.model.managers;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.view.HierarchyFormat;

/**
 * @author jmilliron
 *
 */
public class GlobalStyleModelManager extends ModelManager {

	public GlobalStyleModelManager(IProject project) {
		super(project);		
		load();
		
	}

	private static Logger logger = Logger.getLogger(GlobalStyleModelManager.class);
	
	@SuppressWarnings("unchecked")
	public void load() {

		model = new TreeMap<String, HierarchyFormat>(String.CASE_INSENSITIVE_ORDER);
		try {
			model = (TreeMap<String, HierarchyFormat>) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_GlobalStyleMetaData));
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_GlobalStyleMetaData);	
			model = new TreeMap<String, HierarchyFormat>(String.CASE_INSENSITIVE_ORDER);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

	}
	
	public void save() {

		ACPafXStream.exportObjectToXml(model, getIFileFromProject(PafBaseConstants.FN_GlobalStyleMetaData));

	}
	
	public String getNextGlobalStyleName() {

		String nextNewStyleName = null;
		int defaultStyleCnt = 1;

		while (true) {
			
			nextNewStyleName = "Global Style " + defaultStyleCnt;
			
			if (! model.containsKey(nextNewStyleName)) {
				break;
			} else {
				defaultStyleCnt++;
			}
		}

		return nextNewStyleName;
	}


	
}
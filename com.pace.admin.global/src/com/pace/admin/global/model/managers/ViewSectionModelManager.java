/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.MemberTagUtil;
import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.base.db.membertags.MemberTagCommentEntry;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.ViewTuple;
import com.pace.base.utility.PafImportExportUtility;
import com.pace.base.utility.PafXStream;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * Manages the view section model to/from xml.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ViewSectionModelManager extends ModelManager {

	private static Logger logger = Logger
			.getLogger(ViewSectionModelManager.class);
		
	public ViewSectionModelManager(IProject project) {
		super(project);
		load();
	}

	/**
	 * Loads the view sections from xml
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
	
		//PafViewSection[] pafViewSections = PafImportExportUtility.importViewSections(iConfFolder.getLocation().toString(), true);
		
		List<PafViewSection> pafViewSections = null;
		
		
		try {
			
			XMLPaceProject pp = new XMLPaceProject(
					iConfFolder.getLocation().toString(), 
					new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.ViewSections)),
							false);
			
			pafViewSections = pp.getViewSections();
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		} catch (InvalidPaceProjectInputException e) {
			
			logger.error("Invalid Pace Project: " + e.getMessage());
			
		} catch (PaceProjectCreationException e) {
			
			logger.error("Invalid Pace Project: " + e.getMessage());
		}
		
		model = new TreeMap<String, PafViewSectionUI>(String.CASE_INSENSITIVE_ORDER);
		
		if ( pafViewSections != null ) {
		
			//populate the model
			for (PafViewSection pafViewSection : pafViewSections ) {
				
				model.put(pafViewSection.getName(), new PafViewSectionUI(pafViewSection));
				
			}			
			
		}		
		
		logger.debug(model);

	}

	
	/**
	 * Saves only the items in the list.
	 * @param items items to save.
	 */
	public void save(ArrayList<String> items) {
				
		String[] keys = (String[]) getKeys();
		
		PafViewSection[] pafViewSections = new PafViewSection[items.size()];
		
		int pafViewSectionNdx = 0;
		
		for (String key : keys) {	
			
			if(items.contains(key)){
			
				PafViewSectionUI pafViewSectionUI = (PafViewSectionUI) model.get(key);
							
				pafViewSections[pafViewSectionNdx++] = pafViewSectionUI.getPafViewSection();
			
			}
			
		}

		PafXStream.setMode(XStream.NO_REFERENCES);

		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		PafImportExportUtility.exportViewSections(pafViewSections, iConfFolder.getLocation().toString(), false);
		
		//try to refresh the directory to one depth
		try {
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		}

	}
	
	
	
	
	/**
	 * Saves the view sections to xml
	 */
	@Override
	public void save() {
				
		String[] keys = (String[]) getKeys();
		
		PafViewSection[] pafViewSections = new PafViewSection[keys.length];
		
		int pafViewSectionNdx = 0;
		
		for (String key : keys) {			
			
			PafViewSectionUI pafViewSectionUI = (PafViewSectionUI) model.get(key);
						
			pafViewSections[pafViewSectionNdx++] = pafViewSectionUI.getPafViewSection();
			
		}

		PafXStream.setMode(XStream.NO_REFERENCES);

		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		PafImportExportUtility.exportViewSections(pafViewSections, iConfFolder.getLocation().toString(), true);
		
		//try to refresh the directory to one depth
		try {
			
			iConfFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		}

	}
	
	/**
	 * 
	 * Update member tag references in all view sections.  Member Tag name references are in 
	 * the Row and Column view tuples, and also in the memberTagCommentNames array.
	 *
	 * @param oldMemberTagName  Old Member Tag name
	 * @param newMemberTagName	New Member Tag name
	 */
	public void updateMemberTagReferences(String oldMemberTagName, String newMemberTagName ) {
		
		String[] viewSectionKeys = getKeys();
		
		if ( viewSectionKeys != null ) {
		
			//loop all view sections
			for ( String viewSectionKey : viewSectionKeys ) {
			
				PafViewSectionUI pafViewSection = (PafViewSectionUI) getItem(viewSectionKey);
				
				//update row view tuples
				pafViewSection.setRowTuples(updateMemberTagReferences(pafViewSection.getRowTuples(), oldMemberTagName, newMemberTagName));
				
				//update col view tuples
				pafViewSection.setColTuples(updateMemberTagReferences(pafViewSection.getColTuples(), oldMemberTagName, newMemberTagName));
				
				/*
				//get current member tag comment names
				String[] memberTagCommentNames = pafViewSection.getMemberTagCommentNames();
				
				//if not null
				if ( memberTagCommentNames != null ) {
					
					//loop and update member tags
					for (int i = 0; i < memberTagCommentNames.length; i++ ) {
						
						if ( memberTagCommentNames[i].equalsIgnoreCase(oldMemberTagName) ) {
							
							memberTagCommentNames[i] = newMemberTagName;
							
						}
						
					}
					
				}
				
				//set with updated member tag comment names
				pafViewSection.setMemberTagCommentNames(memberTagCommentNames);
				*/
				
				MemberTagCommentEntry[] memberTagCommentEntries = pafViewSection.getMemberTagCommentEntries();
				
				if ( memberTagCommentEntries != null ) {				
					
//					loop and update member tags
					for (int i = 0; i < memberTagCommentEntries.length; i++ ) {
						
						if ( memberTagCommentEntries[i].getName().equalsIgnoreCase(oldMemberTagName) ) {
							
							memberTagCommentEntries[i].setName(newMemberTagName);
							
						}
						
					}
					
				}
				
				pafViewSection.setMemberTagCommentEntries(memberTagCommentEntries);
				
				
				//replace existing view section
				replace(viewSectionKey, pafViewSection);
			}		
			
			//save changes
			save();
			
		}	
		
	}
	
	/**
	 * 
	 * Removes member tag references from the comment and tuple member tags.
	 *
	 * @param memberTagName - name of member tag to remove
	 */
	public void removeMemberTagReferences(String memberTagName) {
		
		if ( memberTagName != null ) {
			
			String[] keys = getKeys();
			
			if ( keys != null && keys.length > 0 ) {
								 				
				for (String key : keys ) {
				
					PafViewSectionUI viewSection = (PafViewSectionUI) this.getItem(key);
					
					if ( viewSection != null ) {
					
						//update comment member tags
						MemberTagCommentEntry[] commentMemberTagEntries = viewSection.getMemberTagCommentEntries();
						
						//if comment member tag names exists
						if ( commentMemberTagEntries != null ) {
						
							//create empty list
							List<MemberTagCommentEntry> validCommentMemberTagNameList = new ArrayList<MemberTagCommentEntry>();
							
							//loop comment member tags
							for ( MemberTagCommentEntry commentMemberTagEntry : commentMemberTagEntries ) {
								
								//add to valid comment member tag list if member tag doesn't equal member tag being removed
								if ( commentMemberTagEntry != null && ! commentMemberTagEntry.getName().equalsIgnoreCase(memberTagName)) {
									
									validCommentMemberTagNameList.add(commentMemberTagEntry);
									
								}
								
							}
							
							//if valid comment member tags exists, convert list to array
							if ( validCommentMemberTagNameList.size() > 0 ) {
								
								viewSection.setMemberTagCommentEntries(validCommentMemberTagNameList.toArray(new MemberTagCommentEntry[0]));
								
							} else {
								
								viewSection.setMemberTagCommentEntries(null);
								
							}
							
						}						
												
						//update member tags on view tuples
						
						viewSection.setRowTuples(MemberTagUtil.removeMemberTagTuples(viewSection.getRowTuples(), memberTagName));
						viewSection.setColTuples(MemberTagUtil.removeMemberTagTuples(viewSection.getColTuples(), memberTagName));
																	
					}	
					
				}			
				
			}	
			
			//save changes
			save();
			
		}
		
	}
	
	/**
	 * 
	 * Updates teh member Tag references given an array of view tuples.
	 *
	 * @param viewTuples		View tuples to update
	 * @param oldMemberTagName  Old Member Tag name
	 * @param newMemberTagName	New Member Tag name
	 * @return	The updated view tuples
	 */
	private ViewTuple[] updateMemberTagReferences(ViewTuple[] viewTuples, String oldMemberTagName, String newMemberTagName) {

		if ( viewTuples != null ) {
		
			for ( ViewTuple viewTuple : viewTuples ) {
				
				//verify its a member tag tuple
				if ( viewTuple.isMemberTag() ) {
					
					if ( viewTuple.getMemberDefs() != null ) {
						
						//loop member tag defs and update all referneces
						for (int i = 0; i < viewTuple.getMemberDefs().length; i++ ) {
							
							if ( viewTuple.getMemberDefs()[i].equalsIgnoreCase(oldMemberTagName)) {
								
								viewTuple.getMemberDefs()[i] = newMemberTagName;
								
							}
							
						}
						
					}
					
				}
				
			}
		
		}
		
		return viewTuples;
	}
	
	
	public String[] getInvalidMemberTagTupleViewSectionNames(MemberTagDef memberTagDef) {
		
		Set<String> invalidViewSectionNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		String[] keys = getKeys();
		
		if ( keys != null && keys.length > 0 ) {
			
			 PafAxis[] pafAxiss = new PafAxis[] { new PafAxis(PafAxis.ROW), new PafAxis(PafAxis.COL) };
			
			for (String key : keys ) {
			
				PafViewSectionUI viewSection = (PafViewSectionUI) this.getItem(key);
				
				if ( viewSection != null ) {
				
					for (PafAxis pafAxis : pafAxiss) {
					
						String[] memberTagNamesInTuples = viewSection.getMemberTagNamesFromViewTuples(pafAxis);
						
						if ( memberTagNamesInTuples != null ) {
							
							for (String memberTagName : memberTagNamesInTuples ) {
								
								if ( memberTagDef.getName().equalsIgnoreCase(memberTagName)) {
									
									if ( ! MemberTagUtil.isMemberTagValidForAxis(viewSection, memberTagDef, pafAxis) ) {

										invalidViewSectionNameSet.add(viewSection.getName());
										
									}
									
								}							
								
							}
							
						}
						
						
					}
										
				}	
				
			}			
			
		}		
		
		if ( invalidViewSectionNameSet.size() > 0 ) {
			return invalidViewSectionNameSet.toArray(new String[0]);
		} else {
			return null;
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public void renameViewSection(String oldViewSectionName, String newViewSectionName) {
		
		Object[] objectKeys = model.keySet().toArray(); 
		ArrayList<String> items = new ArrayList<String>();
		boolean wasChanged = false;
		
		for (Object objKey : objectKeys) {			
			
			if ( objKey instanceof String ) {
				
				String key = (String) objKey;
				
				if ( key.equalsIgnoreCase(oldViewSectionName) ) {
					
					PafViewSectionUI pafViewSection = (PafViewSectionUI) model.get(key);
					
					pafViewSection.setName(newViewSectionName);
					
					//remove old object from map and put the new one in.
					remove(key);
					model.put(pafViewSection.getName(), pafViewSection);
					if(!items.contains(pafViewSection.getName())){
						items.add(pafViewSection.getName());
					}
					wasChanged = true;
				} 
			}		
			
		}
		if(wasChanged){
			save(items);
			load();
		}
	}
	
	
	@Override
	public void remove(String key) {

		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		super.remove(key);
		PafImportExportUtility.deleteViewSections(new String[] {key}, iConfFolder.getLocation().toString());
		
	}
	
	
	
	
	
	

}

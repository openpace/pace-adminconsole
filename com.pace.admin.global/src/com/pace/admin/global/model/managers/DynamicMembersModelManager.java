/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.DynamicMemberDef;

/**
 * Manages dynamic members model.
 *
 * @version	1.00
 * @author jmilliron
 *
 */
public class DynamicMembersModelManager extends ModelManager {

	private static Logger logger = Logger.getLogger(DynamicMembersModelManager.class); 
	
	public DynamicMembersModelManager(IProject project) {
		super(project);
		load();
	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.model.ModelManager#load()
	 */
	@Override
	public void load() {

		DynamicMemberDef[] dynamicMemberDefAr = null;

		//try to import the dynamic member definitions
		try {
			dynamicMemberDefAr = (DynamicMemberDef[]) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_DynamicMembers));
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_DynamicMembers);
		} catch (Exception ex) { 
			logger.error(ex.getMessage()); 
		}
		
		//create empty model
		model = new TreeMap<String, DynamicMemberDef>(String.CASE_INSENSITIVE_ORDER);
		
		//if array is not null, push array values into model
		if ( dynamicMemberDefAr != null ) {
			
			for (DynamicMemberDef dynamicMemberDef : dynamicMemberDefAr ) {
				
				model.put(dynamicMemberDef.getDimension(), dynamicMemberDef);
				
			}
			
		}		

	}

	/* (non-Javadoc)
	 * @see com.pace.admin.menu.model.ModelManager#save()
	 */
	@Override
	public void save() {
				
		//write to disk
		ACPafXStream.exportObjectToXml(model.values().toArray(new DynamicMemberDef[0]), getIFileFromProject(PafBaseConstants.FN_DynamicMembers));

	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.StringUtil;
import com.pace.base.ui.PrintStyle;
import com.pace.base.ui.PrintStyles;

public class PrintStylesManager extends PrintStyles {
	private static Logger logger = Logger.getLogger(PrintStylesManager.class);
	
	public PrintStylesManager(IProject project) {
		super();
		if( project != null ) {
			this.projectFolder = project.getFolder(Constants.CONF_DIR).getLocation().toString();
			load();
		}
	}
	
	public PrintStyle getPrintStyleByName( String name ) {
		name = StringUtil.removeDefaultMarker(name);
		
		return super.getPrintStyleByName(name);
	}

	public String[] getNames( boolean withDefaultMarker ) {
		int i = 0;
		String[] names = new String[printStyles.size()];
		
		for (String guid : getKeys()) {
			PrintStyle style = (PrintStyle) printStyles.get(guid);
			String name = style.getName();
			if (style.getDefaultStyle() && withDefaultMarker ) {
				name += Constants.DEFAULT_MARKER;
			}
			names[i++] = name;
		}
		
		return names;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.VersionDef;
import com.pace.base.app.VersionType;

public class VersionDefModelManager extends ModelManager {

	private static Logger logger = Logger
		.getLogger(VersionDefModelManager.class);
	
	private Collection<VersionType> filter;
	
	public VersionDefModelManager(IProject project) {
		super(project);
		this.filter = null;
		load();
	}

	public VersionDefModelManager(IProject project, boolean filter) {
		super(project);
		this.filter = Arrays.asList(VersionType.ForwardPlannable, VersionType.Plannable);
		load();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		
		model = new TreeMap<String, VersionDef>(
				String.CASE_INSENSITIVE_ORDER);
		
		//create an nulled VersionDef array
		VersionDef[] versionDefs = null;

		try {
			versionDefs = (VersionDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_VersionMetaData));

			
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_VersionMetaData);
		} catch (Exception ex) { logger.error(ex.getMessage()); }
		
		// if there are paf views...
		if (versionDefs != null) {
			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (VersionDef version : versionDefs) {
				if(filter == null){
					model.put(version.getName(), version);
				}
				else{
					if (filter.contains(version.getType())){
						model.put(version.getName(), version);
					}
				}
			}
		}
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

}

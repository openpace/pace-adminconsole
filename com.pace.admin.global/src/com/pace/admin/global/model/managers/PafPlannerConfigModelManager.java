/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.comm.PafPlannerConfig;

/**
* The PafPlannerConfigModelManager handles the model objects for the PafPlannerConfig.
* @author kmoos
* @version x.xx
*
*/
public class PafPlannerConfigModelManager extends ModelManager {

	
	private static Logger logger = Logger.getLogger(PafPlannerConfigModelManager.class);
	/**
	 */
	public static final String NO_CYCLE = "No Cycle";
	
	public static final String NO_ROLE = "No Role";
	
	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public PafPlannerConfigModelManager(IProject project) {
		super(project);
		this.load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
		model = new TreeMap<String, PafPlannerConfig>(
				String.CASE_INSENSITIVE_ORDER);
		
		//create an nulled VersionDef array
		PafPlannerConfig[] pafPlannerConfigs = null;

		try {
			pafPlannerConfigs = (PafPlannerConfig[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_PlannerConfigs));
		} catch (PafConfigFileNotFoundException e) {
			logger.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_PlannerConfigs);
		} catch (Exception ex) { 
			logger.error(ex.getMessage()); 
		}
		
		// if there are paf views...
		if (pafPlannerConfigs != null) {
			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (PafPlannerConfig pafPlannerConfig : pafPlannerConfigs) {
				
				model.put(getKey(pafPlannerConfig.getRole(), pafPlannerConfig.getCycle()), pafPlannerConfig);
				
			}
		}
		logger.debug(model);
	}
	
	/**
	 * 
	 *  Generates key based on role and plan cycle
	 *
	 * @param role
	 * @param planCycle
	 * @return
	 */
	public String getKey(String role, String planCycle) {
		
		String key = null;
		
		if(role != null && role.trim().length() > 0 && planCycle != null && planCycle.trim().length() > 0){
			key = role + " - " + planCycle;
		} else if ( ( role == null || role.trim().length() == 0 )&& ( planCycle == null || planCycle.trim().length() == 0 )){
			key = NO_ROLE + " - " + NO_CYCLE; 
		} else if ( role != null && role.trim().length() > 0 ) {
			key = role + " - " + NO_CYCLE;
		} else {
			key = NO_ROLE + " - " + planCycle;
		}
				
		return key;
	}

	/**
	 * Saves the model to file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		try {
			PafPlannerConfig[] pafPlannerConfig = (PafPlannerConfig[]) model.values().toArray(new PafPlannerConfig[0]);
			
			ACPafXStream.exportObjectToXml(pafPlannerConfig , getIFileFromProject(PafBaseConstants.FN_PlannerConfigs));
		} catch (Exception ex) { 
			logger.error(ex.getMessage()); 
		}
	}
	
	/**
	 * Change all occurances of a plan cycle name to a new name in the PafPlannerConfigs.
	 * @param oldPlanCycleName The old plan cycle name.
	 * @param newPlanCycleName The new plan cycle name.
	 */
	@SuppressWarnings("unchecked")
	public void renamePlanCycle(String oldPlanCycleName, String newPlanCycleName){
		PafPlannerConfig[] pafPlannerConfigs = (PafPlannerConfig[]) model.values().toArray(new PafPlannerConfig[0]);
		boolean change = false;
		
		for(PafPlannerConfig pafPlannerConfig : pafPlannerConfigs){
			if(pafPlannerConfig.getCycle() != null){
				if(pafPlannerConfig.getCycle().equalsIgnoreCase(oldPlanCycleName)){
					pafPlannerConfig.setCycle(newPlanCycleName);
					change = true;
				}
			}
		}
		if(change){
			save();
		}
	}
	
	/**
	 * Change all occurances of a role name to a new name in the PafPlannerConfigs.
	 * @param oldRoleName The old role name.
	 * @param newRoleName The new role name.
	 */
	@SuppressWarnings("unchecked")
	public void renameRoleName(String oldRoleName, String newRoleName){
		PafPlannerConfig[] pafPlannerConfigs = (PafPlannerConfig[]) model.values().toArray(new PafPlannerConfig[0]);
		boolean change = false;
		
		for(PafPlannerConfig pafPlannerConfig : pafPlannerConfigs){
			if(pafPlannerConfig.getRole() != null){
				if(pafPlannerConfig.getRole().equalsIgnoreCase(oldRoleName)){
					pafPlannerConfig.setRole(newRoleName);
					change = true;
				}
			}
		}
		if(change){
			save();
		}
	}
	
	/**
	 * Removes any PafPlannerCOnfigs that have the role name.
	 * @param roleName The role name to remove.
	 */
	@SuppressWarnings("unchecked")
	public void removeRoles(String roleName){
		PafPlannerConfig[] pafPlannerConfigs = (PafPlannerConfig[]) model.values().toArray(new PafPlannerConfig[0]);
		boolean change = false;
		
		for(PafPlannerConfig pafPlannerConfig : pafPlannerConfigs){
			if(pafPlannerConfig.getRole() != null){
				if(pafPlannerConfig.getRole().equalsIgnoreCase(roleName)){
					if(pafPlannerConfig.getCycle() != null){
						model.remove(pafPlannerConfig.getRole() + " - " + pafPlannerConfig.getCycle());
					} else{
						model.remove(pafPlannerConfig.getRole() + " - No Cycle");
					}
					change = true;
				}
			}
		}
		if(change){
			save();
		}
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	/**
	 * Deletes deletedViewName from all role configurations
	 * @param deletedViewName
	 */
	public void deleteViewInRoleConfigurations(String deletedViewName){
		
		// Get the keys from PafPlannerConfigModelManager
		
		String [] keysInPpcmm = getKeys();
		
		for (int i = 0; i < keysInPpcmm.length; i++) {
			
			// Get the item using a key and  cast it to PafPlannerConfig
			
			PafPlannerConfig tempPPC = ( PafPlannerConfig ) getItem(keysInPpcmm[i]);
			
			//Get the list of selected view names
			
			String [] viewsInTree = tempPPC.getViewTreeItemNames();
			
			//Put the views in an ArrayList if they are not the one being removed
			
			ArrayList<String> listOfViewsInTree = new ArrayList<String>();
			
			for (int j = 0; j < viewsInTree.length; j++) {
				
				if(!deletedViewName.equalsIgnoreCase(viewsInTree[j])){
					
					listOfViewsInTree.add(viewsInTree[j]);
					
				}
				
			}
			
			// Convert the list into an array and set the selected views to this array
			
			viewsInTree = listOfViewsInTree.toArray(new String[0]);
			
			tempPPC.setViewTreeItemNames(viewsInTree);
			
		}
		
	}
	
	
	/**
	 * Renames all occurrences of a view or view group with a paf_planner_conf
	 * @param oldName Old view or view group name.
	 * @param newName New view or view group name.
	 */
	public void renameViewOrViewGroupInRoleConfigurations(String oldName,
			String newName){
		
		// Get the keys from PafPlannerConfigModelManager
		
		String [] keysInPpcmm = getKeys();
		boolean changeOccured = false;
		
		for (int i = 0; i < keysInPpcmm.length; i++) {
			
			// Get the item using a key and  cast it to PafPlannerConfig
			
			PafPlannerConfig tempPPC = ( PafPlannerConfig ) getItem(keysInPpcmm[i]);
			
			//Get the list of selected view names
			
			String [] viewsInTree = tempPPC.getViewTreeItemNames();
			
			//Put the views in an ArrayList if they are not the one being removed
			
			ArrayList<String> listOfViewsInTree = new ArrayList<String>();
			
			for (int j = 0; j < viewsInTree.length; j++) {
				
				if(!oldName.equalsIgnoreCase(viewsInTree[j])){
					listOfViewsInTree.add(viewsInTree[j]);
				} else{
					listOfViewsInTree.add(newName);
					changeOccured = true;
				}
				
			}
			
			// Convert the list into an array and set the selected views to this array
			
			viewsInTree = listOfViewsInTree.toArray(new String[0]);
			
			tempPPC.setViewTreeItemNames(viewsInTree);
			
		}
		if(changeOccured){
			save();
		}
	}
	
	public List<String> findRoleConfigsByAdminLock(String adminLockName) {
		List<String> adminLockRoleConfigs = new ArrayList<String>();
		String [] keys = getKeys();
		for (int i = 0; i < keys.length; i++) {
			// Get the item using a key and  cast it to PafPlannerConfig
			PafPlannerConfig roleConfig = ( PafPlannerConfig ) getItem(keys[i]);
			if( roleConfig.getAdminPersistLockNames() != null && roleConfig.getAdminPersistLockNames().length > 0 ) {
				List<String> roleAdminLocks = Arrays.asList(roleConfig.getAdminPersistLockNames());
				if( roleAdminLocks != null && roleAdminLocks.contains(adminLockName)) {
					adminLockRoleConfigs.add(keys[i]);
				}
			}
		}
		
		return adminLockRoleConfigs;
	}

	@SuppressWarnings("unchecked")
	public PafPlannerConfig findConfigRoleByKey(String roleConfigName){
		String[] roleConfig = roleConfigName.split(" - ");
		PafPlannerConfig[] pafPlannerConfigs = (PafPlannerConfig[]) model.values().toArray(new PafPlannerConfig[0]);
		for(PafPlannerConfig pafPlannerConfig : pafPlannerConfigs){
			if(pafPlannerConfig.getRole() != null){
				if(pafPlannerConfig.getRole().equalsIgnoreCase(roleConfig[0])){
					if( ! roleConfig[1].equals(NO_CYCLE) ) {
						if( pafPlannerConfig.getCycle() != null &&  pafPlannerConfig.getCycle().equals(roleConfig[1])) {
							return pafPlannerConfig;
						}
					}
					else {
						if( pafPlannerConfig.getCycle() == null || pafPlannerConfig.getCycle().trim().isEmpty() ) {
							return pafPlannerConfig;
						}
					}
				}
			}
		}
		return null;
	}

	public void addAdminLockToRoleConfig(String roleConfigName, String adminLockName) {
		PafPlannerConfig roleConfig = findConfigRoleByKey(roleConfigName);
		if( roleConfig != null ) {
			if( roleConfig.getAdminPersistLockNames() == null || roleConfig.getAdminPersistLockNames().length == 0 ) {
				roleConfig.setAdminPersistLockNames(new String[] {adminLockName});
			}
			else {
				List<String> roleAdminLocks = new ArrayList<String>(Arrays.asList(roleConfig.getAdminPersistLockNames()));
				if( roleAdminLocks == null ||
						( roleAdminLocks != null 
						&& ( roleAdminLocks.size() == 0 || ! roleAdminLocks.contains(adminLockName))) ) {
					roleAdminLocks.add(adminLockName);
					roleConfig.setAdminPersistLockNames(roleAdminLocks.toArray(new String[0]));
				}
			}
		}
	}
	
	public Set<String> getAdminLocksFromRoleConfig(String roleConfigName) {
		PafPlannerConfig roleConfig = findConfigRoleByKey(roleConfigName);
		Set<String> roleAdminLocks = null;
		if( roleConfig != null ) {
			if( roleConfig.getAdminPersistLockNames() != null && roleConfig.getAdminPersistLockNames().length > 0 ) {
				roleAdminLocks = new HashSet<String>(Arrays.asList(roleConfig.getAdminPersistLockNames()));
			}
		}
		return roleAdminLocks;
	}
	
	public void removeAdminLockFromRoleConfig(String roleConfigName, String adminLockName) {
		PafPlannerConfig roleConfig = findConfigRoleByKey(roleConfigName);
		if( roleConfig != null 
				&& roleConfig.getAdminPersistLockNames() != null 
				&& roleConfig.getAdminPersistLockNames().length > 0 ) {
			List<String> roleAdminLocks = new ArrayList<String>(Arrays.asList(roleConfig.getAdminPersistLockNames()));
			if( roleAdminLocks != null 
					&& roleAdminLocks.size() > 0 && roleAdminLocks.contains(adminLockName) ) {
				roleAdminLocks.remove(adminLockName);
				roleConfig.setAdminPersistLockNames(roleAdminLocks.toArray(new String[0]));
			}
		}
	}
	
	public void renameAdminLockInRoleConfig(String roleConfigName, String oldAdminLockName, String newAdminLockName) {
		PafPlannerConfig roleConfig = findConfigRoleByKey(roleConfigName);
		if( roleConfig != null 
				&& roleConfig.getAdminPersistLockNames() != null 
				&& roleConfig.getAdminPersistLockNames().length > 0 ) {
			List<String> roleAdminLocks = new ArrayList<String>(Arrays.asList(roleConfig.getAdminPersistLockNames()));
			if( roleAdminLocks != null 
					&& roleAdminLocks.size() > 0 && roleAdminLocks.contains(oldAdminLockName) ) {
				roleAdminLocks.remove(oldAdminLockName);
				roleAdminLocks.add(newAdminLockName);
				roleConfig.setAdminPersistLockNames(roleAdminLocks.toArray(new String[0]));
			}
		}
	}

	public void removeAdminLockInRoleConfiguraitons(String adminLockName) {
		Iterator<PafPlannerConfig> it = model.values().iterator();
		while (it.hasNext()) {
			PafPlannerConfig roleConfig = it.next();
			if( roleConfig != null ) {
				String[] adminLocks = roleConfig.getAdminPersistLockNames();
				if( adminLocks != null && adminLocks.length > 0 ) {
				List<String> adminLockList = new ArrayList<String>(Arrays.asList(adminLocks));
					for( String adminLock : adminLocks ) {
						if( adminLock.equals(adminLockName) ) {
							adminLockList.remove(adminLockName);
							roleConfig.setAdminPersistLockNames(adminLockList.toArray(new String[0]));
						}
					}
				}
			}
		}
		save();
	}
}

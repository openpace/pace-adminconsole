/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.model.managers;

import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.StringUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.ui.PafAdminConsoleView;
import com.pace.base.view.PafNumberFormat;

/**
 * @author jmilliron
 *
 */
public class NumericFormatModelManager extends ModelManager {

	private static String[] defaultNumericFormatsAr;
	
	public NumericFormatModelManager(IProject project) {
		super(project);
		load();
	}

	private static Logger logger = Logger.getLogger(NumericFormatModelManager.class);
	
	public String[] getKeys() {
		
		int i = 0;
		
		String[] names = super.getKeys();
		
		for (String name : names ) {
			
			PafNumberFormat format = (PafNumberFormat) model.get(name);
			
			if (format.isDefaultFormat()) {
				name += Constants.DEFAULT_MARKER;
			}
			names[i++] = name;
		}
		
		return names;
	}
	
	public PafNumberFormat getItem(String key) {
		
		PafNumberFormat format = null;
		
		key = StringUtil.removeDefaultMarker(key);
		
		if (model.containsKey(key)) {
			format = (PafNumberFormat) model.get(key);
		}
		
		return format;
	}
	
	@SuppressWarnings("unchecked")
	public void add(String key, Object format) {

		//add new format 
		if (model != null && key != null && key.length() > 0
				&& format != null && ((PafNumberFormat) format).getPattern().length() > 0) {

			//if this is new default format
			if (((PafNumberFormat)format).isDefaultFormat()) {
				removeDefaultFormat(key);
			}
			
			model.put(key, format);
		}

	}

	
	@SuppressWarnings("unchecked")
	public void load() {

		model = new TreeMap<String, PafNumberFormat>(String.CASE_INSENSITIVE_ORDER);
		
		try {
			model  = (TreeMap<String, PafNumberFormat>) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_NumericFormatsMetaData));
			
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_NumericFormatsMetaData);
			model = new TreeMap<String, PafNumberFormat>(String.CASE_INSENSITIVE_ORDER);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

	}
	
	public void save() {

		ACPafXStream.exportObjectToXml(model, getIFileFromProject(PafBaseConstants.FN_NumericFormatsMetaData));

	}
	
	public String getNextDefaultFormatName() {

		String nextNewFormatName = null;
		int defaultFormatCnt = 1;

		while (true) {
			nextNewFormatName = "Numeric Format " + defaultFormatCnt;
			if (!model.containsKey(nextNewFormatName)) {
				break;
			} else {
				defaultFormatCnt++;
			}
		}

		return nextNewFormatName;
	}

	//list of default numeric formats
	public String[] defaultNumericFormats() {
		
		if ( defaultNumericFormatsAr == null ) {
			
			TreeSet<String> defaultNumericFormats = new TreeSet<String>();
			
			defaultNumericFormats.add("###%");
			defaultNumericFormats.add("###.0%");
			defaultNumericFormats.add("###.00%");
			defaultNumericFormats.add("#,##0");
			defaultNumericFormats.add("#,##0.0");
			defaultNumericFormats.add("#,##0.00");
			defaultNumericFormats.add("#,##0.000");
			defaultNumericFormats.add("$ #,##0");
			defaultNumericFormats.add("$ #,##0.0");
			defaultNumericFormats.add("$ #,##0.00");
			defaultNumericFormats.add("#");
			defaultNumericFormats.add("$#");
			
			defaultNumericFormatsAr = new String[defaultNumericFormats.size()];
			
			defaultNumericFormats.toArray(defaultNumericFormatsAr);
			
		}
		
		return defaultNumericFormatsAr;
		
	}
	
	public void setDefaultFormat(String key, boolean setDefault) {
		
		if (model != null && model.containsKey(key)	&& setDefault) {

			//unset default format
			removeDefaultFormat(key);
			
			//set new default
			((PafNumberFormat) model.get(key)).setDefaultFormat(true);			
			
		//if not setting default, just turn it off
		} else if (! setDefault ) {
			
			//unset default format
			removeDefaultFormat(key);
			
		}
		
		
	}
	
	private void removeDefaultFormat(String key) {
		
		//disable old default
		for (String keyValue : super.getKeys()) {
			
			if ( ((PafNumberFormat) model.get(keyValue)).isDefaultFormat() ) {
				
				((PafNumberFormat) model.get(keyValue)).setDefaultFormat(false);
				break;
			} 
			
		}
		
	}
	
	public String[] getSimpleKeys() {
		
		return super.getKeys();
		
	} 

	
}
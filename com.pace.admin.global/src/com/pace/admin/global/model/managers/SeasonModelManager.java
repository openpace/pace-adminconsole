/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.Season;
import com.pace.base.app.SeasonList;

/**
* The SeasonModelManager handles the model objects for the seasons.
* @author kmoos
* @version x.xx
*
*/
public class SeasonModelManager extends ModelManager {

	private static Logger logger = Logger
		.getLogger(SeasonModelManager.class);

	/**
	 * Constructor.
	 * @param project Current IProject.
	 */
	public SeasonModelManager(IProject project) {
		super(project);
		load();
	}
	
	/**
	 * Loads model objects.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void load() {
//		 create empty model with case insensitive keys
		model = new TreeMap<String, SeasonList>(
				String.CASE_INSENSITIVE_ORDER);

		// create an nulled paf view array
		SeasonList seasonList = null;

		try {
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));

			seasonList = appDef[0].getSeasonList();
			
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_ApplicationMetaData);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		if (seasonList != null) {

			// loop through them can create PafAdminConsoleViews and put them in
			// the model
			for (Object obj : seasonList.getSeasons()) {
				Season season = (Season) obj;
				model.put(season.getId(), season);
			}
		}
		logger.debug(model);
	}

	/**
	 * Saves the model to file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		try{
			//reread the entire file
			PafApplicationDef[] appDef = (PafApplicationDef[]) ACPafXStream.importObjectFromXml(
					getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
			
			SeasonList newSeasonList = new SeasonList();
			Season[] seasons = (Season[]) model.values().toArray(new Season[0]);
			
			for(Season season : seasons){
				newSeasonList.addSeason(season);
			}
			//set the new season list.
			appDef[0].setSeasonList(newSeasonList);
			//serialize the xml.
			ACPafXStream.exportObjectToXml(appDef, getIFileFromProject(PafBaseConstants.FN_ApplicationMetaData));
				
		} catch(Exception e){
			logger.error(e.getLocalizedMessage());
		}
	}

	/**
	 * Renames all the plan cycles in the array of seasons.
	 * @param oldPlanCycleName The old plan cycle name.
	 * @param newPlanCycleName The new plan cycle name.
	 */
	@SuppressWarnings("unchecked")
	public void renamePlanCycle(String oldPlanCycleName, String newPlanCycleName){
		Season[] seasons = (Season[]) model.values().toArray(new Season[0]);
		boolean change = false;
		
		for(Season season : seasons){
			if(season.getPlanCycle().equalsIgnoreCase(oldPlanCycleName)){
				season.setPlanCycle(newPlanCycleName);
				change = true;
			}
		}
		if(change){
			save();
			load();
		}
	}
	
	/**
	 * Deletes all seasons associated with the given plan cycle
	 * TTN-760
	 * @param deletedPlanCycle
	 */
	public void deletePlanCycle(String deletedPlanCycle){
		
		boolean deleted = false;
		
		// Get all keys from the model
		Object [] objectKeys = model.keySet().toArray();
		
		// Convert the keys to strings
		List<String> keys = new ArrayList<String>();
		
		for(Object objectKey : objectKeys){
			
			keys.add((String) objectKey);
			
		}
		
		// If a season contains the key remove the season from the model
		for(String key : keys){
			
			if(model.containsKey(key)){
				
				Season season = (Season) model.get(key);
				
				String planCycle = season.getPlanCycle();
				
				if(planCycle.equalsIgnoreCase(deletedPlanCycle)){
					
					model.remove(key);
					
					deleted = true;
					
				}
			}
		}
		if(deleted){
			
			save();
			
			load();
		}
	}
	
	/**
	 * Renames all the season processes in the array of seasons.
	 * @param oldSeasonProcessName The old season process name.
	 * @param newSeasonProcessName The new season process name.
	 */
	@SuppressWarnings("unchecked")
	public void renameSeason(String oldSeasonProcessName, String newSeasonProcessName){
		Season[] seasons = (Season[]) model.values().toArray(new Season[0]);
		boolean change = false;
		
		for(Season season : seasons){
			if(season.getId().equalsIgnoreCase(oldSeasonProcessName)){
				season.setId(newSeasonProcessName);
				change = true;
			}
		}
		if(change){
			save();
			load();
		}
	}
	
	/**
	 * Find duplicates in array of seasons
	 * @param newSeasonName
	 * @return
	 */
	public boolean findDulicateSeasonName(String newSeasonName) {
		Season[] seasons = (Season[]) model.values().toArray(new Season[0]);
		for (Season season : seasons) {
			if (season.getId().equals(newSeasonName)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model.managers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.view.PafUserSelection;

public class UserSelectionModelManager extends ModelManager {

	private static Logger logger = Logger
			.getLogger(UserSelectionModelManager.class);

	public UserSelectionModelManager(IProject project) {
		super(project);
		load();

	}

	@Override
	public void save() {

		PafUserSelection[] pafUserSelections = null;
		
		if ( model.size() > 0 ) {
			
			pafUserSelections = new PafUserSelection[model.size()];
			
			int index = 0;
			
			for (Object key : model.keySet() ) {
				
				PafUserSelection pafUserSelection = (PafUserSelection) model.get((String) key);
				
				pafUserSelections[index++] = pafUserSelection;
				
			}
			
		} else {
			
			pafUserSelections = new PafUserSelection[0];
			
		}
		
		ACPafXStream.exportObjectToXml(pafUserSelections, getIFileFromProject(PafBaseConstants.FN_UserSelections));

	}

	@SuppressWarnings("unchecked")
	@Override
	public void load() {

		PafUserSelection[] pafUserSelections = null;

		try {
			pafUserSelections = (PafUserSelection[]) ACPafXStream
					.importObjectFromXml(getIFileFromProject(PafBaseConstants.FN_UserSelections));
		} catch (PafConfigFileNotFoundException e) {
			logger
					.debug("PafConfigFileNotFoundException thrown, new file is being created at: "
							+ PafBaseConstants.FN_UserSelections);
		} catch (Exception ex) { logger.error(ex.getMessage()); }

		model = new TreeMap<String, PafUserSelection>(String.CASE_INSENSITIVE_ORDER);

		if (pafUserSelections != null) {

			for (PafUserSelection pafUserSelection : pafUserSelections) {

				model.put(pafUserSelection.getId(), pafUserSelection);

			}

		} 

	}
		
	public Map<String, PafUserSelection[]> getDimensionMap() {
		
		Map<String, PafUserSelection[]> dimensionMap = new HashMap<String, PafUserSelection[]>();
		Map<String, Set<PafUserSelection>> internalDimensionMap = new HashMap<String, Set<PafUserSelection>>();
		
		//load model
		load();
		
		
		//for each key in model
		for (Object objKey : model.keySet() ) {
			
			String key = (String) objKey;
			
			//get paf user selection
			PafUserSelection pafUserSelection = (PafUserSelection) model.get(key);
			
			//name of dimension
			String dimension = pafUserSelection.getDimension();
			
			//create a sub dimension set that will be used to hold the user selections per dim
			Set<PafUserSelection> subDimSet = internalDimensionMap.get(dimension);
			
			//if null, create an empty set
			if ( subDimSet == null ) {
				subDimSet = new TreeSet<PafUserSelection>();
			}
						
			//add user selectioni
			subDimSet.add(pafUserSelection);
		
			//put updated set into dimension map
			internalDimensionMap.put(dimension, subDimSet);			
			
		}
		
		//for each dimension with a user selection
		for (String dimension : internalDimensionMap.keySet()) {
			
			Set<PafUserSelection> subDimSet = internalDimensionMap.get(dimension);
			
			//if sum dimension set exist, add to map with user selecitons, otherwise add to map as null			
			if ( subDimSet != null ) {
				dimensionMap.put(dimension, subDimSet.toArray(new PafUserSelection[0]));
			} else {
				dimensionMap.put(dimension, null);
			}
			
		}
		
		
		return dimensionMap;
		
	}

}

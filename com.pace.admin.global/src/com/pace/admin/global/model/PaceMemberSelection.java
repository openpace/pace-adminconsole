/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import com.pace.admin.global.constants.SecurityMemberConsants;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.util.StringUtil;
import com.pace.base.PafBaseConstants;

/**
 * 
 * Used to decorate members with Pace Tree Node Selection Type and User Selection.
 * 
 * createMemberSelectionFromExistingMember() is used the most.  Will take an existing member (string)
 * and parse out the relevant information.
 * 
 * For example: 
 * define member = @IDESC(@USER_SEL(T1), L1)
 * call createMemberSelectionFromExistingMember(member)
 * creates a PaceMemberSelection:
 * 	
 *  members[0] = T1
 *  selectionType = PaceTreeNodeSelectionType.IDescendants
 *  levelGenNumber = 1
 *  userSelection = true
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PaceMemberSelection {

	private String[] members;

	private PaceTreeNodeSelectionType selectionType;
	
	private int levelGenNumber;
	private int offsetMinNumber = 0;
	private int offsetMaxNumber = 0;
	
	private boolean userSelection = false;

	public final static String AT_TAG = "@";
	
	public final static String ICHILDREN = "ICHILDREN";

	public final static String CHILDREN = "CHILDREN";

	public final static String IDESCENDANTS = "IDESC";

	public final static String DESCENDANTS = "DESC";

	public final static String LEVEL = "LEVEL";

	public final static String MEMBERS = "MEMBERS";

	public final static String USER_SELECTION = "USER_SEL";

	public final static String GENERATION = "GEN";
	
	public final static String USER_SEL_TAG = AT_TAG + USER_SELECTION;
	
	public final static String OFFSET_MEMBERS = "OFFSET_MEMBERS";
	
	private PaceMemberSelection() {
		
	}
	
	public PaceMemberSelection(String[] members, PaceTreeNodeSelectionType selectionType) {

		this.members = members;
	
		this.selectionType = selectionType;

	}

	public String[] getMembers() {
		return members;
	}

	public void setMembers(String[] members) {
		this.members = members;
	}

	/**
	 * 
	 * Adds a member to end of members array
	 *
	 * @param newMember new member to add
	 */
	public void addMember(String newMember) {

		if (members != null) {

			int numberOfMembers = members.length;

			String[] newMemberAr = new String[numberOfMembers + 1];

			int memberNdx = 0;

			for (String member : members) {

				newMemberAr[memberNdx++] = member;

			}

			newMemberAr[memberNdx] = newMember;

			members = newMemberAr;
		}

	}

	/**
	 * @return selection type of member
	 */
	public PaceTreeNodeSelectionType getSelectionType() {
		return selectionType;
	}

	/**
	 * 
	 * Sets selection type.  If selection type was members and new one isn't members, truncate
	 * the members array to only 1st member in members array.
	 * 
	 * Before members: {"One", "Two", "Three"}
	 * After members: {"One"} 
	 *
	 * @param selectionType selection type to set.
	 */
	public void setSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		if ( this.selectionType != null && this.selectionType.equals(PaceTreeNodeSelectionType.Members) && ! selectionType.equals(PaceTreeNodeSelectionType.Members)) {
			
			String firstMember = members[0];
			
			members = new String[] { firstMember };
			
		}
		
		this.selectionType = selectionType;
	}

	/**
	 * 	
	 * Converts the members array into a long string separated by commas
	 *
	 * @return newly created string list
	 */
	private String getMemberListAsString() {
		
		StringBuffer strBuff = new StringBuffer();

		int memberNdx = 1;

		for (String member : members) {

			strBuff.append(member);

			if (memberNdx != members.length) {
				strBuff.append(", ");
			}

			memberNdx++;
		}

		return strBuff.toString();
		
	}
	
	/**
	 * 
	 * Gets 1st member in array with user selection
	 *
	 * @return if member is user selection, returns member with wrapped user selection tag.
	 */
	public String getMemberWithUserSelection() {
		
		String memberWithUserSelection = null;
		
		if ( userSelection ) {
			
			memberWithUserSelection = USER_SEL_TAG + "(" + members[0] + ")";
			
		} else {
			
			memberWithUserSelection = members[0];
			
		}
		
		
		return memberWithUserSelection;
		
	}
	
	/**
	 * 
	 * Gets member with user selection and level/gen
	 *
	 * @return member with user selection + level gen info
	 */
	private String getMemberWithUserSelectionAndLevelGen() {
		
		String memberWithUserSelection = getMemberListAsString();
		
		//handle level differently
		if ( isKindOfDescendantLevelOrGenSelectionType(selectionType) ) {
		
			if ( isKindOfLevelSelectionType(selectionType) || isKindOfGenerationSelectionType(selectionType) ) {
				switch(selectionType) {
				
					case ILevel:
						memberWithUserSelection = getMemberWithUserSelection();
						break;
					case Level:
					case Generation:
						memberWithUserSelection = getMemberWithUserSelection() + ", " + levelGenNumber;
						break;
				}
			}
			else if (isKindOfGenerationSelectionType(selectionType) ) {
				memberWithUserSelection = getMemberWithUserSelection() + ", " + levelGenNumber;
				
			}
			else if ( isKindOfDescendantSelectionType(selectionType)) {					
				
				switch(selectionType) {
				
					case IDescendants:
					case Descendants:
						memberWithUserSelection = getMemberWithUserSelection();
						break;
					case DescLevel:
					case IDescLevel:
						memberWithUserSelection = getMemberWithUserSelection() +", " + SecurityMemberConsants.LEVEL_IDNT + levelGenNumber;
						break;
					case DescGen:
					case IDescGen:
						memberWithUserSelection = getMemberWithUserSelection() + ", " + SecurityMemberConsants.GENERATION_IDNT + levelGenNumber;
						break;						
				
				}
			} 
			
		} else {
	
			if ( userSelection ) {
			
				memberWithUserSelection = USER_SEL_TAG + "(" + memberWithUserSelection + ")";
				
			} 
		}	
		
		return memberWithUserSelection;
	}
	
	/**
	 * 
	 * Gets member with selection and user selection
	 * 
	 * For example, member T1, IDescendant and Gen of 1 and user selection would result in returning:
	 * @IDESC(@USER_SEL(T1), G1)
	 *
	 * @return member with selection type and user selection
	 */
	public String getMemberWithSelection() {

		String memberWithSelection = null;

		switch (selectionType) {

		case Selection:
			memberWithSelection = getMemberWithUserSelectionAndLevelGen();
			break;
		//case IChild:
		case IChildren:
			memberWithSelection = AT_TAG + ICHILDREN + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		//case Child:
		case Children:
			memberWithSelection = AT_TAG + CHILDREN + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		//case IDesc:
		case IDescendants:
		case IDescLevel:
		case IDescGen:
			memberWithSelection = AT_TAG + IDESCENDANTS + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		//case Desc:
		case Descendants:
		case DescLevel:
		case DescGen:
			memberWithSelection = AT_TAG + DESCENDANTS + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		case ILevel:
		case Level:
			memberWithSelection = AT_TAG + LEVEL + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		case Generation:
			memberWithSelection = AT_TAG + GENERATION + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;		
		case Members:
			memberWithSelection = AT_TAG + MEMBERS + "(" + getMemberWithUserSelectionAndLevelGen() + ")";
			break;
		case OffsetMember:
			memberWithSelection = AT_TAG + OFFSET_MEMBERS + "(" + getMemberWithUserSelectionAndLevelGen()+ "," + offsetMinNumber +"," + offsetMinNumber + ")";
			break;
		case OffsetMemberRange:
			memberWithSelection = AT_TAG + OFFSET_MEMBERS + "(" + getMemberWithUserSelectionAndLevelGen()+ "," + offsetMinNumber +"," + offsetMaxNumber + ")";
			break;
		default:
			memberWithSelection = getMemberWithUserSelectionAndLevelGen();
			break;
		}

		return memberWithSelection;

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getMemberWithSelection();
	}

	/**
	 * Will take an existing member (string) and parse out the relevant information.
	 * 
	 * For example: 
	 * define member = @IDESC(@USER_SEL(T1), L1)
	 * call createMemberSelectionFromExistingMember(member)
	 * creates a PaceMemberSelection:
	 * 	
	 *  members[0] = T1
	 *  selectionType = PaceTreeNodeSelectionType.IDescendants
	 *  levelGenNumber = 1
	 *  userSelection = true
	 * 
	 *  Creates a PaceMemberSelection from an existing member.
	 *
	 * @param member member to parse
	 * @return a new Pace Member Selection built from parsed member
	 */
	public static PaceMemberSelection createMemberSelectionFromExistingMember(String member) {
		
		PaceMemberSelection memberSelection = new PaceMemberSelection ();		
		
		//if "@"
		if ( member.contains(AT_TAG) ) {
			PaceTreeNodeSelectionType memberSelectionType = findPaceTreeNodeSelectionType(member);
			
			//if not members (leave on @USER_SEL) and contains @USER_SEL
			if ( ! memberSelectionType.equals(PaceTreeNodeSelectionType.Members) 
					&&	! memberSelectionType.equals(PaceTreeNodeSelectionType.OffsetMember)
					&&  member.contains(USER_SELECTION)) {
				memberSelection.setUserSelection(true);
				member = stripUserSelection(member);
			} else {
				memberSelection.setUserSelection(false);			
			}			
									
			member = stripSelectionType(member);
			
			String[] memberAr = StringUtil.createStringArFromString(member);
						
			if ( isKindOfDescendantLevelOrGenSelectionType(memberSelectionType) && memberAr.length > 1) {
				
				String descMember = memberAr[0];
								
				if ( memberAr.length == 1 ) {
				
					memberSelection.setLevelGenNumber(PaceTreeNodeProperties.UNDEFINED_TREE_LEVEL_OR_GEN);
					
				} else if ( memberAr.length > 1 ) {

					String levelGenWithNumStr = memberAr[1];
					
					//int levelGenLen = levelGenWithNumStr.length();
					
					String levelGen = levelGenWithNumStr.substring(0, 1);
										
					int numberLevelGenLength = levelGenWithNumStr.length();
					
					String levelGenNum = levelGenWithNumStr.substring(1, numberLevelGenLength);
					
					if ( levelGen.matches("[0123456789]+")) {
						
						memberSelection.setLevelGenNumber(Integer.parseInt(levelGen));
						
					}
					
					if ( levelGenNum.matches("[0123456789]+")) {
						
						memberSelection.setLevelGenNumber(Integer.parseInt(levelGenNum));
						
					}
					
					memberAr = new String[] { descMember };
					
				}
			}	else if( isKindOfOffsetSelectionType(memberSelectionType) ) {
				String offsetMember = memberAr[0];
				if ( memberAr.length == 1 ) {
					
					memberSelection.setLevelGenNumber(PaceTreeNodeProperties.UNDEFINED_TREE_LEVEL_OR_GEN);
					
				} else if ( memberAr.length > 1 ) {

					String offsetMinNum = memberAr[1];
					if ( offsetMinNum.matches("[-0123456789]+")) {
						
						memberSelection.setOffsetMinNumber(Integer.parseInt(offsetMinNum));
						
					}
					String offsetMaxNum = memberAr[2];
					if ( offsetMaxNum.matches("[-0123456789]+")) {
						
						memberSelection.setOffsetMaxNumber(Integer.parseInt(offsetMaxNum));
						
					}
					
					memberAr = new String[] { offsetMember };

				}
			}					
			
			memberSelection.setMembers(memberAr);
			
			memberSelection.setSelectionType(memberSelectionType);
			if( memberSelection.getOffsetMinNumber() != memberSelection.getOffsetMaxNumber() ) {
				memberSelection.setSelectionType(PaceTreeNodeSelectionType.OffsetMemberRange);
			}
			
		}  else {
			
			memberSelection.setSelectionType(PaceTreeNodeSelectionType.Selection);
			memberSelection.setLevelGenNumber(0);
			memberSelection.setUserSelection(false);
			memberSelection.setMembers(new String[] {member});

		}
		
		return memberSelection;
				
	}
	
	/**
	 * 
	 * Removes selection type from member
	 *
	 * @param member member to remove selection type
	 * @return member without selection type
	 */
	private static String stripSelectionType(String member) {
		
		int index1 = member.indexOf("(");
		int index2 = member.lastIndexOf(")");
		
		if ( index1 == -1 || index2 == -1) {
			return member;
		}
		
		return member.substring(++index1, index2);
		
	}

	/**
	 * 
	 * Remove @USER_SEL tag from member
	 *
	 * @param member member to remove user selection
	 * @return member without user selection tag
	 */
	private static String stripUserSelection(String member) {
		
		member = member.replace(USER_SEL_TAG + "(", "");
					
		member = member.replaceFirst("\\)", "");
		
		return member;
		
	}

	/**
	 * 
	 * Gets number of members in members array.
	 *
	 * @return number of members in members array
	 */
	public int numberOfMembers() {
		
		if ( members == null ) {
			return 0;
		}
		
		return members.length;
		
	}

	/**
	 * 
	 * Remove last member from array
	 *
	 */
	public void removeMember() {

		if (members != null && members.length != 0) {

			int numberOfMembers = members.length;

			String[] newMemberAr = new String[numberOfMembers - 1];

			for (int i = 0; i < newMemberAr.length; i++) {

				newMemberAr[i] = members[i];

			}

			members = newMemberAr;

		}

	}

	/**
	 * 
	 * Checks if this is Multi-Year Member, or OffsetMemberRange 
	 *
	 * @param selectionType selection type to check
	 * @return	true if is OffsetMember, or OffsetMemberRange 
	 */
	public boolean isMultiYearSingleMember(String member) {
		
		return 
		member.equals(PafBaseConstants.VIEW_TOKEN_FIRST_PLAN_YEAR) ||
		member.equals(PafBaseConstants.VIEW_TOKEN_FIRST_NONPLAN_YEAR) ||
		member.equals(PafBaseConstants.VIEW_TOKEN_FIRST_PLAN_PERIOD); 
		
	}

	/**
	 * 
	 * Checks if this is Multi-Year Member, or OffsetMemberRange 
	 *
	 * @param selectionType selection type to check
	 * @return	true if is OffsetMember, or OffsetMemberRange 
	 */
	public boolean isMultiYearMultipleMember(String member) {
		
		return 
		member.equals(PafBaseConstants.VIEW_TOKEN_PLAN_YEARS) || 
		member.equals(PafBaseConstants.VIEW_TOKEN_NONPLAN_YEARS); 
		
	}
	/**
	 * 
	 * Checks if this is a user selection
	 *
	 * @return true if user selection
	 */
	public boolean isUserSelection() {
		return userSelection;
	}

	/**
	 * 
	 * Sets user selection
	 *
	 * @param userSelection
	 */
	public void setUserSelection(boolean userSelection) {
		this.userSelection = userSelection;
	}
	
	
	/**
	 * 
	 * Gets the selection type from an existing member.
	 *
	 * @param member member to parse
	 * @return	selection type from member
	 */
	public static PaceTreeNodeSelectionType findPaceTreeNodeSelectionType(String member) {
				
		PaceTreeNodeSelectionType memberSelectionType = PaceTreeNodeSelectionType.Selection;

		if( member.contains("(")) {

			String memberType = member.substring(member.indexOf(AT_TAG)+1, member.indexOf("("));
		
			if ( memberType.equals(ICHILDREN)) {
				memberSelectionType = PaceTreeNodeSelectionType.IChildren;
			} 
			
			else if ( memberType.equals(CHILDREN)) {
				memberSelectionType = PaceTreeNodeSelectionType.Children;
			} 
			
			else if ( memberType.equals(IDESCENDANTS)) {
				
				if ( member.matches(".*, " + SecurityMemberConsants.GENERATION_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.IDescGen;
					
				} else if (member.matches(".*, " + SecurityMemberConsants.LEVEL_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.IDescLevel;
					
				} else {
				
					memberSelectionType = PaceTreeNodeSelectionType.IDescendants;
				
				}
				
			} 
			
			else if ( memberType.equals(DESCENDANTS)) {
				
				if ( member.matches(".*, " + SecurityMemberConsants.GENERATION_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.DescGen;
					
				} else if (member.matches(".*, " + SecurityMemberConsants.LEVEL_IDNT + "[0123456789]+\\)")) {
					
					memberSelectionType = PaceTreeNodeSelectionType.DescLevel;
					
				} else {
				
					memberSelectionType = PaceTreeNodeSelectionType.Descendants;
				
				}
				
			} 
			
			else if ( memberType.equals(LEVEL)) {
				if( member.matches(".*, " + "[0123456789]+\\)")) {
					memberSelectionType = PaceTreeNodeSelectionType.Level;
				}
				else {
					memberSelectionType = PaceTreeNodeSelectionType.ILevel;
				}
			} 
			
			else if ( memberType.equals(GENERATION)) {
				memberSelectionType = PaceTreeNodeSelectionType.Generation;			
			} 
			
			else if ( memberType.equals(MEMBERS)) {
				memberSelectionType = PaceTreeNodeSelectionType.Members;
			} 
			
			else if ( memberType.equals(OFFSET_MEMBERS)) {
					memberSelectionType = PaceTreeNodeSelectionType.OffsetMember;	
			} 
		}
		
		return memberSelectionType;
	}
	
	/**
	 * 
	 * Checks if selection type is any type of Level
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is Level
	 */		
	public static boolean isKindOfLevelSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return selectionType.equals(PaceTreeNodeSelectionType.Level) || selectionType.equals(PaceTreeNodeSelectionType.ILevel);
		
	}

	/**
	 * 
	 * Checks if selection type is any type of Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is Generation
	 */		
	public static boolean isKindOfGenerationSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return selectionType.equals(PaceTreeNodeSelectionType.Generation);
		
	}
	
	/**
	 * 
	 * Checks if selection type is any type of Descendant or IDescendant
	 *
	 * @param selectionType selection type to check
	 * @return	true if selection type is any type of Descendant or IDescendant
	 */	
	public static boolean isKindOfDescendantSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return 	selectionType.equals(PaceTreeNodeSelectionType.IDescendants) || 
				selectionType.equals(PaceTreeNodeSelectionType.IDescLevel) ||
				selectionType.equals(PaceTreeNodeSelectionType.IDescGen) ||
				selectionType.equals(PaceTreeNodeSelectionType.Descendants) ||
				selectionType.equals(PaceTreeNodeSelectionType.DescLevel) ||
				selectionType.equals(PaceTreeNodeSelectionType.DescGen) ;
	}

	/**
	 * 
	 * Checks if selection type is IDesc, Desc, Level or Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if is IDesc, Desc, Level or Generation
	 */
	public static boolean isKindOfDescendantLevelOrGenSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return isKindOfDescendantSelectionType(selectionType) || 
				isKindOfLevelSelectionType(selectionType) || 
				isKindOfGenerationSelectionType(selectionType);
		
	}

	/**
	 * 
	 * Checks if selection type is Children, IChildren, IDesc, Desc, ILevel, Level or Generation
	 *
	 * @param selectionType selection type to check
	 * @return	true if is Children, IChildren, IDesc, Desc, ILevel, Level or Generation
	 */
	public static boolean isKindOfChildrenDescLevelOrGenSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return 
				selectionType.equals(PaceTreeNodeSelectionType.Children) ||
				selectionType.equals(PaceTreeNodeSelectionType.IChildren) || 
				isKindOfDescendantLevelOrGenSelectionType(selectionType);
		
	}
	
	/**
	 * 
	 * Checks if selection type is OffsetMember, or OffsetMemberRange 
	 *
	 * @param selectionType selection type to check
	 * @return	true if is OffsetMember, or OffsetMemberRange 
	 */
	public static boolean isKindOfOffsetSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		return 
			selectionType.equals(PaceTreeNodeSelectionType.OffsetMember) ||
			selectionType.equals(PaceTreeNodeSelectionType.OffsetMemberRange); 
		
	}

	/**
	 * @return Returns the levelGenNumber.
	 */
	public int getLevelGenNumber() {
		return levelGenNumber;
	}

	/**
	 * @param levelGenNumber The levelGenNumber to set.
	 */
	public void setLevelGenNumber(int levelGenNumber) {
		this.levelGenNumber = levelGenNumber;
	}
	
	/**
	 * 
	 * Checks if member is a level member
	 *
	 * @param member member to see if LEVEL
	 * @return true if member is level
	 */
	public boolean isLevelMember(String member) {
		
		if ( member != null && member.contains(LEVEL)) {
			return true;
		}
		
		return false;
	}

	/**
	 * 
	 * Checks if member is a generation member
	 *
	 * @param member member to see if GENERATION
	 * @return true if member is generation
	 */	
	public boolean isGenerationMember(String member) {
		
		if ( member != null && member.contains(GENERATION)) {
			return true;
		}
		
		return false;
	}
	public int getOffsetMinNumber() {
		return offsetMinNumber;
	}

	public void setOffsetMinNumber(int offsetMinNumber) {
		this.offsetMinNumber = offsetMinNumber;
	}

	public int getOffsetMaxNumber() {
		return offsetMaxNumber;
	}

	public void setOffsetMaxNumber(int offsetMaxNumber) {
		this.offsetMaxNumber = offsetMaxNumber;
	}

	public static void main(String[] args ) {
		
//		String[] ar = {"single_member", "@IDESC(totprod, L0)", "@IDESC(totprod, G2)", "@ICHILD(totprod, L4)", "@ICHILD(totprod, g3)"};
		
		String[] ar = {"@IDESC(totprod, L1)", "@DESC(totprod, G2)", "@LEVEL(totProd, 1)", "@GEN(totProd, 1)"};
		
		for (String s : ar)  {
		
			PaceMemberSelection paceMember = new PaceMemberSelection();
			paceMember = PaceMemberSelection.createMemberSelectionFromExistingMember(s);
		
			System.out.println(paceMember);
		}
		
		
		
		
	}
}

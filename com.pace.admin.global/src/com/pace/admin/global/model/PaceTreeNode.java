/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.List;

import com.pace.admin.global.enums.ListPlacement;
import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.util.PaceTreeUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.view.ViewTuple;

/**
 * 
 * This class is a node used within a pace tree and is self contained.  Children can 
 * be added to this node using the api, the node will remove itself from its parent using
 * the api and the gui can access the getDisplayName to get any relevant alias and/or
 * built selection type tags (@MEMBERS()) for example.  The 1st node found in a pace
 * tree should be of type PaceTreeNodeType.Root with it's children being the 1st visible
 * nodes within the tree.
 *
 * @version	1.0
 * @author JavaJ
 *
 */
public class PaceTreeNode implements Cloneable {
		
	//by default, children will be added to end of children list
	public static final ListPlacement DEFULAT_LIST_PLACEMENT = ListPlacement.End;
	
	//name of node, this could be member name, user selection name, member tag name, etc
	private String name;
	
	//a node's properties
	private PaceTreeNodeProperties properties;
	
	//used for @MEMBERS, the additional pace tree node list will contain the 2nd - end of members list
	private List<PaceTreeNode> additionalPaceTreeNodeList = new ArrayList<PaceTreeNode>();

	//parent of current node
	private PaceTreeNode parentNode;
		
	//ordered list of children for current node
	private List<PaceTreeNode> children = new ArrayList<PaceTreeNode>();
	
	//view tuple for current node.  really only relevant is isViewTuple() returns true.
	private ViewTuple viewTuple;
	
	//used by the excel client
	private Integer symetricGroupNumber;
				
	/**
	 * 
	 * Constructor - creates a pace tree node
	 * 
	 * @param name			Name of node
	 * @param properties	Properties of node
	 * @param parentNode	Parent of node
	 */
	public PaceTreeNode(String name, PaceTreeNodeProperties properties, PaceTreeNode parentNode) {
	
		this(name, properties, parentNode, null);
		
	}
		
	/**
	 * 
	 * Constructor - creates a pace tree node. 
	 * 
	 * name and properties are required input parameters.  If they are null, a NullPointerException
	 * will be thrown. 
	 * 
	 * @param name				Name of node
	 * @param properties		Properties of node
	 * @param parentNode		Parent of node
	 * @param newNodePosition	Will add at position specified in parent's children
	 * 
	 * @throws NullPointerException Thrown if name or propertes are null.
	 */
	public PaceTreeNode(String name, PaceTreeNodeProperties properties, PaceTreeNode parentNode, Integer newNodePosition) {
	
		//validate input arguments
		if ( name == null ) {
			throw new NullPointerException("Pace Node must have a name");
		} else if ( properties == null ) {
			throw new NullPointerException("Pace Node must have a properties");
		}
			
		//set instance variables
		this.name = name;
		this.properties = properties;
		this.parentNode = parentNode;	
		
		//only time parent would be null is when a member is added to the @MEMBERS list and parent is set after creation of node
		if ( this.parentNode != null ) {
				
			this.parentNode.addChild(this, newNodePosition);		
			
		}
		
		//if member of a tuple and a blank or member tag, do special logic
		if (  properties.isMemberOfTuple() && (this.isType(PaceTreeNodeType.Blank) || this.isType(PaceTreeNodeType.MemberTag) )) {
			
			try {
			
				//number of axis dimension for node
				int numOfAxisDims = properties.getAxisDimSize();
								
				//actual axis index for axis dimensions for node
				int axisIndex = properties.getAxisDimIndex();
				
				//clone properties
				PaceTreeNodeProperties propertiesClone = this.properties.clone();
				
				if ( this.isType(PaceTreeNodeType.Blank)) {
							
					//if not built from existing tuple, blow out until last axis dim
					if ( ! properties.isBuiltFromViewTuple() ) {
					
						/*
						 * If the axis Index is less than number of dims on axis, create
						 * new blank nodes.  for example:
						 * 
						 * 2nd node is added as Blank
						 * axis dim size is 3
						 * 
						 * Before:
						 * 
						 * SLS_DLR <- selected
						 * SLS_AUR
						 * 
						 * After:
						 * 
						 * SLS_DLR
						 * 		Blank		<- node added
						 * 			Blank	<- node added via code because prior node was 2nd axis dim, blanks need
						 * 					   to be created all the way down to last axis dim which is 3 in this case.
						 * SLS_AUR
						 * 
						 */
						if ( axisIndex < numOfAxisDims ) {
													
								//increment axis dim index by one
								propertiesClone.setAxisDimIndex(axisIndex + 1);
								
								PaceTreeNode blankNode = new PaceTreeNode(name, propertiesClone, null);
								
								this.addChild(blankNode);
								
								blankNode.setParentNode(this);
							
												
						}
						
					}
					
				} else if (this.isType(PaceTreeNodeType.MemberTag)) {
					
					if ( axisIndex != numOfAxisDims) {
					
						//if axis index is greater than number of dims, this is a member tag node. Set the current
						//axis to 1 because start of axis.  
						if ( axisIndex > numOfAxisDims ) {
							
							axisIndex = 1;
							
							this.properties.setAxisDimIndex(axisIndex);
						}
						
						//if not built from existing tuple, blow out until last axis dim
						if ( ! properties.isBuiltFromViewTuple() ) {
						
							/*
							 * if the axis index doesn't equal the number of axis dims, 
							 * create the next member tag node until there is one member tag
							 * node per axis dimension starting from the 1st axis dim.
							 * 
							 * For example:
							 * 
							 * 2nd node is selected on a built 3 dim axis
							 * Member Tag "MT1" is added
							 * 
							 * Before:
							 * 
							 * SLS_DLR
							 * 		WP	<- selected
							 * 			S01
							 * SLS_AUR
							 * 		WP
							 * 			S01
							 * 
							 * After:
							 * 
							 * SLS_DLR
							 * 		WP
							 * 			S01
							 * MT1	<- node added at 1st dim
							 * 		MT1	 	<- node added at 2nd dim 
							 * 			MT1		<- node added at 3rd and final dim for axis
							 * SLS_AUR
							 * 		WP
							 * 			S01
							 * 
							*/
							if ( axisIndex != numOfAxisDims ) {
							
								propertiesClone.setAxisDimIndex(axisIndex + 1);
								
								PaceTreeNode memberTagNode = new PaceTreeNode(name, propertiesClone, null);
								
								this.addChild(memberTagNode);
								
								memberTagNode.setParentNode(this);
							
							}
						
						}
					}	
				}
			
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//if this is a view tuple, create a new view tuple with defaults
		if ( isViewTuple() ) {
						
			this.viewTuple = new ViewTuple();
						
		}
			
				
	}
	
	/**
	 * 
	 * Node to add to root.
	 *
	 * @param nodeToAdd 	node to check and verify if can be added to Root Node
	 * @return				true if node can be added to Root Node
	 */
	private boolean canAddToRoot(PaceTreeNode nodeToAdd) {
		
		//if current node is at axis dim index of 0 and node to be added is at 1st axis dim index
		if ( nodeToAdd != null && nodeToAdd.getProperties() != null &&
				this.getProperties().getAxisDimIndex() == 0 && isFirstDimMember(nodeToAdd) ) {
			
			return true;
			
		}
		
		return false;
	}
	
	/**
	 * 
	 * Sets the children
	 *
	 * @param children 	children list to replace current children list
	 */
	public void setChildren(List<PaceTreeNode> children) {
		
		this.children = children;
		
	}
	
	/**
	 * 
	 * Can add node as child of current node.
	 *
	 * @param nodeToAdd 	node to check and verify if can be added as child of current node
	 * @return				true if nodeToAdd can be added as child
	 */
	private boolean canAddAsChild(PaceTreeNode nodeToAdd) {
		
		//can't add child unless same axis dim index and not a blank or member tag
		if ( nodeToAdd != null && nodeToAdd.getProperties() != null && this.getProperties() != null &&
				( this.getProperties().getAxisDimIndex() + 1 ) == nodeToAdd.getProperties().getAxisDimIndex() 
				&& ! this.isType(PaceTreeNodeType.MemberTag)) {
			
			if ( this.isType(PaceTreeNodeType.Blank) 
					&& ! nodeToAdd.isType(PaceTreeNodeType.Blank) ) {
				
				return false;
				
			} else {
			
				return true;
				
			}
			
		}
		
		return false;
	}
	
	/**
	 * 
	 * Can add node as peer of current node.
	 *
	 * @param nodeToAdd  	node to check and verify if can be added as peer of current node
	 * @return				true if nodeToAdd can be added as peer
	 */
	private boolean canAddAsPeer(PaceTreeNode nodeToAdd) {
		
		if ( nodeToAdd != null && nodeToAdd.getProperties() != null && 
				this.getProperties().getAxisDimIndex() == nodeToAdd.getProperties().getAxisDimIndex() &&
				! this.isType(PaceTreeNodeType.MemberTag)) {
						
			if ( isFirstDimMember(nodeToAdd )) {
				
				return true;
				
			} else {
				
				if ( this.getParentNode().isType(PaceTreeNodeType.Blank)) {
					
					if ( nodeToAdd.isType(PaceTreeNodeType.Blank)) {
						return true;
					}
					
				} else {
					
					return true;
				}
								
			}
			
		}
		
		return false;
	}
	
	/**
	 * 
	 * Checks if node passed in is on the 1st axis dim
	 *
	 * @param node 	node to check if on 1st axis dimension
	 * @return		true if node has axis dim index of 1
	 */
	private boolean isFirstDimMember(PaceTreeNode node) {
		
		if ( node != null && node.getProperties() != null && 
				node.getProperties().getAxisDimIndex() == 1 ) {
			return true;
		}
		
		return false;
		
	}
				
	/**
	 * 
	 *  Adds child to current childrens list.  If positionIndex is null, added to 
	 *  the end of the child list, if not null, adds at positionIndex if valid.  Valid
	 *  range is between 0 and size of current child list.
	 *
	 * @param childNode		child node to add
	 * @param positionIndex	position to add child node to parent's children
	 * 
	 * @throws NullPointerException	Thrown if childNode is null
	 */
	public void addChild(PaceTreeNode childNode, Integer positionIndex ) {
		
		if ( childNode == null ) {
			throw new NullPointerException("Child node can't be null for node " + name);
		}
		
		if ( positionIndex == null ) {
			
			addChild(childNode);
			
		} else {
			
			if ( positionIndex >= 0 && positionIndex <= this.children.size()) {
				
				this.children.add(positionIndex, childNode);
				
			} else {
				
				addChild(childNode);
				
			}
			
		}
				
	}
	
	/**
	 * 
	 * Adds a child node to the default placement on the childrens list
	 *
	 * @param childNode child node to add
	 */
	public void addChild(PaceTreeNode childNode) {
		
		if ( childNode != null) {
		
			this.children.add(childNode);
			
		}
		
	}
	
	/**
	 * 
	 *  Removes a child node if exists in children list
	 *
	 * @param childNode child node to remove
	 * @throws NullPointerException if child node is null
	 */
	public void removeChild(PaceTreeNode childNode) {
		
		if ( childNode == null ) {
			throw new NullPointerException("Child node can't be null for node " + name);
		}

		if ( hasChild(childNode)) {
			this.children.remove(childNode);
		}
		
	}

	/**
	 * 
	 *  Moves a child element by the +/- i.  A bounds check is made before 
	 *  children items are moved
	 *
	 * @param childNode child element to be moved
	 * @param relativeNewPosition			+ to move down list, - to move up list
	 */
	public void moveChild(PaceTreeNode childNode, int relativeNewPosition) {

		int currentChildNdx = this.children.indexOf(childNode);
		
		int newChildNdx = relativeNewPosition + currentChildNdx;
		
		//bounds check
		if ( newChildNdx >= 0 && newChildNdx < this.children.size()) {
			
			removeChild(childNode);
			
			addChild(childNode, newChildNdx);
					
		}
		
	}	
	
	/**
	 * 
	 *  Creates a new list of children nodes and returns
	 *
	 * @return new list of children nodes
	 */
	public List<PaceTreeNode> getChildren() {
				 		
		return new ArrayList<PaceTreeNode>(children);
		
	}
		
	/**
	 * 
	 * Gets the child index if current node has child in children list
	 *
	 * @param childNode  	node to get index for  
	 * @return				index of child node in current children's list.  returns -1 if not in childs list.
	 */
	public int getChildIndex(PaceTreeNode childNode) {
		
		int childNdx = -1;
		
		if ( childNode != null && hasChild(childNode) ) {
		
			childNdx = this.getChildren().indexOf(childNode);
			
		}
		
		return childNdx;
		
	}
			
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 *  The pace node's display name.  Alias value will be used if 
	 *  aliasTable is specified or name will be returned.
	 *
	 * @return formated display name of node
	 */
	public String getDisplayName() {
	
		String displayName = null;
		
		if ( properties.getType().equals(PaceTreeNodeType.Blank)) {
			
			displayName = PaceTreeNodeType.Blank.toString();
			
		} else if ( ! properties.hasAliasTables() ) {
			
			displayName = name;
			
		} else {
			
			if ( properties.getAliasValue() != null ) {
				
				displayName = properties.getAliasValue();
				
			} else {
				
				displayName = name;
			}
			
		}
		
		if ( properties.getSelectionType() != null && ! this.isSelectionType(PaceTreeNodeSelectionType.None)) {
			
			if ( this.isSelectionType(PaceTreeNodeSelectionType.Members)) {
			
				List<String> aliasMembers = new ArrayList<String>();
				
				for (PaceTreeNode additionalPaceTreNode : additionalPaceTreeNodeList ) {
										
					//try to get alias member value from the additional node
					String aliasValue = additionalPaceTreNode.getProperties().getAliasValue(); 
					
					//if null, use the name, otherwise use alias value
					if ( aliasValue == null ) {
												
						if ( additionalPaceTreNode.getName().equals(PafBaseConstants.PAF_BLANK)) {
							aliasMembers.add(PaceTreeNodeType.Blank.toString());
						} else {						
							aliasMembers.add(additionalPaceTreNode.getName());
						}
						
						
						
					} else {
					
						aliasMembers.add(aliasValue);
						
					}					
					
				}				
				
				aliasMembers.add(0, displayName);
				
				return (new PaceMemberSelection(aliasMembers.toArray(new String[0]), properties.getSelectionType())).getMemberWithSelection();
				
			} else {
				
				PaceMemberSelection memberSelection = new PaceMemberSelection(new String[] { displayName }, properties.getSelectionType());
				
				if ( this.isSelectionType(PaceTreeNodeSelectionType.Level) 
						|| this.isSelectionType(PaceTreeNodeSelectionType.IDescLevel)
						|| this.isSelectionType(PaceTreeNodeSelectionType.DescLevel)
						|| this.isSelectionType(PaceTreeNodeSelectionType.Generation)
						|| this.isSelectionType(PaceTreeNodeSelectionType.IDescGen)
						|| this.isSelectionType(PaceTreeNodeSelectionType.DescGen)) {
					
					memberSelection.setLevelGenNumber(properties.getLevelGenNumber());
					
				}
				else if ( this.isSelectionType(PaceTreeNodeSelectionType.OffsetMember) ) {
					
					memberSelection.setOffsetMinNumber(properties.getOffsetMinNumber());
					memberSelection.setOffsetMaxNumber(properties.getOffsetMinNumber());
					
				}
				else if ( this.isSelectionType(PaceTreeNodeSelectionType.OffsetMemberRange) ) {
					
					memberSelection.setOffsetMinNumber(properties.getOffsetMinNumber());
					memberSelection.setOffsetMaxNumber(properties.getOffsetMaxNumber());
				}
				
				return memberSelection.getMemberWithSelection();
			}
			
		} else {
			
			return displayName;
			
		}
		
	}

	
	/**
	 * 
	 *  The pace node's alias display name.
	 *
	 * @return alias display name
	 */
	public String getAliasDisplayName() {
	
		String displayName = null;
			
		if ( this.isSelectionType(PaceTreeNodeSelectionType.Members)) {
		
			if ( ! properties.hasAliasTables() ) {
				
				if ( this.getName().equals(PafBaseConstants.PAF_BLANK)) {
					
					displayName = PaceTreeNodeType.Blank.toString();
					
				} else {						
					
					displayName = name;
					
				}

				
			} else {
				
				if ( properties.getAliasValue() != null ) {
					
					displayName = properties.getAliasValue();
					
				} else {
					
					displayName = name;
				}
				
			}		
				
			
		} else {
			
			displayName = getDisplayName();
		}
			
		return displayName;
	} 
		

	
	/**
	 * @return the parentNode
	 */
	public PaceTreeNode getParentNode() {
		return parentNode;
	}

	/**
	 * 
	 * Checks if current node has children.
	 *
	 * @return	true if children size is greater than 0
	 */
	public boolean hasChildren() {
	
		return (children.size() > 0) ? true : false;
	}
	
	/**
	 * 
	 * Checks if current node has child node
	 *
	 * @param childNode	child node to check if in children's list
	 * @return			true if child nod is in children's list
	 */
	public boolean hasChild(PaceTreeNode childNode) {
		
		boolean hasChild = false;
		
		//if current node has children and child node is not null
		if ( hasChildren() && childNode != null) {
			
			hasChild = children.contains(childNode);			
			
		}
		
		return hasChild;
		
	}

	
	/**
	 * 
	 *  Replaces the a node in a parent node.
	 *
	 * @param nodeToSwap	node to replace
	 * @return				reference to new node replacing old current node
	 */
	public void replace(PaceTreeNode nodeToSwap) {
		
		PaceTreeNode newSwappedNode = null;
		
		if ( canSwap(nodeToSwap)) {
		
			PaceTreeNode parentNode = this.getParentNode();
			
			int childNdx = parentNode.getChildIndex(this);
			
			PaceTreeNodeProperties nodeToSwapProperties = nodeToSwap.getProperties();
			
			//try to clone
			try {
				nodeToSwapProperties = nodeToSwapProperties.clone();
				
			} catch (CloneNotSupportedException e) {
				e.printStackTrace(); //will never happen
			}
			
			nodeToSwapProperties.setSelectionType(nodeToSwap.getProperties().getSelectionType());
			//nodeToSwapProperties.setMemberOfTuple(true);
			
			newSwappedNode = new PaceTreeNode(nodeToSwap.getName(), nodeToSwapProperties, parentNode, childNdx);
			
			newSwappedNode.addMembers(nodeToSwap.getAdditionalPaceTreeNodeList());
			
			if ( ! nodeToSwap.isType(PaceTreeNodeType.Blank) ) {
				
				for (PaceTreeNode childNode : this.getChildren()) {
					
					childNode.setParentNode(newSwappedNode);
					
				}
				
				newSwappedNode.setChildren(this.getChildren());
							
			} 
				
			//if is a view tuple, swap the view tuple properties
			if ( this.isViewTuple() ) {
				
				newSwappedNode.setViewTuple(this.getViewTuple());
				
			}
			
			this.remove();
		
		}
	}
	

	/**
	 * 
	 * Removes the last member for @MEMBERS
	 *
	 * @return	pace tree node removed from additional node list
	 */
	public PaceTreeNode removeMember() {
		
		PaceTreeNode paceTreeNodeRemoved = null;
		
		if ( additionalPaceTreeNodeList.size() > 0 ) {
			
			//remove last pace tree node
			paceTreeNodeRemoved = additionalPaceTreeNodeList.remove(additionalPaceTreeNodeList.size() - 1 );
			
		}
	
		return paceTreeNodeRemoved;
		
	}
	
	/**
	 * 
	 * Clears the additional pace tree node list.
	 *
	 */
	public void clearMembers() {		
		
		if ( additionalPaceTreeNodeList != null && additionalPaceTreeNodeList.size() > 0 ) {
			
			additionalPaceTreeNodeList.clear();
			
		}
	
	}
	
	/**
	 * 
	 * If parent node contains current node as a child, the parent will 
	 * remove the child node.  If the current node is a member tag, remove
	 * all member tag nodes up the tree until is removed from Root node.
	 *
	 */
	public void remove() {
			
		if ( parentNode != null && parentNode.hasChild(this)) {
			
			if ( this.isType(PaceTreeNodeType.MemberTag) && this.getProperties().getAxisDimIndex() > 1 ) {
				
				//get 1st node from 1st dimension for member tag, then remove
				PaceTreeNode topMemberTagNode = PaceTreeUtil.getParentNodeAtDimNdx(this, 2);
				
				topMemberTagNode.remove();			
				
				
			} else {
			
				parentNode.removeChild(this);
				
			}
									
		}
		
	}
	
	/**
	 * 
	 * Adds member to the @MEMBERS additional node list
	 *
	 * @param additionalMemberPaceNode
	 */
	public void addMember(PaceTreeNode additionalMemberPaceNode) {
				
		if ( additionalMemberPaceNode != null ) {
			
			additionalPaceTreeNodeList.add(additionalMemberPaceNode);
		}		
		
	}
	
	/**
	 * Adds a list of  members to the @MEMBERS additional node list
	 * 
	 * @param additionalMemberPaceNodes
	 */
	public void addMembers(List<PaceTreeNode> additionalMemberPaceNodes){
		
		if(additionalMemberPaceNodes != null && additionalMemberPaceNodes.size() > 0){
			
			for(PaceTreeNode node : additionalMemberPaceNodes){
				
				addMember(node);
				
			}
				
		}
		
	}

	
	/**
	 * 
	 *  Checks if current node has additional members.  Used for the @MEMBERS node.
	 *
	 * @return	true if additional node list is not empty
	 */
	public boolean hasAdditionalMembers() {
		
		return (additionalPaceTreeNodeList.size() != 0 ) ? true : false;
		
	}

	/**
	 * @return the properties
	 */
	public PaceTreeNodeProperties getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(PaceTreeNodeProperties properties) {
		this.properties = properties;
	}

	/**
	 * @return the additionalPaceTreeNodeList
	 */
	public List<PaceTreeNode> getAdditionalPaceTreeNodeList() {
		return additionalPaceTreeNodeList;
	}	

	/**
	 * @param parentNode the parentNode to set
	 */
	public void setParentNode(PaceTreeNode parentNode) {
		this.parentNode = parentNode;
	}
		
	/**
	 * 
	 * Checks if node to add can in fact be added.
	 *
	 * @param nodeToAdd	node to try to add
	 * @return			true if node can be added
	 */
	public boolean canAdd(PaceTreeNode nodeToAdd) {
				
		if ( nodeToAdd != null && nodeToAdd.getProperties() != null 
				&& ! nodeToAdd.isType(PaceTreeNodeType.TextOnly)) {
		
			//if a member tag, can be added
			if ( nodeToAdd.isType(PaceTreeNodeType.MemberTag) ) {
				
				return true;
							
			} else if ( canAddToRoot(nodeToAdd)) {
				
				return true;
				
			} else {
				
				if ( canAddAsChild(nodeToAdd) ) {
			
					return true;
				
				} else if ( canAddAsPeer(nodeToAdd) ) {
				
					return true;
				
				} else if ( isFirstDimMember(nodeToAdd)) {
				
					return true;
					
				}
			}
		}
		
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PaceTreeNode clone() throws CloneNotSupportedException {
		try {
			
			PaceTreeNode node = (PaceTreeNode) super.clone();
			
			if( this.properties != null ) {
				node.properties = this.properties.clone();
			}
			if( this.viewTuple != null ) {
				node.viewTuple = this.viewTuple.clone();					
			}
			node.children = new ArrayList<PaceTreeNode>();
			if( this.children != null ) {
				for( PaceTreeNode child : this.children ) {
					PaceTreeNode copyChild = child.clone();
					copyChild.setParentNode(node);
					node.children.add(copyChild);
				}
				node.setChildren(node.children);
			}
			return node;
			
		} catch (CloneNotSupportedException e) {
			throw new AssertionError();  // Can't happen
		}
		
	}
	
	/**
	 * 
	 * Adds node to an existing node if canAdd() returns true.
	 *
	 * @param nodeToAdd				node to add
	 * @param isViewTupleTreeMember	if node is on a PaceViewTupleTree, true
	 * @return						new node created on existing node
	 */
	public PaceTreeNode add(PaceTreeNode nodeToAdd, boolean isViewTupleTreeMember) {		
		
		PaceTreeNode newNodeToAdd = null;
		
		if ( canAdd(nodeToAdd) ) {		
					
			//get node name
			String nodeToAddName = nodeToAdd.getName();
			
			//get node to add properties
			PaceTreeNodeProperties nodeToAddProperties = nodeToAdd.getProperties();
			
			//try to clone
			try {
				nodeToAddProperties = nodeToAddProperties.clone();
				
			} catch (CloneNotSupportedException e) {
				e.printStackTrace(); //will never happen
			}
			
			//set if node is member of a tuple
			nodeToAddProperties.setMemberOfTuple(isViewTupleTreeMember);
			
			//if member tag
			if ( nodeToAdd.isType(PaceTreeNodeType.MemberTag) ) {
				
				if ( this.getProperties().getAxisDimIndex() > 0 ) {
				
					//get index and insert as next child of root + 1	
					int positionNdx = PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(this, 1);
					
					positionNdx++;
					
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, PaceTreeUtil.getParentNodeAtDimNdx(this, 1), positionNdx);
					
				} else {
					
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, PaceTreeUtil.getRootNode(this));
					
				}
												
			} else if ( canAddToRoot(nodeToAdd)) {
				
				if ( this.isType(PaceTreeNodeType.Root) ) {
				
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, this);
					
				} else {
					
					//get index and insert as next child of root + 1	
					int positionNdx = PaceTreeUtil.getPositionNdxForParentNodeAtDimNdx(this, 1);
					
					positionNdx++;
					
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, PaceTreeUtil.getParentNodeAtDimNdx(this, 1), positionNdx);
					
				}
												
			} else {
				
				if ( canAddAsChild(nodeToAdd) ) {
			
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, this, 0);
				
				} else if ( canAddAsPeer(nodeToAdd) ) {
				
					if ( this.isSelectionType(PaceTreeNodeSelectionType.Members)) {
						
						newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, null);
						
						newNodeToAdd.setParentNode(this.getParentNode());
						
						this.addMember(nodeToAdd);
																		
					} else {
						
						int toTreeSelectedNodeNdx = this.getParentNode().getChildren().indexOf(this);
						
						//add one for next selection
						toTreeSelectedNodeNdx++;
						
						newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, this.getParentNode(), toTreeSelectedNodeNdx);
											
					}
				
				} else if ( isFirstDimMember(nodeToAdd) ) {
				
					newNodeToAdd = new PaceTreeNode(nodeToAddName, nodeToAddProperties, PaceTreeUtil.getRootNode(this));
					
				}
			}
			
			
		}		
		
		return newNodeToAdd;
		
	}
	
	/**
	 * 
	 * Checks if node to swap can be swapped with current node
	 *
	 * @param nodeToSwap	node to swap
	 * @return				true if node to swap can be swapped with current node
	 */
	public boolean canSwap(PaceTreeNode nodeToSwap) {
		
		if ( nodeToSwap != null) {
		
			int swapNodeIndex = nodeToSwap.getProperties().getAxisDimIndex();
			
			//if the current axis dim index equals the node to swap axis dim, and this isn't a member tag
			//and node to swap isn't text only
			if ( this.getProperties() != null && this.getProperties().getAxisDimIndex() != null &&
					this.getProperties().getAxisDimIndex() == swapNodeIndex &&
					! this.isType(PaceTreeNodeType.MemberTag) 
					&& ! this.isType(PaceTreeNodeType.Blank)
					&& ! nodeToSwap.isType(PaceTreeNodeType.Blank)
					&& ! nodeToSwap.isType(PaceTreeNodeType.TextOnly) ) {
				
				/*
				//if blank and parent is blank or node trying to be added isn't on last dim axis, return false
				if ( this.isType(PaceTreeNodeType.Blank)) {
					
					int axisDimSize = this.getProperties().getAxisDimSize();
					
					if ( swapNodeIndex != axisDimSize || this.getParentNode().isType(PaceTreeNodeType.Blank) ) {
						return false;
					}
					
				}*/
				
				return true;
				
			}
			
		}
		
		return false;
	}
	
	/**
	 * 
	 *  Swaps node passed in with current node.  Doesn't really swap, more so replaces current node
	 *  with node passed in
	 *
	 * @param nodeToSwap	node to swap
	 * @return				reference to new node replacing old current node
	 */
	public PaceTreeNode swap(PaceTreeNode nodeToSwap) {
		
		PaceTreeNode newSwappedNode = null;
		
		if ( canSwap(nodeToSwap)) {
		
			PaceTreeNode parentNode = this.getParentNode();
			
			int childNdx = parentNode.getChildIndex(this);
			
			PaceTreeNodeProperties nodeToSwapProperties = nodeToSwap.getProperties();
			
			//try to clone
			try {
				nodeToSwapProperties = nodeToSwapProperties.clone();
				
			} catch (CloneNotSupportedException e) {
				e.printStackTrace(); //will never happen
			}
			
			//remove old selection type, per requirement NRF-141
			nodeToSwapProperties.setSelectionType(PaceTreeNodeSelectionType.Selection);
			nodeToSwapProperties.setMemberOfTuple(true);
			
			newSwappedNode = new PaceTreeNode(nodeToSwap.getName(), nodeToSwapProperties, parentNode, childNdx);
			
			if ( ! nodeToSwap.isType(PaceTreeNodeType.Blank) ) {
				
				for (PaceTreeNode childNode : this.getChildren()) {
					
					childNode.setParentNode(newSwappedNode);
					
				}
				
				newSwappedNode.setChildren(this.getChildren());
							
			} 
				
			//if is a view tuple, swap the view tuple properties
			if ( this.isViewTuple() ) {
				
				newSwappedNode.setViewTuple(this.getViewTuple());
				
			}
			
			this.remove();
		
		}
		
		return newSwappedNode;
	}
	
	/**
	 * 
	 * Sets all alias tables for current node, additional pace tree nodes and children nodes.
	 * 
	 * @param aliasTable the aliasTable to set
	 */
	public void setAliasTable(String aliasTable) {

		//set current alias table
		properties.setAliasTableToDisplay(aliasTable);
		
		//loop through additional pace tree node list and set alias table on them
		for (PaceTreeNode additionalPaceTreeNode : this.additionalPaceTreeNodeList) {
			
			additionalPaceTreeNode.setAliasTable(aliasTable);
			
		}
		
		//get children and set alias table on them
		List<PaceTreeNode> paceTreeNodeChildren = getChildren();
		
		if ( paceTreeNodeChildren != null ) {
			
			for (PaceTreeNode childNode : paceTreeNodeChildren) {
				
				childNode.setAliasTable(aliasTable);
				
			}			
			
		}
	}

	/**
	 * 
	 * Checks if the axis dim size is same as axis dim index and if member of a tuple.  
	 * If the axis only has 1 dim on it, then any node with axis index of 1 would be considered a view tuple.  
	 * If the axis has 3 dims on it, then any node with axis index of 3 would be considered a view tuple.
	 *
	 * @return	true if a view tuple
	 */
	public boolean isViewTuple() {		
		
		boolean isViewTuple = false;;
		
		if ( this.properties != null && this.properties.isMemberOfTuple() && this.properties.getAxisDimIndex() != null 
				&& this.properties.getAxisDimSize() != null && this.properties.getAxisDimIndex() == this.properties.getAxisDimSize()) {
			
			return true;
			
		}	
		
		return isViewTuple;
		
	}
	
	/**
	 * 
	 * Returns a view tuple if is a view tuple and uses existing view tuple for properties.
	 *
	 * @return
	 */
	public ViewTuple getViewTuple() {
		
		ViewTuple viewTuple = null;
		
		if ( isViewTuple() ) {
		
			//clone view tuple
			viewTuple = this.viewTuple.clone();
									
			List<String> memberDefList = new ArrayList<String>();
			
			List<Integer> symetricGroupNumberList = new ArrayList<Integer>();
			
			List<PaceTreeNode> parentNodesList = PaceTreeUtil.getParentNodesAsList(this);
			
			for ( PaceTreeNode parentNodeFromList : parentNodesList ) {
				
				String memberDef = getMemberDefFromNode(parentNodeFromList);
				
				memberDefList.add(memberDef);
				
				if ( parentNodeFromList.getSymetricGroupNumber() != null ) {
					
					symetricGroupNumberList.add(parentNodeFromList.getSymetricGroupNumber());
					
				}
								
								
			}	
			
			//add current member def to list of member defs
			memberDefList.add(getMemberDefFromNode(this));
			
			viewTuple.setMemberDefs(memberDefList.toArray(new String[0]));
			
			if ( symetricGroupNumberList.size() > 0 ) {
				viewTuple.setSymetricGroupNo(symetricGroupNumberList.toArray(new Integer[0]));
			}
			
			viewTuple.setMemberTag(isType(PaceTreeNodeType.MemberTag));			
			
		}	
		
		return viewTuple;
	}
	
	/**
	 * 
	 * Gets the member definition form the pace node.
	 *
	 * @param node	node to get member definition from
	 * @return		member definition
	 */
	private String getMemberDefFromNode(PaceTreeNode node) {

		String memberDef = node.getDisplayName();
		
		//if blank, set as paf blank
		if ( node.isType(PaceTreeNodeType.Blank)) {
			
			memberDef = PafBaseConstants.PAF_BLANK;
			
		//if selection of members, replace all Blank with PAF_BLANKS
		} else if ( node.isSelectionType(PaceTreeNodeSelectionType.Members) ) {
							
			String memberDefForMembers = node.getDisplayName();
			
			//replace all occurrences of Blank to PAFBLANK
			memberDef = memberDefForMembers.replaceAll(PaceTreeNodeType.Blank.toString(), PafBaseConstants.PAF_BLANK);
			
		} 
		
		return memberDef;
	}

	/**
	 * 
	 * If view tuple, sets view tuple passed in
	 *
	 * @param viewTuple	new view tuple
	 */
	public void setViewTuple(ViewTuple viewTuple) {
		
		if ( isViewTuple() ) {
			
			this.viewTuple = viewTuple;
			
		}	
		
	}
	
	/**
	 * 
	 *  Compares if node is of a Pace Tree Node type
	 *
	 * @param type	PaceTreeNodeType
	 * @return		true if current node is of type passed in
	 */
	public boolean isType(PaceTreeNodeType type) {
		
		boolean isType = false;
		
		if ( type != null && this.properties != null && this.properties.getType() != null && this.properties.getType().equals(type)) {
			isType = true;
		}
		
		return isType;
		
	}
	
	/**
	 * 
	 *  Compares if node is of a Pace Tree Selection Node type
	 *
	 * @param selectionType PaceTreeNodeSelectionType
	 * @return 				true if current node is of selection type passed in
	 */
	public boolean isSelectionType(PaceTreeNodeSelectionType selectionType) {
		
		boolean isSelectionType = false;
		
		if ( selectionType != null && this.properties != null && this.properties.getSelectionType() != null && this.properties.getSelectionType().equals(selectionType)) {
			isSelectionType = true;
		}
		
		return isSelectionType;
		
	}
	
	/**
	 * @return the symetricGroupNumber
	 */
	public Integer getSymetricGroupNumber() {
		return symetricGroupNumber;
	}

	/**
	 * @param symetricGroupNumber the symetricGroupNumber to set
	 */
	public void setSymetricGroupNumber(Integer symetricGroupNumber) {
		this.symetricGroupNumber = symetricGroupNumber;
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("Node: " + name);
		
		for (PaceTreeNode child : children) {
			
			sb.append("\n\tChild: " + child.getName());
			
		}
		
		return sb.toString();
	}	

	public boolean isSelectionTypeValidForSorting() {
		if( isSelectionType(PaceTreeNodeSelectionType.Selection) || isSelectionType(PaceTreeNodeSelectionType.None) ) {
			PaceTreeNode parent = this.getParentNode();
			do {
				if( ! parent.isSelectionType(PaceTreeNodeSelectionType.Selection) && ! parent.isSelectionType(PaceTreeNodeSelectionType.None)
						&& ! parent.getName().equalsIgnoreCase("Root") )
					return false;
			} while(  (parent = parent.getParentNode()) != null && ! parent.getName().equalsIgnoreCase("Root") );
			return true;
		}
		else
			return false;
	}

	public boolean isNodeTypeValidForSorting() {
		if( isType(PaceTreeNodeType.UOWRoot) || isType(PaceTreeNodeType.UserSelSingle) 
				|| isType(PaceTreeNodeType.MultiYearSingleMember) || isType(PaceTreeNodeType.MultiYearMultiMember)
				|| isType(PaceTreeNodeType.PaceMember) || isType(PaceTreeNodeType.PaceMemberLevel0) || isType(PaceTreeNodeType.DynamicMember) ) {
			PaceTreeNode parent = this.getParentNode();
			do {
				if( ! parent.getName().equalsIgnoreCase("Root") && ! isType(PaceTreeNodeType.UOWRoot) && ! isType(PaceTreeNodeType.UserSelSingle) 
						&& ! isType(PaceTreeNodeType.MultiYearSingleMember) && ! isType(PaceTreeNodeType.MultiYearMultiMember) 
						&& ! isType(PaceTreeNodeType.PaceMember) && ! isType(PaceTreeNodeType.PaceMemberLevel0) && ! isType(PaceTreeNodeType.DynamicMember) )
					return false;
			} while(  (parent = parent.getParentNode()) != null && ! parent.getName().equalsIgnoreCase("Root") );
			return true;
		}
		else
			return false;
	}
	
	
	/**
	 * 
	 * Gets the child node if there is a child of childIndex
	 *
	 * @param childIndex  	index for childNode 
	 * @return				childNode
	 */
	public PaceTreeNode getChildNodeOfIndex(int childIndex) {
		
		PaceTreeNode childNd ;
		childNd = this.getChildren().get(childIndex);
		return childNd;
		
	}
}

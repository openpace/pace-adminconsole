/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pace.admin.global.exceptions.InvalidStateException;
import com.pace.admin.global.util.CollectionsUtil;
import com.pace.admin.global.util.PaceTreeUtil;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.base.view.PageTuple;

/**
 * Model to hold the PageTupleNode's.
 *
 * @version	1.0
 * @author JavaJ
 *
 */
public class PageTupleList {

	//list of axis dimension names
	private List<String> axisDims;
	
	//map to hold the page tuple nodes
	private Map<String, PageTupleNode> pageTupleNodeMap = new HashMap<String, PageTupleNode>();
			
	/**
	 * 
	 * Constructor - creates an map with all axis dims as keys.  Initially the map values are null.
	 * 
	 * @param axisDims			list of axis dimension names
	 * @throws PafException		throws a paf exception
	 */
	public PageTupleList(List<String> axisDims) throws PafException {
		
		if ( axisDims != null ) {
						
			this.axisDims = new ArrayList<String>(axisDims);
			
			for (String axisDim : axisDims) {
				
				pageTupleNodeMap.put(axisDim, null);
				
			}
			
		} else {
			
			throw new PafException("Axis dims is null.", PafErrSeverity.Error);
			
		}
	
	}
	
	/**
	 * 
	 * Constructor - creates a page tuple list from existing page tuples.
	 * 
	 * @param pageTuples		page tuples to create page tuple list from
	 * @param pafDimTreeMap		map that contains PafSimpleDimTree's, used to get PafSimpleDimMembers
	 * @throws PafException
	 */
	public PageTupleList(List<PageTuple> pageTuples, HashMap<String, PafSimpleDimTree> pafDimTreeMap) throws PafException {		
						
		axisDims = new ArrayList<String>();
		
		for (PageTuple pageTuple : pageTuples) {
			
			String dimName = pageTuple.getAxis();
			String memberName = pageTuple.getMember();
			
			Map<String, String> aliasTableMap = null;
			
			//if dim tree map contains dimension name, get tree and loop over members.  If member is found, 
			//create an alias table map <key = alias table name> and <value = alias value>
			if ( pafDimTreeMap != null && pafDimTreeMap.containsKey(dimName) ) {
			
				PafSimpleDimTree pafSimpleDimTree = pafDimTreeMap.get(dimName);
				
				PafSimpleDimMember simpleDimMember = PaceTreeUtil.getPafSimpleDimMember(pafSimpleDimTree, memberName);
				
				if ( simpleDimMember != null ) {
					
					aliasTableMap = CollectionsUtil.createMapFromTwoLists(simpleDimMember.getPafSimpleDimMemberProps().getAliasKeys(), simpleDimMember.getPafSimpleDimMemberProps().getAliasValues());
					
				}
												
			} 		
			
			PageTupleNode pageTupleNode = new PageTupleNode(dimName, memberName, aliasTableMap);
			
			pageTupleNodeMap.put(dimName, pageTupleNode);
			
			axisDims.add(dimName);
			
		}
		
	}
	
	
	/**
	 * 
	 * Gets the page tuple nodes from the map.
	 *
	 * @return a list of page tuple nodes
	 */
	public List<PageTupleNode> getPageTupleNodes() {
		
		List<PageTupleNode> pageTupleNodeList = new ArrayList<PageTupleNode>();
		
		for (String axisDim : axisDims) {
			
			pageTupleNodeList.add(pageTupleNodeMap.get(axisDim));			
			
		}
				
		return pageTupleNodeList;
	}
		
	/**
	 * 
	 *  Adds an item to the page tuple node map.
	 *
	 * @param nodeToAdd  Page tuple node to add to map
	 */
	public void addItem(PageTupleNode nodeToAdd) {
		
		if ( pageTupleNodeMap.containsKey(nodeToAdd.getDimensionName())) {
			
			pageTupleNodeMap.put(nodeToAdd.getDimensionName(), nodeToAdd);
			
		}
		
	}
	
	/**
	 * 
	 *  Removes an item from the page tuple node map
	 *
	 * @param nodeToRemove node to remove from map
	 */
	public void removeItem(PageTupleNode nodeToRemove) {
		
		if ( pageTupleNodeMap.containsKey(nodeToRemove.getDimensionName())) {
						
			pageTupleNodeMap.put(nodeToRemove.getDimensionName(), null);
			
		}
		
	}
	

	/**
	 * @param aliasTable the aliasTable to set
	 */
	public void setAliasTableToDisplay(String aliasTableName) {

		for (PageTupleNode paceTupleNode : pageTupleNodeMap.values()) {
			
			if ( paceTupleNode != null ) {
			
				paceTupleNode.setAliasTableName(aliasTableName);
				
			}
			
		}
		
	}

	/**
	 * 
	 * If dimensionName is in page tuple node map and value exists, returns true, else false.
	 *
	 * @param dimensionName
	 * @return
	 */
	public boolean doesItemExist(String dimensionName) {
						
		return ( pageTupleNodeMap.get(dimensionName) == null ) ? false : true;
		
	}

	/**
	 * @return the axisDims
	 */
	public List<String> getAxisDims() {
		return axisDims;
	}

	/**
	 * 
	 * Validates the state of the PageTupleList.  All keys in the page tuple node map must
	 * have NON null values.
	 *
	 * @throws InvalidStateException Thrown when the map doesn't contain an axis dim key or value is null
	 */
	public void validate() throws InvalidStateException {		
		
		for (String axisDim : axisDims) {
			
			if ( ! pageTupleNodeMap.containsKey(axisDim) ||  pageTupleNodeMap.get(axisDim) == null  || pageTupleNodeMap.get(axisDim).getName() == null ) {
				throw new InvalidStateException("Invalid Page axis member for dimension "+axisDim + ".");
			}
			
		}
		
	}
	
	/**
	 * 
	 *  Creates an ordered list of page tuples from the page tuple node map.
	 *
	 * @return ordered list of page tuples
	 */
	public List<PageTuple> getPageTuples() throws InvalidStateException {
		
		validate();
		
		List<PageTuple> pageTuples = new ArrayList<PageTuple>();

		for (String axisDim : axisDims) {
			
			PageTupleNode pageTupleNode = pageTupleNodeMap.get(axisDim);
			
			PageTuple pageTuple = new PageTuple();
			
			pageTuple.setAxis(axisDim);
			pageTuple.setMember(pageTupleNode.getName());
			
			pageTuples.add(pageTuple);
			
		}
		
		
		return pageTuples;
	}
	
}

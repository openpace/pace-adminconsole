/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.Map;

/**
 * Used in a PageTupleList.  Contains a name and a dimension name.  Supports alias tables.
 *
 * @version	1.0
 * @author JavaJ
 *
 */
public class PageTupleNode {

	//current alias table name
	private String aliasTableName;
	
	//name of page member
	private String name;
	
	//name of dimension
	private String dimensionName;
	
	//alias table map that has: key=alias table name, value=alias member
	private Map<String, String> aliasTableMap;

	/**
	 * 
	 * Constructor - used to create a page tuple node
	 * 
	 * @param dimensionName	name of dimension
	 * @param name			name of member
	 * @param aliasTableMap map that contains alias info
	 */
	public PageTupleNode(String dimensionName, String name, Map<String, String> aliasTableMap) {
		
		this.dimensionName = dimensionName;
		
		this.name = name;
		
		this.aliasTableMap = aliasTableMap;
		
	}

	/**
	 * @return the member name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * Uses alias member to create a display name.  If alias table name isn't present, member name is returned.
	 *
	 * @return formated name
	 */
	public String getDisplayName() {
		
		String aliasMemberName = name;
		
		if ( aliasTableName != null && aliasTableMap != null && aliasTableMap.containsKey(aliasTableName)) {
			
			aliasMemberName = aliasTableMap.get(aliasTableName);
			
		}
		
		return aliasMemberName;
		
	}

	/**
	 * @return the dimensionName
	 */
	public String getDimensionName() {
		return dimensionName;
	}

	/**
	 * @return the aliasTableName
	 */
	public String getAliasTableName() {
		return aliasTableName;
	}

	/**
	 * @param aliasTableName the aliasTableName to set
	 */
	public void setAliasTableName(String aliasTableName) {
		this.aliasTableName = aliasTableName;
	}
	
}

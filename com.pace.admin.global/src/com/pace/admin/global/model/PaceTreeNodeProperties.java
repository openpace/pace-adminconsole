/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.Map;
import java.util.TreeMap;

import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.util.CollectionsUtil;
import com.pace.base.SortOrder;
import com.pace.base.SortPriority;
import com.pace.server.client.PafSimpleDimMember;

/**
 * Supports a pace tree node by providing selection type, type, axis index, axis dim size,
 * level/gen numbers, selected alias table, dimension name, etc.    
 *
 * @version	1.0
 * @author JavaJ
 *
 */
public class PaceTreeNodeProperties implements Cloneable {

	public static final int UNDEFINED_TREE_LEVEL_OR_GEN = -1;
	
	public static final int UNDEFINED_DIM_INDEX = -1;
	
	private PaceTreeNodeType type;
	
	private boolean memberOfTuple;
	
	//selection by default
	private PaceTreeNodeSelectionType selectionType = PaceTreeNodeSelectionType.Selection;

	private Integer axisDimIndex;
	
	private Integer axisDimSize;
	
	private int levelNumber = UNDEFINED_TREE_LEVEL_OR_GEN;
	
	private int generationNumber = UNDEFINED_TREE_LEVEL_OR_GEN;	
	
	private int levelGenNumber = UNDEFINED_TREE_LEVEL_OR_GEN;

	private int offsetMinNumber = 0;
	private int offsetMaxNumber = 0;
	
	private String aliasTableToDisplay;
	
	private String dimensionName;
	
	private Map<String, String> aliasTableMap = new TreeMap<String, String>();	
	
	private boolean builtFromViewTuple;
	
	private SortOrder sortOrder;
	
	private SortPriority sortPriority;
	
    private boolean isFreezePaneSet;
    
    

	
	public PaceTreeNodeProperties() {
		
	}
	
	/**
	 * 
	 * @param dimensionName
	 * @param axisDimSize
	 * @param axisDimIndex
	 * @param simpleDimMember
	 * @param type
	 */
	public PaceTreeNodeProperties(String dimensionName, Integer axisDimSize, Integer axisDimIndex, PafSimpleDimMember simpleDimMember, PaceTreeNodeType type ) {
						
		this(dimensionName, axisDimSize, axisDimIndex, simpleDimMember, type, null, false);
		
	}
	
	/**
	 * 
	 * @param dimensionName
	 * @param axisDimSize
	 * @param axisDimIndex
	 * @param simpleDimMember
	 * @param type
	 * @param selectionType
	 */
	public PaceTreeNodeProperties(String dimensionName, Integer axisDimSize, Integer axisDimIndex, PafSimpleDimMember simpleDimMember, PaceTreeNodeType type, PaceTreeNodeSelectionType selectionType) {
				
		this(dimensionName, axisDimSize, axisDimIndex, simpleDimMember, type, selectionType, false);		
		
	}
	
	/**
	 * 
	 * @param dimensionName
	 * @param axisDimSize
	 * @param axisDimIndex
	 * @param simpleDimMember
	 * @param type
	 * @param selectionType
	 * @param isMemberOfTuple
	 */
	public PaceTreeNodeProperties(String dimensionName, Integer axisDimSize, Integer axisDimIndex, PafSimpleDimMember simpleDimMember, PaceTreeNodeType type, PaceTreeNodeSelectionType selectionType, boolean isMemberOfTuple) {
	
		this(dimensionName, axisDimSize, axisDimIndex, simpleDimMember, type, selectionType, isMemberOfTuple, UNDEFINED_TREE_LEVEL_OR_GEN, false);
		
	}
	
	/**
	 * 
	 * @param dimensionName
	 * @param axisDimSize
	 * @param axisDimIndex
	 * @param simpleDimMember
	 * @param type
	 * @param selectionType
	 * @param isMemberOfTuple
	 * @param levelGenNumber
	 * @param builtFromViewTuple
	 */
	public PaceTreeNodeProperties(String dimensionName, Integer axisDimSize, Integer axisDimIndex, PafSimpleDimMember simpleDimMember, PaceTreeNodeType type, PaceTreeNodeSelectionType selectionType, boolean isMemberOfTuple, int levelGenNumber, boolean builtFromViewTuple) {
		this.dimensionName = dimensionName;
		this.axisDimSize = axisDimSize;
		this.axisDimIndex = axisDimIndex;
		
		if ( simpleDimMember != null ) {
			
			if ( simpleDimMember.getPafSimpleDimMemberProps().getAliasKeys() != null && 
					simpleDimMember.getPafSimpleDimMemberProps().getAliasValues() != null ) {
				
				aliasTableMap = CollectionsUtil.createMapFromTwoLists(simpleDimMember.getPafSimpleDimMemberProps().getAliasKeys(),
						simpleDimMember.getPafSimpleDimMemberProps().getAliasValues());
				
			}		
			
			if ( simpleDimMember.getPafSimpleDimMemberProps() != null ) {
				
				this.levelNumber = simpleDimMember.getPafSimpleDimMemberProps().getLevelNumber();
				this.generationNumber = simpleDimMember.getPafSimpleDimMemberProps().getGenerationNumber();
			}
			
		}
		
		this.type = type;
		this.selectionType = selectionType;
		this.memberOfTuple = isMemberOfTuple;
		this.levelGenNumber = levelGenNumber;
		this.builtFromViewTuple = builtFromViewTuple;
	}
		
	/**
	 * 
	 * @param dimensionName
	 * @param axisDimSize
	 * @param axisDimIndex
	 * @param simpleDimMember
	 * @param type
	 * @param selectionType
	 * @param isMemberOfTuple
	 * @param minOffsetNum
	 * @param maxOffsetNum
	 * @param builtFromViewTuple
	 */
	public PaceTreeNodeProperties(String dimensionName, Integer axisDimSize, Integer axisDimIndex, PafSimpleDimMember simpleDimMember, PaceTreeNodeType type, PaceTreeNodeSelectionType selectionType, boolean isMemberOfTuple, int minOffsetNum, int maxOffsetNum, boolean builtFromViewTuple) {
		this.dimensionName = dimensionName;
		this.axisDimSize = axisDimSize;
		this.axisDimIndex = axisDimIndex;
		
		if ( simpleDimMember != null ) {
			
			if ( simpleDimMember.getPafSimpleDimMemberProps().getAliasKeys() != null && 
					simpleDimMember.getPafSimpleDimMemberProps().getAliasValues() != null ) {
				
				aliasTableMap = CollectionsUtil.createMapFromTwoLists(simpleDimMember.getPafSimpleDimMemberProps().getAliasKeys(),
						simpleDimMember.getPafSimpleDimMemberProps().getAliasValues());
				
			}		
			
			if ( simpleDimMember.getPafSimpleDimMemberProps() != null ) {
				
				this.levelNumber = simpleDimMember.getPafSimpleDimMemberProps().getLevelNumber();
				this.generationNumber = simpleDimMember.getPafSimpleDimMemberProps().getGenerationNumber();
			}
			
		}
		
		this.type = type;
		this.selectionType = selectionType;
		this.memberOfTuple = isMemberOfTuple;
		this.offsetMinNumber = minOffsetNum;
		this.offsetMaxNumber = maxOffsetNum;
		this.builtFromViewTuple = builtFromViewTuple;
	}
		
	/**
	 * @return the axisDimIndex, if null, returns -1
	 */
	public Integer getAxisDimIndex() {
		
		if ( axisDimIndex == null ) {
			return UNDEFINED_DIM_INDEX;
		}
		
		return axisDimIndex;
	}

	/**
	 * @param axisDimIndex the axisDimIndex to set
	 */
	public void setAxisDimIndex(Integer axisDimIndex) {
		this.axisDimIndex = axisDimIndex;
	}

	/**
	 * @return the axisDimSize
	 */
	public Integer getAxisDimSize() {
		return axisDimSize;
	}

	/**
	 * @param axisDimSize the axisDimSize to set
	 */
	public void setAxisDimSize(Integer axisDimSize) {
		this.axisDimSize = axisDimSize;
	}

	/**
	 * @return the type
	 */
	public PaceTreeNodeType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(PaceTreeNodeType type) {
		this.type = type;
	}

	/**
	 * @return the selectionType
	 */
	public PaceTreeNodeSelectionType getSelectionType() {
		return selectionType;
	}

	/**
	 * @param selectionType the selectionType to set
	 */
	public void setSelectionType(PaceTreeNodeSelectionType selectionType) {
		this.selectionType = selectionType;
	}
	
	/**
	 * @return the dimensionName
	 */
	public String getDimensionName() {
		return dimensionName;
	}

	/**
	 * 
	 * @return alias table map
	 */
	public Map<String, String> getAliasTableMap() {
		
		return (new TreeMap<String, String>(this.aliasTableMap));
		
	}

	/**
	 * 
	 * @return true if property has alias members
	 */
	public boolean hasAliasTables() {
		return (aliasTableMap.size() == 0) ? false : true;
	}
	
	/**
	 * @return the aliasTableToDisplay
	 */
	public String getAliasTableToDisplay() {
		return aliasTableToDisplay;
	}

	/**
	 * @param aliasTableToDisplay the aliasTableToDisplay to set
	 */
	public void setAliasTableToDisplay(String aliasTableToDisplay) {
		this.aliasTableToDisplay = aliasTableToDisplay;
	}

	/**
	 * @return the levelNumber
	 */
	public int getLevelNumber() {
		return levelNumber;
	}

	/**
	 * @param levelNumber the levelNumber to set
	 */
	public void setLevelNumber(int levelNumber) {
		this.levelNumber = levelNumber;
	}

	/**
	 * @return the generationNumber
	 */
	public int getGenerationNumber() {
		return generationNumber;
	}

	/**
	 * @param generationNumber the generationNumber to set
	 */
	public void setGenerationNumber(int generationNumber) {
		this.generationNumber = generationNumber;
	}

	/**
	 * @param dimensionName the dimensionName to set
	 */
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	public String getAliasValue() {
		
		String aliasValue = null;
		
		if ( this.aliasTableToDisplay != null && aliasTableMap.containsKey(this.aliasTableToDisplay)) {
			aliasValue = aliasTableMap.get(this.aliasTableToDisplay );
		} 
		
		return aliasValue;		
	}
	
		

	/**
	 * @return the memberOfTuple
	 */
	public boolean isMemberOfTuple() {
		return memberOfTuple;
	}

	/**
	 * @param memberOfTuple the memberOfTuple to set
	 */
	public void setMemberOfTuple(boolean memberOfTuple) {
		this.memberOfTuple = memberOfTuple;
	}	
	
	/**
	 * @return the levelGenNumber
	 */
	public int getLevelGenNumber() {
		return levelGenNumber;
	}

	/**
	 * @param levelGenNumber the levelGenNumber to set
	 */
	public void setLevelGenNumber(int levelGenNumber) {						
		this.levelGenNumber = levelGenNumber;		
	}

	
	
	/**
	 * @return the builtFromViewTuple
	 */
	public boolean isBuiltFromViewTuple() {
		return builtFromViewTuple;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PaceTreeNodeProperties clone() throws CloneNotSupportedException {
		
		try {
			
			PaceTreeNodeProperties properties = (PaceTreeNodeProperties) super.clone();
			
			properties.aliasTableMap = new TreeMap<String, String>(this.aliasTableMap);
					
			return properties;
			
		} catch (CloneNotSupportedException e) {
			throw new AssertionError();  // Can't happen
		}
		
	}
	
	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SortPriority getSortPriority() {
		return sortPriority;
	}

	public void setSortPriority(SortPriority sortPriority) {
		this.sortPriority = sortPriority;
	}

	public int getOffsetMinNumber() {
		return offsetMinNumber;
	}

	public void setOffsetMinNumber(int offsetMinNumber) {
		this.offsetMinNumber = offsetMinNumber;
	}

	public int getOffsetMaxNumber() {
		return offsetMaxNumber;
	}

	public void setOffsetMaxNumber(int offsetMaxNumber) {
		this.offsetMaxNumber = offsetMaxNumber;
	}
	
	public boolean isFreezePaneSet() {
        return isFreezePaneSet;
    }

    public void setFreezePaneSet(boolean isFreezePaneSet) {
        this.isFreezePaneSet = isFreezePaneSet;
    }

}

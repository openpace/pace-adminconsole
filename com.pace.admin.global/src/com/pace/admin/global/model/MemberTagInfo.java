/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.pace.server.client.MemberTagInformation;

/**
 * AC version of Member Tag Info
 *
 * @version	x.xx
 * @author JavaJ
 *
 */
public class MemberTagInfo {

	//application id/name
	private String appId;
	
	private String overrideAppId;
			
	//key = member tag; value = number of member tag data items
	private Map<String, Integer> memberTagCountMap = new HashMap<String, Integer>();
	
	//filter to hold
	private Set<String> memberTagFilterSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	
	private MemberTagInformation memberTagInfo;
	
	public MemberTagInfo(MemberTagInformation memberTagInfo	) {
		
		this.memberTagInfo = memberTagInfo;
		
		if ( memberTagInfo != null ) {
			
			this.appId = memberTagInfo.getAppName();
			
			if ( memberTagInfo.getMemberTagNames() != null && memberTagInfo.getMemberTagCounts() != null 
					&& memberTagInfo.getMemberTagNames().size() == memberTagInfo.getMemberTagCounts().size() ) {
			
				
				for (int i = 0; i < memberTagInfo.getMemberTagNames().size(); i++ ) {
					
					//push member tag and data counts into the map
					memberTagCountMap.put(memberTagInfo.getMemberTagNames().get(i), memberTagInfo.getMemberTagCounts().get(i));
					
				}
				
				
				
			}			
			
		}
		
	}
	
	/**
	 * 
	 * Sets the member tag data item count for all member tags.
	 *
	 * @param memberTagDataItemCount number of member tag data items
	 */
	public void setMemberTagDataItemCount(int memberTagDataItemCount) {
		
		if ( memberTagInfo != null ) {
			
			memberTagInfo.setAppMemberTagCount(memberTagDataItemCount);
			
		}
		
	}
	
	/**
	 * 
	 *  Returns the number of member tag data item counts.  If a filter is specified, only returns filter count.
	 *
	 * @return number of member tag data itmes, either all or based on filter counts.
	 */
	public int getMemberTagDataItemCount() {
		
		int memberTagDataItemCnt = 0;
		
		//if no filters are present, set count as total member tag count
		if ( memberTagFilterSet.size() == 0  ) {
			
			memberTagDataItemCnt = memberTagInfo.getAppMemberTagCount();
			
		//else add up all member tag filter counts
		} else {
									
			for ( String memberTagFilter : memberTagFilterSet ) {
			
				if ( memberTagCountMap.containsKey(memberTagFilter) ) {
				
					memberTagDataItemCnt += memberTagCountMap.get(memberTagFilter);
					
				}
				
			}
			
			
		}
		
		return memberTagDataItemCnt;
	}
	
	/**
	 * 
	 * Clears the filter set.
	 *
	 */
	public void resetMemberTagFilter() {
		
		memberTagFilterSet.clear();
		
	}
	
	/**
	 * 
	 *  Sets the member tag filter items.  Clears existing member tag filter set and then
	 *  adds the new set.
	 *
	 * @param memberTagFilters
	 */
	public void setMemberTagFilters(String[] memberTagFilters) {
		
		resetMemberTagFilter();
		
		if ( memberTagFilters != null ) {
		
			memberTagFilterSet.addAll(Arrays.asList(memberTagFilters));
			
		}		
		
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the memberTagInfo
	 */
	public MemberTagInformation getMemberTagInfo() {
		return memberTagInfo;
	}
	
	/**
	 * 
	 * Gets the member tag filters and can sort them.
	 *
	 * @param haveSorted sort member tags
	 * @return array of member tag names
	 */
	public String[] getMemberTagFilters(boolean haveSorted) {
		
		String[] memberTagFilterAr = null;
		
		if (memberTagFilterSet.size() > 0) {			
			
			memberTagFilterAr = memberTagFilterSet.toArray(new String[0]);
	
			if (haveSorted) {
	
				Arrays.sort(memberTagFilterAr);
	
			}

		}

		return memberTagFilterAr;
	}
	
	/**
	 * 
	 * Gets the member tag names from the member tag info
	 *
	 * @return array of member tag names
	 */
	public String[] getMemberTagNames() {
		
		String[] memberTagNames = null;
		
		if ( memberTagInfo != null)  {
			
			memberTagNames = memberTagInfo.getMemberTagNames().toArray(new String[0]);
			
		}		
		
		return memberTagNames;		
		
	}

	/**
	 * @return the overrideAppId
	 */
	public String getOverrideAppId() {
		return overrideAppId;
	}

	/**
	 * @param overrideAppId the overrideAppId to set
	 */
	public void setOverrideAppId(String overrideAppId) {
		this.overrideAppId = overrideAppId;
	}
}

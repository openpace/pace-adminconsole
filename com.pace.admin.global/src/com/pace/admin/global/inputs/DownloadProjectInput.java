/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.inputs;

import org.eclipse.core.runtime.jobs.JobChangeAdapter;

import com.pace.base.ui.PafServer;

/**
 * Input used for downloading project
 * 
 * @author JMilliron
 *
 */
public class DownloadProjectInput {

	private String projectName;
	
	private PafServer pafServer;
	
	private boolean slientMode;
	
	private boolean hotDeploy;
	
	private JobChangeAdapter jobChangeAdapter;

	public DownloadProjectInput(String projectName, PafServer pafServer) {
		super();
		this.projectName = projectName;
		this.pafServer = pafServer;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the pafServer
	 */
	public PafServer getPafServer() {
		return pafServer;
	}

	/**
	 * @param pafServer the pafServer to set
	 */
	public void setPafServer(PafServer pafServer) {
		this.pafServer = pafServer;
	}

	
	
	/**
	 * @return the slientMode
	 */
	public boolean isSlientMode() {
		return slientMode;
	}

	/**
	 * @param slientMode the slientMode to set
	 */
	public void setSlientMode(boolean slientMode) {
		this.slientMode = slientMode;
	}

	/**
	 * @return the hotDeploy
	 */
	public boolean isHotDeploy() {
		return hotDeploy;
	}

	/**
	 * @param hotDeploy the hotDeploy to set
	 */
	public void setHotDeploy(boolean hotDeploy) {
		this.hotDeploy = hotDeploy;
	}

	/**
	 * @return the jobChangeAdapter
	 */
	public JobChangeAdapter getJobChangeAdapter() {
		return jobChangeAdapter;
	}

	/**
	 * @param jobChangeAdapter the jobChangeAdapter to set
	 */
	public void setJobChangeAdapter(JobChangeAdapter jobChangeAdapter) {
		this.jobChangeAdapter = jobChangeAdapter;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DownloadProjectInput [projectName=" + projectName
				+ ", pafServer=" + pafServer + ", slientMode=" + slientMode
				+ ", hotDeploy=" + hotDeploy + ", jobChangeAdapter="
				+ jobChangeAdapter + "]";
	}
		
}

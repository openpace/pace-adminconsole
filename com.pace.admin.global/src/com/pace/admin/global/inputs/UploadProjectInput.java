/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.inputs;

import java.util.Set;

import org.eclipse.core.runtime.jobs.JobChangeAdapter;

import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;

/**
 * Input for Upload Project job
 * 
 * @author JMilliron
 *
 */
public class UploadProjectInput {

	private String projectName;
	
	private PafServer pafServer;

	private boolean applyConfigChanges;
	
	private boolean applyCubeChanges;
	
	private boolean slientMode;
	
	private boolean hotDeploy;
	
	private JobChangeAdapter jobChangeAdapter;
	
	private Set<ProjectElementId> filterSet;
	
	private String successfulMessage;
	
	private String unsuccessfulMessage;

	public UploadProjectInput(String projectName) {
		this(projectName, null);
	}
	
	public UploadProjectInput(String projectName, PafServer pafServer) {
		super();
		this.projectName = projectName;
		this.pafServer = pafServer;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the pafServer
	 */
	public PafServer getPafServer() {
		return pafServer;
	}

	/**
	 * @param pafServer the pafServer to set
	 */
	public void setPafServer(PafServer pafServer) {
		this.pafServer = pafServer;
	}

	/**
	 * @return the applyConfigChanges
	 */
	public boolean isApplyConfigChanges() {
		return applyConfigChanges;
	}

	/**
	 * @param applyConfigChanges the applyConfigChanges to set
	 */
	public void setApplyConfigChanges(boolean applyConfigChanges) {
		this.applyConfigChanges = applyConfigChanges;
	}

	/**
	 * @return the applyCubeChanges
	 */
	public boolean isApplyCubeChanges() {
		return applyCubeChanges;
	}

	/**
	 * @param applyCubeChanges the applyCubeChanges to set
	 */
	public void setApplyCubeChanges(boolean applyCubeChanges) {
		this.applyCubeChanges = applyCubeChanges;
	}

	/**
	 * @return the slientMode
	 */
	public boolean isSlientMode() {
		return slientMode;
	}

	/**
	 * @param slientMode the slientMode to set
	 */
	public void setSlientMode(boolean slientMode) {
		this.slientMode = slientMode;
	}

	/**
	 * @return the hotDeploy
	 */
	public boolean isHotDeploy() {
		return hotDeploy;
	}

	/**
	 * @param hotDeploy the hotDeploy to set
	 */
	public void setHotDeploy(boolean hotDeploy) {
		this.hotDeploy = hotDeploy;
	}

	/**
	 * @return the jobChangeAdapter
	 */
	public JobChangeAdapter getJobChangeAdapter() {
		return jobChangeAdapter;
	}

	/**
	 * @param jobChangeAdapter the jobChangeAdapter to set
	 */
	public void setJobChangeAdapter(JobChangeAdapter jobChangeAdapter) {
		this.jobChangeAdapter = jobChangeAdapter;
	}
	
	/**
	 * @return the filterSet
	 */
	public Set<ProjectElementId> getFilterSet() {
		return filterSet;
	}

	/**
	 * @param filterSet the filterSet to set
	 */
	public void setFilterSet(Set<ProjectElementId> filterSet) {
		this.filterSet = filterSet;
	}

	
	
	
	/**
	 * @return the successfulMessage
	 */
	public String getSuccessfulMessage() {
		return successfulMessage;
	}

	/**
	 * @param successfulMessage the successfulMessage to set
	 */
	public void setSuccessfulMessage(String successfulMessage) {
		this.successfulMessage = successfulMessage;
	}

	/**
	 * @return the unsuccessfulMessage
	 */
	public String getUnsuccessfulMessage() {
		return unsuccessfulMessage;
	}

	/**
	 * @param unsuccessfulMessage the unsuccessfulMessage to set
	 */
	public void setUnsuccessfulMessage(String unsuccessfulMessage) {
		this.unsuccessfulMessage = unsuccessfulMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UploadProjectInput [projectName=" + projectName
				+ ", pafServer=" + pafServer + ", applyConfigChanges="
				+ applyConfigChanges + ", applyCubeChanges=" + applyCubeChanges
				+ ", slientMode=" + slientMode + ", hotDeploy=" + hotDeploy
				+ ", jobChangeAdapter=" + jobChangeAdapter + ", filterSet="
				+ filterSet + ", successfulMessage=" + successfulMessage
				+ ", unsuccessfulMessage=" + unsuccessfulMessage + "]";
	}
}

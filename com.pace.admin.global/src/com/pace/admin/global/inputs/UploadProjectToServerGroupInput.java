/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.inputs;

import java.util.List;

import com.pace.base.ui.PafServer;

public class UploadProjectToServerGroupInput extends UploadProjectInput {

	private List<PafServer> pafServers;
	private String groupName;
	
	public UploadProjectToServerGroupInput(String projectName, String groupName, List<PafServer> pafServers) {
		super(projectName);
		this.pafServers = pafServers;
		this.groupName = groupName;
	}

	/**
	 * @return the pafServer
	 */
	public List<PafServer> getPafServers() {
		return pafServers;
	}

	/**
	 * @param pafServer the pafServer to set
	 */
	public void setPafServers(List<PafServer> pafServers) {
		this.pafServers = pafServers;
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}

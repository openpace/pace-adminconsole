/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * Custom message box with a "Do not show me again" option.
 * @author kmoos
 *
 */
public class CustomDialogMessageBox extends Dialog {
	
//	private Button okButton;
//	private Button cancelButton;
	private Label labelMessage;
	private Button buttonMessage;
	private String dialogTitle;
	private String message;
	private String checkBoxMessage;
	
	private boolean checkBoxMessageChecked;
	
	
	/**
	 * 
	 * @param parentShell shell
	 * @param dialogTitle title of the dialog
	 * @param message message to display to the user.
	 * @param checkBoxMessage message to display in the checkbox.
	 */
	public CustomDialogMessageBox(Shell parentShell, String dialogTitle, String message, 
			String checkBoxMessage) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.dialogTitle = dialogTitle;
		this.message = message;
		this.checkBoxMessage = checkBoxMessage;
		
	}

	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if(dialogTitle != null){
			newShell.setText(dialogTitle);
		}
	}
	
	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
//		GridLayout gridLayout_1 = (GridLayout) container.getLayout();
		{
			labelMessage = new Label(container, SWT.NONE);
			labelMessage.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			labelMessage.setText("New Label");
		}
		new Label(container, SWT.NONE);
		{
			buttonMessage = new Button(container, SWT.CHECK);
			buttonMessage.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			buttonMessage.setText("Check Button");
		}
		
		if(message != null){
			labelMessage.setText(message);
		}
		if(checkBoxMessage != null){
			buttonMessage.setText(checkBoxMessage);
		}
		
		
		return container;
	}
	
	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, true);

	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	protected void buttonPressed(int buttonId) {
		//set status
		if (buttonId == IDialogConstants.OK_ID) {
			
			setCheckBoxMessageChecked(buttonMessage.getSelection());
			
		}
		
		super.buttonPressed(buttonId);
	}

	public boolean isCheckBoxMessageChecked() {
		return checkBoxMessageChecked;
	}

	public void setCheckBoxMessageChecked(boolean checkBoxMessageChecked) {
		this.checkBoxMessageChecked = checkBoxMessageChecked;
	}
	
}

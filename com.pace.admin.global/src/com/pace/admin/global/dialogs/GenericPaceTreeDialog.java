/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.PafSimpleDimTreeUtil;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

/**
 * Generic dialog that displays a pace tree and allows user to select 0 or more itmes in the tree.
 * For now, this only supports PafSimpleDimTree and PafSimpleDimMembers.
 * 
 * @author JMilliron
 *
 */
public class GenericPaceTreeDialog extends Dialog {
	
	private static final Logger logger = Logger.getLogger(GenericPaceTreeDialog.class);
	
	private static Map<String, PafSimpleDimMember> memberMap = new HashMap<String, PafSimpleDimMember>();
	
	private String dialogTitle;
	
	private PafSimpleDimTree pafSimpleDimTree;
	
	private PafSimpleDimMember rootSimpleDimMember = null;
	
	private List<String> checkedMemberList = new ArrayList<String>();

	private CheckboxTreeViewer checkboxTreeViewer;
	
	private List<String> initialCheckedItems;
	
	private static class ViewerLabelProvider extends LabelProvider {
		
		public Image getImage(Object element) {
			//no images needed
			return null;
			
		}
		
		public String getText(Object element) {
			
			//if element is a paf simple dim member, cast and get key
			if ( element instanceof PafSimpleDimMember) {
				
				logger.debug("ViewerLabelProvider.getText() instanceof PafSimpleDimMember");
				
				PafSimpleDimMember member = (PafSimpleDimMember) element;
												
				return member.getKey();
				
			//if string, cast and return
			} else if ( element instanceof String ) {
				
				logger.debug("ViewerLabelProvider.getText() instanceof String");
				
				return (String) element;
				
			}
			
			return null;
		}
	}
	private static class TreeContentProvider implements ITreeContentProvider {
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object inputElement) {
			return getChildren(inputElement);
		}
		public Object[] getChildren(Object parentElement) {
			//holds refs to child keys
			String[] childKeys = null;
			
			//if parenet element is a tree, return root of tree as child
			if ( parentElement instanceof PafSimpleDimTree) {
				
				logger.debug("TreeContentProvider.getChildren() instanceof PafSimpleDimTree");
				
				PafSimpleDimTree tree = (PafSimpleDimTree) parentElement;
										
				childKeys = new String[] { tree.getRootKey() };
																
			//else if parent is type of simple dim member, return child keys
			} else if ( parentElement instanceof PafSimpleDimMember) {
				
				logger.debug("TreeContentProvider.getChildren() instanceof PafSimpleDimMember");
				
				PafSimpleDimMember member = (PafSimpleDimMember) parentElement;
				
				if ( member.getChildKeys().size() > 0 ) {
				
					childKeys = member.getChildKeys().toArray(new String[0]);
					
				}
				
				
			}
			
			//create empty list
			List<PafSimpleDimMember> children = new ArrayList<PafSimpleDimMember>();
			
			//if child keys exists, loop through them and add the to the children map
			if ( childKeys != null ) {
				
				for (String childKey : childKeys) {
					
					if ( memberMap.containsKey(childKey)) {
						
						children.add(memberMap.get(childKey));
						
					}
					
				}
				
			}
			
			//convert to array and return
			return children.toArray(new PafSimpleDimMember[0]);
		}
		public Object getParent(Object element) {
			
			//if paf simple dim member
			if ( element instanceof PafSimpleDimMember) {
				
				//cast into simple dim member
				PafSimpleDimMember member = (PafSimpleDimMember) element;
				
				//get parent key
				String parentKey = member.getParentKey();
				
				//if member map contains the parent, return parent member
				if ( memberMap.containsKey(parentKey)) {
					return memberMap.get(parentKey);
				}
				
				
			}
			
			return null;
		}
		public boolean hasChildren(Object element) {
			return getChildren(element).length > 0;
		}
	}

	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public GenericPaceTreeDialog(Shell parentShell, String dialogTitle, PafSimpleDimTree pafSimpleDimTree, List<String> initialCheckedItems) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.dialogTitle = dialogTitle;
		
		this.pafSimpleDimTree = pafSimpleDimTree;
				
		if ( pafSimpleDimTree != null ) {
			
			//convert tree into member map
			memberMap = PafSimpleDimTreeUtil.convertTreeIntoHashMap(pafSimpleDimTree);
			
			String rootMemberKey = pafSimpleDimTree.getRootKey();
			
			//get root member
			if ( memberMap.containsKey(rootMemberKey)) {
				rootSimpleDimMember = memberMap.get(rootMemberKey);
			}
			
		}
		
		this.initialCheckedItems = initialCheckedItems;
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));
		
		Label lblMeasuresTree = new Label(composite, SWT.NONE);
		lblMeasuresTree.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblMeasuresTree.setBounds(0, 0, 55, 15);
		lblMeasuresTree.setText("Select Members:");
		
		checkboxTreeViewer = new CheckboxTreeViewer(composite, SWT.BORDER);
		checkboxTreeViewer.addCheckStateListener(new ICheckStateListener() {
			public void checkStateChanged(CheckStateChangedEvent event) {
				//everytime user checks or unchecks item, update checks state
				updateCheckState();				
				
			}
		});
		checkboxTreeViewer.setAutoExpandLevel(2);
		Tree dimensionTree = checkboxTreeViewer.getTree();
		dimensionTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		checkboxTreeViewer.setLabelProvider(new ViewerLabelProvider());
		checkboxTreeViewer.setContentProvider(new TreeContentProvider());
		
		if ( pafSimpleDimTree != null ) {
			
			checkboxTreeViewer.setInput(pafSimpleDimTree);
						
		} else {
			
			logger.error("createDialogArea() - pafSimpleDimTree is null");
			
		}
		
		//if there is at least one initially checked item, check items in tree viewer
		if ( initialCheckedItems != null && initialCheckedItems.size() > 0 && memberMap != null )  {
			
			List<PafSimpleDimMember> initialCheckedItemList = new ArrayList<PafSimpleDimMember>(); 
			
			for (String initialCheckedItem : initialCheckedItems ) {
				
				if ( memberMap.containsKey(initialCheckedItem)) {
					
					initialCheckedItemList.add(memberMap.get(initialCheckedItem));
					
				}
				
			}
			
			EditorControlUtil.setCheckedElements(checkboxTreeViewer, initialCheckedItemList.toArray());
			
		}
		
		final Button btnSelectDeselect = new Button(composite, SWT.CHECK);
		
		//if every item is checked in tree viewere, check select/deselect button
		if ( checkboxTreeViewer != null && memberMap != null) {
						
			if ( memberMap.size() != 0 &&
					memberMap.size() == checkboxTreeViewer.getCheckedElements().length) {
				
				btnSelectDeselect.setSelection(true);
				
			}			
			
		}
		
		btnSelectDeselect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				//check or uncheck all items in tree based on button state
				checkboxTreeViewer.setSubtreeChecked(rootSimpleDimMember, btnSelectDeselect.getSelection());
				
				updateCheckState();
								
			}
		});
		btnSelectDeselect.setBounds(0, 0, 93, 16);
		btnSelectDeselect.setText("Select / Deselect All");
		
		return container;
	}

	/**
	 * Clears the checked members list and adds all checked items to list
	 */
	protected void updateCheckState() {

		if ( checkboxTreeViewer != null) {
									
			checkedMemberList.clear();
			
			Object[] checkedObjectAr = checkboxTreeViewer.getCheckedElements();
			
			for (Object checkedObject : checkedObjectAr ) {
				
				if ( checkedObject instanceof PafSimpleDimMember)
				
				checkedMemberList.add(((PafSimpleDimMember) checkedObject).getKey());
				
			}
								
		}
		
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(325, 464);
	}

	/**
	 * @return the checkedMemberList
	 */
	public List<String> getCheckedMemberList() {
		return checkedMemberList;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if(dialogTitle != null){
			newShell.setText(dialogTitle);
		}
	}
	
}

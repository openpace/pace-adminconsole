/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.dialogs;

import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.constants.Constants;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafServer;
import com.swtdesigner.SWTResourceManager;

public class UploadLocalProjectDialog extends Dialog {
	private static class ViewerLabelProvider extends LabelProvider {
		public Image getImage(Object element) {
			return super.getImage(element);
		}
		public String getText(Object element) {
			
			if ( element instanceof ProjectElementId) {
				return ((ProjectElementId) element).getName();
			}
			
			return super.getText(element);
		}
	}
	
	private static class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			
			if ( inputElement instanceof Set) {
				
				return ((Set) inputElement).toArray();
				
			}
			
			return new Object[0];
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	private PafServer server;
	private String appId;
	private boolean applyConfig = true;
	private boolean applyCube = false;
	private boolean doNotShowPrompt = false;
	private boolean hotDeploy = false;

	private Button btnApplyServerConfig;
	private Button btnApplyCubeChanges;
	private Label lblUploadProject;
	private Button btnPrompt;
	private Composite composite;
	private Composite optionalComposite;
	private Label lblTheFollowingItems;
	
	private Set<ProjectElementId> uploadProjectItems;
//	private Label label_1;
	private Label blankLabel;
	private String labelOverride;
	
	/**
	 * @wbp.parser.constructor
	 */
	public UploadLocalProjectDialog(Shell parentShell, String labelOverride, Set<ProjectElementId> uploadProjectItems) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.labelOverride = labelOverride;
		this.server = null;
		this.appId = null;		
		this.hotDeploy = false; //only show prompt for hot deploy
		this.uploadProjectItems	= uploadProjectItems; //only show when uploading individual project items
	}
	
	
	public UploadLocalProjectDialog(Shell parentShell, PafServer server, String appId, boolean hotDeploy, Set<ProjectElementId> uploadProjectItems) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.server = server;
		this.appId = appId;		
		this.hotDeploy = hotDeploy; //only show prompt for hot deploy
		this.uploadProjectItems	= uploadProjectItems; //only show when uploading individual project items
	}
	

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Upload Local Project" );
	}
	
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		createButton(parent, IDialogConstants.YES_ID, IDialogConstants.YES_LABEL,
				true);
		createButton(parent, IDialogConstants.NO_ID,
				IDialogConstants.NO_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 25;
		container.setLayout(gridLayout);
		
//		GridData gd_lblProject = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
		
		composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		composite.setLayout(new GridLayout(1, false));
		
		lblUploadProject = new Label(composite, SWT.WRAP | SWT.SHADOW_IN);
		GridData gd_lblUploadProject = new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1);
		gd_lblUploadProject.widthHint = 297;
		gd_lblUploadProject.heightHint = 44;
		lblUploadProject.setLayoutData(gd_lblUploadProject);
		lblUploadProject.setSize(316, 25);
		lblUploadProject.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		
		blankLabel = new Label(composite, SWT.NONE);
		GridData gd_blankLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		blankLabel.setLayoutData(gd_blankLabel);
		
		optionalComposite = new Composite(composite, SWT.NONE);
		optionalComposite.setLayout(new GridLayout(1, false));
		GridData gd_optionalComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		optionalComposite.setLayoutData(gd_optionalComposite);
		
		lblTheFollowingItems = new Label(optionalComposite, SWT.NONE);
		lblTheFollowingItems.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblTheFollowingItems.setBounds(0, 0, 55, 15);
		lblTheFollowingItems.setText("The following items will be uploaded:");
		
		ListViewer listViewer = new ListViewer(optionalComposite, SWT.BORDER | SWT.V_SCROLL);
		List projectElementIdList = listViewer.getList();
		GridData gd_projectElementIdList = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_projectElementIdList.heightHint = 45;
		gd_projectElementIdList.minimumHeight = 48;
		projectElementIdList.setLayoutData(gd_projectElementIdList);
		projectElementIdList.setBounds(0, 0, 88, 68);
		listViewer.setLabelProvider(new ViewerLabelProvider());
		listViewer.setContentProvider(new ContentProvider());
		listViewer.setInput(uploadProjectItems);
		
		btnApplyServerConfig = new Button(composite, SWT.CHECK);
		btnApplyServerConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnApplyServerConfig.setSize(314, 25);
		btnApplyServerConfig.setEnabled(true);
		btnApplyServerConfig.setSelection(true);
		btnApplyServerConfig.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setApplyConfig( btnApplyServerConfig.getSelection() );
			}
		});
		btnApplyServerConfig.setText(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
		
		btnApplyCubeChanges = new Button(composite, SWT.CHECK);
		btnApplyCubeChanges.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnApplyCubeChanges.setSize(300, 25);
		btnApplyCubeChanges.setEnabled(true);
		btnApplyCubeChanges.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setApplyCube( btnApplyCubeChanges.getSelection() );
			}
		});
		btnApplyCubeChanges.setText(Constants.START_OR_RESTART_APPLICATION);
		
		btnPrompt = new Button(composite, SWT.CHECK);
		btnPrompt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnPrompt.setSize(314, 25);
		btnPrompt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setDoNotShowPrompt(btnPrompt.getSelection());
			}
		});
		btnPrompt.setText("Do not prompt this again");
		btnPrompt.setVisible(hotDeploy);
		if(labelOverride == null || labelOverride.trim().length() == 0){
			lblUploadProject.setText("Upload Project '" + appId + "' to Server '" + server.getName() + "'?");
		} else {
			lblUploadProject.setText(labelOverride);
		}
		
		if ( uploadProjectItems != null && uploadProjectItems.size() > 0) {
			gd_blankLabel.exclude = true;
		} else {
			gd_optionalComposite.exclude = true;			
		}
		
		return container;
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		if (IDialogConstants.YES_ID == buttonId) {
			super.okPressed();
		} else if (IDialogConstants.NO_ID == buttonId) {
			super.cancelPressed();
		}
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(335, 327);
	}
	
	public boolean isApplyConfig() {
		return applyConfig;
	}

	public void setApplyConfig(boolean applyConfig) {
		this.applyConfig = applyConfig;
	}

	public boolean isApplyCube() {
		return applyCube;
	}

	public void setApplyCube(boolean applyCube) {
		this.applyCube = applyCube;
	}

	public boolean isDoNotShowPrompt() {
		return doNotShowPrompt;
	}

	public void setDoNotShowPrompt(boolean doNotShowPrompt) {
		this.doNotShowPrompt = doNotShowPrompt;
	}
}

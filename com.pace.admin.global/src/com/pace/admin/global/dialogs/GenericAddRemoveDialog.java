/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.dialogs;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.pace.admin.global.util.EditorControlUtil;

/**
 * Dialog used for selecting users for import/export from/to a CSV file
 * @author ihuang
 *
 */
public class GenericAddRemoveDialog extends Dialog {
	
	private static Logger logger = Logger.getLogger(GenericAddRemoveDialog.class);

	// Table used
	private Table table;
	// Checkbox table viewer widget
	private CheckboxTableViewer checkboxTableViewer;
	private Button okButton;
	
	private String title;
	private String subtitle;
	private boolean minRequired;
	private String[] allItems;
	private String[] selectedItems;
	/**
	 * Create the dialog (constructor)
	 * @param parentShell
	 */
	public GenericAddRemoveDialog(Shell parentShell, String title, String subtitle, String[] allItems, String[] selectedItems, boolean minRequired) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
	    // Get the server URL
		this.title = title;
		this.subtitle = subtitle;
		this.minRequired = minRequired;
        this.allItems = allItems; 
        this.selectedItems = selectedItems;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		// Create the container
		Composite container = (Composite) super.createDialogArea(parent);
		
		final GridLayout gridLayout = new GridLayout();
		
		gridLayout.marginRight = 5;
		
		gridLayout.marginLeft = 5;
		
		container.setLayout(gridLayout);
		
		Label lblNewLabel = new Label(container, SWT.WRAP);
		GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.TOP, true, false);
		gd_lblNewLabel.heightHint = 51;
		gd_lblNewLabel.widthHint = 339;
		lblNewLabel.setLayoutData(gd_lblNewLabel);
		lblNewLabel.setText(subtitle);
		

		// Create the table container
		final Composite TableComposite = new Composite(container, SWT.NONE);
		
		final GridData gd_tableComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		
		gd_tableComposite.widthHint = 753;
		
		TableComposite.setLayoutData(gd_tableComposite);
		
		TableComposite.setLayout(new GridLayout());
		
	    final Composite tableComposite = new Composite(TableComposite, SWT.NONE);
	    tableComposite.setLayoutData(new GridData(347, SWT.DEFAULT));
		
		tableComposite.setLayout(new GridLayout());

		// Create a check box table viewer 
		checkboxTableViewer = CheckboxTableViewer.newCheckList(tableComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		
		table = checkboxTableViewer.getTable();
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.heightHint = 293;
		table.setLayoutData(gd_table);
		
		EditorControlUtil.addElements(checkboxTableViewer, allItems);
		
		EditorControlUtil.setCheckedElements(checkboxTableViewer, selectedItems);
		// Disable or enable the OK button depending whether anything was selected
		checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {

			public void checkStateChanged(CheckStateChangedEvent arg0) {

				enableOKButton();
				
			}
		});
		
		// Create button container
		final Composite buttonComposite = new Composite(container, SWT.NONE);
		
		buttonComposite.setLayoutData(new GridData(179, SWT.DEFAULT));
		
		final GridLayout gridLayout_1 = new GridLayout();
		
		gridLayout_1.makeColumnsEqualWidth = true;
		
		gridLayout_1.numColumns = 2;
		
		buttonComposite.setLayout(gridLayout_1);

		
		// Create Check All button
		final Button checkAllButton = new Button(buttonComposite, SWT.NONE);
		
		checkAllButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				checkboxTableViewer.setAllChecked(true);
				// Enable the OK button
				okButton.setEnabled(true);

			}
		});
		
		GridData gd_checkAllButton = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_checkAllButton.widthHint = 58;
		checkAllButton.setLayoutData(gd_checkAllButton);
		
		checkAllButton.setText("Check All");

		
		// Create Uncheck All button
		final Button uncheckAllButton = new Button(buttonComposite, SWT.NONE);
		
		uncheckAllButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
        		checkboxTableViewer.setAllChecked(false);
				
				// Disable the OK button
        		if( minRequired )
        			okButton.setEnabled(false);
				
			}
		});

		uncheckAllButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		uncheckAllButton.setText("Uncheck All");
		
		return container;
	}


	protected void enableOKButton() {
		if(minRequired && checkboxTableViewer.getCheckedElements().length == 0){
			
			okButton.setEnabled(false);
			
		}
		else{
			
			okButton.setEnabled(true);
			
		}
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		// Create OK and cancel buttons
		okButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		enableOKButton();

	}
	

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		
		return new Point(392, 523);
		
	}
	
	/**
	 * Implement the event of selecting the OK button
	 */
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
			String[] checkedItems = new String[checkboxTableViewer.getCheckedElements().length];
			for(int i = 0; i < checkedItems.length; i++){
				checkedItems[i] = checkboxTableViewer.getCheckedElements()[i].toString();
			}
        	selectedItems = checkedItems;
		}
		
		super.buttonPressed(buttonId);
		
	}
	

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}
	
	public String[] getAllItems() {
		return allItems;
	}

	public void setAllItems(String[] allItems) {
		this.allItems = allItems;
	}

	public String[] getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}

}

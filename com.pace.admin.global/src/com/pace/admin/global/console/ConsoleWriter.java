/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.console;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ConsoleUtil;

/**
 * Writes messages to the specified console.  If console is null, will be written to default console.
 * 
 * @author JMilliron
 *
 */
public class ConsoleWriter {
	
	private static Map<String, MessageConsole> messageConsoleMap = new ConcurrentHashMap<String, MessageConsole>();
	
	public static void writeMessage(String message) {
		writeMessage(Constants.CONSOLE_NAME, message);
	}
	
	public static void writeMessage(String consoleName, String message) {
		
		if ( consoleName == null)  {
			consoleName = Constants.CONSOLE_NAME;
		}
		
		if ( message != null ) {
			
			MessageConsole msgConsole = null;
									
			if ( messageConsoleMap.containsKey(consoleName) && messageConsoleMap.get(consoleName) != null ) {
				
				msgConsole = messageConsoleMap.get(consoleName);
				
			} else {
							
				msgConsole = ConsoleUtil.findConsole(consoleName);				
				messageConsoleMap.put(consoleName, msgConsole);
				
			}
		
			if ( msgConsole != null ) {
				
				msgConsole.activate();
			
				MessageConsoleStream msgStream = msgConsole.newMessageStream();
				
				msgStream.println(Constants.CONSOLE_SIMPLE_DATE_FORMAT.format(Calendar.getInstance().getTime()) + " - " + message);
				
			}
		
		}
		
		
	}
	

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;

import com.Ostermiller.util.StringTokenizer;
import com.pace.base.utility.FileUtils;

/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 * 
 */
public class FileHelperUtil {

	private static Logger logger = Logger.getLogger(FileHelperUtil.class);	
	
	/**
	 * 
	 *	Move the contents of an iFolder to another iFolder. 
	 *
	 * @param fromIFolder	from folder
	 * @param toIFolder		to folder
	 * @param filesToCopy   collection of resources to copy
	 * @throws CoreException
	 */
	public static void copyContentsOfIFolder(IFolder fromIFolder, IFolder toIFolder, 
			Set<IResource> filesToCopy) throws CoreException {
		
		//if the from folder exists
		if ( fromIFolder != null && fromIFolder.exists() ) {
		
			NullProgressMonitor monitor = new NullProgressMonitor();
			
			//if to folder doesn't exists, create
			if ( ! toIFolder.exists() ) {
				
				toIFolder.create(true, true, null);
				
			} 
			
			//for each resource in from folder, copy to new folder
			for (IResource resource : fromIFolder.members() ) {
				
				IPath newPathForResource = null;
				
				if(filesToCopy.contains(resource)){
				
				
					if ( resource instanceof IFolder) {
					
						IFolder fromFolder = (IFolder) resource;
						
						IFolder backupFolder = toIFolder.getFolder(resource.getName());
						
						copyContentsOfIFolder(fromFolder, backupFolder, true);
						
					
					} else if ( resource instanceof IFile) {
						
						newPathForResource = toIFolder.getFile(resource.getName()).getFullPath();	
						
						IFile file = toIFolder.getFile(resource.getName());
						
						file.delete(true, null);
						
					}
	
					//if new path isn't null, move from old to new folder
					if ( newPathForResource != null)  {
						
						resource.copy(newPathForResource, true, monitor);
						
					}
					
				}
				
			}
			
		}	
		
	}
	
	private static void copyOrMoveContentsOfIFolder(IFolder fromIFolder, IFolder toIFolder, boolean deleteNewFolderContentsBeforeChange, boolean isCopy, boolean isMove ) throws CoreException {
		
		//if the from folder exists
		if ( fromIFolder != null && fromIFolder.exists() ) {
		
			//if flag is true, delete contents of to folder
			if ( deleteNewFolderContentsBeforeChange ) {
			
				toIFolder.delete(true, null);
								
			}
			
			//if to folder doesn't exists, create
			if ( ! toIFolder.exists() ) {
				
				toIFolder.create(true, true, null);
				
			} 
			
			//for each resource in from folder, copy to new folder
			for (IResource resource : fromIFolder.members() ) {
				
				IPath newPathForResource = null;
				
				if ( resource instanceof IFolder) {
				
					newPathForResource = toIFolder.getFolder(resource.getName()).getFullPath();
					
				} else if ( resource instanceof IFile) {
					
					newPathForResource = toIFolder.getFile(resource.getName()).getFullPath();							
				}

				//if new path isn't null, copy from old to new folder
				if ( newPathForResource != null)  {
					
					if ( isCopy ) {
						
						resource.copy(newPathForResource, true, null);
						
					} else if ( isMove ) {
						
						resource.move(newPathForResource, true, null);
						
					}
					
					
				}
				
			}
			
			
		}	
		
	}
	
	/**
	 * 
	 *	Move the contents of an iFolder to another iFolder. 
	 *
	 * @param fromIFolder	from folder
	 * @param toIFolder		to folder
	 * @param deleteNewFolderContentsBeforeMove flag to specify to delete contents from the "to" folder.
	 * @throws CoreException
	 */
	public static void moveContentsOfIFolder(IFolder fromIFolder, IFolder toIFolder, boolean deleteNewFolderContentsBeforeMove ) throws CoreException {
		
		copyOrMoveContentsOfIFolder(fromIFolder, toIFolder, deleteNewFolderContentsBeforeMove, false, true);
		
	}
	
	/**
	 * 
	 *	Copy the contents of an iFolder to another iFolder. 
	 *
	 * @param fromIFolder	from folder
	 * @param toIFolder		to folder
	 * @param deleteNewFolderContentsBeforeCopy flag to specify to delete contents from the "to" folder.
	 * @throws CoreException
	 */
	public static void copyContentsOfIFolder(IFolder fromIFolder, IFolder toIFolder, boolean deleteNewFolderContentsBeforeCopy ) throws CoreException {
		
		copyOrMoveContentsOfIFolder(fromIFolder, toIFolder, deleteNewFolderContentsBeforeCopy, true, false);
		
	}	
		
	
	/**
	 * 
	 *	Import directory from file system into an IFolder
	 *
	 * @param fromDirectory	os directory
	 * @param toIFolder		eclipse iFolder
	 * @param fileFilter	exclusion file filiter
	 */
	public static void importDirectoryFromFileSystem(File fromDirectory,
			IFolder toIFolder, String fileFilter) {

		//if from and to folders exists
		if (fromDirectory != null && fromDirectory.isDirectory()
				&& toIFolder != null) {

			//get to folder
			File toFolder = new File(toIFolder.getLocation().toString());

			if (!toFolder.exists()) {
				toFolder.mkdir();
			}

			for (File fromFile : fromDirectory.listFiles()) {

				if (fileFilter != null) {

					if (fromFile.getName().startsWith(fileFilter)) {
						continue;
					}
				}

				if (fromFile.isDirectory()) {

					IFolder newFolder = toIFolder.getFolder(fromFile.getName());

					importDirectoryFromFileSystem(fromFile, newFolder,
							fileFilter);

				} else if (fromFile.isFile()) {

					// get ref to to file
					IFile toIFile = toIFolder.getFile(fromFile.getName());

					try {

						importFileFromFileSystem(fromFile, toIFile);

					} catch (IOException e) {

						logger.error("There was a problem copying file: "
								+ fromFile);

					}

				}

			}

		}

	}

	/**
	 * 
	 *	exports IFolder to the file system
	 *
	 * @param fromIFolder
	 * @param toFolder
	 * @throws IOException
	 */
	public static void exportDirectoryToFileSystem(IFolder fromIFolder,
			File toFolder) throws IOException {

		if (fromIFolder == null || !fromIFolder.exists()) {
			// throw exception
		}

		if (!toFolder.exists()) {

			logger.info("Creating directory " + toFolder.getPath());

			toFolder.mkdir();

		}

		IResource[] resources = null;

		try {

			resources = fromIFolder.members();

		} catch (CoreException e) {

			logger.error("Core Exception occurred: " + e.getMessage());

		}

		if (resources != null) {

			for (IResource resource : resources) {

				if (resource instanceof IFile) {

					IFile fromIFile = (IFile) resource;

					File toFile = new File(toFolder.getPath() + File.separator
							+ fromIFile.getName());

					exportFileToFileSystem(fromIFile, toFile);

				} else if (resource instanceof IFolder) {

					IFolder iFolder = (IFolder) resource;

					File newToFolder = new File(toFolder.getPath()
							+ File.separator + iFolder.getName());

					logger.info("Copying directory "
							+ resource.getLocation().toString() + " to "
							+ newToFolder + ".");

					exportDirectoryToFileSystem(iFolder, newToFolder);

				}

			}

		}

	}

	/**
	 * 
	 *	Exports an IFile to the File System
	 *
	 * @param fromIFile
	 * @param toFile
	 * @throws IOException
	 */
	public static void exportFileToFileSystem(IFile fromIFile, File toFile)
			throws IOException {

		File fromFile = new File(fromIFile.getLocation().toString());

		logger.info("Copying file " + fromFile + " to " + toFile + ".");

		FileUtils.copy(fromFile, toFile);

	}

	/**
	 * 
	 *	Imports a File from the file system
	 *
	 * @param fromFile
	 * @param toIFile
	 * @throws IOException
	 */
	public static void importFileFromFileSystem(File fromFile, IFile toIFile)
			throws IOException {

		File toFile = new File(toIFile.getLocation().toString());

		logger.info("Copying file " + fromFile + " to " + toFile + ".");

		FileUtils.copy(fromFile, toFile);

	}
	
	/**
	 * 
	 *	Gets and IFile from an IFolder
	 *
	 * @param iFolder
	 * @param fileName
	 * @return
	 */
	public static IFile getIFileFromIFolder(IFolder iFolder, String fileName) {
		
		IFile iFile = null;

		if ( iFolder != null && iFolder.exists() ) {
		
			iFile = iFolder.getFile(fileName);
			
		}		
		
		return iFile;		
		
	}
	
	/**
	 * Check if a file exists
	 * @param pathFileName
	 * @return
	 */
	public static boolean doesFileExist(String pathFileName){
		
		boolean r = false;
		
		if(pathFileName == null || pathFileName == ""){
			return r;
		}
		
		File f = new File(pathFileName);
		
		if(f != null){
			
			if(f.exists()){
				
				r=true;
				
			}
			
		}
		
		return r;
		
	}
		
	/**
	 * 
	 * Searches an IFile's path to see if it contains the iFolderName
	 * 
	 * @param iFile			file's path to search
	 * @param iFolderName	name of folder to searc
	 * @return				true if folder is in files path
	 */
	public static boolean isIFileDescendantOfIFolder(IFile iFile, String iFolderName) {
		
		boolean isDescendant = false;
		
		if ( iFile != null && iFolderName != null) {
									
			StringTokenizer st = new StringTokenizer(iFile.getProjectRelativePath().toString(), String.valueOf(IPath.SEPARATOR));
			
			while (st.hasNext()) {
				
				if ( st.nextToken().equals(iFolderName)) {
					
					isDescendant = true;
					break;
					
				}
				
			}			
			
		}		
		
		return isDescendant;
		
		
	}

}

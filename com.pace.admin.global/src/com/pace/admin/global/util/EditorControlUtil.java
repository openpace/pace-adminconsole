/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Common functions used with editor controls.
 * @author kmoos
 * @version	x.xx
 */
public class EditorControlUtil {
	
	private static double LARGEST_PRECISE_INTEGER_IN_DOUBLE = Math.pow(2, 53);
	private static Logger logger = Logger.getLogger(EditorControlUtil.class);
	
	/**
	 * Updates the text in a label box, includes null check.
	 * @param control The SWT text box to update.
	 * @param text New text value.
	 */
	public static void setText(Label control, String text){
		if(control != null && text != null){
			control.setText(text);
		}
	}
	
	/**
	 * Updates the text in a text box, includes null check.
	 * @param control The SWT text box to update.
	 * @param text New text value.
	 */
	public static void setText(Text control, String text){
		if(control != null && text != null){
			control.setText(text);
		}
	}
	
	/**
	 * Adds an array of items to a combo box, after a null check.
	 * @param combo The SWT combo box to update.
	 * @param items The string array of items to add to the combo box.
	 */
	public static void addItems(Combo combo, String[] items){
		if(combo != null && items != null && items.length > 0){
			combo.setItems(items);
		}
	}
	
	/**
	 * Adds an array of items to a combo box, after a null check.
	 * @param combo The SWT combo box to update.
	 * @param items The string set of items to add to the combo box.
	 */
	public static void addItems(Combo combo, java.util.List<String> items){
		if(combo != null && items != null && items.size() > 0){
			String[] test = items.toArray(new String[items.size()]);
			Arrays.sort(test);
			addItems(combo, test);
		}
	}
	
	/**
	 * Adds an array of items to a combo box, after a null check.
	 * @param combo The SWT combo box to update.
	 * @param items The string set of items to add to the combo box.
	 */
	public static void addItems(Combo combo, Set<String> items){
		if(combo != null && items != null && items.size() > 0){
			String[] test = items.toArray(new String[items.size()]);
			Arrays.sort(test);
			addItems(combo, test);
		}
	}
	
	/**
	 * Selects a string item in a combo box, after a null check.
	 * @param combo The SWT combo box to update.
	 * @param item The string item to select.
	 */
	public static void selectItem(Combo combo, String item){
		if(combo == null || item == null)
			return;
		
		int i = combo.indexOf(item);
		if(i >= 0) {
			combo.select(i);
		}
	}
	
	/**
	 * Selects a string item in a list box, after a null check.
	 * @param combo The SWT combo box to update.
	 * @param item The string item to select.
	 */
	public static void selectItem(List list, String item){
		if(list == null || item == null)
			return;
		
		int i = list.indexOf(item);
		if(i >= 0) {
			list.select(i);
		}
	}
	
	/**
	 * Get the position of an item in a combo box, after a null check.
	 * @param combo  The SWT combo box to search.
	 * @param items  The string item to find.
	 * @return The index of the item, or -1 if the item does not exist or is null.
	 * @param item
	 */
	public static int getIndex(Combo combo, String item){
		if(item == null)
			return -1;
		else
			return combo.indexOf(item);
	}

	
	/**
	 * Adds an item to a list box, after a null check.
	 * @param list The SWT list box to search.
	 * @param item The string item to add.
	 * @param ignoreDuplicates allow the same item to be added to the list
	 */
	public static void addItemToList(List list, String item, boolean ignoreDuplicates){
		if(!ignoreDuplicates){
			addItemToList(list, item);
		}else{
			int idx = list.getSelectionIndex();
			if(idx == -1){
				list.add(item);
			}else{
				list.add(item, idx+1);
			}
		}
	}
	
	/**
	 * Adds an item to a list box, after a null check.
	 * @param list The SWT list box to search.
	 * @param item The string item to add.
	 */
	public static void addItemToList(List list, String item){
		int idx = list.getSelectionIndex();
		if(idx == -1){
			if(! itemExistsInList(list, item)){
				list.add(item);
			}
		}else{
			if(! itemExistsInList(list, item)){
				list.add(item, idx+1);
			}
		}
	}
	
	/**
	 * Adds an array items to a list box, after a null check.
	 * @param list  The SWT list box to search.
	 * @param item  The string array of items to add.
	 * @param items
	 */
	public static void addItemsToList(List list, String[] items){
		if(list != null && items != null){
			list.setItems(items);
		}
	}
	
	/**
	 * Remove an array of items from a list box, after a null check.
	 * @param list The SWT list box to search.
	 * @param index The array of indexs to remove.
	 */
	public static void removeItemFromList(List list, int[] index){
		if(index.length > 0){
			list.remove(index);
		}
	}
	
	/**
	 * Searches a list box for a string item.
	 * @param list The SWT list box to search.
	 * @param itemToFind 
	 * @return true if the item if found, or false if the item is not found or null.
	 */
	public static boolean itemExistsInList(List list, String itemToFind){
		if(list == null || itemToFind == null)
			return false;
		
		return !(list.indexOf(itemToFind) == -1);
	}
	
	/**
	 * Inserts an array of items into a table, after a null check.
	 * @param table The jface CheckboxTableView to update.
	 * @param items Object array of items to insert.
	 */
	public static void addElements(CheckboxTableViewer table, Object[] items){
		if(items != null){
			table.add(items);
		}
	}
	
	/**
	 * Selects an array of items into a table, after a null check.
	 * @param table The jface CheckboxTableView to update.
	 * @param items Object array of items to select/check.
	 */
	public static void setCheckedElements(CheckboxTableViewer table, Object[] items){
		if(items != null && items.length > 0 ) {
			table.setCheckedElements(items);
		}
	}
	
	/**
	 * Checks an item into a table, after a null check.
	 * @param table The jface CheckboxTableView to update.
	 * @param item Object item to select/check.
	 */
	public static void checkElement(CheckboxTableViewer table, Object item){
		if(item != null) {
			table.setChecked(item, true);
		}
	}
	
	/**
	 * Checks an array of items into a table, after a null check.
	 * @param table The jface CheckboxTableView to update.
	 * @param item Object item to select/check.
	 */
	public static void checkElements(CheckboxTableViewer table, Object[] items){
		if(items != null && items.length > 0) {
			for(Object item : items){
				checkElement(table, item);
			}
		}
	}
	
	/**
	 * Removes and array of items from a CheckboxTreeViewer.
	 * @param tree The jface CheckboxTreeView to update.
	 * @param items Object array of items to remove.
	 */
	public static void removeElements(CheckboxTableViewer tree, Object[] items){
		if(items != null && items.length > 0){
			tree.remove(items);
		}
	}
	
	/**
	 * Selects an array of items into a table, after a null check.
	 * @param tree The jface CheckboxTreeView to update.
	 * @param items Object array of items to select/check.
	 */
	public static void setCheckedElements(CheckboxTreeViewer tree, Object[] items){
		if(items != null && items.length > 0){
			tree.setCheckedElements(items);
		}
	}
	
	/**
	 * Adds a new node to a tree.
	 * @param tree The SWT tree to update.
	 * @param parent The parent of the new tree node item.
	 * @param name The name of the new node to add.
	 * @return The newly created tree item.
	 */
	public static TreeItem addTreeNode(Tree tree, TreeItem parent, String name) {
		if(tree == null){
			return null;
		}
		
		TreeItem newItem = null;
		if (parent == null) {
			newItem = new TreeItem(tree, SWT.NONE);
		} else {
			newItem = new TreeItem(parent, SWT.NONE);
		}
		newItem.setText(name);
		
		return newItem;
	}
	
	/**
	 * Recursively searches an array of tree items for a string value.
	 * @param treeItems The array of tree items to search.
	 * @param itemText The item text to find (ignore case) .
	 * @return A TreeItem if found, or null if nothing was found.
	 */
	public static TreeItem findTreeItem(TreeItem[] treeItems, String itemText){
		TreeItem node = null;
		//System.out.println("tree size: " + treeItems.length);
		for(TreeItem item : treeItems){
			//System.out.println("node text: " + item.getText());
			if(item.getText().equalsIgnoreCase(itemText)){
				return item;
			} else{
				if(item.getItems().length > 0){
					node = findTreeItem(item.getItems(), itemText);
					if(node != null){
						return node;
					}
				} 
			}
		}
		return node;
	}
	
	/**
	 * Recursively expand all the nodes of a tree.
	 * @param treeItems The array of tree items to expand.
	 */
	public static void expandTree(TreeItem[] treeItems){
		//System.out.println("tree size: " + treeItems.length);
		for(TreeItem item : treeItems){
			item.setExpanded(true);
			if(item.getItemCount() > 0){
				expandTree(item.getItems());
			} 
		}
	}
	
	/**
	 * Recursively collapse all the nodes of a tree.
	 * @param treeItems The array of tree items to collapse.
	 */
	public static void collapseTree(TreeItem[] treeItems){
		//System.out.println("tree size: " + treeItems.length);
		for(TreeItem item : treeItems){
			item.setExpanded(false);
			if(item.getItemCount() > 0){
				collapseTree(item.getItems());
			} 
		}
	}
	
	public static void checkButton(Button button, boolean isChecked) {
		
		if ( button != null ) {
			
			button.setSelection(isChecked);
			
		}
		
	}
	
	/**
	 * 
	 * Returns true only consist of positive integers
	 *
	 * @param str String to verify
	 * @return
	 */
	public static boolean isPositiveNumber(String str) {

		if (str != null && str.matches("[.0123456789]*")) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 * Returns true only consist of positive integers
	 *
	 * @param str String to verify
	 * @return
	 */
	public static boolean isNumeric(String str) {
		if (str != null && !str.isEmpty() && str.matches("[-.0123456789]*")) {
			
			// Update the method to make sure the text passed does not represent 
			// a number that is too big to be accurately stored in a double (TTN-2313).
			try {

				double strAsDouble = Double.parseDouble(str);
				if (strAsDouble <= LARGEST_PRECISE_INTEGER_IN_DOUBLE) {
					return true;
				}
				
			} catch (Exception e) {
	
				logger.error("Error enountered in Method 'isNumeric()': " + e.getMessage());
	
			}
		} 
		
		return false;
	}
	
	
	/**
	 * 
	 * Returns true only consist of positive integers
	 *
	 * @param str String to verify
	 * @return
	 */
	public static boolean isValidPercentNumber(String text) {
		if (isPositiveNumber(text)) {

			try {

				double percent = Double.parseDouble(text);
				
				if( percent >= 0 && percent <= 100 ) 
					return true;
			} catch (Exception e) {
	
				logger.error(e.getMessage());
	
			}

		}

		return false;
	}
	/**
	 * 
	 *  Validates a Text Box for a positive int value.
	 *
	 * @param textBox	Text box to validate
	 * @return	true/false
	 */
	public static boolean isTextValidPositiveInt(Text textBox) {

		String strInTextBox = textBox.getText();

		if (isPositiveNumber(strInTextBox)) {

			try {

				Integer.parseInt(strInTextBox);

				return true;

			} catch (Exception e) {

				logger.error(e.getMessage());

			}

		}

		return false;

	}
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}

	/**
	 * 
	 * Adds items to the list viewer
	 *
	 * @param listViewer	List viewer to have items added too.
	 * @param elements		Items to add.
	 */
	public static void addElements(ListViewer listViewer, Object[] elements) {

		if ( listViewer != null && elements != null ) {
			
			listViewer.add(elements);
			
		}
		
	}	
	
	/**
	 * 
	 * Selects an item in a list viewer.
	 *
	 * @param listViewer   	List viewer to have item selected
	 * @param itemToSelect	Item to select
	 */
	public static void selectItem(ListViewer listViewer, Object itemToSelect) {
		
		if ( listViewer != null && itemToSelect != null ) {
			
			listViewer.setSelection(new StructuredSelection(itemToSelect));
			
		}
		
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.Ostermiller.util.ArrayHelper;

/**
 * Array Utility class
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ArraysUtil {	
	
	private final static Logger logger = Logger.getLogger(ArraysUtil.class);
	
	public static <T> T[] listArrayToArray(T... arr) {
		
		T[] tAr = null;
	
		if ( arr != null && arr.length > 0 ) {
			
			tAr = arr;
			
		}
		
		return tAr;	
		
	}
	
	/**
	 * 
	 *	Removes items from an String array.  If either arguments is null, 
	 *	return the initalAR which could be null or have values in it.
	 *
	 * @param initialAr		Initial array of Strings
	 * @param itemsToRemove	String Array items to be removed
	 * @return	A filtered string array
	 */
	public static String[] remove(String[] initialAr, String[] itemsToRemove) {
		
		//if initial value or itemsToRemove is null, return null
		if (initialAr == null || itemsToRemove == null || itemsToRemove.length == 0 ) {
			
			return initialAr;
			
		}
		
		//create list
		List<String> list = new ArrayList<String>(Arrays.asList(initialAr));
		
		//filter out unwanted items
		list.removeAll(Arrays.asList(itemsToRemove));				
		
		//convert to string []
		return list.toArray(new String[0]);		
		
	}
	
	/**
	 * 
	 *	Removes an item from an String array.  If either arguments is null, 
	 *	return the initalAR which could be null or have values in it.
	 *
	 * @param initialAr		Initial array of Strings
	 * @param itemsToRemove	String Array items to be removed
	 * @return	A filtered string array
	 */
	public static String[] remove(String[] initialAr, String itemToRemove) {
		
		//if initial value or itemsToRemove is null, return null
		if (initialAr == null || itemToRemove == null ) {
			
			return initialAr;
			
		}
							
		
		//convert to string []
		return remove(initialAr, new String[] { itemToRemove });	
		
	}
	
	
	public static void main(String[] args) {
		
//		String[] listAr = new String[] { "jason", "mere", "bob", "roxy" };
//		
//		String[] removeAr = new String[] { "bob", "mere"};
//		
//		ArrayHelper.print(ArraysUtil.remove(listAr, removeAr));
		String myStr = "(ABC, 12)";
		if( myStr.matches(".*, " + "[0123456789]+\\)")) {
			System.out.println("matches.");
		}
		else {
			System.out.println("do not match.");
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class ColorsUtil {

	public static String colorToHexString(Color color) {
		
		String r = Integer.toHexString(color.getRed());
		String g = Integer.toHexString(color.getGreen());
		String b = Integer.toHexString(color.getBlue());
		
		if(r.length() == 1){
			r = "0" + r;
		}
		
		if(g.length() == 1){
			g = "0" + g;
		}
		
		if(b.length() == 1){
			b = "0" + b;
		}
		
		return r + g + b;
	}
	
	public static Color hexStringToColor(Display display, String hex) {
		
		Color color = null;
		
		if ( hex != null && hex.length() == 6 ) {
			
			String red = hex.substring(0, 2);
			String green = hex.substring(2, 4);
			String blue = hex.substring(4, 6);
			
			int redInt = Integer.parseInt(red, 16);
			int greenInt = Integer.parseInt(green, 16);
			int blueInt = Integer.parseInt(blue, 16);
			
			if ( display != null)  {
				
				color = new Color(display, redInt, greenInt, blueInt);
						
				
			}
			
			
		}
		
		return color;
		
	}
	
	public static void main(String[] args) {
		
		String hex = "FF00CC";
		
		Color color = ColorsUtil.hexStringToColor(null, hex);
		
		System.out.println(color);
		
	}
	
}

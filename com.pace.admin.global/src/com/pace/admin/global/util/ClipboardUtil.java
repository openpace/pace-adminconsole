/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
 
/**
 * Utility class for putting files into the system clipboard
 * 
 * @author Alan Farkas
 */
 
public final class ClipboardUtil {
 
    private ClipboardUtil() {
        // Utility class, prevent instantiation
    }
     
    /**
     * Copy a file into the clipboard
     * Assumes the file exists -> no additional check
     * @param fileName - includes the path 
     */
 
    public static void copytoClipboard(String fileName) {
        Display display = Display.getCurrent();
        Clipboard clipboard = new Clipboard(display);
        String[] data = { fileName };
        clipboard.setContents(new Object[] { data },
                new Transfer[] { FileTransfer.getInstance() });
        clipboard.dispose();
    }
}

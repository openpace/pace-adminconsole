/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapUtility {

	/**
	 *  Method_description_goes_here
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		
		String str = "CN=user,CN=builtin,DN=ADG,DN=adg1,DN=COM";
		
		Map<String, Set<String>> keyMap = new HashMap<String, Set<String>>();
		
		String[] keyValueEntrys = str.split(",");
		
		for (String keyValueEntry : keyValueEntrys ) {
			
			String[] keyValue = keyValueEntry.split("=");
			
			String key = keyValue[0];
			String value = keyValue[1];
			
			Set<String> valueSet = keyMap.get(key);
			
			if ( valueSet == null ) {
				
				valueSet = new HashSet<String>();
			}
			
			valueSet.add(value);
			
			keyMap.put(key, valueSet);
			
		}
		
		
		for (String key : keyMap.keySet()) {
			
			StringBuffer strBuff = new StringBuffer(key + "\n");
			
			Set<String> valueFromMap = keyMap.get(key);
			
			if ( valueFromMap != null ) {
				
				for (String value : valueFromMap) {
					
					strBuff.append("\t" + value + "\n");
					
				}
				
			}
			
			System.out.println(strBuff.toString());
			
		}
		

	}

}

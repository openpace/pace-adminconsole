/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import com.pace.admin.global.constants.Constants;

/**
 * @author Jason
 *
 */
public class StringUtil {

	/**************************************************************************
	 * 	Init Cap Words With Spaces
	 * 	@param in string
	 * 	@return init cap
	 */
	public static String initCap (String in)
	{
		if (in == null || in.length() == 0)
			return in;
		//
		boolean capitalize = true;
		char[] data = in.toCharArray();
		for (int i = 0; i < data.length; i++)
		{
			if (data[i] == ' ' || Character.isWhitespace(data[i]))
				capitalize = true;
			else if (capitalize)
			{
				data[i] = Character.toUpperCase (data[i]);
				capitalize = false;
			}
			else
				data[i] = Character.toLowerCase (data[i]);
		}
		return new String (data);
	}	//	initCap


	public static String[] createStringArFromString(String memberList) {
		
		String[] stringAr = null;
		
		if ( memberList.contains(",")) {
			
			stringAr = memberList.split(",");
			
			if ( stringAr != null ) {
				
				for (int i = 0; i < stringAr.length; i++) {
					
					stringAr[i] = stringAr[i].trim();
					
				}
				
			}			
			
		} else {
			
			stringAr = new String[] { memberList };
			
		}
		
		return stringAr;
		
	}
	
	//if this is the default, remove the marker from the end of the key
	public static String removeDefaultMarker(String name) {
		String newName = name;
		if (name.endsWith(Constants.DEFAULT_MARKER)) {
			int position = name.lastIndexOf(Constants.DEFAULT_MARKER);
			newName = name.substring(0, position);
		}
		return newName;
	}

}

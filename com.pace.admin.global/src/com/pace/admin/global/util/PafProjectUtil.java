/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.MessageConsole;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.CustomDialogMessageBox;
import com.pace.admin.global.dialogs.UploadLocalProjectDialog;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.inputs.DownloadProjectInput;
import com.pace.admin.global.inputs.UploadProjectInput;
import com.pace.admin.global.inputs.UploadProjectToServerGroupInput;
import com.pace.admin.global.jobs.DownloadProjectJob;
import com.pace.admin.global.jobs.DownloadProjectJobChangeAdapter;
import com.pace.admin.global.jobs.UploadProjectJob;
import com.pace.admin.global.jobs.UploadProjectJobChangeAdapter;
import com.pace.admin.global.jobs.UploadProjectToServerGroupJob;
import com.pace.admin.global.jobs.UploadProjectToServerGroupJobChangeAdapter;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.funcs.CustomFunctionDef;
import com.pace.base.project.ProjectElementId;
import com.pace.base.ui.PafProject;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.PafXStream;

/**
 * This class is used to provide helper utility methods for a paf project. A paf
 * project is located under the Projects/<project name> directory.
 * 
 * @version 1.00
 * @author jmilliron
 * 
 */
public class PafProjectUtil {

	private static Logger logger = Logger.getLogger(PafProjectUtil.class);
	private static Map<String, PafProject> pafProjectMap = new HashMap<String, PafProject>();
	private static Boolean expandProjectTree = true;
	public static Boolean isProjectAutoConvert(IProject project) {
		
		if(pafProjectMap == null || pafProjectMap.size() == 0) return null;
		if(project == null || project.getName() == null) return null;
		
		PafProject pafProject = pafProjectMap.get(project.getName());
		if(pafProject == null) return null;
		
		return pafProject.isAutoConvertProject();
	}
	
	public static void toggleProjectAutoConvert(IProject project) {
		
		PafProject pafProject = pafProjectMap.get(project.getName());
		if( pafProject.isAutoConvertProject() != null ) {
			pafProject.setAutoConvertProject(!pafProject.isAutoConvertProject());
		}
		else {
			pafProject.setAutoConvertProject(true);
		}
		ACPafXStream.exportObjectToXml(pafProject, project.getFile(Constants.PAF_PROJECT_FILE));

	}
	
	/**
	 * This method tries to get the server name from the paf_project.xml file,
	 * but if the server is invalid or not found, then the default server is
	 * used. If there is no default server, then null is returned.
	 * 
	 * @param project
	 *            the workspace project dir
	 * @return name of project server
	 * @throws PafServerNotFound
	 */
	private static String getProjectServerName(IProject project) {
		
		if(pafProjectMap == null || pafProjectMap.size() == 0) return null;
		
		if(pafProjectMap.containsKey(project.getName())){
			return pafProjectMap.get(project.getName()).getServerName();
		} else {
			return null;
		}
//			throws PafServerNotFound {
//
//		// check for null
//		if (project == null) {
//			logger.error("Project variable is null.");
//			return null;
//		}
//
//		String projectServerName = null;
//		PafProject pafProject = null;
//		// try to get the paf project object
//		try {
//
//			pafProject = (PafProject) ACPafXStream
//					.importObjectFromXml(project
//							.getFile(Constants.PAF_PROJECT_FILE));
//
//		} catch (PafConfigFileNotFoundException e) {
//
//			logger.warn("File '" + Constants.PAF_PROJECT_FILE + "' wasn't found.");
//
//		} catch (Exception ex) {
//			logger.error(ex.getMessage());
//		}
//		
//		// if paf project is not null, get the project server name.
//		//TTN 1723 - Project not picking up default project server from paf_project.xml when no default server set
//		if (pafProject != null) {
//			
//			projectServerName = pafProject.getServerName();
//			
//			if( projectServerName != null ) {
//				
//				return projectServerName;
//				
//			}
//		}
//		
//		// if paf project is null, get the default server name.
//		if ( pafProject == null || projectServerName == null ) {
//			PafServer pafServer = null;
//			
//			try {
//			
//				// if still null, try to get the default
//				pafServer = PafServerUtil.getDefaultServer();
//				
//			} catch (PafServerNotFound psnf) {
//				
////				logger.warn("No default server was found.");
//				String outMessage = psnf.getMessage();
//				logger.warn(outMessage);
//				//TTN 1723 - Project not picking up default project server from paf_project.xml when no default server set
//				throw new PafServerNotFound(outMessage);
//			}
//			
//			//update project file if PafServer is not null
//			if ( pafServer != null ) {
//							
//				projectServerName = pafServer.getName();
//				
//				//create or update PafProject file with the default server 
//				URI uri = project.getLocationURI();
//				if( uri != null ) {
//					File projectDir = new File(project.getLocationURI().getPath().toString());
//					
//					File projectFile = new File(project.getLocationURI().getPath().toString() + File.separator + Constants.PAF_PROJECT_FILE);
//											
//					//if project server name is not null and the project file doesn't exists, create new project file
//					if ( projectServerName != null && projectDir.exists() &&  ! projectFile.exists()) {
//						
//						pafProject = new PafProject();
//						pafProject.setServerName(projectServerName);
//												
//						logger.info("Creating new project file: " + projectFile.toString());
//						
//						ACPafXStream.exportObjectToXml(pafProject, project
//									.getFile(Constants.PAF_PROJECT_FILE));
//						
//						try {
//							
//							project.refreshLocal(1, null);
//	
//						} catch (CoreException e) {
//							
//							logger.error("Problem refreshing project folder: " + e.getMessage());
//							
//						}
//					}
//				}
//				
//			}
//				
//		}
//
//		return projectServerName;

	}
	
	public static void removeProjectFromPafProjectMap(IProject project) {
		pafProjectMap.remove(project.getName());
	}
	
	/**
	 * This method tries to get the server name from the paf_project.xml file,
	 * but if the server is invalid or not found, then the default server is
	 * used. If there is no default server, then null is returned.
	 * 
	 * @param project
	 *            the workspace project dir
	 * @return name of project server
	 * @throws PafServerNotFound
	 */
	public static void addPafProjectMap(IProject project) throws PafServerNotFound {

		if( pafProjectMap.size() == 0 || pafProjectMap.get(project.getName()) == null ) {
			// try to get the paf project object
			PafProject pafProject = null;
			try {
	
				pafProject = (PafProject) ACPafXStream
						.importObjectFromXml(project
								.getFile(Constants.PAF_PROJECT_FILE));
	
			} catch (PafConfigFileNotFoundException e) {
	
				logger.warn("File '" + Constants.PAF_PROJECT_FILE + "' wasn't found.");
	
			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}

			// if paf project is not null, get the project server name.
			//TTN 1723 - Project not picking up default project server from paf_project.xml when no default server set
			String projectServerName = null;
			PafServer pafServer = null;
			if (pafProject != null) {
				
				projectServerName = pafProject.getServerName();
				try {
					pafServer = PafServerUtil.getServer(projectServerName);
					if( pafProject.isAutoConvertProject() == null ) {
						pafProject.setAutoConvertProject(true);
						ACPafXStream.exportObjectToXml(pafProject, project.getFile(Constants.PAF_PROJECT_FILE));
					}
				} catch(PafServerNotFound e) {
					
					logger.info("Server: " + projectServerName + " Not found.");
				}
			}
			else {
				
				if( pafServer == null ) {
					PafServer defaultServer = null;
					try {
						defaultServer = PafServerUtil.getDefaultServer();
					}
					catch(PafServerNotFound psnf) {
						logger.info("Default Server: " + projectServerName + " Not found.");
					}
					
					if( defaultServer == null ) {
						projectServerName = "Local Server";
					}
					else {
						projectServerName = defaultServer.getName();
					}
					//create or update PafProject file with the default server 
					URI uri = project.getLocationURI();
					if( uri != null ) {
						
						File projectDir = new File(project.getLocationURI().getPath().toString());
						if ( projectDir.exists() ) {
							File projectFile = new File(project.getLocationURI().getPath().toString() + File.separator + Constants.PAF_PROJECT_FILE);
							//if project server name is not null and the project file doesn't exists, create new project file
							pafProject = new PafProject();
							pafProject.setServerName(projectServerName);
							
							logger.info("Creating new project file: " + projectFile.toString());
							
							ACPafXStream.exportObjectToXml(pafProject, project.getFile(Constants.PAF_PROJECT_FILE));
							
							try {
								
								project.refreshLocal(1, null);
		
							} catch (CoreException e) {
								
								logger.error("Problem refreshing project folder: " + e.getMessage());
								
							}
						}
					}
				}
			}	
			pafProjectMap.put(project.getName(), pafProject);
		}
	}

	/**
	 * Gets the current servers project.
	 * 
	 * @param project
	 *            the workspace project
	 * @return instance of paf server
	 */
	public static PafServer getProjectServer(IProject project)
			throws PafServerNotFound {

		// get the project server name, then try to get the server
		return PafServerUtil.getServer(getProjectServerName(project));

	}

	public static void setDefaultProjectServer(IProject project, PafServer server) {

		if ( project == null || server == null) {
			return;
		}
		
		PafProject pafProject = new PafProject();
		
		pafProject.setServerName(server.getName());
		
		// TTN-2717 - allow user to set default server on new project
		try {
			addPafProjectMap(project);
			pafProjectMap.get(project.getName()).setServerName(server.getName());
		} catch(NullPointerException e) {
			e.printStackTrace();
		} catch (PafServerNotFound e) {
			e.printStackTrace();
		}
		IFile projectFile = project.getFile(Constants.PAF_PROJECT_FILE);
		
		ACPafXStream.exportObjectToXml(pafProject, projectFile);
		
//		 try to refresh the directory to one depth
		try {
			
			projectFile.getParent().refreshLocal(IResource.DEPTH_INFINITE, null);

		} catch (CoreException e) {
			
			logger.error("Problem refreshing local folder: " + e.getMessage());
			
		}
				
	}
	
	public static boolean getExpandProjectTree() {
		return expandProjectTree;
	}
	
	public static void loadExpandProjectTree() {
		try{
			expandProjectTree = (Boolean) ACPafXStream.importObjectFromXml(Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY, Constants.EXPAND_PROJECT_TREE_FILE);
		}
		catch(PafConfigFileNotFoundException e){
			logger.warn("paf_expand_tree.xml not found.");
			try {
				ACPafXStream.exportObjectToXml(true, Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY, Constants.EXPAND_PROJECT_TREE_FILE);
			} catch(Exception ex){
				logger.warn(String.format("Cannot create file:  %s%s%s.", new Object[]{Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY, File.separator, Constants.EXPAND_PROJECT_TREE_FILE} ));
			}
			
		}
	}
	
	public static void toggleExpandProjectTree(){
		
		expandProjectTree = !expandProjectTree;
		ACPafXStream.getXStream();
		File directory = new File(Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY);
		if(!directory.exists()){
			logger.info("Directory: " + Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY + " not found, creating.");
			directory.mkdirs();
		}
		ACPafXStream.exportObjectToXml(expandProjectTree, Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY, Constants.EXPAND_PROJECT_TREE_FILE);
	}
	/**
	 * Gets the project dimensions
	 * 
	 * @param project
	 *            the workspace project
	 * @return String[] of project dimensions
	 */
	public static String[] getProjectDimensions(IProject project) {

		String[] projectDims = null;

		//get paf apps
		PafApplicationDef[] applications = getPafApplicationDef(project);
		
		//if an app exists
		if (applications != null && applications.length > 0) {
			
			//get project dims
			projectDims = applications[0].getMdbDef().getAllDims();
			
		}

		return projectDims;

	}
	
	/**
	 * Gets the hierarchy dimensions
	 * 
	 * @param project
	 *            the workspace project
	 * @return String[] of hier project dimensions
	 */
	public static String[] getHierarchyDimensions(IProject project) {

		String[] hierDims = null;

		//get paf apps
		PafApplicationDef[] applications = getPafApplicationDef(project);
		
		//if an app exists
		if (applications != null && applications.length > 0) {
			
			//get project dims
			hierDims = applications[0].getMdbDef().getHierDims();
			
		}

		return hierDims;

	}
	
	/**
	 * Gets the non hierarchy dimensions
	 * 
	 * @param project
	 *            the workspace project
	 * @return String[] of non hier project dimensions
	 */
	public static String[] getNonHierarchyDimensions(IProject project) {

		List<String> nonHierDimList = new ArrayList<String>();
		
		//get all dimensions
		String[] allDimensions = getProjectDimensions(project);
		
		//if dimensions are not null, add to non hier list
		if ( allDimensions != null ) {
			nonHierDimList.addAll(Arrays.asList(allDimensions));
		}
		
		//get hier dims
		String[] hierDimensions = getHierarchyDimensions(project);
		
		//if hier dims not null, remove from non hier list
		if ( hierDimensions != null ) {
			nonHierDimList.removeAll(Arrays.asList(hierDimensions));
		}		

		//return non hier dims
		return nonHierDimList.toArray(new String[0]);

	}

	/**
	 * Gets the project app name
	 * 
	 * @param project
	 *            the workspace project
	 * @return Project Application name
	 */
	public static String getApplicationName(IProject project) {
		
		String appName = null;
		PafApplicationDef[] applications = getPafApplicationDef(project);
		if (applications != null && applications.length > 0) {
			appName = applications[0].getAppId();
		}
		return appName;
		
	}

	/**
	 * Gets the project application def
	 * 
	 * @param project
	 *            the workspace project
	 * @return PafApplicationDef[] 
	 */
	public static PafApplicationDef[] getPafApplicationDef(IProject project) {

		IFolder confDir = project.getFolder(Constants.CONF_DIR);

		PafApplicationDef[] applications = null;

		try {
			
			IFile iPafApps = confDir.getFile(PafBaseConstants.FN_ApplicationMetaData);
			
			if ( iPafApps.exists() ) {
				
				applications = (PafApplicationDef[]) ACPafXStream
						.importObjectFromXml(iPafApps);
				
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return applications;
	}
	
//	/**
//	 * Tries to deploy the projects conf dir to server's conf dir.  The server's conf will be
//	 * backed up in the conf/backup dir on server.
//	 * 
//	 * @param project
//	 *            the workspace project
//	 * @param pafServer
//	 * 				project to deploy conf dir to TTN-751
//	 */
//	public static void deployProjectToServer(IProject project, PafServer pafServer) throws PafServerNotFound, IOException {		
//		
//		//conf folder
//		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
//		
//		deployProjectToServer(iConfFolder, pafServer);
//
//	}
	
//	/**
//	 * Tries to deploy the projects conf dir to server's conf dir.  The server's conf will be
//	 * backed up in the conf/backup dir on server.
//	 * 
//	 * @param sourceConfDir
//	 *            the projects IFolder conf folder.
//	 * @param pafServer
//	 * 				project to deploy conf dir to TTN-751
//	 */
//	public static void deployProjectToServer(IFolder sourceConfDir, PafServer pafServer) throws PafServerNotFound, IOException {		
//		
//		//if server exists
//		if ( pafServer != null ) {
//		
//			//conf folder
//			IFolder iConfFolder = sourceConfDir;
//			
//			//conf directory
//			File toConfDir = new File(pafServer.getHomeDirectory() + Constants.CONF_DIR);
//			
//			//backup directory
//			File backupDir = new File(toConfDir.toString() + File.separator + Constants.BACKUP);
//			
//			//try to backup contents of server's conf dir and then export project conf to server
//			try {
//				
//				if ( toConfDir.isDirectory()) {
//					
//					//if backup dir exists, delete all files
//					if ( backupDir.exists() ) {
//					
//						FileUtils.deleteFilesInDir(backupDir, true);						
//						
//					} else {
//						
//						//make new backup dir
//						backupDir.mkdir();
//						
//					}
//					
//					//move all resources to backup dir
//					for (File resource : toConfDir.listFiles()) {
//						
//						if ( ! resource.equals(backupDir ) ) {
//							
//							resource.renameTo(new File(backupDir.toString() + File.separator + resource.getName()));
//							
//						}
//						
//					}
//					
//					
//				}
//				
//				//export project's conf folder to server 
//				FileHelperUtil.exportDirectoryToFileSystem(iConfFolder, toConfDir);
//				
//			} catch (IOException e) {
//
//				logger.error("Problem deploying project to server: " + e.getMessage());
//				
//				throw e;
//				
//			}
//			
//		}		
//		
//	}
	
//	/**
//	 * Imports a project from the server.  First, local project in admin console is backed up, then
//	 * new files are imported.
//	 * 
//	 * @param project
//	 *            the workspace project
//	 * @param pafServer server to import project from TTN-751
//	 * @return PafApplicationDef[] 
//	 */
//	public static void importProjectFromServer(IProject project, PafServer pafServer) throws PafServerNotFound, CoreException {
//		
//		if ( pafServer != null ) {
//			
//			//conf dir on server
//			File fromConfDir = new File(pafServer.getHomeDirectory() + Constants.CONF_DIR);
//			
//			//conf folder in project
//			IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
//			
//			//if conf directory exists on server
//			if ( fromConfDir.exists() ) {
//							
//				//backup local
//				IFolder iBackupFolder = project.getFolder(Constants.BACKUP);
//					
//				//move contents of conf to backup
//				FileHelperUtil.moveContentsOfIFolder(iConfFolder, iBackupFolder, true);
//							
//			}
//				
//			//import servers conf directory
//			FileHelperUtil.importDirectoryFromFileSystem(fromConfDir, iConfFolder, Constants.BACKUP);		
//						
//		}
//		
//	}
	
	
	/**
	 * local project in admin console is backed up, t
	 * 
	 * @param project the workspace project
	 */
	public static void backupProject(IProject project) throws CoreException {
		
		//conf folder in project
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		//if conf project directory 
		if ( iConfFolder.exists() ) {
						
			//backup local
			IFolder iBackupFolder = project.getFolder(Constants.BACKUP);
				
			//move contents of conf to backup
			FileHelperUtil.moveContentsOfIFolder(iConfFolder, iBackupFolder, true);
						
		}
		
		
	}


	/**
	 * local project in admin console is backed up, t
	 * 
	 * @param project the workspace project
	 */
	public static void backupProject(IProject project, ProjectElementId[] elements) throws CoreException {
		
		//conf folder in project
		IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
		
		IFolder iBackupFolder = project.getFolder(Constants.BACKUP);
				
		Set<IResource> filesToCopy = new HashSet<IResource>();
		
		if ( elements == null ) {
			
			List<ProjectElementId> allProjectElementIdList = new ArrayList<ProjectElementId>(Arrays.asList(ProjectElementId.values()));
			
			//removed unwanted elements
			allProjectElementIdList.remove(ProjectElementId.RuleSet_RuleSet);
			allProjectElementIdList.remove(ProjectElementId.RuleSet_RuleGroup);
			allProjectElementIdList.remove(ProjectElementId.RuleSet_Rule);
			
			elements = allProjectElementIdList.toArray(new ProjectElementId[0]);
			
			
		}
		
		//if conf project directory 
		if ( iConfFolder.exists() ) {
						
			for(ProjectElementId element : elements){
				
				switch (element) {
				
					case ApplicationDef:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ApplicationMetaData));
						break;
									  
					case Views: 
						filesToCopy.add(iConfFolder.getFolder(PafBaseConstants.DN_ViewsFldr));
						break;
						
					case ViewSections:
						filesToCopy.add(iConfFolder.getFolder(PafBaseConstants.DN_ViewSectionsFldr));
						break;
						 
					case ViewGroups:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ViewGroups));
						break;
						
					case RuleSets:
						filesToCopy.add(iConfFolder.getFolder(PafBaseConstants.DN_RuleSetsFldr));
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_RuleSetMetaData));
						break;
						
					case UserSecurity:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_SecurityMetaData));
						break;
						
					case PlanCycles:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ApplicationMetaData));
						break;
						 
					case Seasons:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ApplicationMetaData));
						break;
						 
					case Roles:
						//filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ApplicationMetaData));
						//filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_PlannerConfigs));
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_RoleMetaData));
						break;
						 
					case RoleConfigs:
						//filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_ApplicationMetaData));
						//filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_RoleMetaData));
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_PlannerConfigs));
						break;
						
					case Versions:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_VersionMetaData));
						break;
						
					case Measures:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_MeasureMetaData));
						break;
										
					case NumericFormats:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_NumericFormatsMetaData));
						break;
						 
					case HierarchyFormats:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_HierarchyFormats));
						break;
						 
					case GlobalStyles:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_GlobalStyleMetaData));
						break;
						 
					case UserSelections: 
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_UserSelections));
						break;
						
					case DynamicMembers:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_DynamicMembers));
						break;
						 
					case CustomMenus:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_CustomMenus));
						break;
						 
					case CustomFunctions:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_CustomFunctionMetaData));
						break;
						
					case RoundingRules:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_RoundingRules));
						break;
						
					case MemberTags:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_MemberTagMetaData));
						break;
					//TTN 900 - Print Preferences - added by Iris	
					case PrintStyles:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_PrintStyles));
						break;
						
					case MemberLists:
						
						filesToCopy.add(iConfFolder.getFile(PafBaseConstants.FN_UserMemberLists));
						break;
						
					default:
						
						
						break;
				
				}
				
			}
			
						
		}
		
		//TTN-1446:  Files moved to the backup folder in the AC should now be copied instead of moved
		FileHelperUtil.copyContentsOfIFolder(iConfFolder, iBackupFolder, filesToCopy);
		
		
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param serverName
	 */
	public static void deleteServerReferences(String serverName) {

		if ( serverName == null ) {
			return;
		}
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot rootWorkspace = workspace.getRoot();
		
		//if projects exist
		if ( rootWorkspace.getProjects().length != 0 ) {
			
			for (IProject project : rootWorkspace.getProjects()) {
				
				IFile projectFile = project.getFile(Constants.PAF_PROJECT_FILE);
				
				if ( projectFile != null && projectFile.exists()) {
					
					PafProject pafProject = null;
					try {
						pafProject = (PafProject) ACPafXStream.importObjectFromXml(projectFile);
					} catch (Exception e) {
					}

					if ( pafProject != null && pafProject.getServerName() != null ) {
						
						if ( pafProject.getServerName().equalsIgnoreCase(serverName)) {
					
							try {
								projectFile.delete(true, null);
								project.refreshLocal(IResource.DEPTH_INFINITE, null);
							} catch (CoreException e) {
								logger.error("Problem deleting or refreshing paf project file or project dir");
							}
							
						}
						
					}					
				}
			}
			
		}

		
	}	
	
	/** 
	 *  Imports the custom functions file for a particular project.
	 *
	 * @param project Current Project
	 * @return Map of Custom Function Definitions
	 */
	public static Map<String, CustomFunctionDef> importCustomFunctions(IProject project) {
		
		Map<String, CustomFunctionDef> customFunctionMap = new HashMap<String, CustomFunctionDef>();
		
		if ( project != null ) {
			
			//get reference to conf folder
			IFolder confFolder = project.getFolder(Constants.CONF_DIR);
			
			//if conf folder exists, import custom functions
			if ( confFolder.exists()) {
				
				IFile customFunctionsFile = confFolder.getFile(PafBaseConstants.FN_CustomFunctionMetaData);
		
				customFunctionMap = importCustomFunctions(customFunctionsFile);
		
			}
			
		}		
		
		return customFunctionMap;
		
		
	}
	
	/** 
	 *  Imports the custom functions file.
	 *
	 * @param customFunctionsFile The Custom Functions IFile 
	 * @return Map of Custom Function Definitions
	 */
	public static Map<String, CustomFunctionDef> importCustomFunctions(IFile customFunctionsFile) {
		
		Map<String, CustomFunctionDef> customFunctionMap = new HashMap<String, CustomFunctionDef>();
		
		//if IFile exists
		if ( customFunctionsFile != null && customFunctionsFile.exists() ) {
			
			customFunctionMap = importCustomFunctions(new File(customFunctionsFile.getLocation().toOSString()));
			
		}				

		return customFunctionMap;
		
	}
	
	/** 
	 *  Imports the custom functions file.
	 *
	 * @param customFunctionsFile The Custom Functions File 
	 * @return Map of Custom Function Definitions
	 */
	public static Map<String, CustomFunctionDef> importCustomFunctions(File customFunctionsFile) {
		
		Map<String, CustomFunctionDef> customFunctionMap = new HashMap<String, CustomFunctionDef>();
		
		//if file exists
		if ( customFunctionsFile != null && customFunctionsFile.exists()) {
			
			CustomFunctionDef[] customFunctionsAr = null;
			
			//import the custom Function Ar and then populate the map
			try {
				
				//import
				customFunctionsAr = (CustomFunctionDef[]) PafXStream.importObjectFromXml(customFunctionsFile.toString());
				
				if ( customFunctionsAr != null ) {
					
					//populate the map
					for (CustomFunctionDef customFunction : customFunctionsAr) {
						
						if ( customFunction != null) {
							
							customFunctionMap.put(customFunction.getFunctionName(), customFunction);
							
						}
						
					}					
					
				}
				
			} catch (Exception e) {
				
				logger.error("An exception was thrown importing file: " + customFunctionsFile.toString() + ". The error was " + e.getMessage());
				
			}
			
		}
		
		return customFunctionMap;
		
	}
	
	/**
	 * Uploads project to server.
	 * 
	 * @param uploadJobInput input for upload job
	 */
	public static void uploadProjectToServer(UploadProjectInput uploadJobInput) {
		//TTN-2177:
		//Prompt Save Resources Dialog Box On Deploying Projects to Server When any Editors are Unsaved
		if( PlatformUI.getWorkbench().saveAllEditors(true) ) {
			
			if ( uploadJobInput.isHotDeploy() && ! uploadJobInput.isSlientMode()) {
				
				CustomDialogMessageBox dialogBox = new CustomDialogMessageBox(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"Hot upload project: " + uploadJobInput.getProjectName(),
						"Hot upload project: '" + uploadJobInput.getProjectName() + "' to server: '" + uploadJobInput.getPafServer().getName() + "'",
						"Do not prompt me again.");
				
				int rc = dialogBox.open();
				
				if(rc == Dialog.CANCEL){
					return;
				}
				
				uploadJobInput.getPafServer().setDoesNotPromptOnHotDeploy(dialogBox.isCheckBoxMessageChecked());
				
				uploadJobInput.setSlientMode(uploadJobInput.getPafServer().isDoesNotPromptOnHotDeploy());
				
				PafServerUtil.addPafServer(uploadJobInput.getPafServer());
				
			} else if ( ! uploadJobInput.isHotDeploy() && ! uploadJobInput.isSlientMode() )	{	
				
				//look for any open editors and close them.
				UploadLocalProjectDialog dialog = new UploadLocalProjectDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), uploadJobInput.getPafServer(), uploadJobInput.getProjectName(), false, uploadJobInput.getFilterSet() );
				
				int rc = dialog.open();
				
				if ( rc != 0 ) {
					return;
				}
				
				uploadJobInput.setApplyConfigChanges(dialog.isApplyConfig());
				uploadJobInput.setApplyCubeChanges(dialog.isApplyCube());
			
			}
			
			//get i project
			IProject iProject = ResourcesPlugin.getWorkspace().getRoot().getProject(uploadJobInput.getProjectName());
			
			//if server is still running, execute job otherwise alert user with error message
			if ( ServerMonitor.getInstance().isServerRunning(uploadJobInput.getPafServer())) {
			
				MessageConsole msgConsole = ConsoleUtil.findConsole(uploadJobInput.getPafServer().getName());
				if( msgConsole != null ) {
					msgConsole.activate();
				}
	
				UploadProjectJob uploadProjectJob = new UploadProjectJob(iProject, uploadJobInput.getPafServer(), uploadJobInput.isApplyConfigChanges(), uploadJobInput.isApplyCubeChanges(), uploadJobInput.getFilterSet());
				
				if ( uploadJobInput.getJobChangeAdapter() != null ) {
					uploadProjectJob.addJobChangeListener(uploadJobInput.getJobChangeAdapter());
				} else {
					uploadProjectJob.addJobChangeListener(new UploadProjectJobChangeAdapter(uploadJobInput));
				}
								
				uploadProjectJob.schedule();				
			
			} else {
				
				GUIUtil.serverNotRunning(uploadJobInput.getPafServer());
												
			}
		}
	}
			
	/**
	 * Downloads a project from the server
	 * 
	 * @param server server to download from
	 * @param projectName project to download to
	 * @param silentMode if true, only prompted when errors
	 */
	public static void downloadProjectFromServer(DownloadProjectInput downloadProjInput) {
		
		if ( downloadProjInput.isHotDeploy() && ! downloadProjInput.isSlientMode()) {
			
			CustomDialogMessageBox dialogBox = new CustomDialogMessageBox(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
					"Hot download project: " + downloadProjInput.getProjectName(),
					"Hot download project: '" + downloadProjInput.getProjectName() + "' from server: '" + downloadProjInput.getPafServer().getName() + "'",
					"Do not prompt me again.");
			
			int rc = dialogBox.open();
			
			if(rc == Dialog.CANCEL){
				return;
			}
			
			downloadProjInput.getPafServer().setDoesNotPromptOnHotDeploy(dialogBox.isCheckBoxMessageChecked());
									
			PafServerUtil.addPafServer(downloadProjInput.getPafServer());
			
		// confirm with user to import conf directory from server
		} else if (! downloadProjInput.isSlientMode() && !GUIUtil
				.askUserAQuestion("Do you want to download project from server '"
						+ downloadProjInput.getPafServer().getName() + "' to local project '" + downloadProjInput.getProjectName() + "'?" )) {
			return;
		}
								
		//if server is still running, execute job otherwise alert user with error message
		if ( ServerMonitor.getInstance().isServerRunning(downloadProjInput.getPafServer())) {
		
			MessageConsole msgConsole = ConsoleUtil.findConsole(downloadProjInput.getPafServer().getName());
			if( msgConsole != null ) {
				msgConsole.activate();
			}


			//get i project
			IProject iProject = ResourcesPlugin.getWorkspace().getRoot().getProject(downloadProjInput.getProjectName());
			
			DownloadProjectJob downloadProjectJob = new DownloadProjectJob(iProject, downloadProjInput.getPafServer());
			
			
			if ( downloadProjInput.getJobChangeAdapter() == null ) {
				downloadProjectJob.addJobChangeListener(new DownloadProjectJobChangeAdapter(downloadProjInput.isSlientMode()));
			} else {
				downloadProjectJob.addJobChangeListener(downloadProjInput.getJobChangeAdapter());
			}
			downloadProjectJob.schedule();				
		
		} else {
			
			String errorMessage = "Server '" + downloadProjInput.getPafServer().getName() + "' is no longer running.  Please start the server and try again.";
			
			logger.error(errorMessage);
			
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, errorMessage, SWT.ICON_ERROR);
			
								
		}
		
	}


	/**
	 * Uploads project to server.
	 * 
	 * @param uploadJobInput input for upload job
	 */
	public static void uploadProjectToServerGroup(UploadProjectToServerGroupInput input) {

		IProject iProject = ResourcesPlugin.getWorkspace().getRoot().getProject(input.getProjectName());
		
		UploadProjectToServerGroupJob job = new UploadProjectToServerGroupJob(
				iProject, 
				input.getPafServers(), 
				input.getGroupName(), 
				input.isApplyConfigChanges(), 
				input.isApplyCubeChanges(), 
				input.getFilterSet());
		
		if ( input.getJobChangeAdapter() != null ) {
			job.addJobChangeListener(input.getJobChangeAdapter());
		} else {
			job.addJobChangeListener(new UploadProjectToServerGroupJobChangeAdapter(input));
		}
						
		job.schedule();				
		
	}

}

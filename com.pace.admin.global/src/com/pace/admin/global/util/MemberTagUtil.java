/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.pace.admin.global.view.PafViewSectionUI;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.view.PafAxis;
import com.pace.base.view.ViewTuple;

/**
 * Member Tag Utilities
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class MemberTagUtil {

	private static Set<String> getFilteredDimensionsForAxis(PafViewSectionUI viewSection, PafAxis pafAxis) {

		Set<String> filteredDimSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		filteredDimSet.addAll(Arrays.asList(viewSection.getPageAxisDims()));
				
		//for column axis, use row dimensions
		if ( pafAxis.equals(PafAxis.COL) ) {
			
			if ( viewSection.getRowAxisDims() != null ) {
				
				filteredDimSet.addAll(Arrays.asList(viewSection.getRowAxisDims()));
				
			}				

		//for row axis, use column dimensions
		} else if (pafAxis.equals(PafAxis.ROW) ) {			
			
			if ( viewSection.getColAxisDims() != null ) {
				
				filteredDimSet.addAll(Arrays.asList(viewSection.getColAxisDims()));
				
			}				
		
		}
		
		return filteredDimSet;
	}
	
	/**
	 * 
	 * Validates member tag against the axis dimensions.  If a Row paf axis is passed in,
	 * the member tag is validated against the page and column axis dimensions.  If a 
	 * column paf axis is passed in, the member tag is validated agains the page and row axis
	 * dimensions.
	 *
	 * @param memberTagDef  member tag to validate
	 * @param pafAxis		axis dimenions
	 * @return	true if validated/false if not
	 */
	public static boolean isMemberTagValidForAxis(PafViewSectionUI viewSection, MemberTagDef memberTagDef, PafAxis pafAxis) {
		
		//simple param check
		if ( viewSection != null && memberTagDef != null && memberTagDef.getDims() != null && pafAxis != null ) {
			
			//create a set using the member tag dimension names
			Set<String> memberTagDimensionNameSet = new HashSet<String>(Arrays.asList(memberTagDef.getDims()));
			
			//if paf axis is row or column, verify the member def dims are not all on the page axis
			if ( pafAxis.equals(PafAxis.ROW) || pafAxis.equals(PafAxis.COL) ) {
				
				//create a set of page dimension names
				Set<String> pageDimensionNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
				//add page dimension ar to the set
				pageDimensionNameSet.addAll(Arrays.asList(viewSection.getPageAxisDims()));
	
				//by default, have true
				boolean allMemberTagDimensionOnPage = true;
				
				//for each member tag dimension in the member tag dimension name set
				for ( String memberTagDimensionName : memberTagDimensionNameSet ) {
				
					//if the page dimension name set doesn't contain the member tag dim name, invalidate the flag
					if ( ! pageDimensionNameSet.contains(memberTagDimensionName) ) {
						
						allMemberTagDimensionOnPage = false;
					}
					
				}			
				
				//if all member tag dimensions are on page, return false
				if ( allMemberTagDimensionOnPage ) {
					
					return false;
				}
			}
			
			//unique set of dim names
			Set<String> filteredDimSet = getFilteredDimensionsForAxis(viewSection, pafAxis); 
			
			for (String memberTagDim : memberTagDimensionNameSet ) {
					
				if ( ! filteredDimSet.contains(memberTagDim) ) {
						
					return false;
						
				}					
									
			}
			
			//got this far, lets return a validated status
			return true;
			
		}		
		
		return false;
	}

	
	public static boolean isMemberTagValidForViewSection(PafViewSectionUI viewSection, MemberTagDef memberTagDef) {
		
		boolean isValid = false;
		
		if ( isMemberTagValidForAxis(viewSection, memberTagDef, new PafAxis(PafAxis.ROW)) || 
				isMemberTagValidForAxis(viewSection, memberTagDef, new PafAxis(PafAxis.COL)) ) {
			isValid = true;
		}
		
		
		
		return isValid;	
		
	}
			
	
	/**
	 * 
	 *  Removes the member tag from all view tuples. It bascially 
	 *  clears all member tags for the view tuples passed in.
	 *
	 * @param tuples tuples to have member tag removed
	 * @param memberTagFilterName name of member tag to remove, if null, all will be removed
	 * @return view tuples with member tags removed
	 */
	public static ViewTuple[] removeMemberTagTuples(ViewTuple[] tuples, String memberTagFilterName) {
		
		ViewTuple[] viewTuples = null;
		
		if ( tuples != null ) {
			
			List<ViewTuple> tuplesList = new ArrayList<ViewTuple>();
			
			//loop tuples and add to list if not a member tag tuple
			for (ViewTuple tuple : tuples ) {
				
				//if null, remove all member tags
				if ( memberTagFilterName == null ) {
				
					if ( ! tuple.isMemberTag() ) {			
						
						tuplesList.add(tuple);
						
					} 
					
				} else {
					
					if ( tuple.isMemberTag() && tuple.getMemberDefs()[0].equalsIgnoreCase(memberTagFilterName ) 	) {
						
						continue;
						
					} 
					
					tuplesList.add(tuple);
					
				}
								
			}
			
			//if list has tuples, convert to an array
			if ( tuplesList.size() > 0 ) {
				
				viewTuples = tuplesList.toArray(new ViewTuple[0]);
				
			} 			
			
		}
		
		return viewTuples;
	}
}

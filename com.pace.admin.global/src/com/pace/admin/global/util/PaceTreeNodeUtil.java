/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.pace.admin.global.enums.PaceTreeNodeSelectionType;
import com.pace.admin.global.enums.PaceTreeNodeType;
import com.pace.admin.global.model.PaceTreeNode;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PaceTreeNodeUtil {
	
	public static final String JUST_LEVEL_OR_GENERATION_DISPLAY = "Just Level or Generation";
	public static final String JUST_DESCENDANTS_OF_SELECTION_DISPLAY = "Just Descendants of Selection";
	public static final String SELECTION_AND_DESCENDANTS_DISPLAY = "Selection and Descendants";
	public static final String JUST_CHILDREN_OF_SELECTION_DISPLAY = "Just Children of Selection";
	public static final String SELECTION_AND_CHILDREN_DISPLAY = "Selection and Children";
	public static final String MEMBER_SELECTION_DISPLAY = "Member Selection";
	public static final String SELECTION_DISPLAY = "Selection";
	//TTN 1595 - N-Year Support
	public static final String OFFSET_MEMBER_DISPLAY = "Offset Member";
	public static final String OFFSET_MEMBER_RANGE_DISPLAY = "Offset Member Range";
	private static final String BOTTOM = "Bottom";
	private static final String LEVEL = "Level";
	private static final String GENERATION = "Generation";
	
/*	private static final java.util.List<String> selectionTypeArrayAllOptions = new ArrayList<String>(
			Arrays.asList(new String[] { SELECTION, MEMBER_SELECTION, SELECTION_AND_CHILDREN, 
					JUST_CHILDREN_OF_SELECTION, SELECTION_AND_DESCENDANTS, JUST_DESCENDANTS_OF_SELECTION,
					JUST_LEVEL_OR_GENERATION })
	);
	*/
	
	private static Map<PaceTreeNodeSelectionType, String> selectionTypeDisplayMap = new TreeMap<PaceTreeNodeSelectionType, String>();
			
	static {
		
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.Selection, SELECTION_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.Members, MEMBER_SELECTION_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.IChildren, SELECTION_AND_CHILDREN_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.Children, JUST_CHILDREN_OF_SELECTION_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.IDescendants, SELECTION_AND_DESCENDANTS_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.Descendants, JUST_DESCENDANTS_OF_SELECTION_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.LevelGen, JUST_LEVEL_OR_GENERATION_DISPLAY);
		//TTN 1595 - N-Year Support
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.OffsetMember, OFFSET_MEMBER_DISPLAY);
		selectionTypeDisplayMap.put(PaceTreeNodeSelectionType.OffsetMemberRange, OFFSET_MEMBER_RANGE_DISPLAY);
		
	}

	public static List<PaceTreeNodeSelectionType> getAllSelectionTypes() {
		
		List<PaceTreeNodeSelectionType> selectionTypeList = new ArrayList<PaceTreeNodeSelectionType>();
		
		selectionTypeList.add(PaceTreeNodeSelectionType.Selection);
		selectionTypeList.add(PaceTreeNodeSelectionType.Members);
		selectionTypeList.add(PaceTreeNodeSelectionType.IChildren);
		selectionTypeList.add(PaceTreeNodeSelectionType.Children);
		selectionTypeList.add(PaceTreeNodeSelectionType.IDescendants);
		selectionTypeList.add(PaceTreeNodeSelectionType.Descendants);
		selectionTypeList.add(PaceTreeNodeSelectionType.LevelGen);
		
		return selectionTypeList;
		
	}
	
	public static List<String> getAllUserSelectionTypes() {
		
		List<String> selectionTypeList = new ArrayList<String>();
		
		selectionTypeList.add(SELECTION_DISPLAY);
		selectionTypeList.add(SELECTION_AND_CHILDREN_DISPLAY);
		selectionTypeList.add(JUST_CHILDREN_OF_SELECTION_DISPLAY);
		selectionTypeList.add(SELECTION_AND_DESCENDANTS_DISPLAY);
		selectionTypeList.add(JUST_DESCENDANTS_OF_SELECTION_DISPLAY);
		selectionTypeList.add(JUST_LEVEL_OR_GENERATION_DISPLAY);
		
		return selectionTypeList;
		
	}
	
	public static List<String> getAllLevelGenerationTypes() {
		
		List<String> levelGenTypeList = new ArrayList<String>();
		
		levelGenTypeList.add(BOTTOM);
		levelGenTypeList.add(LEVEL);
		levelGenTypeList.add(GENERATION);
		
		return levelGenTypeList;
		
	}
	
	public static List<PaceTreeNodeSelectionType> getPaceTreeNodeSelectionTypes(PaceTreeNodeType type) {
		
		List<PaceTreeNodeSelectionType> selectionTypeList = new ArrayList<PaceTreeNodeSelectionType>();
		
		if ( type != null ) {
		
			switch (type) {
			
			case Blank:
			case MemberTag:
			case Root:
			case TextOnly:				
				break;
			case UOWRoot:
				selectionTypeList.addAll(getAllSelectionTypes());
				break;
			case DynamicMember:
			case UserSelSingle:
			case PaceMember:				
				selectionTypeList.addAll(getAllSelectionTypes());
				selectionTypeList.add(PaceTreeNodeSelectionType.OffsetMember);
				selectionTypeList.add(PaceTreeNodeSelectionType.OffsetMemberRange);
				break;
			case PaceMemberLevel0:
			case MultiYearSingleMember:
				selectionTypeList.add(PaceTreeNodeSelectionType.Selection);
				selectionTypeList.add(PaceTreeNodeSelectionType.Members);
				selectionTypeList.add(PaceTreeNodeSelectionType.OffsetMember);
				selectionTypeList.add(PaceTreeNodeSelectionType.OffsetMemberRange);
				break;		
			case MultiYearMultiMember:
				selectionTypeList.add(PaceTreeNodeSelectionType.Selection);
				selectionTypeList.add(PaceTreeNodeSelectionType.Members);
				break;
			case UserSelMulti:
				selectionTypeList.add(PaceTreeNodeSelectionType.Selection);
				selectionTypeList.add(PaceTreeNodeSelectionType.Members);
				selectionTypeList.add(PaceTreeNodeSelectionType.IDescendants);
				selectionTypeList.add(PaceTreeNodeSelectionType.Descendants);
				selectionTypeList.add(PaceTreeNodeSelectionType.IChildren);
				selectionTypeList.add(PaceTreeNodeSelectionType.Children);
				break;
			}					
			
		}		
		
		return selectionTypeList;
		
	}
	
	public static PaceTreeNodeSelectionType getSelectionTypeFromDisplayName(String displayName) {
		
		PaceTreeNodeSelectionType selectionType = null;
		
		if ( displayName != null && selectionTypeDisplayMap != null && selectionTypeDisplayMap.containsValue(displayName)) {
			
			for ( PaceTreeNodeSelectionType paceTreeNodeSelectionType : selectionTypeDisplayMap.keySet()) {
				
				String selectionTypeDisplayName = selectionTypeDisplayMap.get(paceTreeNodeSelectionType);
				
				if ( selectionTypeDisplayName.equals(displayName)) {
					
					selectionType = paceTreeNodeSelectionType;
					break;
				}
				
			}
		}		
		
		return selectionType;
		
	}

	public static java.util.List<String> getDisplayNamesFromSelectionType(PaceTreeNode selectNode) {

		java.util.List<PaceTreeNodeSelectionType> paceTreeNodeSelectionTypeList = PaceTreeNodeUtil.getPaceTreeNodeSelectionTypes(selectNode.getProperties().getType());			

		java.util.List<String> paceTreeNodeSelectionTypeDisplayNames = new ArrayList<String>();
		
		for (PaceTreeNodeSelectionType paceTreeNodeSelectionType : paceTreeNodeSelectionTypeList ) {
			
			String selectionTypeDisplayName = null;
			
			if ( selectNode != null ) {
				if( paceTreeNodeSelectionType != null ) {
					//if in display map
					if ( selectionTypeDisplayMap != null && selectionTypeDisplayMap.containsKey(paceTreeNodeSelectionType)) {
							
						selectionTypeDisplayName = selectionTypeDisplayMap.get(paceTreeNodeSelectionType);
						
					//if not display map, handle unique situtations.
					} 
					else {
						
						switch (paceTreeNodeSelectionType) {
						
						case Level:
						case Generation:
							selectionTypeDisplayName = JUST_LEVEL_OR_GENERATION_DISPLAY;
							break;
						case IDescGen:
						case IDescLevel:
							selectionTypeDisplayName = SELECTION_AND_DESCENDANTS_DISPLAY;
							break;
						case DescGen:
						case DescLevel:
							selectionTypeDisplayName = JUST_DESCENDANTS_OF_SELECTION_DISPLAY;
							break;
						case OffsetMember:
							//Offset Member
							selectionTypeDisplayName = OFFSET_MEMBER_DISPLAY;
							break;
						case OffsetMemberRange:
							selectionTypeDisplayName = OFFSET_MEMBER_RANGE_DISPLAY;
							break;
						}
					}
					paceTreeNodeSelectionTypeDisplayNames.add(selectionTypeDisplayName);
				}
			}
		}		
		
		return paceTreeNodeSelectionTypeDisplayNames;
	}
	
	public static String getDisplayNameFromSelectionType(PaceTreeNode selectNode) {

		PaceTreeNodeSelectionType paceTreeNodeSelectionType = selectNode.getProperties().getSelectionType();
		String selectionTypeDisplayName = null;
		
		if ( selectNode != null ) {
			if( paceTreeNodeSelectionType != null ) {
				//if in display map
				if ( selectionTypeDisplayMap != null && selectionTypeDisplayMap.containsKey(paceTreeNodeSelectionType)) {
						
					selectionTypeDisplayName = selectionTypeDisplayMap.get(paceTreeNodeSelectionType);
					
				//if not display map, handle unique situtations.
				} 
				else {
					
					switch (paceTreeNodeSelectionType) {
					
					case Level:
					case Generation:
						selectionTypeDisplayName = JUST_LEVEL_OR_GENERATION_DISPLAY;
						break;
					case IDescGen:
					case IDescLevel:
						selectionTypeDisplayName = SELECTION_AND_DESCENDANTS_DISPLAY;
						break;
					case DescGen:
					case DescLevel:
						selectionTypeDisplayName = JUST_DESCENDANTS_OF_SELECTION_DISPLAY;
						break;
					case OffsetMember:
						//Offset Member
						selectionTypeDisplayName = OFFSET_MEMBER_DISPLAY;
						break;
					case OffsetMemberRange:
						selectionTypeDisplayName = OFFSET_MEMBER_RANGE_DISPLAY;
						break;
					}
				}
			}
		}
		
		return selectionTypeDisplayName;
	}
	
	public static String getDisplayNameFromSelectionType(PaceTreeNodeSelectionType paceTreeNodeSelectionType) {

		String selectionTypeDisplayName = null;
		
		if ( paceTreeNodeSelectionType != null ) {
			
			//if in display map
			if ( selectionTypeDisplayMap != null && selectionTypeDisplayMap.containsKey(paceTreeNodeSelectionType)) {
					
				selectionTypeDisplayName = selectionTypeDisplayMap.get(paceTreeNodeSelectionType);
				
			//if not display map, handle unique situtations.
			} else {
				
				switch (paceTreeNodeSelectionType) {
				
				case ILevel:
				case Level:
				case Generation:
					selectionTypeDisplayName = JUST_LEVEL_OR_GENERATION_DISPLAY;
					break;
				case IDescGen:
				case IDescLevel:
					selectionTypeDisplayName = SELECTION_AND_DESCENDANTS_DISPLAY;
					break;
				case DescGen:
				case DescLevel:
					selectionTypeDisplayName = JUST_DESCENDANTS_OF_SELECTION_DISPLAY;
				}
							
				
			}
			
		}		
		
		return selectionTypeDisplayName;
	}
	
}

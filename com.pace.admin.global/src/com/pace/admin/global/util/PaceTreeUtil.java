/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.admin.global.model.PaceTreeNode;
import com.pace.base.utility.PafXStream;


public class PaceTreeUtil {

	public static final File cachedTreeDir = new File("C:/Program Files (x86)/Pace/Admin Console/cachedDimTrees/default/Server/"); 
	
	public static final File projectDir = new File("C:/Program Files (x86)/Pace/PafServer/conf/");
	
	//public static final File cachedTreeDir = new File("C:/Program Files/Pace/Admin Console/cachedDimTrees/default/TitanCopy/");
	//public static final File projectDir = new File("C:/Program Files/Pace/PafServer/conf/");
	
	List<PafSimpleDimTree> pageSimpleTrees = new ArrayList<PafSimpleDimTree>();
	List<PafSimpleDimTree> colSimpleTrees = new ArrayList<PafSimpleDimTree>();
	List<PafSimpleDimTree> rowSimpleTrees = new ArrayList<PafSimpleDimTree>();
	
	public PaceTreeUtil() {
		
		pageSimpleTrees.add(readSimpleTree("Product.xml"));
		pageSimpleTrees.add(readSimpleTree("Version.xml"));
		pageSimpleTrees.add(readSimpleTree("Measures.xml"));
		
		
	}
	
	private PafSimpleDimTree readSimpleTree(String fileName) {
		
		PafSimpleDimTree cachedSimpleTree = null;
		
		File inputFile = new File(cachedTreeDir.getAbsolutePath() + File.separator + fileName);
		
		// try to get cached simple tree
		try {
			cachedSimpleTree = (PafSimpleDimTree) PafXStream
					.importObjectFromXml(inputFile.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cachedSimpleTree;		
		
	}
	
	public List<PafSimpleDimTree> getPageSimpleTrees() {
		
		List<PafSimpleDimTree> listToReturn = null;
		
		if ( pageSimpleTrees != null ) {
			
			listToReturn = new ArrayList<PafSimpleDimTree>();
			listToReturn.addAll(pageSimpleTrees);
			
		}
		
		return listToReturn;
	}	
	
	public static PaceTreeNode getRootNode(PaceTreeNode nodeToFindRootFrom) {
		
		PaceTreeNode parentNode = null;
		
		if ( nodeToFindRootFrom.getParentNode() != null ) {
		
			parentNode = getRootNode(nodeToFindRootFrom.getParentNode());
			
		} else {
			
			parentNode = nodeToFindRootFrom;
			
		}
		
		return parentNode;
		
	}	
		
	public static int getPositionNdxForParentNodeAtDimNdx(PaceTreeNode nodeToSearchFrom, int dimNdx) {
		
		int foundNdx = -1;
		
		if ( nodeToSearchFrom != null && nodeToSearchFrom.getProperties().getAxisDimIndex() != dimNdx ) {
		
			foundNdx = getPositionNdxForParentNodeAtDimNdx(nodeToSearchFrom.getParentNode(), dimNdx);
			
		} else {
			
			if ( nodeToSearchFrom != null ) {
			
				PaceTreeNode parentNode = nodeToSearchFrom.getParentNode();
				
				if ( parentNode != null ) {
				
					foundNdx = parentNode.getChildIndex(nodeToSearchFrom);
									
				}
				
			}
						
		}		
		
		return foundNdx;
		
	}
	
	public static PaceTreeNode getParentNodeAtDimNdx(PaceTreeNode nodeToSearchFrom, int dimNdx) {
		
		PaceTreeNode parentNode = null;
				
		if ( nodeToSearchFrom.getProperties().getAxisDimIndex() == dimNdx ) {

			parentNode = nodeToSearchFrom.getParentNode();
			
		} else if ( nodeToSearchFrom.getProperties().getAxisDimIndex() > dimNdx ) {
		
			if ( nodeToSearchFrom.getParentNode() != null ) {
			
				parentNode = getParentNodeAtDimNdx(nodeToSearchFrom.getParentNode(), dimNdx);
				
			} else {
				
				parentNode = null;
			}
			
		} else {
			
			parentNode = null;	
						
		}		
		
		return parentNode;
		
	}
	
	/*public static int getDepthOfTreeNode(PaceTreeNode paceTreeNode) {
		
		int depth = 0;
		
		PaceTreeNode lowestLevelPaceTreeNode = getFirstBottomLevelPaceTreeNode(paceTreeNode);
		
		lowestLevelPaceTreeNode.getProperties().get
		
		
		
		return depth;
		
	}*/
	
	/**
	 *  
	 *  Based off the node passed in, will return the 1st node without children going down
	 *  the hierarchy.
	 * 
	 * 
	 * @param paceTreeNode node to search from
	 * @return 1st node without children
	 */
	public static PaceTreeNode getFirstBottomLevelPaceTreeNode(PaceTreeNode paceTreeNode) {
		
		PaceTreeNode firstBottomLevelPaceTreeNode = null;
		
		if ( paceTreeNode != null ) {
			
			if ( paceTreeNode.hasChildren() ) {
				
				for (PaceTreeNode child : paceTreeNode.getChildren() ) {
				
					firstBottomLevelPaceTreeNode = getFirstBottomLevelPaceTreeNode(child);
					
					if ( firstBottomLevelPaceTreeNode != null ) {
						break;
					}
					
				}
								
			} else {
				
				firstBottomLevelPaceTreeNode = paceTreeNode;			
			}
		}
		
		return firstBottomLevelPaceTreeNode;
	}

	public static List<PaceTreeNode> getAllBottomLevelPaceTreeNodes(PaceTreeNode paceTreeNode) {
		
		List<PaceTreeNode> bottomLevelNodes = new ArrayList<PaceTreeNode>();
		
		if ( paceTreeNode.hasChildren() ) {
			
			for (PaceTreeNode child : paceTreeNode.getChildren() ) {
			
				List<PaceTreeNode> bottomLevelChildrenNodeList = getAllBottomLevelPaceTreeNodes(child);
				
				if ( bottomLevelChildrenNodeList != null ) {
					bottomLevelNodes.addAll(bottomLevelChildrenNodeList);
				}
							
			}
							
		} else {
			
			bottomLevelNodes.add(paceTreeNode);			
		}
		
		
		return bottomLevelNodes;
		
	}
	
	public static PafSimpleDimMember getPafSimpleDimMember(PafSimpleDimTree pafSimpleDimTree, String memberName) {
		
		PafSimpleDimMember member = null;		
					
		if ( pafSimpleDimTree != null && memberName != null) {
			
			for (PafSimpleDimMember simpleDimMember : pafSimpleDimTree.getMemberObjects()) {
		
				if ( simpleDimMember.getKey().equalsIgnoreCase(memberName)) {
								
					member = simpleDimMember;					
					break;
					
				}						
				
			}					
			
		}		
		
		return member;
		
		
	}

	public static List<PaceTreeNode> getParentNodesAsList(PaceTreeNode paceTreeNode) {

		int axisDimToSearchFrom = paceTreeNode.getProperties().getAxisDimIndex();
		
		List<PaceTreeNode> parentNodeList = new ArrayList<PaceTreeNode>();
		
		if ( axisDimToSearchFrom == 2 ) {
			
			parentNodeList.add(paceTreeNode.getParentNode());
			
		} else {
		
			for (int i = 2; i <= axisDimToSearchFrom; i++) {
			
				PaceTreeNode parentNode = getParentNodeAtDimNdx(paceTreeNode, i);
				
				if ( parentNode != null ) {
					parentNodeList.add(parentNode);
				}
				
			}
			
		}
		
		return parentNodeList;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;

import com.pace.base.app.MdbDef;
import com.pace.base.app.PafApplicationDef;

/**
 *  Used to find the measure and version dimension name from the paf apps
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class DimensionUtil {
	
	//logger
	private static Logger logger = Logger.getLogger(DimensionUtil.class);
	
	/**
	 * Trys to get the measure dimension name
	 *
	 * @param iProject Current project selected
	 * @return String measure dimension name
	 */
	public static String getMeasureDimensionName(IProject project) {

		logger.debug("Trying to find measure dimension name from project");
		
		String measureDimensionName = null;

		PafApplicationDef[] appDef = PafApplicationUtil.getPafApps(project);

		if (appDef != null) {
			
			// get mdbdef to get measure dim name
			MdbDef mdbDef = appDef[0].getMdbDef();

			// measure dim via paf apps
			measureDimensionName = mdbDef.getMeasureDim();
		
			//log message
			logger.debug("Measure dimension name found: " + measureDimensionName);
			
		}
		
		return measureDimensionName;

	}

	/**
	 * Trys to get the version dimension name
	 *
	 * @param iProject Current project selected
	 * @return String version dimension name
	 */
	public static String getVersionDimensionName(IProject project) {

		logger.debug("Trying to find version dimension name from project");
		
		String versionDimensionName = null;

		PafApplicationDef[] appDef = PafApplicationUtil.getPafApps(project);

		if (appDef != null) {
			// get mdbdef to get version dim name
			MdbDef mdbDef = appDef[0].getMdbDef();

			// version dim via paf apps
			versionDimensionName = mdbDef.getVersionDim();
			
			//log message
			logger.debug("Version dimension name found: " + versionDimensionName);
		}
		
		return versionDimensionName;

	}

	
	/**
	 * Trys to get the paf application definition
	 *
	 * @param iProject Current project selected
	 * @return PafApplicationDef[]
	 */
	/*
	private static PafApplicationDef[] getPafApps(IProject project) {

		PafApplicationDef[] appDef = null;

		//if project is not null
		if (project != null) {

			//get conf folder
			IFolder iFolder = project.getFolder(Constants.CONF_DIR);

			// get pafapps.xml ifile
			IFile pafAppFile = iFolder.getFile(Constants.PAF_APPS_FILE);

			//try to get app definition
			try {
				appDef = (PafApplicationDef[]) ACPafXStream
						.importObjectFromXml(pafAppFile);
				
				if ( appDef != null ) {
					logger.info("Application definition was successfully imported");
				} else {
					logger.warn("There was a problem importing application definition");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}

		}

		return appDef;
	}
	*/
	
	/**
	 * Gets the list of dimension names
	 *
	 * @param iProject Current project selected
	 * @return String[] gets the array of dimension names
	 */
	public static String[] getDimensionNames(IProject project) {
	
		//get the apps def
		PafApplicationDef[] pafAppDefs = PafApplicationUtil.getPafApps(project);
			
		//return the list of dimensions
		return pafAppDefs[0].getMdbDef().getAllDims();
	}
	
	/**
	 * Trys to get the measure dimension name
	 *
	 * @param iProject Current project selected
	 * @return String measure dimension name
	 */
	public static String[] getHierDimensionNames(IProject project) {

		logger.debug("Trying to find hier dims name from project");
		
		String[] hierDims = null;

		PafApplicationDef[] appDef = PafApplicationUtil.getPafApps(project);

		if (appDef != null) {
			
			// get mdbdef to get measure dim name
			MdbDef mdbDef = appDef[0].getMdbDef();

			// get hier dims
			hierDims = mdbDef.getHierDims();
		
			//log message
			logger.debug("Hier Dims: " + hierDims);
			
		}
		
		return hierDims;

	}

	/**
	 * Trys to get the Time dimension name
	 *
	 * @param iProject Current project selected
	 * @return String version dimension name
	 */
	public static String getTimeDimensionName(IProject project) {

		logger.debug("Trying to find Time dimension name from project");
		
		String versionDimensionName = null;

		PafApplicationDef[] appDef = PafApplicationUtil.getPafApps(project);

		if (appDef != null) {
			// get mdbdef to get version dim name
			MdbDef mdbDef = appDef[0].getMdbDef();

			// version dim via paf apps
			versionDimensionName = mdbDef.getTimeDim();
			
			//log message
			logger.debug("Time dimension name found: " + versionDimensionName);
		}
		
		return versionDimensionName;

	}
	
	/**
	 * Trys to get the Year dimension name
	 *
	 * @param iProject Current project selected
	 * @return String version dimension name
	 */
	public static String getYearDimensionName(IProject project) {

		logger.debug("Trying to find Year dimension name from project");
		
		String versionDimensionName = null;

		PafApplicationDef[] appDef = PafApplicationUtil.getPafApps(project);

		if (appDef != null) {
			// get mdbdef to get version dim name
			MdbDef mdbDef = appDef[0].getMdbDef();

			// version dim via paf apps
			versionDimensionName = mdbDef.getYearDim();
			
			//log message
			logger.debug("Year dimension name found: " + versionDimensionName);
		}
		
		return versionDimensionName;

	}

	
	
}

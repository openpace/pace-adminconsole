/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import org.apache.log4j.Logger;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.tail.Tail;

/**
 * Utility for Eclipse Console's.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ConsoleUtil {

	//logger
	private static Logger logger = Logger.getLogger(ConsoleUtil.class);

	/**
	 * find console will find a console inside the console manager
	 * 
	 *  @param name	Name of console.
	 */
	public static MessageConsole findConsole(String name) {
				
		//get console plugin
		ConsolePlugin consolePlugin = ConsolePlugin.getDefault();

		//get console manager
		IConsoleManager consoleManager = consolePlugin.getConsoleManager();
		
		//get an array of existing consoles
		IConsole[] existingConsoles = consoleManager.getConsoles();
		
		//loop through existing consoles
		for (int i = 0; i < existingConsoles.length; i++ ) {
			
			//if console name match is found
			if ( name.equals(existingConsoles[i].getName())) {
				
				//log message
				logger.debug("Console '" + existingConsoles[i].getName() + "' found.  Returning to caller");
				
				//return found console
				return (MessageConsole) existingConsoles[i];
			}
			
		}		
	
		//create new console
		MessageConsole newConsole = new MessageConsole(name, null);

		//add console to console manager
		consoleManager.addConsoles(new IConsole[] { newConsole });
		
		//log message
		logger.info("New Console '" + name + "' is being created and registered with the ConsolePlugin.");
		
		//return new conosle
		return newConsole;
		
	}
	
	/** 
	 *	Creates a new Tail to watch file output
	 *
	 * @param fileName
	 */
	public static void outputFileToConsole(String fileName) {
		
		logger.debug("Getting Server Console");
		
		MessageConsole serverConsole = ConsoleUtil.findConsole(Constants.CONSOLE_NAME);
		
		if ( serverConsole != null) {
			
			logger.info("Attempting to tail file: " + fileName);
			
			new Tail(fileName, serverConsole.newMessageStream());
			
		
		}
		
	}
	
}

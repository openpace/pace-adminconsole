/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.BlockSet.Member;

import com.pace.server.client.PafSimpleBaseMember;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;

/**
 * @author jmilliron
 *
 */
public class PafSimpleDimTreeUtil {

	/**
	 * Converts a PafSimpleTree into a HashMap
	 * @param tree The PafSimpleTree to convert.
	 * @return A HashMap, containing the items from the PafSimpleTree.
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, PafSimpleDimMember> convertTreeIntoHashMap(PafSimpleDimTree tree) {

		HashMap<String, PafSimpleDimMember> hash = new HashMap<String, PafSimpleDimMember>();

		List<PafSimpleDimMember> members = tree.getMemberObjects();

		if (members != null) {

			for (int i = 0; i < members.size(); i++) {
				
				hash.put(members.get(i).getKey(), members.get(i));
				
			}
			
		}
		return hash;
	}
	
	
	//This method will do a traversal of the list to find the descendants of the parent in the list based on the generation number.
	public static List<PafSimpleDimMember> getDescendantsForParent(List<PafSimpleDimMember> members , String parent)
	{
		List<PafSimpleDimMember> descendants = new ArrayList<PafSimpleDimMember>();
		int generationNoOfParent = 0;
		int generationOfDescendants =0;
		int parentIndex = 0;
		boolean isParentFound = false;
		
		HashMap<String,PafSimpleDimMember> descendantMap = new HashMap<String,PafSimpleDimMember>();
		// Get the gen no of parent measure and the gen no of the descendants
		for(int i=0;i<members.size();i++)
		{
			
			if(members.get(i).getKey().equals(parent))
			{
				
				generationNoOfParent = members.get(i).getPafSimpleDimMemberProps().getGenerationNumber();
				generationOfDescendants = generationNoOfParent + 1;
				parentIndex = i;
				isParentFound = true;
				// add the parent first to the list
				if( descendantMap.put(members.get(i).getKey(), members.get(i)) == null)
				descendants.add(members.get(i));
				
			}
		}
		
		// if parent belongs in the list, then add the descendats to the list to be returned.
		if(isParentFound)
		{
		
			
			for(int i=(parentIndex+1);i<members.size();i++)
			{
			
			
				if(members.get(i).getPafSimpleDimMemberProps().getGenerationNumber() >= generationOfDescendants)
				{
					if( descendantMap.put(members.get(i).getKey(), members.get(i)) == null)
					descendants.add(members.get(i));
					
				}
				else 
					break;
			}
			
		}
		
		return descendants;
		
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class CollectionsUtil {

	/**
	 * 
	 * @param aliasKeys
	 * @param aliasValues
	 * @return
	 */
	public static Map<String, String> createMapFromTwoLists(List<String> keys,
			List<String> values) {

		Map<String, String> map = new TreeMap<String, String>();
			
		if ( keys.size() == values.size() && keys.size() > 0 ) {
			
			int ndxCnt = 0;
			
			for (String key : keys ) {
			
				String value = values.get(ndxCnt++);
				
				map.put(key, value);
				
			}
			
		}
		
		return map;
	}
	
	
	public static Map<Integer, String> convertToMap(String delimitedString, String delimiter1, String delimiter2)
    {
        List<String> longStrings = convertToList(delimitedString, delimiter1);
        Map<Integer, String> map = new HashMap<Integer, String>();

        for (String str : longStrings)
        {
            List<String> shortStrings = convertToList(str, delimiter2);
            map.put(Integer.parseInt(shortStrings.get(1)), shortStrings.get(0));
        }

        return map;
    }
	
	public static List<List<String>> convertToListOfLists(String delimitedString, String delimiter1, String delimiter2)
    {						
        List<String> longStrings = convertToList(delimitedString, delimiter1);
        List<List<String>> stringLists = new ArrayList<List<String>>();

        for (String str : longStrings)
        {
            List<String> shortStrings = convertToList(str, delimiter2);
            stringLists.add(shortStrings);
        }

        return stringLists;
    }
	
    public static List<String> convertToList(String delimitedString, String delimiter)
    {
    	
    	delimiter = delimiter.replaceAll("\\|", "\\\\|");
    	
    	delimiter = delimiter.replaceAll("\\^", "\\\\^");
    	
    	String[] stringArray = delimitedString.split(delimiter);
        //String[] stringArray = delimitedString.Split(new String[] {delimiter}, StringSplitOptions.RemoveEmptyEntries);
        
    	/*
    	if (delimiter.contains("|")) {
    		delimiter = delimiter.replace("|", "\\|");
		}
    	
        StringTokenizer strTokennizer = new StringTokenizer(delimitedString, delimiter);
        
        String[] stringArray = new String[strTokennizer.countTokens()];
        
        int i = 0;
        while (strTokennizer.hasMoreElements()) {
        
        	stringArray[i++] = strTokennizer.nextToken(); 
        }        
		*/
    	
        List<String> strings = new ArrayList<String>();

        for (String stringItem : stringArray)
        {
            strings.add(stringItem);
        }

        return strings;
    }
    
    public static void main(String[] argss) {
    	
    	List<String> list = convertToList("abc^dce|^kjk^kjd|^lll^lll", "|^");
    	
    	for (String str : list) {
    		System.out.println(str);
    	}
    	
    }
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;

/**
 * Used to find the line number in an ifile via a string passed in
 * 
 * @author jmilliron 
 * @version 1.00
 * 
 */
public class IResourceUtil {

	/**
	 * Used to find the line number in an ifile via a string passed in
	 *
	 * @param iFile the file to search
	 * @param stringToMatch string to search in the ifile
	 * @return int line number of string found
	 */
	public static int findLineNo(IFile iFile, String stringToMatch) {
		
		//buffered reader
		BufferedReader br = null;
		
		try {

			//create new buffered reader
			br = new BufferedReader(new InputStreamReader(iFile.getContents()));
			
			//tmp holder
			String thisLine;
			
			//initialize line count
			int lineCnt = 0;
			
			//read until no more lines to read
			while ((thisLine = br.readLine()) != null) {

				//increment line count
				lineCnt++;
				
				//if string is found
				if (thisLine.contains(">" + stringToMatch + "<")) {

					//return line count
					return lineCnt;
					
				} 
			}		
		
		}
		
		//catch exception
		catch (Exception ex) {

		} finally {

			//try to close buffered reader
			if (br != null) {
				try {
					
					//close
					br.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
		
		//return -1 if line number was not found
		return -1;
	}
	
}

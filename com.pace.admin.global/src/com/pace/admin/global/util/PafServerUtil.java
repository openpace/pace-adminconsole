/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PafXStream;

/**
 * This class is used to provide helper utility methods for the paf servers. The
 * paf servers are located under the <Admin console home>/conf directory.
 * 
 * @version 2.00
 * @author jmilliron
 * 
 */
public class PafServerUtil {

	private static Logger logger = Logger.getLogger(PafServerUtil.class);

	private static Set<PafServer> confServers = new TreeSet<PafServer>();
	/**
	 * Trys to load the paf servers.xml file and returns the paf servers.
	 * 
	 * @return A set of all paf servers
	 */
	@SuppressWarnings("unchecked")
	public static Set<PafServer> getPafServers() {
		
		return confServers;
		
	}
	
	@SuppressWarnings("unchecked")
	public static void loadPafServers() {

		// full file name of paf server file
		String fullFileName = getPafServerFileName();

		// try to import the paf servers
		try {
			confServers = (Set<PafServer>) PafXStream
					.importObjectFromXml(fullFileName);
		} catch (PafConfigFileNotFoundException e) {

			// log message
			logger.warn("File '" + fullFileName + "' not found.");

		}

		if ( confServers == null ) {
			
			// if paf file didn't exist, create a blank tree set
			confServers = new TreeSet<PafServer>();
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Set<PafServer> getRunningPafServers() {

		Set<PafServer> runningServerSet = new HashSet<PafServer>();
		
		Set<PafServer> servers = getPafServers();
		
		if (servers != null && servers.size() > 0 ) {
			
			for (PafServer pafServer : servers) {
				
				if ( ServerMonitor.getInstance().isServerRunning(pafServer) ) {
					runningServerSet.add(pafServer);
				}
			
			}
		}
		
		// return running paf servers
		return runningServerSet;

	}

	/**
	 * 
	 *  Gets the full paf servers xml path and file name
	 *
	 * @return
	 */
	private static String getPafServerFileName() {

		return Constants.ADMIN_CONSOLE_GLOBAL_APPDATA_CONF_DIRECTORY + Constants.SERVERS_FILE;
	}

	/**
	 * Get an instance of a paf server from the serverName.
	 * 
	 * @param serverName
	 *            name of server
	 * @throws paf
	 *             server not found
	 * @return instance of paf server
	 */
	public static PafServer getServer(String serverName)
			throws PafServerNotFound {

		// check for null
		if (serverName == null) {

			//logger.error("The server name variable was null.");

			return null;

		}

		// get paf servers
		Set<PafServer> pafServers = getPafServers();

		// loop through servers
		for (PafServer pafServer : pafServers) {

			// if paf server name matches server name arg, return the server
			if (pafServer.getName().equals(serverName)) {

				// return instance of the found paf server
				return pafServer;

			}

		}

		// if no servers are found, throw exception
		throw new PafServerNotFound("PafServer '" + serverName
				+ "' was not found in the listing of PafServers.");

	}

	/**
	 * Gets the url attribute off of hte servername passed in
	 * 
	 * @parm server The paf server to use 
	 * @return url to server
	 * @throws PafServerNotFound 
	 */
	public static PafServer getServerFromUrl(String url) 
		throws PafServerNotFound {
						
		return getServer( getServerNameFromUrl(url));		
		
		
	}
	
	/**
	 * Tries to get the default server
	 * 
	 * @throws paf
	 *             server not found
	 * @return the defaul paf server
	 */
	@SuppressWarnings("unchecked")
	public static PafServer getDefaultServer() throws PafServerNotFound {

		// get the paf servers
		Set<PafServer> pafServers = getPafServers();

		// loop over paf servers
		for (PafServer pafServer : pafServers) {

			// check for default server
			if (pafServer.isDefaultServer()) {

				// return default server
				return pafServer;
			}

		}

		// if no servers are found, throw exception
		throw new PafServerNotFound("No default pace server, nor default project server found.");

	}
	
	/**
	 * Appends the file seperator char if not present to end of server home name
	 * 
	 * @parm server The paf server to use 
	 * @return the fixed servers home directory
	 */
	public static String getProjectServerHome(PafServer server) {

		//return server home dir
		return server.getHomeDirectory();
	}
	
	/**
	 * Gets the url attribute off of hte servername passed in
	 * 
	 * @parm server The paf server to use 
	 * @return url to server
	 */
	public static String getServerWebserviceUrl(String serverName) {
		
		String url = null;
		
		PafServer pafServer = null;
		
		try {
			
			pafServer = PafServerUtil.getServer(serverName);
			
			if ( pafServer != null ) {
				url = pafServer.getCompleteWSDLService();
			}
		} catch (PafServerNotFound e1) {

			logger.error("The server: " + serverName + " was not found.");
			
		}
		
		return url;
		
	}
	
	/**
	 * Gets the url attribute off of hte servername passed in
	 * 
	 * @parm server The paf server to use 
	 * @return url to server
	 */
	public static String getServerNameFromUrl(String url) {
						
		Set<PafServer> pafServers = getPafServers();		
		
		if ( pafServers != null ) {
			
			for ( PafServer pafServer : pafServers)  {
				
				if ( pafServer.getCompleteWSDLService() != null && pafServer.getCompleteWSDLService().equals(url)) {
					
					return pafServer.getName();
					
				}
				
			}
			
		}
				
		return null;
		
	}
	
	/**
	 * 
	 *  Adds a paf server to list of servers
	 *
	 * @param pafServer	Server to add.
	 */
	public static void addPafServer(PafServer pafServer) {
		
		//if null, throw exc
		if ( pafServer == null ) {
			
			throw new IllegalArgumentException("Paf Server can not be null");
			
		}

		
		//set default server
		if ( pafServer.isDefaultServer() ) {
			setDefaultServer(pafServer);
		}
		
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		
		//if null, create empty set
		if ( pafServers != null ) {
				
			//if contains..remove first
			if ( pafServers.contains(pafServer)) {
				pafServers.remove(pafServer);
				//also purge the instance.
				ServerMonitor.getInstance().removeServer(pafServer);
			}
			
			//add to server set
			pafServers.add(pafServer);
			
			//add to monitor
			ServerMonitor.getInstance().addServer(pafServer);
			
			//get full path and file naem for server .xml
			String pafServerPathAndFileName = getPafServerFileName();
			
			//export servers to file system
			PafXStream.exportObjectToXml(pafServers, pafServerPathAndFileName);
	
		}
		
	}

	public static void renamePafServer(String oldName, String newName) throws PafServerNotFound {
		//if null, throw exc
		PafServer oldServer = getServer(oldName);
		PafServer newServer = null;
		try {
			newServer = (PafServer)(oldServer.clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//set default server
		if ( oldServer.isDefaultServer() ) {
			setDefaultServer(newServer);
		}
		
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		
		//if null, create empty set
		if ( pafServers != null ) {
				
			//if contains..remove first
			if ( pafServers.contains(oldServer)) {
				
				pafServers.remove(oldServer);
				ServerMonitor.getInstance().removeServer(oldServer);
				
				newServer.setName(newName);
				
				pafServers.add(newServer);
				ServerMonitor.getInstance().addServer(newServer);
			}
			
			//get full path and file naem for server .xml
			String pafServerPathAndFileName = getPafServerFileName();
			
			//export servers to file system
			PafXStream.exportObjectToXml(pafServers, pafServerPathAndFileName);
		}
	}
	/**
	 * 
	 * Deletes a paf server vai name.
	 *
	 * @param pafServerName Paf Server to delete.
	 */
	public static void deletePafServer(String pafServerName) {
	
		//if null, throw exc
		if ( pafServerName == null ) {
			
			throw new IllegalArgumentException("Paf Server can not be null");
			
		}
		
		try {
			
			//get paf server to delete
			PafServer pafServer = getServer(pafServerName);
			
			//delete paf server
			deletePafServer(pafServer);
			
		} catch (PafServerNotFound e) {
			logger.error("The server: " + pafServerName + " was not found.");
			
		}
	
		
	}
	
	/**
	 * 
	 *  Delete Paf Server
	 *
	 * @param pafServer Paf server to delete.
	 */
	public static void deletePafServer(PafServer pafServer) {
		
		//if null, throw exc
		if ( pafServer == null ) {
			
			throw new IllegalArgumentException("Paf Server can not be null");
			
		}
		
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		
		//if null, create empty set
		if ( pafServers != null ) {
				
			//if contains
			if ( pafServers.contains(pafServer)) {
				
				//remove server
				pafServers.remove(pafServer);
				
				ServerMonitor.getInstance().removeServer(pafServer);
				
				//get full path and file naem for server .xml
				String pafServerPathAndFileName = getPafServerFileName();
				
				//export servers to file system
				PafXStream.exportObjectToXml(pafServers, pafServerPathAndFileName);
			}
						
		}
		
	}

	
	/**
	 * 
	 *  Clones an existing server.
	 *
	 * @param pafServer	server to be cloned.
	 * @return	cloned server
	 */
	public static PafServer clonePafServer(PafServer pafServer) {
	
		//if null, throw exc
		if ( pafServer == null ) {
			
			throw new IllegalArgumentException("Paf Server can not be null");
			
		}
		
		//try to clone server
		PafServer clonedPafServer = null;
		try {
			clonedPafServer = (PafServer) pafServer.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("Could not clone server " + pafServer.getName());
		}
		
		//return cloned server
		return clonedPafServer;
	}

	public static boolean findDuplicateServerName(String newName) {
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		if ( pafServers != null ) {
			//if contains
			for ( PafServer pafServer : pafServers)  {
				if(	pafServer.getName().equals(newName) )
					return true;
			}
		}
		return false;
	}

	public static void printServerNames() {
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		if ( pafServers != null ) {
			//if contains
			for ( PafServer pafServer : pafServers)  {
				System.out.println(pafServer.getName());
			}
		}
	}

	/**
	 * 
	 *  Set's the default server
	 *
	 * @param defaultPafServer
	 */
	public static void setDefaultServer(PafServer defaultPafServer ) {
		
		//if null, throw exc
		if ( defaultPafServer == null ) {
			
			throw new IllegalArgumentException("Default Paf Server can not be null");
			
		}
		
		
			
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
		
		//if null, create empty set
		if ( pafServers != null ) {
				
			
			PafServer currentDefaultPafServer = null;
			
			try {
				currentDefaultPafServer = getDefaultServer();
			} catch (PafServerNotFound e) {
			}
			
			if ( currentDefaultPafServer != null) {
				
				currentDefaultPafServer.setDefaultServer(false);
				
				pafServers.remove(currentDefaultPafServer);
				
				pafServers.add(currentDefaultPafServer);
			}
			
			//if contains..remove first
			if ( pafServers.contains(defaultPafServer)) {
				pafServers.remove(defaultPafServer);
			}
			
			//set to default
			defaultPafServer.setDefaultServer(true);
			
			//add to server set
			pafServers.add(defaultPafServer);
			
			//get full path and file naem for server .xml
			String pafServerPathAndFileName = getPafServerFileName();
			
			//export servers to file system
			PafXStream.exportObjectToXml(pafServers, pafServerPathAndFileName);
			
		}
			
	}
	
	/**
	 * 
	 * Checks to seeif serverName is a valid server
	 *
	 * @param serverName	Name of server to validate
	 * @return	true if server is valid
	 */
	public static boolean isValidServer(String serverName) {
	
		boolean isValidServer = false;
		
		if ( serverName != null ) {
		
			Set<PafServer> pafServers = getPafServers();
			
			if ( pafServers != null && pafServers.size() > 0 ) {
			
				for (PafServer pafServer : pafServers) {
					
					if ( pafServer.getName().equalsIgnoreCase(serverName)) {
						
						isValidServer = true;
						break;
						
					}
					
				}
				
			}
			
		}
		
		return isValidServer;
		
	}
	
	
	/**
	 * Get a set of assigned server groups
	 * @return instance of paf server
	 */
	public static List<String> getServerGroups() {
		
		List<String> groups = new ArrayList<String>();
		
		Set<PafServer> pafServers = getPafServers();
		
		if ( pafServers != null && pafServers.size() > 0 ) {
		
			for (PafServer pafServer : pafServers) {
				
				if ( pafServer.getServerGroupName() != null && !pafServer.getServerGroupName().isEmpty() && !groups.contains(pafServer.getServerGroupName())) {
					
					groups.add(pafServer.getServerGroupName());
					
				}
				
			}
			
		}
		
		if(groups.size() > 0){
			Collections.sort(groups);
		}
		
		return groups;
	}
	
	/**
	 * Get a list of servers that don't have groups assigned.
	 * @return instance of paf server
	 */
	public static List<PafServer> getServersWithoutGroups() {
		
		List<PafServer> groups = new ArrayList<PafServer>();
		
		Set<PafServer> pafServers = getPafServers();
		
		if ( pafServers != null && pafServers.size() > 0 ) {
		
			for (PafServer pafServer : pafServers) {
				
				if (pafServer.getServerGroupName() ==null ||  pafServer.getServerGroupName().isEmpty()){
					
					groups.add(pafServer);
					
				}
				
			}
			
		}
		
		
		return groups;
	}
	
	/**
	 * Gets all the servers for the specified group name.
	 * @param groupName
	 * @return
	 */
	public static List<PafServer> getServers(String groupName){
		
		List<PafServer> servers = new ArrayList<PafServer>(10);
		Set<PafServer> pafServers = getPafServers();
		
		if ( pafServers != null && pafServers.size() > 0 ) {
			
			for (PafServer pafServer : pafServers) {
				
				if ( pafServer.getServerGroupName() != null && pafServer.getServerGroupName().equalsIgnoreCase(groupName)) {
					
					servers.add(pafServer);
					
				}
				
			}
			
		}
		
		return servers;
	}
	
	/**
	 * Save the existing PafServers to xml
	 */
	public static void saveServersToXml(){
		
		//get all existing servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();
				
		//if null, create empty set
		if ( pafServers != null ) {
		
			//get full path and file naem for server .xml
			String pafServerPathAndFileName = getPafServerFileName();
		
			//export servers to file system
			PafXStream.exportObjectToXml(pafServers, pafServerPathAndFileName);
		}
	}
}

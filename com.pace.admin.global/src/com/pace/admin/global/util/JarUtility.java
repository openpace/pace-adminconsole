/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.log4j.Logger;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class JarUtility {

	private static Logger logger = Logger.getLogger(JarUtility.class);
	
	public static void loadJarFile(String jarFilePath, Thread currentThread, ClassLoader currentClassLoader) {
		
		if ( jarFilePath != null ) {
		
			File jarFile = new File(jarFilePath);
			
			loadJarFile(jarFile, currentThread, currentClassLoader);
			
		}
		
		
	}
	
	public static void loadJarFile(File jarFile, Thread currentThread, ClassLoader currentClassLoader) {
			
			//if jar folder is not null and members are greter than 0
			if ( jarFile != null && jarFile.exists() ) {
		
				try {
				
					URL fileURL = jarFile.toURL();
	
					if ( fileURL != null ) {
						
						//create new class loader on top of current class loader
						URLClassLoader 	classLoader = new URLClassLoader(new URL[] { fileURL }, currentClassLoader);
						
						//if class loader is not null, set current threads class loader
						if ( classLoader != null ) 	{
							
							currentThread.setContextClassLoader(classLoader);
							
						}
						
					}
					
				} catch (MalformedURLException e) {

					logger.debug("Malformed URL exception");
					e.printStackTrace();

				}								
			}			
		
	}
	
}

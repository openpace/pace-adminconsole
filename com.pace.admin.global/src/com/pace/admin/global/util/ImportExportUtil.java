/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ImportExportFileType;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PafXStream;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.SimpleCellNote;
import com.pace.server.client.SimpleMemberTagData;
import com.thoughtworks.xstream.XStream;

/**
 * Exports/Imports model from/to file system from/to by user.
 *
 * 1. Simple Cell Note Arrays
 * 2. Simple Member Tag Data Arrays
 * 
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ImportExportUtil {
	
	private static Logger logger = Logger.getLogger(ImportExportUtil.class);
		
	/**
	 * 
	 * Imports Cell Notes from the file system
	 *
	 * @param importFullFileName Dir + file name of file to import
	 * @return	imported cell notes
	 */
	public static SimpleCellNote[] importSimpleCellNotes(String importFullFileName) {
		
		//cell not array
		SimpleCellNote[] simpleCellNotes = null;
		
		//create file
		File importFile = new File(importFullFileName);
		
		//ensure file is file and can be read
		if ( importFile != null && importFile.isFile() && importFile.canRead()) {

			//if binary
			if ( importFullFileName.endsWith(Constants.FILE_EXT_BINARY)) {
								
				//import object
				Object binaryObject = importBinaryStr(importFullFileName);
				
				//ensure object is a simple cell note []
				if (binaryObject instanceof SimpleCellNote[] ) {
					
					//cast
					simpleCellNotes = (SimpleCellNote[]) binaryObject;
					
				}
				
			//if xml
			} else if ( importFullFileName.endsWith(PafBaseConstants.XML_EXT)) {
				
				try {
					
					//import simple cell notes
					simpleCellNotes = (SimpleCellNote[]) PafXStream.importObjectFromXml(importFullFileName);
					
				} catch (PafConfigFileNotFoundException e) {
					
					logger.error(e.getMessage());
					
				}
			}
			 
			
		}	
		
		//return simple cell note array
		return simpleCellNotes;		
		
	}
	
	/**
	 * 
	 * Imports Simple Member Tag Data from the file system
	 *
	 * @param importFullFileName
	 * @return
	 */
	public static SimpleMemberTagData[] importSimpleMemberTagData(String importFullFileName) {
		
		//simple member tag data rray
		SimpleMemberTagData[] simpleMemberTagData = null;
		
		//create file
		File importFile = new File(importFullFileName);
		
		//ensure file is file and can be read
		if ( importFile != null && importFile.isFile() && importFile.canRead()) {

			//if binary
			if ( importFullFileName.endsWith(Constants.FILE_EXT_BINARY)) {
								
				//import object
				Object binaryObject = importBinaryStr(importFullFileName);
				
				//ensure object is a simple member tag data []
				if (binaryObject instanceof SimpleMemberTagData[] ) {
					
					//cast
					simpleMemberTagData = (SimpleMemberTagData[]) binaryObject;
					
				}
				
			//if xml
			} else if ( importFullFileName.endsWith(PafBaseConstants.XML_EXT)) {
				
				try {
					
					//import simple cell notes
					simpleMemberTagData = (SimpleMemberTagData[]) PafXStream.importObjectFromXml(importFullFileName);
					
				} catch (PafConfigFileNotFoundException e) {
					logger.error(e.getMessage());
				}
			}
			 
			
		}	
		
		//return simple member tag data array
		return simpleMemberTagData;		
		
	}
	
	/**
	 * 
	 * Imports any file from XStream using the full file name.
	 *
	 * @param importFullFileName name of file to import
	 * @return imported object
	 */
	private static Object importBinaryStr(String importFullFileName) {

		//plain object
		Object binaryObject = null;
		
		//in file stream
		FileInputStream inFile = null;
		
		//object input stream
		ObjectInputStream ois = null;
		
		try {
			
			//create input stream
			 inFile = new FileInputStream(importFullFileName);
			 
			 //create object input stream
			 ois = new ObjectInputStream(inFile);
			 
			 //convert read object into an XStream xml string
			 String renderedXml = (String) ois.readObject();
			 
			 //convert from XML to a Java object
			 binaryObject = PafXStream.getXStream().fromXML(renderedXml);
			 
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage());
			
		} catch (IOException e) {
			
			logger.error(e.getMessage());
			
		} catch (ClassNotFoundException e) {
			
			logger.error(e.getMessage());
			
		} finally {
			
			//close object input stream
			if (ois != null ) {
				
				try {
					ois.close();
				} catch (IOException e) {
					//do nothing
				}
				
				ois = null;
				
			}

			//close file
			if ( inFile != null) {
				try {
					inFile.close();
				} catch (IOException e) {
					//do nothing							
				}
				
				inFile = null;
			}
			
		}
		
		//return object
		return binaryObject;
	}

	/**
	 * 
	 *  Exports simple cell notes to the file system.
	 *
	 * @param simpleCellNotes	Simple Cell Notes to export.
	 * @param exportDirectory	The directory to export notes.
	 * @param exportFileType	Type of file to export, binary or xml.
	 * @throws Exception
	 * @retruns File name of exported simple cell note file
	 */
	public static String exportSimpleCellNotes(SimpleCellNote[] simpleCellNotes, String exportDirectory, ImportExportFileType exportFileType) throws Exception {
					
		String cellNoteFileName = null;
		
		if ( simpleCellNotes != null && exportDirectory != null && exportFileType != null ) {
		
			logger.info("About to export " + simpleCellNotes.length + " cell notes.	" + new Date());
			
			SimpleDateFormat timeStampFormatter = new SimpleDateFormat("yyyyMMddhhmmss");
			cellNoteFileName = exportDirectory + File.separator + Constants.CELL_NOTE_FILE_NAME + 
					"-" + timeStampFormatter.format(Calendar.getInstance().getTime());
			
			PafXStream.setMode(XStream.NO_REFERENCES);
			
			//export in binary
			if ( exportFileType.equals(ImportExportFileType.Binary)  ) {
			
				//add binary extension
				cellNoteFileName += Constants.FILE_EXT_BINARY;
			
				try {
					
					exportBinary(cellNoteFileName, simpleCellNotes);
									
					logger.info("Successfully exported " + simpleCellNotes.length + " cell notes. " + new Date());
					
				} catch (IOException e) {
					
					logger.error("Probem exporting cell notes: " + e.getMessage());
					
					throw new Exception(e.getMessage());
					
				} 
				
			//export in XML
			} else if ( exportFileType.equals(ImportExportFileType.XML) ) {
				
				//add xml extension
				cellNoteFileName += PafBaseConstants.XML_EXT;
				
				//export simple cell notes in xml format
				PafXStream.exportObjectToXml(simpleCellNotes, cellNoteFileName);
				
			}
			
		}		
		
		return cellNoteFileName;
		
	}
	
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param memberTagDataItems
	 * @param exportDirectory
	 * @param exportFileType
	 * @throws Exception 
	 */
	public static String exportMemberTagData(SimpleMemberTagData[] memberTagDataItems, String exportDirectory, ImportExportFileType exportFileType) throws Exception {
		
		String memberTagDataFileName = null;
		
		if ( memberTagDataItems != null && exportDirectory != null && exportFileType != null ) {
			
			logger.info("About to export " + memberTagDataItems.length + " member tag data items.	" + new Date());
			
			SimpleDateFormat timeStampFormatter = new SimpleDateFormat("yyyyMMddhhmmss");
			memberTagDataFileName = exportDirectory + File.separator + Constants.MEMBER_TAG_DATA_FILE_NAME + 
					"-" + timeStampFormatter.format(Calendar.getInstance().getTime());
			
			PafXStream.setMode(XStream.NO_REFERENCES);
			
			//export in binary
			if ( exportFileType.equals(ImportExportFileType.Binary)  ) {
			
				//add binary extension
				memberTagDataFileName += Constants.FILE_EXT_BINARY;
			
				try {
					
					exportBinary(memberTagDataFileName, memberTagDataItems);
									
					logger.info("Successfully exported " + memberTagDataItems.length + " member tag data items. " + new Date());
					
				} catch (IOException e) {
					
					logger.error("Probem exporting member tag data items: " + e.getMessage());
					
					throw new Exception(e.getMessage());
					
				} 
				
			//export in XML
			} else if ( exportFileType.equals(ImportExportFileType.XML) ) {
				
				//add xml extension
				memberTagDataFileName += PafBaseConstants.XML_EXT;
				
				//export simple cell notes in xml format
				PafXStream.exportObjectToXml(memberTagDataItems, memberTagDataFileName);
				
			}
			
		}		
		
		return memberTagDataFileName;
	}
	
	/**
	 * 
	 * Exports an object as binary.
	 *
	 * @param fullFileName			file name and directory to export to
	 * @param object	object to export
	 * @throws IOException
	 */
	public static void exportBinary(String fullFileName, Object object) throws IOException {
		
//		create xml string
		String exportCellNoteXmlStr = PafXStream.getXStream().toXML(object);		
						
		FileOutputStream fos = null;
		
		ObjectOutputStream oos = null;
		
		try {
			
			//create file output stream
			fos = new FileOutputStream(fullFileName);
			
			//create object output stream
			oos = new ObjectOutputStream(fos);
			
			//write out object
			oos.writeObject(exportCellNoteXmlStr);
									
		} catch (IOException e) {
						
			throw e;
			
		} finally {
			
			//close object output stream
			if ( oos != null ) {
				
				try {
					oos.close();
				} catch (IOException e) {
					//do nothing
				}
				
				oos = null;
			}
			
			//close file output stream
			if ( fos != null ) {
				
				fos.close();
				fos = null;
			}
			
			
		}
		
	}
	
	public static boolean okToDeployToNonMatchingServerProjects(String appId, List<PafServer> servers){
		boolean ok = true;
		Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
		for(PafServer server : servers){
			//Don't bother checking server that aren't running.
			if(!runningServerSet.contains(server)) continue;
			//Get the app state and compare the id to the passed in id.
			ApplicationState appState = ServerMonitor.getInstance().getApplicationState(server);
			if(!appState.getApplicationId().equals(appId)){
				ok = false;
				break;
			}
		}
		
		if(!ok){
			String question = "One or more Pace servers are currently running a project with a different application id.\r\nDo you still wish to deploy?";
			ok = GUIUtil.askUserAQuestion("Application ID Mismatch", question, MessageDialog.QUESTION, 1);
		}
		return ok;
	}
	
	public static boolean okToDeployNonRunningServers(boolean allRunning, List<PafServer> servers){
		if(!allRunning){
			String question = "One or more Pace servers is not currently running.\r\nDo you still wish to deploy to remaining servers?" + "\r\n\r\n" + getServersStatusAsString(servers);
			boolean okToRun = GUIUtil.askUserAQuestion("Pace Servers Not Running", question, MessageDialog.QUESTION, 1);
			if(!okToRun){
				return false;
			}
		}
		return true;
	}

	private static String getServersStatusAsString(List<PafServer> servers){

		Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
		StringBuilder sb = new StringBuilder();
		for(PafServer server : servers){
			sb.append(server.getName()).append(" : ");
			String statusMsg = "Running";
			if(server.isDisabled()){
				statusMsg = "Disabled";
			} else if(!runningServerSet.contains(server)){
		    	statusMsg = "Stopped";
		    }
		    sb.append(statusMsg).append("\r\n");
		}
		return sb.toString();
	}
	
	/**
	 * Converts Map to a string of servers and results
	 * @param results
	 * @return 
	 */
	public static String parseUploadResults(Map<String, Boolean> results){
		StringBuilder sb = new StringBuilder();
		for (String server : results.keySet()) {
		    Boolean status = results.get(server);
		    sb.append(server).append(" : ");
		    String statusMsg = "Success";
		    if(status == null){
		    	statusMsg = "Skipped";
		    } else if(!status){
		    	statusMsg = "Failure";
		    }
		    sb.append(statusMsg).append("\r\n");
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param isSuccessful
	 * @param outMessage
	 */
	public static void showProcessCompleteDialog(boolean isSuccessful, String outMessage){
		
		int icon = SWT.ICON_ERROR;
		String heading = Constants.DIALOG_ERROR_HEADING;
		if(isSuccessful){
			icon = SWT.ICON_INFORMATION;
			heading = Constants.DIALOG_INFO_HEADING;
			logger.info(outMessage);
		} else {
			logger.error(outMessage);
		}
	
		GUIUtil.openMessageWindow(heading, outMessage, icon);
	}
	
	public static boolean getResults(Collection<Boolean> results){
		for (Boolean status : results) {
		    if(status != null && !status){
		    	return false;
		    }
		}
		return true;
	}
	
}

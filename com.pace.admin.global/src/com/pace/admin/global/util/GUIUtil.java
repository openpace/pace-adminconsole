/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.ui.PafServer;

public class GUIUtil {
	
	private static final Logger logger = Logger.getLogger(GUIUtil.class);

	/**
	 * Opens a Message box dialog informing the user of a message/problem.
	 * 
	 * @param title
	 *            Window Title
	 * @param message
	 *            Window Message
	 */
	public static void openMessageWindow(String title, String message) {

		openMessageWindow(title, message, SWT.NONE);

	}
	
	/**
	 * Opens a Message box dialog informing the user of a message/problem.
	 * 
	 * @param title
	 *            Window Title
	 * @param message
	 *            Window Message
	 * @parm sytle
	 * 			  Style options
	 */
	public static void openMessageWindow(String title, String message, int style) {

		//KRM - Added these to statements to fix a bunch of error that were introduced by TTN-2395
		if(PlatformUI.getWorkbench() == null) {
			logger.warn("Workbench is null, This proably means that a job (background thread) is trying to access the active window which will be null.  Message: " + message);
			return;
		}
		
		if(PlatformUI.getWorkbench().getActiveWorkbenchWindow() == null) {
			logger.warn("Workbench Window is null.  This proably means that a job (background thread) is trying to access the active window which will be null.  Message: " + message);
			return;
		}
		
		//get an instance of the message box dialog
		MessageBox messageBox = new MessageBox(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), style);
		
		//set title on message box
		messageBox.setText(title);
		
		//set message on message box
		messageBox.setMessage(message);
		
		//open message box
		messageBox.open();

	}
	
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @param question
	 *            Question to ask user
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean openMessageWindowOkCancel(String question) {

		//set flag to false
		boolean verified = false;

		//create an instance of the message dialog
		MessageDialog dialog = new MessageDialog(null, "Question?", null,
				question, MessageDialog.QUESTION, new String[] { "OK", "Cancel" },
				0);
		
		//open dialog and cache return value
		int result = dialog.open();

		//if the return value is 0, then user clicked yes
		if (result == 0) {
			verified = true;
		}
		
		//return verified flag
		return verified;
	}
		
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @param question
	 *            Question to ask user
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean openMessageWindowOkCancel(String title, String question) {

		//set flag to false
		boolean verified = false;

		//create an instance of the message dialog
		MessageDialog dialog = new MessageDialog(null, title, null,
				question, MessageDialog.WARNING, new String[] { "OK", "Cancel" },
				0);
		
		//open dialog and cache return value
		int result = dialog.open();

		//if the return value is 0, then user clicked yes
		if (result == 0) {
			verified = true;
		}
		
		//return verified flag
		return verified;
	}
	
	/**
	 * Opens an Eclipse Error dialog.
	 * @param title
	 * @param message
	 * @return
	 */
	public static void openErrorDialog(String title, String message){
		MessageDialog.openError(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell(),	
				title,
				message);
	}
		
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @param question
	 *            Question to ask user
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean askUserAQuestion(String question) {

		return askUserAQuestion("Question?", question, MessageDialog.QUESTION, 0);

	}
	
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @parm title The title of the message box.
	 * @param question
	 *            Question to ask user
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean askUserAQuestion(String title, String question) {

		return askUserAQuestion(title, question, MessageDialog.QUESTION);
		
	}
	
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @parm title The title of the message box.
	 * @param question
	 *            Question to ask user
	 * @param style
	 * 				Message Dialog style
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean askUserAQuestion(String title, String question, int style) {
		return askUserAQuestion(title, question, style, 0);
	}
	
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @parm title The title of the message box.
	 * @param question
	 *            Question to ask user
	 * @param style
	 * 				Message Dialog style
	 * @parm defaultButton 
	 * 				Index of the default button.
	 * @return boolean stating if user answered yes or no
	 */
	public static boolean askUserAQuestion(String title, String question, int style, int defaultButton) {

		//set flag to false
		boolean verified = false;

		//create an instance of the message dialog
		MessageDialog dialog = new MessageDialog(null, title, null,
				question, style, new String[] { "Yes", "No" },
				defaultButton);
		
		//open dialog and cache return value
		int result = dialog.open();

		//if the return value is 0, then user clicked yes
		if (result == 0) {
			verified = true;
		}
		
		//return verified flag
		return verified;
	}
	
	
	/**
	 * Opens a Message box dialog asking the user a question.
	 * 
	 * @parm title The title of the message box.
	 * @param question
	 *            Question to ask user
	 * @param style
	 * 				Message Dialog style
	 * @parm defaultButton 
	 * 				Index of the default button.
	 * @return boolean stating if user answered yes or no
	 */
	public static int askUserAQuestionWithCancel(String title, String question, int style, int defaultButton) {

		//create an instance of the message dialog
		MessageDialog dialog = new MessageDialog(null, title, null,
				question, style, new String[] { "Yes", "No" ,"Cancel"},
				defaultButton);
		
		//open dialog and cache return value
		int result = dialog.open();

		//return verified flag
		return result;
	}
	
	public static int promptUserAboutAppId(PafApplicationDef oldPafAppDef, IProject iProject) {
		
		int result =0;
		PafApplicationDef newPafAppDef = PafApplicationUtil.getPafApp(iProject);
		
		if ( oldPafAppDef != null && newPafAppDef != null ) {
			
			if ( oldPafAppDef.getAppId() != null && newPafAppDef.getAppId() != null)  {
				
				if ( ! oldPafAppDef.getAppId().equalsIgnoreCase(newPafAppDef.getAppId())) {
					
					// TTN-2429 Add cancel button on the warning message.
					result =  GUIUtil.askUserAQuestionWithCancel(Constants.DIALOG_WARNING_HEADING, 
							"Conflicting Application Id's.\n\nReplace the imported project's application id '" + newPafAppDef.getAppId()+ "' with local project's application id '" + oldPafAppDef.getAppId() + "'? (Yes or No)  \n\nOr select Cancel to cancel the project import.", MessageDialog.QUESTION_WITH_CANCEL , 0) ;
						// if user clicks on Yes
					 if (result ==0){
						newPafAppDef.setAppId(iProject.getName());
						
						PafApplicationUtil.setPafApps(iProject, newPafAppDef);
					 } // if user clicks on cancel.
					 else if ( result == 2) { // When user clicks on No
							IFolder backupFolder = iProject.getFolder(Constants.BACKUP);
							IFolder confFolder = iProject.getFolder(Constants.CONF_DIR);
							// Confirm if import operation needs to be cancelled. 
							//move contents of backup to conf ..
							try {
								FileHelperUtil.moveContentsOfIFolder(backupFolder,confFolder, true);
							} catch (CoreException e) {
								logger.error(e.getMessage());
							}
						
						}	
					}
					
				}
				
			}
		
		return result;
			
		}
	
	
	
public static void promptUserAboutAppIdForServer(PafApplicationDef oldPafAppDef, IProject iProject) {
		
		PafApplicationDef newPafAppDef = PafApplicationUtil.getPafApp(iProject);
		
		if ( oldPafAppDef != null && newPafAppDef != null ) {
			
			if ( oldPafAppDef.getAppId() != null && newPafAppDef.getAppId() != null)  {
				
				if ( ! oldPafAppDef.getAppId().equalsIgnoreCase(newPafAppDef.getAppId())) {
					
					int result =  GUIUtil.askUserAQuestionWithCancel(Constants.DIALOG_WARNING_HEADING, 
							"Conflicting Application Id's.\n\nReplace the imported project's application id '" + newPafAppDef.getAppId()+ "' with local project's application id '" + oldPafAppDef.getAppId() + "'? (Yes or No)  \n\nOr select Cancel to cancel the project import.", MessageDialog.QUESTION_WITH_CANCEL , 0) ;
						// if user clicks on Yes
					    if (result ==0){
					    	
					    	newPafAppDef.setAppId(iProject.getName());
					    	PafApplicationUtil.setPafApps(iProject, newPafAppDef);
						}	// if user clicks on cancel
						else if( result == 2) { // When user clicks on No
							IFolder backupFolder = iProject.getFolder(Constants.BACKUP);
							IFolder confFolder = iProject.getFolder(Constants.CONF_DIR);
							// Confirm if import operation needs to be cancelled. 
							//move contents of backup to conf ..
							try {
								FileHelperUtil.moveContentsOfIFolder(backupFolder,confFolder, true);
							} catch (CoreException e) {
								logger.error(e.getMessage());
							}
						
						}
					
					}
				
				}
			
			}
	
	}

	/**
	 * Open popup alerting user server is no longer running.
	 * 
	 * @param server server that isn't running
	 */
	public static void serverNotRunning(PafServer server) {
		
		if ( server != null ) {
			
			String errorMessage = "Server '" + server.getName() + "' is no longer running.  Please start the server and try again.";
		
			logger.error(errorMessage);
			
			GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, errorMessage, SWT.ICON_ERROR);
									
		}
		
	}
	
}

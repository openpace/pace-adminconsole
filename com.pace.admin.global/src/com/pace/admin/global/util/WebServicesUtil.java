/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.activation.DataHandler;
import javax.xml.ws.BindingProvider;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.team.core.TeamException;
import org.springframework.util.StopWatch;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.DataHandlerPaceProjectUtil;
import com.pace.base.utility.FileUtils;
import com.pace.server.client.AuthMode;
import com.pace.server.client.ClientInitRequest;
import com.pace.server.client.DownloadAppRequest;
import com.pace.server.client.DownloadAppResponse;
import com.pace.server.client.PafSecurityDomainGroups;
import com.pace.server.client.PafSecurityGroup;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafServiceProviderService;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.SecurityGroup;
import com.pace.server.client.UploadAppRequest;
import com.pace.server.client.UploadAppResponse;

/**
 * Utility to get a reference to a webservice and to check if a server is
 * running via the web service
 * 
 * @version 1.0
 * @author jmilliron
 * 
 */
public class WebServicesUtil {

	private static final int DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS = 200;
	private static final String PACE_SERVICE_PROVIDER_SERVICE_WSDL = "PafServiceProviderService.wsdl";
	// logger
	private static final Logger logger = Logger.getLogger(WebServicesUtil.class);
	private static PafServiceProviderService psps ;
	//serviceMap - url, PafService
	private static Map<String, PafService> servicesMap = new ConcurrentHashMap<String, PafService>();
	//serverAckMap - url, pafServerAck
	private static Map<String, PafServerAck> serverAckMap = new ConcurrentHashMap<String, PafServerAck>();


	public static void loadPafService() {
		//We don't want to download the WSDL every time
		String wsdlFileLoc = Constants.ADMIN_CONSOLE_CONF_DIRECTORY + PACE_SERVICE_PROVIDER_SERVICE_WSDL;
		StopWatch sw = new StopWatch();
		sw.start();
		File file = new File(wsdlFileLoc);
		try {
			psps = new PafServiceProviderService(file.toURI().toURL());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sw.stop();
		logger.info(String.format("Took %s (ms) to load WSDL", sw.getTotalTimeMillis()));
	}


	/**
	 * Creates a new PafServer using the URL and Timeout defined in teh PafServer object.
	 * @param url
	 * @param timeOut
	 * @return
	 */
	public static PafService getPafService(String url, long timeOut){
		
		PafService service = psps.getPafServiceProviderPort();
		 
		java.util.Map<String, Object> requestContext = ((BindingProvider) service).getRequestContext();
		requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
		
		Client client = ClientProxy.getClient(service);
		
		HTTPConduit http = (HTTPConduit) client.getConduit();
		
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(timeOut);
		httpClientPolicy.setReceiveTimeout(timeOut);
		 
		http.setClient(httpClientPolicy);
		
		return service;
		
	}
	
	public static PafService getPafService(String url) {
		PafService service = null;
		if (servicesMap.containsKey(url)) {
			service = servicesMap.get(url);
		} 
		else {
			if( psps != null ) {
				logger.debug("Start building pace service from '" + url + "' at " + Calendar.getInstance().getTime());
//				System.out.println("Start building pace service from '" + url + "' at " + Calendar.getInstance().getTime());
				service = psps.getPafServiceProviderPort();
				((BindingProvider)service).getRequestContext().put(
						BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
				servicesMap.put(url, service);
				logger.debug("End building pace service from '" + url + "' at " + Calendar.getInstance().getTime());
//				System.out.println("End building pace service from '" + url + "' at " + Calendar.getInstance().getTime());
			}
		}
		return service;
	}
	

	public static void removePafService(String url) {
		servicesMap.remove(url);
	}
	
	public static void resetPafServiceMap() {
		servicesMap.clear();
	}
	
	/**
	 * Gets a PafServerAck from a PafServer object
	 * @param pafServer 
	 * @return PafServerAck
	 */
	public static PafServerAck getPafServerAck(PafServer pafServer){
		PafServerAck pafServerAck = null;
		if( pafServer != null && ServerMonitor.getInstance().isServerRunning(pafServer) ) {
			pafServerAck = getPafServerAckPrivate(pafServer.getCompleteWSDLService());
		}
		return pafServerAck;
	}
	
	/**
	 * Gets a PafServerAck from a PafServer URL.
	 * NOTE:  If two PafServers exist with the same URL this method may return the wrong status. Consider using: {@link #getPafServerAck(PafServer)}
	 * @param url WSDL URL to the Pace Server
	 * @return PafServerAck
	 */
	public static PafServerAck getPafServerAck(String url) {
		PafServerAck pafServerAck = null;
		if( url != null && ServerMonitor.getInstance().isServerRunning(url) ) {
			pafServerAck = getPafServerAckPrivate(url);
		}
		return pafServerAck;
	}
	
	/**
	 * This method returns the cached PafServerAck if it exists, if not it will attempt to connect.
	 * NOTE: this method does not check to see if the server is running prior to attempting to get the PafServerAck.
	 * If you require to connection check prior to getting the PafServerAck use:
	 * {@link #getPafServerAck(PafServer)} or {@link #getPafServerAck(String)} as those methods check for a running server.
	 * @param url WSDL URL to the Pace Server
	 * @return PafServerAck 
	 * @see #getPafServerAck(String url) 
	 * @see #getPafServerAck(PafServer pafServer)
	 */
	private static PafServerAck getPafServerAckPrivate(String url) {
		PafServerAck pafServerAck = null;
		if(url != null) {
			if (serverAckMap.containsKey(url)) {
				pafServerAck = serverAckMap.get(url);
			} 
			else {
				PafService service = WebServicesUtil.getPafService(url);
				ClientInitRequest clientInitRequest = new ClientInitRequest();
				
				logger.debug( "Start retrieving PafServerAck from clientInit service for '" + url + "' at " + Calendar.getInstance().getTime());
				
				try {
					
					InetAddress thisIp = InetAddress.getLocalHost();
					clientInitRequest.setClientVersion(SystemUtil.getAcVersion());
					clientInitRequest.setIpAddress(thisIp.getHostAddress());
					
				} catch (UnknownHostException e) {
					
					logger.error(e.getMessage());
					
					e.printStackTrace();
					
				}
				
				clientInitRequest.setClientType(Messages.getString("WebServicesUtil.AdminConsoleName")); //$NON-NLS-1$
																
				try {
				
					pafServerAck = service.clientInit(clientInitRequest);
					
				} catch (RuntimeException re) {
					
					logger.error("ClientInit service call error: " + re.getMessage());
					
				} catch (PafSoapException_Exception e) {
	
					logger.error("ClientInit service call error: " + Messages.getString("WebServicesUtil.PafSoapException") + e.getMessage()); //$NON-NLS-1$
				}
				
				if ( pafServerAck != null ) {
					
					serverAckMap.put(url, pafServerAck);
					
				}
				else {
				
					logger.warn("Can not access clientInit service at url: " + url);
					serverAckMap.remove(url);
					
				}
				logger.debug( "End retrieving PafServerAck from clientInit service for '" + url + "' at " + Calendar.getInstance().getTime());
			}
		}
		return pafServerAck;
	}

	public static void removePafServerAck(String url) {
		serverAckMap.remove(url);
	}
	
	public static void resetPafServerAckMap() {
		serverAckMap.clear();
	}
	
	/**
	 * Uploads project to server over web services.
	 * 
	 * @param project project to upload
	 * @param pafServers list of servers server to upload project to
	 * @param refreshConfiguration reloads application on server
	 * @param refreshCube reloads cube data on server
	 * @parma serverGroup name of the server group
	 * @param filterSet if set only uploading filtered pace project data
	 * @return true if upload was successful
	 */
	public static Map<String, Boolean> uploadProjectToServerGroup(PaceProject paceProject, List<PafServer> pafServers, boolean refreshConfiguration, 
			boolean refreshCube, String serverGroup, Set<ProjectElementId> filterSet) {
		
		Map<String, Boolean> results = new TreeMap<String, Boolean>();
		
		if ( paceProject != null && pafServers != null ) {
			Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
			for(PafServer server : pafServers){
				Boolean succesfullyUploaded = false;
				if(runningServerSet.contains(server)){
					try {
						succesfullyUploaded = uploadProjectToServer(paceProject, server, refreshConfiguration, refreshCube, filterSet);
					}  catch (Exception e) {
						logger.error(e.getMessage());
					} 
				} else {
					succesfullyUploaded = null;
				}
				results.put(server.getName(), succesfullyUploaded);
			}
				
		}
		
		return results;
	}

	/**
	 * Uploads project to server over web services.
	 * 
	 * @param project project to upload
	 * @param pafServers list of servers server to upload project to
	 * @param refreshConfiguration reloads application on server
	 * @param refreshCube reloads cube data on server
	 * @parma serverGroup name of the server group
	 * @param filterSet if set only uploading filtered pace project data
	 * @return true if upload was successful
	 */
	public static Map<String, Boolean> uploadProjectToServerGroup(IProject project, List<PafServer> pafServers, boolean refreshConfiguration, 
			boolean refreshCube, String serverGroup, Set<ProjectElementId> filterSet) {
		
		logger.debug( "Start uploading project '" + project.getName() + " to server group'" + serverGroup + "' at " + Calendar.getInstance().getTime());
		
		Map<String, Boolean> results = new TreeMap<String, Boolean>();
		
		
		if ( project != null && pafServers != null ) {
		
			try {
				
				//conf folder
				IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
				
				PaceProject paceProject = null;
				
				boolean isFilteredData = false;
				
				if ( filterSet != null && filterSet.size() > 0 ) {
					isFilteredData = true;
				} 
				
				if ( isFilteredData ) {
					paceProject = new XMLPaceProject(iConfFolder.getLocation().toString(), filterSet, false);
				} else {
					paceProject = new XMLPaceProject(iConfFolder.getLocation().toString(), false);
				}
						
				results = uploadProjectToServerGroup(paceProject, pafServers, refreshConfiguration, refreshCube, serverGroup, filterSet);
				
				
			} catch (InvalidPaceProjectInputException e) {
				logger.error(e.getMessage());
			} catch (PaceProjectCreationException e) {
				logger.error(e.getMessage());
			}
		}
		
		logger.debug( "End uploading project '" + project.getName() + " to server group '" + serverGroup + "' at " + Calendar.getInstance().getTime());
		
		return results;
		
	}
	
			

	public static PafServerAck testPafServerAck(String url, boolean forceRefresh) {
		PafServerAck pafServerAck = null;

		if ( testServerConnectionWithUrl(url) ) {
			
			if ( serverAckMap.containsKey(url) && ! forceRefresh ) {
				
				pafServerAck = serverAckMap.get(url);
				
			} else {		
			
				//get stub
				PafService service = WebServicesUtil.testPafService(url);
	
				try {
	
					// if stub not null
					if (service != null) {
						
						ClientInitRequest clientInitRequest = new ClientInitRequest();
						
						try {
							
							InetAddress thisIp = InetAddress.getLocalHost();
							
							clientInitRequest.setIpAddress(thisIp.getHostAddress());
							
						} catch (Exception e) {
							
							logger.error(e.getMessage());
							
							e.printStackTrace();
							
						}
						
						clientInitRequest.setClientType(Messages.getString("WebServicesUtil.AdminConsoleName")); //$NON-NLS-1$
																		
						try {
						
							pafServerAck = service.clientInit(clientInitRequest);
							
						} catch (RuntimeException re) {
							//TODO do better error handeling on this
							logger.error(re.getMessage());
							
							
						}
						
						if ( pafServerAck != null ) {
							
							AuthMode authMode = pafServerAck.getAuthMode();
							
							if ( authMode != null ) {
							
								logger.debug("Auth Mode from server is " + authMode.toString());
								
							}							
							
							serverAckMap.put(url, pafServerAck);
							
						}
	
					} else {
						
						logger.warn("Can not access service at url: " + url);
						
					}
	
				/*
					// catch a remote exception call
				} catch (RemoteException re) {
	
					logger.error(Messages.getString("WebServicesUtil.RemoteException") + re.getMessage()); //$NON-NLS-1$
				*/
				} catch (PafSoapException_Exception e) {
	
					logger.error(Messages.getString("WebServicesUtil.PafSoapException") + e.getMessage()); //$NON-NLS-1$
				}
				
			}
		} else {
			
			if ( serverAckMap.containsKey(url) ) {
				
				serverAckMap.remove(url);
				
			}
		}
		
		return pafServerAck;
	}
			
			
	public static Map<String, Set<String>> createDomainSecurityGroupMap(SecurityGroup[] paceSecurityGroups) {

		Map<String, Set<String>> domainSecurityGroupMap = new TreeMap<String, Set<String>>(String.CASE_INSENSITIVE_ORDER);
		
		if ( paceSecurityGroups != null ) {
		
			for (SecurityGroup paceSecurityGroup : paceSecurityGroups ) {
				
				String domainName = paceSecurityGroup.getSecurityDomainNameTxt();
				
				Set<String> securityGroupNameSet = null;
				
				if ( domainSecurityGroupMap.containsKey(domainName)) {
					
					securityGroupNameSet = domainSecurityGroupMap.get(domainName);
					
				} else {
					
					securityGroupNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
															
				}
				
				securityGroupNameSet.add(paceSecurityGroup.getSecurityGroupNameTxt());
				
				domainSecurityGroupMap.put(domainName, securityGroupNameSet);
				
			}
			
		}	
		
		return domainSecurityGroupMap;
		
	}
	
	public static Map<String, Set<String>> createDomainSecurityGroupMap(List<PafSecurityDomainGroups> securityDomainGroupList) {
		
		Map<String, Set<String>> domainPaceSecurityGroupMap = new TreeMap<String, Set<String>>(String.CASE_INSENSITIVE_ORDER);
		
		if ( securityDomainGroupList != null && securityDomainGroupList.size() > 0 ) {
			
			for ( PafSecurityDomainGroups pafSecurityDomainGroup: securityDomainGroupList ) {
				
				String domainName = pafSecurityDomainGroup.getDomain();
				
				Set<String> securityGroupSet = null;
				
				if ( domainPaceSecurityGroupMap.containsKey(domainName)) {
					
					securityGroupSet = domainPaceSecurityGroupMap.get(domainName);
					
				} 
				
				if ( securityGroupSet == null ) {
					
					securityGroupSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
				}
				
				if ( pafSecurityDomainGroup.getSecurityGroups() != null ) {
					
					for ( PafSecurityGroup pafSecurityGroup : pafSecurityDomainGroup.getSecurityGroups() ) {
						
						if ( pafSecurityGroup.getGroupName() != null)  {
							
							securityGroupSet.add(pafSecurityGroup.getGroupName());
							
						}
						
					}
					
				}
				
				domainPaceSecurityGroupMap.put(domainName, securityGroupSet);
				
			}
			
		}
		
		return domainPaceSecurityGroupMap;
	}
	
	/**
	 * Uploads project to server over web services.
	 * 
	 * @param project project to upload
	 * @param pafServer server to upload project to
	 * @param refreshConfiguration reloads application on server
	 * @param refreshCube reloads cube data on server
	 * @return true if upload was successful
	 */
	public static boolean uploadProjectToServer(IProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube) {
		return uploadProjectToServer(project, pafServer, refreshConfiguration, refreshCube, null);
	}
	
	/**
	 * Uploads project to server over web services.
	 * 
	 * @param project project to upload
	 * @param pafServer server to upload project to
	 * @param refreshConfiguration reloads application on server
	 * @param refreshCube reloads cube data on server
	 * @param filterSet if set only uploading filtered pace project data
	 * @return true if upload was successful
	 */
	public static boolean uploadProjectToServer(IProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube, Set<ProjectElementId> filterSet) {
		
		logger.debug( "Start uploading project '" + project.getName() + " to server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime());
		
		boolean succesfullyUploaded = false;
		
		if ( project != null && pafServer != null ) {
		
			try {
				
				//conf folder
				IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
				
				PaceProject paceProject = null;
				
				boolean isFilteredData = false;
				
				if ( filterSet != null && filterSet.size() > 0 ) {
					isFilteredData = true;
				} 
				
				if ( isFilteredData ) {
					paceProject = new XMLPaceProject(iConfFolder.getLocation().toString(), filterSet, false);
				} else {
					paceProject = new XMLPaceProject(iConfFolder.getLocation().toString(), false);
				}
						
				//upload project to server
				succesfullyUploaded = uploadProjectToServer(paceProject, pafServer, refreshConfiguration, refreshCube, filterSet);
				
			} catch (InvalidPaceProjectInputException e) {
				logger.error(e.getMessage());
			} catch (PaceProjectCreationException e) {
				logger.error(e.getMessage());
			}
		}
		
		logger.debug( "End uploading project '" + project.getName() + " to server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime());
		
		return succesfullyUploaded;
		
	}
	
	/**
	 * Uploads project to server over web services.
	 * 
	 * @param project project to upload
	 * @param pafServer server to upload project to
	 * @param refreshConfiguration reloads application on server
	 * @param refreshCube reloads cube data on server
	 * @param filterSet if set only uploading filtered pace project data
	 * @return true if upload was successful
	 */
	public static boolean uploadProjectToServer(PaceProject project, PafServer pafServer, boolean refreshConfiguration, boolean refreshCube, Set<ProjectElementId> filterSet) {
		
		boolean succesfullyUploaded = false;

		if ( project != null && pafServer != null ) {
		
			String url = pafServer.getCompleteWSDLService();
			
			try {
				
				boolean isFilteredData = false;
				
				if ( filterSet != null && filterSet.size() > 0 ) {
					isFilteredData = true;
				} 
										
				PafService pafService = WebServicesUtil.getPafService(url);
				
				PafServerAck pafServerAck = getPafServerAck(url);
				
//				if ( pafServerAck == null ) {
//				
//					pafServerAck = getPafServerAck(url);
//				}
				
				if( pafServerAck != null && pafService != null ) {
					
					UploadAppRequest request = new UploadAppRequest();
					
					request.setClientId(pafServerAck.getClientId());
	
					if ( isFilteredData ) {
						
						for ( ProjectElementId filter : filterSet ) {
							
							request.getProjectElementIdFilters().add(com.pace.server.client.ProjectElementId.fromValue(filter.toString()));
							
						}
						
					}
					
					DataHandler dh2 = DataHandlerPaceProjectUtil.convertPaceProjectToDataHandler(project, FileUtils.getSystemTempDirectory().getAbsolutePath());
					
					request.setPaceProjectDataHandler(dh2);
								
					request.setApplyConfigurationUpdate(refreshConfiguration);
					
					request.setApplyCubeUpdate(refreshCube);
					
					if ( project instanceof XMLPaceProject) {
						request.setPartialDeployment(((XMLPaceProject) project).isUpdateOnly());
					}
					
					UploadAppResponse resp = pafService.uploadApplication(request);
					
					succesfullyUploaded = resp.isSuccess();
									
				}
			} catch (PafSoapException_Exception e) {
				logger.error(e.getMessage());
			} catch (ProjectSaveException e) {
				logger.error(e.getMessage());
			} catch (RuntimeException e) {
				logger.error(e.getMessage());
			}
			
//			if ( succesfullyUploaded ) {
//				String outMessage = "Project '" + project.getProjectInput() + "' was successfully uploaded to server '" + pafServer.getName() + "'";
//				GUIUtil.openMessageWindow("Upload Project", outMessage);
//			} else {
//				String outMessage = "Project '" + project.getProjectInput() + "' was not successfully uploaded to server '" + pafServer.getName() + "'";
//				GUIUtil.openMessageWindow("Upload Project", outMessage);
//			}
		}
		
		return succesfullyUploaded;
		
	}

	
	/**
	 * Download a project from a server into a local project
	 * 
	 * @param project project to import into
	 * @param pafServer server to download project from
	 * @return true if successful
	 */
	public static boolean downloadProjectFromServer(IProject project, PafServer pafServer) {
		
		return downloadProjectFromServer(project, pafServer, null);
		
	}
	
	/**
	 * Download a project from a server into a local project
	 * 
	 * @param project project to import into
	 * @param pafServer server to download project from
	 * @param filterSet if set only download the project element ids specified
	 * @return true if successful
	 */
	public static boolean downloadProjectFromServer(final IProject project, final PafServer pafServer, Set<ProjectElementId> filterSet) {
		
		logger.debug( "Start downloading project '" + project.getName() + " to server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime());
		
		boolean succesfullyDownloaded = false;
		
		if ( pafServer != null ) {
		
			boolean isProjectManagedBySubclipse = SubclipseUtil.isSubclipseProject(project);
			
			try{

				//if svn project, unmap from reposity, will remap in a bit				
				if ( isProjectManagedBySubclipse ) {
						
					SubclipseUtil.unmapSubclipseProject(project);
					
				}
			}catch(TeamException e) // TTN-2625 Handle the subclipse error.
			{
				logger.info(e.getMessage());
			}
				try {				
				boolean isFilteredData = false;
				
				if ( filterSet != null && filterSet.size() > 0 ) {
					isFilteredData = true;
				}
				
				if ( isFilteredData ) {
					
					PafProjectUtil.backupProject(project, filterSet.toArray(new ProjectElementId[0]));
					
				} else {
				
					PafProjectUtil.backupProject(project, null);
					
				}
				
				
				PaceProject pp = downloadProjectFromServer(pafServer, filterSet);				
				
				if ( pp != null ) {
					
					//conf folder
					IFolder iConfFolder = project.getFolder(Constants.CONF_DIR);
					
					//TODO: DO BACKUP					
					if ( isFilteredData ) {
						pp.saveTo(iConfFolder.getLocation().toString(), new HashSet<ProjectElementId>(filterSet));
					} else {
						pp.saveTo(iConfFolder.getLocation().toString());
					}
			
					IFolder confFolder = project.getFolder(Constants.CONF_DIR);
					
					confFolder.refreshLocal(IResource.DEPTH_INFINITE, null);

					//if svn project, remap to repository
					if ( isProjectManagedBySubclipse ) {

						SubclipseUtil.mapSubclipseProject(project);
						
						IFolder backupFolder = project.getFolder(Constants.BACKUP);
						
						//TODO: Add ignore for backup folder and change move project from server to copy each file and dir seperate
						//		it's causing the svn project to become corrupt.
						if ( backupFolder.exists()) {
							
							//ISVNLocalResource svnResource = SVNWorkspaceRoot.getSVNResourceFor(backupFolder);
			                //new AddIgnoredPatternCommand(svnResource.getParent(), "*.").run(new NullProgressMonitor());
							
						}
						
					}
					
					// try to refresh the directory
					confFolder.refreshLocal(IResource.DEPTH_INFINITE, null);
					
					succesfullyDownloaded = true;
				}
				
			} catch (TeamException e) {
				logger.error(e.getMessage());
			} catch (CoreException e) {
				logger.error(e.getMessage());
			} catch (ProjectSaveException e) {
				logger.error(e.getMessage());
			}	
			
			if ( succesfullyDownloaded ) {
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
				
						String outMessage = "";
						try {
							logger.info("Server project: " + project.getDescription().getName() + ".");
							outMessage = "The project '" + PafApplicationUtil.getPafApp(project).getAppId() + "' on server '" + pafServer.getName() + "', was successfully downloaded to local project '" + project.getName() + "'.";
						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//GUIUtil.openMessageWindow("Download Project", outMessage); 
						ConsoleWriter.writeMessage(outMessage);
					}
				});	
			}
			else {
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
				
						String outMessage = "Project '" + project.getName() + "' was not successfully downloaded to server '" + pafServer.getName() + "'";
						//GUIUtil.openMessageWindow("Download Project", outMessage);
						ConsoleWriter.writeMessage(outMessage);
					}
				});	
			}
		}		
		
		logger.debug( "End downloading project '" + project.getName() + " to server '" + pafServer.getName() + "' at " + Calendar.getInstance().getTime());
		
		return succesfullyDownloaded;
				
	}

	/**
	 * Downloads a pace project from the server
	 * 
	 * @param pafServer server to download project from
	 * @param filterSet optional filter set
	 * @return
	 * @throws PafSoapException_Exception
	 * @throws IOException
	 * @throws InvalidPaceProjectInputException
	 * @throws PaceProjectCreationException
	 */
	public static PaceProject downloadProjectFromServer(PafServer pafServer, Set<ProjectElementId> filterSet)  {
		
		PaceProject paceProject = null;

		String url = pafServer.getCompleteWSDLService();
		
		PafServerAck pafServerAck = getPafServerAck(url);
		
		if ( pafServerAck == null ) {
		
//			loadPafServerAckMap(url);
			pafServerAck = getPafServerAck(url);
//			pafServerAck = WebServicesUtil.getPafServerAck(url, true);
		}
		
		PafService pafService = WebServicesUtil.getPafService(url);
		
		if( pafServerAck != null && pafService != null ) {
			
			DownloadAppRequest request = new DownloadAppRequest();
			
			request.setClientId(pafServerAck.getClientId());
			
			if ( filterSet != null ) {
				
				for (ProjectElementId filter : filterSet) {
					
					com.pace.server.client.ProjectElementId projectElementId = com.pace.server.client.ProjectElementId.fromValue(filter.toString()); 
					
					request.getProjectElementIdFilters().add(projectElementId);
				}
				
			}
			
			DownloadAppResponse response;
			try {
				response = pafService.downloadApplication(request);
			
				if ( response != null && response.isSuccess() && response.getPaceProjectDataHandler() != null) {
								
					paceProject = DataHandlerPaceProjectUtil.convertDataHandlerToPaceProject(response.getPaceProjectDataHandler(), FileUtils.getSystemTempDirectory().getAbsolutePath());
					
				}		
			} catch (PafSoapException_Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidPaceProjectInputException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PaceProjectCreationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return paceProject;
	}

	/**
	 * Gets a reference to a web service call
	 * 
	 * @param url
	 *            The wsdl url location
	 * 
	 * @return IPafService the web service stub
	 */
	public static PafService testPafService(String url) {
		PafService service = null;

		// if url is null, log error
		if (url == null) {

			logger.error(Messages.getString("WebServicesUtil.URLNotNull")); //$NON-NLS-1$
		}
		else {
			if (servicesMap.containsKey(url)) {
	
				service = servicesMap.get(url);
	
			} else {

				// else try to connect to webservice
				if ( testServerConnectionWithUrl(url)) {
				
					logger.debug(Messages.getString("WebServicesUtil.CreatingWebServiceTo") + url); //$NON-NLS-1$
					
					PafServiceProviderService psps = null;
					
					try {
						//We don't want to download the WSDL every time
						File file = new File("C:\\adminconsole\\" + PACE_SERVICE_PROVIDER_SERVICE_WSDL);
						psps = new PafServiceProviderService(file.toURL());
						
						if( psps != null ) {
							logger.debug("Getting PafServiceProviderPort()");
																									
							service = psps.getPafServiceProviderPort();
							
							
							if( service != null ) {
								((BindingProvider)service).getRequestContext().put(
										BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
								
								logger.debug("Adding service to services Map");
								
								servicesMap.put(url, service);
							}
						}
					} catch (Exception e) {
						
						logger.error("Service Error: " + url);
						logger.error(e.getMessage());
					}
												
				}
			}	
		
//			//try to get pafserver from url
//			PafServer pafServer = null;
//			try {
//				pafServer = PafServerUtil.getServer(PafServerUtil.getServerNameFromUrl(url));
//			} catch (PafServerNotFound e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			//if server has a connection timeout or receive timeout set
//			if ( service != null && pafServer != null && (pafServer.getServiceConnectionTimeoutInMilliseconds() != null 
//								|| pafServer.getServiceReceiveTimeoutInMilliseconds() != null) ) {
//			
//				logger.debug("Setting service timeouts");
//				
//				Long connectionTimeout = pafServer.getServiceConnectionTimeoutInMilliseconds();
//				Long receiveTimeout = pafServer.getServiceReceiveTimeoutInMilliseconds();
//																
//				Client cl = ClientProxy.getClient(service);
//				
//				HTTPConduit http = (HTTPConduit) cl.getConduit();
//				
//				HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
//				
//				logger.debug("Service Connection Timeout: " + connectionTimeout);
//				logger.debug("Service Receive Timeout: " + receiveTimeout);
//				
//				httpClientPolicy.setConnectionTimeout(connectionTimeout);
//				httpClientPolicy.setReceiveTimeout(receiveTimeout);
//				 
//				http.setClient(httpClientPolicy);
//				
//			}
		}
		
		//return service
		return service;
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param url
	 * @return
	 * @throws InterruptedException 
	 */
	public static boolean testServerConnectionWithUrl(String url) {
		
		if ( url == null ) {
			return false;
		}
		
		boolean succesfullyConnected = false;
						
		HttpURLConnection con = null;
		
		try {
						
			con = (HttpURLConnection) new URL(url).openConnection();
			 
			con.setConnectTimeout(DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS);
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK ) {
				succesfullyConnected = true;
			} 
			
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException: " + e.getMessage());
		} catch (IOException e) {
			logger.error("URL IOException: " + e.getMessage());
		} finally {
			
			if ( con != null ) {
				con.disconnect();
			}
			
		}
		
		return succesfullyConnected;
	}

//	public static PafServerAck getPafServerAckByServerName(String serverName) {
//	return serverAckMap.get(serverName);
//}

///**
// * Gets the servers acknoledgement
// * 
// * @param url
// *            The wsdl url location
// * 
// * @return PafServerAck Contains server information
// */
//public synchronized static PafServerAck getPafServerAck(String url, boolean forceRefresh) {
//	PafServerAck pafServerAck = null;
//
//	if ( isServerRunning(url) ) {
//		
//		if ( serverAckMap.containsKey(url) && ! forceRefresh ) {
//			
//			pafServerAck = serverAckMap.get(url);
//			
//		} else {		
//		
//			//get stub
//			PafService service = WebServicesUtil.getPafService(url);
//
//			try {
//
//				// if stub not null
//				if (service != null) {
//					
//					ClientInitRequest clientInitRequest = new ClientInitRequest();
//					
//					try {
//						
//						InetAddress thisIp = InetAddress.getLocalHost();
//						
//						clientInitRequest.setIpAddress(thisIp.getHostAddress());
//						
//					} catch (Exception e) {
//						
//						logger.error(e.getMessage());
//						
//						e.printStackTrace();
//						
//					}
//					
//					clientInitRequest.setClientType(Messages.getString("WebServicesUtil.AdminConsoleName")); //$NON-NLS-1$
//																	
//					try {
//					
//						pafServerAck = service.clientInit(clientInitRequest);
//						
//					} catch (RuntimeException re) {
//						//TODO do better error handeling on this
//						logger.error(re.getMessage());
//						
//						
//					}
//					
//					if ( pafServerAck != null ) {
//						
//						AuthMode authMode = pafServerAck.getAuthMode();
//						
//						if ( authMode != null ) {
//						
//							logger.debug("Auth Mode from server is " + authMode.toString());
//							
//						}							
//						
//						serverAckMap.put(url, pafServerAck);
//						
//					}
//
//				} else {
//					
//					logger.warn("Can not access service at url: " + url);
//					
//				}
//
//			/*
//				// catch a remote exception call
//			} catch (RemoteException re) {
//
//				logger.error(Messages.getString("WebServicesUtil.RemoteException") + re.getMessage()); //$NON-NLS-1$
//			*/
//			} catch (PafSoapException_Exception e) {
//
//				logger.error(Messages.getString("WebServicesUtil.PafSoapException") + e.getMessage()); //$NON-NLS-1$
//			}
//			
//		}
//	} else {
//		
//		if ( serverAckMap.containsKey(url) ) {
//			
//			serverAckMap.remove(url);
//			
//		}
//	}
//	
//	return pafServerAck;
//}

//	/**
//	 * Gets a reference to a web service call
//	 * 
//	 * @param url
//	 *            The wsdl url location
//	 * 
//	 * @return IPafService the web service stub
//	 */
//	public synchronized static PafService getPafServiceByUrl(String url) {
//		PafService service = null;
//
//		// if url is null, log error
//		if (url == null) {
//
//			logger.error(Messages.getString("WebServicesUtil.URLNotNull")); //$NON-NLS-1$
//		}
//		else {
//			if (servicesMap.containsKey(url)) {
//	
//				service = servicesMap.get(url);
//	
//			} else {
//
//				// else try to connect to webservice
//				if ( isServerRunning(url)) {
//				
//					logger.debug(Messages.getString("WebServicesUtil.CreatingWebServiceTo") + url); //$NON-NLS-1$
//					
//					PafServiceProviderService psps = null;
//					
//					try {
//						//We don't want to download the WSDL every time
//						File file = new File(Constants.ADMIN_CONSOLE_CONF_DIRECTORY + PACE_SERVICE_PROVIDER_SERVICE_WSDL);
//						psps = new PafServiceProviderService(file.toURL());
//						
//						if( psps != null ) {
//							logger.debug("Getting PafServiceProviderPort()");
//																									
//							service = psps.getPafServiceProviderPort();
//							
//							
//							if( service != null ) {
//								((BindingProvider)service).getRequestContext().put(
//										BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
//								
//								logger.debug("Adding service to services Map");
//								
//								servicesMap.put(url, service);
//							}
//						}
//					} catch (Exception e) {
//						
//						logger.error("Service Error: " + url);
//						logger.error(e.getMessage());
//					}
//												
//				}
//			}	
//		
////			//try to get pafserver from url
////			PafServer pafServer = null;
////			try {
////				pafServer = PafServerUtil.getServer(PafServerUtil.getServerNameFromUrl(url));
////			} catch (PafServerNotFound e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////			
////			//if server has a connection timeout or receive timeout set
////			if ( service != null && pafServer != null && (pafServer.getServiceConnectionTimeoutInMilliseconds() != null 
////								|| pafServer.getServiceReceiveTimeoutInMilliseconds() != null) ) {
////			
////				logger.debug("Setting service timeouts");
////				
////				Long connectionTimeout = pafServer.getServiceConnectionTimeoutInMilliseconds();
////				Long receiveTimeout = pafServer.getServiceReceiveTimeoutInMilliseconds();
////																
////				Client cl = ClientProxy.getClient(service);
////				
////				HTTPConduit http = (HTTPConduit) cl.getConduit();
////				
////				HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
////				
////				logger.debug("Service Connection Timeout: " + connectionTimeout);
////				logger.debug("Service Receive Timeout: " + receiveTimeout);
////				
////				httpClientPolicy.setConnectionTimeout(connectionTimeout);
////				httpClientPolicy.setReceiveTimeout(receiveTimeout);
////				 
////				http.setClient(httpClientPolicy);
////				
////			}
//		}
//		
//		//return service
//		return service;
//	}
	
//	/**
//	 * Checks to see if a server is currently running or not
//	 * 
//	 * @param url
//	 *            The wsdl url location
//	 * 
//	 * @return boolean true/false
//	 */
//
//	public static boolean isServerRunning(PafServer pafServer) {
//		
//		return ServerMonitor.getInstance().isServerRunning(pafServer);
//	}
//	/**
//	 * Checks to see if a server is currently running or not
//	 * 
//	 * @param url
//	 *            The wsdl url location
//	 * 
//	 * @return boolean true/false
//	 */
//
//	public static boolean isServerRunning(String url) {
//
//		// set default
//		boolean isServerRunning = false;
//		
//		try {
//			
//			//try to get pafserver from url
//			PafServer pafServer = PafServerUtil.getServerFromUrl(url);
//			
//			logger.debug("Url is:"+url);
//			
//			if(pafServer!=null) {
//
//				isServerRunning = ServerMonitor.getInstance().isServerRunning(pafServer);
//				
//			}
//		} catch (PafServerNotFound e) {
//			logger.error("Server not found");
//		} 
//		
//		// log info
//		logger.debug("URL: '" + url + "' is alive: " + isServerRunning); //$NON-NLS-1$ //$NON-NLS-2$
//
//		// return true if server is running or false if not.
//		return isServerRunning;
//	}
//	
//
//	/**
//	 * Checks to see if a server is currently running or not
//	 * 
//	 * @param url
//	 *            The wsdl url location
//	 * 
//	 * @return boolean true/false
//	 */
//
//	public synchronized static boolean isServerRunning(String url) {
//
//		// set default
//		boolean isServerRunning = false;
//		
//		try {
//						
//			//try to get pafserver from url
//			PafServer pafServer = PafServerUtil.getServer(PafServerUtil.getServerNameFromUrl(url));
//			
//			logger.info("Url is:"+url);
//			
////			int urlTimeoutInMilliseconds = Constants.DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS;
//			
//			if(pafServer!=null)
//			isServerRunning = pafServer.isServerRunning();            
//                       
//		
//		} catch (PafServerNotFound e) {
//			logger.error("Server not found");
//		} 
//		
//		// log info
//		logger.info("URL: '" + url + "' is alive: " + isServerRunning); //$NON-NLS-1$ //$NON-NLS-2$
//
//		// return true if server is running or false if not.
//		return isServerRunning;
//	}


//	public static void refershPafServerAck(String url) {
//		
//		getPafServerAck(url, true);
//		
//	}
//	
//	/**
//	 * Gets the servers acknoledgement
//	 * 
//	 * @param url
//	 *            The wsdl url location
//	 * 
//	 * @return PafServerAck Contains server information
//	 */
//	public static PafServerAck getPafServerAckByUrl(String url) {
//		
//		return getPafServerAck(url, false);
//		
//	}
//	
	
	// TTN-2623 Add method back so that the Client can be initiated before authentication.
	public static void loadPafServerAckMap(String url) {
		PafServerAck pafServerAck = null;
		PafService service = WebServicesUtil.getPafService(url);
		ClientInitRequest clientInitRequest = new ClientInitRequest();
		
		logger.debug( "Retrieving PafServerAck from clientInit service for " + url + ".");
		
		try {
			
			InetAddress thisIp = InetAddress.getLocalHost();
			
			clientInitRequest.setIpAddress(thisIp.getHostAddress());
			
		} catch (UnknownHostException e) {
			
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
		}
		
		clientInitRequest.setClientType(Messages.getString("WebServicesUtil.AdminConsoleName")); //$NON-NLS-1$
														
		try {
		
			pafServerAck = service.clientInit(clientInitRequest);
			
		} catch (RuntimeException re) {
			
			logger.error("ClientInit service call error: " + re.getMessage());
			
		} catch (PafSoapException_Exception e) {

			logger.error("ClientInit service call error: " + Messages.getString("WebServicesUtil.PafSoapException") + e.getMessage()); //$NON-NLS-1$
		}
		
		if ( pafServerAck != null ) {
			
			serverAckMap.put(url, pafServerAck);
			
		}
		else {
		
			logger.warn("Can not access clientInit service at url: " + url);
			serverAckMap.remove(url);
			
		}
	}

}

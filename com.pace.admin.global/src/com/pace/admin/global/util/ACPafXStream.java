/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrSeverity;
import com.pace.base.utility.PafXStream;
import com.thoughtworks.xstream.XStream;

/**
 * @author jmilliron
 * 
 */
public class ACPafXStream extends PafXStream {
		
	//logger for class
	private static Logger logger = Logger.getLogger(ACPafXStream.class);
	
	
	static {	
		
		getXStream().setMode(XStream.NO_REFERENCES);
		
	}
	
	
	//import object using IFile
	public static Object importObjectFromXml(IFile iFile)
			throws Exception {
		
		//log message
		logger.debug("Importing object from xml, " + iFile.getName());
		
		//create buffer to hold xml info
		StringBuffer sb = new StringBuffer();

		//object to import
		Object o = null;

		//if file does not exist, throw exception
		if (!iFile.exists()) {
			throw new PafConfigFileNotFoundException("File " + iFile.getName()
					+ " does not exist.", PafErrSeverity.Info);
		}

		//use buffered reader to read in xml file
		BufferedReader br = null;
		try {

			Charset charSet = Charset.forName("UTF-8");
			
			//create new buffered reader
			br = new BufferedReader(new InputStreamReader(iFile.getContents(), charSet));
			
			//use to hold temp line
			String thisLine;
			
			//while there are more lines to read, append to buffer
			while ((thisLine = br.readLine()) != null) {
				sb.append(thisLine);
			}

			//get xstream reference
			XStream xs = getXStream();
			
			//convert string to object
			o = xs.fromXML(sb.toString());
			
			//log success message
			logger.debug("Succesfully import: " + o.getClass().getSimpleName());
		}
		//cach exception
		catch (Exception ex) {

			//log exception and throw again
			logger.error(ex.getMessage());
			throw ex;

		} finally {

			//try to close the buffered reader
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}

		//return object that was imported
		return o;
	}

	//export object to IFile
	public static void exportObjectToXml(Object object, IFile iFile) {
		
		//log message
		logger.debug("Exporting object to xml. "
				+ object.getClass().getSimpleName() + " to " + iFile.getName());
		
		//render object to string
		String xmlObject = PafXStream.getXStream().toXML(object); 
		
		logger.debug("Export String Pre-xsd: " + xmlObject);
		
		xmlObject = PafXStream.addXSDHeader(xmlObject);
		
		logger.debug("Export String Post-xsd: " + xmlObject);
		
		/* old dtd 
		//add dtd header to top of xml string
		String headNode = xmlObject.substring(xmlObject.indexOf('<') + 1, xmlObject.indexOf('>'));
		
		String relativeDTDloc = Constants.CONF_DTD_PREFIX;
		
		//if paf project file, file is located in diff dir than conf and path is shorter
		if ( iFile.toString().endsWith(Constants.PAF_PROJECT_FILE)) {
			
			relativeDTDloc = Constants.PROJECT_DTD_PREFIX;
			
		}
		
		//create new string buffer
		StringBuffer sb = new StringBuffer("<!DOCTYPE " + headNode + " SYSTEM \"" + relativeDTDloc + iFile.getName().replaceFirst(".xml", ".dtd") + "\">\n");
		
		//append the xml String obj to buffer
		sb.append(xmlObject);

		 */

		//use input string to get byte array input stream
		InputStream is = null;
		
		try {

			//get input stream
			//is = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
			is = new ByteArrayInputStream(xmlObject.getBytes("UTF-8"));
			
			//if file exist, set contents, else create new file
			if ( iFile.exists() ) {
				iFile.setContents(is, true, false, null);
			} else {
				iFile.create(is, true, null);
			}
			
			//log success
			logger.info("Succesfully exported "
					+ object.getClass().getSimpleName());
		} catch (Exception ex) {
			logger.warn(ex.getMessage());
		} finally {
			
			//close input stream if not null
			if ( is != null ) {
				
				//try go close
				try {
					is.close();
				} catch (IOException e) {
					//do nothing
				}
			}
			
		}
		
	}


}

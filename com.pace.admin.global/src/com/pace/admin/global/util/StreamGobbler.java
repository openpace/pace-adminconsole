/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import com.pace.admin.global.constants.Constants;

/**
 * 
 * Gobbles streams by reading in an input stream and then logging it.  This prevents
 * an inputstream from filling up and causing deadlock.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class StreamGobbler implements Runnable {
	
	private static Logger logger = Logger.getLogger(StreamGobbler.class);
	
	private String name;

	private InputStream is;

	private Thread thread;

	private boolean dataInStream = false;
	
	private boolean consoleLoggingEnabled = false;
	
	private MessageConsole myConsole = ConsoleUtil.findConsole(Constants.CONSOLE_NAME);

	private MessageConsoleStream msgStream = myConsole.newMessageStream();
	
	public StreamGobbler(InputStream is) {
		this.is = is;
	}
	
	public StreamGobbler(String name, InputStream is) {
		this(is);
		this.name = name;		
	}
	
	/**
	 * 
	 *	Creates a thread and starts it
	 *
	 */
	public void start() {
		thread = new Thread(this);
		thread.start();
	}

	
	
	/**
	 * @return the consoleLoggingEnabled
	 */
	public boolean isConsoleLoggingEnabled() {
		return consoleLoggingEnabled;
	}

	/**
	 * @param consoleLoggingEnabled the consoleLoggingEnabled to set
	 */
	public void setConsoleLoggingEnabled(boolean consoleLoggingEnabled) {
		this.consoleLoggingEnabled = consoleLoggingEnabled;
	}

	/**
	 * Reads in input and then writes to standard out.
	 */
	public void run() {
		
		try {
			
			//create input stream reader and buffered reader
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			//while forever
			while (true) {
				String s = br.readLine();
				
				if (s == null) {
					break;
				}
				
				String output = s;
				
				if ( name != null ) {
					output = "[" + name + "] " + s;
				}
				
				logger.info(output);
				
				if ( isConsoleLoggingEnabled() ) {
					
					msgStream.println(output);
					
				}
				
				dataInStream = true;
				
			}			

		} catch (Exception ex) {
			
			logger.error("Problem reading stream " + name + "... :" + ex);
						
		} finally {
			
			if ( is != null ) {
				
				//close input stream
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}
		}
	}
		
	public boolean isDataInStream() {
	
		return dataInStream;
		
	}
}

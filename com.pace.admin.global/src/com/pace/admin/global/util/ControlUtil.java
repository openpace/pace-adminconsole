/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;

import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;


/**
 * Control utiltiy class.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ControlUtil {

	/**
	 * Up button id
	 */
	public final static int UP_BUTTON_ID = -1;

	/**
	 * Down button id
	 */
	public final static int DOWN_BUTTON_ID = 1;
	
	
	/**
	 * Event hander for when a directional button is pushed.
	 * @param control List control that is associated to the directional buttons.
	 * @param buttonId Id of the button that was pushed.
	 */
	public static void directionalButtonPressed(List control, int buttonId) {
		
		//get selected items
		String[] selItems = control.getSelection();
		
		//get selcted items indexs
		int[] selItemIdx = control.getSelectionIndices();
		
		//initialize vars
		int topSelIdx = 0;
		int bottomSelIdx = 0;
		int newIdx = 0;
		
		//if something is actually selected
		if(selItems != null && selItems.length > 0){
			//index of the item at the top of the selection.
			topSelIdx = selItemIdx[0];
			//index of the item at the bottom of the selection.
			bottomSelIdx = selItemIdx[selItemIdx.length - 1];
			//check top/bottm and the buttons to see if the items can be moved.
			if(buttonId == UP_BUTTON_ID){
				//Selection is at the top, so exit.
				if(topSelIdx == 0){
					return;
				}
			} else{
				//check to see if the bottom item is selected, if so we can't move down.
				if(bottomSelIdx == (control.getItemCount() - 1)){
					return;
				}
			}
			//set the new index.
			newIdx = topSelIdx + buttonId;
			//remove all the items to be removed.
			control.remove(selItemIdx);
			//Indexes to reselect.
			int[] idxToSel = new int[selItems.length];
			//counter
			int i = 0;
			//now reinsert the items at the new position.
			for(String selItem : selItems){
				control.add(selItem, newIdx);
				idxToSel[i] = newIdx;
				newIdx++;
				i++;
			}
			//reselect the items.
			//control.setSelection(selItems);
			control.setSelection(idxToSel);
		}
	}
	
	/**
	 * Event hander for when a directional button is pushed.
	 * @param control CheckboxTableViewer control that is associated to the directional buttons.
	 * @param buttonId Id of the button that was pushed.
	 */
	public static void directionalButtonPressed(CheckboxTableViewer control, int buttonId) {
		
		Table table = control.getTable();
		
		//get selected items
		TableItem[] selItems = table.getSelection();
		ArrayList<String> items = new ArrayList<String>();
		boolean[] checkedStatus = new boolean[selItems.length];
		
		//get selcted items indexs
		int[] selItemIdx = table.getSelectionIndices();
		
		//initialize vars
		int topSelIdx = 0;
		int bottomSelIdx = 0;
		int newIdx = 0;
		
		
		
		//if something is actually selected
		if(selItems != null && selItems.length > 0){
			
			int i = 0;
			for(TableItem selItem : selItems){
				items.add(selItem.getText());
				checkedStatus[i] = selItem.getChecked();
				i++;
			}
			
			
			//index of the item at the top of the selection.
			topSelIdx = selItemIdx[0];
			//index of the item at the bottom of the selection.
			bottomSelIdx = selItemIdx[selItemIdx.length - 1];
			//check top/bottm and the buttons to see if the items can be moved.
			if(buttonId == UP_BUTTON_ID){
				//Selection is at the top, so exit.
				if(topSelIdx == 0){
					return;
				}
			} else{
				//check to see if the bottom item is selected, if so we can't move down.
				if(bottomSelIdx == (table.getItemCount() - 1)){
					return;
				}
			}
			//set the new index.
			newIdx = topSelIdx + buttonId;
			//remove all the items to be removed.
			table.remove(selItemIdx);
			//Indexes to reselect.
			int[] idxToSel = new int[selItems.length];
			//counter
			i = 0;
			//now reinsert the items at the new position.
			for(String selItem : items){
				control.insert(selItem, newIdx);
				Object element = control.getElementAt(newIdx);
				control.setChecked(element, checkedStatus[i]);
				idxToSel[i] = newIdx;
				newIdx++;
				i++;
			}
			//reselect the items.
			//control.setSelection(selItems);
			table.setSelection(idxToSel);
		}
	}
	
		
	/*
	public static void directionalButtonPressed(Table control, int buttonId) {
		
		TableItem[] selectedItems = control.getSelection();
		
		String[] selectedItemTextAr = new String[selectedItems.length];
		boolean[] selectedItemCheckedStatusAr = new boolean[selectedItems.length];
		
		for(int i = 0; i < selectedItems.length; i++) {
			
			selectedItemTextAr[i] = selectedItems[i].getText();
			selectedItemCheckedStatusAr[i] = selectedItems[i].getChecked();
		}
		
//		get selcted items indexs
		int[] selItemIdx = control.getSelectionIndices();
		
		//initialize vars
		int topSelIdx = 0;
		int bottomSelIdx = 0;
		int newIdx = 0;
		
		//if something is actually selected
		if(selectedItems != null && selectedItems.length > 0){
			//index of the item at the top of the selection.
			topSelIdx = selItemIdx[0];
			//index of the item at the bottom of the selection.
			bottomSelIdx = selItemIdx[selItemIdx.length - 1];
			//check top/bottm and the buttons to see if the items can be moved.
			
			
			if(buttonId == UP_BUTTON_ID){
		
				//Selection is at the top, so exit.
				if(topSelIdx == 0){
					return;
				}
			
			} else{
				//check to see if the bottom item is selected, if so we can't move down.
				if(bottomSelIdx == (control.getItemCount() - 1)){
					return;
				}
			}
			
			//set the new index.
			newIdx = topSelIdx + buttonId;
			//remove all the items to be removed.
			control.remove(selItemIdx);
			
			Set<TableItem> selectedTableItems = new HashSet<TableItem>();
			
			//now reinsert the items at the new position.
			for(int i = 0; i < selectedItemTextAr.length; i++ ){
				
				TableItem newTableItem = new TableItem(control, SWT.NONE, newIdx);
				
				newTableItem.setText(selectedItemTextAr[i]);
				newTableItem.setChecked(selectedItemCheckedStatusAr[i]);
				
				selectedTableItems.add(newTableItem);
				
				newIdx++;
			}
			//reselect the items.
			control.setSelection(selectedTableItems.toArray(new TableItem[0]));
		}
	}
	*/
	
	public static boolean isItemInCombo(Combo comboControl, String itemToSearchFor) {
		
		if ( comboControl == null || comboControl.isDisposed() || itemToSearchFor == null)  {
			return false;
		}
		
		String[] comboItems = comboControl.getItems();
		
		if ( comboItems != null ) {
							
			for (String comboItem : comboItems ) {
				
				if ( comboItem.equalsIgnoreCase(itemToSearchFor)) {
					
					return true;
					
				}
				
			}
			
		}
		
		return false;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import com.pace.admin.global.constants.Constants;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.DynamicMemberDef;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafUserSecurity;
import com.pace.base.app.PafWorkSpec;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.view.PafUserSelection;

/**
 * @author jmilliron
 * 
 */
public class PafApplicationUtil {
	
	private static Logger logger = Logger.getLogger(PafApplicationUtil.class);

	/**
	 * Trys to get all the  paf application definition
	 * 
	 * @param iProject
	 *            Current project selected
	 * @return PafApplicationDef[]
	 */
	public static PafApplicationDef[] getPafApps(IProject project) {

		PafApplicationDef[] pafAppDefs = null;

		// if project is not null
		if (project != null) {

			// get conf folder
			IFolder iFolder = project.getFolder(Constants.CONF_DIR);

			try {
				iFolder.refreshLocal(1, null);
			} catch (CoreException e1) {
				logger.error("Couldn't refresh dir : " + iFolder.getFullPath().toString());
			}
			
			// get pafapps.xml ifile
			IFile pafAppFile = iFolder.getFile(PafBaseConstants.FN_ApplicationMetaData);

			// try to get app definition
			try {
				pafAppDefs = (PafApplicationDef[]) ACPafXStream
						.importObjectFromXml(pafAppFile);
				
				if ( pafAppDefs != null ) {
					logger.debug("Application definition was successfully imported");
				} else {
					logger.warn("There was a problem importing application definition");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}

		}

		return pafAppDefs;
	}
	

	/**
	 * 
	 * Trys to get the paf application definition
	 *	
	 * @param project 	Current project selected
	 * @return			Project's app def
	 */
	public static PafApplicationDef getPafApp(IProject project) {
		
		PafApplicationDef pafAppDef = null;
		
		PafApplicationDef[] pafAppDefs = getPafApps(project);
		
		if ( pafAppDefs != null && pafAppDefs.length > 0 ) {
			
			pafAppDef = pafAppDefs[0];
			
		}
		
		return pafAppDef;
		
	}
	
	/**
	 * 
	 * Sets the paf apps to the one passed in.
	 *
	 * @param project	Project of paf_apps
	 * @param pafAppDef	New Paf Application Def
	 */
	public static void setPafApps(IProject project, PafApplicationDef pafAppDef) {
	
		//if project is not null
		if (project != null && pafAppDef != null ) {

			// get conf folder
			IFolder iFolder = project.getFolder(Constants.CONF_DIR);

			// get pafapps.xml ifile
			IFile pafAppFile = iFolder.getFile(PafBaseConstants.FN_ApplicationMetaData);
			
			//export
			ACPafXStream.exportObjectToXml(new PafApplicationDef[] { pafAppDef }, pafAppFile);

		}
		
	}
	
	
	/**
	 * 
	 *	Gets all the paf users from a specific project.
	 *
	 * @param project
	 * @return array of paf user security objects
	 */
	public static PafUserSecurity[] getPafSecurityUsers(IProject project) {
		
		PafUserSecurity[] pafUsers = null;
		
		
		IFile pafSecurityFile = getPafSecurityFile(project);

		try {
				pafUsers = (PafUserSecurity[]) ACPafXStream
							.importObjectFromXml(pafSecurityFile);
		} catch (Exception e) {
				
		}
		
		return pafUsers;
	}	
	
	public static List<PafUserSelection> getUserSelections(IProject project) {
		
		
		List<PafUserSelection> userSelList = new ArrayList<PafUserSelection>();
		
		IFile pafUserSelectionFile = getConfProjectIFile(project, PafBaseConstants.FN_UserSelections);
		
		try {
			PafUserSelection[] userSelections = (PafUserSelection[]) ACPafXStream
						.importObjectFromXml(pafUserSelectionFile);
			
			//if not null, add to list
			if ( userSelections != null ) {
				
				userSelList.addAll(Arrays.asList(userSelections));
				
			}
			
		} catch (Exception e) {
			//TODO: do something?
		}
		
		
		return userSelList;		
		
	}
		
	
	public static List<DynamicMemberDef> getDynamicMemberDefs(IProject project) {		
		
		List<DynamicMemberDef> dynamicMemberDefList = new ArrayList<DynamicMemberDef>();
		
		IFile pafDynamicMemberDefFile = getConfProjectIFile(project, PafBaseConstants.FN_DynamicMembers);
		
		try {
			DynamicMemberDef[] DynamicMemberDefs = (DynamicMemberDef[]) ACPafXStream
						.importObjectFromXml(pafDynamicMemberDefFile);
			
			if ( DynamicMemberDefs != null ) {
				
				dynamicMemberDefList.addAll(Arrays.asList(DynamicMemberDefs));
				
			}
			
		} catch (Exception e) {
			//TODO: do something?
		}
		
		
		return dynamicMemberDefList;		
		
	}
	
	public static List<MemberTagDef> getMemberTagDefs(IProject project) {
		
		
		List<MemberTagDef> memberTagDefList = new ArrayList<MemberTagDef>();
		
		IFile pafMemberTagDefFile = getConfProjectIFile(project, PafBaseConstants.FN_MemberTagMetaData);
		
		try {
			MemberTagDef[] memberTagDefs = (MemberTagDef[]) ACPafXStream
						.importObjectFromXml(pafMemberTagDefFile);
			
			if ( memberTagDefs != null ) {
				
				memberTagDefList.addAll(Arrays.asList(memberTagDefs));
				
			}
			
		} catch (Exception e) {
			//TODO: do something?
		}
		
		
		return memberTagDefList;		
		
	}
	
	/**
	 * 
	 *	Gets the paf security file from a project.
	 *
	 * @param project
	 * @return the iFile of the paf security file
	 */
	public static IFile getPafSecurityFile(IProject project) {
				
		return getConfProjectIFile(project, PafBaseConstants.FN_SecurityMetaData);
	}
			
	private static IFile getConfProjectIFile(IProject project, String fileName) {
		
		IFile iFile = null;
		
		IResource confResource = project.findMember(Constants.CONF_DIR);

		if (confResource != null && confResource.exists()
				&& confResource instanceof IFolder) {

			IFolder confFolder = (IFolder) confResource;

			IResource pafSecurityResource = confFolder
					.findMember(fileName);

			if (pafSecurityResource != null && pafSecurityResource.exists()
					&& pafSecurityResource instanceof IFile) {

				iFile = (IFile) pafSecurityResource;
				
			}
		}
		
		return iFile;
		
		
	}
	
	/**
	 * 
	 *	Gets the paf Security user roles for a specific project.
	 *
	 * @param project
	 * @param userName
	 * @param domainName 
	 * @return the users security roles
	 */
	public static LinkedHashMap<String, PafWorkSpec[]> getPafSecurityUserRoles(IProject project, String userName, String domainName) {
		
		LinkedHashMap<String, PafWorkSpec[]> roleFilters = null;
		
		IResource confResource = project.findMember(Constants.CONF_DIR);

		if (confResource != null && confResource.exists()
				&& confResource instanceof IFolder) {

			IFolder confFolder = (IFolder) confResource;

			IResource pafSecurityResource = confFolder
					.findMember(PafBaseConstants.FN_SecurityMetaData);

			if (pafSecurityResource != null && pafSecurityResource.exists()
					&& pafSecurityResource instanceof IFile) {

				IFile pafSecurityFile = (IFile) pafSecurityResource;

				PafUserSecurity[] pafUsers = null;
				
				try {
					pafUsers = (PafUserSecurity[]) ACPafXStream
							.importObjectFromXml(pafSecurityFile);
					
					for (PafUserSecurity pafUser : pafUsers) {						
						
						//if user names match and either domain's are both null or match
						if ( pafUser.getUserName().equalsIgnoreCase(userName) 
								&& ( (domainName == null && pafUser.getDomainName() == null) || pafUser.getDomainName().equals(domainName) )) {
							
							roleFilters = pafUser.getRoleFilters();
							
							break;
							
						} 
						
						
					}					
					
				} catch (Exception e) {
					
				}
				
			}
		}
		
		return roleFilters;
	}	
}

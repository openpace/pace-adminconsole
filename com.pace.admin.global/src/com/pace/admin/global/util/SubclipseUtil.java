/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.util;

import org.eclipse.core.resources.IProject;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.core.TeamException;
import org.tigris.subversion.subclipse.core.SVNProviderPlugin;
import org.tigris.subversion.subclipse.core.resources.SVNWorkspaceRoot;

/**
 * @author JMilliron
 *
 */
public class SubclipseUtil {

	/**
	 * Determines if project is managed by subclipse.
	 * 
	 * @param project project to test
	 * @return true if project is subclipse.
	 */
	public static boolean isSubclipseProject(IProject project) {
		
		boolean isSubclipseProject = false;
		
		if ( project != null ) {
			
			isSubclipseProject = SVNWorkspaceRoot.isManagedBySubclipse(project);
			
		}
		
		return isSubclipseProject;
	}

	/**
	 * Unmaps project if subclipse project
	 * 
	 * @param project project to unmap
	 * @throws TeamException
	 */
	public static void unmapSubclipseProject(IProject project) throws TeamException {
		
		if ( isSubclipseProject(project) ) {
			
			RepositoryProvider.unmap(project);
			
		}
		
	}
	
	/**
	 * Maps project back to subclipse 
	 * 
	 * @param project project to map to subclipse 
	 * @throws TeamException
	 */
	public static void mapSubclipseProject(IProject project) throws TeamException {
		
		if ( project != null ) {
			
			RepositoryProvider.map(project, SVNProviderPlugin.getTypeId());
			
		}
		
	}
	
}

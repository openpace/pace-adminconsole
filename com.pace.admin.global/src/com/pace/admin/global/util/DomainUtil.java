/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.lang.reflect.Method;
import org.apache.log4j.Logger;

/**
 * Domain Utility
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DomainUtil {

	private static Logger logger = Logger.getLogger(DomainUtil.class);
	
	/**
	 * 
	 *  Gets the domain current domain from the NT System.
	 *
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String getDomainName() {
	
		String domainName = null;
		Object system = null;
		
		
		try {
			Class noparams[] = {};
			Class ntSystem = Class.forName("com.sun.security.auth.module.NTSystem");
			Object obj = ntSystem.newInstance();
			Method method = ntSystem.getMethod("getDomain", noparams);
			system = method.invoke(obj);
			
		} catch (Exception e) {
			logger.error("com.sun.security.auth.module.NTSystem - AD integration will not be available.");
			logger.error(e.getMessage());
		} 
		
		if ( system != null ) {
			
			domainName = system.toString();
			
		}
		
		return domainName;
		
	}
	
	// TTN-1718
	/**
	 * Get user name from NT System
	 */
	public static String getUserName() {
		String userName = null;
		Object system = null;
		
		try {
			Class noparams[] = {};
			Class ntSystem = Class.forName("com.sun.security.auth.module.NTSystem");
			Object obj = ntSystem.newInstance();
			Method method = ntSystem.getMethod("getName", noparams);
			system = method.invoke(obj);
			
		} catch (Exception e) {
			logger.error("com.sun.security.auth.module.NTSystem - AD integration will not be available.");
			logger.error(e.getMessage());
		} 
		
		if ( system != null ) {
			
			userName = system.toString();
			
		}
		
		return userName;
	}
	
	/**
	 * Get user sid from NT System
	 */
	public static String getUserSid() {
		
		String userSID = null;
		Object system = null;
		
		try {
			Class noparams[] = {};
			Class ntSystem = Class.forName("com.sun.security.auth.module.NTSystem");
			Object obj = ntSystem.newInstance();
			Method method = ntSystem.getMethod("getUserSID", noparams);
			system = method.invoke(obj);
		} catch (Exception e) {
			logger.error("com.sun.security.auth.module.NTSystem - AD integration will not be available.");
			logger.error(e.getMessage());
		} 
		
		if ( system != null ) {
			
			userSID = system.toString();
			
		}
		
		return userSID;
	}
}

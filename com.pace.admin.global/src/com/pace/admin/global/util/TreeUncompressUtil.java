/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimMemberProps;
import com.pace.server.client.PafSimpleDimTree;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class TreeUncompressUtil {
	
	static class ChildMember {
		
		PafSimpleDimMember pafSimpleDimMember;
		List<String> children;
		/**
		 * @return the children
		 */
		public List<String> getChildren() {
			return children;
		}
		/**
		 * @param children the children to set
		 */
		public void setChildren(List<String> children) {
			this.children = children;
		}
		/**
		 * @return the pafSimpleDimMember
		 */
		public PafSimpleDimMember getPafSimpleDimMember() {
			return pafSimpleDimMember;
		}
		/**
		 * @param pafSimpleDimMember the pafSimpleDimMember to set
		 */
		public void setPafSimpleDimMember(PafSimpleDimMember pafSimpleDimMember) {
			this.pafSimpleDimMember = pafSimpleDimMember;
		}
		
	}
	
	public static PafSimpleDimTree uncompressTree(PafSimpleDimTree tree) {
		
		if ( tree != null ) {		
			
			tree.setCompressed(false);			
			String[] aliasTableNameOrder = tree.getAliasTableNames().toArray(new String[0]); 
			
            List<String> aliasTableNames;
            Map<Integer, String> memberIndices;
            List<List<String>> parentChildRecords;
            Map<Integer, ChildMember> simpleDimMembers;
            List<PafSimpleDimMember> simpleDimMembersList;
            List<String> traversedMembers;
            PafSimpleDimMember simpleDimMember;
            PafSimpleDimMemberProps simpleMemberProps;
            int i;
            int j;
            String pKey;
            String cKey;
            int pIndex;
            int cIndex;
            String[] aliasValues;
            ChildMember parentMember;
            List<String> children;
            ChildMember childMember;
                           
            aliasTableNames = CollectionsUtil.convertToList(tree.getCompAliasTableNames(), tree.getElementDelim());
            memberIndices = CollectionsUtil.convertToMap(tree.getCompMemberIndex(), tree.getGroupDelim(), tree.getElementDelim());
            parentChildRecords = CollectionsUtil.convertToListOfLists(tree.getCompParentChild(), tree.getGroupDelim(), tree.getElementDelim());
            simpleDimMembers = new HashMap<Integer, ChildMember>();
            simpleDimMembersList = new ArrayList<PafSimpleDimMember>();
            traversedMembers = new ArrayList<String>();

            for (List<String> childRecord : parentChildRecords)
            {
                simpleDimMember = new PafSimpleDimMember();
                simpleMemberProps = new PafSimpleDimMemberProps();
                i = 0;
                j = 0;
                pKey = "";
                cKey = "";
                pIndex = 0;
                cIndex = 0;
                aliasValues = new String[childRecord.size() - 6];

                for (String record : childRecord)
                {
                    switch (i++)
                    {
                        case 0:
                            {
                                if (!record.equals("null"))
                                {
                                    pIndex = Integer.parseInt(record);
                                    
                                    if ( memberIndices.containsKey(pIndex)) {
                                    	pKey = memberIndices.get(pIndex);
                                    }
                                    
                                }
                                simpleDimMember.setParentKey(pKey);
                                break;
                            }
                        case 1:
                            {
                                cIndex = Integer.parseInt(record);
                                
                                if ( memberIndices.containsKey(cIndex)) {
                                	cKey = memberIndices.get(cIndex);
                                }
                                
                                simpleDimMember.setKey(cKey);
                                break;
                            }
                        case 2:
                            {
                                simpleMemberProps.setGenerationNumber(Integer.parseInt(record));
                                break;
                            }
                        case 3:
                            {
                                simpleMemberProps.setLevelNumber(Integer.parseInt(record));
                                break;
                            }
                        case 4:
                        {
                            simpleMemberProps.setTimeBalanceOption(Integer.parseInt(record));
                            break;
                        }
                        case 5:
                        {
                            if(Integer.parseInt(record) == 1)
                            	simpleMemberProps.setTwoPassCalc(true);
                            else
                            	simpleMemberProps.setTwoPassCalc(false);
                            break;
                        }
                        default:
                            {
                                aliasValues[j++] = record;
                                break;
                            }
                    }
                }
           
                if ( aliasTableNames != null ) {
                	simpleMemberProps.getAliasKeys().clear();
                	simpleMemberProps.getAliasKeys().addAll(aliasTableNames);
                }
                
                if ( aliasValues != null && aliasValues.length > 0 ) {
                	simpleMemberProps.getAliasValues().clear();
                	simpleMemberProps.getAliasValues().addAll(Arrays.asList(aliasValues));
                }
                
                simpleDimMember.setPafSimpleDimMemberProps(simpleMemberProps);

                parentMember = new ChildMember();
                
                if ( simpleDimMembers.containsKey(pIndex)) {
                	parentMember = simpleDimMembers.get(pIndex);
                }

                if (parentMember != null)
                {
                    if (parentMember.getChildren() == null)
                    {
                        parentMember.setChildren(new ArrayList<String>());
                    }

                    parentMember.getChildren().add(cKey);
                    
                    if ( simpleDimMembers.containsKey(pIndex)) {
                    
                    	simpleDimMembers.get(pIndex).getPafSimpleDimMember().getChildKeys().clear();
                    	simpleDimMembers.get(pIndex).getPafSimpleDimMember().getChildKeys().addAll(parentMember.getChildren());
                    	
                    }
                }

                childMember = new ChildMember();
                childMember.setPafSimpleDimMember(simpleDimMember);
                simpleDimMembers.put(cIndex, childMember);
                simpleDimMembersList.add(simpleDimMember);
                traversedMembers.add(cKey);
            }

            if ( simpleDimMembersList != null ) {
            	
            	tree.getMemberObjects().clear();
            	tree.getMemberObjects().addAll(simpleDimMembersList);
            	
            }
            
            if ( traversedMembers != null ) {
            	
            	tree.getTraversedMembers().clear();
            	tree.getTraversedMembers().addAll(traversedMembers);
            	
            }
            
            String[] unsortedAliasTableNames = aliasTableNames.toArray(new String[0]);
            
            //get alias table order
            int[] ndxOrder = new int[tree.getAliasTableNames().size()]; 
    		                        
    		for (i = 0; i < ndxOrder.length; i++) {
    			
    			for (j = i; j < ndxOrder.length; j++) {
    				
    				if ( aliasTableNameOrder[i].equals(unsortedAliasTableNames[j])) {
    					ndxOrder[i] = j;
    					break;
    				}
    				
    			}
    			
    		}
    		    		
    		//sort alias table names
    		String[] sortedAliasTableNames = sortStringAr(unsortedAliasTableNames, ndxOrder);
    		
    		if ( sortedAliasTableNames != null) {
	    		
    			//set on tree
    			tree.getAliasTableNames().clear();
	    		tree.getAliasTableNames().addAll(Arrays.asList(sortedAliasTableNames));
	    		
    		}
    		
    		//sort alias members    		    		
    		for (PafSimpleDimMember member : tree.getMemberObjects()) {
    		
    			if ( sortedAliasTableNames != null ) {
    				
    				member.getPafSimpleDimMemberProps().getAliasKeys().clear();
    				member.getPafSimpleDimMemberProps().getAliasKeys().addAll(Arrays.asList(sortedAliasTableNames));
    				
    			}   			
    			
    			String[] aliasValuesAr = sortStringAr(member.getPafSimpleDimMemberProps().getAliasValues(), ndxOrder);
  			
    			if ( aliasValuesAr != null ) {
    			
    				member.getPafSimpleDimMemberProps().getAliasValues().clear();
    				member.getPafSimpleDimMemberProps().getAliasValues().addAll(Arrays.asList(aliasValuesAr));
    				
    			}
    			    			    			
    		}   		
            
        }
		
		return tree;
	}
	
	private static String[] sortStringAr(List<String> list, int[] ndxOrderAr) {
		
		String[] sortedAr = null;
		
		if ( list != null ) {
			
			sortedAr = new String[list.size()];
			
			for (int i = 0; i < ndxOrderAr.length; i++) {
				sortedAr[i] = list.get(ndxOrderAr[i]);
			}
			
		}
		
		return sortedAr;
		
	}
	
	private static String[] sortStringAr(String[] ar, int[] ndxOrderAr) {
		
		String[] sortedAr = null;
		
		if ( ar != null ) {
			
			sortedAr = new String[ar.length];
			
			for (int i = 0; i < ndxOrderAr.length; i++) {
				sortedAr[i] = ar[ndxOrderAr[i]];
			}
			
		}
		
		return sortedAr;
		
	}

}

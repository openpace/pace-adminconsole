/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.Platform;

import com.pace.admin.global.constants.Constants;
import com.pace.base.utility.OSDetector;
import com.pace.base.utility.OSUtil;

public class SystemUtil {

	/**
	 * Gets the current version of the Global plugin.
	 * @return
	 */
	public static String getAcVersion(){
		return Platform.getBundle("com.pace.admin").getHeaders().get("Bundle-Version");
	}
	
	/**
	 * Gets the OS name.
	 * @return
	 */
	public static String getOs(){
		return System.getProperty("os.name");
	}
	
	/**
	 * Gets the OS Version String
	 * @return
	 */
	public static String getOsVersion(){
		return System.getProperty("os.version");
	}
	
	/**
	 * Gets the OS bitness
	 * @return
	 */
	public static String getOsBitness(){
		return System.getProperty("os.arch");
	}
	
	/**
	 * Gets the Java Version
	 * @return
	 */
	public static String getJdkVersion(){
		return System.getProperty("java.version");
	}
	
	/**
	 * Returns the %ProgramData% directory with AC pathing.
	 * @return On non-Windows systems this will return the user's home directory
	 */
  	public static String getAcProgramDataDirectory(){
  		if(OSDetector.isWindows()){
  			return FilenameUtils.concat(OSUtil.getProgramDataDirectory(), Constants.CONF_SUB_DIR_WIN);
  		} else {
  			return FilenameUtils.concat(OSUtil.getUserHomeDirectory(), Constants.CONF_SUB_DIR_OTHER);
  		}
  	}
  	
	/**
	 * Returns the OSGI Configuration directory with AC pathing.
	 * @return On non-Windows systems this will return the user's home directory
	 */
  	public static String getAcConfigurationDirectory(){
  		String basePath = FilenameUtils.concat(OSUtil.getProgramDataDirectory(), Constants.CONF_SUB_DIR_WIN);
  		if(!OSDetector.isWindows()){
  			basePath =  FilenameUtils.concat(OSUtil.getUserHomeDirectory(), Constants.CONF_SUB_DIR_OTHER);
  		}
  		return FilenameUtils.concat(basePath, Constants.CONFIGURATION_SUB_DIR_OTHER);
  	}
	
	/**
	 * Returns the %APPDATA% directory with AC pathing.
	 * @return On non-Windows systems this will return the user's home directory
	 */
  	public static String getAcAppDataDirectory(){
  		if(OSDetector.isWindows()){
  			return FilenameUtils.concat(OSUtil.getUsersAppDataDirectory(), Constants.CONF_SUB_DIR_WIN);
  		} else {
  			return FilenameUtils.concat(OSUtil.getUserHomeDirectory(), Constants.CONF_SUB_DIR_OTHER);
  		}
  	}
	
  	
	/**
	 * Returns the %ProgramData% directory.
	 * @return On non-Windows systems this will return the user's home directory
	 */
  	public static String getProgramDataDirectory1(){
  		if(OSDetector.isWindows()){
  			return OSUtil.getProgramDataDirectory();
  		} else {
  			return OSUtil.getUserHomeDirectory();
  		}
  			
  	}
}
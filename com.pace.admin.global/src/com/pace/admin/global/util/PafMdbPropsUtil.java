/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.util;

import java.util.Set;
import java.util.TreeSet;

import com.pace.server.client.PafMdbProps;

/**
 * Paf MDB Props Utilility.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafMdbPropsUtil {

	/**
	 * 
	 * Gets the base dimensions from the paf mdb props.
	 *
	 * @param pafMdbProps
	 * @return base dimension set
	 */
	public static Set<String> getBaseDimensions(PafMdbProps pafMdbProps) {
		
		Set<String> baseDimensionSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		if ( pafMdbProps != null && pafMdbProps.getBaseDims() != null ) {
			
			baseDimensionSet.addAll(pafMdbProps.getBaseDims());
			
		}	
		
		return baseDimensionSet;		
		
	}
	
	/**
	 * 
	 * Gets the attribute dimensions from the paf mdb props.
	 *
	 * @param pafMdbProps
	 * @return attribute dimension set
	 */
	public static Set<String> getAttributeDimensions(PafMdbProps pafMdbProps) {
		
		Set<String> attDimensionSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		if ( pafMdbProps != null && pafMdbProps.getAttributeDims() != null ) {
			
			attDimensionSet.addAll(pafMdbProps.getAttributeDims());
			
		}	
		
		return attDimensionSet;		
		
	}
	
	/**
	 * 
	 * Gets both base and attribute dimensions from the paf mdb props.
	 *
	 * @param pafMdbProps
	 * @return base and attribute dimension set
	 */
	public static Set<String> getAllDimensions(PafMdbProps pafMdbProps) {
		
		Set<String> allDimensionSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		allDimensionSet.addAll(getBaseDimensions(pafMdbProps));
		allDimensionSet.addAll(getAttributeDimensions(pafMdbProps));		
		
		return allDimensionSet;		
		
	}	
	
	
	
}

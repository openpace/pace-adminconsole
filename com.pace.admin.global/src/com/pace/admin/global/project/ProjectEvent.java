/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.project;

import org.eclipse.core.resources.IProject;


/**
 * Project event used to alert project action (i.e. project refresh)
 * 
 * @author JMilliron
 *
 */
public class ProjectEvent {
	
	final private IProject project;
	
	final private ProjectRequest projectRequest;

	/**
	 * Creates project event.
	 * 
	 * @param project project to perform action on
	 * @param projectRequest request(i.e. refresh project)
	 */
	public ProjectEvent(IProject project, ProjectRequest projectRequest) {
		super();
		this.project = project;
		this.projectRequest = projectRequest;
	}
	
	/**
	 * @return the project
	 */
	public IProject getProject() {
		return project;
	}



	/**
	 * @return the projectRequest
	 */
	public ProjectRequest getProjectRequest() {
		return projectRequest;
	}



	@Override
	public String toString() {
		return "ProjectEvent [project=" + project + ", projectRequest="
				+ projectRequest + "]";
	}
	
}

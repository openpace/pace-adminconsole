/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.project;

import java.util.Observable;

import org.eclipse.core.resources.IProject;

/**
 * Project Monitor. sends project events to Observers.
 * 
 * @author JMilliron
 *
 */
public class ProjectMonitor extends Observable {

	private static ProjectMonitor instance = null;
	
	private ProjectMonitor() {		
	}
	
	/**
	 * Singleton
	 * 
	 * @return only instance of Project Monitor
	 */
	public static ProjectMonitor getInstance() {
		
		if ( instance == null ) {
			instance = new ProjectMonitor();
		}
		
		return instance;
		
	}
	
	/**
	 * Sends project event to notify them to refresh their views with model changes
	 * 
	 * @param project project to refresh
	 */
	public void refreshProject(IProject project) {
		
		ProjectEvent pe = new ProjectEvent(project, ProjectRequest.Refresh);
		
		//enable observers to be notified
		setChanged();
		
		//notify observers of project event
		notifyObservers(pe);		
		
	}

}

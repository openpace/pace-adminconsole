/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.pace.admin.global.util.MemberTagUtil;
import com.pace.base.app.AliasMapping;
import com.pace.base.app.SuppressZeroSettings;
import com.pace.base.db.membertags.MemberTagCommentEntry;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.mdb.FreezePaneTuples;
import com.pace.base.mdb.SortingTuples;
import com.pace.base.view.GroupingSpec;
import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewHeader;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;

/**
 * 
 * Wrapper for Paf View Section
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafViewSectionUI {

	//the view section
	private PafViewSection viewSection = null;

	/**
	 * 
	 * Create a paf view section from an existing view section
	 * 
	 * @param viewSection View Section to wrap
	 */
	public PafViewSectionUI(PafViewSection viewSection) {
		this.viewSection = viewSection;
	}

	/**
	 * 
	 * Gets a List of view headers.
	 *
	 * @return list of view headers
	 */
	public List<PafViewHeader> getHeaderList() {
	
		List<PafViewHeader> headerList = new ArrayList<PafViewHeader>();

		if (viewSection.getPafViewHeaders() != null) {
			
			headerList.addAll(Arrays.asList(viewSection.getPafViewHeaders()));
			
		}

		return headerList;
	}

	/**
	 * 
	 * Adds a header to the list of headers
	 *
	 * @param header view header to add.
	 */
	public void addHeader(PafViewHeader header) {

		List<PafViewHeader> headerList = getHeaderList();

		headerList.add(header);

		setViewSectionHeaders(headerList);

	}

	/**
	 * 
	 * Deletes header.
	 *
	 * @param header view header to delete
	 */
	public void deleteHeader(PafViewHeader header) {

		List<PafViewHeader> headerList = getHeaderList();

		//if header is in list, remove
		if (headerList.contains(header)) {
			headerList.remove(header);
		}

		setViewSectionHeaders(headerList);

	}

	/**
	 * 
	 * Updates existing view header
	 *
	 * @param header
	 */
	public void updateHeader(PafViewHeader header) {

		List<PafViewHeader> headerList = getHeaderList();

		if (headerList.contains(header)) {
			headerList.add(header);
		}

		setViewSectionHeaders(headerList);

	}

	/**
	 * 
	 * Updates the view section with the member tags.  A member tag is only valid on a
	 * row axis when all member tag dimensions are all on the page or column axis 
	 * Vise versa for the column member tags (only valid when all member tag
	 * dimensions are on the row or page axis).  Member Tag Comment Names on the view section
	 * are only valid when all the dimensions exists on the row and column axis AND the 
	 * member tag is not already defined in a row or column tuple.
	 *
	 * @param memberTagDefs
	 */
	public void updateMemberTags(MemberTagDef[] memberTagDefs) {
		
		if ( this.viewSection != null ) {
				
			//if member tag defs are null or 0, clear out all member tag references
			if ( memberTagDefs == null || memberTagDefs.length == 0 ) {
				
				this.viewSection.setRowTuples(MemberTagUtil.removeMemberTagTuples(this.viewSection.getRowTuples(), null));
				this.viewSection.setColTuples(MemberTagUtil.removeMemberTagTuples(this.viewSection.getColTuples(), null));
				//this.viewSection.setMemberTagCommentNames(null);
				this.viewSection.setMemberTagCommentEntries(null);
				
			} else {
			
				//create a map for member tags
				Map<String, MemberTagDef> memberTagDefMap = new TreeMap<String, MemberTagDef>(String.CASE_INSENSITIVE_ORDER);
				
				//populate map of member tags
				for (MemberTagDef memberTagDef : memberTagDefs ) {
					
					memberTagDefMap.put(memberTagDef.getName(), memberTagDef);
					
				}
				
				//get valid row view tuples
				this.viewSection.setRowTuples(getValidViewTuples(memberTagDefMap, this.viewSection.getRowTuples(), new PafAxis(PafAxis.ROW)));

				//get valid column view tuples
				this.viewSection.setColTuples(getValidViewTuples(memberTagDefMap, this.viewSection.getColTuples(), new PafAxis(PafAxis.COL)));
				
				//get valid member Tag Comment states
				//this.viewSection.setMemberTagCommentNames(getValidMemberTagCommentNames(memberTagDefMap));
				this.viewSection.setMemberTagCommentEntries(getValidMemberTagCommentNames(memberTagDefMap));
				
			}		
		
		}		
		
	}	
	
	/**
	 * 
	 * Returns the valid array of member tag comment names.  This validates that all the dimensions
	 * defined in the member tag are on the page, row and column axis's.  If not, the member tag 
	 * name is not returned.
	 *
	 * @param memberTagDefMap
	 * @return
	 */
	private MemberTagCommentEntry[] getValidMemberTagCommentNames(Map<String, MemberTagDef> memberTagDefMap) {
		
		MemberTagCommentEntry[] validMemberTagCommentEntries = null;
		
		//if vs not null and comment names not null
		if ( this.viewSection != null && this.viewSection.getMemberTagCommentEntries() != null ) {
			
			//list to hold the valid member tag comment names
			List<MemberTagCommentEntry> validMemberTagCommentNameList = new ArrayList<MemberTagCommentEntry>();
			
			//get a dimension set
			Set<String> dimensionSet = getDimensionSet();
			
			//outer loop: loop over member tag comment names.  If in key and all dims match, add to
			//valid list of member tag comment names
			MT_COMMENT_LOOP:
			for (MemberTagCommentEntry memberTagCommentEntry : this.viewSection.getMemberTagCommentEntries()) {
				
				if ( memberTagDefMap.containsKey(memberTagCommentEntry.getName())) {
					
					MemberTagDef commentMemberTag = memberTagDefMap.get(memberTagCommentEntry.getName());
					
					for (String commentMemberTagDimName : commentMemberTag.getDims()) {
												
						//verify  comment member tag is still valid for this view section
						if ( ! MemberTagUtil.isMemberTagValidForViewSection(this, commentMemberTag)) {
							
							//continue to next member tag comment name
							continue MT_COMMENT_LOOP;
							
						}
						
					}
					
					//add member tag comment name to valid list
					validMemberTagCommentNameList.add(memberTagCommentEntry);
				}
				
			}
			
			//only if size is greater than 0, convert list to string array
			if ( validMemberTagCommentNameList.size() > 0 ) {
				
				validMemberTagCommentEntries = validMemberTagCommentNameList.toArray(new MemberTagCommentEntry[0]);
				
			}
			
		}
		
		return validMemberTagCommentEntries;
	}

	/**
	 * 
	 * Gets the page, row and column dimension set from the view section.
	 *
	 * @return a unique set of dimension names
	 */
	public Set<String> getDimensionSet() {

		Set<String> dimensionSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		if ( viewSection != null ) {
			
			//if page tuples exists
			if ( this.viewSection.getPageTuples() != null ) {
				
				for (PageTuple pageTuple : this.viewSection.getPageTuples() ) {
					
					dimensionSet.add(pageTuple.getAxis());					
					
				}
				
			}
			
			//if row axis dimensions exists
			if ( this.viewSection.getRowAxisDims() != null ) {
				
				dimensionSet.addAll(Arrays.asList(this.viewSection.getRowAxisDims()));
				
			}
			
			//if col axis dimensions exists
			if ( this.viewSection.getColAxisDims() != null ) {
				
				dimensionSet.addAll(Arrays.asList(this.viewSection.getColAxisDims()));
				
			}
			
		}		
		
		return dimensionSet;
	}

	/**
	 * 
	 * Gets the valid view tuples.  If a view tuple is a member tag view tuple, then the member
	 * tag is validated against the view sections axis dimensions.
	 *
	 * @param memberTagDefMap <member tag name, member tag> member tag map
	 * @param viewTuples view tuples to validate
	 * @param pafAxis PafAxis, axis to validate.  Page always gets validated
	 * @return	valid view tuple arrays
	 */
	private ViewTuple[] getValidViewTuples(Map<String, MemberTagDef> memberTagDefMap, ViewTuple[] viewTuples, PafAxis pafAxis) {

		ViewTuple[] validViewTuples = null;
		
		if ( viewTuples != null ) {
			
			List<ViewTuple> tupleList = new ArrayList<ViewTuple>();
			
			//validate each view tuple
			for (ViewTuple viewTuple : viewTuples ) {
				
				//if member tag, check to see if its valid, if not member tag, add to list
				if ( viewTuple.isMemberTag() ) {
					
					MemberTagDef memberTagDef = memberTagDefMap.get(viewTuple.getMemberDefs()[0]);
					
					if ( memberTagDef != null ) {
						
						//if valid, add to list
						if ( MemberTagUtil.isMemberTagValidForAxis(this, memberTagDef, pafAxis) ) {
							
							tupleList.add(viewTuple);
							
						} 
						
					}						
					
				} else {
					
					//not member tag tuple, add to list
					tupleList.add(viewTuple);
					
				}
				
			}
			
			//if tuple list is greater than 0, convert to array
			if ( tupleList.size() > 0 ) {
				
				validViewTuples = tupleList.toArray(new ViewTuple[0]);
				
			}
			
		}
		
		return validViewTuples;
		
	}

	/**
	 * 
	 * Get Member tag names that exists on a particular axis.  This will
	 * search the row or column axis for the all member tag and return
	 * an array of them.
	 *
	 * @param pafAxis axis to search
	 * @return member tag names from axis
	 */
	public String[] getMemberTagNamesFromViewTuples(PafAxis pafAxis) {
		
		String[] memberTagNames = null;
		
		//valid parm and that view section exist
		if ( this.viewSection != null && pafAxis != null ) {
								
			//create list to hold all tuples
			List<ViewTuple> allTuplesList = new ArrayList<ViewTuple>();
					
			//filter on ROW
			if ( pafAxis.equals(PafAxis.ROW) ) {
				
				if ( viewSection.getRowTuples() != null ) {
				
					allTuplesList.addAll(Arrays.asList(viewSection.getRowTuples()));
					
				}
				
			}

			//filter on COL			
			if ( pafAxis.equals(PafAxis.COL) ) {
				
				if ( viewSection.getColTuples() != null ) {
					
					allTuplesList.addAll(Arrays.asList(viewSection.getColTuples()));
					
				}
				
			}
			
			//if tuples list has anything
			if ( allTuplesList.size() > 0 ) {
				
				List<String> memberTagNameList = new ArrayList<String>();
				
				//loop tuples and get member tags names from them
				for (ViewTuple viewTuple : allTuplesList) {
					
					if ( viewTuple.isMemberTag() ) {
						
						if ( viewTuple.getMemberDefs() != null && viewTuple.getMemberDefs().length > 0 ) {
						
							memberTagNameList.add(viewTuple.getMemberDefs()[0]);
							
						}
												
					}
					
				}
				
				//if list is greater than 0, create names
				if ( memberTagNameList.size() > 0 ) {
					
					memberTagNames = memberTagNameList.toArray(new String[0]);
					
				}
				
			}
									
		}
		
		return memberTagNames;
		
	}
	
	/**
	 * 
	 * Gets all member tag names from all row and column tuples
	 *
	 * @return
	 */
	public String[] getMemberTagNamesFromViewTuples() {
					
		Set<String> memberTagNameSet = new HashSet<String>();
		
		PafAxis pafAxis = new PafAxis(PafAxis.ROW);
		
		String[] memberTagNamesOnRow = getMemberTagNamesFromViewTuples(pafAxis);
		
		if ( memberTagNamesOnRow != null)  {
			
			memberTagNameSet.addAll(Arrays.asList(memberTagNamesOnRow));
			
		}
		
		pafAxis.setValue(PafAxis.COL);
		
		String[] memberTagNamesOnCol = getMemberTagNamesFromViewTuples(pafAxis);

		if ( memberTagNamesOnCol != null)  {
			
			memberTagNameSet.addAll(Arrays.asList(memberTagNamesOnCol));
			
		}
		
		if ( memberTagNameSet.size() > 0 ) {
			
			return memberTagNameSet.toArray(new String[0]);
			
		} else {
			
			return null;
			
		}
				
	}
	
	private void setViewSectionHeaders(List<PafViewHeader> headerList) {

		viewSection.setPafViewHeaders(headerList.toArray(new PafViewHeader[0]));

	}
		
	public PafViewHeader getHeader(int index) {

		List<PafViewHeader> headerList = getHeaderList();

		PafViewHeader header = null;

		if (index <= headerList.size()) {
			header = headerList.get(index);
		}

		return header;
	}
	
	public PafViewHeader[] getHeaders() {
		return viewSection.getPafViewHeaders();
	}
	
	public void setHeaders(PafViewHeader[] headers) {
		viewSection.setPafViewHeaders(headers);
	}

	public String[] getPageAxisDims() {

		List<String> pageDimensions = new ArrayList<String>();

		PageTuple[] pageTuples = viewSection.getPageTuples();

		if (pageTuples != null) {

			for (PageTuple pageTuple : pageTuples) {
				pageDimensions.add(pageTuple.getAxis());
			}
		}
		
		return pageDimensions.toArray(new String[0]);
	}

	public String[] getRowAxisDims() {
		return viewSection.getRowAxisDims();
	}

	public String[] getColAxisDims() {
		return viewSection.getColAxisDims();
	}

	public String convertArToCommaDelimitedStr(String[] strAr) {
		
		StringBuffer strBuff = new StringBuffer();
		
		if ( strAr != null && strAr.length != 0 ) {
			
			for ( String str : strAr ) {
				strBuff.append(str + ", ");
			}
			
			//remove last , and space
			if ( strBuff.length() >= 2 ) {
				strBuff.delete(strBuff.length()-2, strBuff.length()-1);
			}

		}
	
		return strBuff.toString();
	}
	
	public String getDataAlias() {
		return viewSection.getDataAlias();
	}

	public void setDataAlias(String dataAlias) {
		viewSection.setDataAlias(dataAlias);
	}

	public String getDescription() {
		return viewSection.getDescription();
	}

	public void setDescription(String description) {
		viewSection.setDescription(description);
	}

	public String getName() {
		return viewSection.getName();
	}

	public void setName(String name) {
		viewSection.setName(name);
	}

	public int getPrimaryFormattingAxis() {
		return viewSection.getPrimaryFormattingAxis();
	}

	public void setPrimaryFormattingAxis(int primaryFormattingAxis) {
		viewSection.setPrimaryFormattingAxis(primaryFormattingAxis);
	}

	public boolean isReadOnly() {
		return viewSection.isReadOnly();
	}

	public void setReadOnly(boolean readOnly) {
		viewSection.setReadOnly(readOnly);
	}
	
	/**
	 * @return the isRowHeaderRepeated
	 */
	public boolean isRowHeaderRepeated() {
		return viewSection.isRowHeaderRepeated();
	}

	/**
	 * @param isRowHeaderRepeated the isRowHeaderRepeated to set
	 */
	public void setRowHeaderRepeated(boolean isRowHeaderRepeated) {
		viewSection.setRowHeaderRepeated( isRowHeaderRepeated );
	}

	/**
	 * @return the isColHeaderRepeated
	 */
	public boolean isColHeaderRepeated() {
		return viewSection.isColHeaderRepeated();
	}

	/**
	 * @param isColHeaderRepeated the isColHeaderRepeated to set
	 */
	public void setColHeaderRepeated(boolean isColHeaderRepeated) {
		viewSection.setColHeaderRepeated( isColHeaderRepeated );
	}

	public PafViewSection getPafViewSection() {
		return viewSection;
	}
	
	public void setPafViewSection(PafViewSection viewSection) {
		this.viewSection = viewSection;
	}
	
	public void updatePageAxisDims(String[] newDims, PageTuple[] pageTuples){
		if(pageTuples != null && newDims != null){
			PageTuple[] updatedPageTuples = new PageTuple[newDims.length];
			int index = 0;
			for(String dim : newDims){
				updatedPageTuples[index] = new PageTuple();
				updatedPageTuples[index].setAxis(dim);
				for(PageTuple oldTuple : pageTuples){
					if(dim.equalsIgnoreCase(oldTuple.getAxis())){
						updatedPageTuples[index].setMember(oldTuple.getMember());
						break;
					}
				}
				index++;
			}
			
			viewSection.setPageTuples(updatedPageTuples);
		}
		else{
			viewSection.setPageTuples(null);
		}
	}
	
	public void setPageAxisDims(String[] dims) {
		
		if ( dims != null ) {
		
			PageTuple[] pageTuples = new PageTuple[dims.length];
			int index = 0;
			for (String dim : dims ) {
				pageTuples[index] = new PageTuple();
				pageTuples[index++].setAxis(dim);
			}
			
			viewSection.setPageTuples(pageTuples);
		}
		
		
	}
	
	public void setRowAxisDims(String[] dims) {
		viewSection.setRowAxisDims(dims);
	}
	
	public void setColAxisDims(String[] dims) {
		viewSection.setColAxisDims(dims);
	}
	
	public void setGenerationFormatName(String generationFormatName) {
		viewSection.setGenerationFormatName(generationFormatName);
	}
	
	public String getGenerationFormatName() {
		return viewSection.getGenerationFormatName();
	}
	
	public void setHierarchyFormatName(String hierarchyFormatName) {
		viewSection.setHierarchyFormatName(hierarchyFormatName);
	}
	
	public String getHierarchyFormatName() {
		return viewSection.getHierarchyFormatName();
	}
	
	public void setCondtionalFormatName(String conditionalFormatName) {
		viewSection.setConditionalFormatName(conditionalFormatName);
	}
	
	public String getCondtionalFormatName() {
		return viewSection.getConditionalFormatName();
	}
	
	public AliasMapping[] getAliasMappings() {
		return viewSection.getAliasMappings();
	}
	
	public void setAliasMappings(AliasMapping[] aliasMappings) {
		viewSection.setAliasMappings(aliasMappings);
	}
	
	public SuppressZeroSettings getSuppressZeroSettings() {
		return viewSection.getSuppressZeroSettings();
	}
	
	public void setSuppressZeroSettings(SuppressZeroSettings suppressZeroSettings) {
		viewSection.setSuppressZeroSettings(suppressZeroSettings);
	}

	/**
	 * @return the viewSection
	 */
	public PafViewSection getViewSection() {
		return viewSection;
	}
	
	/*
	public String[] getMemberTagCommentNames() {
		return viewSection.getMemberTagCommentNames();
	}
	*/
	
	/**
	 * 
	 * Gets the comment member tag entries
	 *
	 * @return
	 */
	public MemberTagCommentEntry[] getMemberTagCommentEntries() {
		return viewSection.getMemberTagCommentEntries();
	}
	
	/*
	public void setMemberTagCommentNames(String[] memberTagCommentNames) {
		viewSection.setMemberTagCommentNames(memberTagCommentNames);
	}
	*/

	public void setMemberTagCommentEntries(MemberTagCommentEntry[] memberTagCommentEntries) {
		viewSection.setMemberTagCommentEntries(memberTagCommentEntries);
	}
	
	public ViewTuple[] getColTuples() {
		return viewSection.getColTuples();
	}

	public void setColTuples(ViewTuple[] colTuples) {
		viewSection.setColTuples(colTuples);
	}

	public ViewTuple[] getRowTuples() {
		return viewSection.getRowTuples();
	}

	public void setRowTuples(ViewTuple[] rowTuples) {
		viewSection.setRowTuples(rowTuples);
	}
	
	public PageTuple[] getPageTuples() {
		
		return viewSection.getPageTuples();
		
	}
	
	public void setPageTuples(PageTuple[] pageTuples ) {
		
		viewSection.setPageTuples(pageTuples);
		
	}
	
	public SortingTuples getSortingTuples() {
		
		return viewSection.getSortingTuples();
		
	}
	
	public void setSortingTuples(SortingTuples sortingTuples ) {
		
		viewSection.setSortingTuples(sortingTuples);
		
	}
	
	public GroupingSpec[] getGroupingSpecs() {
		return viewSection.getGroupingSpecs();
	}

	public void setGroupingSpecs(GroupingSpec[] groupingSpecs) {
		viewSection.setGroupingSpecs(groupingSpecs);
	}
	

}

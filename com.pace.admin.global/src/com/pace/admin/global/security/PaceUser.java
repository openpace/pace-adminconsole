/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.security;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PaceUser implements Comparable<PaceUser> {

	private String userName;
	
	private String displayName;

	/**
	 * 
	 * @param userName
	 */
	public PaceUser (String userName) {
		
		this.userName = userName;
		
	}
	
	/**
	 * 
	 * @param userName
	 * @param displayName
	 */
	public PaceUser (String userName, String displayName ) {
		
		this(userName);
		
		this.displayName = displayName;
		
	}
	
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		
		if ( displayName == null || displayName.equals("")  ) {
			return userName;
		}
		
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj instanceof PaceUser ) {
			
			PaceUser paceUser = (PaceUser) obj;
			
			if ( this.userName != null && paceUser.getUserName() != null ) {
				
				return this.userName.equalsIgnoreCase(paceUser.getUserName());
			}
						
			
		}
		
		return super.equals(obj);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if ( this.userName != null ) {
			
			return this.userName.hashCode();
			
		}
		
		return super.hashCode();
	}

	public int compareTo(PaceUser o) {

		if ( this.userName != null && o.getUserName() != null ) {
			
			return this.userName.compareToIgnoreCase(o.getUserName());
			
		}
		
		return 0;
	}
}

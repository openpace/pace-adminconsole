/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

public abstract class AbstractAction extends Action implements ISelectionListener, IWorkbenchAction {

	protected static final Logger logger = Logger.getLogger(AbstractAction.class);
	protected final IWorkbenchWindow window;
	protected IStructuredSelection selection = null;
	
	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractAction(String name, String actionId, IWorkbenchWindow window) {
		super(name);
		this.setId(actionId);
		this.window = window;
		this.window.getSelectionService().addSelectionListener(this);

	}
	
	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractAction(String name, String actionId, IWorkbenchWindow window, int style) {
		super(name, style);
		this.setId(actionId);
		this.window = window;
		this.window.getSelectionService().addSelectionListener(this);

	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// Selection containing elements
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public abstract void run();
	
}

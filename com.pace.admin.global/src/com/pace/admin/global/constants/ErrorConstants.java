/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.constants;

/**
 * Error Constants used in error messages
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ErrorConstants {

	public static final String INVALID_DIMENSION = "%s is an invalid dimension.  Could not find the dimension tree.";
	public static final String THE_FOLLOWING_DIMENSIONS_ARE_NO_LONGER_VALID = "The following dimensions are no longer valid:\n";
	public static final String MISSING_DIMENSIONS = "Missing Dimensions";
	public static final String INVALID_SORTING_COMBINATION = "Invalid Sorting Combination";
	
}

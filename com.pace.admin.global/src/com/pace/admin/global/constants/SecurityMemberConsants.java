/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.global.constants;

/**
 * @author jmilliron
 *
 */
public class SecurityMemberConsants {

	//don't allow an object to be created
	private SecurityMemberConsants() {}
	
	
	public final static String IDESC = "IDESC";

	public final static String DESC = "DESC";
	
	public final static String ICHILD = "ICHILD";
	
	public final static String CHILDREN  = "CHILDREN";
	
	public final static String LEVEL = "LEVEL";
	
	public final static String GENERATION  = "GENERATION";
	
	public final static String LEVEL_IDNT = "L";
	
	public final static String GENERATION_IDNT = "G";
	
}


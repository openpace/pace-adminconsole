/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.constants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Platform;

import com.pace.admin.global.util.PafServerUtil;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Platform;

import com.pace.admin.global.util.StringUtil;
import com.pace.admin.global.util.SystemUtil;
import com.pace.base.PafBaseConstants;
/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 */
/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 */
/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 */
public class Constants {

	/**
	 * Automatically generated method: toString
	 */
	public String toString() {
		return super.toString();
	}

	private static Logger logger = Logger.getLogger(Constants.class);
	
	private final static String pathSep = File.separator;

	public final static String MENU_VIEW_ID = "com.pace.admin.menu.views.menuview";

	public final static String CONF_DIR = PafBaseConstants.DN_ConfFldr + pathSep;
	
	public final static String CONF_SERVER_DIR = PafBaseConstants.DN_ConfServerFldr + pathSep;

	public final static String CACHED_DIM_TREE_DIR = "cachedDimTrees" + pathSep;

	public final static String TEMPLATES_DIR = "templates" + pathSep;
	
	//public final static String ADMIN_ENV_VAR = "PafAdminHome";
	
	public static final String CONF_SUB_DIR_WIN = "Open Pace" + File.separator + "Pace Administration";
	
	public static final String CONF_SUB_DIR_OTHER = ".open-pace" + File.separator + "pace-administration";
	
	public static final String CONFIGURATION_SUB_DIR_OTHER = "configuration";

	//private static String AdminConsoleHomeEnv = System.getenv(ADMIN_ENV_VAR);

	private static String AdminConsoleWorkspaceDir = Platform.getInstanceLocation().getURL().getFile();
	
	private static String AdminConsoleInstallDir = Platform.getInstallLocation().getURL().getFile();
		
	private static String OsgiConfDir = Platform.getConfigurationLocation().getURL().getFile();
	
	private final static String ADMIN_CONSOLE_LOCAL_APPDATA_HOME;
	
	private final static String ADMIN_CONSOLE_GLOBAL_APPDATA_HOME;
	
	static {
		
		ADMIN_CONSOLE_LOCAL_APPDATA_HOME = SystemUtil.getAcAppDataDirectory() + pathSep;
		ADMIN_CONSOLE_GLOBAL_APPDATA_HOME = SystemUtil.getAcProgramDataDirectory() + pathSep;
				 
		logger.info("AC Workspace directory: " + AdminConsoleWorkspaceDir);
		logger.info("AC Install directory: " + AdminConsoleInstallDir);
		logger.info("AC Configuration directory (OSGI): " + OsgiConfDir);
		logger.info("AC Local AppData directory: " + ADMIN_CONSOLE_LOCAL_APPDATA_HOME);
		logger.info("AC Global AppData directory: " + ADMIN_CONSOLE_GLOBAL_APPDATA_HOME);
		
	}
	
	//public final static String ADMIN_CONSOLE_HOME = AdminConsoleHomeEnv;
	
	public final static String ADMIN_CONSOLE_HOME = AdminConsoleInstallDir;
	
	// KRM - 07/16/2014 - Dead Code
	//public final static String PROJECTS_DIRECTORY = ADMIN_CONSOLE_HOME + "Projects" + pathSep;

	public final static String ADMIN_CONSOLE_CONF_DIRECTORY = ADMIN_CONSOLE_HOME + CONF_DIR;
	
	public final static String ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY = ADMIN_CONSOLE_LOCAL_APPDATA_HOME + CONF_DIR;
	
	public final static String ADMIN_CONSOLE_GLOBAL_APPDATA_CONF_DIRECTORY = ADMIN_CONSOLE_GLOBAL_APPDATA_HOME + CONF_DIR;

	//public final static String GLOBAL_FONTS_FILE = "fonts.xml";

	public final static String MEASURE_VERSION_DEFAULT_NUMERIC_PROPERTY_DIALOG_HEADING = "Measure/Version Default Numeric Formatting";

	public final static String SERVERS_FILE = "paf_servers.xml";

	//public final static String EXCEL_COLORS = "paf_excel_colors.xml";
	
	//TTN 900 - Added by Iris
	public final static String PRINT_STYLES_FILE = PafBaseConstants.FN_PrintStyles;
	public final static String DEFAULT_PRINT_SETTINGS_FILE = "paf_default_printsettings.xml";
	
	public final static String PAF_PROJECT_FILE = PafBaseConstants.FN_Project;

	public final static String BACKUP = "backup";

	public final static String CONSOLE_NAME = "Pace Admin Console";

//	public final static String SAMPLE_PROJECT_DIR = ADMIN_CONSOLE_CONF_DIRECTORY
//			+ "Sample Project" + pathSep;

//	public final static String SERVER_CONFIG_MDB_DS_FILE = "mdbDs.xml";
//	
//	public final static String SERVER_CONFIG_RDB_DS_FILE = "rdbDs.xml";
	
//	public final static String SERVER_CONFIG_SERVER_SETTINGS_FILE = "serverSettings.xml";
	
	public final static String SERVER_LOG = "server" + pathSep + "server"
			+ pathSep + "default" + pathSep + "log" + pathSep + "server.log";

	public final static String LIB_DIR = "lib";

	// used in pafxstream to prefix dtd path to dtd file in relative locationi
	// to xml file.
	public static final String CONF_DTD_PREFIX = ".." + File.separator + ".." + File.separator + ".." + File.separator + "conf" + File.separator + "dtd" + File.separator;
	
	public static final String PROJECT_DTD_PREFIX = ".." + File.separator + ".." + File.separator + "conf" + File.separator + "dtd" + File.separator;

	// wizard names
	public final static String HIERARCHY_FORMATTING_WIZARD_NAME = "Hierarchy Formatting Wizard";
	
//	public final static String PRINT_PREFERENCES_WIZARD_NAME = "Print Preferences Wizard";

	public final static String USER_SELECTION_WIZARD_NAME = "User Selection Wizard";
	
	public final static String DYNAMIC_MEMBERS_WIZARD_NAME = "Dynamic Members Wizard";
	
	public final static String MEMBER_TAGS_WIZARD_NAME = "Member Tags Wizard";

	public final static String GLOBAL_SYTLES_WIZARD_NAME = "Global Styles Wizard";

	public final static String NUMERIC_FORMATTING_WIZARD_NAME = "Numeric Formatting Wizard";

//	public final static String PROJECT_SETTINGS_WIZARD_NAME = "Project Settings Wizard";
	// menu names
	public final static String MENU_VIEWS = "Views";

	public final static String MENU_VIEW_SECTIONS = "View Sections";

	public final static String MENU_ROLES_PROCESS = "Roles & Processes";
	public final static String MENU_STYLES_FORMATS_PROCESS = "Styles & Formats";

	public final static String MENU_ROLES_PROCESS_PLANCYCLES = "Plan Cycles";

	public final static String MENU_ROLES = "Roles";
	
	//TTN-2222 Heatmap Formatting
	public final static String MENU_HIERARCHY_FORMATS = "Hierarchy Formats";
	public final static String MENU_NUMERIC_FORMATS = "Numeric Formats";
	public final static String MENU_STYLES = "Styles";
	public final static String MENU_FORMATS = "Formats";
	public final static String MENU_CONDITIONAL = "Conditional";
	public final static String MENU_NUMERIC = "Numeric";
	public final static String MENU_GLOBAL = "Global";
	public final static String MENU_HIERARCHY = "Hierarchy";
	public final static String CONDITIONAL_FORMATS = "Conditional Formats";
	public final static String CONDITIONAL_STYLES = "Conditional Styles";
	
	public final static String MENU_SEASONS = "Seasons / Processes";

	public final static String MENU_ROLE_CONFIGURATIONS = "Role Configurations";

	public final static String MENU_ROLES_PROCESS_PLANCYCLE = "Plan Cycle";

	public final static String MENU_ROLE = "Role";

	public final static String MENU_SEASON = "Season / Process";

	public final static String MENU_ROLE_CONFIGURATION = "Role Configuration";
	
	public final static String MENU_USER_SECURITY = "User Security";
	
	public final static String MENU_VIEW_GROUPS = "View Groups";
	
	public final static String MENU_VIEW_GROUP = "View Group";
	
	public final static String MENU_ADMIN_LOCK = "Admin Lock";
	
	//TTN 900 - Added for Print Preferences
	public final static String PRINT_STYLES_NAME = "Print Styles";
	public final static String PRINT_STYLE_NAME = "Print Style";
	public final static String EXPAND_PROJECT_TREE_FILE = "paf_expand_tree.xml";

	// file extension
	public final static String XML_EXT = "xml";

	public final static String JAR_EXT = "jar";
	
	public final static String CSV_EXT = "csv";
	
	public final static String PAF_EXT = "paf";
	
	public final static String SECURITY_SESSION_FILE = "paf_sessions.xml";

//	public final static int DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS = 200;
	
//	public final static long DEFAULT_WEBSERVICE_CONNECTION_TIMEOUT_IN_MILLISECONDS = 60000;
	
//	public final static long DEFAULT_WEBSERVICE_RECEIEVE_TIMEOUT_IN_MILLISECONDS = 180000;

	public final static String DEFAULT_SERVER_STARTUP_FILE = "PafServer.bat";
	
	public final static String DEFAULT_SERVER_SHUTDOWN_FILE = "shutdown.bat";
	
	//Changed in 2.8.2.x, TTN-1581
	public final static Integer DEFAULT_SERVER_JNDI_PORT = 10900;
	
	public final static Integer DEFAULT_SERVER_JMS_MESSAGING_PORT = 5445;
	
	public final static String META_DATA_FOLDER_NAME = ".metadata";
	
	public final static String GLOBAL_XML_FOLDER_NAME = "global_xml";
	
	public final static String DIALOG_ERROR_HEADING = "Error";
	
	public final static String DIALOG_INFO_HEADING = "Information";
	
	public final static String DIALOG_WARNING_HEADING = "Warning";
	
	public final static String DIALOG_QUESTION_HEADING = "Confirm";

	//jar files
	//public final static String JAR_CUSTOM_FUNCTIONS_FILE_NAME = "pafcf.jar";

	public final static String CELL_NOTE_FILE_NAME = "cellnote";
	
	public final static String MEMBER_TAG_DATA_FILE_NAME = "member_tag_data";
	
	public final static String FILE_EXT_BINARY = ".ser";
	
	public final static String WSDL_EXT = "?wsdl";
	
	public final static String DYNAMIC_MEMBERS_NODE_NAME = "Dynamic Members";
	
	public final static String USER_SELECTIONS_NODE_NAME = "User Selections";
	
	public final static String MEMBER_TAGS_NODE_NAME = "Member Tags";
	
	public static final String DROPDOWN_GLOBAL_OPTION = "Default";
	
	public static final String DROPDOWN_BOOL_TRUE = StringUtil.initCap(Boolean.TRUE.toString());
	
	public static final String DROPDOWN_BOOL_FALSE = StringUtil.initCap(Boolean.FALSE.toString());
	
	public static final String DEFAULT_IMPORT_EXPORT_DIRECTORY = "C:\\";
	
	public static final String WIDGET_SELECT_ALL_DESELECT_ALL = "Select / Deselect All";

	public static final String SERVER_SESSION = "Session";

	public static final String USER_NAME_SYSTEM_PROPERTY = "user.name";
	
	public static final String SYMBOL_ERROR_ICON_PATH = "icons" + File.separator + "error.gif";
	
	public static final int APPLY_ID = -526; 
	
	public static final String APPLY_LABEL = "Apply";
	
	public static final String COPY_TO = "Copy of ";
	public static final String COPY = "Copy";
	
	//xlsx, xlsm
	public static final List<String> EXCEL_FILE_EXTENSION_LIST = Arrays.asList(PafBaseConstants.XLSX_EXT.substring(1), PafBaseConstants.XLSM_EXT.substring(1));
	
	//TTN 900 - Print Preferences - added by Iris
	public static final String DEFAULT_MARKER = " [default]";
	
	public final static int SLEEP_TIME = 10000;
	
//	public final static int SERVER_STARTUP_TIMEOUT_DEFAULT = 300000;
	
	public final static String SERVICE_START_BAT = "startService.bat";
	
	public final static String SERVICE_STOP_BAT = "stopService.bat";

	public final static String SERVICE_START_LINUX_BAT = "startService-linux.bat";
	
	public final static String SERVICE_STOP_LINUX_BAT = "stopService-linux.bat";
	
	public final static String REFRESHING_SERVERS = "Refreshing servers";
	
	public final static int LEVEL_SERVER_CHILDREN_NODE = 3;
	
	public final static String DEFAULT_SERVER_SERVICE_NAME = "OPEN_PACE_APP_SVR";

	public static final String SERVER_LOG_TOPIC = "MyErrorsTopic";
	
	public static final SimpleDateFormat CONSOLE_SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-SSS");
		
	public static final String RELOAD_SERVERS_APPLICATION_CONFIGURATION = "Reload Server's Application Configuration";
	
	public static final Boolean RELOAD_SERVERS_APPLICATION_CONFIGURATION_DEFAULT_VALUE = Boolean.TRUE;
	
	public static final String START_OR_RESTART_APPLICATION = "Start/Restart Application";
	
	public static final Boolean START_OR_RESTART_APPLICATION_DEFAULT_VALUE = Boolean.FALSE;

	public static final String MEMBER_LISTS = "Member Lists";
	
	public static final String PACE_SUPPORT_EMAIL = "pace.support@alvarezandmarsal.com";

	public static final String ENABLE_DEVELOPMENT_FEATURES_FLAG = "development.enable";

	public static final String NO_GROUP_NODE_NAME = "[No Group]";
	
	public final static String IMPORT_RULESET_WIZARD_NAME = "Import and Deploy Ruleset Wizard";
	
	public final static String DISABLED_SERVER_NODE_TEXT = " (Disabled)";
	
	public final static String EDITOR_CONFLICT_OPEN_MESSAGE = "Can't open editor while dependent editor(s) is open";
	
	public final static String EDITOR_CONFLICT_DELETE_MESSAGE = "Can't delete object while dependent editor(s) is open";
	
	public final static String EDITOR_CONFLICT_RENAME_MESSAGE = "Can't rename object while dependent editor(s) is open";
	
	// TTN-2348
	public static final String LDAPAdminUserStartToken = "[";
	public static final String LDAPAdminUserEndToken = "]";
	public static final String LDAPAdminUserSeperatorToken = "@";
	
}


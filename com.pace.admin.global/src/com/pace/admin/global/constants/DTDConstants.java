/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.constants;

public class DTDConstants {

	
	/**
	 * Automatically generated method: toString
	 */
	public String toString () {
		return super.toString();
	}

	public final static String PAF_VIEWS_DTD = "";
	public final static String PAF_VIEW_SECTIONS_DTD = "";
	public final static String PAF_NUMERIC_FORMATS_DTD = "";
	public final static String PAF_GLOBAL_STYLES_DTD = "";
	public final static String PAF_GENERATION_FORMATTING_DTD = "";
	public final static String PAF_USER_SELECTIONS_DTD = "";
	
}

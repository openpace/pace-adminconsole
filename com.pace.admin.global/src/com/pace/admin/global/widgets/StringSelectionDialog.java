/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.widgets;

import java.util.Map;
import java.util.TreeMap;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class StringSelectionDialog extends TitleAreaDialog {

	private Table table;

	private String dialogTitle;

	private String areaTitle;

	private String areaMessage;

	private String[] items;

	private String[] selectedItems;

	private CheckboxTableViewer checkboxTableViewer;
	
	private Map<String, Boolean> optionalCheckOptionsMap = new TreeMap<String, Boolean>();
	
	class TableLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object element, int columnIndex) {
			return (String) element;
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
	}

	class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (String[]) inputElement;
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 * @wbp.parser.constructor
	 */
	public StringSelectionDialog(Shell parentShell, String dialogTitle,
			String areaTitle, String areaMessage, String[] items,
			String[] selectedItems) {
		super(parentShell);
		this.dialogTitle = dialogTitle;
		this.areaTitle = areaTitle;
		this.areaMessage = areaMessage;
		this.items = items;
		this.selectedItems = selectedItems;
	}

	
	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 */
	public StringSelectionDialog(Shell parentShell, String dialogTitle,
			String areaTitle, String areaMessage, String[] items,
			String[] selectedItems, Map<String, Boolean> optionalCheckOptionsMap) {
		
		this(parentShell, dialogTitle, areaTitle, areaMessage, items, selectedItems);
		
		if ( optionalCheckOptionsMap != null ) {
			this.optionalCheckOptionsMap.putAll(optionalCheckOptionsMap);
		}
		
	}

	
	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		GridData gd_container = new GridData(GridData.FILL_BOTH);
		gd_container.horizontalAlignment = SWT.CENTER;
		container.setLayoutData(gd_container);
		
		Label label = new Label(container, SWT.NONE);
		
		Label label_1 = new Label(container, SWT.NONE);
		
		Composite composite_1 = new Composite(container, SWT.NONE);
		GridData gd_composite_1 = new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
		gd_composite_1.heightHint = 268;
		composite_1.setLayoutData(gd_composite_1);
		composite_1.setBounds(0, 0, 64, 64);
				composite_1.setLayout(new GridLayout(1, false));
		
				checkboxTableViewer = CheckboxTableViewer.newCheckList(composite_1,
						SWT.BORDER);
				checkboxTableViewer.setLabelProvider(new TableLabelProvider());
				checkboxTableViewer.setContentProvider(new ContentProvider());
				table = checkboxTableViewer.getTable();
				GridData gd_table = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
				gd_table.minimumHeight = 150;
				table.setLayoutData(gd_table);
				table.setLinesVisible(true);
				
						final TableColumn newColumnTableColumn = new TableColumn(table,
								SWT.NONE);
						newColumnTableColumn.setWidth(277);
						
						Composite composite = new Composite(composite_1, SWT.NONE);
						composite.setLayout(new RowLayout(SWT.HORIZONTAL));
						
								final Button selectAllButton = new Button(composite, SWT.NONE);
								selectAllButton.addSelectionListener(new SelectionAdapter() {
									public void widgetSelected(SelectionEvent e) {
										checkboxTableViewer.setAllChecked(true);
									}
								});
								selectAllButton.setText("Select All");
								
										final Button deselectAllButton = new Button(composite, SWT.NONE);
										deselectAllButton.addSelectionListener(new SelectionAdapter() {
											public void widgetSelected(SelectionEvent e) {
												checkboxTableViewer.setAllChecked(false);
											}
										});
										deselectAllButton.setText("Deselect All");
										
																				
										Composite composite_2 = new Composite(composite_1, SWT.NONE);
										composite_2.setBounds(0, 0, 64, 64);
										
										if ( optionalCheckOptionsMap != null && optionalCheckOptionsMap.size() > 0) {
											
											for (String checkboxText : optionalCheckOptionsMap.keySet()) {
												//String checkboxText = Constants.UPLOAD_APPLY_CONFIGURATION_UPDATES;
												composite_2.setLayout(new GridLayout(1, false));
												final Button btnCheckButton = new Button(composite_2, SWT.CHECK);
												btnCheckButton.addSelectionListener(new SelectionAdapter() {
													@Override
													public void widgetSelected(SelectionEvent e) {
														optionalCheckOptionsMap.put(btnCheckButton.getText(), btnCheckButton.getSelection());
													}
												});
												btnCheckButton.setText(checkboxText);
												
												if ( optionalCheckOptionsMap.get(checkboxText) != null ) {
													btnCheckButton.setSelection(optionalCheckOptionsMap.get(checkboxText));
												}
												
											}
										
										}
						checkboxTableViewer.setInput(items);
		// checkboxTableViewer.setAllChecked(true);

		if (selectedItems != null) {

			checkboxTableViewer.setCheckedElements(selectedItems);

			//added true to make the table viewer scroll down to first selected item.
			if (selectedItems.length > 0) {
				checkboxTableViewer.setSelection(new StructuredSelection(
						selectedItems[0]), true);
			}

			selectedItems = null;
		}

		setTitle(areaTitle);
		setMessage(areaMessage);

		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(482, 517);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(dialogTitle);
	}

	protected void buttonPressed(int buttonId) {

		if (buttonId == IDialogConstants.OK_ID) {

			Object[] selectedObjects = checkboxTableViewer.getCheckedElements();

			if (selectedObjects != null && selectedObjects.length > 0) {

				selectedItems = new String[selectedObjects.length];

				for (int i = 0; i < selectedObjects.length; i++) {

					selectedItems[i] = (String) selectedObjects[i];
				}

			}

			super.buttonPressed(buttonId);

		}
		super.buttonPressed(buttonId);
	}

	/**
	 * @return Returns the selectedItems.
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}

	/**
	 * @return the optionalCheckOptionsMap
	 */
	public Map<String, Boolean> getOptionalCheckOptionsMap() {
		return optionalCheckOptionsMap;
	}
	
	
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.widgets;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.util.ArraysUtil;
import com.pace.admin.global.util.ControlUtil;

public class StringListSelectionDialog extends TitleAreaDialog {

	class StringArContentProvider implements IStructuredContentProvider {
		
		public Object[] getElements(Object inputElement) {
			
			if ( inputElement instanceof String[] ) {
				return (String[]) inputElement;
			}
			
			return null;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	class StringArListLabelProvider extends LabelProvider {
		public String getText(Object element) {
			
			if ( element instanceof String ) {
				
				return (String) element;
				
			}
			
			return null;
		}
		public Image getImage(Object element) {
			return null;
		}
	}
	
	private List list_1;
	
	private List list_2;
	
	private ListViewer toViewer;
	
	private ListViewer fromViewer;
	
	private String dialogTitle;

	private String areaTitle;

	private String areaMessage;

	private String fromListLabel;
	
	private String toListLabel;
	
	private String[] items;

	private String[] selectedItems;
	
	private boolean isOrderingEnabled;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public StringListSelectionDialog(Shell parentShell, String dialogTitle,
	String areaTitle, String areaMessage, String fromListLabel, String toListLabel, String[] items,
	String[] selectedItems) {
		
		this(parentShell, dialogTitle, areaTitle, areaMessage, fromListLabel, toListLabel, items,
				selectedItems, true);
		
	}
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public StringListSelectionDialog(Shell parentShell, String dialogTitle,
	String areaTitle, String areaMessage, String fromListLabel, String toListLabel, String[] items,
	String[] selectedItems, boolean isOrderingEnabled) {
		
		super(parentShell);
		this.dialogTitle = dialogTitle;
		this.areaTitle = areaTitle;
		this.areaMessage = areaMessage;
		this.fromListLabel = fromListLabel;
		this.toListLabel = toListLabel;	
		
		this.items = ArraysUtil.remove(items, selectedItems);		
		this.selectedItems = selectedItems;
		this.isOrderingEnabled = isOrderingEnabled;
	}
	

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginRight = 5;
		gridLayout.marginLeft = 30;
		gridLayout.marginHeight = 30;
		gridLayout.numColumns = 4;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(new GridLayout());

		final Label fromLabel = new Label(composite, SWT.NONE);
		
		if ( fromListLabel != null)  {
		
			fromLabel.setText(fromListLabel);
			
		}

		fromViewer = new ListViewer(composite, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		fromViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
							
				if ( fromViewer.getSelection() instanceof IStructuredSelection) {
					
					IStructuredSelection selection = (IStructuredSelection) fromViewer.getSelection();
					
					Object[] selectedElements = selection.toArray();
					
					toViewer.add(selectedElements);
					
					toViewer.setSelection(new StructuredSelection(selectedElements));
					
					fromViewer.remove(selectedElements);					
					
				}
				
			}
		});
		fromViewer.setContentProvider(new StringArContentProvider());
		fromViewer.setLabelProvider(new StringArListLabelProvider());
		list_1 = fromViewer.getList();
		list_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				toViewer.setSelection(null);
				
			}
		});
		final GridData gridData_1_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_1_1.widthHint = 120;
		list_1.setLayoutData(gridData_1_1);
		fromViewer.setInput(this.items);
		
		fromViewer.setSorter(new ViewerSorter() {
			public int compare(Viewer viewer, Object obj1, Object obj2) {
				return ((String)obj1).compareTo((String) obj2);
			}
		});
		
		//remove already selected values
		//fromViewer.remove(this.selectedItems);
		
		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
		composite_1.setLayout(new GridLayout());

		final Composite composite_3 = new Composite(composite_1, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.CENTER, SWT.CENTER, false, true);
		gridData_2.widthHint = 36;
		composite_3.setLayoutData(gridData_2);
		composite_3.setLayout(new GridLayout());

		final Button button = new Button(composite_3, SWT.ARROW | SWT.RIGHT);
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				IStructuredSelection selection = (IStructuredSelection) fromViewer.getSelection();
				
				if ( selection.size() > 0 ) {				
					
					Object[] selectedElements = selection.toArray();
					
					toViewer.add(selectedElements);
					
					toViewer.setSelection(new StructuredSelection(selectedElements));
					
					fromViewer.remove(selectedElements);			
					
				}
			}
		});
		button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		button.setText("button");

		final Button button_1 = new Button(composite_3, SWT.ARROW | SWT.LEFT);
		button_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				IStructuredSelection selection = (IStructuredSelection) toViewer.getSelection();
				
				if ( selection.size() > 0 ) {				
					
					Object[] selectedElements = selection.toArray();

					fromViewer.add(selectedElements);
									
					fromViewer.setSelection(new StructuredSelection(selectedElements));
					
					toViewer.remove(selectedElements);			
					
				}
				
			}
		});
		button_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		button_1.setText("button");

		final Composite composite_2 = new Composite(container, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite_2.setLayout(new GridLayout());

		final Label toLabel = new Label(composite_2, SWT.NONE);
		toLabel.setLayoutData(new GridData());
		
		if ( toListLabel != null ) {

			toLabel.setText(toListLabel);
			
		}


		toViewer = new ListViewer(composite_2, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		toViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				
				if ( toViewer.getSelection() instanceof IStructuredSelection) {
					
					IStructuredSelection selection = (IStructuredSelection) toViewer.getSelection();
					
					Object[] selectedElements = selection.toArray();
					
					fromViewer.add(selectedElements);
					
					fromViewer.setSelection(new StructuredSelection(selectedElements));
					
					toViewer.remove(selectedElements);					
					
				}

			}
		});
		toViewer.setContentProvider(new StringArContentProvider());
		toViewer.setLabelProvider(new StringArListLabelProvider());
				
		list_2 = toViewer.getList();
		list_2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				fromViewer.setSelection(null);
				
			}
		});
		final GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_1.widthHint = 120;
		list_2.setLayoutData(gridData_1);
		toViewer.setInput(this.selectedItems);

		
			
		final Composite composite_1_1 = new Composite(container, SWT.NONE);
		
		if ( isOrderingEnabled ) {

			composite_1_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			composite_1_1.setLayout(new GridLayout());
			
			final Composite composite_3_1 = new Composite(composite_1_1, SWT.NONE);
			composite_3_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true));
			composite_3_1.setLayout(new GridLayout());
	
			final Button button_2 = new Button(composite_3_1, SWT.ARROW);
			button_2.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent arg0) {
					
															
		
					ControlUtil.directionalButtonPressed(toViewer.getList(), ControlUtil.UP_BUTTON_ID);
					
					
				}
			});
			button_2.setText("button");
	
			final Button button_1_1 = new Button(composite_3_1, SWT.ARROW | SWT.DOWN);
			button_1_1.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent arg0) {
					
					ControlUtil.directionalButtonPressed(toViewer.getList(), ControlUtil.DOWN_BUTTON_ID);
					
				}
			});
			button_1_1.setText("button");
		
		} else {
			
			composite_1_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			GridLayout gl1 = new GridLayout();
			
			gl1.marginWidth = 8;
			
			composite_1_1.setLayout(gl1);
			
			
		}

		setTitle(this.areaTitle);
		setMessage(this.areaMessage);
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(514, 375);
	}

	/**
	 * @return Returns the selectedItems.
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		if ( this.dialogTitle != null) {
		
			newShell.setText(dialogTitle);
			
		}
	}
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
			
			if ( toViewer.getList().getItems().length == 0 ) {
				
				this.selectedItems = null;
				
			} else {
			
				this.selectedItems = toViewer.getList().getItems();
				
			}			
									
		}
		
		super.buttonPressed(buttonId);
	}
	

}

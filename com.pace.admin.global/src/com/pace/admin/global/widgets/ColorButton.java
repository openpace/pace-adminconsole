/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;


/**
 * 
 * Color Button. Normal button with a rectangle of color in center
 * 
 * @author jmilliron
 * 
 */
public class ColorButton {

	// Button color
	private Color color = null;
	// Button
	private Button button = null;

	private int mouse = 0;
	private boolean hit = false;

	/**
	 * Creates an instance of a color button.
	 * 
	 * @param button
	 *            Button to be used
	 * @param color
	 *            Color to be used
	 */
	public ColorButton(Button button, Color color) {

		this.button = button;

		this.color = color;

		// listen for when button gets pained
		this.button.addPaintListener(new PaintListener() {

			public void paintControl(PaintEvent event) {

//				if( mouse == 0 ) {
					Color color = getColor();

					if (color == null) {

						getButton().setBackground(null);
						getButton().setText("Choose Color...");

					} else {

						getButton().setText("");

						GC gc = event.gc;

						// set background
						gc.setBackground(color);

						// get button bounds as a rectangle
						Rectangle rect = getButton().getBounds();

						// fill color rectangle
						gc.fillRectangle(8, 8, rect.width - 16,
								rect.height - 16);

						// set new backgroud color
						gc.setBackground(getButton().getDisplay()
								.getSystemColor(SWT.COLOR_BLACK));

						// draw black rect around color
						gc.drawRectangle(8, 8, rect.width - 16,
								rect.height - 16);
//					}
				}
			}

		});
		
//		this.button.addMouseMoveListener(new MouseMoveListener() {
//			public void mouseMove(MouseEvent e) {
//                if (!hit)
//                    return;
//                mouse = 2;
//                if (e.x < 0 || e.y < 0 || e.x >  getButton().getBounds().width
//                        || e.y > getButton().getBounds().height) {
//                    mouse = 0;
//                }
//			}
//		});
//		
//		this.button.addMouseTrackListener(new MouseTrackAdapter() {
//			public void mouseEnter(MouseEvent e) {
//	               mouse = 1;
//			}
//			public void mouseExit(MouseEvent e) {
//                mouse = 0;
//            }
//		});
//		
//		this.button.addMouseListener(new MouseAdapter() {
//			public void mouseDown(MouseEvent e) {
//				hit = true;
//				mouse = 2;
//			}
//
//			public void mouseUp(MouseEvent e) {
//                hit = false;
//                mouse = 1;
//                if (e.x < 0 || e.y < 0 || e.x > getButton().getBounds().width
//                        || e.y > getButton().getBounds().height) {
//                    mouse = 0;
//                }
//			}
//		});
	}

	/**
	 * 
	 * Set color then redraws button
	 * 
	 * @param color
	 */
	public void setColor(Color color) {
		this.color = color;
		this.button.redraw();
	}

	/**
	 * 
	 * Gets color
	 * 
	 * @return
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * 
	 * Gets button
	 * 
	 * @return
	 */
	public Button getButton() {
		return button;
	}

	/**
	 * 
	 * Button to set.
	 * 
	 * @param button
	 */
	public void setButton(Button button) {
		this.button = button;
	}

}

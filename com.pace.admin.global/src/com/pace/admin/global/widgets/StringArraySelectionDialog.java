/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.widgets;

import java.util.Arrays;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

public class StringArraySelectionDialog extends Dialog {

	class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		
		public String getColumnText(Object element, int columnIndex) {

			String retVal = null;
			
			if ( columnIndex == 0 ) {
				
				if ( element instanceof String ) {
					
					retVal = (String) element;
					
				}
				
			} 				
			
			return retVal;			
		}
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
	}
	class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return (Object[]) inputElement;
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	private Table table;
	
	private String[] items;

	private Button selectDeselectButton;

	private CheckboxTableViewer checkboxTableViewer;
	
	private String[] selectedItems = null;

	private String title;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public StringArraySelectionDialog(Shell parentShell, String title, String[] items, String[] selectedItems) {
		
		super(parentShell);
		
		this.title = title;
		
		
		if ( this.title == null ) {
			
			this.title = "Make Selection";
			
		}
		
		this.items = items;
				
		if ( this.items != null ) {
			
			Arrays.sort(this.items);
			
		}
		
		this.selectedItems = selectedItems;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginBottom = 20;
		gridLayout.marginRight = 10;
		gridLayout.marginLeft = 10;
		container.setLayout(gridLayout);

		final Label selectItemsLabel = new Label(container, SWT.NONE);
		selectItemsLabel.setText("Select Items:");

		checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER);
		checkboxTableViewer.setLabelProvider(new TableLabelProvider());
		checkboxTableViewer.setContentProvider(new ContentProvider());
		checkboxTableViewer.setInput(this.items);
		
		if ( selectedItems != null ) {
					
			checkboxTableViewer.setCheckedElements(selectedItems);
			
		}
		
		table = checkboxTableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		selectDeselectButton = new Button(container, SWT.CHECK);
		selectDeselectButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				checkboxTableViewer.setAllChecked(selectDeselectButton.getSelection());
				
			}
		});
		selectDeselectButton.setText("Select / Deselect All");
		//
		return container;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(276, 463);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			
			if ( checkboxTableViewer.getCheckedElements().length > 0 ) {
				
				selectedItems = Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new String[0]);
				
			} else {
				
				selectedItems = null;
				
			}
					
			
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * @return the selectedItems
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

}

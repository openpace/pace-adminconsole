/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.global.webservices;

import java.util.Arrays;

import javax.xml.datatype.XMLGregorianCalendar;

import com.pace.base.comm.SimpleCoordList;
import com.pace.server.client.SimpleCellNote;


/**
 * Cell Note.
 *
 * @author jmilliron
 * @version	1.00
 * 
 */
public class CellNote {
		
	private SimpleCoordList simpleCoordList;
	
	private int id;
	
	private String text;

	private String creator;

	private XMLGregorianCalendar lastUpdated;
	
	private boolean visible;

	private String applicationName;
	
	private String dataSourceName;
	
	/**
	 * 
	 * Create a new Cell Note
	 */
	public CellNote() {		
	}
	
	/**
	 * 
	 * Create a new Cell Note from a Simple Cell Note
	 * 
	 * @param simpleCellNote Simple Cell Note
	 */
	public CellNote(SimpleCellNote simpleCellNote) {
		
		if ( simpleCellNote == null ) {
			throw new IllegalArgumentException("Simple Cell Note can not be null.");
		}
		
		
		this.id = simpleCellNote.getId();
		this.text = simpleCellNote.getText();
		this.creator = simpleCellNote.getCreator();
		this.lastUpdated = simpleCellNote.getLastUpdated();
		this.applicationName = simpleCellNote.getApplicationName();
		this.dataSourceName = simpleCellNote.getDataSourceName();
		this.visible = simpleCellNote.isVisible();
		com.pace.server.client.SimpleCoordList simpleCellNoteSimpleCoordList = simpleCellNote.getSimpleCoordList();
		
		if ( simpleCellNoteSimpleCoordList != null ) {
			
			this.simpleCoordList = new SimpleCoordList();
			this.simpleCoordList.setAxis(simpleCellNoteSimpleCoordList.getAxis().toArray(new String[0]));
			this.simpleCoordList.setCoordinates(simpleCellNoteSimpleCoordList.getCoordinates().toArray(new String[0]));
			
		}
		
	}
	
	public SimpleCellNote getSimpleCellNote() {
		
		//create simple cell note
		SimpleCellNote simpleCellNote = new SimpleCellNote();
		
		simpleCellNote.setId(this.id);
		simpleCellNote.setApplicationName(this.applicationName);
		simpleCellNote.setCreator(this.creator);
		simpleCellNote.setDataSourceName(this.dataSourceName);
		simpleCellNote.setLastUpdated(this.lastUpdated);
		simpleCellNote.setText(this.text);
		simpleCellNote.setVisible(this.visible);
		
		if ( this.simpleCoordList != null ) {
		
			com.pace.server.client.SimpleCoordList simpleCellNoteSimpleCoordList = new com.pace.server.client.SimpleCoordList();
			
			if ( this.simpleCoordList.getAxis() != null && this.simpleCoordList.getCoordinates() != null ) {
				
				simpleCellNoteSimpleCoordList.getAxis().addAll(Arrays.asList(this.simpleCoordList.getAxis()));
				simpleCellNoteSimpleCoordList.getCoordinates().addAll(Arrays.asList(this.simpleCoordList.getCoordinates()));
				
			}			
			
			simpleCellNote.setSimpleCoordList(simpleCellNoteSimpleCoordList);
			
		}		
				
		return simpleCellNote;		
		
	}	
	

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer("Cell Note:");

		strBuff.append("\n\tId: " + this.id);
		strBuff.append("\n\tText: " + this.text);
		strBuff.append("\n\tCreator: " + this.creator);
		strBuff.append("\n\tLast Updated: " + this.lastUpdated);
		strBuff.append("\n\tApplication: " + this.getApplicationName());
		strBuff.append("\n\tDataSource: " + this.getDataSourceName());
		strBuff.append("\n");
		
		
		return strBuff.toString();
	}

	/**
	 * @return the applicationName
	 */
	public String getApplicationName() {
		return applicationName;
	}

	/**
	 * @param applicationName the applicationName to set
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the dataSourceName
	 */
	public String getDataSourceName() {
		return dataSourceName;
	}

	/**
	 * @param dataSourceName the dataSourceName to set
	 */
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lastUpdated
	 */
	public XMLGregorianCalendar getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(XMLGregorianCalendar lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * @return the simpleCoordList
	 */
	public SimpleCoordList getSimpleCoordList() {
		return simpleCoordList;
	}

	/**
	 * @param simpleCoordList the simpleCoordList to set
	 */
	public void setSimpleCoordList(SimpleCoordList simpleCoordList) {
		this.simpleCoordList = simpleCoordList;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
}

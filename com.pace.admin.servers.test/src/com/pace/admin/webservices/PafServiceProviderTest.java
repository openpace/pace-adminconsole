/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.webservices;


import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.utility.AESEncryptionUtil;
import com.pace.server.client.Application;
import com.pace.server.client.ClientInitRequest;
import com.pace.server.client.PafAuthRequest;
import com.pace.server.client.PafAuthResponse;
import com.pace.server.client.PafClientSecurityRequest;
import com.pace.server.client.PafClientSecurityResponse;
import com.pace.server.client.PafGetPaceGroupsRequest;
import com.pace.server.client.PafGetPaceGroupsResponse;
import com.pace.server.client.PafGroupSecurityRequest;
import com.pace.server.client.PafGroupSecurityResponse;
import com.pace.server.client.PafNotAbletoGetLDAPContext_Exception;
import com.pace.server.client.PafNotAuthenticatedSoapException_Exception;
import com.pace.server.client.PafNotAuthorizedSoapException_Exception;
import com.pace.server.client.PafSecurityDomainGroups;
import com.pace.server.client.PafSecurityDomainUserNames;
import com.pace.server.client.PafSecurityGroup;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSetPaceGroupsRequest;
import com.pace.server.client.PafSetPaceGroupsResponse;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafUserDef;
import com.pace.server.client.PafUserNamesSecurityGroup;
import com.pace.server.client.PafUserNamesforSecurityGroupsRequest;
import com.pace.server.client.PafUserNamesforSecurityGroupsResponse;
import com.pace.server.client.PafVerifyUsersRequest;
import com.pace.server.client.PafVerifyUsersResponse;
import com.pace.server.client.SecurityGroup;

/**
 * @author pmack
 *
 */
public class PafServiceProviderTest extends TestCase {

	private String url = "http://localhost:8080/pace/app?wsdl";
	
	private PafService webService;
	
	private PafServerAck ack;
	
	/**
	 * @throws java.lang.Exception
	 */
	//@Before
	public void setUp() throws Exception {
		
		ack = WebServicesUtil.testPafServerAck(url, false);
		
		webService = WebServicesUtil.testPafService(url);
				
	}

	/**
	 * @throws java.lang.Exception
	 */
	//@After
	public void tearDown() throws Exception {
	}

	
	public void testClientInit() {
		
		ClientInitRequest req = new ClientInitRequest();
		
		req.setIpAddress("test ip");
		req.setClientType("AC");
		
			PafServerAck ack;
			try {
				
				ack = webService.clientInit(req);
			
				assertNotNull(ack);
				assertNotNull(ack.getApplicationId());
				assertNotNull(ack.getClientId());
						
			} catch (PafSoapException_Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		
	}
	
	public void testGetUserNamesforSecurityGroups() throws PafNotAuthenticatedSoapException_Exception {
		PafUserNamesforSecurityGroupsRequest groupSecurityRequest = new PafUserNamesforSecurityGroupsRequest();
    	
		groupSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Jim");
		authReq.setPassword("");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			groupSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
			} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
    	List<PafSecurityDomainGroups> securityDomainGroupsList = new ArrayList<PafSecurityDomainGroups>();
    	PafSecurityDomainGroups securityDomainGroups = new PafSecurityDomainGroups();

//    	securityDomainGroups.setDomain("Native");
//    	securityDomainGroupsList.add(securityDomainGroups);
    	
    	
    	PafSecurityGroup securityGroup;
    	
    	securityDomainGroups.setDomain("ADG.com");
    	List<PafSecurityGroup> securityGroups = new ArrayList<PafSecurityGroup>();
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Pace Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("ADG Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Domain Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Fake Users");
    	securityGroups.add(securityGroup);
    	securityDomainGroups.getSecurityGroups().addAll(securityGroups);
    	securityDomainGroupsList.add(securityDomainGroups);
    	
    	PafSecurityDomainGroups securityDomainGroups2 = new PafSecurityDomainGroups();
    	PafSecurityGroup securityGroup2;
    	
    	securityDomainGroups2.setDomain("ADG2.com");
    	List<PafSecurityGroup> securityGroups2 = new ArrayList<PafSecurityGroup>();
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("Pace Users");
    	securityGroups2.add(securityGroup);
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("ADG Users");
    	securityGroups2.add(securityGroup);
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("Domain Users");
    	securityGroups2.add(securityGroup2);
    	securityDomainGroups2.getSecurityGroups().addAll(securityGroups2);
    	securityDomainGroupsList.add(securityDomainGroups2);
    	
    	//groupSecurityRequest.getDomainUserNames().addAll(securityDomainGroupsList);
			
		groupSecurityRequest.setApplication("TitanCopy");

    	PafUserNamesforSecurityGroupsResponse userNamesforSecurityGroups;
    	
		try {
			userNamesforSecurityGroups = webService.getUserNamesForSecurityGroups(groupSecurityRequest);
					   					
			for ( PafSecurityDomainUserNames domainUserName : userNamesforSecurityGroups.getDomainUserNames()){
				System.out.println(domainUserName.getDomainName());
				for (PafUserNamesSecurityGroup userNamesSecGroup: domainUserName.getUserNamesSecurityGroup()){
					System.out.println(userNamesSecGroup.getGroupName());
					for (String userName : userNamesSecGroup.getUserNames()){
						System.out.println(userName);
					}
					for (String displayName : userNamesSecGroup.getDisplayNames()){
						System.out.println(displayName);
					}
				}
			}
			
		} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}
	}
	
	public void testGetSecurityGroups() {
		PafGroupSecurityRequest groupSecurityRequest = new PafGroupSecurityRequest();
    	
		groupSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Jim");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			groupSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
		} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		PafGroupSecurityResponse groupSecurityResponse = new PafGroupSecurityResponse();
		
		try{
			groupSecurityResponse = webService.getPafGroups(groupSecurityRequest);
			
			List <PafSecurityDomainGroups>  securityDomainGroups = groupSecurityResponse.getDomainGroups();
			
			for(PafSecurityDomainGroups securityDomainGroup : securityDomainGroups){
				System.out.println(securityDomainGroup.getDomain());
				
				for (PafSecurityGroup securityGroup : securityDomainGroup.getSecurityGroups()){
					System.out.println(securityGroup.getGroupName());
				}
			}
			
		} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();			
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	
	public void testGetPaceGroups() {
		PafGetPaceGroupsRequest paceGroupsRequest = new PafGetPaceGroupsRequest();
    	
		paceGroupsRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Mary");
		authReq.setDomain("ADG.com");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			paceGroupsRequest.setSessionToken(response.getSecurityToken().getSessionToken());

		} catch (PafSoapException_Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		paceGroupsRequest.setApplication("ABC.com");
		
		PafGetPaceGroupsResponse paceGroupsResponse = new PafGetPaceGroupsResponse();
	
		try{
			paceGroupsResponse = webService.getGroups(paceGroupsRequest);
			
			for(SecurityGroup securityGroups : paceGroupsResponse.getSecurityGroups())
			{
				System.out.println(securityGroups.getSecurityDomainNameTxt() + "." + 
						securityGroups.getSecurityGroupNameTxt());
				
			}

		} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	public void testSetPaceGroups() {
		PafSetPaceGroupsRequest paceGroupsRequest = new PafSetPaceGroupsRequest();
    	
		paceGroupsRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Jim");
		authReq.setDomain("ADG.com");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			paceGroupsRequest.setSessionToken(response.getSecurityToken().getSessionToken());

		} catch (PafSoapException_Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		Application app = new Application();
		app.setName("corp.thinkfast.com");
		
		SecurityGroup securityGroups1 = new SecurityGroup();
		securityGroups1.setSecurityDomainNameTxt("corp.thinkfast.com");
		securityGroups1.setSecurityGroupNameTxt("ADG.Admin");
		securityGroups1.setApplication(app);
		paceGroupsRequest.getSecurityGroups().add(securityGroups1);
		
		SecurityGroup securityGroups2 = new SecurityGroup();
		securityGroups2.setSecurityDomainNameTxt("corp.thinkfast.com");
		securityGroups2.setSecurityGroupNameTxt("ADG.General");
		securityGroups2.setApplication(app);
		paceGroupsRequest.getSecurityGroups().add(securityGroups2);
		
		Application app2 = new Application();
		app2.setName("ABCD.com");
		
		SecurityGroup securityGroups3 = new SecurityGroup();
		securityGroups3.setSecurityDomainNameTxt("ADG3");
		securityGroups3.setSecurityGroupNameTxt("Group3");
		securityGroups3.setApplication(app2);
		paceGroupsRequest.getSecurityGroups().add(securityGroups3);
	
		try{
			PafSetPaceGroupsResponse paceGroupsResponse = webService.setGroups(paceGroupsRequest);
			
			System.out.println(paceGroupsResponse.isSuccess());
		} catch (PafSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	
	public void testGetUser() {
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Jim");
		authReq.setDomain("Native");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
		} catch (PafSoapException_Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PafUserDef userDef = new PafUserDef();
		
		userDef.setUserName("Mack, Paul");
		userDef.setDomain("corp.thinkfast.com");
		
		clientSecurityRequest.setPafUserDef(userDef);
		
		PafClientSecurityResponse clientSecurityResponse = new PafClientSecurityResponse();
		
		try{
			clientSecurityResponse = webService.getPafUser(clientSecurityRequest);
			
			if (clientSecurityResponse.getPafUserDef() != null){
				System.out.println(clientSecurityResponse.getPafUserDef().getDomain());
				System.out.println(clientSecurityResponse.getPafUserDef().getEmail());
				System.out.println(clientSecurityResponse.getPafUserDef().getFirstName());
				System.out.println(clientSecurityResponse.getPafUserDef().getLastName());
				System.out.println(clientSecurityResponse.getPafUserDef().getPassword());
				System.out.println(clientSecurityResponse.getPafUserDef().getUserName());
			}
		
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();			
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	

	
	public void testSSAuthenticate1() {
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		try{
			String iV = AESEncryptionUtil.generateIV();
			authReq.setUsername(AESEncryptionUtil.encrypt("Jim Watkins", iV));
			authReq.setSid(AESEncryptionUtil.encrypt("S-1-5-21-3383596266-2351974538-3218869496-1116", iV));
			authReq.setIV(iV);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertTrue(response.getSecurityToken().isValid());			
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
			} catch (Exception e1) {
			// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
	}
	
	public void testSSAuthenticate() {
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		try{
			//String iV = AESEncryptionUtil.generateIV();
			String iV = "123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789";
			authReq.setUsername(AESEncryptionUtil.encrypt("jim", iV));
			//authReq.setPassword(AESEncryptionUtil.encrypt("", iV));
			authReq.setIV(iV);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertTrue(response.getSecurityToken().isValid());			
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
			} catch (Exception e1) {
			// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
	}
	
	public void testVerifyUsers() {
	PafVerifyUsersRequest verUsersRequest = new PafVerifyUsersRequest();
	
	verUsersRequest.setClientId(ack.getClientId());
	
	PafAuthRequest authReq = new PafAuthRequest();
	
	authReq.setClientId(ack.getClientId());
	authReq.setUsername("Jim");
	
	PafAuthResponse response = null;
	
	try {
		
		response = webService.clientAuth(authReq);
		
		assertNotNull(response);
		assertNotNull(response.getSecurityToken());			
		
		verUsersRequest.setSessionToken(response.getSecurityToken().getSessionToken());
		
	} catch (PafSoapException_Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	
	PafVerifyUsersResponse verUsersResponse = new PafVerifyUsersResponse();
	
	
	verUsersRequest.getUsers().add("pmack@corp.thinkfast.com");
	verUsersRequest.getUsers().add("pmack@corp.thinkfast.co");
	verUsersRequest.getUsers().add("Paul Mack@ADG.com");
	
	try{
		verUsersResponse = webService.verifyUsers(verUsersRequest);
		
		
		for(String user : verUsersResponse.getUsers()){
			System.out.println(user);
		}
		
	} catch (PafSoapException_Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (PafNotAuthorizedSoapException_Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (PafNotAbletoGetLDAPContext_Exception e){
		// TODO Auto-generated catch block
		e.printStackTrace();			
	} catch (PafNotAuthenticatedSoapException_Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}		
	
}
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


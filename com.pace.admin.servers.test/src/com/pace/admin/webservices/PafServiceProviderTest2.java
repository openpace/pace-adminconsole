/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.webservices;


import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.server.client.Application;
import com.pace.server.client.ClientInitRequest;
import com.pace.server.client.PafAuthRequest;
import com.pace.server.client.PafAuthResponse;
import com.pace.server.client.PafClientSecurityRequest;
import com.pace.server.client.PafClientSecurityResponse;
import com.pace.server.client.PafGetPaceGroupsRequest;
import com.pace.server.client.PafGetPaceGroupsResponse;
import com.pace.server.client.PafGroupSecurityRequest;
import com.pace.server.client.PafGroupSecurityResponse;
import com.pace.server.client.PafNotAbletoGetLDAPContext_Exception;
import com.pace.server.client.PafNotAuthenticatedSoapException_Exception;
import com.pace.server.client.PafNotAuthorizedSoapException_Exception;
import com.pace.server.client.PafSecurityDomainGroups;
import com.pace.server.client.PafSecurityDomainUserNames;
import com.pace.server.client.PafSecurityGroup;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSetPaceGroupsRequest;
import com.pace.server.client.PafSetPaceGroupsResponse;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafUserDef;
import com.pace.server.client.PafUserNamesSecurityGroup;
import com.pace.server.client.PafUserNamesforSecurityGroupsRequest;
import com.pace.server.client.PafUserNamesforSecurityGroupsResponse;
import com.pace.server.client.SecurityGroup;

/**
 * @author pmack
 *
 */
public class PafServiceProviderTest2 extends TestCase {

	private static final String ADG_COM = "adg.com";

	private static final String TITAN_COPY = "TitanCopy";

	private static Logger logger = Logger.getLogger(PafServiceProviderTest2.class);
	
	private String url = "http://localhost:8080/pace/app?wsdl";
	
	private PafService webService;
	
	private PafServerAck ack;
	
	/**
	 * @throws java.lang.Exception
	 */
	//@Before
	public void setUp() throws Exception {
		
		ack = WebServicesUtil.testPafServerAck(url, false);
		
		webService = WebServicesUtil.testPafService(url);
				
	}

	/**
	 * @throws java.lang.Exception
	 */
	//@After
	public void tearDown() throws Exception {
		
		webService = null;
		ack = null;
		
	}

	
	public void testClientInit() {
		
		ClientInitRequest req = new ClientInitRequest();
		
		req.setIpAddress("test ip");
		req.setClientType("AC");
		
			PafServerAck ack;
			try {
				
				ack = webService.clientInit(req);
			
				assertNotNull(ack);
				assertNotNull(ack.getApplicationId());
				assertNotNull(ack.getClientId());
						
			} catch (PafSoapException_Exception e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}	
		
	}
	
	
	public void testGetUserNamesforSecurityGroups() throws PafNotAuthenticatedSoapException_Exception {
				
		loginAsJim();
		
		PafUserNamesforSecurityGroupsRequest groupSecurityRequest = new PafUserNamesforSecurityGroupsRequest();
    	
		groupSecurityRequest.setClientId(ack.getClientId());
		
    	List<PafSecurityDomainGroups> securityDomainGroupsList = new ArrayList<PafSecurityDomainGroups>();
    	PafSecurityDomainGroups securityDomainGroups = new PafSecurityDomainGroups();
    	PafSecurityGroup securityGroup;
    	
    	securityDomainGroups.setDomain("ADG.com");
    	List<PafSecurityGroup> securityGroups = new ArrayList<PafSecurityGroup>();
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Pace Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("ADG Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Domain Users");
    	securityGroups.add(securityGroup);
    	securityGroup = new PafSecurityGroup();
    	securityGroup.setGroupName("Fake Users");
    	securityGroups.add(securityGroup);
    	securityDomainGroups.getSecurityGroups().addAll(securityGroups);
    	securityDomainGroupsList.add(securityDomainGroups);
    	
    	PafSecurityDomainGroups securityDomainGroups2 = new PafSecurityDomainGroups();
    	PafSecurityGroup securityGroup2;
    	
    	securityDomainGroups2.setDomain("ADG2.com");
    	List<PafSecurityGroup> securityGroups2 = new ArrayList<PafSecurityGroup>();
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("Pace Users");
    	securityGroups2.add(securityGroup);
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("ADG Users");
    	securityGroups2.add(securityGroup);
    	securityGroup2 = new PafSecurityGroup();
    	securityGroup2.setGroupName("Domain Users");
    	securityGroups2.add(securityGroup2);
    	securityDomainGroups2.getSecurityGroups().addAll(securityGroups2);
    	securityDomainGroupsList.add(securityDomainGroups2);
    	
    	//groupSecurityRequest.getDomainUserNames().addAll(securityDomainGroupsList);
			
		groupSecurityRequest.setApplication("TitanCopy");

		try {
	    	PafUserNamesforSecurityGroupsResponse userNamesforSecurityGroups = webService.getUserNamesForSecurityGroups(groupSecurityRequest);
					   					
			for ( PafSecurityDomainUserNames domainUserName : userNamesforSecurityGroups.getDomainUserNames()){
				logger.info(domainUserName.getDomainName());
				for (PafUserNamesSecurityGroup userNamesSecGroup: domainUserName.getUserNamesSecurityGroup()){
					logger.info("\t" + userNamesSecGroup.getGroupName());
					for (String userName : userNamesSecGroup.getUserNames()){
						logger.info("\t\t" + userName);
					}
				}
			}
			
		} catch (PafSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthorizedSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			fail(e.getMessage());		
		}
	}
	
	
	
	public void testGetSecurityGroups() {
				
		loginAsJim();
		
		PafGroupSecurityRequest groupSecurityRequest = new PafGroupSecurityRequest();
    	
		groupSecurityRequest.setClientId(ack.getClientId());		
		
		try{
			
			PafGroupSecurityResponse groupSecurityResponse = webService.getPafGroups(groupSecurityRequest);
			
			List <PafSecurityDomainGroups>  securityDomainGroups = groupSecurityResponse.getDomainGroups();
			
			for(PafSecurityDomainGroups securityDomainGroup : securityDomainGroups){
				
				logger.info(securityDomainGroup.getDomain());
				
				for (PafSecurityGroup securityGroup : securityDomainGroup.getSecurityGroups()){
					logger.info("\t" + securityGroup.getGroupName());
				}
			}
			
		} catch (PafSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthorizedSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			fail(e.getMessage());			
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			fail(e.getMessage());
		}		
		
	}
	
	
	
	public void testGetPaceGroups() {
		
		loginAsJim();
		
		PafGetPaceGroupsRequest paceGroupsRequest = new PafGetPaceGroupsRequest();
    	
		paceGroupsRequest.setClientId(ack.getClientId());
				
		paceGroupsRequest.setApplication(TITAN_COPY);
		
		PafGetPaceGroupsResponse paceGroupsResponse = new PafGetPaceGroupsResponse();
	
		try{
			
			logger.info("getPaceGroups() for "+TITAN_COPY);
			paceGroupsResponse = webService.getGroups(paceGroupsRequest);
			
			for(SecurityGroup securityGroups : paceGroupsResponse.getSecurityGroups())
			{
				logger.info("\t" + securityGroups.getSecurityDomainNameTxt() + "." + securityGroups.getSecurityGroupNameTxt());
				
			}

		} catch (PafSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthorizedSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			fail(e.getMessage());
		}		
		
	}

	
	
	public void testSetPaceGroups() {
		
		loginAsJim();
		
		PafSetPaceGroupsRequest paceGroupsRequest = new PafSetPaceGroupsRequest();
    	
		paceGroupsRequest.setClientId(ack.getClientId());
					
		Application app = new Application();
		app.setName(TITAN_COPY);
		
		SecurityGroup securityGroups1 = new SecurityGroup();
		securityGroups1.setSecurityDomainNameTxt(ADG_COM);
		securityGroups1.setSecurityGroupNameTxt("Pace Users");
		securityGroups1.setApplication(app);
		paceGroupsRequest.getSecurityGroups().add(securityGroups1);
		
		SecurityGroup securityGroups2 = new SecurityGroup();
		securityGroups2.setSecurityDomainNameTxt(ADG_COM);
		securityGroups2.setSecurityGroupNameTxt("ADG Users");
		securityGroups2.setApplication(app);
		paceGroupsRequest.getSecurityGroups().add(securityGroups2);
		
		Application app2 = new Application();
		app2.setName("ABC.com");
		
		SecurityGroup securityGroups3 = new SecurityGroup();
		securityGroups3.setSecurityDomainNameTxt("ADG3");
		securityGroups3.setSecurityGroupNameTxt("Group3");
		securityGroups3.setApplication(app2);
		paceGroupsRequest.getSecurityGroups().add(securityGroups3);
	
		try{
			PafSetPaceGroupsResponse paceGroupsResponse = webService.setGroups(paceGroupsRequest);
			
			logger.info(paceGroupsResponse.isSuccess());
			
		} catch (PafSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthorizedSoapException_Exception e) {
			fail(e.getMessage());
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			fail(e.getMessage());
		}		
		
	}
	
	
	public void testGetUser() {
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		authReq.setUsername("Jim");
		authReq.setDomain("Native");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());			
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
		} catch (PafSoapException_Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PafUserDef userDef = new PafUserDef();
		
		userDef.setUserName("Fred");
		userDef.setDomain("ADG1");
		
		clientSecurityRequest.setPafUserDef(userDef);
		
		PafClientSecurityResponse clientSecurityResponse = new PafClientSecurityResponse();
		
		try{
			clientSecurityResponse = webService.getPafUser(clientSecurityRequest);
			
			System.out.println(clientSecurityResponse.getPafUserDef().getDomain());
			System.out.println(clientSecurityResponse.getPafUserDef().getEmail());
			System.out.println(clientSecurityResponse.getPafUserDef().getFirstName());
			System.out.println(clientSecurityResponse.getPafUserDef().getLastName());
			System.out.println(clientSecurityResponse.getPafUserDef().getPassword());
			System.out.println(clientSecurityResponse.getPafUserDef().getUserName());
		
		} catch (PafNotAuthorizedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PafNotAbletoGetLDAPContext_Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();			
		} catch (PafNotAuthenticatedSoapException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}


	
	private void loginAsJim() {

		try {
			assertEquals(true, SecurityManager.testAuthenticate(url, "jim", ""));
		} catch (Exception e1) {
			fail(e1.getMessage());			
		}
		
	}

	public void testAuthenticateMixedSingleSignOn() {
		
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		
		authReq.setUsername("jim");
		authReq.setPassword("");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());
			assertEquals(true, response.getSecurityToken().isValid());
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
			} catch (PafSoapException_Exception e1) {
				
				fail(e1.getMessage());
		}
		
		//create pmack user
		PafUserDef userDef = new PafUserDef();		
		userDef.setUserName("admin");
//		userDef.setDomain(ADG_COM);
		
		clientSecurityRequest.setPafUserDef(userDef);
						
		try{
			PafClientSecurityResponse clientSecurityResponse = webService.getPafUser(clientSecurityRequest);
			
			//not null
			assertNotNull(clientSecurityResponse.getPafUserDef().getUserName());
			assertNotNull(clientSecurityResponse.getPafUserDef().getEmail());
			assertNotNull(clientSecurityResponse.getPafUserDef().getFirstName());
			assertNotNull(clientSecurityResponse.getPafUserDef().getLastName());
			
			//nullable
			assertNotNull(clientSecurityResponse.getPafUserDef().getPassword());
			assertNull(clientSecurityResponse.getPafUserDef().getDomain());
			
			logger.info(clientSecurityResponse.getPafUserDef().getDomain());
			logger.info(clientSecurityResponse.getPafUserDef().getEmail());
			logger.info(clientSecurityResponse.getPafUserDef().getFirstName());
			logger.info(clientSecurityResponse.getPafUserDef().getLastName());
			logger.info(clientSecurityResponse.getPafUserDef().getPassword());
			logger.info(clientSecurityResponse.getPafUserDef().getUserName());
		
			} catch (PafNotAuthorizedSoapException_Exception e) {
				fail(e.getMessage());
			} catch (PafNotAbletoGetLDAPContext_Exception e){
				fail(e.getMessage());
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				fail(e.getMessage());
			}		
		
	}
	
	public void testAuthenticateMixedSingleSignOnButWithNativeUser() {
		
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		PafAuthRequest authReq = new PafAuthRequest();
		
		authReq.setClientId(ack.getClientId());
		
		authReq.setUsername("Jim");
		authReq.setPassword("");
		
		PafAuthResponse response = null;
		
		try {
			
			response = webService.clientAuth(authReq);
			
			assertNotNull(response);
			assertNotNull(response.getSecurityToken());
			assertEquals(true, response.getSecurityToken().isValid());
			
			clientSecurityRequest.setSessionToken(response.getSecurityToken().getSessionToken());
			
			} catch (PafSoapException_Exception e1) {
				
				fail(e1.getMessage());
		}
		

	}
	
	public void testGetUserNative() {
		
		loginAsJim();
		
		PafClientSecurityRequest clientSecurityRequest = new PafClientSecurityRequest();
    	
		clientSecurityRequest.setClientId(ack.getClientId());
		
		//create pmack user
		PafUserDef userDef = new PafUserDef();		
		//userDef.setUserName("Paul Mack");
		//userDef.setUserName("pmack");
		userDef.setUserName("jim");
		userDef.setPassword("");
		
		//userDef.setDomain("ADG.com");
		
		clientSecurityRequest.setPafUserDef(userDef);
						
		try{
			PafClientSecurityResponse clientSecurityResponse = webService.getPafUser(clientSecurityRequest);
			
			PafUserDef retrivedUserDef = clientSecurityResponse.getPafUserDef();
			
			assertNotNull(retrivedUserDef);
			
			//not null
			assertNotNull(retrivedUserDef.getUserName());
			assertNotNull(retrivedUserDef.getPassword());
			assertNotNull(retrivedUserDef.getEmail());
			assertNotNull(retrivedUserDef.getFirstName());
			assertNotNull(retrivedUserDef.getLastName());
			
			//nullable
			assertNull(retrivedUserDef.getDomain());
			
			logger.info(retrivedUserDef.getDomain());
			logger.info(retrivedUserDef.getEmail());
			logger.info(retrivedUserDef.getFirstName());
			logger.info(retrivedUserDef.getLastName());
			logger.info(retrivedUserDef.getPassword());
			logger.info(retrivedUserDef.getUserName());
		
			} catch (PafNotAuthorizedSoapException_Exception e) {
				fail(e.getMessage());
			} catch (PafNotAbletoGetLDAPContext_Exception e){
				fail(e.getMessage());
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				fail(e.getMessage());
			}		
		
	}
		
}


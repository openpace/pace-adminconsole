/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.swtdesigner.SWTResourceManager;


public class DeleteUserDialog extends Dialog {

	private String userName;
	private boolean deleteAllReferences = false;	
	private Button deleteAllSecurityButton;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public DeleteUserDialog(Shell parentShell, String userName) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.userName = userName;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		final Label deleteUserLabel = new Label(container, SWT.NONE);
		deleteUserLabel.setFont(SWTResourceManager.getFont("Tahoma", 9, SWT.NORMAL));
		final FormData formData = new FormData();
		formData.top = new FormAttachment(0, 19);
		formData.right = new FormAttachment(0, 337);
		formData.left = new FormAttachment(0, 91);
		deleteUserLabel.setLayoutData(formData);
		deleteUserLabel.setText("Delete '" + userName + "' from database?");

		deleteAllSecurityButton = new Button(container, SWT.CHECK);
		formData.bottom = new FormAttachment(deleteAllSecurityButton, -19);
		deleteAllSecurityButton.setToolTipText("Delete all security references for this user on all projects");
		final FormData formData_1 = new FormData();
		formData_1.top = new FormAttachment(0, 63);
		formData_1.right = new FormAttachment(100, -188);
		formData_1.left = new FormAttachment(0, 91);
		deleteAllSecurityButton.setLayoutData(formData_1);
		deleteAllSecurityButton.setText("Delete references");
		//
		return container;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(457, 216);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Confirm");
	}

	/**
	 * @return the deleteAllRefernces
	 */
	public boolean isDeleteAllReferences() {
		return deleteAllReferences;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		deleteAllReferences = deleteAllSecurityButton.getSelection();
		
		super.okPressed();
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs.input;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PaceSecurityGroupSelectionInput {
	
//	key = domain name; value = set of security groups 
	private Map<String, Set<String>> initialDomainPaceSecurityGroupMap;
	
	//key = domain name; value = set of security groups 
	private Map<String, Set<String>> allDomainPaceSecurityGroupMap;
		
	//key = domain name; value = set of security groups 
	private Map<String, Set<String>> selectedDomainPaceSecurityGroupMap;
	
	public PaceSecurityGroupSelectionInput(Map<String, Set<String>> allDomainPaceSecurityGroupMap, Map<String, Set<String>> selectedDomainPaceSecurityGroupMap) {
		
		this.allDomainPaceSecurityGroupMap = allDomainPaceSecurityGroupMap;
		this.selectedDomainPaceSecurityGroupMap = selectedDomainPaceSecurityGroupMap;
		
		if ( allDomainPaceSecurityGroupMap != null ) {
			
			initialDomainPaceSecurityGroupMap = new HashMap<String, Set<String>>();
			
			for (String key : allDomainPaceSecurityGroupMap.keySet()) {
				
				Set<String> securityGroupSet = new HashSet<String>();
				
				Set<String> allSecurityGroupSet = allDomainPaceSecurityGroupMap.get(key);
				
				if ( allSecurityGroupSet != null ) {
					
					securityGroupSet.addAll(allSecurityGroupSet);
					
				}
				
				initialDomainPaceSecurityGroupMap.put(key, securityGroupSet);			
				
			}
			
			
		}
		
	}
		
	/**
	 * 
	 *  Gets the domain names.
	 *
	 * @return
	 */
	public String[] getDomainNames() {
		
		String[] domainNames = null;
		
		Set<String> domainNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		if ( allDomainPaceSecurityGroupMap != null ) {
		
			domainNameSet.addAll(allDomainPaceSecurityGroupMap.keySet());
			
		}	
		
		if ( selectedDomainPaceSecurityGroupMap != null ) {
			
			domainNameSet.addAll(selectedDomainPaceSecurityGroupMap.keySet());			
			
		}
		
		if ( domainNameSet.size() > 0 ) {
			
			domainNames = domainNameSet.toArray(new String[0]);
			
		}
		
		return domainNames;		
		
	}
	
	/**
	 * 
	 *  Gets all the security groups for the domain.
	 *
	 * @param domainName
	 * @return
	 */
	public String[] getSecurityGroups(String domainName) {
		
		String[] securityGroups = null;
		
		if ( allDomainPaceSecurityGroupMap != null && domainName != null && allDomainPaceSecurityGroupMap.containsKey(domainName) ) {
		
			Set<String> securityGroupSet = allDomainPaceSecurityGroupMap.get(domainName);
			
			if ( securityGroupSet != null && securityGroupSet.size() > 0 ) {
				
				String[] selectedSecuityGroups = getSelectedSecurityGroups(domainName);
				
				if ( selectedSecuityGroups != null ) {
					
					securityGroupSet.removeAll(Arrays.asList(selectedSecuityGroups));
					
				}
				
				securityGroups = securityGroupSet.toArray(new String[0]);
				
			}
			
		}	
		
		return securityGroups;		
		
	}
	
	/**
	 * 
	 *  Gets the selected security groups for the domain.
	 *
	 * @param domainName
	 * @return
	 */
	public String[] getSelectedSecurityGroups(String domainName) {
		
		String[] selectedSecurityGroups = null;
		
		if ( selectedDomainPaceSecurityGroupMap != null && domainName != null && selectedDomainPaceSecurityGroupMap.containsKey(domainName) ) {
		
			Set<String> selectedSecurityGroupSet = selectedDomainPaceSecurityGroupMap.get(domainName);
			
			if ( selectedSecurityGroupSet != null && selectedSecurityGroupSet.size() > 0 ) {
				
				selectedSecurityGroups = selectedSecurityGroupSet.toArray(new String[0]);
				
			}
			
		}	
		
		return selectedSecurityGroups;		
		
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param domainName
	 * @param newSecurityGroups
	 */
	public void addSelectedSecurityGroups(String domainName, String[] newSecurityGroups) {
		
		if ( selectedDomainPaceSecurityGroupMap != null && domainName != null && newSecurityGroups != null ) {
			
			Set<String> currentSecurityGroupSet = null;
			
			if ( selectedDomainPaceSecurityGroupMap.containsKey(domainName)) {
				
				currentSecurityGroupSet = selectedDomainPaceSecurityGroupMap.get(domainName);
				
			} 
			
			if ( currentSecurityGroupSet == null ) {
				
				currentSecurityGroupSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
			}
			
			List<String> newSecurityGroupsList = Arrays.asList(newSecurityGroups); 
			
			currentSecurityGroupSet.addAll(newSecurityGroupsList);
			
			selectedDomainPaceSecurityGroupMap.put(domainName, currentSecurityGroupSet);
			
			if ( allDomainPaceSecurityGroupMap != null && allDomainPaceSecurityGroupMap.containsKey(domainName)) {
				
				Set<String>  allDomainSecurityGroupSet = allDomainPaceSecurityGroupMap.get(domainName);
				
				if ( allDomainSecurityGroupSet != null ) {
					
					allDomainSecurityGroupSet.removeAll(newSecurityGroupsList);
					
				}
				
				allDomainPaceSecurityGroupMap.put(domainName, allDomainSecurityGroupSet);
			}
			
		}		
		
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param domainName
	 * @param removeSecurityGroups
	 */
	public void removeSelectedSecurityGroups(String domainName, String[] removeSecurityGroups) {
		
		if ( selectedDomainPaceSecurityGroupMap != null && domainName != null && removeSecurityGroups != null ) {
			
			Set<String> currentSecurityGroupSet = null;
			
			List<String> removeSecurityGroupsList = Arrays.asList(removeSecurityGroups);
			
			if ( selectedDomainPaceSecurityGroupMap.containsKey(domainName)) {
				
				currentSecurityGroupSet = selectedDomainPaceSecurityGroupMap.get(domainName);
			
				if ( currentSecurityGroupSet != null ) {
					
					currentSecurityGroupSet.removeAll(removeSecurityGroupsList);
				
					selectedDomainPaceSecurityGroupMap.put(domainName, currentSecurityGroupSet);
					
				}
			} 
			
			if ( allDomainPaceSecurityGroupMap != null ) {
				
				Set<String> allDomainSecurityGroupSet = allDomainPaceSecurityGroupMap.get(domainName);
				
				if ( allDomainSecurityGroupSet == null ) {
					
					allDomainSecurityGroupSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
					
				}
					
				Set<String> initialDomainSecurityGroupSet = initialDomainPaceSecurityGroupMap.get(domainName);
				
				//loop groups to remove and add to all domain sec group set if it orginally existed
				for (String securityGroupRemoved : removeSecurityGroupsList ) {
					
					if ( initialDomainSecurityGroupSet != null && initialDomainSecurityGroupSet.contains(securityGroupRemoved) ) {
						
						allDomainSecurityGroupSet.add(securityGroupRemoved);		
						
					}
					
				}
																
				if ( allDomainSecurityGroupSet.size() == 0 ) {
					
					allDomainPaceSecurityGroupMap.remove(domainName);
					
				} else {
				
					allDomainPaceSecurityGroupMap.put(domainName, allDomainSecurityGroupSet);
					
				}
				
			}
			
		}		
		
	}

	/**
	 * @return the selectedDomainPaceSecurityGroupMap
	 */
	public Map<String, Set<String>> getSelectedDomainPaceSecurityGroupMap() {
		return selectedDomainPaceSecurityGroupMap;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

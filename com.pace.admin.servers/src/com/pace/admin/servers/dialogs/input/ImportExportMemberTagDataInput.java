/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs.input;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.base.PafException;
import com.pace.server.client.MemberTagInformation;
import com.pace.server.client.PafSoapException_Exception;

/**
 * @author jmilliron
 *
 */
public class ImportExportMemberTagDataInput {

	private static final Logger logger = Logger.getLogger(ImportExportMemberTagDataInput.class);
		
	private String url;
	
	public ImportExportMemberTagDataInput(String url) {
		this.url = url;
	}
	
	public MemberTagInfo[] getMemberTagInfo() throws PafException {
		
		MemberTagInfo[] memberTagInfoAr = null;
		
		if ( url != null && SecurityManager.isAuthenticated(url)) {
		
			//get member Tag info ar
			MemberTagInformation[] memberTagInformationAr = null;
			
			try {
				
				memberTagInformationAr = SecurityManager.getMemberTagInformation(url);
				
			} catch (RemoteException e) {
				
				logger.error(e.getMessage());				
				throw new PafException(e);
				
			} catch (PafSoapException_Exception e) {
				
				logger.error(e.getMessage());				
				throw new PafException(e);
				
			}
							
			//if not null and 1st item return isn't null
			if ( memberTagInformationAr != null && memberTagInformationAr[0] != null ) {
				
				List<MemberTagInfo> memberTagInfoList = new ArrayList<MemberTagInfo>();
				
				for ( MemberTagInformation memberTagInfo : memberTagInformationAr ) {
					
					memberTagInfoList.add(new MemberTagInfo(memberTagInfo));
					
				}	
				
				memberTagInfoAr = memberTagInfoList.toArray(new MemberTagInfo[0]);
				
				
			}
		}
		
		return memberTagInfoAr;
		
	}
	
	public boolean memberTagInfoExists() {
		
		boolean memberTagInfoExists = false;
		
		try {
			
			MemberTagInfo[] memberTagInfoAr = getMemberTagInfo();
			
			if ( memberTagInfoAr != null && memberTagInfoAr.length > 0 ) {
				
				memberTagInfoExists = true;
				
			}
			
		} catch (PafException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return memberTagInfoExists;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.servers.dialogs.providers.CellNoteInfoLabelProvider;
import com.pace.admin.servers.dialogs.providers.StandardObjectArrayContenetProvider;
import com.pace.base.PafBaseConstants;
import com.pace.server.client.CellNotesInformation;
import com.pace.server.client.SimpleCellNote;

public class ImportCellNotesDialog extends TitleAreaDialog {

	public static final String OVERRIDE_DATA_SOURCE_ID = "Override Data Source Id";

	public static final String OVERRIDE_APPLICATION_ID = "Override Application Id";

	public static final String CELL_NOTE_COUNT = "Cell Note Count";

	public static final String DATA_SOURCE_ID = "Data Source Id";

	public static final String APPLICATION_ID = "Application Id";

	//export file format, binary or xml
	private Text importFileTextBox;
	
	//export table
	private Table importTable;
	
	//open file dialog
	private Button openFileDialogButton;
	
	private SimpleCellNote[] simpleCellNotesToImport;

	//checks or unchecks all
	private Button checkUncheckAllButton;
	
	//check box table viewer, holds cell note info
	private CheckboxTableViewer checkboxTableViewer;

	private String serverName;
	
	private Map<CellNotesInformation, List<SimpleCellNote>> importedSimpleCellNoteMap;
	
	private SimpleCellNote[] simpleCellNotsToImportAr;
	
	/**
	 * Create the export cell note dialog.
	 * 
	 * @param parentShell			Parent shell.
	 * @param cellNoteInfoArray 	Input for viewer, an array of cell note info objects
	 * @param exportDirectory 		Initial export directory
	 */
	public ImportCellNotesDialog(Shell parentShell, String serverName) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.serverName = serverName;
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 25;
		gridLayout.marginRight = 25;
		gridLayout.marginLeft = 25;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		final Label importFileLabel = new Label(container, SWT.NONE);
		importFileLabel.setText("Import file:");

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 2;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.marginWidth = 0;
		composite.setLayout(gridLayout_1);

		importFileTextBox = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		importFileTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		openFileDialogButton = new Button(composite, SWT.NONE);
		openFileDialogButton.setText("Browse ...");

		new Label(container, SWT.NONE);

		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginHeight = 0;
		gridLayout_2.marginWidth = 1;
		gridLayout_2.horizontalSpacing = 0;
		gridLayout_2.numColumns = 3;
		composite_1.setLayout(gridLayout_2);

		final Label serverLabel = new Label(composite_1, SWT.NONE);
		serverLabel.setText("Server: ");

		final Label serverNameLabel = new Label(composite_1, SWT.NONE);
		new Label(composite_1, SWT.NONE);
		
		//set server name label if server name is not null
		if ( serverName != null ) {
			serverNameLabel.setText(serverName);
		}
		
		checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
		checkboxTableViewer.setLabelProvider(new CellNoteInfoLabelProvider());
		checkboxTableViewer.setContentProvider(new StandardObjectArrayContenetProvider());

		importTable = checkboxTableViewer.getTable();
		importTable.setHeaderVisible(true);
		importTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn.setWidth(23);

		final TableColumn newColumnTableColumn_5 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_5.setWidth(104);
		newColumnTableColumn_5.setText(APPLICATION_ID);

		final TableColumn newColumnTableColumn_2 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_2.setWidth(95);
		newColumnTableColumn_2.setText(DATA_SOURCE_ID);

		final TableColumn newColumnTableColumn_4 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_4.setWidth(96);
		newColumnTableColumn_4.setText(CELL_NOTE_COUNT);

		final TableColumn newColumnTableColumn_1 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_1.setWidth(131);
		newColumnTableColumn_1.setText(OVERRIDE_APPLICATION_ID);

		final TableColumn newColumnTableColumn_3 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_3.setWidth(137);
		newColumnTableColumn_3.setText(OVERRIDE_DATA_SOURCE_ID);
		
		TextCellEditor overrideAppIdTextCellEditor = new TextCellEditor(checkboxTableViewer.getTable());
		Text overrideAppIdTextControl = (Text) overrideAppIdTextCellEditor.getControl();
		overrideAppIdTextControl.setTextLimit(255);
		
		TextCellEditor overrideDataSourceIdTextCellEditor = new TextCellEditor(checkboxTableViewer.getTable());
		Text overrideDataSourceIdTextControl = (Text) overrideDataSourceIdTextCellEditor.getControl();
		overrideDataSourceIdTextControl.setTextLimit(255);
		
		checkboxTableViewer.setCellEditors(new CellEditor[] { null, null, null, null, overrideAppIdTextCellEditor, 
				overrideDataSourceIdTextCellEditor } );
		
		checkboxTableViewer.setColumnProperties(new String[] { "checkBox", APPLICATION_ID, DATA_SOURCE_ID, CELL_NOTE_COUNT, OVERRIDE_APPLICATION_ID, OVERRIDE_DATA_SOURCE_ID });

		attachCellEditors(parent);
		
		checkUncheckAllButton = new Button(container, SWT.CHECK);

		checkUncheckAllButton.setText("Select / Deselect All");
			
		setTitle("Read Me...");
		
		addListeners();
		setMessage("Check the cell notes to be imported for an application id and data source id.  Click in Override Application Id column or Override Data Source Id column to override the Applicaiton Id or the Data Source Id.");
		
		//
		return area;
	}

	private void attachCellEditors(Composite parent) {
		
		if ( checkboxTableViewer != null ) {
		
			checkboxTableViewer.setCellModifier(new ICellModifier() {
	
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
			 */
			public boolean canModify(Object element, String property) {
				
				if ( property.equals(ImportCellNotesDialog.OVERRIDE_DATA_SOURCE_ID) || property.equals(ImportCellNotesDialog.OVERRIDE_APPLICATION_ID)) {
					return true;
				}
		
				return false;
			}
		
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
			 */
			public Object getValue(Object element, String property) {
		
				String value = null;
				
				if ( property.equals(ImportCellNotesDialog.OVERRIDE_APPLICATION_ID) ) {
					
					value = ((CellNotesInformation) element).getOverrideApplicationId();
					
				} else if ( property.equals(ImportCellNotesDialog.OVERRIDE_DATA_SOURCE_ID)) {
					
					value = ((CellNotesInformation) element).getOverrideDataSourceId();
					
				}
				
				if ( value == null ) {
					value = "";
				}
				
				return value;
			}
		
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
			 */
			public void modify(Object element, String property, Object value) {
		
				if ( element instanceof TableItem) {
					
					TableItem item = (TableItem) element;
					
					if ( item.getData() instanceof CellNotesInformation ) {
						
						CellNotesInformation cellNoteInfo = (CellNotesInformation) item.getData();
						
						if ( property.equals(ImportCellNotesDialog.OVERRIDE_APPLICATION_ID)) {
							
							cellNoteInfo.setOverrideApplicationId((String) value);
										
						} else if ( property.equals(ImportCellNotesDialog.OVERRIDE_DATA_SOURCE_ID)) {
							
							cellNoteInfo.setOverrideDataSourceId((String) value);
										
						}
						
						item.setData(cellNoteInfo);
								
						checkboxTableViewer.refresh();
														
					}
					
				}
	
			}
			
		});
			
		}
	}
	/**
	 * 
	 *  Adds listeners to the controls.
	 *
	 */
	private void addListeners() {

		checkUncheckAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				//if check/uncheck button is checked, check all, else uncheck all items in checkbox table viewer
				checkboxTableViewer.setAllChecked(checkUncheckAllButton.getSelection());
								
			}
		});
		
		openFileDialogButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
		
				//get directory dialog
				FileDialog dlg = new FileDialog(openFileDialogButton.getShell(), SWT.OPEN);

				dlg.setFilterNames(new String[] {			
						new String("XML Cell Note Files (*" + PafBaseConstants.XML_EXT + ")"),						
						new String("Binary Cell Note Files (*" + Constants.FILE_EXT_BINARY + ")")
				});
				
				dlg.setFilterExtensions(new String[] { Constants.CELL_NOTE_FILE_NAME + "*" + PafBaseConstants.XML_EXT, Constants.CELL_NOTE_FILE_NAME + "*" + Constants.FILE_EXT_BINARY });
				
				dlg.setText("Browse to Cell Note Import File");
				
				//open dialog
				String importFileLocation = dlg.open();
				
				if( importFileLocation != null ) {
					File importFile = new File(importFileLocation);
					
					//if importFile exists, set text box with value
					if ( importFile != null && importFile.isFile() && importFile.canRead() ) {
						
						importFileTextBox.setText(importFileLocation);
				
						loadSimpleCellNotesFile(importFileLocation);
						
					} 
				}
				
			}
		});
		
			
	}
	
	protected void loadSimpleCellNotesFile(String importFileLocation) {
		
		simpleCellNotsToImportAr = ImportExportUtil.importSimpleCellNotes(importFileLocation);
				
		if ( simpleCellNotsToImportAr != null && simpleCellNotsToImportAr.length > 0 ) {
			
			Map<String, Integer> cellNoteInfoCountMap = new HashMap<String, Integer>();
			
			Map<String, List<SimpleCellNote>> simpleCellNoteMap = new HashMap<String, List<SimpleCellNote>>();
			
			for (SimpleCellNote simpleCellNote: simpleCellNotsToImportAr ) {
				
				String key = simpleCellNote.getApplicationName() + "|" + simpleCellNote.getDataSourceName();
				
				if ( ! cellNoteInfoCountMap.containsKey(key)) {
					
					//initialize counter
					cellNoteInfoCountMap.put(key, new Integer(0));
					
				}
				
				cellNoteInfoCountMap.put(key, cellNoteInfoCountMap.get(key) + 1);

				List<SimpleCellNote> simpleCellNoteList = simpleCellNoteMap.get(key);
				
				if ( simpleCellNoteList == null ) {
					
					simpleCellNoteList = new ArrayList<SimpleCellNote>();
				}
				
				simpleCellNoteList.add(simpleCellNote);
				
				simpleCellNoteMap.put(key, simpleCellNoteList);				
				
				
			}
			
			List<CellNotesInformation> cellNoteInfoList = new ArrayList<CellNotesInformation>();
			
			importedSimpleCellNoteMap = new HashMap<CellNotesInformation, List<SimpleCellNote>>();
			
			for (String key : cellNoteInfoCountMap.keySet()	) {
				
				String[] splitAttributes = key.split("\\|");
				
				String appId = splitAttributes[0];
				String dataSourceId = splitAttributes[1];
				
				Integer countPerAppDataSource = cellNoteInfoCountMap.get(key);
				
				CellNotesInformation cellNoteInfo = new CellNotesInformation();//appId, countPerAppDataSource, dataSourceId, null, null);
				cellNoteInfo.setApplicationId(appId);
				cellNoteInfo.setCellNoteCount(countPerAppDataSource);
				cellNoteInfo.setDataSourceId(dataSourceId);
				
				cellNoteInfoList.add(cellNoteInfo);
				
				importedSimpleCellNoteMap.put(cellNoteInfo, simpleCellNoteMap.get(key));
				
			}
			
			checkboxTableViewer.setInput(cellNoteInfoList.toArray(new CellNotesInformation[0]));
		} else {
			
			MessageDialog.openWarning(this.getShell(), Constants.DIALOG_WARNING_HEADING, "No cell notes were found for the specified file.  There could have been a problem loading the file.  Please check the log file.");
			
		}
		
	}

	/**
	 * 
	 *  Makes sure at least 1 item is checked and there is an output directory.
	 *
	 * @return true if export info is popualted, false if not.
	 */
	private boolean verifyExportInformation() {
	
		//if no items are checked
		if ( checkboxTableViewer.getCheckedElements().length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please check at least one set of cell notes to import.");
			
			return false;
		}
			
		
		//all conditions are now true for a exporting
		return true;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		createButton(parent, IDialogConstants.OK_ID, "Import",
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(667, 511);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Import Cell Notes");
	}
	
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
	
			//if export info isn't complete, return
			if ( ! verifyExportInformation() ) {
				return;
			}
			
			//set values
			this.simpleCellNotesToImport = getCellNotesToImport();
			
			
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 *  Process cell notes to import.
	 *
	 * @return an array of simple cell notes to import
	 */
	private SimpleCellNote[] getCellNotesToImport() {
		
		//create empty list
		List<SimpleCellNote> cellNotesToImportList = new ArrayList<SimpleCellNote>();		
		
		//for each check item in the checkbox table viewer
		for (Object checkedObject : checkboxTableViewer.getCheckedElements()) {
			
			//cast to cell note info
			CellNotesInformation checkedCellNotesInfo = (CellNotesInformation) checkedObject;
			
			//get the simple cell notes to import from the map
			List<SimpleCellNote> simpleCellNotesToImportList = importedSimpleCellNoteMap.get(checkedCellNotesInfo);
			
			//if not null
			if ( simpleCellNotesToImportList != null ) {
				
				//get override app id
				String overrideAppId = checkedCellNotesInfo.getOverrideApplicationId();
				
				//get override data source id
				String overrideDsId = checkedCellNotesInfo.getOverrideDataSourceId();
				
				//if either are not null
				if ( overrideAppId != null ||  overrideDsId != null ) {

					//create temp list
					List<SimpleCellNote> updatedSimpleCellNoteList = new ArrayList<SimpleCellNote>();
					
					//for each cell note to export, check to see if app id or data source id 
					//be included on cell note for update.
					for (SimpleCellNote simpleCellNote : simpleCellNotesToImportList ) {
						
						//if not null and not blank
						if ( overrideAppId != null && overrideAppId.trim().length() != 0 ) {
							
							simpleCellNote.setApplicationName(overrideAppId);
							
						}

						//if not null and not blank
						if ( overrideDsId != null && overrideDsId.trim().length() != 0 ) {
							
							simpleCellNote.setDataSourceName(overrideDsId);
						}
						
						//add updated item to list
						updatedSimpleCellNoteList.add(simpleCellNote);
						
					}
					
					//overwrite import list with this updated one
					simpleCellNotesToImportList = updatedSimpleCellNoteList; 
					
				}
				
				//add to the entire list of cell notes to import
				cellNotesToImportList.addAll(simpleCellNotesToImportList);
				
			}
			
		}
		
		//convert to array and return
		return cellNotesToImportList.toArray(new SimpleCellNote[0]);
	}

	/**
	 * @return the simpleCellNotesToImport
	 */
	public SimpleCellNote[] getSimpleCellNotesToImport() {
		return simpleCellNotesToImport;
	}
			
	/**
	 * @return the checkboxTableViewer
	 */
	public CheckboxTableViewer getCheckboxTableViewer() {
		return checkboxTableViewer;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs.providers;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class StringAscSorter extends ViewerSorter {

	public int compare(Viewer viewer, Object e1, Object e2) {
		
		if ( e1 instanceof String && e2 instanceof String ) {
			
			String str1 = (String) e1;
			
			String str2 = (String) e2;
			
			return str1.compareToIgnoreCase(str2);
		}
		
		return 0;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs.providers;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

import com.pace.admin.servers.nodes.SecurityGroupNode;
import com.pace.admin.servers.nodes.TreeNode;

public class ServerViewTreeViewerSorter extends ViewerSorter {

	public int compare(Viewer viewer, Object e1, Object e2) {
			
		
			//if both security group nodes and both belong to same parent, comapare the name
			if ( e1 instanceof SecurityGroupNode && e2 instanceof SecurityGroupNode 
						&& ((TreeNode) e1).getParent().equals(((TreeNode) e2).getParent() ) ) {
			
				SecurityGroupNode sgn1 = (SecurityGroupNode) e1;
				SecurityGroupNode sgn2 = (SecurityGroupNode) e2;
				
				return sgn1.getName().compareToIgnoreCase(sgn2.getName());				
				
				
			}
			
			Object item1 = e1;
			Object item2 = e2;
			return 0;
			
	}
}
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs.providers;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.model.MemberTagInfo;
import com.pace.base.utility.StringUtils;

/**
 * 
 * Member Tag Info Label Provider.  This class is used to display Member Tag Info 
 * items in the export table viewer.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ExportMemberTagInfoTableLabelProvider extends LabelProvider implements
		ITableLabelProvider {
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	public String getColumnText(Object element, int columnIndex) {

		//if a member tag info object
		if (element instanceof MemberTagInfo) {

			//cast
			MemberTagInfo mti = (MemberTagInfo) element;

			//if not null
			if (mti != null) {

				//return correct value based on index
				switch (columnIndex) {

				case 0:
					return null;
				case 1:
					return mti.getAppId();
				case 2:
					return new Integer(mti.getMemberTagDataItemCount())
							.toString();
				case 3:
					String[] memberTagFilters = mti.getMemberTagFilters(true);

					if (memberTagFilters == null) {
						return null;
					} else {
						return StringUtils
								.arrayToString(mti.getMemberTagFilters(true),
										"", "", "", "", ",");
					}

				case 4:
					
					return null;

				}

			}

		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
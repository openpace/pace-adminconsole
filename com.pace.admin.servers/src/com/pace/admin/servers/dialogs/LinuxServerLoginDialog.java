/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.security.LinuxServerLogin;
import com.pace.admin.servers.security.SecurityManager;

public class LinuxServerLoginDialog extends Dialog {

	private static final Logger logger = Logger.getLogger(LinuxServerLoginDialog.class);
	
	private String pafServerName;
	private String userName;
	private String password;
	
	private Text userNameText;
	private Text passwordText;

	private Button btnCheckButton;
	private Button okButton;
	/**
	 * Create the Login dialog
	 * @param parentShell
	 */
	public LinuxServerLoginDialog(Shell parentShell, String pafServerName) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.pafServerName = pafServerName;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 40;
		gridLayout.marginLeft = 40;
		gridLayout.marginBottom = 20;
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		
		final Label serverLabel = new Label(container, SWT.NONE);
		serverLabel.setText("Server:");

		final Label serverNameLabel = new Label(container, SWT.NONE);
		serverNameLabel.setText(pafServerName);
		
		final Label usernameLabel = new Label(container, SWT.NONE);
		usernameLabel.setText("Username:");

		userNameText = new Text(container, SWT.BORDER);
		userNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateOkButtonState();
			}
		});
		userNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		userNameText.setFocus();
		final Label passwordLabel = new Label(container, SWT.NONE);
		passwordLabel.setText("Password:");

		passwordText = new Text(container, SWT.PASSWORD | SWT.BORDER);
		passwordText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateOkButtonState();
			}
		});
		passwordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);
		
		btnCheckButton = new Button(container, SWT.CHECK);
		btnCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnCheckButton.setText("Remember me");
		
		//
		return container;
	}

	protected void enableLoginDialogFormFields(boolean enable) {

		userNameText.setEnabled(enable);
		
		passwordText.setEnabled(enable);
						
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		okButton.setEnabled(false);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(349, 266);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Login");
	}
	
	protected void buttonPressed(int buttonId) {
		//Save Linux User Login Info to the table
		if (buttonId == IDialogConstants.OK_ID) {
			//Add user info to SecurityMananger.cachedLinuxUserLogin map
			userName = userNameText.getText().trim();
			password = passwordText.getText().trim();
			if( btnCheckButton.getSelection() &&
				SecurityManager.getLinuxServerLogin(pafServerName) == null ) {
				LinuxServerLogin linuxLogin = new LinuxServerLogin(pafServerName,userName,password);
				SecurityManager.addLinuxUser(linuxLogin);
			}
		}
		super.buttonPressed(buttonId);
	}

	protected void updateOkButtonState() {

		if (okButton != null) {
			okButton.setEnabled(isFormDataValid());
		}

	}
	
	private boolean isFormDataValid() {

		if (userNameText.getText().trim().length() == 0) {
			return false;
		}
		if (passwordText.getText().trim().length() == 0) {
			return false;
		}
		return true;
		
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

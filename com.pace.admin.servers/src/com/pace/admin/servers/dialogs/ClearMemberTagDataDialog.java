/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.widgets.StringArraySelectionDialog;
import com.pace.admin.servers.dialogs.providers.ExportMemberTagInfoTableLabelProvider;
import com.pace.admin.servers.dialogs.providers.StandardObjectArrayContenetProvider;

public class ClearMemberTagDataDialog extends TitleAreaDialog {

	private final static Logger logger = Logger.getLogger(ClearMemberTagDataDialog.class);
	
	private Table memberTagInfoTable;
		
	private MemberTagInfo[] memberTagInfoAr;

	private CheckboxTableViewer memberTagInfoTableViewer;
	
	private Map<MemberTagInfo, Button> dynamicButtonsMap = new HashMap<MemberTagInfo, Button>();
	
	private MemberTagInfo[] selectedMemberTagInfos;

	private Button selectDeselectButton;
	
	private String serverName;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param infos 
	 */
	public ClearMemberTagDataDialog(Shell parentShell, String serverName, MemberTagInfo[] memberTagInfoAr) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.serverName = serverName;
		this.memberTagInfoAr = memberTagInfoAr;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		memberTagInfoTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER);
		//memberTagInfoTableViewer.setSorter(new MemberTagInfoSorter());
		memberTagInfoTableViewer.setLabelProvider(new ExportMemberTagInfoTableLabelProvider());
		memberTagInfoTableViewer.setContentProvider(new StandardObjectArrayContenetProvider());
		memberTagInfoTable = memberTagInfoTableViewer.getTable();
		memberTagInfoTable.setHeaderVisible(true);
		memberTagInfoTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(memberTagInfoTable, SWT.NONE);
		newColumnTableColumn.setWidth(21);

		final TableColumn newColumnTableColumn_1 = new TableColumn(memberTagInfoTable, SWT.NONE);
		newColumnTableColumn_1.setWidth(88);
		newColumnTableColumn_1.setText("Application Id");

		final TableColumn newColumnTableColumn_2 = new TableColumn(memberTagInfoTable, SWT.NONE);
		newColumnTableColumn_2.setResizable(false);
		newColumnTableColumn_2.setWidth(132);
		newColumnTableColumn_2.setText("Member Tag Data Count");

		final TableColumn newColumnTableColumn_3 = new TableColumn(memberTagInfoTable, SWT.NONE);
		newColumnTableColumn_3.setResizable(false);
		newColumnTableColumn_3.setWidth(311);
		newColumnTableColumn_3.setText("Filtered Member Tags");

		final TableColumn newColumnTableColumn_4 = new TableColumn(memberTagInfoTable, SWT.NONE);
		newColumnTableColumn_4.setResizable(false);
		newColumnTableColumn_4.setWidth(31);
		memberTagInfoTableViewer.setInput(memberTagInfoAr);

		selectDeselectButton = new Button(container, SWT.CHECK);
		selectDeselectButton.setText(Constants.WIDGET_SELECT_ALL_DESELECT_ALL);
		setTitle("Read Me...");
		setMessage("Clears Member Tag Data on the server by App Id and/or filtered Member Tags.  If no Filters exist, all Member Tag Data items will be cleared for the given App Id.");
		
		addEllipseButtonToTable();
		
		addListeners();
		
		//
		return area;
	}

	private void addEllipseButtonToTable() {

		if ( memberTagInfoAr != null && memberTagInfoAr.length > 0 ) {
			
			//get table items
			TableItem [] items = memberTagInfoTable.getItems();
			
			for (int i = 0; i < memberTagInfoAr.length; i++ ) {
				
//				create new table editor
				TableEditor editor = new TableEditor (memberTagInfoTable);
				
//				new button
				final Button button = new Button (memberTagInfoTable, SWT.PUSH);
				button.setText("...");
				button.setData(new Integer(i));				
				button.pack();
				editor.minimumWidth = button.getSize ().x;
				editor.horizontalAlignment = SWT.RIGHT;
				editor.setEditor (button, items[i], 4);	
				
				final int currentNdx = i;
				
				button.addSelectionListener(new SelectionAdapter() {

					/* (non-Javadoc)
					 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
					 */
					@Override
					public void widgetSelected(SelectionEvent selectionEvent) {

						StringArraySelectionDialog dialog = new StringArraySelectionDialog(button.getShell(), 
								"Member Tag Filter",
								memberTagInfoAr[currentNdx].getMemberTagNames(), 
								memberTagInfoAr[currentNdx].getMemberTagFilters(false));						
						
						if ( dialog.open() == IDialogConstants.OK_ID ) {
							
							String[] selectedMemberTags = dialog.getSelectedItems();
																							
							memberTagInfoAr[currentNdx].setMemberTagFilters(selectedMemberTags);
							
							memberTagInfoTableViewer.refresh();
							
							
						}
						
					}
						
				});
				
				button.setEnabled(false);
				
				dynamicButtonsMap.put(memberTagInfoAr[currentNdx], button);
				
			}
		}
		
	}
	
	/**
	 * 
	 *  Adds listeners to the export widgets/viewers
	 *
	 */
	private void addListeners() {	
		
		/**
		 * Catch when a user checks/unchecks and item in the viewer.  When checked,
		 * enabled the member tag filter button; when unchecked, disable the member
		 * tag filter button (but don't clear the filter members)
		 */
		memberTagInfoTableViewer.addCheckStateListener(new ICheckStateListener() {

			public void checkStateChanged(CheckStateChangedEvent checkEvent) {

				//get source; should be the checkbox table viewer
				Object sourceObject = checkEvent.getSource();

				//get element
				Object element = checkEvent.getElement();
			
				logger.info(checkEvent);
								
				//if source and element are correct classes
				if ( sourceObject instanceof CheckboxTableViewer && element instanceof MemberTagInfo ) {

					//cast
					CheckboxTableViewer viewer = (CheckboxTableViewer) sourceObject;
					
					//cast
					MemberTagInfo memberTagInfo = (MemberTagInfo) element;
					
					//if the dynamic button map contains the member tag, get button and set enabled
					if ( dynamicButtonsMap.containsKey(memberTagInfo )) {
						
						//get button from map
						Button button = dynamicButtonsMap.get(memberTagInfo);
						
						//if button is not null and button is not disposed
						if ( button != null && ! button.isDisposed() ) {
							
							button.setEnabled(viewer.getChecked(element));
							
						}
						
					}
					
				}				
				
			}
			
		});
		
		/**
		 * When checked, check all items in checkbox table viewer and enabled
		 * all member tag filter buttons.
		 *  
		 * When unchecked, uncheck all items in checkbox table viewer and disable 
		 * all member tag filter buttons.
		 */
		selectDeselectButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			
				memberTagInfoTableViewer.setAllChecked(selectDeselectButton.getSelection());
				
				//enabled all or disable all member tag filter buttons based on selection
				for (Button button : dynamicButtonsMap.values()) {
					
					button.setEnabled(selectDeselectButton.getSelection());
					
				}
				
			}
			
		});
				
	}
	
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(650, 468);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Clear Member Tag Data");
	}

	/**
	 * @return the selectedMemberTagInfos
	 */
	public MemberTagInfo[] getSelectedMemberTagInfos() {
		return selectedMemberTagInfos;
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			
			if ( ! verifyInputInformation() ) {
				return;
			}
			
			selectedMemberTagInfos = Arrays.asList(memberTagInfoTableViewer.getCheckedElements()).toArray(new MemberTagInfo[0]);
			
		}
		super.buttonPressed(buttonId);
	}

	private boolean verifyInputInformation() {

		String url = PafServerUtil.getServerWebserviceUrl(serverName);
		
		//if server is running
		if ( ! ServerMonitor.getInstance().isServerRunning(url) ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Server problem: Member Tag Data can not be cleared if the server is not running.");
			
			return false;
		}
		
		//if no items are checked
		if ( memberTagInfoTableViewer.getCheckedElements().length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please check at least one item to clear.");
			
			return false;
		}
		
		return true;
	}

}

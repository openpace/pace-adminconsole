/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.StringUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.nodes.GroupNode;
import com.pace.base.server.ServerPlatform;
import com.pace.base.ui.PafServer;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.PafServerAck;

/**
 * 
 * Server dialog used to manage paf servers.
 * 
 * @version x.xx
 * @author jmilliron
 * 
 */
public class ServerDialog extends Dialog {

	private final static int MIN_PORT_VALUE = 80;
	private final static int MAX_PORT_VALUE = 65535;
	
	private Label wsdlServiceNameLabel;

	private Label webApplicationNameLabel;

	private Label httpPortLabel;

	private Label hostLabel;

	private Label serverNameLabel;

	private Text completeWsdlUrlTextBox;
	
	private Text testApplicationUrlTextBox;

	private Text serverStartupTimeoutInMillisecondsTextBox;

	private Text urlTimeoutInMillisecondsTextBox;

	private Combo svrGroupCombo;

	private Text shutdownFileNameTextBox;

	private Text startupFileNameTextBox;

	private Combo defaultServerCombo;

	private Text wsdlServiceNameTextBox;

	private Text webApplicationNameTextBox;

	private Text httpPortTextBox;

	private Text hostTextBox;

	private Text serverNameTextBox;

	private Button startupFileNameBrowseButton;

	private Button shutdownFileNameBrowseButton;

	private Button validateButton;
	
	private Button validateApplicationButton;

	private Button okButton;

	private Label errorMessage;

	private PafServer initialPafServer;

	private PafServer pafServer;

	private boolean isNew;

	private Set<String> serverNameSet = new TreeSet<String>(
			String.CASE_INSENSITIVE_ORDER);
	
	
	private Label svrGroupNameLabel;
	private Button buttonPromptOnDeploy;
	private Label lblDoNotPrompt;
	private Combo useSsl;
	private Label labelUseSsl;
	private Label lblOsServiceName;
	private Text osServiceNameTxtBox;
	private Label lblMessagingPort;
	private Text messagePortTxtBox;
	private Label lblServerPlatform;
	private Combo cmbServerPlatform;
	private List<String> serverGroups;
	private Label lblEnabled;
	private Button buttonEnabled;
	
	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 *            Parent shell
	 * @param initialPafServer
	 *            Initial server (existing server) or clone
	 */
	public ServerDialog(Shell parentShell, PafServer initialPafServer) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.initialPafServer = initialPafServer;

		if (initialPafServer != null) {

			// try to clone
			try {
				pafServer = (PafServer) initialPafServer.clone();
			} catch (CloneNotSupportedException e) {
			}

		}

		// if server is null, create new instance and set default jdni
		if (pafServer == null) {

			pafServer = new PafServer();
			
		}

		// get set of servers
		Set<PafServer> pafServers = PafServerUtil.getPafServers();

		//Get the set of server groups
		serverGroups = PafServerUtil.getServerGroups();
		
		
		// if servers exists
		if (pafServers != null && pafServers.size() > 0) {

			// loop thorugh and add name of server to sever name set
			for (PafServer tmpPafServer : pafServers) {

				if (tmpPafServer != null && tmpPafServer.getName() != null) {

					serverNameSet.add(tmpPafServer.getName());

				}

			}

		}

	}

	/**
	 * 
	 * Creates the dialog
	 * 
	 * @param parentShell
	 *            Parent shell
	 * @param initialPafServer
	 *            Initial server (existing server) or clone
	 * @param isNew
	 *            If server is new
	 * @wbp.parser.constructor
	 */
	public ServerDialog(Shell parentShell, PafServer initialPafServer, boolean isNew) {
		this(parentShell, initialPafServer);
		this.isNew = isNew;
	}

	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginBottom = 25;
		gridLayout.marginRight = 15;
		gridLayout.marginLeft = 15;
		gridLayout.marginTop = 10;
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);

		errorMessage = new Label(container, SWT.NONE);
		errorMessage.setAlignment(SWT.CENTER);
		errorMessage.setForeground(Display.getCurrent().getSystemColor(
				SWT.COLOR_RED));
		errorMessage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				false, 2, 1));

		serverNameLabel = new Label(container, SWT.NONE);
		serverNameLabel.setText("Server Name");

		serverNameTextBox = new Text(container, SWT.BORDER);

		serverNameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false));

		hostLabel = new Label(container, SWT.NONE);
		hostLabel.setText("Host (Computer Name)");

		hostTextBox = new Text(container, SWT.BORDER);

		hostTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));

		httpPortLabel = new Label(container, SWT.NONE);
		httpPortLabel.setText("HTTP Port");

		httpPortTextBox = new Text(container, SWT.BORDER);
		httpPortTextBox.setTextLimit(5);

		httpPortTextBox.setLayoutData(new GridData(60, SWT.DEFAULT));

		webApplicationNameLabel = new Label(container, SWT.NONE);
		webApplicationNameLabel.setText("Web Application Name");

		webApplicationNameTextBox = new Text(container, SWT.BORDER);

		webApplicationNameTextBox.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false));

		wsdlServiceNameLabel = new Label(container, SWT.NONE);
		wsdlServiceNameLabel.setText("Service Name");

		wsdlServiceNameTextBox = new Text(container, SWT.BORDER);

		wsdlServiceNameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false));

		final Label defaultServerLabel = new Label(container, SWT.NONE);
		defaultServerLabel.setText("Default Server");

		defaultServerCombo = new Combo(container, SWT.READ_ONLY);
		defaultServerCombo.setItems(new String[] { "False", "True" });
		defaultServerCombo.select(0);
		defaultServerCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				true, false));
		
		labelUseSsl = new Label(container, SWT.NONE);
		labelUseSsl.setText("Use HTTPS");
		
		useSsl = new Combo(container, SWT.READ_ONLY);
		useSsl.setItems(new String[] {"False", "True"});
		useSsl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		useSsl.select(0);
		
		lblServerPlatform = new Label(container, SWT.NONE);
		lblServerPlatform.setText("Server Platform");
		
		cmbServerPlatform = new Combo(container, SWT.READ_ONLY);
		cmbServerPlatform.setItems(new String[] {"Windows", "Linux"});
		cmbServerPlatform.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		cmbServerPlatform.select(0);
		
		lblOsServiceName = new Label(container, SWT.NONE);
		lblOsServiceName.setText("OS Service Name");
		lblOsServiceName.setToolTipText("Default service name is " + Constants.DEFAULT_SERVER_SERVICE_NAME);
		
		osServiceNameTxtBox = new Text(container, SWT.BORDER);
		osServiceNameTxtBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false));

		
		lblMessagingPort = new Label(container, SWT.NONE);
		lblMessagingPort.setText("Messaging Port");
		lblMessagingPort.setToolTipText("Default messaging port is " + Constants.DEFAULT_SERVER_JMS_MESSAGING_PORT);
		
		messagePortTxtBox = new Text(container, SWT.BORDER);
		messagePortTxtBox.setTextLimit(5);
		messagePortTxtBox.setLayoutData(new GridData(60, SWT.DEFAULT));
		
		lblEnabled = new Label(container, SWT.NONE);
		lblEnabled.setText("Enabled");
		
		buttonEnabled = new Button(container, SWT.CHECK);
		buttonEnabled.setSelection(true);	// TTN-2732 - Set Enabled box as default when adding new Server Connector

		final Group optionalSettingsGroup = new Group(container, SWT.NONE);
		optionalSettingsGroup.setText("Optional Settings");
		optionalSettingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true, false, 2, 1));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.numColumns = 3;
		optionalSettingsGroup.setLayout(gridLayout_2);

		final Label startupFileNameLabel = new Label(optionalSettingsGroup,
				SWT.NONE);
		startupFileNameLabel.setText("Startup File Name");
		startupFileNameLabel.setToolTipText("File must be located in \"" + Constants.ADMIN_CONSOLE_CONF_DIRECTORY + "\" directory.");

		startupFileNameTextBox = new Text(optionalSettingsGroup, SWT.BORDER);
		startupFileNameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false));

		startupFileNameBrowseButton = new Button(optionalSettingsGroup,
				SWT.NONE);

		startupFileNameBrowseButton.setText("Browse...");

		final Label shutdownFileNameLabel = new Label(optionalSettingsGroup,
				SWT.NONE);
		shutdownFileNameLabel.setText("Shutdown File Name");
		shutdownFileNameLabel.setToolTipText("File must be located in \"" + Constants.ADMIN_CONSOLE_CONF_DIRECTORY + "\" directory.");
		
		shutdownFileNameTextBox = new Text(optionalSettingsGroup, SWT.BORDER);
		shutdownFileNameTextBox.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false));

		shutdownFileNameBrowseButton = new Button(optionalSettingsGroup,
				SWT.NONE);
		shutdownFileNameBrowseButton.setText("Browse...");

		svrGroupNameLabel = new Label(optionalSettingsGroup, SWT.NONE);
		svrGroupNameLabel.setText("Server Group Name");

		svrGroupCombo = new Combo(optionalSettingsGroup, SWT.BORDER);
		
		svrGroupCombo.setTextLimit(20);
		EditorControlUtil.addItems(svrGroupCombo, serverGroups);

		GridData gd_svrGroupCombo = new GridData(100, SWT.DEFAULT);
		gd_svrGroupCombo.grabExcessHorizontalSpace = true;
		gd_svrGroupCombo.horizontalAlignment = SWT.FILL;
		svrGroupCombo.setLayoutData(gd_svrGroupCombo);
		new Label(optionalSettingsGroup, SWT.NONE);

		final Label urlTimeoutInLabel = new Label(optionalSettingsGroup,
				SWT.NONE);
		urlTimeoutInLabel.setText("URL Timeout In Milliseconds");

		urlTimeoutInMillisecondsTextBox = new Text(optionalSettingsGroup,
				SWT.BORDER);

		urlTimeoutInMillisecondsTextBox.setTextLimit(10);
		urlTimeoutInMillisecondsTextBox.setLayoutData(new GridData(115,
				SWT.DEFAULT));
		new Label(optionalSettingsGroup, SWT.NONE);

		final Label serverStartupTimeLabel = new Label(optionalSettingsGroup,
				SWT.NONE);
		serverStartupTimeLabel.setText("Startup Time In Milliseconds");

		serverStartupTimeoutInMillisecondsTextBox = new Text(
				optionalSettingsGroup, SWT.BORDER);

		serverStartupTimeoutInMillisecondsTextBox.setTextLimit(10);
		serverStartupTimeoutInMillisecondsTextBox.setLayoutData(new GridData(
				115, SWT.DEFAULT));
		new Label(optionalSettingsGroup, SWT.NONE);
		
		lblDoNotPrompt = new Label(optionalSettingsGroup, SWT.NONE);
		lblDoNotPrompt.setText("Do not prompt on upload");
		
		buttonPromptOnDeploy = new Button(optionalSettingsGroup, SWT.CHECK);
		new Label(optionalSettingsGroup, SWT.NONE);
		
		// Test server connection
		final Label completeWsdlUrlLabel = new Label(container, SWT.NONE);
		completeWsdlUrlLabel.setText("Server Connection URL");

		completeWsdlUrlTextBox = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		GridData gd_completeWsdlUrlTextBox = new GridData(SWT.FILL, SWT.CENTER,
				true, false);
		gd_completeWsdlUrlTextBox.widthHint = 186;
		completeWsdlUrlTextBox.setLayoutData(gd_completeWsdlUrlTextBox);
		new Label(container, SWT.NONE);

		validateButton = new Button(container, SWT.NONE);
		validateButton.setEnabled(false);
		validateButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false));
		validateButton.setText("Test Server Connection");
		
		
		// TTN-2345 - button to test application url		
		final Label testApplicationUrlLabel = new Label(container, SWT.NONE);
		testApplicationUrlLabel.setText("Application Connection URL");
		testApplicationUrlLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		testApplicationUrlTextBox = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		GridData gd_testApplicationUrlTextBox = new GridData(SWT.FILL, SWT.CENTER,
				true, false);
		gd_testApplicationUrlTextBox.widthHint = 186;
		testApplicationUrlTextBox.setLayoutData(gd_testApplicationUrlTextBox);
		new Label(container, SWT.NONE);

		validateApplicationButton = new Button(container, SWT.NONE);
		validateApplicationButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false));
		validateApplicationButton.setEnabled(true);
		validateApplicationButton.setText("Test Application Connection");
		
		addListeners();

		populateControls();

		return container;
	}

	/**
	 * 
	 * Adds listeners to the controls.
	 * 
	 */
	private void addListeners() {

		// server modify listener
		serverNameTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				pafServer.setName(serverNameTextBox.getText().trim()); // TTN-2715 - update paf server name for application validation
				updateOkButtonState();
			}
		});

		// host modify listener
		hostTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				pafServer.setHost(hostTextBox.getText().trim());
				refreshWsdlURLTextBox();
				refreshServiceNameURLTextBox();
				updateOkButtonState();
				updateTestConnectionButtonState();
			}
		});

		// http verify listener
		httpPortTextBox.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyOnlyPositiveInt(e);
			}
		});

		// http modify listener
		httpPortTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {

				pafServer.setPort(getIntegerFromTextBox(httpPortTextBox));
				refreshWsdlURLTextBox();
				refreshServiceNameURLTextBox();
				updateOkButtonState();
				updateTestConnectionButtonState();
			}
		});
		
		// message port verify listener
		messagePortTxtBox.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyOnlyPositiveInt(e);
			}
		});

		// message port modify listener
		messagePortTxtBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {

				pafServer.setJmsMessagingPort(getIntegerFromTextBox(messagePortTxtBox));
				updateOkButtonState();

			}
		});

		
		useSsl.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				pafServer.setHttps(new Boolean(useSsl.getText().toLowerCase()));
				refreshWsdlURLTextBox();
				refreshServiceNameURLTextBox();
			}
		});
		
		// web app modify listener
		webApplicationNameTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {

				pafServer.setWebappName(webApplicationNameTextBox.getText()
						.trim());
				refreshWsdlURLTextBox();
				refreshServiceNameURLTextBox();
				updateOkButtonState();
				updateTestConnectionButtonState();
			}
		});

		// wsdl service name modify listener
		wsdlServiceNameTextBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				pafServer.setWsdlServiceName(wsdlServiceNameTextBox.getText()
						.trim());
				refreshWsdlURLTextBox();
				refreshServiceNameURLTextBox();
				updateOkButtonState();
				updateTestConnectionButtonState();
			}
		});
		
		cmbServerPlatform.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				EditorControlUtil.setText(osServiceNameTxtBox, Constants.DEFAULT_SERVER_SERVICE_NAME );
				updateOkButtonState();
			}
		});
		
		// os service name modify listener
		osServiceNameTxtBox.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				pafServer.setOsServiceName(osServiceNameTxtBox.getText()
						.trim());
				//refreshOsServiceNameTextBox();
				updateOkButtonState();
			}
		});

		// startup file name modify listener
		startupFileNameBrowseButton
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {

						openFileDialog(startupFileNameTextBox);

					}
				});

		// shutdown file name modify listener
		shutdownFileNameBrowseButton
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {

						openFileDialog(shutdownFileNameTextBox);

					}
				});
		
		// url timeout verify listener
		urlTimeoutInMillisecondsTextBox.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				verifyOnlyPositiveInt(e);
			}
		});

		// server startup timeout verify listener
		serverStartupTimeoutInMillisecondsTextBox
				.addVerifyListener(new VerifyListener() {
					public void verifyText(VerifyEvent e) {
						verifyOnlyPositiveInt(e);
					}
				});

		
		// validates connection to build wsdl
		validateButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {

				// use an hourglass and start up the tuples dialog
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
						new Runnable() {

							public void run() {

								boolean isUrlActive = false;
								String url = pafServer.getCompleteServerInitServletUrl();
								
								// try to connect to url
								try {
									isUrlActive = pafServer.isTheServerRunning();
								} catch (RuntimeException re) {
									re.printStackTrace();
								}

								// if success, prompt success, if not, prompt
								// failure
								if (isUrlActive) {

									GUIUtil.openMessageWindow(
											"Successful Connection!",
											"Successfully connected to pace server: "
													+ url + ".",
											SWT.ICON_INFORMATION);

								} else {

									GUIUtil
											.openMessageWindow(
													Constants.DIALOG_ERROR_HEADING,
													"Could not successfully connect to pace server: "
															+ url
															+ "! The server might not be running.",
													SWT.ICON_ERROR);
								}

							}
						});
			}
		});
		
		// TTN-2345 - validates application connection
		validateApplicationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				// use an hourglass and start up the tuples dialog
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
						new Runnable() {
							public void run() {
								try {
									// test to make sure server is running
									boolean isUrlActive = false;	
									String wsdlServiceUrl = pafServer.getCompleteWSDLService();
									// try to connect to server url
									isUrlActive = pafServer.isTheServerRunning();
									pafServer.isTheServerRunning();
									
									if (isUrlActive) {
										
										// test application connection
										boolean applicationConnectionSuccess = pafServer.testApplicationConnection(wsdlServiceUrl);
										
										if (applicationConnectionSuccess){
											
											// check that application on current server is running
											PafServerUtil.addPafServer(pafServer); // TTN- 2715 - fix test application button when server is first created
											ApplicationState appState = ServerMonitor.getInstance().getApplicationState(pafServer);
											
											if (pafServer.isDisabled()) {
												GUIUtil.openMessageWindow(
														Constants.DIALOG_WARNING_HEADING,
														"Please enable this Server Connector and reopen it,"
														+ " in order to Test Application: "
																+ wsdlServiceUrl, SWT.ICON_ERROR);
											}
											
											else if (appState != null) {
												displayApplicationVerificationMessage(appState, wsdlServiceUrl);
											}	
											
											else {
												GUIUtil.openMessageWindow(
														Constants.DIALOG_ERROR_HEADING,
														"Could not successfully connect to pace application: "
																+ wsdlServiceUrl, SWT.ICON_ERROR);
											}
											

										} else {
											GUIUtil.openMessageWindow(
													Constants.DIALOG_ERROR_HEADING,
													"Could not successfully connect to pace application: "
															+ wsdlServiceUrl, SWT.ICON_ERROR);
										}
										
									// server is not running
									} else {
										GUIUtil.openMessageWindow(
													Constants.DIALOG_ERROR_HEADING,
													"Could not successfully connect to pace server: "
															+ wsdlServiceUrl
															+ "! The server might not be running.",
													SWT.ICON_ERROR);
									}
								} catch (RuntimeException re) {
									re.printStackTrace();
								}   
							}
						});
			}
		});
		
		//svrGroupCombo.add
		svrGroupCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateOkButtonState();
			}
		});
	}

	/**
	 * 
	 * Gets the integer from the text box
	 * 
	 * @param textBox
	 * @return
	 */
	protected Integer getIntegerFromTextBox(Text textBox) {

		Integer intValue = null;

		try {
			intValue = new Integer(textBox.getText().trim());
		} catch (NumberFormatException nfe) {
		}

		return intValue;
	}

	/**
	 * 
	 * refreshes the text in the wsld text box
	 * 
	 */
	protected void refreshWsdlURLTextBox() {

		EditorControlUtil.setText(completeWsdlUrlTextBox, pafServer.getCompleteServerInitServletUrl());

	}
	
	protected void refreshOsServiceNameTextBox() {

		EditorControlUtil.setText(osServiceNameTxtBox, pafServer.getOsServiceName());

	}
	
	protected void refreshServiceNameURLTextBox() {
		
		EditorControlUtil.setText(testApplicationUrlTextBox, pafServer.getCompleteWSDLService());
	}

	/**
	 * 
	 * Opens the file dialog box
	 * 
	 * @param textBox
	 */
	protected void openFileDialog(Text textBox) {

		FileDialog fileDialog = new FileDialog(textBox.getShell());

		fileDialog.setText("Select file...");
		
		fileDialog.setFilterExtensions(new String[] { "*.bat" } );

//		fileDialog.setFilterPath(serverHomeDirectoryTextBox.getText());

		String fullFileName = fileDialog.open();

		if (fullFileName != null) {

			//int ndx = fullFileName.lastIndexOf(File.separator);

			//String fileName = fullFileName.substring(ndx + 1);

			EditorControlUtil.setText(textBox, fullFileName);

		}

	}

	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);

		updateOkButtonState();

		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(570, 870);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Pace Server Connector");
	}

	/**
	 * 
	 * Populates the controls from the initial model
	 * 
	 */
	private void populateControls() {

		serverNameTextBox.setEnabled(isNew);

		if (initialPafServer != null) {

			EditorControlUtil.setText(serverNameTextBox, initialPafServer
					.getName());

			EditorControlUtil.setText(hostTextBox, initialPafServer.getHost());
//			EditorControlUtil.setText(serverHomeDirectoryTextBox,
//					initialPafServer.getHomeDirectory());

			if (initialPafServer.getPort() != null) {
				EditorControlUtil.setText(httpPortTextBox, initialPafServer
						.getPort().toString());
			}

			EditorControlUtil.setText(webApplicationNameTextBox,
					initialPafServer.getWebappName());
			EditorControlUtil.setText(wsdlServiceNameTextBox, initialPafServer
					.getWsdlServiceName());
			EditorControlUtil.selectItem(defaultServerCombo, StringUtil
					.initCap(Boolean.toString(initialPafServer
							.isDefaultServer())));
			EditorControlUtil.selectItem(useSsl, StringUtil
					.initCap(Boolean.toString(initialPafServer.isHttps())));
			EditorControlUtil.setText(startupFileNameTextBox, initialPafServer
					.getStartupFile());
			EditorControlUtil.setText(shutdownFileNameTextBox, initialPafServer
					.getShutdownFile());
			
			this.buttonEnabled.setSelection(!initialPafServer.isDisabled());
			
			//os service name
			if ( initialPafServer.getServerPlatform() == null ) {
				String url = initialPafServer.getCompleteWSDLService();
				PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
				if( pafServerAck == null ) {
					EditorControlUtil.selectItem(cmbServerPlatform, StringUtil.initCap("Windows"));
					EditorControlUtil.setText(osServiceNameTxtBox, Constants.DEFAULT_SERVER_SERVICE_NAME);
				}
				else { //pafServerAck != null
					if( pafServerAck.getPlatform().indexOf("Windows") > 0 ) {
						EditorControlUtil.selectItem(cmbServerPlatform, StringUtil.initCap("Windows"));
					}
					else if( pafServerAck.getPlatform().indexOf("Linux") > 0 ) {
						EditorControlUtil.selectItem(cmbServerPlatform, StringUtil.initCap("Linux"));
					}
					
					if ( initialPafServer.getOsServiceName() == null || initialPafServer.getOsServiceName().isEmpty() ) {
						initialPafServer.setOsServiceName(Constants.DEFAULT_SERVER_SERVICE_NAME);
					} 
					else {
						EditorControlUtil.setText(osServiceNameTxtBox, initialPafServer.getOsServiceName());
					}
				}
			}
			else { 	//initialPafServer.getServerPlatform() != null 
				if( initialPafServer.getServerPlatform().equals(ServerPlatform.Windows)) {
					EditorControlUtil.selectItem(cmbServerPlatform, StringUtil.initCap("Windows"));
				}
				else if( initialPafServer.getServerPlatform().equals(ServerPlatform.Linux)) {
					EditorControlUtil.selectItem(cmbServerPlatform, StringUtil.initCap("Linux"));
				}
				
				if ( initialPafServer.getOsServiceName() == null || initialPafServer.getOsServiceName().isEmpty() ) {
					initialPafServer.setOsServiceName(Constants.DEFAULT_SERVER_SERVICE_NAME);
				} 
				else {
					EditorControlUtil.setText(osServiceNameTxtBox, initialPafServer.getOsServiceName());
				}
			}
			
			if ( initialPafServer.getJmsMessagingPort() == null ) {
				initialPafServer.setJmsMessagingPort(Constants.DEFAULT_SERVER_JMS_MESSAGING_PORT);
			}
			
			EditorControlUtil.setText(messagePortTxtBox, initialPafServer
					.getJmsMessagingPort().toString());

			if (initialPafServer.getServerGroupName() != null) {
				EditorControlUtil.selectItem(svrGroupCombo, initialPafServer.getServerGroupName());
			} 

			if (initialPafServer.getUrlTimeoutInMilliseconds() != null) {
				EditorControlUtil.setText(urlTimeoutInMillisecondsTextBox,
						initialPafServer.getUrlTimeoutInMilliseconds()
								.toString());
			}

			if (initialPafServer.getServerStartupTimeoutInMilliseconds() != null) {
				EditorControlUtil.setText(
						serverStartupTimeoutInMillisecondsTextBox,
						initialPafServer
								.getServerStartupTimeoutInMilliseconds()
								.toString());
			}

			EditorControlUtil.setText(completeWsdlUrlTextBox, initialPafServer.getCompleteServerInitServletUrl());
			EditorControlUtil.setText(testApplicationUrlTextBox, initialPafServer.getCompleteWSDLService());
			
			EditorControlUtil.checkButton(buttonPromptOnDeploy, initialPafServer.isDoesNotPromptOnHotDeploy());

		} else {
			
			//set defaults for new server config
			EditorControlUtil.setText(osServiceNameTxtBox, Constants.DEFAULT_SERVER_SERVICE_NAME);
			EditorControlUtil.setText(messagePortTxtBox, Constants.DEFAULT_SERVER_JMS_MESSAGING_PORT.toString());
			EditorControlUtil.setText(completeWsdlUrlTextBox, "");
			EditorControlUtil.setText(testApplicationUrlTextBox, "");
			
		}
		
	}

	/**
	 * 
	 * Verify event
	 * 
	 * @param event
	 */
	private void verifyOnlyPositiveInt(VerifyEvent event) {

		event.doit = EditorControlUtil.isPositiveNumber(event.text);

	}

	/**
	 * @return the pafServer
	 */
	public PafServer getPafServer() {
		return pafServer;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {

			saveChanges();

		}
		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 * Populates the paf server from controls.
	 *
	 */
	private void saveChanges() {

		pafServer.setName(serverNameTextBox.getText().trim());
		pafServer.setHost(hostTextBox.getText().trim());

//		String serverHomeDir = serverHomeDirectoryTextBox.getText().trim();
//
//		if (!serverHomeDir.endsWith(File.separator)) {
//			serverHomeDir += File.separator;
//		}
//
//		pafServer.setHomeDirectory(serverHomeDir);

		pafServer
				.setPort(getIntegerFromString(httpPortTextBox.getText().trim()));

		pafServer.setWebappName(webApplicationNameTextBox.getText().trim());
		pafServer.setWsdlServiceName(wsdlServiceNameTextBox.getText().trim());
		pafServer.setServerPlatform(ServerPlatform.valueOf(cmbServerPlatform.getText()));
		pafServer.setOsServiceName(osServiceNameTxtBox.getText().trim());
		pafServer.setJmsMessagingPort(getIntegerFromString(messagePortTxtBox.getText().trim()));
		
		String booleanStringVal = defaultServerCombo.getText().toLowerCase();

		boolean defaultServer = new Boolean(booleanStringVal);

		pafServer.setDefaultServer(defaultServer);

		if (startupFileNameTextBox.getText().trim().length() == 0) {

			pafServer.setStartupFile(null);

		} else {

			pafServer.setStartupFile(startupFileNameTextBox.getText().trim());

		}

		if (shutdownFileNameTextBox.getText().trim().length() == 0) {

			pafServer.setShutdownFile(null);

		} else {

			pafServer.setShutdownFile(shutdownFileNameTextBox.getText().trim());

		}
		

		//pafServer.setJndiPort(getIntegerFromString(svrGroupCombo.getText().trim()));
		if(svrGroupCombo.getText() != null){
			String group = svrGroupCombo.getText().trim();
			pafServer.setServerGroupName(group);
		}
		
		
		pafServer
				.setUrlTimeoutInMilliseconds(getLongFromString(urlTimeoutInMillisecondsTextBox
						.getText().trim()));

		pafServer
				.setServerStartupTimeoutInMilliseconds(getLongFromString(serverStartupTimeoutInMillisecondsTextBox
						.getText().trim()));
		
		pafServer.setDoesNotPromptOnHotDeploy(buttonPromptOnDeploy.getSelection());
		
		pafServer.setHttps( new Boolean(useSsl.getText().toLowerCase()));
		
		pafServer.setDisabled(!buttonEnabled.getSelection());

	}

	/**
	 * 
	 * Creates an Integer from the string input
	 *  
	 * @param input
	 * @return
	 */
	private Integer getIntegerFromString(String input) {

		Integer integer = null;

		if (input != null && input.length() > 0) {

			try {

				integer = Integer.parseInt(input);

			} catch (Exception e) {

			}
		}
		return integer;
	}

	/**
	 * 
	 * Creates a Long from the string input
	 * 
	 * @param input
	 * @return
	 */
	private Long getLongFromString(String input) {

		Long longVar = null;

		if (input != null && input.length() > 0) {

			try {

				longVar = Long.parseLong(input);

			} catch (Exception e) {

			}
		}
		return longVar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		// TODO Auto-generated method stub
		super.create();

	}

	/**
	 * 
	 * Enables/Disables ok button
	 * 
	 */
	protected void updateOkButtonState() {

		if (okButton != null) {
			okButton.setEnabled(isFormDataValid());
		}

	}

	protected void updateTestConnectionButtonState() {
		if( hostTextBox.getText() != null && hostTextBox.getText().trim().length() != 0
				&& httpPortTextBox.getText() != null && httpPortTextBox.getText().trim().length() != 0 
				&& wsdlServiceNameTextBox.getText() != null && wsdlServiceNameTextBox.getText().trim().length() != 0
				&& webApplicationNameTextBox.getText() != null && webApplicationNameTextBox.getText().trim().length() != 0 ){
			validateButton.setEnabled(true);
			validateApplicationButton.setEnabled(true);
		}
		else {
			validateButton.setEnabled(false);
			validateApplicationButton.setEnabled(false);
		}
	}
	
	/**
	 * 
	 * Validates form data
	 * 
	 * @return true if form data is all valid.
	 */
	private boolean isFormDataValid() {

		errorMessage.setText("");

		if (isNew) {

			String newServerName = serverNameTextBox.getText().trim();

			if (newServerName.length() == 0) {
				
				errorMessage.setText(serverNameLabel.getText()
						+ " is a required field.");
				return false;

			} else if (serverNameSet.contains(newServerName)) {

				errorMessage.setText("Server name '" + newServerName
						+ "' already exists.  Please rename.");
				return false;
			}

		}

		if (hostTextBox.getText().trim().length() == 0) {
			errorMessage.setText(hostLabel.getText() + " is a required field.");
			return false;
		}

		if(svrGroupCombo != null && svrGroupCombo.getText() != null){
			
			String groupName = svrGroupCombo.getText().trim();
			if(groupName.equalsIgnoreCase(GroupNode.BLANK_GROUP_NAME)){
				errorMessage.setText("Invalid server group name.");
				return false;
			}
			
		}
		
//		if (serverHomeDirectoryTextBox.getText().trim().length() == 0) {
//			errorMessage.setText(serverHomeDirectoryLabel.getText()
//					+ " is a required field.");
//			return false;
//		}

		if (httpPortTextBox.getText().trim().length() == 0) {
			errorMessage.setText(httpPortLabel.getText()
					+ " is a required field.");
			return false;
		}
		
		if ( ! isPortValid(httpPortTextBox.getText().trim())) {
			
			errorMessage.setText(httpPortLabel.getText() + " must be between " + MIN_PORT_VALUE + " and " + MAX_PORT_VALUE + ".");
			return false;
			
		}

		if (webApplicationNameTextBox.getText().trim().length() == 0) {
			errorMessage.setText(webApplicationNameLabel.getText()
					+ " is a required field.");
			return false;
		}

		if (wsdlServiceNameTextBox.getText().trim().length() == 0) {
			errorMessage.setText(wsdlServiceNameLabel.getText()
					+ " is a required field.");
			return false;
		}
		
		if( initialPafServer != null && initialPafServer.getServerPlatform() != null 
				&& initialPafServer.getServerPlatform().equals(ServerPlatform.Windows) ) {
			if ( osServiceNameTxtBox.getText().trim().length() == 0) {
				errorMessage.setText(lblOsServiceName.getText()
						+ " is a required field.");
				return false;
			}
		}
		
		if ( messagePortTxtBox.getText().trim().length() == 0) {
			errorMessage.setText(lblMessagingPort.getText()
					+ " is a required field.");
			return false;
		}

//		if ( svrGroupCombo.getText().trim().length() > 0 && ! isPortValid(svrGroupCombo.getText().trim())) {
//			
//			errorMessage.setText(svrGroupNameLabel.getText() + " must be between " + MIN_PORT_VALUE + " and " + MAX_PORT_VALUE + ".");
//			return false;
//			
//		}

		return true;

	}

	private boolean isPortValid(String port) {
		
		boolean isPortValid = false;
		
		try {
			
			int portValue = Integer.parseInt(port);
			
			if ( portValue >= MIN_PORT_VALUE && portValue <= MAX_PORT_VALUE	) {
				
				isPortValid = true;
				
			}
		
		} catch (RuntimeException re ) {
			//do nothing			
		}
		
		
		return isPortValid;
	}

	/**
	 * Method_description_goes_here
	 * 
	 * @return serverNameLabel
	 */
	protected Label getServerNameLabel() {
		return serverNameLabel;
	}

	/**
	 *  Method_description_goes_here
	 * 
	 * @return hostLabel
	 */
	protected Label getHostLabel() {
		return hostLabel;
	}

	/**
	 *  Method_description_goes_here
	 *
	 * @return serverHomeDirectoryLabel
	 */
//	protected Label getServerHomeDirectoryLabel() {
//		return serverHomeDirectoryLabel;
//	}

	/**
	 *  Method_description_goes_here
	 *
	 * @return httpPortLabel
	 */ 
	protected Label getHttpPortLabel() {
		return httpPortLabel;
	}

	/**
	 *  Method_description_goes_here
	 *
	 * @return webApplicationNameLabel
	 */
	protected Label getWebApplicationNameLabel() {
		return webApplicationNameLabel;
	}

	/**
	 *  Method_description_goes_here
	 *
	 * @return wsdlServiceNameLabel
	 */
	protected Label getWsdlServiceNameLabel() {
		return wsdlServiceNameLabel;
	}
	
	private void displayApplicationVerificationMessage(ApplicationState appState, String wsdlServiceUrl) {
		
		if (appState != null) {
			
			String appStatus = appState.getCurrentRunState().toString();
			
			if (appStatus == "RUNNING") {
				GUIUtil.openMessageWindow(
						"Successful Connection!",
						"Successful Application Connection " + wsdlServiceUrl + ".",
						SWT.ICON_INFORMATION);
			} else if (appStatus != "RUNNING"){
					String appNotRunningMsg = "The Pace Server application [%s] is not running. Please select "
							+ "a Pace Server with a running application. Otherwise, restart"
							+ " the [%s] application.";
					String errMsg = String.format(appNotRunningMsg, appState.getApplicationId(), appState.getApplicationId());
					GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, errMsg, MessageDialog.ERROR);
			}
				
		// not able to establish a connection
		} else {
			GUIUtil.openMessageWindow(
					Constants.DIALOG_ERROR_HEADING,
					"Could not successfully connect to pace application: "
							+ wsdlServiceUrl, SWT.ICON_ERROR);
			return;

		}
	}
}

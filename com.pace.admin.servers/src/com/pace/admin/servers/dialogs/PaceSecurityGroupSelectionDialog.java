/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.servers.dialogs.input.PaceSecurityGroupSelectionInput;
import com.pace.admin.servers.dialogs.providers.StandardObjectArrayContenetProvider;
import com.pace.admin.servers.dialogs.providers.StringAscSorter;

public class PaceSecurityGroupSelectionDialog extends TitleAreaDialog {
			
	class ListLabelProvider extends LabelProvider {
		public String getText(Object element) {
			return element.toString();
		}
		public Image getImage(Object element) {
			return null;
		}
	}
	
	private List toList;

	private List fromList;

	private Combo domainNameCombo;

	private ListViewer toViewer;

	private Button rightButton;

	private Button leftButton;
	
	private PaceSecurityGroupSelectionInput dialogInput;

	private ListViewer fromViewer;
	
	private String initialSelectedDomainName;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param dialogInput 
	 * @param initialSelectedDomainName 
	 */
	public PaceSecurityGroupSelectionDialog(Shell parentShell, PaceSecurityGroupSelectionInput dialogInput, String initialSelectedDomainName) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.dialogInput = dialogInput;
		
		this.initialSelectedDomainName = initialSelectedDomainName;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginBottom = 15;
		gridLayout.marginTop = 15;
		gridLayout.marginRight = 30;
		gridLayout.marginLeft = 30;
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginWidth = 0;
		gridLayout_1.numColumns = 2;
		composite.setLayout(gridLayout_1);

		final Label domainNameLabel = new Label(composite, SWT.NONE);
		domainNameLabel.setText("Domain Name:");

		domainNameCombo = new Combo(composite, SWT.READ_ONLY);
		domainNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		final Label availableDomainSecurityLabel = new Label(container, SWT.NONE);
		availableDomainSecurityLabel.setText("Available Domain Security Groups:");
		new Label(container, SWT.NONE);

		final Label selectedDomainSecurityLabel = new Label(container, SWT.NONE);
		selectedDomainSecurityLabel.setText("Selected Domain Security Groups:");

		fromViewer = new ListViewer(container, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		fromViewer.setSorter(new StringAscSorter());
		fromViewer.setLabelProvider(new ListLabelProvider());
		fromViewer.setContentProvider(new StandardObjectArrayContenetProvider());
		fromList = fromViewer.getList();
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 170;
		fromList.setLayoutData(gridData);

		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayout(new GridLayout());

		rightButton = new Button(composite_1, SWT.ARROW | SWT.RIGHT);
		

		leftButton = new Button(composite_1, SWT.ARROW | SWT.LEFT);
				
		toViewer = new ListViewer(container, SWT.V_SCROLL | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
		toViewer.setSorter(new StringAscSorter());
		toViewer.setLabelProvider(new ListLabelProvider());
		toViewer.setContentProvider(new StandardObjectArrayContenetProvider());
		toList = toViewer.getList();
		final GridData gridData_2 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData_2.widthHint = 170;
		toList.setLayoutData(gridData_2);
		setTitle("Read Me...");
		setMessage("Select Pace Security Groups by first selecting a domain name and then choosing Pace Security Groups.");
				
		populateControlValues();
		
		addListeners();
		
		if ( initialSelectedDomainName != null ) {
			
			domainNameCombo.setText(initialSelectedDomainName);
			
			populateListViewers();
			
		}
		
		//
		return area;
	}

	private void populateControlValues() {

		if ( dialogInput != null ) {
					
			if ( dialogInput.getDomainNames() != null ) {
				
				domainNameCombo.setItems(dialogInput.getDomainNames());
				domainNameCombo.select(0);
				
				populateListViewers();
				
			}
			
		}
		
	}

	private void populateListViewers() {

		
		if ( dialogInput != null ) {
			
			String[] securityGroupsForDomain = dialogInput.getSecurityGroups(domainNameCombo.getText());
			
			if ( securityGroupsForDomain != null ) {
				
				fromViewer.setInput(securityGroupsForDomain);
				
			} else {
				
				fromViewer.getList().removeAll();
				
			}
			
			String[] selectedSecurityGroupsForDomain = dialogInput.getSelectedSecurityGroups(domainNameCombo.getText());
			
			if ( selectedSecurityGroupsForDomain != null ) {
				
				toViewer.setInput(selectedSecurityGroupsForDomain);
				
			} else {
				
				toViewer.getList().removeAll();
				
			}
			
		}
	}

	private void addListeners() {

		domainNameCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent selectionEvent) {
				
				//String selectedDomainName = domainNameCombo.getText();
				populateListViewers();
				
				
			}
		});
		
		fromViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
							
				if ( fromViewer.getSelection() instanceof IStructuredSelection) {
					
					IStructuredSelection selection = (IStructuredSelection) fromViewer.getSelection();
					
					Object[] selectedElements = selection.toArray();
					
					//toViewer.add(selectedElements);
															
					if ( dialogInput != null ) {
						
						String[] securityGroupsToAdd = convertToStringAr(selectedElements);						
						
						dialogInput.addSelectedSecurityGroups(domainNameCombo.getText(), securityGroupsToAdd);

						populateListViewers();
						
					}
					
					toViewer.setSelection(new StructuredSelection(selectedElements));
					
					//fromViewer.remove(selectedElements);					
					
				}
				
			}
		});
		
		fromList.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				toViewer.setSelection(null);
				
			}
		});
		
		toViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				
				if ( toViewer.getSelection() instanceof IStructuredSelection) {
					
					IStructuredSelection selection = (IStructuredSelection) toViewer.getSelection();
					
					Object[] selectedElements = selection.toArray();
					
					//fromViewer.add(selectedElements);
															
					if ( dialogInput != null ) {
						
						String[] securityGroupsToRemove = convertToStringAr(selectedElements);
						
						dialogInput.removeSelectedSecurityGroups(domainNameCombo.getText(), securityGroupsToRemove);
						
						populateListViewers();
						
					}
					
					fromViewer.setSelection(new StructuredSelection(selectedElements));
					
					//toViewer.remove(selectedElements);					
					
				}

			}
		});
		
		toList.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				fromViewer.setSelection(null);
				
			}
		});
		
		rightButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent selectionEvent) {
				
				IStructuredSelection selection = (IStructuredSelection) fromViewer.getSelection();
				
				if ( selection.size() > 0 ) {				
					
					Object[] selectedElements = selection.toArray();
					
					//toViewer.add(selectedElements);
															
					if ( dialogInput != null ) {
						
						String[] securityGroupsToAdd = convertToStringAr(selectedElements);						
						
						dialogInput.addSelectedSecurityGroups(domainNameCombo.getText(), securityGroupsToAdd);
						
						populateListViewers();
						
					}
					
					toViewer.setSelection(new StructuredSelection(selectedElements));
					
					//fromViewer.remove(selectedElements);			
					
				}
				
			}
		});
		
		leftButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent selectionEvent) {
				
				IStructuredSelection selection = (IStructuredSelection) toViewer.getSelection();
				
				if ( selection.size() > 0 ) {				
					
					Object[] selectedElements = selection.toArray();
												
					if ( dialogInput != null ) {
						
						String[] securityGroupsToRemove = convertToStringAr(selectedElements);
						
						dialogInput.removeSelectedSecurityGroups(domainNameCombo.getText(), securityGroupsToRemove);
						
						populateListViewers();
						
					}
					
					fromViewer.setSelection(new StructuredSelection(selectedElements));
					
				}
				
			}
		});
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(543, 417);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Pace Security Groups");
	}

	private String[] convertToStringAr(Object[] objectAr) {
		
		String[] stringAr = null;
		
		if ( objectAr != null && objectAr.length > 0 ) {
			
			stringAr = new String[objectAr.length];
			
			int ndx = 0;
			for (Object obj : objectAr ) {
				
				stringAr[ndx++] = (String) obj;
				
			}			
			
		}
		
		return stringAr;
		
	}
}

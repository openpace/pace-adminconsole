/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ServersPlugin;


public class RenameServerDialog extends Dialog {
	private String oldServerLabel;
	private String newServerLabel;
	private String dialogTitle;
	private Text oldServerText;
	private Text newServerText;
	
	private String oldServerName;
	private String newServerName;
	
	private Label labelError;

	public RenameServerDialog(Shell parentShell, String oldServerName, String oldServerLabel, 
			String newServerLabel, String dialogTitle) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.oldServerName = oldServerName;
		this.oldServerLabel = oldServerLabel;
		this.newServerLabel = newServerLabel;
		this.dialogTitle = dialogTitle;
	}
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		final Label oldServerGroupLabel = new Label(container, SWT.NONE);
		//oldViewGroupLabel.setText("Old View Name:");
		oldServerGroupLabel.setText(oldServerLabel);
		new Label(container, SWT.NONE);
		
		oldServerText = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		oldServerText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( oldServerName != null ) {
			oldServerText.setText(oldServerName);
		}

		final Label newViewGroupLabel = new Label(container, SWT.NONE);
		//newViewGroupLabel.setText("New View Name:");
		newViewGroupLabel.setText(newServerLabel);
		
		labelError = new Label(container, SWT.NONE);
		ImageDescriptor imageDesc = ServersPlugin.getImageDescriptor(Constants.SYMBOL_ERROR_ICON_PATH);
		if (imageDesc != null ) {
			labelError.setImage(imageDesc.createImage());
		}
		GridData gd_labelError = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_labelError.widthHint = 21;
		labelError.setLayoutData(gd_labelError);
		
		newServerText = new Text(container, SWT.BORDER);
		newServerText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		if ( oldServerName != null ) {
			newServerText.setText(oldServerName);
		}
		
		newServerText.setFocus();
		newServerText.selectAll();
		
		addListeners();
						
		return container;
	}

	private void addListeners() {
		
		newServerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				boolean validEntry = true;
				String newTempServerName = newServerText.getText().trim();
				if ( newTempServerName.equals("")) {
					validEntry = false;
					labelError.setToolTipText("Please enter server connector name.");
				}
				if ( validEntry &&  PafServerUtil.findDuplicateServerName(newTempServerName)) {
					validEntry = false;
					labelError.setToolTipText( newTempServerName + " already exists.");
				}	
				if( ! validEntry ) {
					labelError.setVisible(true);
				}
				else {
					labelError.setVisible(false);
					labelError.setToolTipText("");
				}
				getButton(IDialogConstants.OK_ID).setEnabled(validEntry);
			}
		});		
	}
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		newServerName = newServerText.getText().trim();
		super.okPressed();
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(340, 206);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(dialogTitle);
	}
	/**
	 * @return Returns the newViewGroupName.
	 */
	public String getNewServerName() {
		return newServerName;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.actions.RefreshServersAction;
import com.pace.admin.servers.exceptions.UserNotFoundException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafException;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.DomainNameParser;
import com.pace.server.client.PafUserDef;
import com.swtdesigner.SWTResourceManager;

public class LoginDialog extends Dialog {

	private static final Logger logger = Logger.getLogger(LoginDialog.class);
	
	private static final String INVALID_USER_NAME_OR_PASSWORD_TXT = "Invalid Username and/or Password!";
	
	private static final String LOGIN_PROBLEM = "Problem logging in.  Check server log.";
	
	private Text passwordText;

	private Text userNameText;

	private String pafServerName;

	private ServerSession serverSession;

	private Label errorLabel;

	private boolean refreshServerTree = false;

	private Button loginAsDifferentButton;
		
	/**
	 * Create the Login dialog
	 * @param parentShell
	 */
	public LoginDialog(Shell parentShell, String pafServerName, ServerSession serverSession) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.pafServerName = pafServerName;
		this.serverSession = serverSession;
		
		
}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 40;
		gridLayout.marginLeft = 40;
		gridLayout.marginBottom = 20;
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		
		errorLabel = new Label(container, SWT.CENTER);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gridData.widthHint = 40;
		gridData.heightHint = 40;
		errorLabel.setLayoutData(gridData);
		errorLabel.setForeground(SWTResourceManager.getColor(255, 0, 0));
		errorLabel.setVisible(true);
		
		final Label serverLabel = new Label(container, SWT.NONE);
		serverLabel.setText("Server:");

		final Label serverNameLabel = new Label(container, SWT.NONE);
		serverNameLabel.setText(pafServerName);
		
		final Label usernameLabel = new Label(container, SWT.NONE);
		usernameLabel.setText("Username:");

		userNameText = new Text(container, SWT.BORDER);
		userNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		userNameText.setFocus();
		userNameText.setEnabled(! serverSession.isMixedAuthMode());

		final Label passwordLabel = new Label(container, SWT.NONE);
		passwordLabel.setText("Password:");

		passwordText = new Text(container, SWT.PASSWORD | SWT.BORDER);
		passwordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		passwordText.setEnabled(! serverSession.isMixedAuthMode());
		
		new Label(container, SWT.NONE);

		loginAsDifferentButton = new Button(container, SWT.CHECK);
		loginAsDifferentButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				boolean enableDifferentLogin = loginAsDifferentButton.getSelection();
				enableLoginDialogFormFields(enableDifferentLogin);				
				
			}
		});
		loginAsDifferentButton.setText("Login as different user");
		
		initForm();
		
		if ( serverSession.isMixedAuthMode() ) {
			enableLoginDialogFormFields(serverSession.isLoginOverride());
		}
		
		loginAsDifferentButton.setVisible(serverSession.isMixedAuthMode());		
		
		//
		return container;
	}

	protected void enableLoginDialogFormFields(boolean enable) {

		userNameText.setEnabled(enable);
		
		passwordText.setEnabled(enable);
						
		serverSession.setLoginOverride(enable);
		
		initForm();	
		
	}

	/**
	 * 
	 * Initializes login form.
	 *
	 */
	private void initForm() {
		
		//if native or login override (for mixed mode)
		if ( serverSession.isNativeAuthMode() || serverSession.isLoginOverride() ) {
		
			// TTN-1718 - set user name from previous login
			if (SecurityManager.getPreviousUser() != null) {
				
				userNameText.setText(SecurityManager.getPreviousUser());
			}
			
			loginAsDifferentButton.setSelection(serverSession.isLoginOverride());

			if ( userNameText.getText().trim().length() > 0 ) {
				
				userNameText.setSelection(0, userNameText.getText().trim().length());
				
			}
			
			userNameText.setFocus();
			
		} else if ( serverSession.isMixedAuthMode() ) {
			
			String userName = System.getProperty(Constants.USER_NAME_SYSTEM_PROPERTY);
			
			String domainName = serverSession.getDomainName();
						
			if ( userName != null && domainName != null ) {
			
				userNameText.setText(domainName + DomainNameParser.BACK_SLASH_TOKEN + userName);
				passwordText.setText("");
												
			}			
			
		}
				
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(370, 290);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Login");
	}
	
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
												
			String url = null;
			
			try {
				
				PafServer pafServer = PafServerUtil.getServer(pafServerName);
				
				url = pafServer.getCompleteWSDLService();
				
			} catch (PafServerNotFound e1) {
				//do nothing
			}
			
			final String finalUrl = url;
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getDisplay(), new Runnable() {
								
				public void run() {
				
					String userName = userNameText.getText().trim();
					
					SecurityManager.setPreviousUser(userName); // TTN-1718
					
					String password = passwordText.getText().trim();
					
					try {
						
						boolean isAuthenticated = false;
						
						switch (serverSession.getAuthMode()) {
						
						case NATIVE_MODE:
							
								isAuthenticated = SecurityManager.authenticate(finalUrl, userName, password);
							
								break;
								
						case MIXED_MODE:
													
								boolean isUserOverridden = loginAsDifferentButton.getSelection();
							
								if ( ! isUserOverridden ) {
																
									password = null;
									
								}
								
								serverSession.setLoginOverride(isUserOverridden);
								
								SecurityManager.setSession(finalUrl, serverSession);
							
								isAuthenticated = SecurityManager.authenticate(finalUrl, userName, password);
								
								break;				
						}					
						
						if ( finalUrl != null && ! isAuthenticated ) {
												
							enableErrorMessage(INVALID_USER_NAME_OR_PASSWORD_TXT);
																
							return;
						}
						
					} catch (Exception e) {
						
						logger.error(e.getMessage());
						
						String errorMessage = e.getMessage();
						
						String pafExpClassName = PafException.class.getName() + ": "; 
						
						if ( errorMessage.startsWith(pafExpClassName)) {
							
							enableErrorMessage(e.getMessage().replaceAll(pafExpClassName, ""));
							
						} else {
						
							enableErrorMessage(LOGIN_PROBLEM);
							
						}
						
											
						return;				
					}
					
					
					if (  finalUrl != null && SecurityManager.isAuthenticated(finalUrl) && refreshServerTree ) {
						
						IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
						
						RefreshServersAction refreshServerAction = new RefreshServersAction("", ServerView.getViewer(), false);
						
						refreshServerAction.run();
										
						//reset flag
						refreshServerTree = false;
						
					}
				
					disableErrorMessage();
										
				}
			});
			
			//if authenticated, close dialog
			if ( SecurityManager.isAuthenticated(finalUrl) ) {
				super.buttonPressed(buttonId);
				if (!SecurityManager.isAdmin(url)) {
					MessageDialog.openWarning(null, "Warning", 
							"You are a non-admin user. The "
							+ "following functionality is disabled: \n\n" 
							+ "Edit Users, \nDelete Users, \nCreate users, \n"
							+ "Select Security Groups, \n"
							+ "Assign/Remove Admin Rights to LDAP users");
				}
			}
				
		} else {

			super.buttonPressed(buttonId);
			
		}
	
	}

	private void enableErrorMessage(String text) {
		
		errorLabel.setText(text);
		errorLabel.setVisible(true);
									
		passwordText.setText("");
		userNameText.setFocus();	
		
	}
	
	private void disableErrorMessage() {
		
		errorLabel.setVisible(false);
		
	}

	/**
	 * @param refreshServerTree The refreshServerTree to set.
	 */
	public void setRefreshServerTree(boolean refreshServerTree) {
		this.refreshServerTree = refreshServerTree;
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.enums.UserDialogActionType;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.base.PafBaseConstants;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafUserDef;
 

/**
 * User dialog is used for new and existing users.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */

public class UserDialog extends Dialog {

	private Label changePasswordLabel;
	
	private Button adminCheckButton;
	
	private Button changePasswordButton;

	private Label administratorLabel;

	private Label emailLabel;

	private Text emailText;

	private Label errorLabel;

	private Set<String> existingUserNameSet = new TreeSet<String>(
			String.CASE_INSENSITIVE_ORDER);

	private Label firstNameLabel;

	private Text firstNameText;

	private Label lastNameLabel;

	private Text lastNameText;

	// default
	private int maxPasswordLength = PafBaseConstants.DEFAULT_PASSWORD_MAX_LENGTH;

	// default
	private int minPasswordLength = PafBaseConstants.DEFAULT_PASSWORD_MIN_LENGTH;

	private Button okButton = null;

	private PafUserDef pafUserDef;

	private Text password1Text;

	private Text password2Text;

	private Label passwordAgainLabel;

	private Label passwordLabel;

	private Label passwordsAreLabel;

	private UserDialogActionType userDialogActionType;

	private Label usernameLabel;

	private Text userNameText;
	
	private boolean isReadOnly;

	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 */
	public UserDialog(Shell parentShell, PafUserDef pafUserDef,
			UserDialogActionType userDialogActionType, String pafServerName, boolean isReadOnly) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );

		this.pafUserDef = pafUserDef;

		this.userDialogActionType = userDialogActionType;

		this.isReadOnly = isReadOnly;
		
		PafServer pafServer = getPafServer(pafServerName);

		if ( ! isReadOnly ) {
			
			if (userDialogActionType.equals(UserDialogActionType.NewUser)) {
		
				importDbUsers(pafServer);
				
			}
		
		}

		if (pafServer != null && pafServerName != null) {
			String url = PafServerUtil.getServerWebserviceUrl(pafServerName);
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);

			if ( pafServerAck != null ) {
				
				minPasswordLength = pafServerAck.getMinPassowordLength();
	
				maxPasswordLength = pafServerAck.getMaxPassowordLength();
				
			}

		}
	}

	/**
	 * @wbp.parser.constructor
	 */
	public UserDialog(Shell parentShell,
			UserDialogActionType userDialogActionType, String pafServerName, boolean isReadOnly) {
		this(parentShell, null, userDialogActionType, pafServerName, isReadOnly);
	}

	
	private void addModifyListener(Text text) {

		text.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				updateOkButtonState();

			}

		});
	}

	private void addSelectionChangedListener(Button button) {

		button.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {

				updateOkButtonState();

			}

		});
	}

	private void addValidationListeners() {

		if (userDialogActionType.equals(UserDialogActionType.NewUser)) {

			addModifyListener(userNameText);

		}

		addModifyListener(firstNameText);
		addModifyListener(lastNameText);
		addModifyListener(password1Text);
		addModifyListener(password2Text);
		addModifyListener(emailText);
		addSelectionChangedListener(adminCheckButton);
		addSelectionChangedListener(changePasswordButton);

	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("User");
	}

	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);

		if (userDialogActionType.equals(UserDialogActionType.NewUser)) {
			okButton.setEnabled(false);
		} else if (userDialogActionType.equals(UserDialogActionType.EditUser) && ! isReadOnly) {

			boolean validFormData = isFormDataValid();

			okButton.setEnabled(validFormData);

		}

		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		errorLabel = new Label(container, SWT.CENTER);
		errorLabel.setForeground(Display.getCurrent().getSystemColor(
				SWT.COLOR_RED));
		final FormData formData_12 = new FormData();
		formData_12.left = new FormAttachment(0, 40);
		formData_12.top = new FormAttachment(0, 15);
		formData_12.bottom = new FormAttachment(0, 30);
		errorLabel.setLayoutData(formData_12);

		usernameLabel = new Label(container, SWT.NONE);
		final FormData formData = new FormData();
		formData.right = new FormAttachment(0, 150);
		formData.bottom = new FormAttachment(0, 59);
		formData.top = new FormAttachment(0, 46);
		formData.left = new FormAttachment(0, 81);
		usernameLabel.setLayoutData(formData);
		usernameLabel.setText("Username:");

		firstNameLabel = new Label(container, SWT.NONE);
		final FormData formData_2 = new FormData();
		formData_2.bottom = new FormAttachment(0, 90);
		formData_2.top = new FormAttachment(0, 77);
		formData_2.right = new FormAttachment(0, 166);
		formData_2.left = new FormAttachment(0, 81);
		firstNameLabel.setLayoutData(formData_2);
		firstNameLabel.setText("First Name:");

		lastNameLabel = new Label(container, SWT.NONE);
		final FormData formData_3 = new FormData();
		formData_3.bottom = new FormAttachment(0, 121);
		formData_3.top = new FormAttachment(0, 108);
		formData_3.right = new FormAttachment(0, 166);
		formData_3.left = new FormAttachment(0, 81);
		lastNameLabel.setLayoutData(formData_3);
		lastNameLabel.setText("Last Name:");

		emailLabel = new Label(container, SWT.NONE);
		final FormData formData_4 = new FormData();
		formData_4.right = new FormAttachment(usernameLabel, 0, SWT.RIGHT);
		formData_4.bottom = new FormAttachment(0, 214);
		formData_4.top = new FormAttachment(0, 201);
		formData_4.left = new FormAttachment(0, 81);
		emailLabel.setLayoutData(formData_4);
		emailLabel.setText("E-mail:");

		userNameText = new Text(container, SWT.BORDER);
		userNameText.setTextLimit(20);
		final FormData formData_1 = new FormData();
		formData_1.bottom = new FormAttachment(0, 63);
		formData_1.top = new FormAttachment(0, 44);
		formData_1.right = new FormAttachment(0, 372);
		formData_1.left = new FormAttachment(0, 179);
		userNameText.setLayoutData(formData_1);

		firstNameText = new Text(container, SWT.BORDER);
		firstNameText.setTextLimit(25);
		final FormData formData_1_1 = new FormData();
		formData_1_1.bottom = new FormAttachment(0, 94);
		formData_1_1.top = new FormAttachment(0, 75);
		formData_1_1.right = new FormAttachment(0, 372);
		formData_1_1.left = new FormAttachment(0, 179);
		firstNameText.setLayoutData(formData_1_1);

		lastNameText = new Text(container, SWT.BORDER);
		lastNameText.setTextLimit(25);
		final FormData formData_1_2 = new FormData();
		formData_1_2.bottom = new FormAttachment(0, 125);
		formData_1_2.top = new FormAttachment(0, 106);
		formData_1_2.right = new FormAttachment(0, 372);
		formData_1_2.left = new FormAttachment(0, 179);
		lastNameText.setLayoutData(formData_1_2);

		emailText = new Text(container, SWT.BORDER);
		emailText.setTextLimit(100);
		final FormData formData_1_3 = new FormData();
		formData_1_3.bottom = new FormAttachment(0, 218);
		formData_1_3.top = new FormAttachment(0, 199);
		formData_1_3.right = new FormAttachment(0, 372);
		formData_1_3.left = new FormAttachment(0, 179);
		emailText.setLayoutData(formData_1_3);

		passwordLabel = new Label(container, SWT.NONE);
		final FormData formData_5 = new FormData();
		formData_5.bottom = new FormAttachment(0, 152);
		formData_5.top = new FormAttachment(0, 139);
		formData_5.right = new FormAttachment(0, 166);
		formData_5.left = new FormAttachment(0, 81);
		passwordLabel.setLayoutData(formData_5);
		passwordLabel.setText("Password:*");

		passwordAgainLabel = new Label(container, SWT.NONE);
		final FormData formData_6 = new FormData();
		formData_6.bottom = new FormAttachment(0, 183);
		formData_6.top = new FormAttachment(0, 170);
		formData_6.right = new FormAttachment(0, 166);
		formData_6.left = new FormAttachment(0, 81);
		passwordAgainLabel.setLayoutData(formData_6);
		passwordAgainLabel.setText("Password Again:*");

		password1Text = new Text(container, SWT.PASSWORD | SWT.BORDER);
		password1Text.setTextLimit(20);
		final FormData formData_7 = new FormData();
		formData_7.bottom = new FormAttachment(0, 156);
		formData_7.top = new FormAttachment(0, 137);
		formData_7.right = new FormAttachment(0, 372);
		formData_7.left = new FormAttachment(0, 179);
		password1Text.setLayoutData(formData_7);

		password2Text = new Text(container, SWT.PASSWORD | SWT.BORDER);
		password2Text.setTextLimit(20);
		final FormData formData_8 = new FormData();
		formData_8.bottom = new FormAttachment(0, 187);
		formData_8.top = new FormAttachment(0, 168);
		formData_8.right = new FormAttachment(0, 372);
		formData_8.left = new FormAttachment(0, 179);
		password2Text.setLayoutData(formData_8);

		administratorLabel = new Label(container, SWT.NONE);
		final FormData formData_10 = new FormData();
		formData_10.right = new FormAttachment(passwordAgainLabel, 0, SWT.RIGHT);
		formData_10.top = new FormAttachment(0, 230);
		formData_10.left = new FormAttachment(emailLabel, 0, SWT.LEFT);
		administratorLabel.setLayoutData(formData_10);
		administratorLabel.setText("Administrator:");

		adminCheckButton = new Button(container, SWT.CHECK);
		final FormData formData_11 = new FormData();
		formData_11.right = new FormAttachment(0, 192);
		formData_11.left = new FormAttachment(0, 179);
		formData_11.top = new FormAttachment(administratorLabel, 0, SWT.TOP);
		adminCheckButton.setLayoutData(formData_11);

		passwordsAreLabel = new Label(container, SWT.NONE);
		formData_12.right = new FormAttachment(passwordsAreLabel, 0, SWT.RIGHT);
		final FormData formData_9 = new FormData();
		formData_9.top = new FormAttachment(0, 282);
		formData_9.right = new FormAttachment(0, 435);
		formData_9.left = new FormAttachment(0, 81);
		passwordsAreLabel.setLayoutData(formData_9);
		passwordsAreLabel.setText("* Passwords are case sensitive.");

		changePasswordLabel = new Label(container, SWT.NONE);
		final FormData formData_13 = new FormData();
		formData_13.top = new FormAttachment(0, 258);
		formData_13.right = new FormAttachment(0, 174);
		formData_13.left = new FormAttachment(0, 81);
		changePasswordLabel.setLayoutData(formData_13);
		changePasswordLabel.setText("Change Password:");

		changePasswordButton = new Button(container, SWT.CHECK);
		final FormData formData_14 = new FormData();
		formData_14.top = new FormAttachment(0, 257);
		formData_14.left = new FormAttachment(0, 179);
		changePasswordButton.setLayoutData(formData_14);

		container.setTabList(new Control[] {userNameText, firstNameText, lastNameText, password1Text, password2Text, emailText, adminCheckButton, changePasswordButton });

		if (userDialogActionType.equals(UserDialogActionType.EditUser)) {

			userNameText.setText(pafUserDef.getUserName());
			userNameText.setEnabled(false);

			if (pafUserDef.getFirstName() != null) {
				firstNameText.setText(pafUserDef.getFirstName());
			}
			
			firstNameText.setEnabled(! isReadOnly);

			if (pafUserDef.getLastName() != null) {
				lastNameText.setText(pafUserDef.getLastName());
			}

			lastNameText.setEnabled(! isReadOnly);
			
			if (pafUserDef.getPassword() != null) {
				password1Text.setText(pafUserDef.getPassword());
				password2Text.setText(pafUserDef.getPassword());
			}
			
			password1Text.setEnabled(! isReadOnly);
			password2Text.setEnabled(! isReadOnly);

			if (pafUserDef.getEmail() != null) {
				emailText.setText(pafUserDef.getEmail());
			}
			
			emailText.setEnabled(! isReadOnly);

			adminCheckButton.setSelection(pafUserDef.isAdmin());
			
			adminCheckButton.setEnabled(! isReadOnly);
			
			changePasswordButton.setSelection(pafUserDef.isChangePassword());
			
			changePasswordButton.setEnabled(! isReadOnly);

		}
		
		if ( ! isReadOnly ) {
		
			addValidationListeners();
			
		}
		
		//
		return container;
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(474, 397);
	}

	private PafServer getPafServer(String pafServerName) {

		PafServer pafServer = null;

		try {
			pafServer = PafServerUtil.getServer(pafServerName);
		} catch (PafServerNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return pafServer;
	}

	/**
	 * @return the pafUserDef
	 */
	public PafUserDef getPafUserDef() {
		return pafUserDef;
	}

	private void importDbUsers(PafServer pafServer) {

		if (pafServer != null) {

			PafUserDef[] pafUserDefAr = SecurityManager
					.getDbUsersFromServer(pafServer.getCompleteWSDLService());

			if (pafUserDefAr != null && pafUserDefAr.length > 0) {

				for (PafUserDef pafUserDef : pafUserDefAr) {

					existingUserNameSet.add(pafUserDef.getUserName());
				}

			}

		}

	}

	private boolean isFormDataValid() {

		errorLabel.setText("");

		if (userDialogActionType.equals(UserDialogActionType.NewUser)) {

			String typedUserName = userNameText.getText().trim();
			
			if ( existingUserNameSet.contains(typedUserName) ) {
			
				errorLabel.setText("Username already exists.");
				return false;
			}
									
			if (typedUserName.length() == 0	) {

				errorLabel.setText("Username is a required field.");

				return false;

			}
			
			if ( typedUserName.contains(" ") ) {

				errorLabel.setText("Username can not contain spaces.");

				return false;

				
			}

		}

		if (firstNameText.getText().trim().length() == 0) {

			errorLabel.setText("First name is a required field.");

			return false;
		}

		if (lastNameText.getText().trim().length() == 0) {

			errorLabel.setText("Last name is a required field.");

			return false;
		}

		String password1 = password1Text.getText().trim();
		String password2 = password2Text.getText().trim();
		
		if ( password1.length() >= minPasswordLength && password1.length() <= maxPasswordLength &&
				! password1.equals(password2) ) {

			errorLabel.setText("Passwords do not match.");

			return false;

		}
		
		if (password1.length() < minPasswordLength
				|| password1.length() > maxPasswordLength
				|| password2.length() < minPasswordLength
				|| password2.length() > maxPasswordLength) {

			errorLabel
					.setText("Passwords must be between " + minPasswordLength
							+ " and " + maxPasswordLength
							+ " alphanumeric characters.");

			return false;
		}

		if ( !password1.equals(password2) ) {

			errorLabel.setText("Passwords do not match.");

			return false;
		}

		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		if (pafUserDef == null) {
			pafUserDef = new PafUserDef();
		}

		pafUserDef.setUserName(userNameText.getText().trim());
		pafUserDef.setFirstName(firstNameText.getText().trim());
		pafUserDef.setLastName(lastNameText.getText().trim());
		pafUserDef.setEmail(emailText.getText().trim());
		pafUserDef.setPassword(password1Text.getText().trim());
		pafUserDef.setAdmin(adminCheckButton.getSelection());
		pafUserDef.setChangePassword(changePasswordButton.getSelection());

		super.okPressed();
	}

	protected void updateOkButtonState() {
		
		
		okButton.setEnabled(isFormDataValid());
		

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.servers.dialogs.input.ImportExportMemberTagDataInput;
import com.pace.admin.servers.enums.TransactionType;
import com.pace.base.PafException;
import com.pace.server.client.MemberTagInformation;

public class MigrateMemberTagDataDialog extends TitleAreaDialog{

	private static Logger logger = Logger.getLogger(MigrateMemberTagDataDialog.class);
	
	private String serverName;
	
	private MemberTagInfo[] memberTagInfoAr;
	
	private ImportExportMemberTagDataInput dialogInput;
	
	private Label memberTag;
	
	private Combo memberTagCombo;
	
	String[] itemList;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param lastMemberTagDataExportDir 
	 * @param serverName 
	 * @param memberTagInfoAr 
	 * @param transType 
	 */
	public MigrateMemberTagDataDialog(Shell parentShell, ImportExportMemberTagDataInput dialogInput, String serverName) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.dialogInput = dialogInput;
		
		setMemberTagInfoAr();
		
		//set server named
		this.serverName = serverName;
		
	}
	
	private void setMemberTagInfoAr() {

		if ( dialogInput != null ) {
			
			//set member tag info array
			try {
				
				this.memberTagInfoAr = dialogInput.getMemberTagInfo();
				
				if (memberTagInfoAr!=null && memberTagInfoAr.length>0){
					itemList = new String[memberTagInfoAr.length];
					
					for(int i=0;i<memberTagInfoAr.length;i++) { 
						
						itemList[i] = memberTagInfoAr[i].getMemberTagNames()[0];
						
					}
					
				}
				
			} catch (PafException e) {
	
				logger.error(e.getMessage());
				
				e.printStackTrace();
			}
			
		} else {
			
			this.memberTagInfoAr = null;
			
		}
		
	}
	
	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		GridData gd_container = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_container.heightHint = 421;
		container.setLayoutData(gd_container);
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);
		
		setTitle( "Migrate Member Tag Data"    );
		
		new Label(container, SWT.NONE);
		{
			memberTag = new Label(container, SWT.LEFT);
			memberTag.setText("Select Member Tag :");
			memberTag.setEnabled(true);
		}
		{
			memberTagCombo = new Combo(container, SWT.READ_ONLY);
			memberTagCombo.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				//	validateSelections();
				}
			});
			memberTagCombo.setItems(itemList);
			memberTagCombo.setEnabled(false);
		//	combo.setBounds(151, 50, 203, 23);
			memberTagCombo.setEnabled(true);
		}
		
		return area ;

	}
}

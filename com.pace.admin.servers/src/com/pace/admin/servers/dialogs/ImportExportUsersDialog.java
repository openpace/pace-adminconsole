/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pace.admin.servers.providers.ImportExportUsersContentProvider;
import com.pace.admin.servers.providers.ImportExportUsersLabelProvider;
import com.pace.server.client.PafUserDef;

/**
 * Dialog used for selecting users for import/export from/to a CSV file
 * @author fskrgic
 *
 */
public class ImportExportUsersDialog extends Dialog {
	
	private static Logger logger = Logger.getLogger(ImportExportUsersDialog.class);

	// Table used
	private Table table;
	
	// Checkbox table viewr widget
	private CheckboxTableViewer checkboxTableViewer;
	
	// List of checked indeces
	private List<Integer> checkedIndeces;
	
	//List of users
	private PafUserDef [] users;
	
	// List of users existing on the server in case of import
	private String [] existingUserNames;
	
	// Designates whether importing or exporting
	boolean importing;
	
	/**
	 * Create the dialog (constructor)
	 * @param parentShell
	 */
	public ImportExportUsersDialog(Shell parentShell, PafUserDef [] users, String [] existingUserNames, boolean importing) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		// Initialize the private variables
		this.users = users;
		
		this.existingUserNames = existingUserNames;
		
		this.importing = importing;
		
		checkedIndeces = new ArrayList<Integer>();
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		// Create the container
		Composite container = (Composite) super.createDialogArea(parent);
		
		final GridLayout gridLayout = new GridLayout();
		
		gridLayout.marginRight = 5;
		
		gridLayout.marginLeft = 5;
		
		container.setLayout(gridLayout);


		// Create a text labels
		Label selectUsersToLabel;
		
		selectUsersToLabel = new Label(container, SWT.NONE);
		
		selectUsersToLabel.setText("Select users to import/export");
		

		// Create the table container
		final Composite TableComposite = new Composite(container, SWT.NONE);
		
		final GridData gd_tableComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		
		gd_tableComposite.widthHint = 753;
		
		TableComposite.setLayoutData(gd_tableComposite);
		
		TableComposite.setLayout(new FillLayout());
		

		// Create a check box table viewer 
		final CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(TableComposite, SWT.BORDER);
		
		this.checkboxTableViewer = checkboxTableViewer;
		
		table = checkboxTableViewer.getTable();
		
		table.setHeaderVisible(true);
		
		
		// Disable or enable the OK button depending whether anything was selected
		checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {

			public void checkStateChanged(CheckStateChangedEvent arg0) {

				if(checkboxTableViewer.getCheckedElements().length == 0){
					
					getOKButton().setEnabled(false);
					
				}
				else{
					
					getOKButton().setEnabled(true);
					
				}
			}
		});

		// Create the table columns
		final TableColumn userNameTableColumn = new TableColumn(table, SWT.NONE);
		
		userNameTableColumn.setWidth(100);
		
		userNameTableColumn.setText("User Name");
		

		final TableColumn passwordTableColumn = new TableColumn(table, SWT.NONE);
		
		passwordTableColumn.setWidth(100);
		
		passwordTableColumn.setText("Password");
		

		final TableColumn emailTableColumn = new TableColumn(table, SWT.NONE);
		
		emailTableColumn.setWidth(100);
		
		emailTableColumn.setText("Email");
		

		final TableColumn firsNameTableColumn = new TableColumn(table, SWT.NONE);
		
		firsNameTableColumn.setWidth(100);
		
		firsNameTableColumn.setText("First Name");
		

		final TableColumn lastNameTableColumn = new TableColumn(table, SWT.NONE);
		
		lastNameTableColumn.setWidth(100);
		
		lastNameTableColumn.setText("Last Name");
		

		final TableColumn administratorTableColumn = new TableColumn(table, SWT.NONE);
		
		administratorTableColumn.setWidth(100);
		
		administratorTableColumn.setText("Administrator");
		

		final TableColumn changePasswordTableColumn = new TableColumn(table, SWT.NONE);
		
		changePasswordTableColumn.setWidth(106);
		
		changePasswordTableColumn.setText("Change Password");
		
		// Set the content and label providers and set the input
		checkboxTableViewer.setLabelProvider(new ImportExportUsersLabelProvider());
		
		checkboxTableViewer.setContentProvider(new ImportExportUsersContentProvider());
		
		checkboxTableViewer.setInput(users);
		
		
		// Preselect appropriate users in the table
		for(int i=0; i<users.length; i++ ){
			
			PafUserDef user = users[i];
			
			boolean check = true;
			
			// If importing, do not preselect users already existing on the server 
			if(importing )
			{
				
				for(String existingUser : existingUserNames){
					
					if(existingUser.equals(user.getUserName()))
						
						check = false;
					
				}				
			}
			
			// Set the checked state for i-th user
			checkboxTableViewer.setChecked(checkboxTableViewer.getElementAt(i), check);
			
		}

		
		// Create button container
		final Composite ButtonComposite = new Composite(container, SWT.NONE);
		
		ButtonComposite.setLayoutData(new GridData(150, SWT.DEFAULT));
		
		final GridLayout gridLayout_1 = new GridLayout();
		
		gridLayout_1.makeColumnsEqualWidth = true;
		
		gridLayout_1.numColumns = 2;
		
		ButtonComposite.setLayout(gridLayout_1);

		
		// Create Sellect All button
		final Button selectAllButton = new Button(ButtonComposite, SWT.NONE);
		
		selectAllButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				checkboxTableViewer.setAllChecked(true);

				// Enable the OK button
				getOKButton().setEnabled(true);
					
			}
		});
		
		selectAllButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		selectAllButton.setText("Select All");

		
		// Create Desellect All button
		final Button deselectAllButton = new Button(ButtonComposite, SWT.NONE);
		
		deselectAllButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				checkboxTableViewer.setAllChecked(false);
				
				// Disable the OK button
				getOKButton().setEnabled(false);
				
			}
		});
		deselectAllButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		deselectAllButton.setText("Deselect All");
		
		
		return container;
	}


	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		// Create OK and cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
	}
	

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		
		return new Point(739, 467);
		
	}
	
	
	/**
	 * Return selected indeces
	 * @return
	 */
	public List<Integer> getCheckedIndeces(){
		
		return checkedIndeces;
		
	}
	
	
	/**
	 * Implement the event of selecting the OK button
	 */
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
			
			// When the OK button is clicked, fill the array of selected indeces with appropriate values
			for(int i=0; i< users.length; i++){
				
				if(checkboxTableViewer.getChecked(checkboxTableViewer.getElementAt(i))){
					
					checkedIndeces.add(i);
					
				}
			}
			
			// Close the dialog
			this.close();
			
			return;
			
		}
		
		super.buttonPressed(buttonId);
		
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		if(importing){
			
			newShell.setText("Import Users");
			
		}
		else{
			
			newShell.setText("Export Users");
			
		}
	}
}

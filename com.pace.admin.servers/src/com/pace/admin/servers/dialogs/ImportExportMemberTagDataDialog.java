/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ImportExportFileType;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.widgets.StringArraySelectionDialog;
import com.pace.admin.servers.dialogs.input.ImportExportMemberTagDataInput;
import com.pace.admin.servers.dialogs.providers.ExportMemberTagInfoTableLabelProvider;
import com.pace.admin.servers.dialogs.providers.ImportMemberTagInfoTableLabelProvider;
import com.pace.admin.servers.dialogs.providers.StandardObjectArrayContenetProvider;
import com.pace.admin.servers.enums.TransactionType;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.server.client.MemberTagInformation;
import com.pace.server.client.PafFilteredMbrTagRequest;
import com.pace.server.client.PafGetMemberTagDataResponse;
import com.pace.server.client.PafImportMemberTagRequest;
import com.pace.server.client.PafMbrTagFilter;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafSuccessResponse;
import com.pace.server.client.SimpleMemberTagData;

/**
 * 
 * Imports and Exports Member Tag Data from the server to xml or binary.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ImportExportMemberTagDataDialog extends TitleAreaDialog {
		
	private static Logger logger = Logger.getLogger(ImportExportMemberTagDataDialog.class);
	
	public static final String OVERRIDE_APPLICATION_ID = "Override Application Id";

	public static final String MEMBER_TAG_DATA_COUNT = "Member Tag Data Count";

	public static final String APPLICATION_ID = "Application Id";
		
	private Combo exportFileFormatCombo;

	private Text exportFileDirTextBox;

	private Table exportTable;

	private Table importTable;

	private Text importFileLocationText;

	private Button exportFileDirBrowseButton;

	private TransactionType transType;

	private TabFolder tabFolder;

	private TabItem importTabItem;

	private TabItem exportTabItem;

	private MemberTagInfo[] memberTagInfoAr;

	private Button importSelectDeselectButton;

	private CheckboxTableViewer importCheckboxTableViewer;

	private CheckboxTableViewer exportCheckboxTableViewer;
	
	private SimpleMemberTagData[] importedSimpleMemberTagDataAr;
	
	private Map<MemberTagInfo, Button> exportDynamicButtonsMap = new HashMap<MemberTagInfo, Button>();
	
	private Map<MemberTagInfo, List<SimpleMemberTagData>> importedSimpleMemberTagDataMap;
	
	private Button exportSelectDeselectButton;
	
	private Button exportDataButton;
	
	private String serverName;

	private String lastFileExportDir;

	private Button importBrowseButton;

	private Button importDataButton;

	private Label importServerLabel;

	private Label exportServerLabel;

	private ImportExportMemberTagDataInput dialogInput;
	/**
	 * Create the dialog
	 * @param parentShell
	 * @param lastMemberTagDataExportDir 
	 * @param serverName 
	 * @param memberTagInfoAr 
	 * @param transType 
	 */
	public ImportExportMemberTagDataDialog(Shell parentShell, ImportExportMemberTagDataInput dialogInput, String serverName, TransactionType transType, String lastMemberTagDataExportDir) {
		
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		
		this.dialogInput = dialogInput;
		
		setMemberTagInfoAr();
		
		//if trans type is null, default to import
		if ( transType == null ) {
			transType = TransactionType.Import;
		} else {
			this.transType = transType;
		}
		
		//set server named
		this.serverName = serverName;
		
		this.lastFileExportDir = lastMemberTagDataExportDir;
		
		if ( this.lastFileExportDir == null ) {
			
			this.lastFileExportDir = Constants.DEFAULT_IMPORT_EXPORT_DIRECTORY;
			
		}
		
	}

	private void setMemberTagInfoAr() {

		if ( dialogInput != null ) {
			
			//set member tag info array
			try {
				
				this.memberTagInfoAr = dialogInput.getMemberTagInfo();
				
			} catch (PafException e) {
	
				logger.error(e.getMessage());
				
				e.printStackTrace();
			}
			
		} else {
			
			this.memberTagInfoAr = null;
			
		}
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		GridData gd_container = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_container.heightHint = 421;
		container.setLayoutData(gd_container);
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.marginRight = 20;
		gridLayout.marginLeft = 20;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);

		tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		importTabItem = new TabItem(tabFolder, SWT.NONE);
		importTabItem.setToolTipText("Imports Member Tag Data");
		importTabItem.setText("Import");

		final Composite composite = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.marginTop = 8;
		gridLayout_1.marginRight = 20;
		gridLayout_1.marginLeft = 20;
		gridLayout_1.marginBottom = 8;
		composite.setLayout(gridLayout_1);
		importTabItem.setControl(composite);

		final Label importFileLabel = new Label(composite, SWT.NONE);
		importFileLabel.setText("Import File:");

		final Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginHeight = 0;
		gridLayout_2.marginWidth = 0;
		gridLayout_2.numColumns = 2;
		composite_1.setLayout(gridLayout_2);

		importFileLocationText = new Text(composite_1, SWT.BORDER);
		importFileLocationText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		importBrowseButton = new Button(composite_1, SWT.NONE);
		importBrowseButton.setText("Browse ...");

		final Label label = new Label(composite, SWT.NONE);

		final Composite composite_6 = new Composite(composite, SWT.NONE);
		final GridLayout gridLayout_7 = new GridLayout();
		gridLayout_7.marginWidth = 0;
		gridLayout_7.numColumns = 2;
		gridLayout_7.marginHeight = 0;
		composite_6.setLayout(gridLayout_7);

		final Label serverLabel = new Label(composite_6, SWT.NONE);
		serverLabel.setText("Server:");

		importServerLabel = new Label(composite_6, SWT.NONE);
		importCheckboxTableViewer = CheckboxTableViewer.newCheckList(composite, SWT.BORDER | SWT.FULL_SELECTION);
		//importCheckboxTableViewer.setSorter(new MemberTagInfoSorter());
		importCheckboxTableViewer.setContentProvider(new StandardObjectArrayContenetProvider());
		importCheckboxTableViewer.setLabelProvider(new ImportMemberTagInfoTableLabelProvider());
		importTable = importCheckboxTableViewer.getTable();
		importTable.setHeaderVisible(true);
		importTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn tableColumn = new TableColumn(importTable, SWT.NONE);
		tableColumn.setResizable(false);
		tableColumn.setWidth(23);

		final TableColumn newColumnTableColumn = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn.setText("Application Id");
		newColumnTableColumn.setWidth(85);

		final TableColumn newColumnTableColumn_1 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_1.setWidth(137);
		newColumnTableColumn_1.setText("Member Tag Data Count");

		final TableColumn newColumnTableColumn_2 = new TableColumn(importTable, SWT.NONE);
		newColumnTableColumn_2.setWidth(143);
		newColumnTableColumn_2.setText("Override Application Id");

		final Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.marginWidth = 0;
		gridLayout_3.marginHeight = 0;
		composite_2.setLayout(gridLayout_3);
		
		TextCellEditor overrideAppIdTextCellEditor = new TextCellEditor(importTable);
		Text overrideAppIdTextControl = (Text) overrideAppIdTextCellEditor.getControl();
		overrideAppIdTextControl.setTextLimit(255);
		
		importCheckboxTableViewer.setCellEditors(new CellEditor[] { null, null, null, overrideAppIdTextCellEditor } );
		
		importCheckboxTableViewer.setColumnProperties(new String[] { "checkBox", APPLICATION_ID, MEMBER_TAG_DATA_COUNT, OVERRIDE_APPLICATION_ID });

		attachCellEditors(parent);
		
		importSelectDeselectButton = new Button(composite_2, SWT.CHECK);
		importSelectDeselectButton.setText(Constants.WIDGET_SELECT_ALL_DESELECT_ALL);

		importDataButton = new Button(composite, SWT.NONE);
		final GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false, false);
		gridData.widthHint = 85;
		importDataButton.setLayoutData(gridData);
		importDataButton.setText("Import Data");

		exportTabItem = new TabItem(tabFolder, SWT.NONE);
		exportTabItem.setToolTipText("Exports Member Tag Data");
		exportTabItem.setText("Export");

		final Composite composite_3 = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginTop = 8;
		gridLayout_4.marginRight = 20;
		gridLayout_4.marginLeft = 20;
		gridLayout_4.marginBottom = 8;
		composite_3.setLayout(gridLayout_4);
		exportTabItem.setControl(composite_3);

		final Composite composite_5 = new Composite(composite_3, SWT.NONE);
		final GridLayout gridLayout_6 = new GridLayout();
		gridLayout_6.marginWidth = 0;
		gridLayout_6.numColumns = 2;
		gridLayout_6.marginHeight = 0;
		composite_5.setLayout(gridLayout_6);

		final Label serverLabel_1 = new Label(composite_5, SWT.NONE);
		serverLabel_1.setText("Server:");

		exportServerLabel = new Label(composite_5, SWT.NONE);
		exportCheckboxTableViewer = CheckboxTableViewer.newCheckList(composite_3, SWT.BORDER);
		//exportCheckboxTableViewer.setSorter(new MemberTagInfoSorter());
		exportCheckboxTableViewer.setLabelProvider(new ExportMemberTagInfoTableLabelProvider());
		exportCheckboxTableViewer.setContentProvider(new StandardObjectArrayContenetProvider());
		exportTable = exportCheckboxTableViewer.getTable();
		exportTable.setHeaderVisible(true);
		exportTable.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn_3 = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn_3.setResizable(false);
		newColumnTableColumn_3.setWidth(22);

		final TableColumn appIdColumnTableColumn = new TableColumn(exportTable, SWT.NONE);
		appIdColumnTableColumn.setWidth(100);
		appIdColumnTableColumn.setText("Application Id");

		final TableColumn countColumnTableColumn = new TableColumn(exportTable, SWT.NONE);
		countColumnTableColumn.setResizable(false);
		countColumnTableColumn.setWidth(141);
		countColumnTableColumn.setText("Member Tag Data Count");

		final TableColumn filteredMemberTagsColumnTableColumn = new TableColumn(exportTable, SWT.NONE);
		filteredMemberTagsColumnTableColumn.setResizable(false);
		filteredMemberTagsColumnTableColumn.setWidth(381);
		filteredMemberTagsColumnTableColumn.setText("Filtered Member Tags");

		final TableColumn newColumnTableColumn_7 = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn_7.setResizable(false);
		newColumnTableColumn_7.setWidth(23);
		refreshExportViewer();

		exportSelectDeselectButton = new Button(composite_3, SWT.CHECK);
		exportSelectDeselectButton.setText(Constants.WIDGET_SELECT_ALL_DESELECT_ALL);

		final Label label_2 = new Label(composite_3, SWT.NONE);
	
		final Label exportFileDirectoryLabel = new Label(composite_3, SWT.NONE);
		exportFileDirectoryLabel.setText("Export file directory:");

		final Composite composite_4 = new Composite(composite_3, SWT.NONE);
		composite_4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		final GridLayout gridLayout_5 = new GridLayout();
		gridLayout_5.numColumns = 2;
		gridLayout_5.marginWidth = 0;
		gridLayout_5.marginHeight = 0;
		composite_4.setLayout(gridLayout_5);

		exportFileDirTextBox = new Text(composite_4, SWT.READ_ONLY | SWT.BORDER);
		exportFileDirTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		exportFileDirBrowseButton = new Button(composite_4, SWT.NONE);
		exportFileDirBrowseButton.setText("Browse ...");

		final Label exportFileFormatLabel = new Label(composite_3, SWT.NONE);
		exportFileFormatLabel.setText("Export file format:");

		exportFileFormatCombo = new Combo(composite_3, SWT.READ_ONLY);
		exportFileFormatCombo.setItems(new String[] {"XML",  "Binary"} );
		exportFileFormatCombo.select(0);

		exportDataButton = new Button(composite_3, SWT.NONE);
		final GridData gridData_2 = new GridData(SWT.RIGHT, SWT.CENTER, false, false);
		gridData_2.widthHint = 85;
		exportDataButton.setLayoutData(gridData_2);
		exportDataButton.setText("Export Data");

		final Label label_1 = new Label(container, SWT.NONE);
		setTitle("Read Me...");
		
		//select tab folder based on import or export
		updateTabFolderSelection();
		
		//add buttons to export table
		addEllipseButtonToExportTable();
		
		//populate widget items with values
		popululateFormItems();
		
		//add import listeners
		addImportListeners();
		
		//add export listeners
		addExportListeners();
		
		setMessage("This dialog can import/export member tag data to/from a server.  The member tag data saved to disk can be saved in XML or binary format.");
		
		//
		return area;
	}

	private void refreshExportViewer() {

		setMemberTagInfoAr();
		
		if ( memberTagInfoAr != null ) {
			
			exportCheckboxTableViewer.setInput(memberTagInfoAr);
			
		} else {
			
			exportCheckboxTableViewer.getTable().removeAll();
		}
		
	}

	/**
	 * 
	 * Addess cell editors to the import table.  The purpose of this is to allow and 
	 * override for the application id by allowing a user to double click in the table
	 *
	 * @param parent
	 */
	private void attachCellEditors(Composite parent) {
		
		if ( importCheckboxTableViewer != null ) {
		
			//set the cell modifier
			importCheckboxTableViewer.setCellModifier(new ICellModifier() {
	
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
			 */
			public boolean canModify(Object element, String property) {
				
				if ( property.equals(OVERRIDE_APPLICATION_ID)) {
					return true;
				}
		
				return false;
			}
		
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
			 */
			public Object getValue(Object element, String property) {
		
				String value = null;
				
				//if property is the override app id, set value
				if ( property.equals(OVERRIDE_APPLICATION_ID) ) {
					
					value = ((MemberTagInfo) element).getOverrideAppId();
					
				} 
				
				//if null, set to blank
				if ( value == null ) {
					value = "";
				}
				
				//trim value
				return value.trim();
			}
		
			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
			 */
			public void modify(Object element, String property, Object value) {
		
				if ( element instanceof TableItem) {
					
					TableItem item = (TableItem) element;
					
					//if member tag info
					if ( item.getData() instanceof MemberTagInfo ) {
						
						//cast 
						MemberTagInfo memberTagInfo = (MemberTagInfo) item.getData();
						
						//if override app id prop
						if ( property.equals(OVERRIDE_APPLICATION_ID)) {
							
							//cast value
							String overrideAppId = (String) value;
							
							//check for not null and blank, change back to null
							if ( overrideAppId != null && overrideAppId.equals("")) {
								
								overrideAppId = null;
								
							}
														
							//set member tag info
							memberTagInfo.setOverrideAppId(overrideAppId);
										
						} 
						
						//set table item
						item.setData(memberTagInfo);
								
						//reversh viewer
						importCheckboxTableViewer.refresh();
														
					}
					
					
				}
	
			}
			
		});
			
		}
	}
	
	/**
	 * 
	 *  Import listeners:
	 *  
	 *  1. Select / Deselect button: checks/unchecks all checks in viewer when selected/deselected.
	 *  2. Opens a dialog a user can select an import file
	 *  3. Imports the simple member tag data from file system
	 *
	 */
	private void addImportListeners() {

		
		importSelectDeselectButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				//if check/uncheck button is checked, check all, else uncheck all items in checkbox table viewer
				importCheckboxTableViewer.setAllChecked(importSelectDeselectButton.getSelection());
								
			}
		});
		
		importBrowseButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
		
				//get directory dialog
				FileDialog dlg = new FileDialog(importBrowseButton.getShell(), SWT.OPEN);

				dlg.setFilterNames(new String[] {			
						new String("XML Member Tag Data Files (*" + PafBaseConstants.XML_EXT + ")"),						
						new String("Binary Member Tag Data Files (*" + Constants.FILE_EXT_BINARY + ")")
				});
				
				//customize extensions
				dlg.setFilterExtensions(new String[] { Constants.MEMBER_TAG_DATA_FILE_NAME + "*" + PafBaseConstants.XML_EXT, Constants.MEMBER_TAG_DATA_FILE_NAME  + "*" + Constants.FILE_EXT_BINARY });
				
				//set text
				dlg.setText("Browse to Simple Member Data Import File");
				
				//open dialog
				String importFileLocation = dlg.open();
				
				if( importFileLocation != null ) {
					//create file
					File importFile = new File(importFileLocation);
					
					//if importFile exists, set text box with value
					if ( importFile != null && importFile.isFile() && importFile.canRead() ) {
						
						//set text on text widget
						importFileLocationText.setText(importFileLocation);
						
						//load simple member tag data file so user can select which member tags to load
						loadSimpleMemberTagDataFile(importFileLocation);
						
					} 
				}
			}
		});
		
		importDataButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				importMemberTagData();
			
//				refreshExportViewer();
				
//				addEllipseButtonToExportTable();

			}
		});
		
	}
		
	/**
	 * 
	 *  Loads the simple member tag data file from the file system into here
	 *
	 * @param importFileLocation
	 */
	protected void loadSimpleMemberTagDataFile(String importFileLocation) {

		//import simple member Tag Data array
		importedSimpleMemberTagDataAr = ImportExportUtil.importSimpleMemberTagData(importFileLocation);
	
		if ( importedSimpleMemberTagDataAr != null && importedSimpleMemberTagDataAr.length > 0 ) {
			
			//<app id, number of simple member tag data items for app>
			Map<String, Integer> appIdCountMap = new HashMap<String, Integer>();
			
			//map keyed on app id and value is a list of all simple member tag data items
			Map<String, List<SimpleMemberTagData>> simpleMemberTagDataMap = new HashMap<String, List<SimpleMemberTagData>>();
			
			//loop simple member tag data times and populdate list
			for (SimpleMemberTagData simpleMemberTagData : importedSimpleMemberTagDataAr ) {
				
				String appId = simpleMemberTagData.getApplicationName();
				
				//if null, continue to next record
				if ( appId == null ) {
					
					continue;
					
				}
				
				//get app id count
				Integer appIdCount = appIdCountMap.get(appId);
				
				//if null, set to zero
				if ( appIdCount == null ) {
					
					appIdCount = 0;
					
				}
				
				appIdCount++;
				
				//put count back into map
				appIdCountMap.put(appId, appIdCount);
				
				//get simple member tag data list
				List<SimpleMemberTagData> simpleMemberTagDataList = simpleMemberTagDataMap.get(appId);
				
				//if null, init
				if ( simpleMemberTagDataList == null ) {
					
					simpleMemberTagDataList = new ArrayList<SimpleMemberTagData>();
				}
				
				//add to list
				simpleMemberTagDataList.add(simpleMemberTagData);
				
				//push list into map
				simpleMemberTagDataMap.put(appId, simpleMemberTagDataList);
						
			}
			
			List<MemberTagInfo> memberTagInfoList = new ArrayList<MemberTagInfo>();
			
			//map to hold the member tag info, value = list of simple member tag data
			importedSimpleMemberTagDataMap = new HashMap<MemberTagInfo, List<SimpleMemberTagData>>();
			
			for (String appId : appIdCountMap.keySet()	) {
				
				Integer countPerAppDataSource = appIdCountMap.get(appId);
				
				//if null, set to zero
				if ( countPerAppDataSource == null ) {
					
					countPerAppDataSource = 0;
					
				}
				
				//create a member tag info object
				MemberTagInformation memberTagInformation = new MemberTagInformation();
				memberTagInformation.setAppName(appId);
				memberTagInformation.setAppMemberTagCount(countPerAppDataSource);
				
				//user member tag info object to create wrapper object
				MemberTagInfo memberTagInfo = new MemberTagInfo(memberTagInformation);
				
				//add to list
				memberTagInfoList.add(memberTagInfo);
				
				//add list to map
				importedSimpleMemberTagDataMap.put(memberTagInfo, simpleMemberTagDataMap.get(appId));
				
			}
			
			//set input in viewer
			importCheckboxTableViewer.setInput(memberTagInfoList.toArray(new MemberTagInfo[0]));
			
		}
		
		
	}

	/**
	 * 
	 * Sets the export file dir text box text
	 *
	 */
	private void popululateFormItems() {

		if ( this.serverName != null ) {
			
			importServerLabel.setText(serverName);
			exportServerLabel.setText(serverName);
			
		}
		
		if ( this.lastFileExportDir != null ) {
			
			exportFileDirTextBox.setText(this.lastFileExportDir);
			
		}
		
	}

	/**
	 * 
	 *  Adds listeners to the export widgets/viewers
	 *
	 */
	private void addExportListeners() {

		/**
		 * When export file dir text box is clicked on, set focuse to
		 * the browse button
		 */
		exportFileDirTextBox.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg0) {

				exportFileDirBrowseButton.setFocus();

			}
		});
		
		/**
		 * Catch when a user checks/unchecks and item in the viewer.  When checked,
		 * enabled the member tag filter button; when unchecked, disable the member
		 * tag filter button (but don't clear the filter members)
		 */
		exportCheckboxTableViewer.addCheckStateListener(new ICheckStateListener() {

			public void checkStateChanged(CheckStateChangedEvent checkEvent) {

				//get source; should be the checkbox table viewer
				Object sourceObject = checkEvent.getSource();

				//get element
				Object element = checkEvent.getElement();
			
				logger.info(checkEvent);
								
				//if source and element are correct classes
				if ( sourceObject instanceof CheckboxTableViewer && element instanceof MemberTagInfo ) {

					//cast
					CheckboxTableViewer viewer = (CheckboxTableViewer) sourceObject;
					
					//cast
					MemberTagInfo memberTagInfo = (MemberTagInfo) element;
					
					//if the dynamic button map contains the member tag, get button and set enabled
					if ( exportDynamicButtonsMap.containsKey(memberTagInfo )) {
						
						//get button from map
						Button button = exportDynamicButtonsMap.get(memberTagInfo);
						
						//if button is not null and button is not disposed
						if ( button != null && ! button.isDisposed() ) {
							
							button.setEnabled(viewer.getChecked(element));
							
						}
						
					}
					
				}				
				
			}
			
		});
		
		/**
		 * When checked, check all items in export checkbox table viewer and enabled
		 * all member tag filter buttons.
		 *  
		 * When unchecked, uncheck all items in export checkbox table viewer and disable 
		 * all member tag filter buttons.
		 */
		exportSelectDeselectButton.addSelectionListener(new SelectionAdapter() {

			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			
				exportCheckboxTableViewer.setAllChecked(exportSelectDeselectButton.getSelection());
				
				//enabled all or disable all member tag filter buttons based on selection
				for (Button button : exportDynamicButtonsMap.values()) {
					
					button.setEnabled(exportSelectDeselectButton.getSelection());
					
				}
				
			}
			
		});
		
		/**
		 * Opens a dialog directory for a user to select a directory
		 */
		exportFileDirBrowseButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				//get directory dialog
				DirectoryDialog dlg = new DirectoryDialog(exportFileDirBrowseButton.getShell());

				//if export file dir already has something in it
				if ( exportFileDirTextBox.getText().trim().length() != 0 ) {
					
					//create file ref
					File alreadyExistingDir = new File(exportFileDirTextBox.getText().trim());
					
					//if not null and is directory
					if ( alreadyExistingDir != null && alreadyExistingDir.isDirectory() ) {
					
						//set filter path
						dlg.setFilterPath(alreadyExistingDir.toString());	
						
					}
					
				}				
				
				dlg.setText("Browse to Member Tag Data Export Directory");
				
				//open dialog
				String exportDirectory = dlg.open();
				
				//if export directory exists, set text box with value
				if ( exportDirectory != null ) {
					
					exportFileDirTextBox.setText(exportDirectory);
					
				}
				
			}
		});
		
		/**
		 * Exports the checked member tags data to disk.
		 */
		exportDataButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent selectionEvent) {
				
				exportMemberTagData();
				
			}
		});
	}
	
	/**
	 * 
	 * Exports Member Tag Data to disk. Either xml or binary format.
	 *
	 */
	protected void exportMemberTagData() {

		
//		if export info isn't complete, return
		if ( ! verifyExportInformation() ) {
			return;
		}
		
		try {
			
			//get selected member tag info ar [ info to export ]
			final MemberTagInfo[] selectedMemberTagInfoAR = Arrays.asList(exportCheckboxTableViewer.getCheckedElements()).toArray(new MemberTagInfo[0]);
			
			//get export file type { xml or binary }
			final ImportExportFileType exportFileType = getFileType(exportFileFormatCombo.getText());					
			
			//get export dir
			final String exportDir = exportFileDirTextBox.getText().trim();
			
			//create a progress mon dialog
			ProgressMonitorDialog pd = new ProgressMonitorDialog(this.getShell());
			
			//exports the member tag data to disk from server
			pd.run(true, true, new IRunnableWithProgress() {
	
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				
					//start task
					monitor.beginTask("Exporting Member Tag Data...", IProgressMonitor.UNKNOWN);
				
					//set sub task
					monitor.subTask("Getting Member Tag Data from the server");
					
					//get server url for webservice
					String url = PafServerUtil.getServerWebserviceUrl(serverName);		
					
					//create request
					PafFilteredMbrTagRequest request = new PafFilteredMbrTagRequest();
									
					//get current session
					ServerSession serverSession = SecurityManager.getSession(url);
					
					if ( serverSession != null ) {
						
						//populate client id and security token
						request.setClientId(serverSession.getClientId());
						
						if ( serverSession.getSecurityToken() != null ) {
							
							request.setSessionToken(serverSession.getSecurityToken());
						}
									
					}
					
					//create filter set to hold all the Member Tag Filters
					Set<PafMbrTagFilter> filterSet = new HashSet<PafMbrTagFilter>();
					
					//populate filter set with member tag filters
					for (MemberTagInfo memberTagInfo : selectedMemberTagInfoAR ) {
							
						//create new filter
						PafMbrTagFilter filter = new PafMbrTagFilter();
						
						//set app name
						filter.setAppName(memberTagInfo.getAppId());
						
//						if no filters exists, use all member tags
						if ( memberTagInfo.getMemberTagFilters(false) == null ) {
						
							if ( memberTagInfo.getMemberTagNames() != null ) {
								
								filter.getMemberTagNames().addAll(Arrays.asList(memberTagInfo.getMemberTagNames()));
								
							}
							
						//if filters exists, only use filtered member tags
						} else {
							
							String[] memberTagFiltersAr = memberTagInfo.getMemberTagFilters(false);
							
							filter.getMemberTagNames().addAll(Arrays.asList(memberTagFiltersAr));
							
						}				
					
						//add filter to set
						filterSet.add(filter);						
						
					}
					
					//set member tag filters
					request.getMemberTagFilters().addAll(filterSet);
					
					try {
						
						//get response
						PafGetMemberTagDataResponse response = WebServicesUtil.getPafService(url).exportMemberTagData(request);
						
						List<SimpleMemberTagData> simpleMemberTagDataList = response.getMemberTagData();
												
						if ( simpleMemberTagDataList.size() > 0 ) {							
						
							//get simple member tag data aray from server response
							SimpleMemberTagData[] simpleMemberTagDataAr = simpleMemberTagDataList.toArray(new SimpleMemberTagData[0]);
	
							//if user cancelled
							if ( monitor.isCanceled()) {
								
								throw new InterruptedException("Exporting of Member Tag Data was successfully cancelled.");
								
							}
						
							//if member tag data exists
							if ( simpleMemberTagDataAr != null ) {
								
								//start 2nd sub task
								monitor.subTask("Saving Member Tag Data to disk");
													
								//export member tag data to disk
								ImportExportUtil.exportMemberTagData(simpleMemberTagDataAr, exportDir, exportFileType);
															
							}
							
						} else {
							
							throw new InterruptedException("No Member Tag Data to export.");
						}
						
					} catch (RemoteException e) {
						
						logger.error("Remote Exception: " + e.getMessage());
						throw new InvocationTargetException(e);
						
					} catch (PafSoapException_Exception e) {
						
						logger.error("Paf Soap Exception: " + e.getMessage());
						throw new InvocationTargetException(e);
						
					} catch (Exception e) {
						
						logger.error("Exception: " + e.getMessage());
						throw new InvocationTargetException(e);
						
					} finally {
					
						monitor.done();
						
					}
									
				}
				
			});
		
			//display a success message dialog
			MessageDialog.openInformation(this.getShell(), Constants.DIALOG_INFO_HEADING, "Successfully exported member tag data to directory " + exportDir + ".");
			okPressed();
			
		} catch (InterruptedException e1) {
			
			e1.printStackTrace();
			
			//do nothing
			//MessageDialog.openInformation(this.getShell(), Constants.DIALOG_INFO_HEADING, "Operation was cancelled by user.");
			
		} catch (InvocationTargetException e1) {
						
			//if interrupted exception { cancelled }
			if ( e1.getTargetException() != null && e1.getTargetException() instanceof InterruptedException ) {
				
				//cast
				InterruptedException ie = (InterruptedException) e1.getTargetException();
				
				//tell user operation was successfully cancelled
				MessageDialog.openInformation(this.getShell(), Constants.DIALOG_INFO_HEADING, ie.getMessage());			
				
			} else {
			
				//error occurred
				e1.printStackTrace();
			
				//get error from inside
				String error = e1.getTargetException().getMessage();
				
				//create string buffer for error message
				StringBuffer errMsgBuff = new StringBuffer( "There was a problem exporting member tag data");
				
				//if error is not null, append to error message
				if ( error != null ) {
					
					errMsgBuff.append(": " + error);
					
				}
				
				//inform user of error
				MessageDialog.openInformation(this.getShell(), Constants.DIALOG_ERROR_HEADING, errMsgBuff.toString());
				
			}
			
		}
			
	}

	/**
	 * 
	 * Imports the member tag data
	 *
	 */
	protected void importMemberTagData() {

		if ( ! verifyImportInformation() ) {
			return;
		}
		
		if ( MessageDialog.openConfirm(getShell(), Constants.DIALOG_QUESTION_HEADING, "Import Selected Member Tag Data Items?") ) {
			
			//get selected member tag info ar
			MemberTagInfo[] selectedMemberTagInfoAR = Arrays.asList(importCheckboxTableViewer.getCheckedElements()).toArray(new MemberTagInfo[0]);
			
			//create set of simple member tag data itmes
			Set<SimpleMemberTagData> importSimpleMemberTagDataSet = new HashSet<SimpleMemberTagData>();
			
			if ( selectedMemberTagInfoAR != null ) {
				
				//loop member tag infos and add simple member tag datas to set
				for (MemberTagInfo selectedMemberTagInfo : selectedMemberTagInfoAR ) {
					
					List<SimpleMemberTagData> simpleMemberTagDataToImportList = importedSimpleMemberTagDataMap.get(selectedMemberTagInfo);
					
					if ( simpleMemberTagDataToImportList != null ) {

						//if override, replace all simple member tag data with app ids
						if ( selectedMemberTagInfo.getOverrideAppId() != null ) {
							
							for ( SimpleMemberTagData overrideSimpleMemberTagData : simpleMemberTagDataToImportList ) {
								
								overrideSimpleMemberTagData.setApplicationName(selectedMemberTagInfo.getOverrideAppId());
								
							}
							
						}
						
						importSimpleMemberTagDataSet.addAll(simpleMemberTagDataToImportList);
						
					}
					
				}
					
				
			}
			
			final SimpleMemberTagData[] simpleMemberTagDataToImportAr = importSimpleMemberTagDataSet.toArray(new SimpleMemberTagData[0]);
						
//			final String importFileName = importFileLocationText.getText();
//			
//			//get export file type { xml or binary }
//			final ImportExportFileType importFileType;
//			
//			if ( importFileName.endsWith(Constants.XML_EXT)) {
//				
//				importFileType = ImportExportFileType.XML;
//				
//			} else if ( importFileName.endsWith(Constants.FILE_EXT_BINARY)) {
//				
//				importFileType = ImportExportFileType.Binary;
//				
//			} 								
		
//				//create a progress mon dialog
//				ProgressMonitorDialog pd = new ProgressMonitorDialog(this.getShell());
//				
//				//exports the member tag data to disk from server
//				pd.run(true, true, new IRunnableWithProgress() {
			Display.getDefault().asyncExec(new Runnable() {
				
				@Override
				public void run() {
//					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				
					//start task
//						monitor.beginTask("Importing Member Tag Data...", IProgressMonitor.UNKNOWN);
//					
//						//set sub task
//						monitor.subTask("Sending Member Tag Data to server");
//						
					//get server url for webservice
					String url = PafServerUtil.getServerWebserviceUrl(serverName);		
					
					//create request
					PafImportMemberTagRequest request = new PafImportMemberTagRequest();
									
					//get current session
					ServerSession serverSession = SecurityManager.getSession(url);
					
					if ( serverSession != null ) {
						
						//populate client id and security token
						request.setClientId(serverSession.getClientId());
						
						if ( serverSession.getSecurityToken() != null ) {
							
							request.setSessionToken(serverSession.getSecurityToken());
						}
									
					}
										
					if ( simpleMemberTagDataToImportAr != null ) {
						
						//set member tag data items
						request.getMemberTagData().addAll(Arrays.asList(simpleMemberTagDataToImportAr));
						
					}
					
					try {
						
						//have server import member tag data
						PafSuccessResponse response = WebServicesUtil.getPafService(url).importMemberTagData(request);
						
						//if response is null or if not successful, throw exception
						if ( response == null || ! response.isSuccess() ) {
							String msg = "There was a problem importing member tag data to server " + serverName + ".  Please check the server log file.";
							throw new Exception(msg);
					
						}
																	
						//display success message dialog
						MessageDialog.openInformation(getShell(), Constants.DIALOG_INFO_HEADING, "Successfully imported member tag data to server " + serverName + ".");
						okPressed();
									
					} catch (RemoteException e) {
						
						logger.error("Remote Exception: " + e.getMessage());
						MessageDialog.openError(getShell(), "Membertag Improt Error", e.getMessage());
						
					} catch (PafSoapException_Exception e) {
						
						logger.error("Paf Soap Exception: " + e.getMessage());
						MessageDialog.openError(getShell(), "Membertag Improt Error", e.getMessage());
						
					} catch (Exception e) {
						
						logger.error("Exception: " + e.getMessage());
						MessageDialog.openError(getShell(), "Membertag Improt Error", e.getMessage());
						
					} 
									
				}
				
			});
			
		
//			} 
//			catch (InterruptedException e1) {
//				
//				//do nothing
//				//MessageDialog.openInformation(this.getShell(), Constants.DIALOG_INFO_HEADING, "Operation was cancelled by user.");
//				
//			} catch (InvocationTargetException e1) {
//							
//				e1.printStackTrace();
//				
//				MessageDialog.openInformation(this.getShell(), Constants.DIALOG_ERROR_HEADING, "There was a problem importing member tag data: " + e1.getMessage());
//							
//			}
		}
	}
	
	/**
	 * 
	 *  Determines the ImportExportFileType based on str.
	 *  
	 *  Default is Binary
	 *
	 * @param fileTypeStr file type
	 * @return	Import Export File Type 
	 */
	private ImportExportFileType getFileType(String fileTypeStr) {

		//default to binary
		ImportExportFileType fileType = ImportExportFileType.Binary;;
		
		//if not null
		if ( fileTypeStr != null ) {
			
			if ( fileTypeStr.equalsIgnoreCase(ImportExportFileType.Binary.toString()) ) {
				
				fileType = ImportExportFileType.Binary;
				
			} else if ( fileTypeStr.equalsIgnoreCase(ImportExportFileType.XML.toString()) ){ 

				fileType = ImportExportFileType.XML;
				
			} 
			
		}			
		
		return fileType;
	}

	/**
	 * 
	 * Verfies member tag import information is valid.
	 * 
	 * The checks are:
	 * 1. is server running
	 * 2. ensure at least one item is checked
	 *
	 * @return true if all conditions fail.
	 */
	private boolean verifyImportInformation() {
		
		String url = PafServerUtil.getServerWebserviceUrl(serverName);
		
		//if server is running
		if ( ! ServerMonitor.getInstance().isServerRunning(url) ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Server problem: Member Tag Data can not be imported if the server is not running.");
			
			return false;
		}

		String importFileName = importFileLocationText.getText().trim();
		
		//if export path is empty
		if ( importFileName.length() == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please select an import member tag data file.");
			return false;
			
		}
		
		//if no items are checked
		if ( importCheckboxTableViewer.getCheckedElements().length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please check at least one item to import.");
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * Verfies member tag export information is valid.
	 * 
	 * The checks are:
	 * 1. is server running
	 * 2. ensure at least one item is checked
	 * 3. export dir must be specified
	 * 4. export dir must be a directory and writeable
	 *
	 * @return true if all conditions fail.
	 */
	private boolean verifyExportInformation() {
		
		String url = PafServerUtil.getServerWebserviceUrl(serverName);
		
		//if server is running
		if ( ! ServerMonitor.getInstance().isServerRunning(url) ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Server problem: Member Tag Data can not be exported if the server is not running.");
			
			return false;
		}
		
		//if no items are checked
		if ( exportCheckboxTableViewer.getCheckedElements().length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please check at least one item to export.");
			
			return false;
		}
		
		String exportFileDirPath = exportFileDirTextBox.getText();
		
		//if export path is empty
		if ( exportFileDirPath.trim().length() == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please select a directory to export the member tag data items to.");
			return false;
			
		}
		
		File exportFileDir = new File(exportFileDirPath);
		
		//if directory and can write to directory
		if ( ! exportFileDir.isDirectory() || ! exportFileDir.canWrite() ) {

			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Can't write to directory: " + exportFileDirPath);
			
			return false;
		}
		
		//all conditions are now true for a exporting
		return true;
	}

	private void addEllipseButtonToExportTable() {

		if ( memberTagInfoAr != null && memberTagInfoAr.length > 0 ) {
			
			for (Button buttonToDispose : exportDynamicButtonsMap.values()) {
				
				if ( buttonToDispose != null && ! buttonToDispose.isDisposed() ) {
					
					buttonToDispose.dispose();
					
				}
				
			}
			
			//clear map
			exportDynamicButtonsMap.clear();
			
			//get table items
			TableItem [] items = exportTable.getItems();
			
			for (int i = 0; i < memberTagInfoAr.length; i++ ) {
				
//				create new table editor
				TableEditor editor = new TableEditor (exportTable);
				
//				new button
				final Button button = new Button (exportTable, SWT.PUSH);
				button.setText("...");
				button.setData(new Integer(i));				
				button.pack();
				editor.minimumWidth = button.getSize ().x;
				editor.horizontalAlignment = SWT.RIGHT;
				editor.setEditor (button, items[i], 4);	
				
				final int currentNdx = i;
				
				button.addSelectionListener(new SelectionAdapter() {

					/* (non-Javadoc)
					 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
					 */
					@Override
					public void widgetSelected(SelectionEvent selectionEvent) {

						StringArraySelectionDialog dialog = new StringArraySelectionDialog(button.getShell(), 
								"Member Tag Filters",
								memberTagInfoAr[currentNdx].getMemberTagNames(), 
								memberTagInfoAr[currentNdx].getMemberTagFilters(false));						
						
						if ( dialog.open() == IDialogConstants.OK_ID ) {
							
							String[] selectedMemberTags = dialog.getSelectedItems();
																							
							memberTagInfoAr[currentNdx].setMemberTagFilters(selectedMemberTags);
							
							exportCheckboxTableViewer.refresh();
							
							
						}
						
					}
						
				});
				
				button.setEnabled(false);
				
				exportDynamicButtonsMap.put(memberTagInfoAr[currentNdx], button);
				
			}
		}
		
	}

	/**
	 * 
	 *  Based on transtype, folder gets selected
	 *
	 */
	private void updateTabFolderSelection() {

		switch (transType) {
					
			case Import:
					tabFolder.setSelection(importTabItem);
					break;
					
			case Export:
					tabFolder.setSelection(exportTabItem);
					break;
					
		}
		
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800,600);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Import/Export Member Tag Data");
	}

}

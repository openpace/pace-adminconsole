/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.masks.TextEditMask;
import com.pace.admin.global.prefrences.SettingsPreferences;
import com.pace.admin.global.util.ClipboardUtil;
import com.pace.admin.global.util.EditorControlUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.SystemUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.widgets.LinkMessageDialog;
import com.pace.admin.servers.job.GenerateSupportRequestJob;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.base.PafException;
import com.pace.base.jira.JiraUtil;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.DataHandlerPaceProjectUtil;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.PafZipUtil;
import com.pace.server.client.PafException_Exception;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.SupportRequest;
import com.pace.server.client.SupportResponse;

public class GenerateSupportRequestDialog extends Dialog {
	private static Logger logger = Logger.getLogger(GenerateSupportRequestDialog.class);
	private final String PREF_NODE = "com.pace.admin.servers.dialog.GenerateSupportRequest";
	private final String ERROR_MSG = "There was a problem getting the Pace support request files.  Please check the Pace Server log.";
	private final String NO_EMAIL_CLIENT_MSG = "Could not find a compatible desktop email client.";
	private final String CONNECTION_ERR_MSG = "An issue cannot be automatically created due to a connectivity issue.  "
			+ "To submit the issue please send an email message to <a href=\"mailto:%s?subject=%s&body=%s\">%s</a> and make sure to include the compressed "
			+ "<a href=\"file://///%s\">diagnostic files</a>.";
	private final String EMAIL_BODY_FORMAT = "***\r\n*** THE PACE DIAGNOSTIC FILES HAVE BEEN COMPRESSED AND WRITTEN TO: "
			+ "file://///%s. \r\n*** PLEASE PRESS CTRL-V or [PASTE] TO ADD THESE FILES AS AN ATTACHMENT TO THIS EMAIL."
			+ "\r\n***\r\n\n\n\n%s";
	//private final String SIZE_KEY = "DialogSize";
	private final String SUPPORT_EMAIL = Constants.PACE_SUPPORT_EMAIL;
	private Shell shell;
	private SettingsPreferences preferences;
	private Composite container; 
	private boolean talkedToServer;
	private String title;
	private Text txtCustomerName;
	private Text txtEmail;
	private Text txtName;
	private Text txtPhone;
	private Text txtIssueSummary;
	private Text txtIssueDesc;
	private Text txtServerDetails;
	private TextEditMask phoneMask;
	private CheckboxTableViewer attachmentsCheckboxViewer;
	private Table attachmentsCheckbox;
	private Button okButton;
	private Button btnIncludeEsbScripts;
	private Combo cboSeverity;
	private Combo cboServer;
	private String serverVersion;
	private Map<String, File> attachments;
	private File tempDirectory;
	private List<String> verboseFileNames;
	private boolean jiraAlive;
	private Text txtAcDetails;
	
	public GenerateSupportRequestDialog(Shell parentShell, String title) {
		
		super(parentShell);
		setShellStyle(SWT.BORDER | SWT.CLOSE | SWT.RESIZE | SWT.SYSTEM_MODAL);
		this.title = title;
		this.shell = parentShell;
		attachments = new LinkedHashMap<String, File>(15);
		preferences = new SettingsPreferences(PREF_NODE);
	}
	
	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, true)); 
		
		Composite compositeName = new Composite(container, SWT.NONE);
		GridData gd_compositeName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositeName.widthHint = 228;
		compositeName.setLayoutData(gd_compositeName);
		compositeName.setLayout(new GridLayout(1, true));
		
		String nameToolTip = "Enter your name (Required)";
		Label lblName = new Label(compositeName, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblName.setToolTipText(nameToolTip);
		lblName.setText("Name");
		FontData boldFontData = lblName.getFont().getFontData()[0];
		Font boldFont = new Font(shell.getDisplay(), new FontData(boldFontData.getName(), boldFontData.getHeight(), SWT.BOLD));
		lblName.setFont(boldFont);
		
		txtName = new Text(compositeName, SWT.BORDER);
		txtName.setToolTipText(nameToolTip);
		txtName.setTextLimit(50);
		txtName.setData("name");
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtName.widthHint = 98;
		txtName.setLayoutData(gd_txtName);
		
		Composite compositeEmail = new Composite(container, SWT.NONE);
		GridData gd_compositeEmail = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositeEmail.widthHint = 242;
		compositeEmail.setLayoutData(gd_compositeEmail);
		compositeEmail.setLayout(new GridLayout(1, true));
		
		String mailToolTip = "Enter your corporate email address (Required)";
		Label lblEmail = new Label(compositeEmail, SWT.NONE);
		lblEmail.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblEmail.setToolTipText(mailToolTip);
		lblEmail.setText("Email Address");
		lblEmail.setFont(boldFont);
		
		txtEmail = new Text(compositeEmail, SWT.BORDER);
		txtEmail.setToolTipText(mailToolTip);
		txtEmail.setTextLimit(50);
		txtEmail.setData("email");
		txtEmail.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite compositeCustomerName = new Composite(container, SWT.NONE);
		compositeCustomerName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		compositeCustomerName.setLayout(new GridLayout(1, true));
		
		String customerNameToolTip = "Enter your company name (Required)";
		Label lblCustomerName = new Label(compositeCustomerName, SWT.NONE);
		lblCustomerName.setToolTipText(customerNameToolTip);
		lblCustomerName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblCustomerName.setText("Customer Name");
		lblCustomerName.setFont(boldFont);
		
		txtCustomerName = new Text(compositeCustomerName, SWT.BORDER);
		txtCustomerName.setToolTipText(customerNameToolTip);
		txtCustomerName.setTextLimit(50);
		txtCustomerName.setData("customername");
		txtCustomerName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite compositePhone = new Composite(container, SWT.NONE);
		GridData gd_compositePhone = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositePhone.widthHint = 231;
		compositePhone.setLayoutData(gd_compositePhone);
		compositePhone.setLayout(new GridLayout(1, true));
		
		String phoneToolTip = "Enter your phone number";
		Label lblPhone = new Label(compositePhone, SWT.NONE);
		lblPhone.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblPhone.setToolTipText(phoneToolTip);
		lblPhone.setText("Phone");
		

		txtPhone = new Text(compositePhone, SWT.BORDER);
		txtPhone.setToolTipText(phoneToolTip);
		txtPhone.setTextLimit(15);
		txtPhone.setData("phone");
		txtPhone.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		phoneMask = new TextEditMask(txtPhone);
		phoneMask.setMask("(###) ###-####");
		phoneMask.setPlaceholder('_');
		phoneMask.setFireChangeOnKeystroke(true);
		
		List<String> servers = new ArrayList<String>(10);
		Set<PafServer> runningServerSet = PafServerUtil.getRunningPafServers();
		if(runningServerSet != null && runningServerSet.size() > 0){
			for(PafServer server : runningServerSet){
				servers.add(server.getName());
			}
		}
		
		Composite compositeSeverity = new Composite(container, SWT.NONE);
		compositeSeverity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		compositeSeverity.setLayout(new GridLayout(1, true));
		
		String severityToolTip = "Blocker:  Blocks development and/or testing work, production could not run. \r\nCritical: Crashes, loss of data, severe memory leak. \r\nMajor: Major loss of function.\r\nTrivial: Cosmetic problem like misspelled words or misaligned text.";
		Label lblSeverity = new Label(compositeSeverity, SWT.NONE);
		lblSeverity.setToolTipText(severityToolTip);
		lblSeverity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblSeverity.setText("Issue Severity");
		lblSeverity.setFont(boldFont);
		
		cboSeverity = new Combo(compositeSeverity, SWT.READ_ONLY);
		cboSeverity.setToolTipText(severityToolTip);
		cboSeverity.setItems(new String[] {"Blocker", "Critical", "Major", "Trivial"});
		cboSeverity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cboSeverity.select(3);
		
		Composite compositeServer = new Composite(container, SWT.NONE);
		GridData gd_compositeServer = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositeServer.widthHint = 226;
		compositeServer.setLayoutData(gd_compositeServer);
		compositeServer.setLayout(new GridLayout(1, true));
		
		String serverToolTip = "Select the Pace server where the issue is occuring";
		Label lblServer = new Label(compositeServer, SWT.NONE);
		GridData gd_lblServer = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblServer.widthHint = 179;
		lblServer.setLayoutData(gd_lblServer);
		lblServer.setToolTipText(serverToolTip);
		lblServer.setText("Pace Server");
		lblServer.setFont(boldFont);
		
		cboServer = new Combo(compositeServer, SWT.READ_ONLY);
		cboServer.setToolTipText(serverToolTip);
		cboServer.setItems(new String[] {});
		cboServer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		EditorControlUtil.addItems(cboServer, servers.toArray(new String[servers.size()]));
		cboServer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if(tempDirectory != null){
					org.apache.commons.io.FileUtils.deleteQuietly(tempDirectory);
				}
				attachmentsCheckbox.removeAll();
				String serverName = cboServer.getText();
				final String url = PafServerUtil.getServerWebserviceUrl(serverName);
				final boolean verbose = btnIncludeEsbScripts.getSelection(); 

				//Show the hourglass
				BusyIndicator.showWhile(Display.getDefault(), new Runnable(){

					public void run(){
						try {
							SupportResponse response = queryServer(url, verbose);

							//if not success, throw exception
							if (response == null ||  response.getResponseCode() != 0) {
								talkedToServer = false;
								throw new Exception();
							}

							talkedToServer = true;
							serverVersion = response.getServerPaceVersion();
							verboseFileNames = response.getVerboseFileNames();
							
							//create the enviro string.
							final StringBuilder server = new StringBuilder();
							server.append("Pace Version: ").append(serverVersion).append("\r\n")
							.append("OS Info: ").append(response.getServerPlatform()).append(" : " ).append(response.getServerPlatformVersion()).append(" : " ).append(response.getServerBitness()).append("\r\n")
							.append("JDK Version: ").append(response.getServerJdkVersion()).append("\r\n")
							.append("Essbase Version: ").append(response.getEssbaseVersion()).append("\r\n");
							
							final StringBuilder ac = new StringBuilder();
							ac.append("Pace Version: ").append(SystemUtil.getAcVersion()).append("\r\n")
							.append("OS Info: ").append(SystemUtil.getOs()).append(" : " ).append(SystemUtil.getOsVersion()).append(" : " ).append(SystemUtil.getOsBitness()).append("\r\n")
							.append("JDK Version: ").append(SystemUtil.getJdkVersion()).append("\r\n");
							//Clear the map of log files
							attachments.clear();
							//get the data handler and convert it to a zip file
							DataHandler dataHandler = response.getDataHandler();
							File zipFile = DataHandlerPaceProjectUtil.convertDataHandlerToFile(dataHandler, ".zip");
							//If we have a valid zip
							if(zipFile != null){
								//Create / get a temp directory
								tempDirectory = FileUtils.createTempDirectory();
								//Unzip the zip to the temp dir.
								PafZipUtil.unzipFile(zipFile.getPath(), tempDirectory.getPath());
								//Add the files to the map
								for(File f :tempDirectory.listFiles()){
									attachments.put(f.getName(), f);
								}

							}
							//Update UI 
							Display.getDefault().syncExec(new Runnable() {

								@Override
								public void run() {
									txtServerDetails.setText(server.toString());
									txtAcDetails.setText(ac.toString());
									if(attachments != null && attachments.size() > 0){
										String[] files = new String[attachments.size()];
										int i = 0;
										for(Map.Entry<String, File> entry : attachments.entrySet() ){
											files[i] = entry.getKey();
											i++;
										}
										Arrays.sort(files, String.CASE_INSENSITIVE_ORDER);
										EditorControlUtil.addElements(attachmentsCheckboxViewer, files);
										EditorControlUtil.setCheckedElements(attachmentsCheckboxViewer, files);
									}
									updateOkButtonState();
								}

							});

							zipFile.delete();
						} catch (Exception e) {

							//display error if an exception was thrown
							Display.getDefault().asyncExec(new Runnable() {

								@Override
								public void run() {
									MessageDialog.openError(shell,	"Error", ERROR_MSG);

									updateOkButtonState();
								}
							});			
						} 
					}
				});
			}
		});
		
		Composite compositeIssueSummary = new Composite(container, SWT.NONE);
		compositeIssueSummary.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		compositeIssueSummary.setLayout(new GridLayout(1, false));
		
		String issueSummary = "Enter a brief summary of the issue";
		Label lblIssueSummary = new Label(compositeIssueSummary, SWT.NONE);
		lblIssueSummary.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIssueSummary.setToolTipText(issueSummary);
		lblIssueSummary.setText("Issue Summary");
		lblIssueSummary.setFont(boldFont);
		
		txtIssueSummary = new Text(compositeIssueSummary, SWT.BORDER);
		txtIssueSummary.setTextLimit(100);
		txtIssueSummary.setToolTipText(issueSummary);
		txtIssueSummary.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite compositeIssueDesc = new Composite(container, SWT.NONE);
		compositeIssueDesc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		compositeIssueDesc.setLayout(new GridLayout(1, false));
		
		String issueDescToolTip = "Enter a detailed description about the issue.";
		Label lblIssueDescription = new Label(compositeIssueDesc, SWT.NONE);
		lblIssueDescription.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblIssueDescription.setToolTipText(issueDescToolTip);
		lblIssueDescription.setText("Issue Description");
		lblIssueDescription.setFont(boldFont);
		
		txtIssueDesc = new Text(compositeIssueDesc, SWT.BORDER | SWT.MULTI);
		txtIssueDesc.setTextLimit(1000);
		txtIssueDesc.setToolTipText(issueDescToolTip);
		GridData gd_txtIssueDesc = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_txtIssueDesc.heightHint = 62;
		txtIssueDesc.setLayoutData(gd_txtIssueDesc);
		
		Composite compositeEnvDetails = new Composite(container, SWT.NONE);
		GridData gd_compositeEnvDetails = new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1);
		gd_compositeEnvDetails.heightHint = 102;
		compositeEnvDetails.setLayoutData(gd_compositeEnvDetails);
		compositeEnvDetails.setLayout(new GridLayout(2, true));
		
		String acDetailsToolTip = "Current Admin Console Details";
		String serverDetailsToolTip = "Selected Pace Server Details";
		Label lblEnviormentDetails = new Label(compositeEnvDetails, SWT.NONE);
		lblEnviormentDetails.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblEnviormentDetails.setText("Server Environment");
		lblEnviormentDetails.setToolTipText(serverDetailsToolTip);

		Label lblAcDetails = new Label(compositeEnvDetails, SWT.NONE);
		lblAcDetails.setToolTipText(acDetailsToolTip);
		lblAcDetails.setText("AC Environment");
		
		txtServerDetails = new Text(compositeEnvDetails, SWT.BORDER | SWT.READ_ONLY | SWT.MULTI);
		txtServerDetails.setToolTipText(serverDetailsToolTip);
		GridData gd_txtServerDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_txtServerDetails.heightHint = 54;
		txtServerDetails.setLayoutData(gd_txtServerDetails);
		
		txtAcDetails = new Text(compositeEnvDetails, SWT.BORDER | SWT.READ_ONLY | SWT.MULTI);
		txtAcDetails.setToolTipText(acDetailsToolTip);
		txtAcDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		Composite compositeAttachments = new Composite(container, SWT.NONE);
		GridData gd_compositeAttachments = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1);
		gd_compositeAttachments.heightHint = 106;
		compositeAttachments.setLayoutData(gd_compositeAttachments);
		compositeAttachments.setLayout(new GridLayout(1, false));
		
		String attachmentToolTip = "File(s) to be added to the support ticket";
		Label lblAttachments = new Label(compositeAttachments, SWT.NONE);
		lblAttachments.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblAttachments.setToolTipText(attachmentToolTip);
		lblAttachments.setText("Atachments");
		
		attachmentsCheckboxViewer = CheckboxTableViewer.newCheckList(compositeAttachments, SWT.BORDER | SWT.FULL_SELECTION);
		attachmentsCheckbox = attachmentsCheckboxViewer.getTable();
		attachmentsCheckbox.setToolTipText(attachmentToolTip);
		GridData gd_attachmentsCheckbox = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_attachmentsCheckbox.heightHint = 85;
		attachmentsCheckbox.setLayoutData(gd_attachmentsCheckbox);
		
		btnIncludeEsbScripts = new Button(container, SWT.CHECK);
		btnIncludeEsbScripts.setToolTipText("Return detailed information about current Essbase server");
		btnIncludeEsbScripts.setData("verbose");
		btnIncludeEsbScripts.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String serverName = cboServer.getText();
				final String url = PafServerUtil.getServerWebserviceUrl(serverName);
				final boolean verbose = btnIncludeEsbScripts.getSelection(); 
				preferences.saveBooleanSetting(btnIncludeEsbScripts, verbose);
				if(verbose){
					if(!talkedToServer) return;
					BusyIndicator.showWhile(Display.getDefault(), new Runnable(){
						@Override
						public void run() {
							File temp = FileUtils.createTempDirectory();
							try {
								//Get the log files from the server
								SupportResponse response = queryServer(url, verbose);
								//Get the data handler
								DataHandler dataHandler = response.getDataHandler();
								//Convert the data handler to a file.
								File zipFile = DataHandlerPaceProjectUtil.convertDataHandlerToFile(dataHandler, ".zip");
								//Only process 
								if(zipFile != null){
									//Create a list to hold the file names.
									List<String> newFiles = new ArrayList<String>(2);
									//Unzip to a temp path
									PafZipUtil.unzipFile(zipFile.getPath(), temp.getPath());
									//Loop the files in the temp directory
									for(File f :temp.listFiles()){
										String fileName = f.getName();
										//
										if(verboseFileNames.contains(fileName)){
											//Create a file in the existing temp dir
											File newTempFile = new File(tempDirectory + File.separator + fileName);
											//Copy the file from current temp dir to the "master" temp dir.
											org.apache.commons.io.FileUtils.copyFile(f, newTempFile);
											//add the logs to the log file map
											attachments.put(newTempFile.getName(), newTempFile);
											//add the logs to a temp list, which is used to populate the checkbox list.
											newFiles.add(fileName);
										}
										
									}
									//purge the file
									org.apache.commons.io.FileUtils.deleteQuietly(zipFile);
									//Perform the UI updates.
									EditorControlUtil.addElements(attachmentsCheckboxViewer, newFiles.toArray(new String[newFiles.size()]));
									EditorControlUtil.checkElements(attachmentsCheckboxViewer, newFiles.toArray(new String[newFiles.size()]));
								}
								
							} catch (Exception e1) {
								logger.error(e1.getMessage());
								MessageDialog.openError(shell,	"Error", ERROR_MSG);
								updateOkButtonState();
							} finally {
								org.apache.commons.io.FileUtils.deleteQuietly(temp);
							}
							
						}
						
					});
				} else {
					EditorControlUtil.removeElements(attachmentsCheckboxViewer, verboseFileNames.toArray(new String[verboseFileNames.size()]));
					for(String file : verboseFileNames){
						attachments.remove(file);
					}
				}
			}
		});
		btnIncludeEsbScripts.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		btnIncludeEsbScripts.setText("Include Essbase Scripts");

		
		Button cmdUpload = new Button(container, SWT.NONE);
		cmdUpload.setToolTipText("Add additional files to the support ticket");
		cmdUpload.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Shell shell = new Shell(Display.getDefault());
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setText("Select file to add...");
				fd.setFilterExtensions(new String[] { "*.*" });
				String file = fd.open();
				if(file != null){
					File newFile = new File(file);
					
					
					if(!attachments.containsKey(newFile.getName())){
						attachments.put(newFile.getName(), newFile);
						EditorControlUtil.addElements(attachmentsCheckboxViewer, new Object[]{newFile.getName()});
						EditorControlUtil.checkElement(attachmentsCheckboxViewer, newFile.getName());
					}else {
						MessageDialog.openError(shell,	"Error", String.format("A file with the name: %s already exists.  Please rename the file and try again.", new Object[]{newFile.getName()}));
					}
					
				}
			}
		});
		cmdUpload.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		cmdUpload.setText("Add File");

		shell.addListener(SWT.CANCEL, new Listener(){
			public void handleEvent(Event event){
				cleanupTempDirectory();
			}
		});
		
		jiraAlive = JiraUtil.isJiraServerAlive();
		
		loadSettings();
		
		addValidationListeners();
		
		return container;
	}
	
	private void loadSettings(){
		logger.info("Loading settings from: " + preferences.getFileLocation());
		
		txtName.setText(preferences.getStringSetting(txtName));
		txtCustomerName.setText(preferences.getStringSetting(txtCustomerName));
		txtEmail.setText(preferences.getStringSetting(txtEmail));
		txtPhone.setText(preferences.getStringSetting(txtPhone));
		btnIncludeEsbScripts.setSelection(preferences.getBooleanSetting(btnIncludeEsbScripts));
	}
	
	private SupportResponse queryServer(String url, boolean verbose) throws PafException_Exception, PafSoapException_Exception{
		//get session
		ServerSession serverSession = SecurityManager.getSession(url);
		
		//create request
		SupportRequest request = new SupportRequest();

		//set parms
		request.setClientId(serverSession.getClientId());
		request.setVerbose(verbose);
		if ( serverSession.getSecurityToken() != null ) {
		
			request.setSessionToken(serverSession.getSecurityToken());
			
		}
						
		PafService service = WebServicesUtil.getPafService(url);
		SupportResponse response = null;
		if( service != null ) {
			//try to import cell notes
			response = service.getSupportFiles(request);
				
			//if not success, throw exception
			if ( response.getResponseCode() != 0) {
				talkedToServer = false;
			}
		}
		return response;
	}
	
	/**
	 * Generate a Pace support request
	 */
	private void submitRequest(){

		final String firstName = txtName.getText();
		final String customerName =  txtCustomerName.getText();
		final String email =  txtEmail.getText();
		final String phone = txtPhone.getText();
		final String summary = txtIssueSummary.getText();
		final String description = txtIssueDesc.getText();
		final String environment = String.format("Server Environment:\r\n%s\r\nAC Environment:\r\n%s", txtServerDetails.getText(), txtAcDetails.getText());
		final String priority = cboSeverity.getText();

		final Map<String, File> selAttachments = new HashMap<String, File>(attachments.size());
		
		for(Map.Entry<String, File> entry : attachments.entrySet() ){
			if (attachmentsCheckboxViewer.getChecked(entry.getKey())){
				selAttachments.put(entry.getKey(), entry.getValue());
			}
		}
		
		// TTN-2680 
		File zipFile = null;
		String mailTo = "", errMsg = "", body = "";
		try {
			String details = String.format("Name: %s\r\nCompany Name: %s\r\nEmail: %s\r\nPhone: %s\r\n\nSummary: %s\r\nPriority: %s\r\n\nDescription: \r\n%s\r\n\n\n%s",
			new Object[]{ firstName, customerName, email, phone, summary, priority, description, environment});
				
			File issueDetailsFile = new File(tempDirectory + File.separator + "IssueDetails.log");
			FileUtils.writeStringToFile(details, issueDetailsFile);
			selAttachments.put(issueDetailsFile.getName(), issueDetailsFile);
				
			List<File> files = new ArrayList<File>(selAttachments.size());
			for(Map.Entry<String, File> entry : selAttachments.entrySet() ){
				files.add(entry.getValue());
			}
				
			//Create the zip from the temp directory
			try {
				zipFile = PafZipUtil.zipFiles(files);
				// TTN-2680 -- allow user to save zip file on their machine
				String selected = openSaveDialog(zipFile);
				File newFile = new File(selected);
				if (newFile.exists()) {
					MessageDialog.openError(shell,	"Error", String.format("A file with the name: %s already exists.  "
							+ "Please rename the file and try again.", new Object[]{newFile.getName()}));
				} else {
					newFile.createNewFile();
					org.apache.commons.io.FileUtils.copyFile(zipFile, newFile);
					logger.info("File saved at: " + newFile.getAbsolutePath());
				}
			} catch (IOException e1) {				
				logger.error(e1.getMessage());
				throw new PafException(e1);
			}
						
		} catch (Exception e) {
				
			logger.error(e);

		} finally {
			//Purge all the leftover files and dirs
			if(tempDirectory != null){
				org.apache.commons.io.FileUtils.deleteQuietly(tempDirectory);
			}
		}			
	}
	
	private String encode(String p) {
        if (p == null)
            p = "";
        try {
            return URLEncoder.encode(p, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException();
        }
    }
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}
	
	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		// Create OK and cancel buttons
		if(jiraAlive){
			okButton = createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		} else {
			okButton = createButton(parent, IDialogConstants.OK_ID, "Generate Files", true);
		}
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		enableOKButton();

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		submitRequest();
		super.okPressed();
	}
	
	@Override
	protected void cancelPressed(){
		cleanupTempDirectory();
		super.cancelPressed();
	}
	
	protected void enableOKButton() {
		okButton.setEnabled(false);
	}
	
	private void cleanupTempDirectory(){
		if(tempDirectory != null){
			org.apache.commons.io.FileUtils.deleteQuietly(tempDirectory);
		}
	}
	
	private void addValidationListeners() {

		addModifyListener(txtName);
		addModifyListener(txtCustomerName);
		addModifyListener(txtEmail);
		addModifyListener(txtIssueSummary);
		addModifyListener(txtIssueDesc);

	}
	
	private void addModifyListener(Text text) {

		text.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				updateOkButtonState();

			}

		});
	}
	
	protected void updateOkButtonState() {
		
		
		okButton.setEnabled(isFormDataValid());
		

	}
	
	private boolean isFormDataValid() {
		
		String name = txtName.getText().trim();
		String customerName = txtCustomerName.getText().trim();
		String emailAddress = txtEmail.getText().trim();
		String summary = txtIssueSummary.getText().trim();
		String desc = txtIssueDesc.getText().trim();
		String phone = txtPhone.getText().trim();
		
		/*
		if(!talkedToServer){
			return false;
		}*/
		
		if (name.length() == 0	|| name == "") {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		preferences.saveStringSetting(txtName);
		
		if (customerName.length() == 0	|| customerName == "") {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		preferences.saveStringSetting(txtCustomerName);
		
		if (emailAddress.length() == 0	|| emailAddress == "") {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		if (!emailAddress.contains("@")	|| !emailAddress.contains(".")) {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		preferences.saveStringSetting(txtEmail);
		
		if(phone.length() > 0){
			preferences.saveStringSetting(txtPhone);
		}
		
		/*
		if (summary.length() == 0	|| summary == "") {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		if (desc.length() == 0	|| desc == "") {
			//errorLabel.setText("Username is a required field.");
			return false;
		}
		*/
		

		
		return true;
	}
	
	private String openSaveDialog(File file) {
		
		// Create a new file dialog
		FileDialog fileDialog =  new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), SWT.SAVE);
		
		fileDialog.setFilterExtensions(new String[] {"*.zip"});
		
		// Set the title
		fileDialog.setText("Save");
		
		// Set the default directory
		fileDialog.setFilterPath("C:/");
		
		fileDialog.setFileName(file.getName());
		
		// Set the filtered file extension
		String[] fileExtensions = {".zip"};
		
		// Get the selected path string
        String selected = fileDialog.open();
		
        if (selected != null && !selected.isEmpty()) {
        	return selected;
        }
        
        return "";
	}
}

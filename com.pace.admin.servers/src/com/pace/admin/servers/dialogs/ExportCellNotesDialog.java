/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.dialogs;

import java.io.File;
import java.util.Arrays;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ImportExportFileType;
import com.pace.admin.servers.dialogs.providers.CellNoteInfoLabelProvider;
import com.pace.admin.servers.dialogs.providers.StandardObjectArrayContenetProvider;
import com.pace.server.client.CellNotesInformation;

public class ExportCellNotesDialog extends TitleAreaDialog {

	//text box to export
	private Combo exportFileFormatCombo;
	
	//export file format, binary or xml
	private Text exportFileDirTextBox;
	
	//export table
	private Table exportTable;
	
	//open file dialog
	private Button openDirDialogButton;
	
	//initial set of cell notes information passed in
	private CellNotesInformation[] cellNoteInfoArray;

	//checks or unchecks all
	private Button checkUncheckAllButton;
	
	//check box table viewer, holds cell note info
	private CheckboxTableViewer checkboxTableViewer;
	
	//the export output diretory
	private String exportDirectory;
	
	private String serverName;
	
	private ImportExportFileType exportFileType;
	
	/**
	 * Create the export cell note dialog.
	 * 
	 * @param parentShell			Parent shell.
	 * @param cellNoteInfoArray 	Input for viewer, an array of cell note info objects
	 * @param exportDirectory 		Initial export directory
	 */
	public ExportCellNotesDialog(Shell parentShell, CellNotesInformation[] cellNoteInfoArray, String serverName, String exportDirectory) {
		super(parentShell);
		setShellStyle(getShellStyle()|SWT.MAX|SWT.RESIZE );
		this.cellNoteInfoArray = cellNoteInfoArray; 
		this.serverName = serverName;
		this.exportDirectory = exportDirectory;
		
		//if export dir is null, set default
		if ( this.exportDirectory == null ) {
			
			this.exportDirectory = Constants.DEFAULT_IMPORT_EXPORT_DIRECTORY;
			
		}
		
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 25;
		gridLayout.marginRight = 25;
		gridLayout.marginLeft = 25;
		gridLayout.marginBottom = 20;
		container.setLayout(gridLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		final Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.marginHeight = 0;
		gridLayout_2.marginWidth = 1;
		gridLayout_2.horizontalSpacing = 0;
		gridLayout_2.numColumns = 3;
		composite_1.setLayout(gridLayout_2);

		final Label serverLabel = new Label(composite_1, SWT.NONE);
		serverLabel.setText("Server: ");

		final Label serverNameLabel = new Label(composite_1, SWT.NONE);
		new Label(composite_1, SWT.NONE);
		
		//set server name label if server name is not null
		if ( serverName != null ) {
			serverNameLabel.setText(serverName);
		}
		
		checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
		checkboxTableViewer.setLabelProvider(new CellNoteInfoLabelProvider());
		checkboxTableViewer.setContentProvider(new StandardObjectArrayContenetProvider());

		exportTable = checkboxTableViewer.getTable();
		exportTable.setHeaderVisible(true);
		exportTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn.setWidth(23);

		final TableColumn newColumnTableColumn_5 = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn_5.setWidth(124);
		newColumnTableColumn_5.setText("Application Id");

		final TableColumn newColumnTableColumn_2 = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn_2.setWidth(116);
		newColumnTableColumn_2.setText("Data Source Id");

		final TableColumn newColumnTableColumn_4 = new TableColumn(exportTable, SWT.NONE);
		newColumnTableColumn_4.setWidth(100);
		newColumnTableColumn_4.setText("Cell Note Count");
		
		checkboxTableViewer.setInput(cellNoteInfoArray);

		checkUncheckAllButton = new Button(container, SWT.CHECK);

		checkUncheckAllButton.setText("Select / Deselect All");

		new Label(container, SWT.NONE);

		final Label exportFileDirectoryLabel = new Label(container, SWT.NONE);
		exportFileDirectoryLabel.setText("Export file directory:");

		final Composite composite = new Composite(container, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 2;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.marginWidth = 0;
		composite.setLayout(gridLayout_1);

		exportFileDirTextBox = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		exportFileDirTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		//if export dir is not null, populate text box
		if ( exportDirectory != null ) {
			
			exportFileDirTextBox.setText(exportDirectory);
			
		}
		
		openDirDialogButton = new Button(composite, SWT.NONE);
		openDirDialogButton.setText("Browse ...");

		final Label exportFileFormatLabel = new Label(container, SWT.NONE);
		exportFileFormatLabel.setText("Export file format:");

		exportFileFormatCombo = new Combo(container, SWT.READ_ONLY);
		
		exportFileFormatCombo.setItems(new String[] { ImportExportFileType.XML.toString(),ImportExportFileType.Binary.toString() } );
		exportFileFormatCombo.select(0);
		
		setMessage("Check the cell notes to be exported for an application id and data source id.");
		setTitle("Read Me...");
		
		addListeners();
		
		//
		return area;
	}

	/**
	 * 
	 *  Adds listeners to the controls.
	 *
	 */
	private void addListeners() {

		checkUncheckAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				
				//if check/uncheck button is checked, check all, else uncheck all items in checkbox table viewer
				checkboxTableViewer.setAllChecked(checkUncheckAllButton.getSelection());
								
			}
		});
		
		openDirDialogButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				//get directory dialog
				DirectoryDialog dlg = new DirectoryDialog(openDirDialogButton.getShell());

				//if export file dir already has something in it
				if ( exportFileDirTextBox.getText().trim().length() != 0 ) {
					
					//create file ref
					File alreadyExistingDir = new File(exportFileDirTextBox.getText().trim());
					
					//if not null and is directory
					if ( alreadyExistingDir != null && alreadyExistingDir.isDirectory() ) {
					
						//set filter path
						dlg.setFilterPath(alreadyExistingDir.toString());	
						
					}
					
				}				
				
				dlg.setText("Browse to Cell Note Export Directory");
				
				//open dialog
				String exportDirectory = dlg.open();
				
				//if export directory exists, set text box with value
				if ( exportDirectory != null ) {
					
					exportFileDirTextBox.setText(exportDirectory);
					
				}
				
			}
		});
		
	}
	
	/**
	 * 
	 *  Makes sure at least 1 item is checked and there is an output directory.
	 *
	 * @return true if export info is popualted, false if not.
	 */
	private boolean verifyExportInformation() {
	
		//if no items are checked
		if ( checkboxTableViewer.getCheckedElements().length == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please check at least one set of cell notes to export.");
			
			return false;
		}
		
		String exportFileDirPath = exportFileDirTextBox.getText();
		
		//if export path is empty
		if ( exportFileDirPath.trim().length() == 0 ) {
			
			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Please select a directory to export the cell notes to.");
			return false;
			
		}
		
		File exportFileDir = new File(exportFileDirPath);
		
		//if directory and can write to directory
		if ( ! exportFileDir.isDirectory() || ! exportFileDir.canWrite() ) {

			MessageDialog.openError(this.getShell(), Constants.DIALOG_ERROR_HEADING, "Can't write to directory: " + exportFileDirPath);
			
			return false;
		}
		
		//all conditions are now true for a exporting
		return true;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		createButton(parent, IDialogConstants.OK_ID, "Export",
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(485, 511);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Export Cell Notes");
	}
	
	protected void buttonPressed(int buttonId) {
		
		if (buttonId == IDialogConstants.OK_ID) {
	
			//if export info isn't complete, return
			if ( ! verifyExportInformation() ) {
				return;
			}
			
			//set values
			this.cellNoteInfoArray = Arrays.asList(checkboxTableViewer.getCheckedElements()).toArray(new CellNotesInformation[0]);
			this.exportDirectory = exportFileDirTextBox.getText().trim(); 
			
			if ( exportFileFormatCombo.getText().equals(ImportExportFileType.Binary.toString()) ) {
				
				this.exportFileType = ImportExportFileType.Binary;
				
			} else if ( exportFileFormatCombo.getText().equals(ImportExportFileType.XML.toString()) ){ 

				this.exportFileType = ImportExportFileType.XML;
				
			}
			
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * @return the cellNoteInfoArray
	 */
	public CellNotesInformation[] getCellNoteInfoArray() {
		return cellNoteInfoArray;
	}

	/**
	 * @param cellNoteInfoArray the cellNoteInfoArray to set
	 */
	public void setCellNoteInfoArray(CellNotesInformation[] cellNoteInfoArray) {
		this.cellNoteInfoArray = cellNoteInfoArray;
	}

	/**
	 * @return the exportDirectory
	 */
	public String getExportDirectory() {
		return exportDirectory;
	}

	/**
	 * @param exportDirectory the exportDirectory to set
	 */
	public void setExportDirectory(String exportDirectory) {
		this.exportDirectory = exportDirectory;
	}

	/**
	 * @return the exportFileType
	 */
	public ImportExportFileType getExportFileType() {
		return exportFileType;
	}
	
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.views;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.security.PaceUser;
import com.pace.admin.global.server.ApplicationEvent;
import com.pace.admin.global.server.ServerEvent;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.server.ServerStatus;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.actions.ApplyConfigChangesToServerAction;
import com.pace.admin.servers.actions.ApplyCubeChangesToServerAction;
import com.pace.admin.servers.actions.ClearImportedAttributesAction;
import com.pace.admin.servers.actions.ClearMemberTagDataAction;
import com.pace.admin.servers.actions.CloneServerAction;
import com.pace.admin.servers.actions.CollapseAllAction;
import com.pace.admin.servers.actions.ConnectToServerAction;
import com.pace.admin.servers.actions.DeleteServerAction;
import com.pace.admin.servers.actions.DeleteUserAction;
import com.pace.admin.servers.actions.DeleteUsersAction;
import com.pace.admin.servers.actions.DisableEnableServerAction;
import com.pace.admin.servers.actions.DownloadCubeChangesFromServerAction;
import com.pace.admin.servers.actions.EditLDAPUserAction;
import com.pace.admin.servers.actions.EditUserAction;
import com.pace.admin.servers.actions.ExportCellNotesAction;
import com.pace.admin.servers.actions.ExportUsersAction;
import com.pace.admin.servers.actions.ImportCellNotesAction;
import com.pace.admin.servers.actions.ImportUsersAction;
import com.pace.admin.servers.actions.LogoffApplicationAction;
import com.pace.admin.servers.actions.MemberTagDataAction;
import com.pace.admin.servers.actions.MigrateMemberTagDataAction;
import com.pace.admin.servers.actions.ModifyAppSettingsAction;
import com.pace.admin.servers.actions.ModifyServerAction;
import com.pace.admin.servers.actions.NewServerAction;
import com.pace.admin.servers.actions.NewUserAction;
import com.pace.admin.servers.actions.OpenApplicationLogFileAction;
import com.pace.admin.servers.actions.OpenImportAttributeDialogAction;
import com.pace.admin.servers.actions.OpenLDAPGroupSelectionDialogAction;
import com.pace.admin.servers.actions.RefreshServersAction;
import com.pace.admin.servers.actions.RenameServerAction;
import com.pace.admin.servers.actions.RenameServerGroupAction;
import com.pace.admin.servers.actions.ResetUserPasswordAction;
import com.pace.admin.servers.actions.RestartServerAction;
import com.pace.admin.servers.actions.SetDefaultServerAction;
import com.pace.admin.servers.actions.StartServerAction;
import com.pace.admin.servers.actions.StopServerAction;
import com.pace.admin.servers.dialogs.LoginDialog;
import com.pace.admin.servers.dialogs.providers.ServerViewTreeViewerSorter;
import com.pace.admin.servers.enums.TransactionType;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.DomainNode;
import com.pace.admin.servers.nodes.GroupNode;
import com.pace.admin.servers.nodes.IDBUsersNode;
import com.pace.admin.servers.nodes.InfoNode;
import com.pace.admin.servers.nodes.LDAPUserNode;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.nodes.NotAuthenticatedNode;
import com.pace.admin.servers.nodes.SecurityGroupNode;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.nodes.ServerInfoNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.admin.servers.providers.ServerViewDragListener;
import com.pace.admin.servers.providers.ServerViewDropListener;
import com.pace.admin.servers.providers.ViewContentProvider;
import com.pace.admin.servers.providers.ViewLabelProvider;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.TreeNodeUtil;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.server.ServerPlatform;
import com.pace.base.ui.PafServer;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.PafServerAck;

/**
 * 
 * The Server view.  Displays a list of servers read from the paf_servers.xml file.
 * 
 * Class_description_goes_here
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ServerView extends ViewPart implements Observer {
	
	public ServerView() {
	}

	private static final Logger logger = Logger.getLogger(ServerView.class);

	public static final String ID = "com.pace.admin.servers.ServerView";

	//TODO: determine if syncronized map is nessasary 
	private Map<PafServer, ServerStatus> serverStatusMap = new HashMap<PafServer, ServerStatus>();
	private Map<PafServer, ApplicationEvent> serverAppEventMap = new HashMap<PafServer, ApplicationEvent>();
	
		
	private static TreeViewer viewer;

	//servers folder menus
	private RefreshServersAction refreshServerAction;
	private NewServerAction newServerAction;
	
	//server group menus
	RenameServerGroupAction renameServerGroupAction;
	
	//server node menus
	private StartServerAction startAction;
	private StopServerAction stopAction;
	private RestartServerAction restartAction;
	private ModifyServerAction modifyServerAction;
	private CloneServerAction cloneServerAction;
	private DeleteServerAction deleteServerAction;
	private RenameServerAction renameServerAction;
	private SetDefaultServerAction setDefaultServerAction;
	private DisableEnableServerAction disableEnableServerAction;

	//applicaiton node menus
	private ApplyConfigChangesToServerAction applyConfigChangesToServerAction;
	private ApplyCubeChangesToServerAction applyCubeChangesToServerAction;
//	private OpenApplicationLogFileAction openServerSettingsFileAction;
	private ModifyAppSettingsAction modifyAppSettingsAction;
//	private OpenApplicationLogFileAction openMdbDsFileAction;
//	private OpenApplicationLogFileAction openRdbDsFileAction;
	private OpenApplicationLogFileAction openAppLogFileAction;
	private OpenImportAttributeDialogAction openImportAttributeDialogAction;
	private ClearImportedAttributesAction clearImportedAttributesAction;
	private ExportCellNotesAction exportCellNotesAction;
	private ImportCellNotesAction importCellNotesAction;
	private MemberTagDataAction exportMemberTagDataAction;
	private MemberTagDataAction importMemberTagDataAction;
	private MigrateMemberTagDataAction migrateMemberTagDataAction;
	private ClearMemberTagDataAction clearMemberTagDataAction;
	private LogoffApplicationAction logoffAppAction;

	//DB Node menus
	private NewUserAction newUserAction;
	private EditUserAction editUserAction;
	private DeleteUserAction deleteUserAction;
	private ImportUsersAction importUsersAction;
	private ExportUsersAction exportUsersAction;
	private DeleteUsersAction deleteUsersAction;
	private ResetUserPasswordAction resetUserPasswordAction;
	private ConnectToServerAction connectToServerAction;
	private OpenLDAPGroupSelectionDialogAction openLDAPGroupsSelectionDialogAction;
	private EditLDAPUserAction editLDAPUserAction;
	
	private DownloadCubeChangesFromServerAction downloadCubeChangesAction;
	
	private CollapseAllAction collapsAllAction;
	
	private final static String DUMMIE_NODE_NAME = "<not authenticated>";
	
	private static Map<String, DBUsersNode> dbUsersNodeMap = new HashMap<String, DBUsersNode>();
	
	private static Map<DBUsersNode, NativeUsersNode> nativeUsersNodeMap = new HashMap<DBUsersNode, NativeUsersNode>();
	
	private static Map<String, DomainNode> domainNodeMap = new HashMap<String, DomainNode>();

	private void createActions() {

		IWorkbenchWindow window = getSite().getWorkbenchWindow();

		//menus for server folder node
		refreshServerAction = new RefreshServersAction("Refresh All\tF5",	viewer);
		newServerAction = new NewServerAction("Create Server Connector", window);
		
		//menus for server groups
		renameServerGroupAction = new RenameServerGroupAction("Rename...\tF2", window, viewer);
		
		//menus for server nodes
		startAction = new StartServerAction("Start Server", window);
		stopAction = new StopServerAction("Stop Server", window);
		restartAction = new RestartServerAction("Restart Server", window);
		renameServerAction = new RenameServerAction("Rename Server Connector...\tF2", window);
		modifyServerAction = new ModifyServerAction("Modify Server Connector...\tEnter", window);
		cloneServerAction = new CloneServerAction("Copy Server Connector...\tCtrl+C", window);
		deleteServerAction = new DeleteServerAction("Delete Server Connector\tDelete", window);
		setDefaultServerAction = new SetDefaultServerAction("Set As Default Server", window);
		disableEnableServerAction = new DisableEnableServerAction("Enabled", window, viewer);
		
		//menus for applications
		applyConfigChangesToServerAction = new ApplyConfigChangesToServerAction(window);
		applyCubeChangesToServerAction = new ApplyCubeChangesToServerAction(window);
		downloadCubeChangesAction = new DownloadCubeChangesFromServerAction(window, null);
//		openServerSettingsFileAction = new OpenApplicationLogFileAction("Server Settings", window, Constants.CONF_SERVER_DIR + Constants.SERVER_CONFIG_SERVER_SETTINGS_FILE);
//		openMdbDsFileAction = new OpenApplicationLogFileAction("Multidimensional Data Store Settings", window, Constants.CONF_SERVER_DIR + Constants.SERVER_CONFIG_MDB_DS_FILE);
//		openRdbDsFileAction = new OpenApplicationLogFileAction("Relational Data Store Settings", window, Constants.CONF_SERVER_DIR + Constants.SERVER_CONFIG_RDB_DS_FILE);
		modifyAppSettingsAction = new ModifyAppSettingsAction("Modify Server/Application Settings", window);
		openAppLogFileAction = new OpenApplicationLogFileAction("View Application Log",	window);
		exportCellNotesAction = new ExportCellNotesAction(window);
		importCellNotesAction = new ImportCellNotesAction(window);
		exportMemberTagDataAction = new MemberTagDataAction(window, "Export Member Tag Data", TransactionType.Export);		
		importMemberTagDataAction = new MemberTagDataAction(window, "Import Member Tag Data", TransactionType.Import);
		migrateMemberTagDataAction = new MigrateMemberTagDataAction(window);
		clearMemberTagDataAction = new ClearMemberTagDataAction(window);
		openImportAttributeDialogAction = new OpenImportAttributeDialogAction("Import Attributes...", window);
		clearImportedAttributesAction = new ClearImportedAttributesAction("Clear Imported Attributes...", window);
		logoffAppAction = new LogoffApplicationAction("Logoff Application", window);

		//menus for DB and DB Users
		connectToServerAction = new ConnectToServerAction("Authenticate to server", window);
		openLDAPGroupsSelectionDialogAction = new OpenLDAPGroupSelectionDialogAction(window, "Select Security Groups");
		editUserAction = new EditUserAction("Edit User", window);
		newUserAction = new NewUserAction("Create New User", window);
		deleteUserAction = new DeleteUserAction("Delete User", window);
		importUsersAction = new ImportUsersAction("Import Users", window);
		exportUsersAction = new ExportUsersAction("Export Users", window);
		resetUserPasswordAction = new ResetUserPasswordAction(window);
		deleteUsersAction = new DeleteUsersAction("Delete Users", window);

		collapsAllAction = new CollapseAllAction("Collapse All Servers");
		
		// TTN-2348 action for LDAP user
		editLDAPUserAction = new EditLDAPUserAction("Assign Admin Rights", window, viewer);
		
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {

		viewer = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
		
		
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new ServerViewTreeViewerSorter());
		//Added For drag-drop support
		int operations = DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[]{TextTransfer.getInstance()};
	    viewer.addDragSupport(operations, transferTypes , new ServerViewDragListener(viewer));
	    viewer.addDropSupport(operations, transferTypes, new ServerViewDropListener(viewer));
	    //End drag-drop support
	    viewer.setInput(createServerModel());
		viewer.expandToLevel(4);
		
		Tree tree = viewer.getTree();
		tree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				try{
					Object object = ((IStructuredSelection)viewer.getSelection()).getFirstElement();
					if ( e.keyCode == SWT.F2 ) {
						//If it's a group node, then allow rename.
						if ( object instanceof GroupNode ) {
							renameServerGroupAction.run();
						} else if(object instanceof ServerNode){
							renameServerAction.run();
						}
					} else if ( e.keyCode == SWT.F5 ) {
						if(object instanceof ServerNode || object instanceof ServerFolderNode || object instanceof GroupNode) {
							refreshServerAction.run();
						}
					} else if ( (e.stateMask & SWT.CTRL) != 0 && (e.keyCode == 67 || e.keyCode == 99 ) ) {
						if(object instanceof ServerNode) {
							cloneServerAction.run();
						}
					}else if ( e.keyCode == SWT.DEL ) {
						if(object instanceof ServerNode) {
							deleteServerAction.run();
						}
					}
				} catch(Exception ex){
					logger.warn(ex.getMessage());
				}
			}
		});
		
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			public void doubleClick(DoubleClickEvent event) {

				IStructuredSelection selection = (IStructuredSelection) event
						.getSelection();

				Object object = selection.getFirstElement();

//				Shell shell = PlatformUI.getWorkbench()
//						.getActiveWorkbenchWindow().getShell();

				if ((object instanceof DBUsersNode || object instanceof NativeUsersNode )
						&& !SecurityManager.isAuthenticated(PafServerUtil.getServerWebserviceUrl(((TreeNode) object).getServer().getName()))) {

					if ( object instanceof DBUsersNode ) {
					
						refreshDBUsersNode((DBUsersNode) object);
						
					} else if ( object instanceof NativeUsersNode ) {
						
						refreshDBUsersNode(((NativeUsersNode) object).getDBUsersNode());
						
					}
				
				} else if (object instanceof NotAuthenticatedNode ) {
					
					refreshDBUsersNode(((NotAuthenticatedNode) object).getDBUsersNode());

				} else if (object instanceof DBUserNode) {

					TreeNode userNode = (TreeNode) object;
																	
					String serverName = userNode.getServer().getName();

					String url = PafServerUtil
							.getServerWebserviceUrl(serverName);

					if (url != null) {
//						//remove this since if server not running, the DB user will not show
//						if ( ! WebServicesUtil.isServerRunning(url) ) {
//							
//							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "Server " + serverName + " is no longer available.", MessageDialog.ERROR);
//						
//							RefreshServersAction refreshServerAction = new RefreshServersAction("", PlatformUI.getWorkbench().getActiveWorkbenchWindow(), ServerView.getViewer());
//							refreshServerAction.run();
//
//						//if user is not authnticated , prompt user to login again and recreate the dbusersnode.
//						}
//						if ( ! SecurityManager.isAuthenticated(PafServerUtil.getServerWebserviceUrl(((TreeNode) object).getServer().getName())) ) {
//							
//							GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "No longer authenticated, please login to server again.", MessageDialog.ERROR);
//							
//							createDBUsersModel(((IDBUsersNode) object).getDBUsersNode(), true);
//						
//						}
						if ( SecurityManager.isAdmin(url)) {
							
							BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

								public void run() {
							
									editUserAction.run();
									
								}
								
							});
						} 
					}
										
				} else if (object instanceof LDAPUserNode) {
					
					TreeNode userNode = (TreeNode) object;
					
					String serverName = userNode.getServer().getName();

					String url = PafServerUtil
							.getServerWebserviceUrl(serverName);
					
					if(url != null) {
						
						if ( SecurityManager.isAdmin(url)) {
							
							BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

								public void run() {
							
									editLDAPUserAction.run();
									
								}
								
							});
						}
					}
					
				} else if ( object instanceof ServerNode ) {
					
					ServerNode serverNode = (ServerNode) object;
					
					try {
						modifyServerAction.run();
						
						
					} catch (Exception e) {
						
						logger.error("Server '" + serverNode.getName() + "' was not found.  Can't modify.");
						
					}
					
				} else {

					if (object instanceof TreeNode) {

						if (viewer.getExpandedState(object)) {

							viewer.collapseToLevel(object, 1);

						} else {

							viewer.expandToLevel(object, 1);
							viewer.refresh(object);
						}
					}
				}
			}
		});

		viewer.addTreeListener(new ITreeViewerListener() {

			public void treeCollapsed(TreeExpansionEvent event) {

				logger.debug("Collapsing: " + event.getElement());
			}

			public void treeExpanded(TreeExpansionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/*
			public void treeExpanded(TreeExpansionEvent event) {

				if (event.getElement() instanceof DBUsersNode) {
					
					logger.debug("Expanding: " + event.getElement());

					DBUsersNode dbUsersNode = (DBUsersNode) event.getElement();

					List<TreeNode> children = dbUsersNode.getChildren();

					if (children != null
							&& children.size() == 1
							&& children.get(0) instanceof NotAuthenticatedNode) {

						refreshDBUsersNode(dbUsersNode);
						//refreshDBUsersNode(((NotAuthenticatedNode) children.get(0)).getDBUsersNode());
						
						children = dbUsersNode.getChildren();
						
						if (children != null
								&& children.size() >= 1
								&& children.get(0) instanceof NativeUsersNode) {

							TreeNode[] childrenAr = children.toArray(new TreeNode[0]);
							
							dbUsersNode.clearChildren();
							
							for (TreeNode childNode : childrenAr) {
							
								dbUsersNode.addChild(childNode);
								childNode.setParent(dbUsersNode);								
								
							}
							
							viewer.refresh(dbUsersNode, true);
																					
						} 						
					
						
						//viewer.expandToLevel(dbUsersNode, TreeViewer.ALL_LEVELS);
						
						
						//viewer.refresh(dbUsersNode, true);
						
					}
				}
				
			}
			 */
		});

		getSite().setSelectionProvider(viewer);

		createActions();
		
		getViewSite().getActionBars().getToolBarManager().add(collapsAllAction);
		
		createContextMenu();
		
		ServerMonitor.getInstance().addObserver(this);

	}

    public static void refreshServerForDBUsersNode(ServerNode serverNode,
			PafServer pafServer) {
		for ( TreeNode treeNode : serverNode.getChildren()) {
			if( treeNode instanceof ApplicationNode ) {
				for ( TreeNode node : treeNode.getChildren()) {
					if( node instanceof DBUsersNode ) {
						((DBUsersNode)node).setServer(pafServer);
					}
				}
			}
		}
		
	}

	/**
	 * 
	 * Creates the server view context menu.
	 *
	 */
	private void createContextMenu() {
		
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		
		menuMgr.setRemoveAllWhenShown(true);
		
		menuMgr.addMenuListener(new IMenuListener() {

			public void menuAboutToShow(IMenuManager m) {

				ISelection selection = viewer.getSelection();

				if (selection != null) {

					IStructuredSelection structuredSel = (IStructuredSelection) selection;

					Object object = structuredSel.getFirstElement();
					
					//TTN 1843 -removed this unnecessary refresh for menu pop ups
					//refresh server model to check if a server has been started or stopped
//					ServerView.refreshServerModel(false);

					if (object instanceof ServerFolderNode || object == null) {
												
						ServerView.this.fillServersContextMenu(m);
												
					} else if (object instanceof GroupNode){
						
						GroupNode groupNode = (GroupNode) object;
						
						ServerView.this.fillServerGroupContextMenu(m);
						
					} else if (object instanceof ServerNode) {

						ServerNode serverNode = (ServerNode) object;
									
						ServerView.this.fillServerContextMenu(m, serverNode);
												
					} else if (object instanceof DBUsersNode) {

						ServerView.this.fillDBUsersContextMenu(m,
								(DBUsersNode) object);

					} else if ( object instanceof NativeUsersNode) {
						
						ServerView.this.fillNativeDBUsersContextMenu(m, (NativeUsersNode) object);
						
					} else if (object instanceof DBUserNode) {

						ServerView.this.fillDBUserContextMenu(m,
								(DBUserNode) object);
						
					} else if ( object instanceof DomainNode ) {
						
						ServerView.this.fillDomainContextMenu(m, (DomainNode) object);
						
					} else if ( object instanceof ApplicationNode ) {

						ServerView.this.fillApplicationContextMenu(m, (ApplicationNode)object);
						
					} else if (object instanceof LDAPUserNode) {
						
						ServerView.this.fillLDAPUserContextMenu(m, (LDAPUserNode)object);
					}

				}

			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);

	}

	private void fillServersContextMenu(IMenuManager menuMgr) {

		menuMgr.add(refreshServerAction);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(newServerAction);						
	}

	private void fillServerGroupContextMenu(IMenuManager menuMgr){
		
		menuMgr.add(refreshServerAction);
		
		ISelection selection = viewer.getSelection();

		if (selection != null) {

			IStructuredSelection structuredSel = (IStructuredSelection) selection;

			Object object = structuredSel.getFirstElement();
			
			if (object instanceof GroupNode){
				
				GroupNode groupNode = (GroupNode) object;
				
				if(!groupNode.getName().equalsIgnoreCase(GroupNode.BLANK_GROUP_NAME)){
					
					menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
					
					menuMgr.add(renameServerGroupAction);
				}
			}
		}
		
	}
	
	private void fillServerContextMenu(IMenuManager menuMgr, ServerNode serverNode) {

		logger.debug("Filling Server Context Menu");
		PafServer pafServer = serverNode.getServer();
		if ( pafServer == null ) {
			try {
				if (serverNode.getName() != null) {
					pafServer = PafServerUtil.getServer(serverNode.getName());
				} 
			} catch (PafServerNotFound e) {
				logger.warn("Server: " + serverNode.getName() + " is no longer available.");
				e.printStackTrace();
			}
		}
		
		menuMgr.add(refreshServerAction);

		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(disableEnableServerAction);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		if( ! pafServer.isDefaultServer() ) {
			menuMgr.add(setDefaultServerAction);
			menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		}	
		
				
		boolean isServerStarted = ServerMonitor.getInstance().isServerRunning(pafServer);
		boolean enabled = !pafServer.isDisabled();
		menuMgr.add(startAction);
		
		//TODO: need to not enable when starting or stopping server so user can't request a second time.
		startAction.setEnabled(!isServerStarted && enabled);
		menuMgr.add(stopAction);
		stopAction.setEnabled(isServerStarted && enabled);
		menuMgr.add(restartAction);
		restartAction.setEnabled(isServerStarted && enabled);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(renameServerAction);
		menuMgr.add(modifyServerAction);
		menuMgr.add(cloneServerAction);
		menuMgr.add(deleteServerAction);

		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(modifyAppSettingsAction);
		modifyAppSettingsAction.setEnabled(isServerStarted);
	}

	protected void fillApplicationContextMenu(IMenuManager menuMgr, ApplicationNode appNode) {

		PafServer pafServer = appNode.getParent().getServer();
		if ( pafServer == null ) {
			try {
				if (appNode.getParent().getName() != null) {
					pafServer = PafServerUtil.getServer(appNode.getParent().getName());
				} 
			} catch (PafServerNotFound e) {
				logger.warn("Server: " + appNode.getParent().getName() + " is no longer available.");
				e.printStackTrace();
			}
		}
		boolean isServerRunning = ServerMonitor.getInstance().isServerRunning(pafServer);

		menuMgr.add(applyConfigChangesToServerAction);
		menuMgr.add(applyCubeChangesToServerAction);
		menuMgr.add(downloadCubeChangesAction);
		
//		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));	
//
//		MenuManager menuServerConfigs = new MenuManager("Server Config Files", "idInsert");
//		menuServerConfigs.add(openServerSettingsFileAction);
//		menuServerConfigs.add(openMdbDsFileAction);
//		menuServerConfigs.add(openRdbDsFileAction);
//		menuMgr.add(menuServerConfigs);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

		menuMgr.add(openAppLogFileAction);
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

//		menuMgr.add(refreshDimensionCacheAction);
//		refreshDimensionCacheAction.setEnabled(isServerRunning);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		menuMgr.add(openImportAttributeDialogAction);
		openImportAttributeDialogAction.setEnabled(isServerRunning);
		menuMgr.add(clearImportedAttributesAction);
		clearImportedAttributesAction.setEnabled(isServerRunning);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));	
		
	
		MenuManager menuCellNotes = new MenuManager("Cell Notes", "idInsert");
		menuCellNotes.add(exportCellNotesAction);
		exportCellNotesAction.setEnabled(isServerRunning);
		menuCellNotes.add(importCellNotesAction);
		importCellNotesAction.setEnabled(isServerRunning);
		menuMgr.add(menuCellNotes);
		
		MenuManager menuMemberTags = new MenuManager("Member Tags", "idInsert");
		menuMemberTags.add(exportMemberTagDataAction);
		exportMemberTagDataAction.setEnabled(isServerRunning);
		menuMemberTags.add(importMemberTagDataAction);
		importMemberTagDataAction.setEnabled(isServerRunning);
		// TTN-1942 Defer the option to 2.9.2
		//menuMemberTags.add(migrateMemberTagDataAction);
		//migrateMemberTagDataAction.setEnabled(isServerRunning);
		menuMemberTags.add(clearMemberTagDataAction);
		clearMemberTagDataAction.setEnabled(isServerRunning);
		menuMgr.add(menuMemberTags);
		
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));	

		String url = PafServerUtil.getServerWebserviceUrl(appNode.getParent().getName());
		
		//if server is running and authenticated
		if ( ServerMonitor.getInstance().isServerRunning(url) && SecurityManager.isAuthenticated(url) ) {
			
			menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));	
			menuMgr.add(logoffAppAction);	
			
		}
	}

	protected void fillDBUsersContextMenu(IMenuManager menuMgr, DBUsersNode node) {

		String url = PafServerUtil.getServerWebserviceUrl(node.getServer().getName());
		
		if( ! SecurityManager.isAuthenticated(url) ) {
			menuMgr.add(connectToServerAction);
		}
		
		// TTN-2690
		// Enables LDAP group selection 
		else {
			ServerSession session = SecurityManager.getSession(url);
			
			if ( session != null && session.isMixedAuthMode() ) {
			
				menuMgr.add(openLDAPGroupsSelectionDialogAction);
				
			}
		}
		
	}
	
	protected void fillNativeDBUsersContextMenu(IMenuManager m, NativeUsersNode node) {

		String url = PafServerUtil.getServerWebserviceUrl(node.getServer().getName());
		
		if (SecurityManager.isAdmin(url)) {
			
			m.add(newUserAction);
			m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			m.add(importUsersAction);
			m.add(exportUsersAction);
			m.add(deleteUsersAction);
			
		} 
		
		m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

	}

	protected void fillDBUserContextMenu(IMenuManager m, DBUserNode node) {

		String url = PafServerUtil.getServerWebserviceUrl(node.getServer().getName());
		
		if ( SecurityManager.isAdmin(url)) {
			
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
			
			//if server password reset is enabled
			if ( pafServerAck != null ) {
			
				m.add(resetUserPasswordAction);
				resetUserPasswordAction.setEnabled(pafServerAck.isServerPasswordResetEnabled());
			}

			m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			// TTN-2567 Add Edit User to User menu.
			m.add(editUserAction);

			
			m.add(deleteUserAction);

			ServerSession serverSession = SecurityManager.getSession(url);
			
			if ( serverSession != null ) {
				
				String currentUser = serverSession.getUserName();
				
				String currentDBNodeUserName = node.getName();
				
				deleteUserAction.setEnabled((currentUser.equalsIgnoreCase(currentDBNodeUserName))? false : true);
				
				
			}
			
			m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			
			m.add(newUserAction);
			
			m.add(deleteUsersAction);
			
		} else if ( ! SecurityManager.isAuthenticated(url) ) {
			
			m.add(connectToServerAction);
			
		}

		m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

	}

	
	protected void fillDomainContextMenu(IMenuManager m, DomainNode node) {

		String url = PafServerUtil.getServerWebserviceUrl(node.getServer().getName());
		
		if (SecurityManager.isAdmin(url)) {
			
			m.add(openLDAPGroupsSelectionDialogAction);
			
			openLDAPGroupsSelectionDialogAction.setDomainName(node.getName());
						
		}

	}
	
	protected void fillLDAPUserContextMenu(IMenuManager m, LDAPUserNode node) {
		
		String url = PafServerUtil.getServerWebserviceUrl(node.getServer().getName());
		
		if ( SecurityManager.isAdmin(url)) {
			m.add(editLDAPUserAction);
		}
	}

//	private void createToolbar() {
//
//		IToolBarManager toolBar = getViewSite().getActionBars()
//				.getToolBarManager();
//
//		toolBar.add(startAction);
//		toolBar.add(stopAction);
//
//	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * @return Returns the viewer.
	 */
	public static TreeViewer getViewer() {
		return viewer;
	}
	
	public static TreeNode refreshServerModel(boolean createNew) {
		
		if ( createNew ) {
			
			return createServerModel();
			
		} else {
			
			return updateServerModel();
			
		}
		
	}
	
	private static TreeNode updateServerModel() {
		
		ServerFolderNode root = (ServerFolderNode) viewer.getInput();
		
		if ( root != null ) {
		
			ServerFolderNode pafServersTreeNode = (ServerFolderNode) root.getChildren().get(0);
		
			if ( pafServersTreeNode != null ) {
				
//				Map<String, PafServerAck> serverCacheMap = new HashMap<String, PafServerAck>();
								
				for ( TreeNode serverTreeNode : pafServersTreeNode.getChildren()) {
					
					if ( serverTreeNode instanceof ServerNode) {
					
						ServerNode sn = (ServerNode) serverTreeNode;
						
						PafServer pafServer = sn.getServer();
						
						if ( pafServer != null ) {
							
							String url = pafServer.getCompleteWSDLService();

							boolean serverNodeHasChildren = ( sn.getChildren().size() > 0 );
							
							PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
							
//							if ( serverCacheMap.containsKey(url)) {
//								pafServerAck = serverCacheMap.get(url);
//							} 
							
//							if ( pafServerAck == null ) { //server is not in server cache
//								pafServerAck = WebServicesUtil.getPafServerAck(url);
//								serverCacheMap.put(url, pafServerAck);
//							}
							
							if (pafServerAck != null) { //server is not up for the specified url
								//if server is running and does not have children, create
								if ( ! serverNodeHasChildren ) {
									createServerNodeChildren(sn, pafServerAck, true);
								} 
							}
							else { //server is not up
								//remove application node from server tree
								if ( serverNodeHasChildren ) {
									sn.clearChildren();
								}
							}
						}
					}
				}
				viewer.refresh(true);
			}
		}		
		
		return root;
		
	}

	private static TreeNode createServerModel() {

		// Proj
		ServerFolderNode pafServersTreeNode = new ServerFolderNode(
				"Pace Servers");
		
		List<String> groups = PafServerUtil.getServerGroups();
		List<PafServer> noGroups = PafServerUtil.getServersWithoutGroups();
		
		
		//Legacy Mode
		if(groups == null || groups.size() == 0){
			for (PafServer pafServer : PafServerUtil.getPafServers()) {
	
				createServerNode(pafServer, pafServersTreeNode); 
				
			}
		} else {
			Map<String, GroupNode> groupNodes = new HashMap<String, GroupNode>(5); 
			//Loop thur the groups creating the nodes.
			for(String group : groups){
				//Create the group node.
				GroupNode node = new GroupNode(group, pafServersTreeNode, null);
				//Add the node to the Pace Servers node
				pafServersTreeNode.addChild(node);
				//add the group node to a map
				groupNodes.put(group, node);
			}
			
			//Check if we have servers that don't have a group
			if(noGroups != null && noGroups.size() > 0){
				//Servers without a group, so create blank group node
				GroupNode node = new GroupNode(pafServersTreeNode, null);
				//Add node to tree
				pafServersTreeNode.addChild(node);
				//save it.
				groupNodes.put(node.getName(), node);
			}
			
			//Loop thur the servers add them to the appropiate group
			for (PafServer pafServer : PafServerUtil.getPafServers()) {
				//Create a var for the empty group
				String groupName = GroupNode.BLANK_GROUP_NAME;
				//If the server has a group set it's name
				if (pafServer.getServerGroupName() !=null &&  !pafServer.getServerGroupName().isEmpty()){
					groupName = pafServer.getServerGroupName();
				} 
				//Get the servers group node
				GroupNode parent = groupNodes.get(groupName);
				//Finally create the server node.
				createServerNode(pafServer, parent); 
				
			}
		}

		ServerFolderNode root = new ServerFolderNode("");
		root.addChild(pafServersTreeNode);

		return root;
	}
	
	private static TreeNode createServerNode(PafServer pafServer, TreeNode parent){
		
		ServerNode serverNode = new ServerNode(pafServer.getName(),	parent, pafServer);
		
		parent.addChild(serverNode);
		
		if(!pafServer.isDisabled()){

			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(pafServer.getCompleteWSDLService());
			
			if (pafServerAck != null) {
	
				createServerNodeChildren(serverNode, pafServerAck, true);
				
			}
		}
		
		return serverNode;
	}
	
	private static void createServerNodeChildren(ServerNode serverNode, PafServerAck pafServerAck, boolean expandDBUsers) {
		
		serverNode.clearChildren();
		
		PafServer pafServer = serverNode.getServer();
		String[] serverPlatforms = pafServerAck.getPlatform().split(":");
		if( pafServerAck.getPlatform().indexOf("Linux") > 0 ) {
			pafServer.setServerPlatform(ServerPlatform.Linux);
		}
		else if( pafServerAck.getPlatform().indexOf("Windows") > 0 ) {
			pafServer.setServerPlatform(ServerPlatform.Windows);
		}
		
		
		//ServerInfoNode - next level under ServerNode
		ServerInfoNode serverInfoNode = new ServerInfoNode(
				"Server Info", serverNode, pafServer);

		serverNode.addChild(serverInfoNode);

		String[] serverInfoAr = {
				"Pace Version: " + pafServerAck.getServerVersion(),
				"Java Version: " + serverPlatforms[3],
				"O/S Info: " + serverPlatforms[1]+":"+serverPlatforms[2]
				};
		
		// for each server info, create node and add to server info node
		for (String serverInfo : serverInfoAr) {
			InfoNode infoNode = new InfoNode(serverInfo);
			serverInfoNode.addChild(infoNode);
		}
		if( pafServerAck != null ) {
			//ApplicationNode - next level under ServerNode - same as ServerInfoNode
			createApplicationTree(serverNode,pafServerAck,expandDBUsers );	
		}
	}
	
	private static void createApplicationTree(ServerNode serverNode, PafServerAck pafServerAck, boolean expandDBUsers) {
		PafServer pafServer = serverNode.getServer();
		String url = pafServer.getCompleteWSDLService();
		ApplicationState appState = ServerMonitor.getInstance().getApplicationState(pafServer);
		if ( appState != null) {
			
			Set<ApplicationNode> currentAppNodeSet = TreeNodeUtil.findApplicationNode(serverNode);
			
			String appId = appState.getApplicationId();
			
			ApplicationNode activeAppNode = null;
			
			if ( currentAppNodeSet != null ) {
				
				for (ApplicationNode currentAppNode : currentAppNodeSet) {
					
					activeAppNode = currentAppNode;
					
					break;
					
				}
			}
			
			if ( activeAppNode != null ) {
				
				//if app id is not longer same, clear session
				if ( appId != activeAppNode.getName()) {
					
					//try to remove session using URL
					SecurityManager.removeSession(url);
					
				}
				
				serverNode.removeChild(activeAppNode);
				
			}
									
			ApplicationNode appNode = new ApplicationNode(appId, serverNode, pafServer);
			
			//appNode.setAppState(appState);
			
			serverNode.addChild(appNode);
			String[] appInfoAr = {
					"Data Source Id: " + pafServerAck.getDataSourceId(),
					};
	
			// for each server info, create node and add to server info node
			for (String appInfo : appInfoAr) {
				InfoNode infoNode = new InfoNode(appInfo);
				appNode.addChild(infoNode);
			}
			
			//TTN-1950 - Create a node to show the GUID, only when development features are enabled.
			File f = new File(Constants.ADMIN_CONSOLE_CONF_DIRECTORY, Constants.ENABLE_DEVELOPMENT_FEATURES_FLAG);
			if(f.exists()){
				try {
					InfoNode infoNode = new InfoNode("Session GUID: " + ServerMonitor.getInstance().getApplicationState(pafServer).getAppSessionId());
					appNode.addChild(infoNode);
				} catch(Exception e){
					//meh, it's just a development feature.
				}
			}
			
			//DBUsersNode - same level as Server Info Node?
			DBUsersNode dbUsersNode = null;
			
			if ( dbUsersNodeMap.containsKey(pafServer.getName()) ) {
				
				dbUsersNode = dbUsersNodeMap.get(pafServer.getName());
														
			} else {
	
				dbUsersNode = new DBUsersNode("Database Users",
						serverNode, pafServer);
				
				dbUsersNodeMap.put(pafServer.getName(), dbUsersNode);
				
			}
			
			appNode.addChild(dbUsersNode);
	
			createDBUsersModel(dbUsersNode, expandDBUsers);
		}
	}

	/*public static TreeNode createServerModel(PafServer serverToRefresh) {

		boolean updateExistingServerOnly = ( serverToRefresh != null );
						
		ServerFolderNode pafServersTreeNode = null;
		
		if ( updateExistingServerOnly ) {
		
			//get current "Pace Servers" node
			pafServersTreeNode = (ServerFolderNode) ((ServerFolderNode) viewer.getInput()).getChildren().get(0);
			
		}
	
		Map<String, ServerNode> serverNodeMap = new HashMap<String, ServerNode>();
		
		if ( pafServersTreeNode == null ) {
			
			pafServersTreeNode = new ServerFolderNode("Pace Servers"); 
			
		}
		
		//if update existing, populate the server node map
		if ( updateExistingServerOnly ) {
		
			for (TreeNode childNode : pafServersTreeNode.getChildren()) {
				
				if ( childNode != null && childNode instanceof ServerNode ) {
					
					ServerNode sn = (ServerNode) childNode;
					
					serverNodeMap.put(sn.getServer().getName(), sn);
					
				}
				
			}
		}
		
				
		for (PafServer pafServer : PafServerUtil.getPafServers()) {
			
			//if update existing server only, continue if not server to update
			if ( updateExistingServerOnly && ! serverToRefresh.getName().equals(pafServer.getName())) {
				
				continue;
				
			}							

			ServerNode serverNode = null;
			
			//try to get existing server node
			if (updateExistingServerOnly && serverNodeMap.containsKey(pafServer.getName())) {
				
				serverNode = serverNodeMap.get(pafServer.getName());
								
			} else {
			
				serverNode = new ServerNode(pafServer.getName(), pafServersTreeNode, pafServer);
				pafServersTreeNode.addChild(serverNode);

			}
						
			// start logging server
			ConsoleUtil.outputFileToConsole(pafServer.getHomeDirectory()
					+ Constants.SERVER_LOG);

			String url = pafServer.getCompleteWSDLService();

			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
			
			boolean serverIsRunning = (pafServerAck != null);

			if (serverIsRunning && serverNode.getChildren().size() == 0 ) {

				ServerInfoNode serverInfoNode = new ServerInfoNode(
						"Server Info", serverNode, pafServer);

				serverNode.addChild(serverInfoNode);

				String[] serverInfoAr = {
						"App Id: " + pafServerAck.getApplicationId(),
						"Data Source Id: " + pafServerAck.getDataSourceId(),
						"Server Version: " + pafServerAck.getServerVersion()
						};
				

				// for each server info, create node and add to server info node
				for (String serverInfo : serverInfoAr) {
					InfoNode infoNode = new InfoNode(serverInfo);
					serverInfoNode.addChild(infoNode);
				}

				DBUsersNode dbUsersNode = null;
				
				if ( dbUsersNodeMap.containsKey(pafServer.getName()) ) {
					
					dbUsersNode = dbUsersNodeMap.get(pafServer.getName());
															
				} else {

					dbUsersNode = new DBUsersNode("Database Users",
							serverNode, pafServer);
					
					dbUsersNodeMap.put(pafServer.getName(), dbUsersNode);
					
				}
				
				serverNode.addChild(dbUsersNode);

				createDBUsersModel(dbUsersNode, true);
				
			} else if ( ! serverIsRunning && serverNode.getChildren().size() > 0){
				
				serverNode.clearChildren();
				
			} 

		}

		ServerFolderNode root = new ServerFolderNode("");
		root.addChild(pafServersTreeNode);
		
		if ( updateExistingServerOnly ) {
			
			viewer.refresh(pafServersTreeNode, true);
			
		}

		return root;
	}
*/
	/**
	 * 
	 * Used to try to auth witha  dialog when not
	 *
	 * @param pafServer
	 * @throws ServerNotRunningException 
	 */
	public static void authWithLoginDialogIfNotAuthed(PafServer pafServer) throws ServerNotRunningException {
		
		authWithLoginDialogIfNotAuthed(pafServer, false);
		
	}
	
	/**
	 * 
	 * Used to try to auth witha  dialog when not
	 *
	 * @param pafServer
	 * @throws ServerNotRunningException 
	 */
	public static void authWithLoginDialogIfNotAuthed(final PafServer pafServer, boolean displayBusy) throws ServerNotRunningException {
	
		if ( pafServer != null && pafServer.getCompleteWSDLService() != null ) {

			String url = pafServer.getCompleteWSDLService();
			
			if ( ! SecurityManager.isAuthenticated(url) ) {
			
				//get login dialog
				LoginDialog loginDialog = null;
				
//				try {
					
					loginDialog = SecurityManager.getLoginDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell()
							.getShell(), pafServer.getName());
					//open
					loginDialog.open();
					
//				} catch (ServerNotRunningException e) {
//	
//					logger.warn("Server '" + pafServer.getName() + "' is not running.");
//				}
	
				
				if ( SecurityManager.isAuthenticated(url) ) {														
											
						test(pafServer, displayBusy);
						
				}
			}
		}
	
	}	
	
	private static void test(final PafServer pafServer, boolean displayBusy) {
		
		if ( displayBusy ) {
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getDisplay(), new Runnable() {

				public void run() {
					
					DBUsersNode currentDBUsersNode = dbUsersNodeMap.get(pafServer.getName());

					if ( currentDBUsersNode != null ) {
					
						createDBUsersModel(currentDBUsersNode, viewer.getExpandedState(currentDBUsersNode));
					
					}
					
				}
				
			});
			
		} else {
		
			Display.getDefault().asyncExec(new Runnable() {

				public void run() {

					DBUsersNode currentDBUsersNode = dbUsersNodeMap.get(pafServer.getName());

					if ( currentDBUsersNode != null ) {
					
						createDBUsersModel(currentDBUsersNode, viewer.getExpandedState(currentDBUsersNode));
						
					}
					
				}
				
			});
			
		}
		
	}
	
	public static void createDBUsersModel(DBUsersNode dbUsersNode, boolean isExpanded) {

		PafServer pafServer = dbUsersNode.getServer();
		
		String url = PafServerUtil.getServerWebserviceUrl(pafServer.getName());
		
		Map<String, Map<String, Set<PaceUser>>> domainSecurityGroupUsersMap = null;;
		
		if ( url != null && SecurityManager.isAuthenticated(url) && pafServer != null) {		
		
			try {
				
				domainSecurityGroupUsersMap = SecurityManager.getUserNamesBySecurityGroups(url);
				
			} catch (PafException e) {
				
				logger.error(e.getMessage());
				
			} 
			
		}
		
		//clear children
		dbUsersNode.clearChildren();
		
		createNativeDBUserModel(dbUsersNode, domainSecurityGroupUsersMap);
		createPaceSecurityGroupsModel(dbUsersNode, domainSecurityGroupUsersMap);
		
		viewer.refresh(dbUsersNode);
		
		if (isExpanded) {
			
			viewer.expandToLevel(dbUsersNode, TreeViewer.ALL_LEVELS);
			
		}
		
	}

	private static void createPaceSecurityGroupsModel(DBUsersNode dbUsersNode, Map<String, Map<String, Set<PaceUser>>> domainSecurityGroupUsersMap) {

		PafServer pafServer = dbUsersNode.getServer();
		
		String url = PafServerUtil.getServerWebserviceUrl(dbUsersNode.getServer().getName());

		if ( url != null && SecurityManager.isAuthenticated(url) && pafServer != null) {
		
			ServerSession session = SecurityManager.getSession(url);
			
			if ( session.isMixedAuthMode() )  {
							
				if ( dbUsersNode != null ) {
					
					if (url != null && SecurityManager.isAuthenticated(url)) {
		
						if ( domainSecurityGroupUsersMap != null ) {
						
							//domainSecurityGroupUsersMap.put("adg.com", new HashMap<String, Set<String>>());
							
							for (String domainName : domainSecurityGroupUsersMap.keySet() ) {
								
								if ( domainSecurityGroupUsersMap.get(domainName).size() > 0 ) {
								
									//if native domain, continue to next domain
									if ( domainName.equals(PafBaseConstants.Native_Domain_Name)) {
										continue;
									}
								
									DomainNode domainNode = null; 			
									
									String serverDomainKey = pafServer.getName() + "|" + domainName;
									
									if ( domainNodeMap.containsKey(serverDomainKey) ) {
										
										domainNode = domainNodeMap.get(serverDomainKey);
										
										domainNode.clearChildren();
													
									} else {
										
										domainNode = new DomainNode(domainName, dbUsersNode, pafServer);
										
										domainNodeMap.put(serverDomainKey, domainNode);
														
									}			
	
									Map<String, Set<PaceUser>> securityGroupMap = domainSecurityGroupUsersMap.get(domainName);
								
									createSecurityGroupsForDomain(domainNode, securityGroupMap);
									
									dbUsersNode.addChild(domainNode);
									
								}
								
							}
							
						}
						
					}			
				}
			}		
		}
		
	}

	public static void createSecurityGroupsForDomain(DomainNode domainNode, Map<String, Set<PaceUser>> securityGroupMap) {
				
		if ( domainNode != null && securityGroupMap != null ) {
		
			PafServer pafServer = domainNode.getServer();		
			
			domainNode.clearChildren();
			
			for (String securityGroupName : securityGroupMap.keySet()) {
				
				SecurityGroupNode sgn = new SecurityGroupNode(securityGroupName, domainNode, pafServer);									
	
				for (PaceUser paceUser : securityGroupMap.get(securityGroupName)) {
					
					LDAPUserNode ldapUserNode = new LDAPUserNode(paceUser, sgn, pafServer);
					
					sgn.addChild(ldapUserNode);
					
				}				
				
				domainNode.addChild(sgn);					
				
			}

			viewer.refresh(domainNode);
			
		}
	}
	
	/*
	public static void addSecurityGroupsForDomain(DomainNode domainNode, Map<String, Set<String>> securityGroupMap) {
		
		if ( domainNode != null && securityGroupMap != null ) {
		
			PafServer pafServer = domainNode.getServer();		
			
			for (String securityGroupName : securityGroupMap.keySet()) {
				
				SecurityGroupNode sgn = new SecurityGroupNode(securityGroupName, domainNode, pafServer);									
	
				for (String securityUserName : securityGroupMap.get(securityGroupName)) {
					
					//LDAPUserNode ldapUserNode = new LDAPUserNode(securityUserName, sgn, pafServer);
					
					//sgn.addChild(ldapUserNode);
					
				}				
				
				domainNode.addChild(sgn);					
				
			}

			viewer.refresh(domainNode, true);
			
		}
	}
	*/

	private static void createNativeDBUserModel(DBUsersNode dbUsersNode,
			Map<String, Map<String, Set<PaceUser>>> domainSecurityGroupUsersMap) {
	
		PafServer pafServer = dbUsersNode.getServer();

		String url = PafServerUtil.getServerWebserviceUrl(dbUsersNode.getServer().getName());
		
		if (url != null && SecurityManager.isAuthenticated(url) && domainSecurityGroupUsersMap != null ) {
						
			NativeUsersNode nativeUsersNode = null; 			
						
			if ( nativeUsersNodeMap.containsKey(dbUsersNode ) ) {
				
				nativeUsersNode = nativeUsersNodeMap.get(dbUsersNode);
				
				nativeUsersNode.clearChildren();
							
			} else {
				
				nativeUsersNode = new NativeUsersNode(PafBaseConstants.Native_Domain_Name, dbUsersNode, pafServer);
				
				nativeUsersNodeMap.put(dbUsersNode, nativeUsersNode);
								
			}
								
			if (pafServer != null &&  domainSecurityGroupUsersMap.containsKey(PafBaseConstants.Native_Domain_Name) ) {
				
					Map<String, Set<PaceUser>> nativeUsersMap = domainSecurityGroupUsersMap.get(PafBaseConstants.Native_Domain_Name);
					
					if ( nativeUsersMap != null && nativeUsersMap.containsKey(PafBaseConstants.Native_Domain_Name)) {
				
						for (PaceUser paceUser : nativeUsersMap.get(PafBaseConstants.Native_Domain_Name)) {
		
							DBUserNode userNode = new DBUserNode(paceUser.getDisplayName(), nativeUsersNode, pafServer);
							nativeUsersNode.addChild(userNode);
		
						}
						
					}
					
			}
						
			dbUsersNode.addChild(nativeUsersNode);
		
			viewer.refresh(nativeUsersNode);
			
			viewer.refresh(dbUsersNode);
			
			/*if (expandDBUsers) {
				
				viewer.expandToLevel(dbUsersNode, 2);
				//viewer.expandToLevel(nativeUsersNode, 1);
				
			}*/
			
		} else {

			NotAuthenticatedNode notAuthNode = new NotAuthenticatedNode(DUMMIE_NODE_NAME, dbUsersNode, pafServer);

			dbUsersNode.addChild(notAuthNode);
			
			viewer.refresh(dbUsersNode);
			
			/*if (expandDBUsers) {
				
				viewer.expandToLevel(dbUsersNode, 1);
				
			}
*/
		}

				
	}

	private static void refreshDBUsersNode(IDBUsersNode treeNode) {
		
		DBUsersNode dbUsersNode = ((IDBUsersNode) treeNode).getDBUsersNode();
			
		final PafServer pafServer = dbUsersNode.getServer();
				
		if ( ! SecurityManager.isAuthenticated(pafServer.getCompleteWSDLService())) {
			
//			get login dialog
			LoginDialog loginDialog = null;
			
			try {
				
				loginDialog = SecurityManager.getLoginDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell()
						.getShell(), pafServer.getName());
				//open
				loginDialog.open();
				
			} catch (ServerNotRunningException e) {

				logger.warn("Server '" + pafServer.getName() + "' is not running.");
			}
			
						
		} 
		
		if ( SecurityManager.isAuthenticated(pafServer.getCompleteWSDLService())) { 
			
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().getDisplay(), new Runnable() {

				public void run() {
					
					DBUsersNode currentDBUsersNode = dbUsersNodeMap.get(pafServer.getName());

					if ( currentDBUsersNode != null ) {
					
						createDBUsersModel(currentDBUsersNode, true);
					
					}
					
				}
				
			});
		
			
		}
		
		
	}

	public static void updateServerViewerWithModel() {
		
		// get instance of server
		// view
		ServerView serverView = (ServerView) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.findView(
						ServerView.ID);

		// if server view was found
		if (serverView != null) {

			// get viewer from
			// server view
			TreeViewer viewer = ServerView
					.getViewer();

			// if viewer exist
			if (viewer != null) {

				logger
						.debug("Updating image from red to green");

				// refresh node
				// viewer.refresh(serverNode);
				viewer
						.setInput(ServerView
								.createServerModel());
				viewer
						.expandToLevel(2);

			}
		}

	}

	public static void removeSecurityGroupsForDomain(DomainNode domainNode, Set<String> securityGroupsToRemove) {

		if ( domainNode != null && securityGroupsToRemove != null && domainNode.getChildren() != null ) {
			
			Map<String, TreeNode> securityGroupNodes = new HashMap<String, TreeNode>();
			
			for (Object childNode : domainNode.getChildren()) {
				
				if ( childNode instanceof TreeNode ) {
					
					TreeNode securityGroupNode  = (TreeNode) childNode;
					
					securityGroupNodes.put(securityGroupNode.getName(), securityGroupNode);					
					
				}
				
			}
			
			for (String securityGroupToRemove : securityGroupsToRemove ) {
				
				if ( securityGroupNodes.containsKey(securityGroupToRemove) ) {
					
					TreeNode nodeToRemove = securityGroupNodes.get(securityGroupToRemove);
					
					domainNode.removeChild(nodeToRemove);
					
				}
				
			}
			
			viewer.refresh(domainNode, true);
			
		}
		
	}
	public DownloadCubeChangesFromServerAction getDownloadCubeChangesFromServerAction() {
		return downloadCubeChangesAction;
	}
	
	@Override
	public void update(Observable o, Object object) {		
		
		if ( object != null && object instanceof ServerEvent) {
					
			ServerEvent se = (ServerEvent) object;
			
			final PafServer ps = se.getPafServer();
			
			ServerStatus ss = se.getServerStatus();
			
			if( ss != null ) {
				ServerNode serverNode = null;
				
				ServerFolderNode root = (ServerFolderNode) viewer.getInput();
				
				if ( root != null ) {
				
					ServerFolderNode pafServersTreeNode = (ServerFolderNode) root.getChildren().get(0);
				
					/*if ( pafServersTreeNode != null ) {
						
	//					Map<String, PafServerAck> serverCacheMap = new HashMap<String, PafServerAck>();
										
						for ( TreeNode serverTreeNode : pafServersTreeNode.getChildren()) {
							
							if ( serverTreeNode instanceof ServerNode ) {
								
								ServerNode sn = (ServerNode) serverTreeNode;
								
								if ( sn.getServer() != null && sn.getServer().equals(ps)) {
									
									serverNode = sn;
									
									break;
									
								}
							}
						}
						
					}*/

					if(pafServersTreeNode.hasGroupNodes()){
						serverNode = TreeNodeUtil.findServerNode(pafServersTreeNode.getChildren(), ps);
					} else {
						serverNode = TreeNodeUtil.findServerNode(pafServersTreeNode, ps);
					}
				}
				
				final ServerNode finalServerNode = serverNode;
				
				if ( serverNode != null ) {
								
					boolean hasServerStatusChanged = false;
					//boolean serverStatusNotExist = false;
					
					//if server status map doesn't have status for server
					//or
					//server status map has key but value doens't match
					if ( ! serverStatusMap.containsKey(ps) || 
							(serverStatusMap.containsKey(ps) && serverStatusMap.get(ps) != null && ! serverStatusMap.get(ps).equals(ss))) {
															
						//update value
						serverStatusMap.put(ps, ss);
						
						hasServerStatusChanged = true;
						
					}
					
					if ( hasServerStatusChanged && (ss.equals(ServerStatus.NotRunning) || ss.equals(ServerStatus.Disabled)) ) {
													
						serverNode.clearChildren();
						WebServicesUtil.removePafServerAck(ps.getCompleteWSDLService());
						ServerMonitor.getInstance().removeApplicationState(ps);

//						WebServicesUtil.resetPafServiceAckMap(ps.getCompleteWSDLService());

//						ServerMonitor.getInstance().refreshServerAckAndServiceAck(ps);
//						ServerMonitor.getInstance().refreshAllServerStatusAndUpdateAcks();
			
						Display.getDefault().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								
								viewer.refresh(finalServerNode);
								
							}
						});
						
					} else if ( hasServerStatusChanged && ss.equals(ServerStatus.Running)) {
																			
						Display.getDefault().asyncExec(new Runnable() {
							
							@Override
							public void run() {

								PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(ps);
								ServerMonitor.getInstance().addApplicationState(ps);

								//wsdl might not be up yet
								if ( pafServerAck != null ) {
									createServerNodeChildren(finalServerNode, pafServerAck, false);
									
									viewer.collapseToLevel(finalServerNode, 0);
									
									viewer.refresh(finalServerNode, true);
									
									viewer.expandToLevel(finalServerNode, 1);
								} else {
									logger.error("pafServerAck was null for url " + ps.getCompleteWSDLService());
								}
							}
						});
					}
				}
				
			}
					
		} else if ( object != null && object instanceof ApplicationEvent ) {
			
			final ApplicationEvent ae = (ApplicationEvent) object;
			
			final PafServer ps = ae.getPafServer();
			
			boolean hasAppStateChanged = false;
			
			if ( ! serverAppEventMap.containsKey(ps) ) {
				serverAppEventMap.put(ps, ae);
			} else if (serverAppEventMap.get(ps) != null && ! serverAppEventMap.get(ps).equals(ae)) {
				hasAppStateChanged = true;
			}
			
			if ( hasAppStateChanged ) {			
									
				ServerFolderNode root = (ServerFolderNode) viewer.getInput();
				
				//ServerNode serverNode = null;
				
				if ( root != null ) {
				
					ServerFolderNode pafServersTreeNode = (ServerFolderNode) root.getChildren().get(0);

					//final ServerNode finalServerNode = TreeNodeUtil.findServerNode(pafServersTreeNode, ps);
					
					final ServerNode finalServerNode;
					if(pafServersTreeNode.hasGroupNodes()){
						finalServerNode = TreeNodeUtil.findServerNode(pafServersTreeNode.getChildren(), ps);
					} else {
						finalServerNode = TreeNodeUtil.findServerNode(pafServersTreeNode, ps);
					}
					

					
					ApplicationEvent lastAppEvent = serverAppEventMap.get(ps);
					
					if ( lastAppEvent != null ) {
					
						String appChangeMessage = "Application state changed on server '" + ps.getName() + "' from [" + lastAppEvent.getAppId() + "]->" + lastAppEvent.getRunningState() + " to [" + ae.getAppId() + "]->" + ae.getRunningState();
						
						logger.info(appChangeMessage);
						
						ConsoleWriter.writeMessage(appChangeMessage);
						
					}
					
					Display.getDefault().asyncExec(new Runnable() {
						
						@Override
						public void run() {
					
							PafServer pafServer = ae.getPafServer();
							PafServerAck pafServerAck = null;
							if(pafServer != null){
								pafServerAck = WebServicesUtil.getPafServerAck(pafServer);
								if ( pafServerAck != null ) {
									logger.info("Creating application tree because of application state change");
									createApplicationTree(finalServerNode, pafServerAck, false);
									viewer.refresh(finalServerNode, true);
								} else {
									logger.error("pafServerAck was null for url " + pafServer.getCompleteWSDLService());
								}
							} else {
								logger.error("pafServerAck was null");
							}
							
						}
						
					});
					
				}
				
				serverAppEventMap.put(ps, ae);
				
			}
			
		}
		
	}
	
	

	

}

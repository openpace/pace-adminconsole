/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.RenameServerDialog;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.views.ServerView;

public class RenameServerAction extends Action implements ISelectionListener {
	private final IWorkbenchWindow window;
	private IStructuredSelection selection;
	private static Logger logger = Logger.getLogger(RenameServerAction.class);
	private final static int OK_ID = 0;
	private String messageBoxTitle = "Rename Server Connector";

	public RenameServerAction(String text, IWorkbenchWindow window) {
		super(text);
		logger.debug("Creating RenameServerAction");
		this.window = window;
		setId(ICommandIds.CMD_RENAME_SERVER);
        //setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD)));
		window.getSelectionService().addSelectionListener(this);
	}
	
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		if (incoming instanceof StructuredSelection) {
			selection  = (StructuredSelection) incoming;
		}
	}

	@Override
	public void run() {
		//if user wants to delete a default print style
		if (selection.getFirstElement() instanceof ServerNode
				&& !(selection.getFirstElement() instanceof ServerFolderNode)) {
			
			ServerNode serverNode = (ServerNode) selection.getFirstElement();
			String oldServerName = serverNode.getName();
			RenameServerDialog dialog = new RenameServerDialog(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					oldServerName, "Old Server Connector Name:", "New Server Connector Name:", messageBoxTitle);
							
			int rc = dialog.open();
			//if user clicks ok
			if ( rc == OK_ID ) {
				//get new print style name
				String newServerName = dialog.getNewServerName();
				logger.info("Old Server Name: " + oldServerName + "; New Serve Name: " + newServerName);                                               
				//rename server name
				try {
					PafServerUtil.renamePafServer(oldServerName, newServerName);
					
					RefreshServersAction refreshServerAction = new RefreshServersAction("", ServerView.getViewer());
					refreshServerAction.run();
				} catch (PafServerNotFound e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}

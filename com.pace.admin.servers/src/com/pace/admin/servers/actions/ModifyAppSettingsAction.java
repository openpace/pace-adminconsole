/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

import com.pace.admin.global.util.DomainUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.base.AuthMode;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafServiceProviderService;

/**
 * Opens eclipse browser to allow users to tweek server applicaiton settings.
 * 
 * @author JMilliron
 *
 */
public class ModifyAppSettingsAction extends Action implements ISelectionListener  {

	// logger
	private static Logger logger = Logger.getLogger(ModifyAppSettingsAction.class);

	private final IWorkbenchWindow window;

	private IStructuredSelection selection = null;

	/**
	 * Constructor
	 * 
	 * @param text
	 * @param window
	 */
	public ModifyAppSettingsAction(String text, IWorkbenchWindow window) {
		super(text);
		this.window = window;
	
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_MODIFY_APP_SETTINGS);

		setToolTipText(text);
	
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * 
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof IStructuredSelection) {
					
			this.selection = (IStructuredSelection) selection;
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		if ( selection instanceof IStructuredSelection ) {
					
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				
				if (structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof ServerNode ) {
			
					ServerNode serverNode = (ServerNode) selection.getFirstElement();
							
					logger.info("Setting default server to '" + serverNode.getName() + "'");
					
					PafServer server = serverNode.getServer();
					
					if ( server != null ) {
						IWorkbenchBrowserSupport service = PlatformUI.getWorkbench().getBrowserSupport();
						try {
							
							IWebBrowser browser = service.createBrowser(IWorkbenchBrowserSupport.AS_EDITOR, null, "Modify Server/Application Settings ("+server.getName()+")","Modify settings for "+server.getName());
							URL url = new URL(server.getCompleteServerSettingsUrl());
							
							// TTN-1718 - pass system domain and user names to settings app if system is in mixed mode 
							String serviceUrl = PafServerUtil.getServerWebserviceUrl(server.getName());
							if (serviceUrl != null) {
								
								//get server ack from web service
								PafServerAck serverAck = WebServicesUtil.getPafServerAck(serviceUrl);
								ServerSession session = SecurityManager.getSession(serviceUrl);
								
								if ( serverAck != null ) {
									
									session.setPafServerAck(serverAck);
								}
								
								boolean isMixedMode = session.isMixedAuthMode();
								
								if(isMixedMode) {
									
									String domainName = DomainUtil.getDomainName();
									String domainUserName = DomainUtil.getUserName();
									String domainUserSID = DomainUtil.getUserSid();
									
									url = new URL(server.getCompleteServerSettingsUrl() + "?domainName=" + URLEncoder.encode(domainName, "UTF-8") 
											+ "&domainUserName=" + URLEncoder.encode(domainUserName, "UTF-8") 
											+ "&domainUserSID=" + URLEncoder.encode(domainUserSID, "UTF-8"));
								}
								
								logger.info("Opening url: " + url.toString());
								
								browser.openURL(url);
							}
							
						} catch (PartInitException e) {
							logger.error(e.getMessage());
							
						} catch (MalformedURLException e) {
							logger.error(e.getMessage());
							
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
			}
		}
	}
}

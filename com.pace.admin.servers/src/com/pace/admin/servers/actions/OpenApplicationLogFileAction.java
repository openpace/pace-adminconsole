/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.MessageConsole;

import com.pace.admin.global.util.ConsoleUtil;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.base.ui.PafServer;

/**
 * @since 3.0
 */
public class OpenApplicationLogFileAction extends Action implements ISelectionListener {

	private static final String ID = "com.pace.admin.servers.actions.OpenServerFileAction";

	private IWorkbenchWindow fWindow;

	private IStructuredSelection selection;

	public OpenApplicationLogFileAction(String text, IWorkbenchWindow fWindow) {

		super(text);
		setEnabled(true);
		this.fWindow = fWindow;

		setId(ID + text);

		fWindow.getSelectionService().addSelectionListener(this);

	}

	/*
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) {

		run();
	}

	/*
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {

		if (selection != null) {
			ApplicationNode appNode = (ApplicationNode) selection.getFirstElement();
			PafServer pafServer = appNode.getServer();
			String serverName = pafServer.getName();
			MessageConsole msgConsole = ConsoleUtil.findConsole(serverName);
			if( msgConsole != null ) {
				msgConsole.activate();
			}
		}

	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
		}

	}

	public void dispose() {
		fWindow.getSelectionService().removeSelectionListener(this);

	}
}
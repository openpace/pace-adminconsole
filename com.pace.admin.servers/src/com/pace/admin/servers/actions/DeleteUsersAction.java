/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.GenericAddRemoveDialog;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafUserDef;

public class DeleteUsersAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger.getLogger(EditUserAction.class);
	private String serverName;
	private String url;
	private PafUserDef[] users;

	// holds console
	//private MessageConsole myConsole = ConsoleUtil.findConsole(Constants.CONSOLE_NAME);

	// allows user to write to console
	//private MessageConsoleStream msgStream = myConsole.newMessageStream();

	// workbench window
	private IWorkbenchWindow window;

	// current db user node
	private NativeUsersNode nativeDbUsersNode;
	
	/**
	 * Constructor.  Sets up the action with ID and text.  Registers with selection service.
	 * 
	 * @param text
	 * @param window
	 */
	public DeleteUsersAction(String text, IWorkbenchWindow window) {
		super(text);

		// set window
		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_EDIT_USER);

		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_EDIT_USER);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/user.png"));

		// set tool tip text
		setToolTipText("Delete Users");

		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		if( nativeDbUsersNode != null && nativeDbUsersNode.getServer()!= null ) {
			serverName = nativeDbUsersNode.getServer().getName();
			url = PafServerUtil.getServerWebserviceUrl(serverName);
			ServerSession serverSession = SecurityManager.getSession(url);
			if ( serverSession != null ) {
				
				String currentAdminUser = serverSession.getUserName();
				users = SecurityManager.getDbUsersFromServer(url);
				List<String> allUsers  = new ArrayList<String>();
				for( PafUserDef user : users ) {
					if( ! user.getUserName().equals(currentAdminUser) ) {
						allUsers.add(user.getUserName());
					}
				}
				
				GenericAddRemoveDialog dlg = new GenericAddRemoveDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"Delete Users",
						"Select the users(s) that will be removed:",
						allUsers.toArray(new String[0]),
						new String[0],
						true);
				
		    	//wait for a return call, if it's ok, then do something.
		    	if(dlg.open() == IDialogConstants.OK_ID){
		    		
		    		List<String> selectedUsers  = new ArrayList<String>();
		    		selectedUsers.addAll(Arrays.asList(dlg.getSelectedItems()));
		    
		    		if( GUIUtil.askUserAQuestion("Delete Users", "You are about to delete user(s): " + selectedUsers + ", do you still want to continue?")){
		    		
						deleteSelectedUsers(selectedUsers);

						final DBUsersNode dbUsersNode = nativeDbUsersNode
								.getDBUsersNode();

						BusyIndicator.showWhile(PlatformUI.getWorkbench()
								.getDisplay(), new Runnable() {

							public void run() {

								// Refresh the users subtree
								ServerView
										.createDBUsersModel(dbUsersNode, true);

							}

						});
		    		}
		    	}
			}
		}
	}

	private void deleteSelectedUsers(List<String> usersToDelete) {
		for( String userToDelete : usersToDelete ) {

			if (url != null) {

				try {
					ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
				} catch (PafServerNotFound e1) {
					logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
				} catch (ServerNotRunningException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// if still authenticated
				if( ! isUserAdmin(userToDelete) 
								||	 GUIUtil.askUserAQuestion(
										"Delete Admin User?", 
										"[" + userToDelete + "] is an admin user, do you still want to delete this user?") ){
						
					// try to delete user
					boolean userDeleted = SecurityManager
							.deleteDBUserFromServer(serverName, userToDelete);

					// write message to console
					ConsoleWriter.writeMessage("Deletion of user "
								+ userToDelete + " was successful: "
								+ userDeleted);
				}
			}			
		}
		
	}

	private boolean isUserAdmin(String userToDelete) {
		for( PafUserDef user : users ) {
			if( user.getUserName().equals(userToDelete) && user.isAdmin()) {
				if( user.isAdmin() ) {
					return true;
				}
			}
		}
		return false;
	}
	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// check selection to seei fstructured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			// Get the selected node
			if (currentSelection.getFirstElement() instanceof NativeUsersNode) {

				nativeDbUsersNode = (NativeUsersNode) currentSelection.getFirstElement();

			}
		}

	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.eclipse.jface.action.Action;

import com.pace.admin.servers.ServersPlugin;
import com.pace.admin.servers.views.ServerView;


/**
 * Collapses the contents of the viewer.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class CollapseAllAction extends Action {
	
	
	public CollapseAllAction(String toolTipText) {
		super();

		setToolTipText(toolTipText);
		setImageDescriptor(ServersPlugin.getImageDescriptor("/icons/collapseall.gif"));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		//refresh menu's viewer
		ServerView serverView = (ServerView) ServersPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ServerView.ID);

		if ( serverView != null ) {
					
			serverView.getViewer().collapseAll();
			serverView.getViewer().expandToLevel(2);
	
		}
		
	}

}

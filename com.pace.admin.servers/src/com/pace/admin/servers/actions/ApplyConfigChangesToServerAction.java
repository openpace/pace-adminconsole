/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.MessageConsole;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.jobs.ApplyConfigChangesJob;
import com.pace.admin.global.util.ConsoleUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.base.ui.PafServer;

public class ApplyConfigChangesToServerAction extends Action implements ISelectionListener {
	
	// logger
	private static Logger logger = Logger.getLogger(ApplyConfigChangesToServerAction.class);

	private IStructuredSelection selection;

	private IWorkbenchWindow window;
	
	public ApplyConfigChangesToServerAction(IWorkbenchWindow window) {
		super(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_UPLOAD_CONFIG_CHANGES);
	
		this.window = window;
		
		// setImageDescriptor(com.pace.admin.servers.ServersPlugin.getImageDescriptor("/icons/deployed.gif"));
		setToolTipText(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
	
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof IStructuredSelection) {
					
			this.selection = (IStructuredSelection) selection;
			
		}
	}

	public void run() {
		
		logger.info(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION);
		
		if ( selection instanceof IStructuredSelection ) {
			
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			
			if (structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof ApplicationNode ) {
		
				ApplicationNode appNode = (ApplicationNode) structuredSelection.getFirstElement();
																
				PafServer server = appNode.getServer();
				
				if (!GUIUtil
						.askUserAQuestion(Constants.RELOAD_SERVERS_APPLICATION_CONFIGURATION + " on server '" + server.getName() +"'?")){
					return;
				}
				
				MessageConsole msgConsole = ConsoleUtil.findConsole(server.getName());
				if( msgConsole != null ) {
					msgConsole.activate();
				}

				//apply config changes
				(new ApplyConfigChangesJob(server, appNode.getName())).schedule();
				
			}	
		}
			
		
	}
}

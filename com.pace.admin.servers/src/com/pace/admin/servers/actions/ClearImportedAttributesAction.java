/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.dialogs.GenericAddRemoveDialog;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.ServersPlugin;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafClearImportedAttrRequest;
import com.pace.server.client.PafClearImportedAttrResponse;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafService;

public class ClearImportedAttributesAction extends Action implements
		ISelectionListener {

	private static Logger logger = Logger.getLogger(ClearImportedAttributesAction.class);
	private IWorkbenchWindow window;
	private IStructuredSelection selection = null;
	private java.util.List<String> availabledAttributeDimensions = new ArrayList<String>();
	private java.util.List<String> checkedAttributeDimensions = new ArrayList<String>();
	private String serverErrorMesage;
	
	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 */
	public ClearImportedAttributesAction(String text, IWorkbenchWindow window) {
		super(text);

	    //set the window.
	    this.window = window;

        // The id is used to refer to the action in a menu or toolbar
        setId(ICommandIds.CMD_CLEAR_IMPORT_ATTRIBUTES_DIALOG);
        
        // Associate the action with a pre-defined command, to allow key bindings.
        //setActionDefinitionId(ICommandIds.CMD_CLEAR_IMPORT_ATTRIBUTES_DIALOG);
        
        //set image
        //setImageDescriptor();
        
        //set tool tip text
        setToolTipText("Clear Import Attributes");
        
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
	}
	
	/**
	 * 
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		if (incoming instanceof IStructuredSelection) {

			// Selection containing elements
			IStructuredSelection selection = (IStructuredSelection) incoming;

			// if server node but not folder node
			if (selection.getFirstElement() instanceof TreeNode
					&& !(selection.getFirstElement() instanceof ServerFolderNode)) {

				this.selection = selection;

			}
		}
	}
	
	/**
	 * Creates the AttributeImportDialog window.
	 */
	public void run() {
		
		TreeNode serverNode = ((TreeNode) selection.getFirstElement()).getParent();
		final String serverName = serverNode.getName();
		final String url = PafServerUtil.getServerWebserviceUrl(serverName);
		ServerSession serverSession = null;
		
		try {
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
		} catch (PafServerNotFound e1) {
			logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (url != null &&  SecurityManager.isAuthenticated(url) ) {
			try {
				//get the session from the security manager.
				serverSession = SecurityManager.getSession(url);
				//get an array of attribute dimensions.
				PafMdbProps mdbProps = DimensionTreeUtility.getMdbProps(
						PafServerUtil.getServer(serverName), 
						serverSession.getPafServerAck().getApplicationId());
				
				//if the attribute array if not null
				if (mdbProps != null && mdbProps.getAttributeDims() != null) {
					
					availabledAttributeDimensions = mdbProps.getAttributeDims();
					
					GenericAddRemoveDialog dlg = new GenericAddRemoveDialog(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
							"Clear Imported Essbase Attributes",
							"Select the Attribute Dimension(s) that will be removed: (Only currently imported attributes will appear below)",
							availabledAttributeDimensions.toArray(new String[0]),
							new String[0],
							true);
					
			    	//wait for a return call, if it's ok, then do something.
			    	if(dlg.open() == IDialogConstants.OK_ID){
			    		if(! GUIUtil.askUserAQuestion("Clear Imported Attributes","Are you sure you want to clear the selected attributes?" +
			    				"\nThis action cannot be reversed.")){
			    			return;
			    		}
			    		
						checkedAttributeDimensions.clear();
						checkedAttributeDimensions.addAll(Arrays.asList(dlg.getSelectedItems()));
						
						//set the client id.
						final String clientId = serverSession.getClientId();
						final String sessionToken = serverSession.getSecurityToken();
						
						// create new startup job
						Job importEssAtribJob = new Job("Clear Imported Essbase Attributes") {
							@Override
							protected IStatus run(IProgressMonitor monitor) {
								
								monitor.beginTask("Clearing Imported Essbase Attributes", IProgressMonitor.UNKNOWN);
								
								try {
						    		//create a request object.
						    		PafClearImportedAttrRequest req = new PafClearImportedAttrRequest();
						    		req.setClientId(clientId);
						    		req.setSessionToken(sessionToken);
						    		req.setClearAllDimensions(false);
						    		
						    		//made web service change
						    		if (checkedAttributeDimensions != null && checkedAttributeDimensions.size() > 0 ) {
						    			
						    			req.getDimensionsToClear().addAll(checkedAttributeDimensions);
						    			
						    		}
						    		
						    		PafService service = WebServicesUtil.getPafService(url);
						    		
						    		if( service != null ) {
							    		//set the request to the server.
							    		PafClearImportedAttrResponse res = service.clearImportedMdbAttributeDims(req);
							    		
							    		if(! res.isSuccess()){
							    			throw new Exception("Error clearing attributes.");
							    		}
							    		
						    		}
						    		
						    		return Status.OK_STATUS;
						    		
								}catch (Exception e) {
									serverErrorMesage = e.getMessage();
									e.printStackTrace();
									logger.error(e.getMessage());
									return Status.CANCEL_STATUS;
								} finally{
									monitor.done();
								}
							}
						};

						//set as user job
						importEssAtribJob.setUser(true);
						
						// set job priroity
						importEssAtribJob.setPriority(Job.LONG);
						
						// add a finished job listener
						importEssAtribJob.addJobChangeListener(new JobChangeAdapter() {
							// called when job is finished
							public void done(IJobChangeEvent event) {
								logger.info("Clearing Imported Essbase Attributes...");
								// if result is ok
								if (event.getResult().isOK()) {
									// tell display to run job next time possible
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											//force a refresh of the dimension cache.
								    		if(ServersPlugin.getDefault().getServerView().getDownloadCubeChangesFromServerAction() != null){
								    			DownloadCubeChangesFromServerAction r = ServersPlugin.getDefault().getServerView().getDownloadCubeChangesFromServerAction();
								    			r.runJob(true, url, serverName);
								    		}
							    			MessageDialog.openInformation(
							    					window.getShell(),
							    					"Process Complete",
							    					"Successfully cleared imported essbase attributes.");
										}
									});
								}else{
									//tell display to run job next time possible
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											MessageDialog.openError(window.getShell(),
							    					"Server returned error",
							    					serverErrorMesage);
										}
									});
								}
							}
						});

						// schedule job to run
						importEssAtribJob.schedule();
			    	}
			    	else{
			    		logger.info("Click cancel.");
			    	}
				}
			}catch(Exception ex){
				ex.printStackTrace();
		    	logger.error(ex.getMessage());
		    }
		}
	}
}
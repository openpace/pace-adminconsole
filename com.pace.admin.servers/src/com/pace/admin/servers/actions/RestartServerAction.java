/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerManager;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.LinuxServerLoginDialog;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.base.server.ServerPlatform;
import com.pace.base.ui.PafServer;

public class RestartServerAction extends Action implements ISelectionListener {
	
	private static final Logger logger = Logger.getLogger(RestartServerAction.class);

	private final IWorkbenchWindow window;
	
	private StructuredSelection selection;

	public RestartServerAction(String text, IWorkbenchWindow window) {
		// call parent
		super(text);

		// set window
		this.window = window;
		
		setToolTipText("Restarting Server");
		
		setId(ICommandIds.CMD_RESTART_SERVER);
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}
		
	
	
	@Override
	public void run() {
		
		if ( selection != null && selection.getFirstElement() instanceof ServerNode ) {
			
			ServerNode serverNode = (ServerNode) selection.getFirstElement();
			
			String serverName = serverNode.getName();
			
			try {
				
				PafServer server = null;
				// if server is not null, try to get server, else get default
				if (serverName != null) {
					server = PafServerUtil.getServer(serverName);
				} else {
					server = PafServerUtil.getDefaultServer();
				}
				
				if( server.getServerPlatform() == null 
						|| server.getServerPlatform().equals(ServerPlatform.Windows ) ) {
					ServerManager.getInstance().restartServer(server, null, null );
				}
				//Open LinuxLoginDialogBox if Linux Server
				else { //if( server.getServerPlatform().equals(ServerPlatform.Linux) ) 
					//if server login is not cached, open Linux server login dialog
					if( SecurityManager.getLinuxServerLogin(serverName)==null 
							|| System.currentTimeMillis() > SecurityManager.getLinuxServerLogin(serverName).getExpireTime() ) {
						LinuxServerLoginDialog loginDialog = new LinuxServerLoginDialog(window.getShell(), serverName);
						if ( loginDialog.open() != IDialogConstants.OK_ID ) {
							return;
						}
						ServerManager.getInstance().restartServer(server,
							loginDialog.getUserName(), loginDialog.getPassword() );
					}
					else {
						ServerManager.getInstance().restartServer(server,
								SecurityManager.getLinuxServerLogin(serverName).getUserName(),
								SecurityManager.getLinuxServerLogin(serverName).getPassword() );
					}
				}
				
				
			} catch (PafServerNotFound e) {

				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
		
	}



	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		
		if ( incoming instanceof StructuredSelection) {
			this.selection = (StructuredSelection) incoming;
		}
		
	}
	
	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

}

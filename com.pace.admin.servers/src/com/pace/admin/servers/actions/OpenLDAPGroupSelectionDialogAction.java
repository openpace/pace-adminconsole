/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.PaceSecurityGroupSelectionDialog;
import com.pace.admin.servers.dialogs.input.PaceSecurityGroupSelectionInput;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.DomainNode;
import com.pace.admin.servers.nodes.IDBUsersNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafException;
import com.pace.server.client.PafSecurityDomainGroups;
import com.pace.server.client.SecurityGroup;

/**
* Opens the String Array Selection Dialog.
* @author jmilliron
* @version	x.xx
*/
public class OpenLDAPGroupSelectionDialogAction extends Action implements ISelectionListener {
	
	private static Logger logger = Logger.getLogger(OpenLDAPGroupSelectionDialogAction.class);
	
	private IWorkbenchWindow window;
	
	private IStructuredSelection selection = null;
	
	private TreeNode treeNode;	
	
	private String domainName;
	
	private class TmpClass { 
	
		private boolean successful;

		public boolean isSuccessful() {
			return successful;
		}

		public void setSuccessful(boolean successful) {
			this.successful = successful;
		}

		
		
	}
	
	/**
	 * Constructor.
	 * @param text The text of node that was clicked.
	 * @param window The Workbench window.
	 */
	public OpenLDAPGroupSelectionDialogAction(IWorkbenchWindow window, String text) {
		
		super(text);
				
	    //set the window.
		this.window = window;

		//initial domain selection
		this.domainName = domainName;
		
        // The id is used to refer to the action in a menu or toolbar
        setId(ICommandIds.CMD_LDAP_GROUPS_DIALOG);
              
        
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
		
	    }
	
	/**
	 * Creates the window.
	 */
	public void run() {
		
		String serverName = treeNode.getServer().getName();	
		
		final String url = PafServerUtil.getServerWebserviceUrl(serverName);
									
		try {
			
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
			
		} catch (PafServerNotFound e1) {
			//do nothing
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (url != null &&  SecurityManager.isAuthenticated(url)) {

			final Display display = PlatformUI.getWorkbench().getDisplay();
			
			final TmpClass tmpObj = new TmpClass();
			
					
			BusyIndicator.showWhile(display, new Runnable() {
												
				public void run() {
					
					try {	
						
						List<PafSecurityDomainGroups> domainSecurityGroupsList = SecurityManager.getDomainSecurityGroupsFromServer(url);
						
						Map<String, Set<String>> domainPaceSecurityGroupMap = WebServicesUtil.createDomainSecurityGroupMap(domainSecurityGroupsList);
						
						SecurityGroup[] activePaceSecurityGroups = SecurityManager.getPaceGroups(url);
				
						Map<String, Set<String>> activeDomainPaceSecurityGroupMap = WebServicesUtil.createDomainSecurityGroupMap(activePaceSecurityGroups);
						
						PaceSecurityGroupSelectionInput dialogInput = new PaceSecurityGroupSelectionInput(domainPaceSecurityGroupMap, activeDomainPaceSecurityGroupMap);
						
						PaceSecurityGroupSelectionDialog dialog = new PaceSecurityGroupSelectionDialog(window.getShell(), dialogInput, domainName);
						
						if ( dialog.open() == IDialogConstants.OK_ID ) {
							
							activeDomainPaceSecurityGroupMap = dialogInput.getSelectedDomainPaceSecurityGroupMap();
							
							SecurityManager.setPaceGroups(url, activeDomainPaceSecurityGroupMap);
							
							tmpObj.setSuccessful(true);
							
						} 
					
					} catch (PafException e) {
						
						MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "Could not get Security Groups from server.");
						
						logger.error(e.getMessage());
						
					}

					
				}
			});
								
			
			if ( tmpObj.isSuccessful() ) {
			
				BusyIndicator.showWhile(display, new Runnable() {
				
					public void run() {
					
						DBUsersNode dbUsersNode = null;
						
						if ( treeNode instanceof DBUsersNode ) {
							
							dbUsersNode = (DBUsersNode) treeNode;
							
						} else if ( treeNode instanceof IDBUsersNode ) {
							
							dbUsersNode = ((IDBUsersNode) treeNode).getDBUsersNode();
							
						}
						
						final DBUsersNode dbUsersNodeFinal = dbUsersNode;  
						
						//create db users model
						ServerView.createDBUsersModel(dbUsersNodeFinal, ServerView.getViewer().getExpandedState(dbUsersNodeFinal));					
				        
					}
					
				});

			}
						
		}
		
		//null out domain name
		setDomainName(null);
	}
	
	//every time selection changes, enable/disable action based if server is
	//running or not.
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		if (incoming instanceof IStructuredSelection) {

			// Selection containing elements
			IStructuredSelection selection = (IStructuredSelection) incoming;

			// if server node but not folder node
			if (selection.getFirstElement() instanceof DBUsersNode || selection.getFirstElement() instanceof DomainNode ) {

				treeNode = (TreeNode) selection.getFirstElement();
											

			} /*else if ( selection.getFirstElement() instanceof DBUsersNode ) {
				
				dbUsersNode = (DBUsersNode) selection.getFirstElement();
				
			}*/
		}
	}





	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
}
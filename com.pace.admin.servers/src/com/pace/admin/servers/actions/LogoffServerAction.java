/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;

public class LogoffServerAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger.getLogger(LogoffServerAction.class);

	private IWorkbenchWindow window = null;
		
	private ServerNode serverNode;	
	
	public LogoffServerAction(String text, IWorkbenchWindow window) {
		super(text);

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_LOGOFF_SERVER);

		// set tool tip text
		setToolTipText("Logoff Server");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	public void run() {

		logger.info("Logging off server.");
		
		if (serverNode != null ) {
			
			String url = PafServerUtil.getServerWebserviceUrl(serverNode.getName());
			
			if ( url != null) {
							
				//try to remove session using URL
				SecurityManager.removeSession(url);
				
			}
		
			for ( Object serverChild : serverNode.getChildren()) {
				
				if ( serverChild instanceof DBUsersNode ) {
					
					DBUsersNode dbUsersNode = (DBUsersNode) serverChild;
					
					ServerView.createDBUsersModel(dbUsersNode, false);
					
					break;
				}
				
			}
			
			ConsoleWriter.writeMessage("Successfully logged off server '" + serverNode.getName() + "'");
			
		}		

	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		
		// check selection to seei fstructured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			// if server node but not folder node
			if ( currentSelection.getFirstElement() instanceof ServerNode ) {

				// Selection containing elements
				serverNode = (ServerNode) currentSelection.getFirstElement();

			}
		}
		
	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}

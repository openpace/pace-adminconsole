/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.dialogs.RenameDialog;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.GroupNode;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;

public class RenameServerGroupAction extends AbstractServerAction {
	
	public RenameServerGroupAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		super(text, ICommandIds.CMD_RENAME_SERVER_GROUP, window, viewer);
	}

	@Override
	public void run() {
		
		Object object = selection.getFirstElement();
		
		if ( object instanceof GroupNode ) {
			//Convert to a node
			GroupNode node = (GroupNode) object;
			//If the group is the default NO Group, then exit
			if(node.isBlank()){
				return;
			}
			List<String> groups = PafServerUtil.getServerGroups();
			groups.add(GroupNode.BLANK_GROUP_NAME);
			//Show the rename dialog
			RenameDialog dialog = new RenameDialog(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					groups,
					node.getName(), 
					"Old Server Group Name:",
					"New Server Group Name:", 
					"Rename Server Group",
					"The server group name can't be blank.",
					"Server group with that name already exists.");
			
			//If the user set a new name, procede with the group rename. 
			if ( dialog.open() == RenameDialog.OK_ID ) {
				String newGroupName = dialog.getNewName();
				for(PafServer server : PafServerUtil.getPafServers()){
					if(server.getServerGroupName().equals(node.getName())){
						server.setServerGroupName(newGroupName);
					}
				}
				//Perform refresh
				//Finally refresh the viewer.
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
					new Runnable() {
						public void run() {
							PafServerUtil.saveServersToXml();
							getViewer().setInput(ServerView.refreshServerModel(true));
							getViewer().expandToLevel(4);
						}
				});
			}
		} 
	}
}
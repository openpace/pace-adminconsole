/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.dialogs.ClearMemberTagDataDialog;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafException;
import com.pace.server.client.MemberTagInformation;
import com.pace.server.client.PafSoapException_Exception;

/**
 * Clears Member Tag Data from the server by App Id and (all Member Tags or filtered Member Tags)
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ClearMemberTagDataAction extends Action implements ISelectionListener {

	//logger
	private static Logger logger = Logger.getLogger(ClearMemberTagDataAction.class);
	
	//parent window
	private final IWorkbenchWindow window;

	//current selection
	private IStructuredSelection selection;

	/**
	 * Opens the export member tag data dailog and then exports the cell notes.
	 * 
	 * @param window
	 */
	public ClearMemberTagDataAction(IWorkbenchWindow window) {
			
		super("Clear Member Tag Data");

		this.window = window;		
		
		logger.debug("Registering ClearMemberTagDataAction to Window Selection Service");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		if (selection.getFirstElement() instanceof ApplicationNode) {
			//get applicaiton node
			ApplicationNode appNode = (ApplicationNode) selection.getFirstElement();
			//get server node
			ServerNode serverNode = (ServerNode)appNode.getParent();
			//get server name			final String serverName = serverNode.getName();
			String serverName = serverNode.getName();
			//get server url
			final String url = PafServerUtil.getServerWebserviceUrl(serverName);
			
			try {
				ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
			} catch (PafServerNotFound e1) {
				logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
			} catch (ServerNotRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//if authenticated, continue
			if ( SecurityManager.isAuthenticated(url) ) {
				
				try {
										
					MemberTagInformation[] memberTagInfoAr = SecurityManager.getMemberTagInformation(url);
					
					if ( memberTagInfoAr != null && memberTagInfoAr.length > 0 ) {
						
						List<MemberTagInfo> memberTagInfoList = new ArrayList<MemberTagInfo>();
						
						for ( MemberTagInformation mti : memberTagInfoAr ) {
							
							memberTagInfoList.add(new MemberTagInfo(mti));
							
						}
						
					
						ClearMemberTagDataDialog dialog = new ClearMemberTagDataDialog(this.window.getShell(), serverName, memberTagInfoList.toArray(new MemberTagInfo[0]));
						
						if ( dialog.open() == IDialogConstants.OK_ID)  {
							
							runJob(url, dialog.getSelectedMemberTagInfos());
								
						}
						
					} else {
						
//						server didn't have cell notes to export
						MessageDialog.openInformation(
		    					window.getShell(),
		    					"Clear Member Tag Data",
		    					"The server doesn't have member tag data to clear.");
						
					}
					
				} catch (RemoteException e1) {
					
					logger.error(e1.getMessage());
					
				} catch (PafSoapException_Exception e1) {
					
					logger.error(e1.getMessage());
					
				} 
				
				
			}
			
		}

	}
	
	private void runJob(final String url, final MemberTagInfo[] memberTagInfoToClearAr ) {
				
		if (url != null && memberTagInfoToClearAr != null ) {
		
			Job clearMemberTagDataJob = new Job("Clear Member Tag Data") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {

					monitor.beginTask("Clearing Member Tag Data", IProgressMonitor.UNKNOWN);

					boolean successfullyCleared = false;
					try {
						successfullyCleared = SecurityManager.clearMemberTagData(url, memberTagInfoToClearAr);
						
					} catch (PafException e) {
						
						e.printStackTrace();
						
						successfullyCleared = false;
					}
					
					if ( successfullyCleared ) {
						
						return Status.OK_STATUS;
						
					} else {

						
						
						return Status.CANCEL_STATUS;
					}
										
				}
				
			};
			
			clearMemberTagDataJob.setUser(true);
			
			clearMemberTagDataJob.setPriority(Job.LONG);
			
			clearMemberTagDataJob.addJobChangeListener(new JobChangeAdapter() {
				
				@Override
				public void done(IJobChangeEvent event) {
					
					//if job complted successfully, dispaly message
					if (event.getResult().isOK()) {
						
						// tell display to run job next time possible
						Display.getDefault().asyncExec(new Runnable() {
							
							public void run() {
								
				    			MessageDialog.openInformation(
				    					window.getShell(),
				    					"Process Complete",
				    					"Successfully cleared member tag data.");
							}
						});
						
					} else {
						
//						 tell display to run job next time possible
						Display.getDefault().asyncExec(new Runnable() {
							
							public void run() {
								
				    			MessageDialog.openInformation(
				    					window.getShell(),
				    					Constants.DIALOG_ERROR_HEADING,
				    					"There was a problem clearing member tag data.");
							}
							
						});						
						
					}
				}

			});
	
			
			clearMemberTagDataJob.schedule();
		
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		//check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			selection  = (StructuredSelection) incoming;

		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		logger.debug("Unregistering ClearMemberTagDataAction from Window Selection Service");
		
		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);
		
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.Ostermiller.util.Base64;
import com.Ostermiller.util.CSVParser;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.ImportExportUsersDialog;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafUserDef;

/**
 * Implements importing users from a CSV file into a server
 * @author fskrgic
 *
 */
public class ImportUsersAction extends Action implements ISelectionListener {

	private static Logger logger = Logger.getLogger(ImportUsersAction.class);
	
	private NativeUsersNode nativeDbUsersNode;
	
	private IWorkbenchWindow window;
	
	
	/**
	 * Consructor
	 * @param text
	 * @param window
	 */
	public ImportUsersAction(String text, IWorkbenchWindow window){
		
		super(text);
		
		this.window = window;
		
		// Register with Selective Service :-)
		window.getSelectionService().addSelectionListener(this);
		
		// Set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin.getImageDescriptor("/icons/user.png"));

		// Set tool tip text
		setToolTipText("Import Native Users");
		
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_IMPORT_USERS);

		// Associate the action with a pre-defined command, to allow key bindings
		//setActionDefinitionId(ICommandIds.CMD_IMPORT_USERS);
		
	}
	
	
	/**
	 * Implementing ISelectionListener interface
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof StructuredSelection) {

			// Get current selection
			StructuredSelection currentSelection = (StructuredSelection) selection;

			// Get the selected node
			if (currentSelection.getFirstElement() instanceof NativeUsersNode) {

				nativeDbUsersNode = (NativeUsersNode) currentSelection.getFirstElement();

			}
		}
	}
	
	
	/**
	 * The action's run() method
	 */
	public void run(){
		
		// Set the shell
		Shell shell = window.getShell();
		
		// Create a new file dialog
		FileDialog fd =  new FileDialog(shell);
		
		// Set the title
		fd.setText("Import CSV File");
		
		// Set the default directory
		fd.setFilterPath("C:/");
		
		// Set the filtered file extension
        String[] filterExt = { "*.csv"};
        
        fd.setFilterExtensions(filterExt);
        
        // Get the selected path string
        String selected = fd.open();
        
        // Import the user csv file (if a file was selected)
        if (selected != null) {
        	
        	// Check for a valid file extension (TTN-2646)
        	//selected += "." + Constants.CSV_EXT;
        	if (!selected.endsWith(Constants.CSV_EXT)) {
        		MessageBox messageBox= new MessageBox(window.getShell(), SWT.ICON_ERROR);
        		messageBox.setMessage(String.format("The file you are importing from does not have a '%s' extension.", Constants.CSV_EXT));
        		messageBox.setText("Invalid File Extension");
        		messageBox.open();
        		return;
        	}
        	        	        
        	// Declare a file reader
	        FileReader input = null;
	        
	        // Values to be parsed from the file
	        String[][] values = null;
	        
	        // Try to parse the input
	        try {	        	
	        	input = new FileReader(selected);
	        	values = CSVParser.parse(input);       	
	        }
	        catch (Exception e){        	
	        	logger.error(e.getMessage());	        	
	        }
	        
	        // Declare the users to be created
	        List<PafUserDef> users = null;
	        
	        // If parsing was successful and the file was in the right format, create users
	        if(values != null){
	        	
				if (values.length > 0 && values[0].length == 7 &&
						values[0][0].equals("UserName") &&
						values[0][1].equals("Password") &&
						values[0][2].equals("Email") &&
						values[0][3].equals("FirstName") &&
						values[0][4].equals("LastName") &&
						values[0][5].equals("Administrator") &&
						values[0][6].equals("ChangePassword")
						){
									
					if (values.length > 1) {
						// Instantiate users
						users = new ArrayList<PafUserDef>();
						for (int i = 1; i < values.length; i++ ){
							
							String[] value = values[i];
	
							// Create a user and fill it in with parsed values
							PafUserDef tempUser = new PafUserDef();
	
							tempUser.setUserName(value[0]);
	
							String password = value[1];
	
							// If the password is in the Base64 format, decode it. Otherwise, use it as is
							if (Base64.isBase64(password)) {
	
								tempUser.setPassword(Base64.decode(password));
	
							} else {
	
								tempUser.setPassword(password);
	
							}
	
							tempUser.setEmail(value[2]);
	
							tempUser.setFirstName(value[3]);
	
							tempUser.setLastName(value[4]);
	
							tempUser.setAdmin(value[5].equalsIgnoreCase("T") ? true
									: false);
	
							tempUser.setChangePassword(value[6]
									.equalsIgnoreCase("T") ? true : false);
	
							// Add the new user to the users
							users.add(tempUser);
	
						}
					}
					else{
						
			        	MessageBox messageBox= new MessageBox(window.getShell(), SWT.ICON_ERROR);
			        	messageBox.setMessage("The file you are importing from is empty.");
			        	messageBox.setText("Empty File");
			        	messageBox.open();
			        	return;
					}
				}
				else{
		        	MessageBox messageBox= new MessageBox(window.getShell(), SWT.ICON_ERROR);
		        	messageBox.setMessage("The file you are importing from is not in valid format.");
		        	messageBox.setText("Invalid File");
		        	messageBox.open();
		        	return;
				}
			}
	        else{
	        	
	        	MessageBox messageBox= new MessageBox(window.getShell(), SWT.ICON_ERROR);
	        	messageBox.setMessage("The file you are importing from is not in valid format.");
	        	messageBox.setText("Invalid File");
	        	messageBox.open();
	        	return;
	        }
 	        
	        // Get the server name from the node
	        String serverName = nativeDbUsersNode.getServer().getName();
	        
	        // Get server web service URL
	        String url = PafServerUtil.getServerWebserviceUrl(serverName);
	
	        // Get the user names from the server
	        String [] serverUserNames = SecurityManager.getNativeDbUserNamesFromServer(url);
	        
	        // Get users from the server
	        PafUserDef [] serverUsers = SecurityManager.getDbUsersFromServer(url);
	        
	        // Get the current session
	        ServerSession serverSession = SecurityManager.getSession(url);
	        
	        // Get the current user
	        String currentUser = serverSession.getUserName();
	        
	        // Create an import/export dialog
	        ImportExportUsersDialog importExportUsersDialog = new ImportExportUsersDialog(shell, users.toArray(new PafUserDef [0]), serverUserNames, true);
	        
	        importExportUsersDialog.setBlockOnOpen(true);
	        
	        // Convert the user list to an array
	        PafUserDef [] userArray = users.toArray(new PafUserDef[0]);
	        
	        // If the dialog successfully opened, add the selected users to the server
	        if(importExportUsersDialog.open()==0){
	        	
	        	// Get the checked indeces
	        	List<Integer> checkedIndeces = importExportUsersDialog.getCheckedIndeces();
	        	
	        	// Get the set of all administrators
	        	HashSet<String> administrators = new HashSet<String>();
	        	
	        	for(PafUserDef serverUser : serverUsers){
	        		
	        		if(serverUser.isAdmin()){
	        			
	        			administrators.add(serverUser.getUserName());
	        			
	        		}
	        	}
	        	
	        	for(PafUserDef user : users){
	        		
	        		if(user.isAdmin()){
	        			
	        			administrators.add(user.getUserName());
	        			
	        		}
	        		else{
	        			
	        			administrators.remove(user.getUserName());
	        			
	        		}
	        	}
	        	
	        	for(Integer i : checkedIndeces){
	        		
	        		// Check whether imported and selected user already exists on the server 
	    			boolean doesNotExist = true;
	    			
	    			for(String serverUser : serverUserNames){
	    				
	    				if(serverUser.equals(userArray[i].getUserName())){
	    					
	    					doesNotExist = false;
	    					
	    				}
	    			}
	    			
	    			if(doesNotExist){
	    				
	    				// Create a new user
	    				SecurityManager.createDBUserOnServer(serverName, userArray[i]);
	    				
	    			}
	    			else{
	    				
	    				// 
	    				if(currentUser.equalsIgnoreCase(userArray[i].getUserName())){
	    					
	    		        	MessageBox messageBox= new MessageBox(window.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
	    		        	
	    		        	messageBox.setText("Overwriting yourself?");
	    		        	
	    		        	messageBox.setMessage("You are about to overwrite your own user information. If you don't know the password you are importing, you will not be able to log in. Do you still want to overwrite your own user information?");
	    		        	
	    		        	if(messageBox.open() == SWT.YES){
	    		        		
	    		        		if(administrators.isEmpty()){
	    		        			
	    		        			userArray[i].setAdmin(true);
	    		        			
	    		                	MessageBox infoMessage= new MessageBox(window.getShell(), SWT.ICON_INFORMATION);
	    		                	infoMessage.setText("Still aministrator");
	    		                	infoMessage.setMessage("After this import, there would have been no administrators left. I did not change your user type. You are still an administrator.");
	    		                	infoMessage.open();
	    		        			
	    		        		}
	    		        		
	    		        		SecurityManager.updateDBUserOnServer(serverName, userArray[i]);
	    		        		
	    		        	}
	    				}
	    				else{
	    					
	        				// Update the existing user
	        				SecurityManager.updateDBUserOnServer(serverName, userArray[i]);
	        				
	    				}				
	    			}
	        	}
	        }        
       }
        
        final DBUsersNode dbUsersNode = nativeDbUsersNode.getDBUsersNode();
        
        BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

			public void run() {
			
		        // Refresh the users subtree
		        ServerView.createDBUsersModel(dbUsersNode, true);
		        
			}
			
        });
        
	}
	
	
	/**
	 * Cleans up
	 *
	 */
	public void dispose() {

		// Remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}

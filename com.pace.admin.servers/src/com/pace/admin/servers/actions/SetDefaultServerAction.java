/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;

/**
 * Starts a server.
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class SetDefaultServerAction extends Action implements ISelectionListener {

	private static final String SET_DEFAULT_SERVER = "Set Default Server";

	private static final Logger logger = Logger
			.getLogger(SetDefaultServerAction.class);

	private final IWorkbenchWindow window;

	private StructuredSelection selection;

	/**
	 * constructor
	 * 
	 * @param text
	 *            Name of action
	 * @param window
	 *            Current window of display
	 */
	public SetDefaultServerAction(String text, IWorkbenchWindow window) {
		// call parent
		super(text);

		// set window
		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_SET_DEFAULT_SERVER);
		
		/*setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/startserveraction.gif"));*/
		setToolTipText(SET_DEFAULT_SERVER);

		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);

	}

	/**
	 * Method that is executed when action is triggered
	 * 
	 */
	public void run() {
		
		if ( selection != null && selection.getFirstElement() instanceof ServerNode ) {
					
			ServerNode serverNode = (ServerNode) selection.getFirstElement();
					
			logger.info("Setting default server to '" + serverNode.getName() + "'");
			
			final PafServer server = serverNode.getServer();
			
			PafServerUtil.setDefaultServer(server);
			
//			PafServer defaultServer = null;
//			
//			try {
//				defaultServer = PafServerUtil.getDefaultServer();
//			} catch (PafServerNotFound e) {
//				logger.error(e.getMessage());
//			}
//			
//			final PafServer finalDefaultServer = defaultServer;
			
			Display.getDefault().asyncExec(new Runnable() {
				
				@Override
				public void run() {

					RefreshServersAction refreshServerAction = new RefreshServersAction("", ServerView.getViewer());
					refreshServerAction.run();

//					if ( finalDefaultServer == null || ! finalDefaultServer.equals(server)) {
//						
//						GUIUtil.openMessageWindow(SET_DEFAULT_SERVER, "Server '" + server.getName() + "' was not set as default.", MessageDialog.ERROR);
//						
//					} else {
//						
//						GUIUtil.openMessageWindow(SET_DEFAULT_SERVER, "Server '" + server.getName() + "' was set as default.", MessageDialog.INFORMATION);
//						
//					}
					
				}
			});
			
				
		}
		
	}

	/**
	 * Called when a selection changes in the UI
	 * 
	 * @param part
	 *            part of the workbench
	 * @param incoming
	 *            the incoming selection
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		if ( incoming instanceof StructuredSelection) {
			this.selection = (StructuredSelection) incoming;
		}

	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}


	@Override
	public String toString() {
		return "StartServerAction";
	}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.base.ui.PafServer;

public class DisableEnableServerAction extends AbstractServerAction {

	public DisableEnableServerAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		super(text, ICommandIds.CMD_DISABLE_ENABLE_SERVER, window, viewer, AS_CHECK_BOX);
	}

	
	@Override
	public void run() {
		Object object = selection.getFirstElement();
		
		if ( object instanceof ServerNode ) {
			//disable or enable the node.
			ServerNode serverNode = (ServerNode) object;
			String serverName = "";
			if(serverNode != null){
				serverName = serverNode.getName();
				PafServer pafServer = null;
				try {
					pafServer = PafServerUtil.getServer(serverName);
					if(pafServer != null){
						pafServer.setDisabled(!isChecked());
						PafServerUtil.addPafServer(pafServer);
						ServerMonitor.getInstance().refreshAllServerStatus(pafServer);
					}
				} catch (PafServerNotFound e) {
					
					logger.error("Server '" + serverName + "' was not found.  Can't modify.");
				}
			}
		}

	}
	
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		// Selection containing elements
		boolean checked = false;
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
			if (selection.getFirstElement() instanceof ServerNode) {
				ServerNode serverNode = (ServerNode) selection.getFirstElement();
				if(serverNode != null){
					String serverName = serverNode.getName();
					PafServer pafServer = null;
					try {
						pafServer = PafServerUtil.getServer(serverName);
						checked = !pafServer.isDisabled();
					} catch (PafServerNotFound e) {
						
						logger.error("Server '" + serverName + "' was not found.  Can't modify.");
					}
				}
			}

		} 
		setChecked(checked);
	}

}

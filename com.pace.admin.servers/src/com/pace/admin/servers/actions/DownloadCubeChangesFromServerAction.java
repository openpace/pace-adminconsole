/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.io.File;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.FileUtils;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimTree;

public class DownloadCubeChangesFromServerAction extends Action implements ISelectionListener {
	public final static String ID = "com.pace.admin.servers.actions.DownloadCubeChangesFromServerAction";
	private static Logger logger = Logger.getLogger(DownloadCubeChangesFromServerAction.class);

	private final IWorkbenchWindow window;
	
	private PafServer server;
		
	private IStructuredSelection selection = null;
		
	public DownloadCubeChangesFromServerAction(IWorkbenchWindow window, PafServer server) {
		super(server!=null?server.getName():"Download Cube Changes");
	    this.window = window;
	    this.server = server;
        // The id is used to refer to the action in a menu or toolbar
        this.setId(ID);
//        setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD)));
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		// TODO Auto-generated method stub
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
		}
	}

	public void run() {
		
		String serverName = null;
		
		if ( server == null ) {
			
			if ( selection != null && selection instanceof IStructuredSelection && 
		
				((IStructuredSelection) selection).getFirstElement() instanceof ApplicationNode) {
			
				ApplicationNode appNode = (ApplicationNode) ((IStructuredSelection) selection).getFirstElement();
			
				if ( appNode.getParent() instanceof ServerNode ) {
				
					serverName = ((ServerNode) appNode.getParent()).getName();
				
				}
			
			}
			
		} else {
			
			serverName = server.getName();
			
		}
		
		if ( serverName != null ) {
		
		   	//using the current window, get the active page.
			
			String url = PafServerUtil.getServerWebserviceUrl(serverName);
	
			if (url != null) {
	
				try {
					ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
				} catch (PafServerNotFound e1) {
					logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
				} catch (ServerNotRunningException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				runJob(false, url, serverName);
	
			}
		} else {
			logger.error("Server name can not be null.");
		}
    	
	}

	public void runJob(final boolean silentRun, final String url,
			final String serverName) {

		if (url != null && serverName != null
				&& SecurityManager.isAuthenticated(url)) {

			boolean runJob = true;

			if (!silentRun) {

				runJob = GUIUtil
						.askUserAQuestion(
								"Download Cube Changes to Server",
								"Are you sure you want to clear existing dimension cache and download cube changes from the server?");

			}

			if (runJob) {
				Job refreshDimCacheJob = new Job("Download Cube Changes to Server...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {

						try {
							monitor.beginTask("Download Cube Changes to Server", IProgressMonitor.UNKNOWN);
							
							logger.info("Start Downloading Cube Changes to Server");
							
							ServerSession serverSession = SecurityManager.getSession(url);
							
							String appId = null;							
							
							if ( serverSession != null && serverSession.getPafServerAck() != null ) {
								
								appId = serverSession.getPafServerAck().getApplicationId();
								
							}
														
							// write to console
							ConsoleWriter.writeMessage("Trying to retrieve outline data from the server.");

							PafSimpleDimTree[] trees = null;
							
							try {
								trees = SecurityManager.getPafSimpleDimTrees(url);
								
							} catch (Exception e) {
								final String errMsg = e.getMessage();
								e.printStackTrace();
								logger.error(errMsg);
								
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {

										if (!silentRun) {
											MessageDialog.openInformation(window.getShell(), Constants.DIALOG_ERROR_HEADING, errMsg);
										}
									}
								});
								return Status.CANCEL_STATUS;
							}
														
							// if trees exists
							if (trees != null) {

								// write success message to server
								ConsoleWriter.writeMessage("Successfully retrieved outline data from the server '"
												+ serverName + "'.");

								// delete old cached files
								String serverCachedTreeDirectoryName = DimensionTreeUtility
										.getServerCachedTreeDir(PafServerUtil
												.getServer(serverName))
										+ File.separator
										+ appId;

								if (serverCachedTreeDirectoryName != null) {

									FileUtils.deleteFilesInDir(new File(
											serverCachedTreeDirectoryName), true);

								}

								// for each tree, cache
								for (PafSimpleDimTree tree : trees) {

									// get name of dimension
									String dimensionName = tree.getId();

									// cache dimension tree
									DimensionTreeUtility
											.setCachedDimensionTree(
													PafServerUtil.getServer(serverName),
													appId,
													dimensionName, tree);

									String msg = "Caching dimension tree '"
											+ dimensionName
											+ "' for application '"
											+ serverSession.getPafServerAck().getApplicationId()
											+ "' running on server '"
											+ serverName + "'.";

									// inform user of caching dim tree
									ConsoleWriter.writeMessage(msg);
									logger.info(msg);

								}

							}

						} catch (PafServerNotFound e) {
							// will not get this far is server is not there
						}

						
						if ( monitor.isCanceled() ) {
							return Status.CANCEL_STATUS;
						}
						
						
						try {

							//TTN-1371 - Start
							PafMdbProps pafMdbProps = SecurityManager.getPafMdbProps(url);
							
							ServerSession serverSession = SecurityManager.getSession(url);
							
							DimensionTreeUtility.setMdbProps(PafServerUtil
									.getServer(serverName), serverSession
									.getPafServerAck().getApplicationId(),
									pafMdbProps);

							//TTN-1371 - END
							
						} catch (Exception e) {
							
							e.printStackTrace();
							
							logger.error(e.getMessage());
							
							return Status.CANCEL_STATUS;
						}

						logger.info("Finished Download Dimension Cache");
						
						return Status.OK_STATUS;

					}

				};

				refreshDimCacheJob.setUser(true);
				
				// set job priroity
				refreshDimCacheJob.setPriority(Job.LONG);

				refreshDimCacheJob.addJobChangeListener(new JobChangeAdapter() {

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.core.runtime.jobs.JobChangeAdapter#done(org.eclipse.core.runtime.jobs.IJobChangeEvent)
					 */
					@Override
					public void done(IJobChangeEvent event) {

						if (event.getResult().isOK()) {

							// tell display to run job next time possible
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {

									if (!silentRun) {

										MessageDialog.openInformation(window
												.getShell(),
												"Process Complete",
												"Successfully cleared and downloaded dimension cache from server '"
														+ serverName + "'.");
									}
								}
							});
						} else {
							
							logger.error("Download Dimension Cache Action didn't complete successfully.");

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {

									if (!silentRun) {
										MessageDialog.openInformation(window
												.getShell(),
												"Process Incomplete",
												"Download Dimension Cache Action didn't complete successfully.");
									}
								}
							});
						}

					}

				});

				refreshDimCacheJob.schedule();

			}
		}
	}

	/**
	 * @return the server
	 */
	public PafServer getServer() {
		return server;
	}
		
}

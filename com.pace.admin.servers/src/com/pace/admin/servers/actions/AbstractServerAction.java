/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.actions.AbstractAction;

public abstract class AbstractServerAction extends AbstractAction {

	private TreeViewer viewer;
	
	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractServerAction(String name, String actionId, IWorkbenchWindow window, TreeViewer viewer) {
		super(name, actionId, window);
		this.viewer = viewer;
	}
	
	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractServerAction(String name, String actionId, IWorkbenchWindow window, TreeViewer viewer, int style) {
		super(name, actionId, window, style);
		this.viewer = viewer;
	}

	/**
	 * Constructor
	 * @param name
	 * @param window
	 */
	public AbstractServerAction(String name, String actionId, IWorkbenchWindow window) {
		this(name, actionId, window, null);
	}
	
	/**
	 * 
	 * @return the associated TreeViewer
	 */
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the associated TreeViewer 
	 * @param viewer
	 */
	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public abstract void run();
}

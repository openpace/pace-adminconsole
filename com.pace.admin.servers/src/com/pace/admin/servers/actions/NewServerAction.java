/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;

import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.ServerDialog;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;


/**
 * Creates a new server.
 *
 * @author jmilliron
 * @version	x.xx 
 *
 */
public class NewServerAction extends Action implements ISelectionListener, ActionFactory.IWorkbenchAction {

	private static Logger logger = Logger.getLogger(NewServerAction.class);
	
	private final IWorkbenchWindow window;

	private IStructuredSelection selection;
	
	public NewServerAction(String text, IWorkbenchWindow window) {
	
		super(text);
		
		logger.debug("Creating NewServerAction");
		
		// set window
		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CREATE_SERVER);
		
		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_CREATE_SERVER);

		setToolTipText("Create New Server");

		logger.debug("Registering NewServerAction to Window Selection Service");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
	
		//if server node but not folder node
		if (selection.getFirstElement() instanceof ServerFolderNode) {
			
				//create server dialog
				ServerDialog serverDialog = new ServerDialog(window.getShell(), null, true);
				
				//if ok
				if ( serverDialog.open() == Dialog.OK) {
					
					//get new server
					PafServer newPafServer = serverDialog.getPafServer();
					
					//add to list of servers
					PafServerUtil.addPafServer(newPafServer);
//					ServerMonitor.getInstance().refreshServerRunningMapAndUpdateServerAck(newPafServer);
					
					//update server viewer
					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
							new Runnable() {

								public void run() {
					
									ServerView.updateServerViewerWithModel();
									
								}
						
					});
					
					
				}
				
				
			
			
		}
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart arg0, ISelection incoming) {

		// check selection to seei fstructured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			selection  = (StructuredSelection) incoming;

			}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		logger.debug("Unregistering CloneServerAction from Window Selection Service");
		
		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);
		
	}


}

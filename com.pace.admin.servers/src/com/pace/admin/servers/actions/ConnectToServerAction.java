/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.views.ServerView;

/**
 * @author jmilliron
 * 
 */
public class ConnectToServerAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger
			.getLogger(ConnectToServerAction.class);

	private IWorkbenchWindow window;

	private DBUsersNode dbUsersNode;

	public ConnectToServerAction(String text, IWorkbenchWindow window) {
		
		super(text);
		
		this.window = window;
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}
	
	public void run() {
		
		logger.info("Trying to authenticate to server");
		
		String serverName = dbUsersNode.getParent().getParent().getName();
		
		try {
			ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName), true);
		} catch (PafServerNotFound e1) {
			logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
		} catch (ServerNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
//		 check selection to seei fstructured
		if (incoming instanceof StructuredSelection) {
			// get current selection
			IStructuredSelection selection = (StructuredSelection) incoming;
			Object selectedObject = selection.getFirstElement();
			if ( selectedObject instanceof DBUsersNode ) {
				dbUsersNode = (DBUsersNode) selectedObject;
			}
		}
	}
	
	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

}

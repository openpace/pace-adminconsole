/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.server.ServerManager;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.DeleteUserDialog;
import com.pace.admin.servers.dialogs.MakeAdminUserDialog;
import com.pace.admin.servers.dialogs.RemoveAdminUserDialog;
import com.pace.admin.servers.exceptions.UserNotFoundException;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.DomainNode;
import com.pace.admin.servers.nodes.LDAPUserNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.server.client.PafUserDef;

public class EditLDAPUserAction extends AbstractServerAction {
	
	private static final Logger logger = Logger.getLogger(EditLDAPUserAction.class);
	
	private boolean isAdmin;
	  
	private LDAPUserNode ldapUserNode;
	private String domainName;
	private String ldapUserName;
	private String ldapUserEmail;
	
	private PafUserDef pafUserDef;
	
	public EditLDAPUserAction(String text, IWorkbenchWindow window, TreeViewer viewer) {
		super(text, ICommandIds.CMD_EDIT_LDAP_ADMIN_USER, window, viewer, AS_CHECK_BOX);
	}
	  
	public void run() {
		
		String pafServerName = ldapUserNode.getServer().getName();
		String url = PafServerUtil.getServerWebserviceUrl(pafServerName);
		
		// remove existing admin record 
	    if(isAdmin){
	    	
	        RemoveAdminUserDialog dialog = new RemoveAdminUserDialog(window.getShell(),
					ldapUserNode.getName());
	        int result = dialog.open();
	      
	        // if ok was pressed
			if (result == 0) {
				
				TreeNode ldapDomain = ldapUserNode.getParent().getParent();
				
				// Remove LDAP admin record from db users table
				if ( ldapDomain instanceof DomainNode) {
					
					boolean success = removeLDAPAdminUser(url, pafServerName, pafUserDef);
					
					if (success) {
						isAdmin = false;
					}
				}
			}       
	    }
	    
	    else if (!isAdmin) {
	    	
	        MakeAdminUserDialog dialog = new MakeAdminUserDialog(window.getShell(),
					ldapUserNode.getName());
	        int result = dialog.open();
	        
	        // if ok was pressed
	     	if (result == 0) {
	     		
	     		TreeNode ldapDomain = ldapUserNode.getParent().getParent();
				
				// Create LDAP admin record in db users table
				if ( ldapDomain instanceof DomainNode) {
					
					DomainNode domainNode = (DomainNode) ldapDomain;
					domainName = domainNode.getName();
					
					boolean success = createLDAPAdminUser(pafServerName);
					
					if (success) {
						isAdmin = true;
					}
					isAdmin = false;
					
				}
	     	}
	    }
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 * Decides what happens when user changes selected item
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		
		if (incoming instanceof StructuredSelection) {
			StructuredSelection currentSelection = (StructuredSelection) incoming;
				if (currentSelection.getFirstElement() instanceof LDAPUserNode) {
					selection = currentSelection;
					ldapUserNode = (LDAPUserNode) currentSelection.getFirstElement();
					if (ldapUserNode != null) {
						
						//get server name
						String serverName = ldapUserNode.getServer().getName();
						
						// get domain name
						TreeNode ldapDomain = ldapUserNode.getParent().getParent();
						DomainNode domainNode = (DomainNode) ldapDomain;
						domainName = domainNode.getName();

						// get admin status from ldap user in security db	
						ldapUserName = ldapUserNode.getName();
						pafUserDef = SecurityManager.createLDAPAdminUserDef(ldapUserName, domainName);	
						
						//get url from server name
						String url = PafServerUtil.getServerWebserviceUrl(serverName);
							
						// check to see if selected user is in the security db	
						 boolean ldapAdminInSecurityDB = SecurityManager.getLDAPAdminUserFromServer(url, pafUserDef);
						 
						 if (ldapAdminInSecurityDB) {
							try {
								// get the generated user name for ldap admin user in security db
								String ldapAdminSecurityUserName = SecurityManager.getLDAPAdminUserNameFromServer(url, pafUserDef);
								PafUserDef serverPafUserDef = SecurityManager.getDbUserFromServer(url, ldapAdminSecurityUserName, domainName);
								logger.debug("User " + ldapUserName + " found.");
								if (serverPafUserDef.isAdmin()) {
									isAdmin = serverPafUserDef.isAdmin();
									setChecked(isAdmin);
								}
							} catch (UserNotFoundException e) {
								logger.debug("User " + ldapUserName + " not found.");
								isAdmin = false;
								setChecked(false);
								e.printStackTrace();
								return;
							}
						 } else if (!ldapAdminInSecurityDB) {
							 logger.debug("User " + ldapUserName + " not found.");
							 isAdmin = false;
							 setChecked(false);
							 return;
						 }
					}
				}
		}
	}
	
	private boolean removeLDAPAdminUser(String url, String pafServerName, PafUserDef pafUserDef) {
		
		if (pafUserDef != null) {
			boolean userDeleted = SecurityManager.deleteLDAPAdminUserFromServer(url, pafServerName, pafUserDef);
			
			logger.debug("Revoked LDAP admin status from user "
					+ ldapUserNode.getName() + " was successful: "
					+ userDeleted);
			
			if (userDeleted) {
				ConsoleWriter.writeMessage("Removed admin status from user: " + ldapUserNode.getName());
			} else if (!userDeleted) {
				logger.error("Admin status was NOT removed from user: " + ldapUserNode.getName());
			}
			
			// get parent dbusers node
			final DBUsersNode parent = ldapUserNode.getDBUsersNode();

			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

				public void run() {

						ServerView.createDBUsersModel(parent, true);	
					}
				});
			
			return userDeleted;
		}
			
		return false;
		
	}
	
	private boolean createLDAPAdminUser(String pafServerName) {
			
		PafUserDef pafUserDef = SecurityManager.createLDAPAdminUserDef(ldapUserName, domainName);
		
		boolean userCreated = SecurityManager.createDBUserOnServer(pafServerName, pafUserDef);

		logger.debug("Granted LDAP admin status to user "
				+ ldapUserNode.getName() + " was successful: "
				+ userCreated);
		
		if (userCreated) {
			ConsoleWriter.writeMessage("Granted LDAP admin status to user "
					+ ldapUserNode.getName());
		} else if (!userCreated) {
			logger.error("Admin status NOT granted to user " + ldapUserNode.getName());
		}
		
		// get parent dbusers node
		final DBUsersNode parent = ldapUserNode.getDBUsersNode();

		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

			public void run() {

				ServerView.createDBUsersModel(parent, true);	
			}
		});
		
		return userCreated;
	}
}

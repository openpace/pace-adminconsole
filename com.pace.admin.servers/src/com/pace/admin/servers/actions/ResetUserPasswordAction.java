/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.exceptions.UserNotFoundException;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafUserDef;

public class ResetUserPasswordAction extends Action implements
		ISelectionListener {

	// logger
	private static Logger logger = Logger
			.getLogger(ResetUserPasswordAction.class);

	private IWorkbenchWindow window;
	
	private IStructuredSelection selection;

	private DBUserNode dbUserNode;

	public ResetUserPasswordAction(IWorkbenchWindow window) {
		super();

		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_RESET_USER_PASSWORD);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/resetPass.ico"));

		// set tool tip text
		setToolTipText("Reset user password");

		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	public void run() {

//		 get server name
		String pafServerName = dbUserNode.getServer().getName();
		
		String url = PafServerUtil.getServerWebserviceUrl(pafServerName);

		if (url != null) {
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);

			// if server password reset is enabled
			if (pafServerAck != null
					&& pafServerAck.isServerPasswordResetEnabled()) {

				String userName = dbUserNode.getName().trim();

				boolean resetPassword = MessageDialog.openConfirm(window
						.getShell(), "Confirm", "Reset password for "
						+ userName + "?");

				if (resetPassword) {

					try {
						
						boolean passwordWasReset = SecurityManager.resetPasswordForDBUserOnServer(pafServerName, userName);
						
						if ( passwordWasReset) {

							PafUserDef userDef = SecurityManager.getDbUserFromServer(url, userName, PafBaseConstants.Native_Domain_Name);
							
							if ( userDef != null && userDef.getPassword() != null) {
							
								String message = "Password for user '" + userName + "' was successfully reset.  An email was sent to '" + userDef.getEmail()+ "'."; 
								
								logger.info(message);
							
								MessageDialog.openInformation(window.getShell(), Constants.DIALOG_INFO_HEADING, message);
								
							}

						}
						
					} catch (PafException e) {
						
						String errorMessage = null;
						
						if ( e.getCause() != null ) {
							errorMessage = e.getCause().getMessage();
						} else {
							errorMessage = e.getMessage();
						}
						
						MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, errorMessage);
						
					} catch (UserNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {

				MessageDialog.openInformation(window.getShell(), Constants.DIALOG_ERROR_HEADING,
						"Password reset is not enabled on the server.");

			}
		}

	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			// if server node but not folder node
			if (currentSelection.getFirstElement() instanceof DBUserNode) {

				// Selection containing elements
				selection = currentSelection;

				dbUserNode = (DBUserNode) currentSelection.getFirstElement();

				this.setText("Reset Password for " + dbUserNode.getName());

			}
		}

	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

}

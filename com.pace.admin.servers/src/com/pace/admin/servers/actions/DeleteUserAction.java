/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.console.ConsoleWriter;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.DeleteUserDialog;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafUserSecurity;

public class DeleteUserAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger.getLogger(DeleteUserAction.class);

	private IWorkbenchWindow window;
	
	private IStructuredSelection selection;

	private String initialText;

	// private String userName;
	private DBUserNode dbUserNode;

	/**
	 * Deletes a user from the webservice call
	 * 
	 * @param text	Name of action.
	 * @param window	Current window.
	 */
	public DeleteUserAction(String text, IWorkbenchWindow window) {
		super(text);

		this.window = window;
		
		initialText = text;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_DELETE_USER);

		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_DELETE_USER);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/delete_user.png"));

		// set tool tip text
		setToolTipText("Delete this user");

		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {

		DeleteUserDialog dialog = new DeleteUserDialog(window.getShell(),
				dbUserNode.getName());
		int result = dialog.open();

		// if ok was pressed
		if (result == 0) {

			// get server name
			String pafServerName = dbUserNode.getServer().getName();

			String url = PafServerUtil.getServerWebserviceUrl(pafServerName);

			if (url != null) {

				try {
					ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(pafServerName));
				} catch (PafServerNotFound e1) {
					logger.error("Server '" + pafServerName + "' is not defined in the " + Constants.SERVERS_FILE);
				} catch (ServerNotRunningException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// if still authenticated
				if (SecurityManager.isAuthenticated(url)) {

					// try to delete user
					boolean userDeleted = SecurityManager
							.deleteDBUserFromServer(pafServerName, dbUserNode
									.getName());

					// write message to console
					ConsoleWriter.writeMessage("Deletion of user "
								+ dbUserNode.getName() + " was successful: "
								+ userDeleted);
					

					// get parent dbusers node
					final DBUsersNode parent = dbUserNode.getDBUsersNode();

					BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

						public void run() {

							// create db users model
							ServerView.createDBUsersModel(parent, true);
							
						}
						
					});
					
					// if user was deleted
					if (userDeleted) {

						// if user wanted all user names removed from projects
						if (dialog.isDeleteAllReferences()) {

							// delete all user references from paf_security
							// fiels
							removeUsersSecurityFromProjects(dbUserNode
									.getName());

						}

					}

				}
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			// if server node but not folder node
			if (currentSelection.getFirstElement() instanceof DBUserNode) {

				// Selection containing elements
				selection = currentSelection;

				dbUserNode = (DBUserNode) currentSelection.getFirstElement();

				this.setText(initialText + " " + dbUserNode.getName());
				
				// TTN-2349 - Adding text to notify that the current user logged into the system can't be deleted
				String url = PafServerUtil.getServerWebserviceUrl(dbUserNode.getServer().getName());
				ServerSession serverSession = SecurityManager.getSession(url);
				String currentUser = serverSession.getUserName();
				String currentDBNodeUserName = dbUserNode.getName();
				boolean currentUserIsDBNodeUser = currentUser.equalsIgnoreCase(currentDBNodeUserName);
				if (currentUserIsDBNodeUser) {
					this.setText(initialText + " " + dbUserNode.getName() + " - [Can't delete current user]");
				}
				

			}
		}

	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}

	/** 
	 *	Removes the user from the paf_security.xml file
	 *
	 * @param userNameToRemove	User name to remove.
	 */
	public void removeUsersSecurityFromProjects(String userNameToRemove) {

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot rootWorkspace = workspace.getRoot();

		boolean userRemoved = false;

		// if projects exist
		if (rootWorkspace.getProjects().length != 0) {

			// populate ordered project map
			for (IProject project : rootWorkspace.getProjects()) {

				if (project.exists()) {

					IResource iResource = project
							.findMember(Constants.CONF_DIR);

					if (iResource.exists() && iResource instanceof IFolder) {

						IFolder confFolder = (IFolder) iResource;

						IResource fileResource = confFolder
								.findMember(PafBaseConstants.FN_SecurityMetaData);

						if (fileResource.exists()
								&& fileResource instanceof IFile) {

							IFile securityFile = (IFile) fileResource;

							PafUserSecurity[] pafUserSecurityAr = null;

							try {
								pafUserSecurityAr = (PafUserSecurity[]) ACPafXStream
										.importObjectFromXml(securityFile);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if (pafUserSecurityAr != null
									&& pafUserSecurityAr.length > 0) {

								List<PafUserSecurity> pafUserSecurityList = new ArrayList<PafUserSecurity>();

								// loop through users and filter out user to
								// delete
								for (PafUserSecurity pafUserSecurity : pafUserSecurityAr) {

									// only add users that don't match user name
									// to remove
									if (!pafUserSecurity.getUserName().trim()
											.equalsIgnoreCase(
													userNameToRemove.trim())) {

										pafUserSecurityList
												.add(pafUserSecurity);

									} else {
										
										userRemoved = true;
									}

								}

								// write out changes
								ACPafXStream
										.exportObjectToXml(
												pafUserSecurityList
														.toArray(new PafUserSecurity[0]),
												securityFile);

							}
						}

					}

				}
			}

		}

		if (userRemoved) {
			GUIUtil
					.openMessageWindow(
							"INFO",
							"User references removed from one or more projects.  Please upload updated project to server to reflect changes.", MessageDialog.INFORMATION);
		}

	}

}

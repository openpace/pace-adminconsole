/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.dialogs.ImportCellNotesDialog;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSimpleCellNoteImportRequest;
import com.pace.server.client.PafSimpleCellNoteImportResponse;
import com.pace.server.client.SimpleCellNote;

/**
 * Imports Cell Note Action
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ImportCellNotesAction extends Action implements ISelectionListener {
	
	//logger
	private static Logger logger = Logger.getLogger(ImportCellNotesAction.class);
	
	//parent window
	private final IWorkbenchWindow window;

	//current selection
	private IStructuredSelection selection;

	/**
	 * Opens the import cell notes dailog and then has the server import the cell notes.
	 * 
	 * @param window
	 */
	public ImportCellNotesAction(IWorkbenchWindow window) {
		
		super("Import Cell Notes");

		this.window = window;
		
		logger.debug("Registering ImportCellNotesAction to Window Selection Service");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
		
	}


	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
	
		if (selection.getFirstElement() instanceof ApplicationNode) {
			//get applicaiton node
			ApplicationNode appNode = (ApplicationNode) selection.getFirstElement();
			//get server node
			ServerNode serverNode = (ServerNode)appNode.getParent();
			//get server name			final String serverName = serverNode.getName();
			String serverName = serverNode.getName();
			//get server url
			final String url = PafServerUtil.getServerWebserviceUrl(serverName);
			
			ImportCellNotesDialog dialog = new ImportCellNotesDialog(window.getShell(), serverName);
				
			if ( dialog.open() == IDialogConstants.OK_ID) {

				try {
					ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
				} catch (PafServerNotFound e1) {
					logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
				} catch (ServerNotRunningException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
				if ( SecurityManager.isAuthenticated(url) ) {				
				
					final SimpleCellNote[] simpleCellNotesToImport = dialog.getSimpleCellNotesToImport();
					
		//					create job to export the cell notes
					Job importCellNoteJob = new Job("Import Cell Cotes") {
		
						@Override
						protected IStatus run(IProgressMonitor monitor) {									
		
							monitor.beginTask("Importing Cell Notes", IProgressMonitor.UNKNOWN);
							
							try {
								
								//get session
								ServerSession serverSession = SecurityManager.getSession(url);
								
								//create request
								PafSimpleCellNoteImportRequest request = new PafSimpleCellNoteImportRequest();
		
								//set parms
								request.setClientId(serverSession.getClientId());
								
								//
								if ( serverSession.getSecurityToken() != null ) {
								
									request.setSessionToken(serverSession.getSecurityToken());
									
								}
															
								if ( simpleCellNotesToImport != null && simpleCellNotesToImport.length > 0) {
	
									request.getSimpleCellNotes().addAll(Arrays.asList(simpleCellNotesToImport));
									
								}
								
								PafService service = WebServicesUtil.getPafService(url);
								if( service != null ) {
									//try to import cell notes
									PafSimpleCellNoteImportResponse importResponse = service.importSimpleCellNotes(request);
														
									//if not success, throw exception
									if ( ! importResponse.isSuccess()) {
										throw new Exception();
									}
								}
							} catch (Exception e) {
								
								//display error if an exception was thrown
								Display.getDefault().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										MessageDialog.openError(
						    					window.getShell(),
						    					"Error!",
						    					"There was a problem importing the cell notes.  Please check the log file.");
									}
								});			
									
								//return as cancel, message was already displayed 
								return Status.CANCEL_STATUS;
								
							} finally{
								
								monitor.done();
								
							}
							
							//so far so good, return ok
							return Status.OK_STATUS;
							
							
						}
						
					};
					
					//set as user job
					importCellNoteJob.setUser(true);
					
					//set job priority
					importCellNoteJob.setPriority(Job.LONG);
		
					//add job change listener
					importCellNoteJob.addJobChangeListener(new JobChangeAdapter() {
				
							@Override
							public void done(IJobChangeEvent event) {
								
								//if job complted successfully, dispaly message
								if (event.getResult().isOK()) {
									
									// tell display to run job next time possible
									Display.getDefault().asyncExec(new Runnable() {
										
										public void run() {
											
							    			MessageDialog.openInformation(
							    					window.getShell(),
							    					"Process Complete",
							    					"Successfully imported cell notes.");
										}
									});
								}
							}
		
						});
				
					//schedule job
					importCellNoteJob.schedule();
					
					
				} else {
					
					MessageDialog.openInformation(
	    					window.getShell(),
	    					Constants.DIALOG_INFO_HEADING,
	    					"A user must be logged in to import cell notes.  Please log in and try again.");
					
				}
								
			}
			
			
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart arg0, ISelection incoming) {

		// check selection to seei fstructured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			selection  = (StructuredSelection) incoming;

		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		logger.debug("Unregistering ImportCellNotesAction from Window Selection Service");
		
		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);
		
	}

}

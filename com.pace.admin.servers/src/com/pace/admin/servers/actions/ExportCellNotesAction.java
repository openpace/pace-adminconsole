/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.enums.ImportExportFileType;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.ImportExportUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.dialogs.ExportCellNotesDialog;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.security.ServerSession;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.CellNotesInformation;
import com.pace.server.client.PafCellNoteInformationResponse;
import com.pace.server.client.PafRequest;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSimpleCellNoteExportRequest;
import com.pace.server.client.PafSimpleCellNoteExportResponse;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.SimpleCellNote;

/**
 * Exports Cell Notes
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class ExportCellNotesAction extends Action implements ISelectionListener {
	
	private static Logger logger = Logger.getLogger(ExportCellNotesAction.class);
	
	//parent window
	private final IWorkbenchWindow window;

	//current selection
	private IStructuredSelection selection;
	
	//remember last export directory
	private static String lastCellNoteExportDir = null;

	/**
	 * Opens the export cell notes dailog and then exports the cell notes.
	 * 
	 * @param window
	 */
	public ExportCellNotesAction(IWorkbenchWindow window) {
		
		super("Export Cell Notes");

		this.window = window;
		
		logger.debug("Registering ExportCellNotesAction to Window Selection Service");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
		
	}


	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
	
		if (selection.getFirstElement() instanceof ApplicationNode) {
			//get applicaiton node
			ApplicationNode appNode = (ApplicationNode) selection.getFirstElement();
			//get server node
			ServerNode serverNode = (ServerNode)appNode.getParent();
			//get server name			final String serverName = serverNode.getName();
			String serverName = serverNode.getName();
			//get server url
			final String url = PafServerUtil.getServerWebserviceUrl(serverName);
			
			try {
				ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
			} catch (PafServerNotFound e1) {
				logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
			} catch (ServerNotRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//if authenticated, continue
			if ( SecurityManager.isAuthenticated(url) ) {
				
				//get session
				final ServerSession serverSession = SecurityManager.getSession(url);
				
				//create new paf request and set client id
				PafRequest pafRequest = new PafRequest();
				pafRequest.setClientId(serverSession.getClientId());
				
				try {
					
					//query server to get cell note info ar in this response
					PafService service = WebServicesUtil.getPafService(url);
					if( service != null ) {
						PafCellNoteInformationResponse pafCellNoteInfoResponse = service.getCellNotesInformation(pafRequest);
										
						List<CellNotesInformation> cellNoteInfoList = pafCellNoteInfoResponse.getCellNotesInformationAr();
																
						//if not null and 1st item return isn't null
						if ( cellNoteInfoList != null && cellNoteInfoList.size() > 0) {
						
	//						servers cell note info array
							CellNotesInformation[] cellNoteInfoArray = cellNoteInfoList.toArray(new CellNotesInformation[0]);
							
							//open export dialog
							ExportCellNotesDialog dialog = new ExportCellNotesDialog(window.getShell(), cellNoteInfoArray, serverName, lastCellNoteExportDir);
							
							//if user clicked ok
							if ( dialog.open() == IDialogConstants.OK_ID)  {
								
								//get cell note info to export
								final CellNotesInformation[] cellNotesInfoToExportAr = dialog.getCellNoteInfoArray();
																						
								lastCellNoteExportDir = dialog.getExportDirectory();
								
								final ImportExportFileType exportFileType = dialog.getExportFileType();
								
								//create job to export the cell notes
								Job exportCellNoteJob = new Job("Export Cell Notes") {
	
									@Override
									protected IStatus run(IProgressMonitor monitor) {									
				
										monitor.beginTask("Exporting Cell Notes", IProgressMonitor.UNKNOWN);
										
										try {
								
	//										create a request for server to get simple cell note array
											PafSimpleCellNoteExportRequest pafSimpleCellNoteRequest = new PafSimpleCellNoteExportRequest();
											pafSimpleCellNoteRequest.setClientId(serverSession.getClientId());
											pafSimpleCellNoteRequest.setSessionToken(serverSession.getSecurityToken());
											
											if ( cellNotesInfoToExportAr != null ) {
											
												pafSimpleCellNoteRequest.getCellNoteInformationAr().addAll(Arrays.asList(cellNotesInfoToExportAr));
												
											}
																					
											//call server
											PafSimpleCellNoteExportResponse pafCellNoteExportResponse = 
													WebServicesUtil.getPafService(url).getSimpleCellNotesToExport(pafSimpleCellNoteRequest);
											
											//get simple cell note array from server
											List<SimpleCellNote> simpleCellNotesToExportList = pafCellNoteExportResponse.getSimpleCellNotesToExport();
											
											if ( simpleCellNotesToExportList == null || simpleCellNotesToExportList.size() == 0 ) {
												
												return Status.OK_STATUS;
												
											}
											
											final SimpleCellNote[] simpleCellNotesToExportAr = simpleCellNotesToExportList.toArray(new SimpleCellNote[0]);
											
											//try to export the cell notes
											ImportExportUtil.exportSimpleCellNotes(simpleCellNotesToExportAr, lastCellNoteExportDir, exportFileType);
											
										} catch (Exception e) {
											
											//display error if an exception was thrown
											MessageDialog.openError(
							    					window.getShell(),
							    					"Error!",
							    					"There was a problem exporting cell notes.  Please check the log file.");
											
											//return as cancel, message was already displayed 
											return Status.CANCEL_STATUS;
											
										} finally{
											
											monitor.done();
											
										}
										
										//so far so good, return ok
										return Status.OK_STATUS;
										
										
									}
									
								};
								
								//set user job
								exportCellNoteJob.setUser(true);
								
								//set job priority
								exportCellNoteJob.setPriority(Job.LONG);
								
								//add job change listener
								exportCellNoteJob.addJobChangeListener(new JobChangeAdapter() {
							
										@Override
										public void done(IJobChangeEvent event) {
											
											//if job complted successfully, dispaly message
											if (event.getResult().isOK()) {
												
												// tell display to run job next time possible
												Display.getDefault().asyncExec(new Runnable() {
													
													public void run() {
														
										    			MessageDialog.openInformation(
										    					window.getShell(),
										    					"Process Complete",
										    					"Successfully exported cell notes.");
													}
												});
											}
										}
		
									});
							
								//schedule job
								exportCellNoteJob.schedule();
								
							}
						} else {
							
							//server didn't have cell notes to export
							MessageDialog.openInformation(
			    					window.getShell(),
			    					"No Cell Notes",
			    					"The server doesn't have any cell notes to export.");
						}
					}
				/*
				} catch (RemoteException e1) {
					
					logger.error(e1.getMessage());
				*/
				} catch (PafSoapException_Exception e1) {
					
					logger.error(e1.getMessage());
					
				} 
				
			}
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart arg0, ISelection incoming) {

		// check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			selection  = (StructuredSelection) incoming;

		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
	 */
	public void dispose() {

		logger.debug("Unregistering ExportCellNotesAction from Window Selection Service");
		
		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);
		
	}

}

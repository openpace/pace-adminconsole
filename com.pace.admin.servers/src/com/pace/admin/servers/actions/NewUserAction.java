/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.UserDialog;
import com.pace.admin.servers.enums.UserDialogActionType;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafUserDef;

public class NewUserAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger.getLogger(NewUserAction.class);

	private IWorkbenchWindow window;
	
	private NativeUsersNode nativeUsersNode;
	
	public NewUserAction(String text, IWorkbenchWindow window) {
		super(text);

		this.window = window;
		
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_NEW_USER);

		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_NEW_USER);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin.getImageDescriptor("/icons/user.png"));

		// set tool tip text
		setToolTipText("Create new user");
		
		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	public void run() {

		String serverName = nativeUsersNode.getServer().getName();
		
		UserDialog userDialog = new UserDialog(window.getShell(), UserDialogActionType.NewUser, serverName, false);
		
		int rc = userDialog.open();
		
		if ( rc == 0 ) {
			
			PafUserDef newUserDef = userDialog.getPafUserDef();
									
			boolean userCreated = SecurityManager.createDBUserOnServer(serverName, newUserDef);
			
			if ( userCreated ) {
											
		        BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {

					public void run() {
					
//						create db users model
						ServerView.createDBUsersModel(nativeUsersNode.getDBUsersNode(), true);
				        
					}
					
		        });
				
			}
			
		}
		
	}

	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		//check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			// if server node but not folder node
			if (currentSelection.getFirstElement() instanceof NativeUsersNode) {
				
				nativeUsersNode = (NativeUsersNode) currentSelection.getFirstElement();

			} else if (currentSelection.getFirstElement() instanceof DBUserNode) {
				
				nativeUsersNode = (NativeUsersNode) ((DBUserNode) currentSelection.getFirstElement()).getParent();
				
			}
		}	
		
	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.dialogs.ClearMemberTagDataDialog;
import com.pace.admin.servers.dialogs.ImportExportMemberTagDataDialog;
import com.pace.admin.servers.dialogs.MigrateMemberTagDataDialog;
import com.pace.admin.servers.dialogs.input.ImportExportMemberTagDataInput;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.MemberTagInformation;
import com.pace.server.client.PafSoapException_Exception;

public class MigrateMemberTagDataAction extends Action implements ISelectionListener {

	//logger
		private static Logger logger = Logger.getLogger(ClearMemberTagDataAction.class);
		
		//parent window
		private final IWorkbenchWindow window;

		//current selection
		private IStructuredSelection selection;

		/**
		 * Opens the export member tag data dailog and then exports the cell notes.
		 * 
		 * @param window
		 */
		public MigrateMemberTagDataAction(IWorkbenchWindow window) {
				
			super("Migrate Member Tag Data");

			this.window = window;		
			
			logger.debug("Registering MigrateMemberTagDataAction to Window Selection Service");
			
			// reg with window selection service
			window.getSelectionService().addSelectionListener(this);
			
		}
		
		@Override
		public void run() {
			
			if (selection.getFirstElement() instanceof ApplicationNode) {
				//get applicaiton node
				ApplicationNode appNode = (ApplicationNode) selection.getFirstElement();
				//get server node
				ServerNode serverNode = (ServerNode)appNode.getParent();
				//get server name			final String serverName = serverNode.getName();
				String serverName = serverNode.getName();
				//get server url
				final String url = PafServerUtil.getServerWebserviceUrl(serverName);
				
				try {
					ServerView.authWithLoginDialogIfNotAuthed(PafServerUtil.getServer(serverName));
				} catch (PafServerNotFound e1) {
					logger.error("Server '" + serverName + "' is not defined in the " + Constants.SERVERS_FILE);
				} catch (ServerNotRunningException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				//if authenticated, continue
				if ( SecurityManager.isAuthenticated(url) ) {
					
					try {
											
						MemberTagInformation[] memberTagInfoAr = SecurityManager.getMemberTagInformation(url);
					
						ImportExportMemberTagDataInput dialogInput = new ImportExportMemberTagDataInput(url);
						
						MigrateMemberTagDataDialog dialog = new MigrateMemberTagDataDialog(window.getShell(),dialogInput, serverName);
						
						dialog.open();
						
					}catch (PafSoapException_Exception e1) {
						
						logger.error(e1.getMessage());
						
					} catch (RemoteException e1) {
						
						logger.error(e1.getMessage());
						
					} 
					
					
				}
				
			}

		}
		
		
		/* (non-Javadoc)
		 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
		 */
		public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

			//check selection to see if structured
			if (incoming instanceof StructuredSelection) {

				// get current selection
				selection  = (StructuredSelection) incoming;

			}

		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.ui.actions.ActionFactory$IWorkbenchAction#dispose()
		 */
		public void dispose() {

			logger.debug("Unregistering MemberTagData from Window Selection Service");
			
			// remove from selection service
			window.getSelectionService().removeSelectionListener(this);
			
		}
}

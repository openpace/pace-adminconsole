/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.Ostermiller.util.Base64;
import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.ImportExportUsersDialog;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.server.client.PafUserDef;

/**
 * Implements importing users from a CSV file into a server
 * @author fskrgic
 *
 */
public class ExportUsersAction extends Action implements ISelectionListener {
	
	private static final Logger logger = Logger.getLogger(ExportCellNotesAction.class);

	//Selected node
	private NativeUsersNode nativeDbUsersNode;
	
	//Corresponding window
	private IWorkbenchWindow window;
	
	/**
	 * The constructor
	 * @param text
	 * @param window
	 */
	public ExportUsersAction(String text, IWorkbenchWindow window){
		
		super(text);
		
		// Set the window
		this.window = window;
		
		// Register window with selection service
		window.getSelectionService().addSelectionListener(this);
		
		// Set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin.getImageDescriptor("/icons/user.png"));

		// Set tool tip text
		setToolTipText("Export Native Users");
		
		// The ID is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_EXPORT_USERS);

		// Associate the action with a pre-defined command, to allow key bindings.
		//setActionDefinitionId(ICommandIds.CMD_EXPORT_USERS);
		
	}
	/**
	 * Implementing ISelectionListener interface
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof StructuredSelection) {

			// Get current selection
			StructuredSelection currentSelection = (StructuredSelection) selection;

			// Get the selected node
			if (currentSelection.getFirstElement() instanceof NativeUsersNode) {

				nativeDbUsersNode = (NativeUsersNode) currentSelection.getFirstElement();

			}
		}
	}
	
	/**
	 * The action's run() method
	 */
	public void run(){
		
		// Set the shell
		Shell shell = window.getShell();
		
		// Create a new file dialog
		FileDialog fd =  new FileDialog(shell, SWT.SAVE);
		
		// Set the title
		fd.setText("Export to a CSV File");
		
		// Set the default directory
		fd.setFilterPath("C:/");
		
		// Set the filtered file extension
        String[] filterExt = { "*." + Constants.CSV_EXT};
        
        fd.setFilterExtensions(filterExt);
        
        // Get the selected path string
        String fullFileName = fd.open();
        
        // Fix the file extension
        if(!fullFileName.endsWith(Constants.CSV_EXT)){
        	
        	fullFileName += "." + Constants.CSV_EXT;
        	
        }
               
        // Get the server name
        String serverName = nativeDbUsersNode.getServer().getName();

        // Get the server URL
        String url = PafServerUtil.getServerWebserviceUrl(serverName);

        // Get the users from the server
        PafUserDef [] users = SecurityManager.getDbUsersFromServer(url);
        
        // Create an import/export dialog
        ImportExportUsersDialog importExportUsersDialog = new ImportExportUsersDialog(shell, users, null, false);
        
        importExportUsersDialog.setBlockOnOpen(true);
        
        // Export the selected users to a file
        if( importExportUsersDialog.open() == IDialogConstants.OK_ID ){
        	
        	 // Declare a file writer
            FileWriter output = null;
            
            // Try to open the file
            try{
            	
            	output = new FileWriter(new File(fullFileName));
            	
            }
            catch (Exception e){
            	
            	logger.error(e.getMessage());
            	
            }
        	
        	// Try to output headers
        	try{
        	
        		output.write("UserName,Password,Email,FirstName,LastName,Administrator,ChangePassword\n");
        		
        	}
        	catch(Exception e){
        		
        		logger.error(e.getMessage());
        		
        	}
        	
        	// Get the checked indeces
        	List<Integer> checkedIndeces = importExportUsersDialog.getCheckedIndeces();
        	
        	// Put the information about each selected user into a string
        	for(Integer index : checkedIndeces){
        		
        		// Add username to line
    			String line = users[index].getUserName()+",";
    			
    			// Add user password to line and encode it using Base64 if necessary
    			String password = users[index].getPassword();  
    			
    			if(password == null || password.equals("") ){
    				
    				line += ",";
    				
    			}
    			else{
    				
    				line += Base64.encode(password) + ",";
    				
    			}
    			
    			// Add user's email to line
    			line += (users[index].getEmail() != null? users[index].getEmail() : "") + ",";
    			
    			// Add user's first name to line
    			line += (users[index].getFirstName() != null? users[index].getFirstName() : "") + ",";
    			
    			// Add user's last name to line
    			line += (users[index].getLastName() != null? users[index].getLastName() : "") + ",";
    			
    			// Add whether user is an admin
    			line += (users[index].isAdmin()?"T":"F")+",";
    			
    			// Add whether user needs to change his password
    			line += (users[index].isChangePassword()?"T":"F")+"\n";
    			
    			// Try to write the line to a file
    			try{
    				
    				output.write(line);
    				
    			}
    			catch(Exception e){
    				
    				logger.error(e.getMessage());
    				
    			}
    			    			    			    			
        	}
        	
        	//Try to close the writer
            try{

            	output.close();

            }
            catch(Exception e){
            	
            	logger.error(e.getMessage());
            	
            }
            
            File fullFile = new File(fullFileName);
			
			if ( fullFile.exists() && fullFile.isFile()) { 
			
				MessageDialog.openInformation(window.getShell(), Constants.DIALOG_INFO_HEADING, "Users were successfully exported to file: " + fullFileName);
				
			} else {
				
				MessageDialog.openError(window.getShell(), Constants.DIALOG_ERROR_HEADING, "Users could not be exported.");
				
			}
        }
                
	}
	
	/**
	 * Cleans up
	 *
	 */
	public void dispose() {

		// Remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}
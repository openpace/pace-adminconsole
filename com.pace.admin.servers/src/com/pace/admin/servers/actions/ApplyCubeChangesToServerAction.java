/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.MessageConsole;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.jobs.ApplyCubeChangesJob;
import com.pace.admin.global.util.ConsoleUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.base.ui.PafServer;

/**
 * Starts/Restarts an application on a server
 * 
 * @author JMilliron
 *
 */
public class ApplyCubeChangesToServerAction extends Action implements ISelectionListener {
	
	private static Logger logger = Logger.getLogger(ApplyCubeChangesToServerAction.class);

	private IStructuredSelection selection;
	
	private IWorkbenchWindow window;	

	public ApplyCubeChangesToServerAction(IWorkbenchWindow window) {
		super(Constants.START_OR_RESTART_APPLICATION);
		
		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_DOWNLOAD_CUBE_CHANGES);
	
		setToolTipText(Constants.START_OR_RESTART_APPLICATION);
	
		// add to selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	public void dispose() {

		window.getSelectionService().removeSelectionListener(this);
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		if (selection instanceof IStructuredSelection) {
					
			this.selection = (IStructuredSelection) selection;
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		
		logger.info(Constants.START_OR_RESTART_APPLICATION);
		
		if ( selection instanceof IStructuredSelection ) {
			
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			
			if (structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof ApplicationNode ) {
		
				ApplicationNode appNode = (ApplicationNode) structuredSelection.getFirstElement();

				PafServer server = appNode.getServer();
				
				if (!GUIUtil
						.askUserAQuestion(Constants.START_OR_RESTART_APPLICATION + " on server '" + server.getName() +"'?")){
					return;
				}
				
				MessageConsole msgConsole = ConsoleUtil.findConsole(server.getName());
				if( msgConsole != null ) {
					msgConsole.activate();
				}

				(new ApplyCubeChangesJob(server, appNode.getName())).schedule();
			
			}
		}
	}
}

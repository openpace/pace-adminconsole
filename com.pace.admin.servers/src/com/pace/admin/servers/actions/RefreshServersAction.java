/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.views.ServerView;

public class RefreshServersAction extends Action {

	// logger
	private static Logger logger = Logger.getLogger(RefreshServersAction.class);

	// holds current viewer
	private TreeViewer viewer;
	
	private boolean silentMode;

	public RefreshServersAction(String text, TreeViewer viewer) {
		this(text, viewer, false);
	}
	
	public RefreshServersAction(String text, TreeViewer viewer, boolean silentMode) {
		super(text);

		// set vars
		this.viewer = viewer;
		
		this.silentMode = silentMode;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_REFRESH_FROM_SERVER);

		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_REFRESH_FROM_SERVER);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/refresh.gif"));

		// set tool tip text
		setToolTipText("Refresh Servers");
	}

	public void run() {

		logger.info("Refeshing Servers");
			
		if ( silentMode ) {
			
			Display.getDefault().asyncExec(getRefreshServerRunnable());
			
		} else {
			
			BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), getRefreshServerRunnable());
			
		}
		
	}
	
	private Runnable getRefreshServerRunnable() {
		
		return  new Runnable() {

			public void run() {

//				 if viewer exist
				if (viewer != null) {
										
					logger.info("RefreshServerAction.run(): Display should be updated.");

					//create new input and then expand
					viewer.setInput(ServerView.refreshServerModel(true));
					viewer.expandToLevel(Constants.LEVEL_SERVER_CHILDREN_NODE);

				}
				
			}
		
		};
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.ICommandIds;
import com.pace.admin.servers.dialogs.UserDialog;
import com.pace.admin.servers.enums.UserDialogActionType;
import com.pace.admin.servers.exceptions.UserNotFoundException;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.IDBUsersNode;
import com.pace.admin.servers.nodes.LDAPUserNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.server.client.PafUserDef;

public class EditUserAction extends Action implements ISelectionListener {

	// logger
	private static Logger logger = Logger.getLogger(EditUserAction.class);

	// holds console
	//private MessageConsole myConsole = ConsoleUtil.findConsole(Constants.CONSOLE_NAME);

	// allows user to write to console
	//private MessageConsoleStream msgStream = myConsole.newMessageStream();

	// workbench window
	private IWorkbenchWindow window;

	// current db user node
	private TreeNode selectedUserNode;
	
	/**
	 * Constructor.  Sets up the action with ID and text.  Registers with selection service.
	 * 
	 * @param text
	 * @param window
	 */
	public EditUserAction(String text, IWorkbenchWindow window) {
		super(text);

		// set window
		this.window = window;

		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_EDIT_USER);

		// Associate the action with a pre-defined command, to allow key
		// bindings.
		//setActionDefinitionId(ICommandIds.CMD_EDIT_USER);

		// set image
		setImageDescriptor(com.pace.admin.servers.ServersPlugin
				.getImageDescriptor("/icons/user.png"));

		// set tool tip text
		setToolTipText("Edit user");

		// reg with window selection service
		window.getSelectionService().addSelectionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {

		//get server name
		String serverName = selectedUserNode.getServer().getName();

		//get url from server name
		String url = PafServerUtil.getServerWebserviceUrl(serverName);

		//if url is not null
		if (url != null) {
			
			boolean isReadOnlyUser = false;
			
			String domainName = null;
			
			String userName = null;
			
			if ( selectedUserNode instanceof DBUserNode ) {
				
				userName = selectedUserNode.getName();
				
				domainName  = selectedUserNode.getParent().getName();
								
			} else if ( selectedUserNode instanceof LDAPUserNode ) {
				
				LDAPUserNode ldapUserNode = (LDAPUserNode) selectedUserNode;
				
				userName = ldapUserNode.getPaceUser().getUserName();
				
				domainName  = ldapUserNode.getParent().getParent().getName();

				isReadOnlyUser = true;
				
			} 	
			
			logger.debug("Trying to display user: " + selectedUserNode.getName());

			//get current user from security manager
			PafUserDef pafUserDef = null;
			try {
				
				pafUserDef = SecurityManager.getDbUserFromServer(url,
						userName, domainName);
				
			} catch (UserNotFoundException e) {
				
				if ( selectedUserNode instanceof IDBUsersNode ) {
					
					IDBUsersNode iDBUsersNode = (IDBUsersNode) selectedUserNode;
					
					ServerView.createDBUsersModel(iDBUsersNode.getDBUsersNode(), true);
					
				}
				
				//if user was not found, open error then update tree
				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "User '" + selectedUserNode.getName() + "' no longer exists.", MessageDialog.ERROR);
			
				return;
			}

			//if user exist
			if (pafUserDef != null) {

				//create user dialog
				UserDialog userDialog = new UserDialog(window.getShell(),
							pafUserDef, UserDialogActionType.EditUser, serverName, isReadOnlyUser);

				//if user cliked ok, not cancel
				if (userDialog.open() == IDialogConstants.OK_ID && ! isReadOnlyUser ) {

					//get user from dialog
					PafUserDef newUserDef = userDialog.getPafUserDef();

					//try to update user
					boolean userUpdated = SecurityManager.updateDBUserOnServer(
							serverName, newUserDef);

					//if user was updated, do something, as of now...do nothing
					if (userUpdated) {

						logger.info("User " + newUserDef.getUserName() + " was successfully updated");
						
					} else {
						
						logger.info("User " + newUserDef.getUserName() + " was NOT successfully updated");
						
					}

				}

			}
								
			
		}

	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {

		// check selection to see if structured
		if (incoming instanceof StructuredSelection) {

			// get current selection
			StructuredSelection currentSelection = (StructuredSelection) incoming;

			if (currentSelection.getFirstElement() instanceof DBUserNode) {

				selectedUserNode = (DBUserNode) currentSelection.getFirstElement();
				
				this.setText("Edit User " + selectedUserNode.getName());

			} else if ( currentSelection.getFirstElement() instanceof LDAPUserNode ) {
				
				selectedUserNode = (LDAPUserNode) currentSelection.getFirstElement();;
				
			}
		}

	}

	/**
	 * Called when disposed
	 * 
	 */
	public void dispose() {

		// remove from selection service
		window.getSelectionService().removeSelectionListener(this);

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers;

/**
 * Interface defining the application's command IDs.
 * Key bindings can be defined for specific commands.
 * To associate an action with a command, use IAction.setActionDefinitionId(commandId).
 *
 * @see org.eclipse.jface.action.IAction#setActionDefinitionId(String)
 */
public interface ICommandIds {

    public static final String CMD_DEPLOY_TO_SERVER = "com.pace.admin.servers.actions.serverDeployAction";
    public static final String CMD_REFRESH_FROM_SERVER = "com.pace.admin.servers.actions.serverRefreshAction";
    public static final String CMD_START_SERVER = "com.pace.admin.servers.actions.startServerAction";
    public static final String CMD_STOP_SERVER = "com.pace.admin.servers.actions.stopServerAction";
    public static final String CMD_TERM_SERVER = "com.pace.admin.servers.actions.terminateServerAction";
    public static final String CMD_NEW_USER = "com.pace.admin.servers.actions.newUserAction";
    public static final String CMD_EDIT_USER = "com.pace.admin.servers.actions.editUserAction";
    public static final String CMD_DELETE_USER = "com.pace.admin.servers.actions.deleteUserAction";
    public static final String CMD_RESET_USER_PASSWORD = "com.pace.admin.servers.actions.resetUserPasswordAction";
    public static final String CMD_IMPORT_ATTRIBUTES_DIALOG = "com.pace.admin.servers.actions.OpenImportAttributeDialogAction";
    public static final String CMD_CLEAR_IMPORT_ATTRIBUTES_DIALOG = "com.pace.admin.servers.actions.ClearImportedAttributesAction";
    public static final String CMD_IMPORT_USERS = "com.pace.admin.servers.actions.ImportUsersAction";
    public static final String CMD_EXPORT_USERS = "com.pace.admin.servers.actions.ExportUsersAction";
    public static final String CMD_LOGOFF_SERVER = "com.pace.admin.servers.actions.LogoffServerAction";
    public static final String CMD_MODIFY_SERVER = "com.pace.admin.servers.actions.modifyServerAction";
    public static final String CMD_CLONE_SERVER = "com.pace.admin.servers.actions.cloneServerAction";
    public static final String CMD_CREATE_SERVER = "com.pace.admin.servers.actions.createServerAction";
    public static final String CMD_DELETE_SERVER = "com.pace.admin.servers.actions.deleteServerAction";
    public static final String CMD_LDAP_GROUPS_DIALOG = "com.pace.admin.servers.actions.OpenLDAPGroupSelectionDialogAction";
    public static final String CMD_UPLOAD_CONFIG_CHANGES = "com.pace.admin.servers.actions.ApplyConfigChangesToServerAction";
    public static final String CMD_DOWNLOAD_CUBE_CHANGES = "com.pace.admin.servers.actions.ApplyCubeChangesToServerAction";
    public static final String CMD_REFRESH_DIMENSION_CACHE = "com.pace.admin.servers.actions.RefreshDimensionCacheAction";
    public static final String CMD_RENAME_SERVER = "com.pace.admin.servers.actions.RenameServerAction";
    public static final String CMD_RESTART_SERVER = "com.pace.admin.servers.actions.RestartServerAction";
    public static final String CMD_SET_DEFAULT_SERVER = "com.pace.admin.servers.actions.setDefaultServerAction";
    public static final String CMD_MODIFY_APP_SETTINGS = "com.pace.admin.servers.actions.ModifyAppSettingsAction";
    public static final String CMD_MODIFY_USERS = "com.pace.admin.servers.actions.ModifyUsersAction";
    public static final String CMD_RENAME_SERVER_GROUP = "com.pace.admin.servers.actions.RenameServerGroupAction";
    public static final String CMD_DISABLE_ENABLE_SERVER = "com.pace.admin.servers.actions.DisableEnableServerAction";
    public static final String CMD_EDIT_LDAP_ADMIN_USER = "com.pace.admin.servers.actions.EditLDAPUserAction";
}

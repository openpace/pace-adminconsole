/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.commands;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.servers.dialogs.GenerateSupportRequestDialog;

public class GenerateSupportRequestCommand extends AbstractHandler {

	private static final Logger logger = Logger.getLogger(GenerateSupportRequestCommand.class);
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		try {
		
			logger.info("Opening Issue Resolution dialog");
			
			GenerateSupportRequestDialog dialog = new GenerateSupportRequestDialog(window.getWorkbench().getActiveWorkbenchWindow().getShell(), "Generate Issue Resolution Files");
			dialog.open();
		
		} catch(Exception e){
			logger.error(e.getMessage());
			throw new ExecutionException(e.getMessage(), e);
		}
		return null;
	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.servers.security;

import com.pace.admin.global.util.DomainUtil;
import com.pace.server.client.AuthMode;
import com.pace.server.client.PafServerAck;

/**
 * @author jmilliron
 *
 */
public class ServerSession {

	public ServerSession() {
	
	}
	
	private String userName;
	private boolean loginOverride;
	private transient String password;
	private transient String clientId;
	private transient boolean isAdmin;
	private transient PafServerAck pafServerAck;
	private transient String domainName;
	private transient String securityToken;
	private transient boolean securityTokenValid;
	
	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Overrid toString
	 */
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer();
		
		strBuff.append("Username: " + userName + "\n");
		strBuff.append("Password: " + password + "\n");
		strBuff.append("Client Id: " + clientId + "\n");
		strBuff.append("Security Token: " + securityToken + "\n");
		strBuff.append("Is Security Token Valid: " + securityTokenValid + "\n");
		strBuff.append("Domain: " + domainName + "\n");
		
		return strBuff.toString();
	}
	/**
	 * @return the isAdmin
	 */
	public boolean isAdmin() {
		return isAdmin;
	}
	/**
	 * @param isAdmin the isAdmin to set
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	/**
	 * @return The PafServerAck
	 */
	public PafServerAck getPafServerAck() {
		return pafServerAck;
	}
	
	/**
	 * @param pafServerAck The PafServerAck.
	 */
	public void setPafServerAck(PafServerAck pafServerAck) {
		this.pafServerAck = pafServerAck;
	}
	
	/**
	 * 
	 * Checks if auth mode is native or null.  
	 *
	 * @return true if auth mode is native
	 */
	public boolean isNativeAuthMode() {
		
		boolean isNativeMode = false;
		
		if ( pafServerAck == null || pafServerAck.getAuthMode() == null || pafServerAck.getAuthMode().equals(AuthMode.NATIVE_MODE)) {
			
			isNativeMode = true;
			
		}
		
		return isNativeMode;
		
	}
	
	/**
	 * 
	 * 
	 *
	 * @return AuthMode
	 */
	public AuthMode getAuthMode() {
		
		AuthMode authMode = null;
		
		if ( pafServerAck != null ) {
			
			authMode = pafServerAck.getAuthMode();
			
		}
		
		return authMode;
		
	}
	
	/**
	 * 
	 * Checks if auth mode is mixed
	 *
	 * @return true if auth mode is mixed
	 */
	public boolean isMixedAuthMode() {
		
		boolean isMixedMode = false;
		
		if ( pafServerAck != null && pafServerAck.getAuthMode() != null && pafServerAck.getAuthMode() .equals(AuthMode.MIXED_MODE)) {
			
			isMixedMode = true;
			
		}
		
		return isMixedMode;
		
	}
	/**
	 * @return the domain
	 */
	public String getDomainName() {
		
		if ( domainName == null ) {
		
			domainName = DomainUtil.getDomainName();
			
		}
		
		return domainName;
	}
	/**
	 * @param domain the domain to set
	 */
	public void setDomainName(String domain) {
		this.domainName = domain;
	}
	/**
	 * @return the securityToken
	 */
	public String getSecurityToken() {
		return securityToken;
	}
	/**
	 * @param securityToken the securityToken to set
	 */
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	/**
	 * @return the securityTokenValid
	 */
	public boolean isSecurityTokenValid() {
		return securityTokenValid;
	}
	/**
	 * @param securityTokenValid the securityTokenValid to set
	 */
	public void setSecurityTokenValid(boolean securityTokenValid) {
		this.securityTokenValid = securityTokenValid;
	}
	/**
	 * @return the loginOverride
	 */
	public boolean isLoginOverride() {
		return loginOverride;
	}
	/**
	 * @param loginOverride the loginOverride to set
	 */
	public void setLoginOverride(boolean loginOverride) {
		this.loginOverride = loginOverride;
	}
}

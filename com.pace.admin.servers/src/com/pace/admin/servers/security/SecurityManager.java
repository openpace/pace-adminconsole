/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.servers.security;

import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.model.MemberTagInfo;
import com.pace.admin.global.security.LinuxServerLogin;
import com.pace.admin.global.security.PaceUser;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.TreeUncompressUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.global.webservices.DomainFilter;
import com.pace.admin.servers.actions.RefreshServersAction;
import com.pace.admin.servers.dialogs.LoginDialog;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.exceptions.UserNotFoundException;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.utility.AESEncryptionUtil;
import com.pace.server.client.Application;
import com.pace.server.client.MemberTagInformation;
import com.pace.server.client.PafAuthRequest;
import com.pace.server.client.PafAuthResponse;
import com.pace.server.client.PafClientSecurityPasswordResetResponse;
import com.pace.server.client.PafClientSecurityRequest;
import com.pace.server.client.PafClientSecurityResponse;
import com.pace.server.client.PafDimSpec;
import com.pace.server.client.PafFilteredMbrTagRequest;
import com.pace.server.client.PafGetMemberTagInfoResponse;
import com.pace.server.client.PafGetPaceGroupsRequest;
import com.pace.server.client.PafGetPaceGroupsResponse;
import com.pace.server.client.PafGroupSecurityRequest;
import com.pace.server.client.PafGroupSecurityResponse;
import com.pace.server.client.PafMbrTagFilter;
import com.pace.server.client.PafMdbPropsRequest;
import com.pace.server.client.PafMdbPropsResponse;
import com.pace.server.client.PafNotAbletoGetLDAPContext_Exception;
import com.pace.server.client.PafNotAuthenticatedSoapException_Exception;
import com.pace.server.client.PafNotAuthorizedSoapException_Exception;
import com.pace.server.client.PafRequest;
import com.pace.server.client.PafSecurityDomainGroups;
import com.pace.server.client.PafSecurityDomainUserNames;
import com.pace.server.client.PafSecurityGroup;
import com.pace.server.client.PafSecurityToken;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSetPaceGroupsRequest;
import com.pace.server.client.PafSetPaceGroupsResponse;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.server.client.PafSoapException_Exception;
import com.pace.server.client.PafSuccessResponse;
import com.pace.server.client.PafTreeRequest;
import com.pace.server.client.PafTreeResponse;
import com.pace.server.client.PafTreesRequest;
import com.pace.server.client.PafTreesResponse;
import com.pace.server.client.PafUserDef;
import com.pace.server.client.PafUserNamesSecurityGroup;
import com.pace.server.client.PafUserNamesforSecurityGroupsRequest;
import com.pace.server.client.PafUserNamesforSecurityGroupsResponse;
import com.pace.server.client.PafVerifyUsersRequest;
import com.pace.server.client.PafVerifyUsersResponse;
import com.pace.server.client.SecurityGroup;

/**
 * @author jmilliron
 * 
 */
public class SecurityManager {

	private static Logger logger = Logger.getLogger(SecurityManager.class);

	private static Map<String, ServerSession> sessionMap = new TreeMap<String, ServerSession>(String.CASE_INSENSITIVE_ORDER);
	
	//key: url; value (key: domain name; value ( key: security group; value: set of pace users ))
	private static Map<String, Map<String, Map<String, Set<PaceUser>>>> cachedUserNameBySecurityGroupByDomainByUrlMap = new HashMap<String, Map<String, Map<String, Set<PaceUser>>>>();
		
	private final static String FULL_SECURITY_SESSION_FILE = Constants.ADMIN_CONSOLE_LOCAL_APPDATA_CONF_DIRECTORY
			+ Constants.SECURITY_SESSION_FILE;
	
	//a map used to store Linux Server user login info: key: server; value (key: serverHost: value(LinuxUserLogin))
	private static Map<String, LinuxServerLogin> cachedLinuxServerLogin = new HashMap<String, LinuxServerLogin>();
	
	public static void addLinuxUser(LinuxServerLogin linuxServer) {
		cachedLinuxServerLogin.put( linuxServer.getServerHost(), linuxServer);
	}
	
	public static LinuxServerLogin getLinuxServerLogin(String linuxServer) {
		return cachedLinuxServerLogin.get(linuxServer);
	}
	
	public static LinuxServerLogin isLinuxServerLoginInfoCached(String linuxServer) {
		return cachedLinuxServerLogin.get(linuxServer);
	}
	
	private static String previousUser;	// TTN-1718 - save previous user for login dialog
	

	/**
	 * @return the session
	 */
	public static ServerSession getSession(String url) {
		
		ServerSession serverSession = sessionMap.get(url);
		
		if ( serverSession == null ) {
			serverSession = new ServerSession();
		}
		
		return serverSession;
	}

	/**
	 * @param serverSession
	 *            the session to set
	 */
	public static void setSession(String url, ServerSession serverSession) {
		SecurityManager.sessionMap.put(url, serverSession);
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 */
	public static void writeSessions() {

		ACPafXStream.getXStream().alias("Session", ServerSession.class);

		ACPafXStream.exportObjectToXml(sessionMap, FULL_SECURITY_SESSION_FILE);

	}

	/**
	 * 
	 *	Logs off a user.  First checks to see if a session already exists per url, and
	 *  if so, it removes it.
	 *
	 * @param url Web Service URL.
	 */
	public static void removeSession(String url) {
						
		//if map contains url, remove from map
		if ( sessionMap != null && url != null && sessionMap.containsKey(url)) {
			
			ServerSession serverSession = sessionMap.get(url);
			
			//if server is running and session stuff is good, try to end planning session
			if ( ServerMonitor.getInstance().isServerRunning(url) && serverSession != null && serverSession.getClientId() != null ) {
				
				PafService service = WebServicesUtil.getPafService(url);
				
				if ( service != null ) {
					
					PafRequest endPlanningSessionRequest = new PafRequest();
					
					endPlanningSessionRequest.setClientId(serverSession.getClientId());
					
					if ( serverSession.getSecurityToken() != null ) {
						endPlanningSessionRequest.setSessionToken(serverSession.getSecurityToken());
					}
					
					try {
						service.endPlanningSession(endPlanningSessionRequest);
						
//						WebServicesUtil.refershPafServerAck(url);
						
					} catch (PafSoapException_Exception e) {
						logger.error("A PafSoapException Exception occurred while trying to logoff the server: " + e.getMessage());
					}
					
				}
				
			}
			
			sessionMap.remove(url);
			
		}
		
	}
	
	/**
	 * 
	 *	Method_description_goes_here
	 *
	 */
	public static void readSessions() {

		ClassLoader cachedClassLoader = ACPafXStream.getXStream()
				.getClassLoader();
		
		Map<String, ServerSession> urlUserNameSessionMap = null;

		try {

			ACPafXStream.getXStream().setClassLoader(
					ServerSession.class.getClassLoader());

			ACPafXStream.getXStream().alias("Session", ServerSession.class);

			urlUserNameSessionMap = (TreeMap) ACPafXStream
			.importObjectFromXml(FULL_SECURITY_SESSION_FILE);

			if (urlUserNameSessionMap != null) {
		
				logger.info("Successfully Loaded sessions");
		
			} else {
		
				logger.info("Sessions did not exist");
		
			}
			
			/*
			urlUserNameSessionMap = (TreeMap) ACPafXStream
					.importObjectFromXml(FULL_SECURITY_SESSION_FILE);

			if (urlUserNameSessionMap != null) {

				logger.info("Successfully Loaded sessions");

			} else {

				logger.info("Sessions did not exist");

			}
			*/

		} catch (PafConfigFileNotFoundException e) {

			//do nothing

		} finally {

			ACPafXStream.getXStream().setClassLoader(cachedClassLoader);

		}
		
		if ( urlUserNameSessionMap != null && sessionMap != null ) {
			
			for ( String url : urlUserNameSessionMap.keySet() ) {
				
				ServerSession currentServerSession = null;
				
				ServerSession cachedServerSession = urlUserNameSessionMap.get(url);
				
				if ( cachedServerSession != null ) {
					
					currentServerSession = getSession(url);
									
					if ( currentServerSession != null && cachedServerSession != null ) {
						
						currentServerSession.setUserName(cachedServerSession.getUserName());
						currentServerSession.setLoginOverride(cachedServerSession.isLoginOverride());
						
						setSession(url, currentServerSession);
						
					}
					
				}
				
			}
			
			
		}

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param shell
	 * @param pafServerName
	 * @return
	 * @throws ServerNotRunningException
	 */
	public static LoginDialog getLoginDialog(Shell shell, String pafServerName) throws ServerNotRunningException {

		readSessions();

		String url = PafServerUtil.getServerWebserviceUrl(pafServerName);

		LoginDialog loginDialog = null;

		if (url != null ) {

			ServerSession serverSession = getSession(url);
			
			if ( serverSession.getPafServerAck() == null ) {
				
				//get server ack from web service
				PafServerAck serverAck = WebServicesUtil.getPafServerAck(url);
				
				if ( serverAck != null ) {
					
					serverSession.setPafServerAck(serverAck);
					
				}
				else {
					logger.info("Server " + pafServerName + " is not running.");
					
					throw new ServerNotRunningException("Server '" + pafServerName + "' is not currently running.  This operation needs to authenticate to the server.  Server must be started before user can log on.");
				}
			}

			loginDialog = new LoginDialog(shell, pafServerName, serverSession);
			
		} 

		return loginDialog;
	}

	/**
	 * 
	 *	Have we already authenciated to the url
	 *
	 * @param url
	 * @return
	 */
	public static boolean isAuthenticated(String url) {

		boolean isAuthed = false;

		if ( url != null ) {
		
			ServerSession serverSession = sessionMap.get(url);
	
			//if webservice is running and server session is not null
			if (ServerMonitor.getInstance().isServerRunning(url ) && serverSession != null) {
	
				if (serverSession.getClientId() != null
						&& serverSession.getSecurityToken() != null) {
	
					isAuthed = serverSession.isSecurityTokenValid();
	
				}
	
			} else {
				//if session map contains key, remove
				if ( sessionMap.containsKey(url) ) {
					
					sessionMap.remove(url);
					
				}
				
			}
			
		}

		return isAuthed;

	}

	/**
	 * 
	 *	Have we already authenciated to the url
	 *
	 * @param url
	 * @return
	 */
	public static boolean testIsAuthenticated(String url) {

		boolean isAuthed = false;

		if ( url != null ) {
		
			ServerSession serverSession = sessionMap.get(url);
	
			//if webservice is running and server session is not null
			if (WebServicesUtil.testServerConnectionWithUrl(url ) && serverSession != null) {
	
				if (serverSession.getClientId() != null
						&& serverSession.getSecurityToken() != null) {
	
					isAuthed = serverSession.isSecurityTokenValid();
	
				}
	
			} else {
				//if session map contains key, remove
				if ( sessionMap.containsKey(url) ) {
					
					sessionMap.remove(url);
					
				}
				
			}
			
		}

		return isAuthed;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @return
	 */
	public static boolean tryToReAuthenticate(String url) {

		boolean isAuthed = false;

		//if url is not null
		if (url != null) {

			//get current session from map using url
			ServerSession session = sessionMap.get(url);
			
			//get refresh server ack
			WebServicesUtil.getPafServerAck(url);
			
			//if session exist and username is not null, try to authenticate
			if (session != null && session.getUserName() != null) {

				//try to authenticate with session information
				try {
					isAuthed = authenticate(url, session.getUserName(),
							session.getPassword());
				} catch (Exception e) {
					//do nothing
				}

			}
			
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			
			//if still not authenticated
			if ( ! isAuthed ) {
				
				//get login dialog
				LoginDialog loginDialog;
				
				try {
					loginDialog = getLoginDialog(window.getShell(), PafServerUtil.getServerNameFromUrl(url));
					
					loginDialog.setRefreshServerTree(true);
					
					//if user clicked ok
					if ( loginDialog.open() == IDialogConstants.OK_ID ) {
					
						//if successful
						if ( isAuthenticated(url) ) {
							
							isAuthed = isAuthenticated(url);
							
						} else {
							
							RefreshServersAction refreshServerAction = new RefreshServersAction("", ServerView.getViewer());
							refreshServerAction.run();
							
						}
						
					}
					
				} catch (ServerNotRunningException e) {
					//do nothing									
				}
								
			}
		}

		return isAuthed;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @param userName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static boolean testAuthenticate(String url, String userName,
			String password) throws Exception {
	
		PafServerAck pafServerAck = WebServicesUtil.testPafServerAck(url, false);

		if (pafServerAck != null) {

			PafService service = WebServicesUtil.testPafService(url);

			PafAuthRequest pafAuthRequest = new PafAuthRequest();

			String iV = AESEncryptionUtil.generateIV();
			
			pafAuthRequest.setClientId(pafServerAck.getClientId());
			
			//encrypt username and password
			pafAuthRequest.setUsername(AESEncryptionUtil.encrypt(userName, iV));
			
			if ( password != null ) {
				
				pafAuthRequest.setPassword(AESEncryptionUtil.encrypt(password, iV));
				
			}
			
			pafAuthRequest.setIV(iV);
			
			String domainName = null;
			@SuppressWarnings("rawtypes")
			Class ntSystem = null;
			Class noparams[] = {};
			Object ntSystemInstance = null;
			Object methReturn = null;
			
			
			try {
				ntSystem = Class.forName("com.sun.security.auth.module.NTSystem");
				ntSystemInstance = ntSystem.newInstance();
				
			} catch (Exception e) {
				logger.warn("com.sun.security.auth.module.NTSystem - AD integration will not be available.");
				logger.warn(e.getMessage());
			} 
			
			
			
			
			if ( ntSystem != null ) {
				
				if ( password == null ) {
					
					//String userSid = ntSystem.getUserSID();
					Method method = ntSystem.getMethod("getUserSID", noparams);
					methReturn = method.invoke(ntSystemInstance);
					
					if ( methReturn != null ) {
					
						String userSid = methReturn.toString();
						
						if ( userSid != null ) {
						
							pafAuthRequest.setSid(AESEncryptionUtil.encrypt(userSid, iV));
							
						}
					}

				} else {

					//domainName = ntSystem.getDomain();
					Method method = ntSystem.getMethod("getDomain", noparams);
					methReturn = method.invoke(ntSystemInstance);
					
					if ( methReturn != null ) {
						
						domainName = methReturn.toString();
						
						if ( domainName != null ) {
							
							pafAuthRequest.setDomain(AESEncryptionUtil.encrypt(domainName, iV));
							
						}
						
					}
					
				}
				
			}

			PafAuthResponse authResponse = null;
			
			try {
				
				try {
	
					authResponse = service.clientAuth(pafAuthRequest);
			
				} catch (PafSoapException_Exception e) {
											
					pafServerAck = WebServicesUtil.testPafServerAck(url, true);
					
					pafAuthRequest.setClientId(pafServerAck.getClientId());
					
					authResponse = service.clientAuth(pafAuthRequest);
														
				}

			} catch (PafSoapException_Exception e) {

				logger.error("Paf Soap Exception occurred: " + e.getMessage());
				
				throw new Exception(e.getMessage());
								
			} 
			
			if ( authResponse != null ) {
			
				PafSecurityToken token = authResponse.getSecurityToken();
	
				if (token != null && token.isValid() ) {
					
					ServerSession serverSession = getSession(url);		
					
					//get old login override value
					boolean loginOverride = serverSession.isLoginOverride();
					
					//create new server session
					serverSession = new ServerSession();
					
					serverSession.setClientId(pafServerAck.getClientId());
					serverSession.setUserName(userName);
					serverSession.setPassword(password);
					serverSession.setAdmin(authResponse.isAdmin());
					serverSession.setPafServerAck(pafServerAck);
					serverSession.setDomainName(domainName);				
					serverSession.setLoginOverride(loginOverride);
					serverSession.setSecurityToken(token.getSessionToken());
					serverSession.setSecurityTokenValid(token.isValid());
					
					//set new session
					setSession( url, serverSession );
					
					//write out sessions to disk
					writeSessions();
					
				}						
								
			}
			
		}

		return testIsAuthenticated(url);
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @param userName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static boolean authenticate(String url, String userName,
			String password) throws Exception {
	
		PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);

		if (pafServerAck != null) {

			PafService service = WebServicesUtil.getPafService(url);

			PafAuthRequest pafAuthRequest = new PafAuthRequest();

			String iV = AESEncryptionUtil.generateIV();
			
			pafAuthRequest.setClientId(pafServerAck.getClientId());
			
			//encrypt username and password
			pafAuthRequest.setUsername(AESEncryptionUtil.encrypt(userName, iV));
			
			if ( password != null ) {
				
				pafAuthRequest.setPassword(AESEncryptionUtil.encrypt(password, iV));
				
			}
			
			pafAuthRequest.setIV(iV);
			
			//NTSystem ntSystem = new NTSystem();
			@SuppressWarnings("rawtypes")
			Class ntSystem = null;
			Class noparams[] = {};
			Object ntSystemInstance = null;
			Object methReturn = null;
			
			
			try {
				ntSystem = Class.forName("com.sun.security.auth.module.NTSystem");
				ntSystemInstance = ntSystem.newInstance();
				
			} catch (Exception e) {
				logger.warn("com.sun.security.auth.module.NTSystem - AD integration will not be available.");
				logger.warn(e.getMessage());
			} 
			
			
			String domainName = null;
			
			if ( ntSystem != null ) {
				
				if ( password == null ) {
					
					//String userSid = ntSystem.getUserSID();
					Method method = ntSystem.getMethod("getUserSID", noparams);
					methReturn = method.invoke(ntSystemInstance);
					
					if(methReturn != null) {
					
						String userSid = methReturn.toString();
						
						if ( userSid != null ) {
						
							pafAuthRequest.setSid(AESEncryptionUtil.encrypt(userSid, iV));
							
						}
					}

				} else {

					//domainName = ntSystem.getDomain();
					
					Method method = ntSystem.getMethod("getDomain", noparams);
					methReturn = method.invoke(ntSystemInstance);
					
					if ( methReturn != null ) {
					
						domainName = methReturn.toString();
						
						if ( domainName != null ) {
							
							pafAuthRequest.setDomain(AESEncryptionUtil.encrypt(domainName, iV));
							
						}
					}
					
				}
				
			}

			PafAuthResponse authResponse = null;
			
			try {
				
				try {
	
					authResponse = service.clientAuth(pafAuthRequest);
			
				} catch (PafSoapException_Exception e) {
											
//					pafServerAck = WebServicesUtil.getPafServerAck(url, true);
					// TTN-2623 Do client Init before it can be authenticated. 
					WebServicesUtil.loadPafServerAckMap(url);
					pafServerAck = WebServicesUtil.getPafServerAck(url);
					
					pafAuthRequest.setClientId(pafServerAck.getClientId());
					
					authResponse = service.clientAuth(pafAuthRequest);
														
				}

			} catch (PafSoapException_Exception e) {

				logger.error("Paf Soap Exception occurred : " + e.getMessage());
				
				throw new Exception(e.getMessage());
								
			} 
			
			if ( authResponse != null ) {
			
				PafSecurityToken token = authResponse.getSecurityToken();
	
				if (token != null && token.isValid() ) {
					
					ServerSession serverSession = getSession(url);		
					
					//get old login override value
					boolean loginOverride = serverSession.isLoginOverride();
					
					//create new server session
					serverSession = new ServerSession();
					
					serverSession.setClientId(pafServerAck.getClientId());
					serverSession.setUserName(userName);
					serverSession.setPassword(password);
					serverSession.setAdmin(authResponse.isAdmin());
					serverSession.setPafServerAck(pafServerAck);
					serverSession.setDomainName(domainName);				
					serverSession.setLoginOverride(loginOverride);
					serverSession.setSecurityToken(token.getSessionToken());
					serverSession.setSecurityTokenValid(token.isValid());
					
					//set new session
					setSession( url, serverSession );
					
					//write out sessions to disk
					writeSessions();
					
				}						
								
			}
			
		}

		return isAuthenticated(url);
	}
	
	/**
	 * 
	 * Returns generated user name for ldap admin user in security db
	 * 
	 * @param url
	 * @param ldapUserName
	 * @param domainName
	 * @return
	 */
	public static String getLDAPAdminUserNameFromServer(String url, PafUserDef pafUserDef) {
		
		PafUserDef localPafUserDef = null;
		
		if (pafUserDef != null) {
		
			PafUserDef[] pafUsersFromServer = getDbUsersFromServer(url);
			
			// See if an existing user matches ldap admin email field - using email field as unique id due to db column length requirements
			for(PafUserDef serverPafUserDef : pafUsersFromServer){
				
				if (serverPafUserDef.getEmail() != null && pafUserDef.getEmail() != null) {
					if(serverPafUserDef.getEmail().equals(pafUserDef.getEmail())) {
						
						// return true;
						return serverPafUserDef.getUserName();
					}
				}
			}
		}
	
		return "";
	}
	
	/**
	 * 
	 * Checks to see if an ldap user is in the security db
	 * 
	 * @param url
	 * @param ldapUserName
	 * @param domainName
	 * @return
	 */
	public static boolean getLDAPAdminUserFromServer(String url, PafUserDef pafUserDef) {
		
		PafUserDef localPafUserDef = null;
		PafUserDef[] pafUsersFromServer = null;
		
		if (pafUserDef != null) {
			
			pafUsersFromServer = getDbUsersFromServer(url, false);
			
			if (pafUsersFromServer != null) {
				for(PafUserDef serverPafUserDef : pafUsersFromServer){
					
					if (serverPafUserDef.getEmail() != null && pafUserDef.getEmail() != null) {
						if(serverPafUserDef.getEmail().equals(pafUserDef.getEmail())) {
							
							return true;
						}
					}
				}
			}
		}
	
		return false;
	}
	
	public static PafUserDef createLDAPAdminUserDef(String ldapUserName, String domainName) {
		
		PafUserDef pafUserDef = new PafUserDef();
		String ldapUserSecurityName = generateLDAPUserSecurityName(15);
		
		// using the email field due to string length limitations in userName field
		String ldapUserSecurityEmail =  createLDAPUserSecurityName(ldapUserName, domainName);
		
		
		pafUserDef.setUserName(ldapUserSecurityName);
		pafUserDef.setEmail(ldapUserSecurityEmail);	
		pafUserDef.setAdmin(true);
		pafUserDef.setDomain(domainName);
		pafUserDef.setPassword("x");
		
		return pafUserDef;
	}
	
	public static boolean deleteLDAPAdminUserFromServer(String url, String pafServerName, PafUserDef pafUserDef) {
		
		if (pafUserDef != null) {
			
			boolean ldapUserInSecuirtyDB = getLDAPAdminUserFromServer(url, pafUserDef);
			
			if (ldapUserInSecuirtyDB) {
				String ldapUserName = getLDAPAdminUserNameFromServer(url, pafUserDef);
				return deleteDBUserFromServer(pafServerName, ldapUserName);
			}
		}
		
		return false;
	}
	
	public static String generateLDAPUserSecurityName(int length) {
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder( length );
		for( int i = 0; i < length; i++ ) 
		      sb.append( AB.charAt( random.nextInt(AB.length()) ) );
		return sb.toString();
	}
	
	
	/**
	 * There is duplicate code on the server that does the same thing in order to check ldap admin user definitions
	 * against the security db.
	 * 
	 * Any changes to this code will also need to be made here: PafServiceProvider.createLDAPUserSecurityName
	 * 
	 * Refactoring the code to use the same method would create complex method signatures on the AC
	 * 
	 * @param ldapUserName
	 * @param domainName
	 * @return
	 */
	public static String createLDAPUserSecurityName(String ldapUserName, String domainName) {
		
		if (!ldapUserName.isEmpty() && !domainName.isEmpty()) {
			
			return Constants.LDAPAdminUserStartToken + ldapUserName + Constants.LDAPAdminUserSeperatorToken + 
					domainName + Constants.LDAPAdminUserEndToken;
		}
	
		return "";

	}
	
	
	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @param userName
	 * @return
	 * @throws UserNotFoundException
	 */
	public static PafUserDef getDbUserFromServer(String url, String userName, String domainName) throws UserNotFoundException {

		PafUserDef dbUserFromServer = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			try {

				ServerSession serverSession = getSession(url);	
				
				PafClientSecurityRequest request = new PafClientSecurityRequest();

				request.setClientId(serverSession.getClientId());

				request.setSessionToken(serverSession.getSecurityToken());

				PafUserDef pafUserDef = new PafUserDef();

				pafUserDef.setUserName(userName);
				pafUserDef.setDomain(domainName);
				
				request.setPafUserDef(pafUserDef);
				
				PafClientSecurityResponse response = service.getPafUser(request);
				
				dbUserFromServer = response.getPafUserDef();
				
				//if user is null from service call, user doesn't exits
				if ( dbUserFromServer == null ) {
					
					throw new UserNotFoundException();
					
				}
			
				String iV = response.getIV();
				
				if ( iV != null ) {
				
					try {
						
						if(dbUserFromServer.getUserName() != null){
							dbUserFromServer.setUserName(AESEncryptionUtil.decrypt(dbUserFromServer.getUserName(), iV));
						}else{
							dbUserFromServer.setUserName(null);
						}
						
						if(dbUserFromServer.getFirstName() != null){
							dbUserFromServer.setFirstName(AESEncryptionUtil.decrypt(dbUserFromServer.getFirstName(), iV));
						}else{
							dbUserFromServer.setFirstName(null);
						}
						
						if(dbUserFromServer.getLastName() != null){
							dbUserFromServer.setLastName(AESEncryptionUtil.decrypt(dbUserFromServer.getLastName(), iV));
						}else{
							dbUserFromServer.setLastName(null);
						}
						
						if(dbUserFromServer.getEmail() != null){
							dbUserFromServer.setEmail(AESEncryptionUtil.decrypt(dbUserFromServer.getEmail(), iV));
						}else{
							dbUserFromServer.setEmail(null);
						}
						
						if(dbUserFromServer.getPassword() != null){
							dbUserFromServer.setPassword(AESEncryptionUtil.decrypt(dbUserFromServer.getPassword(), iV));
						}else{
							dbUserFromServer.setPassword(null);
						}
						
						
					} catch (Exception e) {
						
						logger.error(e.getMessage());
						
					}
				}
				
			} catch (PafNotAbletoGetLDAPContext_Exception e) {

				logger.error(e.getMessage());				
							
			} catch (PafNotAuthenticatedSoapException_Exception e) {				

				if ( tryToReAuthenticate(url) ) {
					
					dbUserFromServer = getDbUserFromServer(url, userName, domainName);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			}
		}

		return dbUserFromServer;

	}

	/**
	 * 
	 *	Gets the DB Users from the server
	 *
	 * @param url
	 * @return
	 */
	public static PafUserDef[] getDbUsersFromServer(String url) {

		PafUserDef[] dbUsersFromServerAr = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafClientSecurityRequest request = new PafClientSecurityRequest();

				request.setClientId(serverSession.getClientId());
				request.setSessionToken(serverSession.getSecurityToken());

				List<PafUserDef> dbUserList = service.getPafUsers(request).getPafUserDefs(); 
				
				if ( dbUserList.size() > 0 ) {
					
					dbUsersFromServerAr = dbUserList.toArray(new PafUserDef[0]);
					
				}

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					dbUsersFromServerAr = getDbUsersFromServer(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			}
		}

		return dbUsersFromServerAr;

	}
	
	/**
	 * 
	 *	TTN-2348 - Disable error dialog from being displayed when EditLDAPAdminActions are disabled 
	 *	Gets the DB Users from the server 
	 *
	 * @param url
	 * @return
	 */
	public static PafUserDef[] getDbUsersFromServer(String url, boolean throwException) {

		PafUserDef[] dbUsersFromServerAr = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafClientSecurityRequest request = new PafClientSecurityRequest();

				request.setClientId(serverSession.getClientId());
				request.setSessionToken(serverSession.getSecurityToken());

				List<PafUserDef> dbUserList = service.getPafUsers(request).getPafUserDefs(); 
				
				if ( dbUserList.size() > 0 ) {
					
					dbUsersFromServerAr = dbUserList.toArray(new PafUserDef[0]);
					
				}

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					dbUsersFromServerAr = getDbUsersFromServer(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				if(throwException) {
					handleNotAuthorizedSoapException();
				}
				
				e.printStackTrace();

			}
		}

		return dbUsersFromServerAr;

	}
	
	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @return
	 */
	public static String[] getNativeDbUserNamesFromServer(String url) {

		String[] dbUserNamesFromServerAr = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafClientSecurityRequest request = new PafClientSecurityRequest();

				request.setClientId(serverSession.getClientId());
				request.setSessionToken(serverSession.getSecurityToken());

				List<String> dbUserNameList = service.getPafUserNames(request).getPafUserNames();
				
				if ( dbUserNameList.size() > 0 ) {
				
					dbUserNamesFromServerAr = dbUserNameList.toArray(new String[0]);
					
				}
				
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					dbUserNamesFromServerAr = getNativeDbUserNamesFromServer(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			}
		}

		return dbUserNamesFromServerAr;

	}


	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param serverName
	 * @param pafUserDef
	 * @return
	 */
	public static boolean createDBUserOnServer(String serverName,
			PafUserDef pafUserDef) {

		boolean userCreated = false;
		
		String url = PafServerUtil.getServerWebserviceUrl(serverName);

		if (url != null && isAuthenticated(url)) {

			ServerSession serverSession = getSession(url);	
			
			PafClientSecurityRequest request = new PafClientSecurityRequest();

			request.setClientId(serverSession.getClientId());
			request.setSessionToken(serverSession.getSecurityToken());
			request.setPafUserDef(pafUserDef);

			try {

				PafClientSecurityResponse response = WebServicesUtil
						.getPafService(url).createPafUser(request);

				userCreated = response.isSuccessful();

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					userCreated = createDBUserOnServer(serverName, pafUserDef);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
								
				handleNotAuthorizedSoapException();
				
			}

		}

		return userCreated;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param serverName
	 * @param pafUserDef
	 * @return
	 */
	public static boolean updateDBUserOnServer(String serverName,
			PafUserDef pafUserDef) {

		boolean userUpdated = false;

		String url = PafServerUtil.getServerWebserviceUrl(serverName);

		if (url != null && isAuthenticated(url)) {

			ServerSession serverSession = getSession(url);
			
			PafClientSecurityRequest request = new PafClientSecurityRequest();
			
			request.setClientId(serverSession.getClientId());
			request.setSessionToken(serverSession.getSecurityToken());

			request.setPafUserDef(pafUserDef);

			try {
				PafService service = WebServicesUtil.getPafService(url);
				if( service != null ) {
					PafClientSecurityResponse response = service.updatePafUser(request);
	
					userUpdated = response.isSuccessful();
				}
				/*
			} catch (RemoteException e) {
				
				logger.error(e.getMessage());
				
				handleRemoteException();
				
				return false;
				*/
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					userUpdated = updateDBUserOnServer(serverName, pafUserDef);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
								
				handleNotAuthorizedSoapException();
				
			} catch (final PafSoapException_Exception e) {
				
				Display.getDefault().asyncExec(new Runnable() {

					public void run() {

						GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
						
					}
				});				
				
			}

		}

		return userUpdated;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param serverName
	 * @param userName
	 * @return
	 */
	public static boolean deleteDBUserFromServer(String serverName,
			String userName) {

		boolean userDeleted = false;
		
		String url = PafServerUtil.getServerWebserviceUrl(serverName);

		if (url != null && isAuthenticated(url)) {
			
			ServerSession serverSession = getSession(url);

			PafClientSecurityRequest request = new PafClientSecurityRequest();

			request.setClientId(serverSession.getClientId());

			request.setSessionToken(serverSession.getSecurityToken());

			PafUserDef pafUserDef = new PafUserDef();

			pafUserDef.setUserName(userName);

			request.setPafUserDef(pafUserDef);

			try {

				PafClientSecurityResponse response = WebServicesUtil
						.getPafService(url).deletePafUser(request);

				userDeleted = response.isSuccessful();


			} catch (PafNotAuthenticatedSoapException_Exception e) {

				if ( tryToReAuthenticate(url) ) {
					
					userDeleted = deleteDBUserFromServer(serverName, userName);
					
				}				
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			} catch (final PafSoapException_Exception e) {
				
				Display.getDefault().asyncExec(new Runnable() {

					public void run() {

						GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, e.getMessage(), MessageDialog.ERROR);
						
					}
				});				
				
			}

		}

		return userDeleted;
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param serverName
	 * @param userName
	 * @return
	 * @throws PafException
	 */
	public static boolean resetPasswordForDBUserOnServer(String serverName,
			String userName) throws PafException {

		boolean isSuccessfulPasswordReset = false;
		
		String url = PafServerUtil.getServerWebserviceUrl(serverName);

		if (url != null && isAuthenticated(url)) {

			ServerSession serverSession = getSession(url);
			
			PafClientSecurityRequest request = new PafClientSecurityRequest();

			request.setClientId(serverSession.getClientId());

			request.setSessionToken(serverSession.getSecurityToken());

			PafUserDef pafUserDef = new PafUserDef();

			pafUserDef.setUserName(userName);

			request.setPafUserDef(pafUserDef);

			try {
				PafService service = WebServicesUtil.getPafService(url);
				if( service != null ) {
					PafClientSecurityPasswordResetResponse response = service.resetPafUserPassword(request);
	
					isSuccessfulPasswordReset = response.isSuccessful();
					
					if ( ! isSuccessfulPasswordReset ) {
						
						String errorMessage = "There was a problem resetting the password for user '" + pafUserDef.getUserName() + "'.";
						
						if ( response.isInvalidUserName() ) {
							
							errorMessage += " User '" + pafUserDef.getUserName() + "' does not exist on the server.  Please refresh the Pace Server list.";
							
						} else if ( response.isInvalidEmailAddress() ) {
							
							errorMessage += " User '" + pafUserDef.getUserName() + "' does not have an email address setup.  Please add an email address to the user's account.";
							
						}
						
						throw new PafException(errorMessage, PafErrSeverity.Warning);
						
					}
				}
			} catch (PafSoapException_Exception e) {
				
				throw new PafException(e);
				
			}		

		}

		return isSuccessfulPasswordReset;
	}

	/**
	 * 
	 *	Gets the Pace Security Domain Group by domain name.
	 *
	 * @param url
	 * @param domainName
	 * @return
	 */
	public static String[] getAllSecurityGroupNamesByDomainFromServer(String url, String domainName) throws PafException {
		
		String[] domainSecurityGroupAr = null;
		
		List<PafSecurityDomainGroups> domainSecurityGroupList = getDomainSecurityGroupsFromServer(url);
		
		if ( domainSecurityGroupList != null ) {
		
			for (PafSecurityDomainGroups domainSecurityGroup : domainSecurityGroupList ) {
				
				if ( domainSecurityGroup.getDomain() != null && domainSecurityGroup.getDomain().equalsIgnoreCase(domainName) && domainSecurityGroup.getSecurityGroups().size() > 0 ) {
					
					domainSecurityGroupAr = new String[domainSecurityGroup.getSecurityGroups().size()];
					
					int ndx = 0;
					
					for (PafSecurityGroup pafSecurityGroup : domainSecurityGroup.getSecurityGroups()) {
					
						domainSecurityGroupAr[ndx++] = pafSecurityGroup.getGroupName();
						
					}			
					
				}				
				
			}
			
		}		
		
		return domainSecurityGroupAr;		
		
	}		
	
	/**
	 * 
	 *	Gets the Pace Security Domain Groups from the server
	 *
	 * @param url
	 * @return
	 */
	public static List<PafSecurityDomainGroups> getDomainSecurityGroupsFromServer(String url) throws PafException {

		List<PafSecurityDomainGroups> securityDomainGroupList = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafGroupSecurityRequest groupSecurityRequest = new PafGroupSecurityRequest();
		    	
				groupSecurityRequest.setClientId(serverSession.getClientId());										
									
				PafGroupSecurityResponse groupSecurityResponse = service.getPafGroups(groupSecurityRequest);
					
				if ( groupSecurityResponse != null ) {
				
					securityDomainGroupList = groupSecurityResponse.getDomainGroups();
					
				}

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					securityDomainGroupList = getDomainSecurityGroupsFromServer(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			} catch (PafSoapException_Exception e) {

				logger.error(e.getMessage());
				
				throw new PafException(e);
				
			} catch (PafNotAbletoGetLDAPContext_Exception e) {

				logger.error(e.getMessage());
				
				throw new PafException(e);
			}
		}

		return securityDomainGroupList;

	}
	
	/**
	 * 
	 *	Gets the Pace Security Group Names for a domain from the server
	 *
	 * @param url
	 * @return
	 */
	public static String[] getPaceSecurityGroupNamesByDomainFromServer(String url, String domainName) throws PafException {
		
		String[] domainSecurityGroupAr = null;
		
		SecurityGroup[] paceSecurityGroups = getPaceGroups(url);
		
		if ( paceSecurityGroups != null ) {
		
			Map<String, Set<String>> activeDomainSecurityGroupMap = WebServicesUtil.createDomainSecurityGroupMap(paceSecurityGroups);
			
			if ( activeDomainSecurityGroupMap != null && activeDomainSecurityGroupMap.containsKey(domainName) ) {
			
				Set<String> activeDomainSecurityGroupSet = activeDomainSecurityGroupMap.get(domainName);
				
				if ( activeDomainSecurityGroupSet != null && activeDomainSecurityGroupSet.size() > 0 ) {
				
					domainSecurityGroupAr = activeDomainSecurityGroupSet.toArray(new String[0]);
					
				}				
				
			}
									
		}		
		
		return domainSecurityGroupAr;		
		
	}
			
	/**
	 * 
	 *	Gets the Pace Security Groups from the server
	 *
	 * @param url
	 * @return
	 */
	public static SecurityGroup[] getPaceGroups(String url) throws PafException {

		SecurityGroup[] securityGroups = null;

		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafGetPaceGroupsRequest paceGroupsRequest = new PafGetPaceGroupsRequest();
		    	
				paceGroupsRequest.setClientId(serverSession.getClientId());
						
				paceGroupsRequest.setApplication(serverSession.getPafServerAck().getApplicationId());
					
				PafGetPaceGroupsResponse paceGroupsResponse = service.getGroups(paceGroupsRequest);
					
				if ( paceGroupsResponse != null && paceGroupsResponse.getSecurityGroups().size() > 0 ) {
				
					securityGroups = paceGroupsResponse.getSecurityGroups().toArray(new SecurityGroup[0]);
					
				}

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					securityGroups = getPaceGroups(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			} catch (PafSoapException_Exception e) {

				logger.error(e.getMessage());
				
				throw new PafException(e);
				
			}
		}
		
		return securityGroups;

	}
	
	public static void setPaceGroups(String url, Map<String, Set<String>> activeDomainPaceSecurityGroupMap) throws PafException {
	
		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				PafSetPaceGroupsRequest paceGroupsRequest = new PafSetPaceGroupsRequest();
		    	
				paceGroupsRequest.setClientId(serverSession.getClientId());
				paceGroupsRequest.setApplication(serverSession.getPafServerAck().getApplicationId());
				
				Application application = new Application();
				
				application.setName(serverSession.getPafServerAck().getApplicationId());
				
				 if ( activeDomainPaceSecurityGroupMap != null ) {
					 
					 for (String domainName : activeDomainPaceSecurityGroupMap.keySet() ) {
						 
						 Set<String> securityGroupSet = activeDomainPaceSecurityGroupMap.get(domainName);
						 
						 if ( securityGroupSet != null ) {
							 
							 for (String paceGroup : securityGroupSet ) {
									
									SecurityGroup securityGroup = new SecurityGroup();
									securityGroup.setSecurityDomainNameTxt(domainName);
									securityGroup.setSecurityGroupNameTxt(paceGroup);
									securityGroup.setApplication(application);
									paceGroupsRequest.getSecurityGroups().add(securityGroup);
								
							}						 
						 }
					 }
					
				 }
				
				if( service != null ) {
					PafSetPaceGroupsResponse paceGroupsResponse = service.setGroups(paceGroupsRequest);
					
					if ( paceGroupsResponse != null && paceGroupsResponse.isSuccess() ) {
					
						logger.info("Setting of pace groups was successful");
						
					} else {
						
						logger.error("Setting of pace groups was NOT successful");
						
					}
				}		
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					setPaceGroups(url, activeDomainPaceSecurityGroupMap);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			} catch (PafSoapException_Exception e) {

				logger.error(e.getMessage());
				
				throw new PafException(e);
				
			}

		}
		
	}
	
	/**
	 * 
	 *	Gets the Pace Security Groups from the server
	 *
	 * @param url
	 * @return
	 */
	public static void setPaceGroups(String url, String domainName, String[] paceGroups) throws PafException {
		
		if (isAuthenticated(url)) {

			PafService service = WebServicesUtil.getPafService(url);
			
			if( service != null ) {
				ServerSession serverSession = getSession(url);
				
				try {					
					
					PafSetPaceGroupsRequest paceGroupsRequest = new PafSetPaceGroupsRequest();
			    	
					paceGroupsRequest.setClientId(serverSession.getClientId());
					
					Application application = new Application();
					
					application.setName(serverSession.getPafServerAck().getApplicationId());
					
					 if ( paceGroups != null ) {
						 
						for (String paceGroup : paceGroups ) {
						
							SecurityGroup securityGroup = new SecurityGroup();
							securityGroup.setSecurityDomainNameTxt(domainName);
							securityGroup.setSecurityGroupNameTxt(paceGroup);
							securityGroup.setApplication(application);
							paceGroupsRequest.getSecurityGroups().add(securityGroup);
							
						}
						
					 }
												
					PafSetPaceGroupsResponse paceGroupsResponse = service.setGroups(paceGroupsRequest);
					
				} catch (PafNotAuthenticatedSoapException_Exception e) {
					
					if ( tryToReAuthenticate(url) ) {
						
						setPaceGroups(url, domainName, paceGroups);
						
					}
					
				} catch (PafNotAuthorizedSoapException_Exception e) {
				
					handleNotAuthorizedSoapException();
					
				} catch (PafSoapException_Exception e) {
	
					logger.error(e.getMessage());
					
					throw new PafException(e);
					
				}
			}
		}

	}
	
	
	/**
	 * 
	 * Returns all the domain, security groups and user names for a server.
	 *
	 * @param url - server location
	 * @return		Map <key : domain name; value Map<key: security group; value set of user names.
	 * @throws PafException
	 */
	public static Map<String, Map<String, Set<PaceUser>>> getUserNamesBySecurityGroups(String url) throws PafException {
		
		return getUserNamesBySecurityGroups(url, null);
		
	}

	public static Map<String, Map<String, Set<PaceUser>>> getCachedUserNamesBySecurityGroups(String url) throws PafException {
		
		Map<String, Map<String, Set<PaceUser>>> domainNameSecuirtyGroupUserNamesMap = new TreeMap<String, Map<String, Set<PaceUser>>>(String.CASE_INSENSITIVE_ORDER);;
		
		if ( cachedUserNameBySecurityGroupByDomainByUrlMap.containsKey(url) ) {
			
			domainNameSecuirtyGroupUserNamesMap = cachedUserNameBySecurityGroupByDomainByUrlMap.get(url);
					
		} else {			
			
			domainNameSecuirtyGroupUserNamesMap = getUserNamesBySecurityGroups(url, null);
			
		}	
		
		if ( domainNameSecuirtyGroupUserNamesMap == null ) {
			
			domainNameSecuirtyGroupUserNamesMap = new TreeMap<String, Map<String, Set<PaceUser>>>(String.CASE_INSENSITIVE_ORDER);
			
		} else {
			
			if ( domainNameSecuirtyGroupUserNamesMap != null ) {
				
				Map<String, Map<String, Set<PaceUser>>> newDomainNameSecGrpUNMap = new TreeMap<String, Map<String, Set<PaceUser>>>(String.CASE_INSENSITIVE_ORDER);
				
				for (String domainName : domainNameSecuirtyGroupUserNamesMap.keySet()) {
										
					Map<String, Set<PaceUser>> securityGroupUserNameMap = domainNameSecuirtyGroupUserNamesMap.get(domainName);
					
					Map<String, Set<PaceUser>> newSecurityGroupUserNameMap = new TreeMap<String, Set<PaceUser>>(String.CASE_INSENSITIVE_ORDER);
					
					if ( securityGroupUserNameMap != null ) {
					
						for ( String securityGroup : securityGroupUserNameMap.keySet() ) {
							
							Set<PaceUser> userNameSet = securityGroupUserNameMap.get(securityGroup);
							
							Set<PaceUser> newUserNameSet = new HashSet<PaceUser>();
							
							if ( userNameSet != null ) {
								
								newUserNameSet.addAll(userNameSet);
								
							}
							
							newSecurityGroupUserNameMap.put(securityGroup, newUserNameSet);
							
						}
						
					}
				
					newDomainNameSecGrpUNMap.put(domainName, newSecurityGroupUserNameMap);
					
				}
				
				domainNameSecuirtyGroupUserNamesMap = newDomainNameSecGrpUNMap;
				
			}
			
			
		}
		
		return domainNameSecuirtyGroupUserNamesMap;
		
		
	}
	
	/**
	 * 
	 * Returns all the domain, security groups and user names for a server via a domain filter
	 *
	 * @param url
	 * @param domainFilters
	 * @return		Map <key : domain name; value Map<key: security group; value set of users.
	 * @throws PafException
	 */
	public static Map<String, Map<String, Set<PaceUser>>> getUserNamesBySecurityGroups(String url, DomainFilter[] domainFilters) throws PafException {

		Map<String, Map<String, Set<PaceUser>>> domainNameSecuirtyGroupUserNamesMap = new TreeMap<String, Map<String, Set<PaceUser>>>(String.CASE_INSENSITIVE_ORDER);

		if (isAuthenticated(url)) {		
			
			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
			
			try {					
				
				// create request
				PafUserNamesforSecurityGroupsRequest groupSecurityRequest = new PafUserNamesforSecurityGroupsRequest();
		    	
				groupSecurityRequest.setClientId(serverSession.getClientId());
				groupSecurityRequest.setApplication(serverSession.getPafServerAck().getApplicationId());
				groupSecurityRequest.setSessionToken(serverSession.getSecurityToken());
				
				if ( domainFilters != null ) {
					
					// add domain names to group
					for (DomainFilter domainFilter : domainFilters) {
						
						PafSecurityDomainGroups pafSecurityDomainGroup = new PafSecurityDomainGroups();
						
						pafSecurityDomainGroup.setDomain(domainFilter.getDomainName());
						
						List<PafSecurityGroup> securityDomainGroupList = pafSecurityDomainGroup.getSecurityGroups();
						
						for (String securityGroupName : domainFilter.getSecurityGroupSet()) {
						
							PafSecurityGroup pafSecurityGroup = new PafSecurityGroup();
							
							pafSecurityGroup.setGroupName(securityGroupName);
													
							securityDomainGroupList.add(pafSecurityGroup);					
						
						}
						
						groupSecurityRequest.getDomainUserNames().add(pafSecurityDomainGroup);
						
					}
					
				}
				
				if( service != null ) {
					PafUserNamesforSecurityGroupsResponse userNamesforSecurityGroupsResponse = service.getUserNamesForSecurityGroups(groupSecurityRequest);
														
					if ( userNamesforSecurityGroupsResponse != null && userNamesforSecurityGroupsResponse.getDomainUserNames().size() > 0 ) {
					
						List<PafSecurityDomainUserNames> domainUserNameList = userNamesforSecurityGroupsResponse.getDomainUserNames();
						
						for (PafSecurityDomainUserNames pafSecurityDomainuserName : domainUserNameList) {
						
							String domainName = pafSecurityDomainuserName.getDomainName();
							
							Map<String, Set<PaceUser>> securityGroupUserNameMap = null;
							
							if ( domainNameSecuirtyGroupUserNamesMap.containsKey(domainName) && domainNameSecuirtyGroupUserNamesMap.get(domainName) != null ) {
								
								securityGroupUserNameMap = domainNameSecuirtyGroupUserNamesMap.get(domainName);
								
							} else {
								
								securityGroupUserNameMap = new TreeMap<String, Set<PaceUser>>();
								
							}
							
							
							List<PafUserNamesSecurityGroup> pafUserNamesSecurityGroupList = pafSecurityDomainuserName.getUserNamesSecurityGroup();
						
							for ( PafUserNamesSecurityGroup pafUserNamesSecurityGroup : pafUserNamesSecurityGroupList ) {
								
								String groupName = pafUserNamesSecurityGroup.getGroupName();
								
								Set<PaceUser> paceUserSet = null;
								
								if ( securityGroupUserNameMap.containsKey(groupName) &&  securityGroupUserNameMap.get(groupName) != null ) {
									
									paceUserSet = securityGroupUserNameMap.get(groupName);
									
								} else {
									
									paceUserSet = new TreeSet<PaceUser>();
									
								}
								
								//TODO: Maybe use the Display Name instead
								//paceUserSet.addAll();
								
								List<PaceUser> paceUserList = new ArrayList<PaceUser>();
								
								int ndx = 0;
								
								for (String userName : pafUserNamesSecurityGroup.getUserNames()) {
									
									PaceUser paceUser = null;
									
									if ( pafUserNamesSecurityGroup.getDisplayNames().size() == 0) {
									
										paceUser = new PaceUser(userName);
										
									} else {
									
										paceUser = new PaceUser(userName, pafUserNamesSecurityGroup.getDisplayNames().get(ndx++));
										
									}							
									
									paceUserList.add(paceUser);
									
								}
								
								paceUserSet.addAll(paceUserList);
								
								securityGroupUserNameMap.put(groupName, paceUserSet);							
								
							}
							
							domainNameSecuirtyGroupUserNamesMap.put(domainName, securityGroupUserNameMap);
						}					
					}					
				}

			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					domainNameSecuirtyGroupUserNamesMap = getUserNamesBySecurityGroups(url);
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
			
				handleNotAuthorizedSoapException();
				
			} catch (PafSoapException_Exception e) {

				logger.error(e.getMessage());
				
				throw new PafException(e);
				
			} catch (PafNotAbletoGetLDAPContext_Exception e) {
				
				logger.warn("LDAP Context could not be created. Only pace users will be used.");
				
				DomainFilter df = new DomainFilter();
				df.setDomainName(PafBaseConstants.Native_Domain_Name);
				df.getSecurityGroupSet().add(PafBaseConstants.Native_Domain_Name);
				
				return getUserNamesBySecurityGroups(url, new DomainFilter[] { df });
			}
		}
		
		
		if ( domainFilters == null ) {
			
			cachedUserNameBySecurityGroupByDomainByUrlMap.put(url, domainNameSecuirtyGroupUserNamesMap);
			
		}

		return domainNameSecuirtyGroupUserNamesMap;

	}
	
	public static boolean clearMemberTagData(String url, MemberTagInfo[] memberTagInfoAr) throws PafException { 
		
		boolean isSuccessful = false;
		
		if (isAuthenticated(url)) {		
			
			PafService service = WebServicesUtil.getPafService(url);

			ServerSession serverSession = getSession(url);
		
			//create request
			PafFilteredMbrTagRequest request = new PafFilteredMbrTagRequest();
				
			//populate client id and security token
			request.setClientId(serverSession.getClientId());
			
			if ( serverSession.getSecurityToken() != null ) {
				
				request.setSessionToken(serverSession.getSecurityToken());
				
			}
			
			//create filter set to hold all the Member Tag Filters
			Set<PafMbrTagFilter> filterSet = new HashSet<PafMbrTagFilter>();
			
			//populate filter set with member tag filters
			for (MemberTagInfo memberTagInfo : memberTagInfoAr ) {
					
				//create new filter
				PafMbrTagFilter filter = new PafMbrTagFilter();
				
				//set app name
				filter.setAppName(memberTagInfo.getAppId());
				
				//if no filters exists, use all member tags
				if ( memberTagInfo.getMemberTagFilters(false) == null ) {
				
					if ( memberTagInfo.getMemberTagNames() != null ) {
						
						filter.getMemberTagNames().addAll(Arrays.asList(memberTagInfo.getMemberTagNames()));
						
					}
					
				//if filters exists, only use filtered member tags
				} else {
					
					String[] memberTagFiltersAr = memberTagInfo.getMemberTagFilters(false);
					
					filter.getMemberTagNames().addAll(Arrays.asList(memberTagFiltersAr));
					
				}				
			
				//add filter to set
				filterSet.add(filter);						
				
			}
			
			//set member tag filters
			request.getMemberTagFilters().addAll(filterSet);
		
			try {
			
				PafSuccessResponse response = service.clearMemberTagData(request);
				
				if ( response != null ) {
					
					isSuccessful =  response.isSuccess();
					
				}
								
			} catch (PafSoapException_Exception e) {
				
				logger.error(e.getMessage());
				
				throw new PafException(e);
	
			} catch (PafNotAuthorizedSoapException_Exception e) {
				
				handleNotAuthorizedSoapException();
				
			} catch (PafNotAuthenticatedSoapException_Exception e) {
				
				if ( tryToReAuthenticate(url) ) {
					
					isSuccessful = clearMemberTagData(url, memberTagInfoAr);
					
				}
				
			}
		}
		
		return isSuccessful;
		
	}
	
	// method to fetch the PafSimpleDimTree from the WSDL based on the datasource. Used by the Essbase New project Wizard.
	public static PafSimpleDimTree getPafSimpleDimTreeForDataSource(String url, String dimensionName , String datasourceId) throws RemoteException, PafException {
		PafSimpleDimTree pafSimpleDimTree = null;

		try {
							
			System.out.println("KB, dimensionName is:"+dimensionName);
			System.out.println("KB, datasourceId is:"+datasourceId);
			ServerSession serverSession = getSession(url);
			
			PafTreeRequest pafTreeRequest = new PafTreeRequest();
								
			pafTreeRequest.setClientId(serverSession.getClientId());

			pafTreeRequest.setTreeName(dimensionName);
			
			pafTreeRequest.setDatasourceID(datasourceId);
			
			pafTreeRequest.setCompressResponse(true);
			
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ){
				// get simple tree from web service call
				PafTreeResponse pafTreeResponse = service.getSimpleMdbTree(pafTreeRequest);
	
				if (pafTreeResponse != null) {
	
					pafSimpleDimTree = pafTreeResponse.getPafSimpleDimTree();
					
					if ( pafSimpleDimTree != null && pafSimpleDimTree.isCompressed()) {
						
						pafSimpleDimTree = TreeUncompressUtil.uncompressTree(pafSimpleDimTree);
						
					}
				}
			}	
			

		} catch (PafSoapException_Exception e) {

			logger.error(e.getMessage());
			
			throw new PafException(e);
							
		}
		return pafSimpleDimTree;
	}
	
	
	
	public static PafSimpleDimTree getPafSimpleDimTree(String url, String dimensionName) throws RemoteException, PafException {
		PafSimpleDimTree pafSimpleDimTree = null;

		try {
							
			ServerSession serverSession = getSession(url);
			
			PafTreeRequest pafTreeRequest = new PafTreeRequest();
								
			pafTreeRequest.setClientId(serverSession.getClientId());

			pafTreeRequest.setTreeName(dimensionName);
			
			pafTreeRequest.setCompressResponse(true);
			
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ){
				// get simple tree from web service call
				PafTreeResponse pafTreeResponse = service.getDimensionTree(pafTreeRequest);
	
				if (pafTreeResponse != null) {
	
					pafSimpleDimTree = pafTreeResponse.getPafSimpleDimTree();
					
					if ( pafSimpleDimTree != null && pafSimpleDimTree.isCompressed()) {
						
						pafSimpleDimTree = TreeUncompressUtil.uncompressTree(pafSimpleDimTree);
						
					}
				}
			}	
			

		} catch (PafSoapException_Exception e) {

			logger.error(e.getMessage());
			
			throw new PafException(e);
							
		}
		return pafSimpleDimTree;
	}
	
	/**
	 * 
	 *  Method_description_goes_here
	 *
	 * @param url url to service
	 * @return	array of paf simple dim tree
	 * @throws RemoteException
	 * @throws PafException
	 */
	public static PafSimpleDimTree[] getPafSimpleDimTrees(String url) throws RemoteException, PafException {
		
		PafSimpleDimTree[] pafSimpleDimTrees = null;
		
		try {
							
			ServerSession serverSession = getSession(url);
			
			PafTreesRequest pafTreesRequest = new PafTreesRequest();
								
			pafTreesRequest.setClientId(serverSession.getClientId());
			
			pafTreesRequest.setCompressResponse(true);

			// get all paf simple trees on running server
			PafTreesResponse pafTreesResponse = WebServicesUtil.getPafService(url).getDimensionTrees(pafTreesRequest);
					
			if (pafTreesResponse != null ) {

				List<PafSimpleDimTree> treeList = pafTreesResponse.getPafSimpleDimTrees();				
				
				if ( treeList.size() > 0 ) {
														
					pafSimpleDimTrees = treeList.toArray(new PafSimpleDimTree[0]);									
															
					for (PafSimpleDimTree tree : pafSimpleDimTrees ) {
						
						if ( tree.isCompressed() ) {
							
							tree = TreeUncompressUtil.uncompressTree(tree);
							
						}
						
					}					
					
				
				}
			}
			else {
				
				String errMsg = "The cube changes were not downloaded successfully.";
				logger.error(errMsg);
				
				throw new PafException(errMsg, PafErrSeverity.Error);
				
			}
			
		} catch (PafSoapException_Exception e) {

			logger.error(e.getMessage());
			
			throw new PafException(e);
							
		}
		
		return pafSimpleDimTrees;
	}
	
	
	
	public static com.pace.server.client.PafMdbProps getPafMdbProps(String url) throws RemoteException, PafSoapException_Exception, PafException {
		
		com.pace.server.client.PafMdbProps pafMdbProps = null;
		
		try {
												
			ServerSession serverSession = getSession(url);
			
			//create a props request object
			PafMdbPropsRequest pafMdbPropsRequest = new PafMdbPropsRequest();
			pafMdbPropsRequest.setClientId(serverSession.getClientId());
			pafMdbPropsRequest.setSessionToken(serverSession.getSecurityToken());
			
			// get all paf simple trees on running server
			PafMdbPropsResponse pafMdbPropsResponse = WebServicesUtil.getPafService(url).getMdbProps(pafMdbPropsRequest);
			
			if (pafMdbPropsResponse != null) {
				pafMdbProps = pafMdbPropsResponse.getMdbProps();
			}

		} catch (PafSoapException_Exception e) {

			logger.error(e.getMessage());
			
			throw new PafException(e);
			
		} catch (PafNotAuthorizedSoapException_Exception e) {
			
			handleNotAuthorizedSoapException();
			
		} catch (PafNotAuthenticatedSoapException_Exception e) {

			if ( tryToReAuthenticate(url) ) {
				
				pafMdbProps = getPafMdbProps(url);
				
			}
			
		}
		
		return pafMdbProps;
	}
	
	
	public static MemberTagInformation[] getMemberTagInformation(String url) throws RemoteException, PafSoapException_Exception {
						
		MemberTagInformation[] memberTagInformationAr = null;
				
		if ( isAuthenticated(url) ) {
		
			//web service
			PafService service = WebServicesUtil.getPafService(url);
			
			ServerSession serverSession = getSession(url);
			
			//create new paf request and set client id
			PafRequest pafRequest = new PafRequest();
			
			//set clietn id
			pafRequest.setClientId(serverSession.getClientId());
			
			//create filtered request
			PafFilteredMbrTagRequest filteredRequest = new PafFilteredMbrTagRequest();
			
			//set client id
			filteredRequest.setClientId(serverSession.getClientId());
				
			//set session token	
			filteredRequest.setSessionToken(serverSession.getSecurityToken());
							
			//call the getMemberTagInfo
			PafGetMemberTagInfoResponse pafMemberTagInfoResponse = null;
			
			try {
				
				pafMemberTagInfoResponse = service.getMemberTagInfo(filteredRequest);
				
				if ( pafMemberTagInfoResponse != null ) {
					
					List<MemberTagInformation> memberTagInformationList =  pafMemberTagInfoResponse.getMemberTagInfo();
					
					if ( memberTagInformationList.size() > 0 ) {
						
						memberTagInformationAr = memberTagInformationList.toArray(new MemberTagInformation[0]);
						
					} 
					
				}
				
			} catch (PafNotAuthorizedSoapException_Exception e) {
				
				handleNotAuthorizedSoapException();
				
			} catch (PafNotAuthenticatedSoapException_Exception e) {

				if ( tryToReAuthenticate(url) ) {
					
					memberTagInformationAr = getMemberTagInformation(url);
					
				}
			} 
						
		}
		
		return memberTagInformationAr;
		
	}
	
	public static String[] validateUsers(String url, String[] userNamesToValidateAr) throws PafException, PafNotAbletoGetLDAPContext_Exception {

		String[] invalidUserNamesWithDomainNameAr = null;
		
		if ( isAuthenticated(url) && userNamesToValidateAr != null ) {
								
			ServerSession serverSession = getSession(url);
			
			PafVerifyUsersRequest request = new PafVerifyUsersRequest();
			
			request.setClientId(serverSession.getClientId());
			request.setSessionToken(serverSession.getSecurityToken());
			request.getUsers().addAll(Arrays.asList(userNamesToValidateAr));
			
//			web service
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ) {
				try {
					PafVerifyUsersResponse response = service.verifyUsers(request);
					
					if (response != null && response.getUsers().size() > 0 ) {
						
						invalidUserNamesWithDomainNameAr = response.getUsers().toArray(new String[0]);
						
					}
					
				} catch (PafSoapException_Exception e) {
	
					logger.error(e.getMessage());
					
					throw new PafException(e);
					
				} catch (PafNotAuthorizedSoapException_Exception e) {
					
					handleNotAuthorizedSoapException();
					
				} catch (PafNotAuthenticatedSoapException_Exception e) {
	
					if ( tryToReAuthenticate(url) ) {
						
						invalidUserNamesWithDomainNameAr = validateUsers(url, userNamesToValidateAr);
						
					}
					
				} catch (PafNotAbletoGetLDAPContext_Exception e) {
	
					logger.warn("LDAP Context could not be created. Only pace users will be used.");
					
					throw e;
					
				}
			}
		}
		
		return invalidUserNamesWithDomainNameAr;
		
	}
	
	/**
	 * 
	 *	Method_description_goes_here
	 *
	 */
	private static void handleNotAuthorizedSoapException() {

//		 tell display to run job next time possible
		Display.getDefault().asyncExec(new Runnable() {

			public void run() {

				GUIUtil.openMessageWindow(Constants.DIALOG_ERROR_HEADING, "Not authorized to perform action on server", MessageDialog.ERROR);
				
			
			}

		});
		
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 */
	private static void handleRemoteException() {
			
		logger.error("Remote Exception occurred; Refreshing server info.");
		
		RefreshServersAction refreshServerAction = new RefreshServersAction("", ServerView.getViewer());
		refreshServerAction.run();
	}
	
	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param url
	 * @return
	 */
	public static boolean isAdmin(String url) {
		
		boolean isAdmin = false;
		
		if ( url != null && SecurityManager.isAuthenticated(url)) {
//			if ( url != null && WebServicesUtil.isServerRunning(url) && SecurityManager.isAuthenticated(url)) {
					
			ServerSession serverSession = getSession(url);
			
			if ( serverSession != null ) {
				
				isAdmin = serverSession.isAdmin();
				
			}
			
		}		
		
		return isAdmin;
		
	}

	public static PafSimpleDimTree getFilteredPafSimpleDimTree(String url, String dimensionName, List<String> filterSpec) throws RemoteException, PafException {
		PafSimpleDimTree pafSimpleDimTree = null;

		try {
							
			ServerSession serverSession = getSession(url);
			
			PafTreeRequest pafTreeRequest = new PafTreeRequest();
								
			pafTreeRequest.setClientId(serverSession.getClientId());

			pafTreeRequest.setTreeName(dimensionName);
			
			pafTreeRequest.setCompressResponse(true);
			
			if(filterSpec != null && filterSpec.size() > 0){
				PafDimSpec pafDimSpec = new PafDimSpec();
				pafDimSpec.setDimension(dimensionName);
				pafDimSpec.getExpressionList().addAll(filterSpec);
				pafTreeRequest.setFilterSpecification(pafDimSpec);
			}
			
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ){
				// get simple tree from web service call
				PafTreeResponse pafTreeResponse = service.getSimpleMdbTree(pafTreeRequest);
	
				if (pafTreeResponse != null) {
	
					pafSimpleDimTree = pafTreeResponse.getPafSimpleDimTree();
					
					if ( pafSimpleDimTree != null && pafSimpleDimTree.isCompressed()) {
						
						pafSimpleDimTree = TreeUncompressUtil.uncompressTree(pafSimpleDimTree);
						
					}
				}
			}	
			

		} catch (PafSoapException_Exception e) {

			logger.error(e.getMessage());
			
			throw new PafException(e);
							
		}
		return pafSimpleDimTree;
	}
	
	public static String getPreviousUser() {
		return previousUser;
	}

	public static void setPreviousUser(String prevUser) {
		previousUser = prevUser;
	}
	
		
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.exceptions;

/**
 * Simple class to flag when a server is not running
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class ServerNotRunningException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2407327731936471276L;

	/**
	 * Class constructor.
	 */
	public ServerNotRunningException() {
		super();
	}

	/**
	 * Class constructor specifying an exception message and
	 * a throwable cause.
	 * @param message Exception message
	 * @param cause Throwable cause
	 */
	public ServerNotRunningException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor specifying an exception message.
	 * @param message Exception message
	 */
	public ServerNotRunningException(String message) {
		super(message);
	}

	/**
	 * Class constructor specifying a throwable cause.
	 * @param cause throwable cause
	 */
	public ServerNotRunningException(Throwable cause) {
		super(cause);
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

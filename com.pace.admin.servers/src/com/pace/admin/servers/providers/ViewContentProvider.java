/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.pace.admin.servers.nodes.TreeNode;

/**
 * The content provider class is responsible for providing objects to the view.
 * It can wrap existing objects in adapters or simply return objects as-is.
 * These objects may be sensitive to the current input of the view, or ignore it
 * and always show the same content (like Task List, for example).
 */
public class ViewContentProvider implements IStructuredContentProvider,
		ITreeContentProvider {

	private static Logger logger = Logger.getLogger(ViewContentProvider.class);

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {

		logger.debug("Input changed on viewer: " + v + ", Old: " + oldInput
				+ ", New: " + newInput);

	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		
		return getChildren(parent);
		
	}

	public Object getParent(Object child) {

		Object parent = null;

		if (child instanceof TreeNode) {
			
			parent = ((TreeNode) child).getParent();
			
		}

		return parent;
	}

	public Object[] getChildren(Object parent) {

		return ((TreeNode) parent).getChildren().toArray();

	}

	public boolean hasChildren(Object parent) {

		boolean hasChildren = false;

		if (parent instanceof TreeNode) {
			hasChildren = ((TreeNode) parent).hasChildren();
		}

		return hasChildren;
	}
}

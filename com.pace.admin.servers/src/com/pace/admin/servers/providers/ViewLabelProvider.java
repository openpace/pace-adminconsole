/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.ServersPlugin;
import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.DBUserNode;
import com.pace.admin.servers.nodes.DBUsersNode;
import com.pace.admin.servers.nodes.DomainNode;
import com.pace.admin.servers.nodes.GroupNode;
import com.pace.admin.servers.nodes.InfoNode;
import com.pace.admin.servers.nodes.LDAPUserNode;
import com.pace.admin.servers.nodes.NativeUsersNode;
import com.pace.admin.servers.nodes.NotAuthenticatedNode;
import com.pace.admin.servers.nodes.SecurityGroupNode;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.nodes.ServerInfoNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.base.ui.PafServer;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.RunningState;

public class ViewLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	public String getColumnText(Object obj, int index) {
		return getText(obj);
	}

	public Image getColumnImage(Object obj, int index) {
		return getImage(obj);
	}

	public String getText(Object obj) {
		if (obj instanceof ApplicationNode) {
			String url = ((ApplicationNode)obj).getServer().getCompleteWSDLService();
			PafServerAck pafServerAck = WebServicesUtil.getPafServerAck(url);
			if( pafServerAck != null ) {
				ApplicationState appState = ApplicationNode.getApplicationState(((ApplicationNode)obj).getServer());
				if ( appState != null) {
					String appId = appState.getApplicationId();
					RunningState runState = appState.getCurrentRunState();
					return appId + " (" + runState + ")";
				}
			}
		} else if(obj instanceof ServerNode){
			
			ServerNode serverNode = (ServerNode) obj;
			if(serverNode != null){
				PafServer pafServer = serverNode.getServer();
				if(pafServer != null && pafServer.isDisabled()){
					return pafServer.getName() + Constants.DISABLED_SERVER_NODE_TEXT;
				}
			}
			
		}
		return obj.toString();
	}

	public Image getImage(Object obj) {

		URL imageURL = null;

		if (obj instanceof ServerFolderNode) {
			imageURL = ServersPlugin.getDefault().getBundle().getEntry(
					"/icons/attrdefq.gif");
		} else if ( obj instanceof ServerNode ){

			ServerNode serverNode = (ServerNode) obj;

			PafServer pafServer = null;
			try {
				pafServer = PafServerUtil.getServer(serverNode.getName());
			} catch (PafServerNotFound e) {
				e.printStackTrace();
			}

			if (pafServer != null) {

				if (pafServer.isDisabled()){
					imageURL = ServersPlugin.getDefault().getBundle().getEntry("/icons/server.gif");
				} else if (ServerMonitor.getInstance().isServerRunning(pafServer)) {
					if( pafServer.isDefaultServer()) {
						imageURL = ServersPlugin.getDefault().getBundle().getEntry(
								"/icons/defaultserverrunning.gif");
					}
					else {
						imageURL = ServersPlugin.getDefault().getBundle().getEntry(
								"/icons/serverrunning.gif");
					}
				} else {
					if( pafServer.isDefaultServer()) {
						imageURL = ServersPlugin.getDefault().getBundle().getEntry(
							"/icons/defaultservernotrunning.gif");
					}else {
						imageURL = ServersPlugin.getDefault().getBundle().getEntry(
							"/icons/servernotrunning.gif");
					}
				}

			} else {

				imageURL = ServersPlugin.getDefault().getBundle().getEntry(
						"/icons/servernotrunning.gif");
			}

		} else if ( obj instanceof DBUsersNode) {
			
			imageURL = ServersPlugin.getDefault().getBundle().getEntry(
			"/icons/Color.png");
				
		} else if (obj instanceof DBUserNode || obj instanceof NotAuthenticatedNode || obj instanceof LDAPUserNode ) {

			imageURL = ServersPlugin.getDefault().getBundle().getEntry("/icons/user.png");
			
		} else if ( obj instanceof ServerInfoNode  ) {
			
			//return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_INFO_TSK);
			imageURL = ServersPlugin.getDefault().getBundle().getEntry("/icons/information.png");
						
		} else if(obj instanceof ApplicationNode){
			return PlatformUI.getWorkbench().getSharedImages().getImage(org.eclipse.ui.ide.IDE.SharedImages.IMG_OBJ_PROJECT);
		} else if ( obj instanceof InfoNode) {
			
			imageURL = ServersPlugin.getDefault().getBundle().getEntry(
					"/icons/document_information.png");
//			"/icons/attrdef.gif");
			
		} else if ( obj instanceof NativeUsersNode || obj instanceof SecurityGroupNode ) {
			
			imageURL = ServersPlugin.getDefault().getBundle().getEntry(
			"/icons/user-group.ico");
			
		} else if ( obj instanceof DomainNode ) {
			
			imageURL = ServersPlugin.getDefault().getBundle().getEntry(
			"/icons/domain.gif");
			
		}else if ( obj instanceof GroupNode) {
			
			imageURL = ServersPlugin.getDefault().getBundle().getEntry("/icons/network.png");
			//return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
			
		}

		ImageDescriptor descriptor = ImageDescriptor.createFromURL(imageURL);

		return descriptor.createImage();
	}
}

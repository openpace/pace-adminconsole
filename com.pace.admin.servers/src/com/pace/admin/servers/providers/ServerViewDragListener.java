/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

import com.pace.admin.servers.nodes.ServerNode;

public class ServerViewDragListener implements DragSourceListener {

	private static final Logger logger = Logger.getLogger(ServerViewDragListener.class);
	private final TreeViewer viewer;

	public ServerViewDragListener(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		logger.debug("Finshed Drag");
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		//Get the drag selection
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		//Conver it to an object and see what type of node it is
		Object node = selection.getFirstElement();
		if ( node instanceof ServerNode ) {
			ServerNode serverNode = (ServerNode) node;
			event.data = serverNode.getServer().getName();
		} else {
			event.data = null;
			event.doit = false;
		}
	}

	@Override
	public void dragStart(DragSourceEvent event) {
		logger.debug("Start Drag");
		event.doit = isNodeValidForDrag((IStructuredSelection) viewer.getSelection());
	}
	
	private boolean isNodeValidForDrag(IStructuredSelection selection){
		Object node = selection.getFirstElement();
		if ( node instanceof ServerNode ) {
			return true;
		} else {
			return false;
		}
	}
}

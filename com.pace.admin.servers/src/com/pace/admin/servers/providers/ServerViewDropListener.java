/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.PlatformUI;

import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.servers.nodes.GroupNode;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;

public class ServerViewDropListener extends ViewerDropAdapter {
	
	private static final Logger logger = Logger.getLogger(ServerViewDropListener.class);
	private final TreeViewer viewer;

	public ServerViewDropListener(TreeViewer viewer) {
		super(viewer);
		this.viewer = viewer;
	}

	// This method performs the actual drop
	// We simply add the String we receive to the model and trigger a refresh of the 
	// viewer by calling its setInput method.
	@Override
	public boolean performDrop(Object data) {
		Object target = (Object) getCurrentTarget();
		String serverGroup = target.toString().trim();
		String serverName = data.toString().trim();
		
		try {

			if(okToDrop(serverGroup, serverName)){
				//User is updating the group, so log, update and return true
				logger.info("Moving server: " + serverName + ", to server group: " + serverGroup);
				PafServer pafServer = PafServerUtil.getServer(serverName);
				pafServer.setServerGroupName(serverGroup);
				
				//Finally refresh the viewer.
				BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
					new Runnable() {
						public void run() {
							PafServerUtil.saveServersToXml();
							viewer.setInput(ServerView.refreshServerModel(true));
							viewer.expandToLevel(4);
						}
				});
				
				return true;
				
			}
			return false;
			
		} catch (PafServerNotFound e) {
			logger.warn(e.getMessage());
			return false;
		}
		
	}

	private boolean okToDrop(String serverGroup, String serverName){
		try {
			boolean sameGroup = false;
			//Get the server to be modified.
			PafServer pafServer = PafServerUtil.getServer(serverName);
			//Check to see if the server has a group assigned.
			if (pafServer.getServerGroupName() !=null &&  !pafServer.getServerGroupName().isEmpty()){
				if(pafServer.getServerGroupName().equals(serverGroup)){
					sameGroup = true;
				}
			} else { //Server has no group assigned.  Make sure user is not dragging to same group
				if(serverGroup.equalsIgnoreCase(GroupNode.BLANK_GROUP_NAME)){
					sameGroup = true;
				}
			}
			//If the user is dragging to same group, log it and return false.
			if(sameGroup){
				logger.debug("Server: " + serverName + ", already belongs to server group: " + serverGroup);
				return false;
			}

		} catch (PafServerNotFound e) {
			logger.warn(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	@Override
	public void dragOver(final DropTargetEvent event) {
		super.dragOver(event);

		try {
			if (! (getCurrentTarget() instanceof GroupNode) ) {
				event.detail = DND.DROP_NONE;
				event.feedback = DND.FEEDBACK_NONE;
			} else if(getCurrentLocation() != LOCATION_ON){
				event.detail = DND.DROP_NONE;
				event.feedback = DND.FEEDBACK_NONE;		
			} else if ((getCurrentTarget() instanceof GroupNode) ) {
				Object target = (Object) getCurrentTarget();
				Object server = getSelectedObject();
				if(target != null && server != null){
					String serverGroup = target.toString().trim();
					String serverName = server.toString().trim();
					if(okToDrop(serverGroup, serverName)){
						return;
					}	
				}
				event.detail = DND.DROP_NONE;
				event.feedback = DND.FEEDBACK_NONE;		
				
			}
		} catch(Exception e){
			logger.warn(e.getMessage());
			event.detail = DND.DROP_NONE;
			event.feedback = DND.FEEDBACK_NONE;		
		}
	}
	 
	
	@Override
	public boolean validateDrop(Object target, int operation,
			TransferData transferType) {

		if ( target instanceof GroupNode ) {
			return true;
		} else {
			return false;
		}
	}
} 
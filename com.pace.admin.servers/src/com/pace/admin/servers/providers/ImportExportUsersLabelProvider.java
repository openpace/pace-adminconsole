/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.pace.server.client.PafUserDef;

/**
 * Label provider for the import/export dialog for importing and exporting users from/to a CSV file
 * @author fskrgic
 *
 */
public class ImportExportUsersLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	/**
	 * Overrides the LabelProvider method
	 */
	public Image getColumnImage(Object element, int columnIndex) {

		return null;
		
	}

	/**
	 * Overrides the LabelProvider method
	 */
	public String getColumnText(Object element, int columnIndex) {
		
		// If element is of user type
		if(element instanceof PafUserDef){
			
			// Cast the element to a user
			PafUserDef user = (PafUserDef) element;
			
			// Depending on what column is being filled return the appropriate user field
			if(columnIndex == 0){
				
				return user.getUserName()==null?"":user.getUserName();
				
			}
			
			if(columnIndex == 1){
				
				return "******";
				
			}
			
			if(columnIndex == 2){
				
				return user.getEmail()==null?"":user.getEmail();
				
			}
			if(columnIndex == 3){
				
				return user.getFirstName()==null?"":user.getFirstName();
				
			}
			
			if(columnIndex == 4){
				
				return user.getLastName()==null?"":user.getLastName();
				
			}
			
			if(columnIndex == 5){
				
				return ( user.isAdmin() ? "True" : "False" );
				
			}
			
			if(columnIndex == 6){
				
				return ( user.isChangePassword() ? "True" : "False" );
				
			}
		}
		
		return null;
		
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.providers;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * The content provider for the import/export dialog for importing and exporting users from/to a CSV file
 * @author fskrgic
 *
 */
public class DeleteUsersContentProvider implements IStructuredContentProvider {
	
	/**
	 * Implements IStructuredContentProvider interface
	 */
	public Object[] getElements(Object inputElement) {
		
		//Cast to an array of users and return
		return (String []) inputElement;
		
	}

	/**
	 * Cleans up
	 */
	public void dispose() {
		
	}

	/**
	 * Implements IStructuredContentProvider interface
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		
	}
}

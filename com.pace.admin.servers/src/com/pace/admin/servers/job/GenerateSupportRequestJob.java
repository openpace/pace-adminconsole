/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.job;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.progress.IProgressConstants;

import com.pace.admin.servers.ServersPlugin;
import com.pace.admin.servers.actions.GenerateSupportRequestCompletedAction;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.jira.JiraClient;
import com.pace.base.jira.JiraIssue;
import com.pace.base.jira.JiraIssuePriority;
import com.pace.base.jira.JiraIssuePriority.IssuePriority;
import com.pace.base.jira.JiraIssueType;
import com.pace.base.jira.JiraIssueType.IssueType;
import com.pace.base.jira.JiraProject;
import com.pace.base.jira.JiraProjectVersion;
import com.pace.base.jira.JiraResponse;
import com.pace.base.jira.JiraUtil;
import com.pace.base.jira.JiraVersion;
import com.pace.base.utility.PafZipUtil;

public class GenerateSupportRequestJob extends Job {

	private static Logger logger = Logger.getLogger(GenerateSupportRequestJob.class);
	public final static String ID = "com.pace.admin.servers.action.GenerateSupportRequestJob";
	private static final String JOB_NAME = "Generate Issue Resolution Files";
	private Shell parentShell;
	private String serverVersion;
	private String fullName;
	private String customerName;
	private String email;
	private String phone;
	private String summary;
	private String description;
	private String environment;
	private String priority;
	private JiraResponse jiraResponse; 
	private Map<String, File> logFiles;
	private File tempDirectory;
	private GenerateSupportRequestCompletedAction completedAction;
	
	/**
	 * 
	 * @param parentShell Shell Object
	 * @param serverVersion Current Pace Server Version
	 * @param fullName Users full name
	 * @param email Users email address
	 * @param customerName Users customer
	 * @param phone Users phone number
	 * @param summary Summary of the Jira issue
	 * @param description Jira issue description
	 * @param priority User defined issue priority
	 * @param environment Enviorment information
	 * @param tempDirectory Temp directory where log files exist
	 * @param logFiles Map of <FileName, FullFileNameWithPath>
	 */
	public GenerateSupportRequestJob(Shell parentShell, 
			String serverVersion, String fullName, String email, 
			String customerName, String phone, String summary, String description, String priority,
			String environment, File tempDirectory, Map<String, File> logFiles) {
		super(JOB_NAME);
	
		this.parentShell = parentShell;
		this.serverVersion = serverVersion;
		this.fullName = fullName;
		this.email = email;
		this.customerName = customerName;
		this.phone = phone;
		this.summary = summary;
		this.description = description;
		this.environment = environment;
		this.priority = priority;
		this.tempDirectory = tempDirectory;
		this.logFiles = logFiles;
		
		this.completedAction = new GenerateSupportRequestCompletedAction(ServersPlugin.getDefault().getBundle().getSymbolicName(), this.parentShell);
		completedAction.setOkTitle("Support Ticket Created");
		completedAction.setFailMsg("There was a problem creating the support request.  Please check the log file.");
		completedAction.setFailTitle("Oops");
		completedAction.setShowDialog(true);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		monitor.beginTask(this.getName(), IProgressMonitor.UNKNOWN);
		
		File zipFile = null;
		try {
			
			JiraVersion[] jiraVersion = null;
			
			//Get the list of product version from the JIRA system.
			Collection<JiraProjectVersion> versions = JiraUtil.getJiraVersions();
			//See if the current AC version is found in the list of versions.  If not leave it blank.
			for(JiraProjectVersion version : versions){
				if(version.getName().equalsIgnoreCase(serverVersion)){
					jiraVersion = new JiraVersion[] { new JiraVersion (serverVersion) };
					break;
				}
			}
			
			//Create a JIRA issue object used to create the JIRA issue.
			JiraIssue issue = new JiraIssue(new JiraProject(PafBaseConstants.JIRA_DEFAULT_PROJECT), 
					summary,
					description,
					new JiraIssueType(IssueType.Bug),
					new JiraIssuePriority(IssuePriority.valueOf(priority)),
					jiraVersion,
					environment,
					String.format("%s - %s - %s - %s", new Object[] { fullName, email, customerName, phone }),
					new JiraClient(PafBaseConstants.JIRA_DEFAULT_CLIENT));
			
			//Create the initial JIRA issue.
			jiraResponse = JiraUtil.createJiraIssue(issue);
			
			//Zip up the list of files and attach it to the ticket.
			if(logFiles != null && logFiles.size() > 0){
				//Get the list of files from the supplied map
				List<File> files = new ArrayList<File>(logFiles.size());
				for(Map.Entry<String, File> entry : logFiles.entrySet() ){
					files.add(entry.getValue());
				}
				
				//Create the zip from the temp directory
				try {
					zipFile = PafZipUtil.zipFiles(files);
				} catch (IOException e) {
					
					logger.error(e.getMessage());
					throw new PafException(e);
				}
				
				//Add the set of attachments to the already created JIRA issue.
				JiraUtil.addAttachmentToIssue(jiraResponse.getKey(), zipFile);
			}
			
			//We're good so tell the user.
			completedAction.setOkMsg("When contacting Pace Support please reference ticket number: " + jiraResponse.getKey() + ".");
			completedAction.setOk(true);

		} catch (Exception e) {
			
			logger.error(e);
			completedAction.setThrowable(e);
			completedAction.setOk(false);
			
		} finally {
			//Purge all the leftover files and dirs
			if(tempDirectory != null){
				org.apache.commons.io.FileUtils.deleteQuietly(tempDirectory);
			}
			if(zipFile != null){
				org.apache.commons.io.FileUtils.deleteQuietly(zipFile);
			}
		}
		
		monitor.done();
		complete();
		return Status.OK_STATUS;
	}
	
	protected void complete(){
		if(completedAction.isOk()){
			setProperty(IProgressConstants.ICON_PROPERTY, ServersPlugin.getImageDescriptor("/icons/check.png"));
		} else {
			setProperty(IProgressConstants.ICON_PROPERTY, ServersPlugin.getImageDescriptor("/icons/cross.png"));
		}
		Boolean isModal = (Boolean) this.getProperty(IProgressConstants.PROPERTY_IN_DIALOG);
		//if (isModal != null && isModal.booleanValue()) {
		if (isModal != null && isModal.booleanValue() && completedAction != null && completedAction.isShowDialog()) {
			// The progress dialog is still open so
			// just open the message
			showResults(completedAction);
		} else {
			setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
			setProperty(IProgressConstants.ACTION_PROPERTY, completedAction);
		}
	}
	
	protected void showResults(final GenerateSupportRequestCompletedAction action){
		Display.getDefault().syncExec(new Runnable() {
			public void run(){
				action.run();
			}
		});
	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers;

import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.springframework.util.StopWatch;

import com.pace.admin.global.jobs.ServerRefreshJob;
import com.pace.admin.global.server.ServerManager;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.ui.PafServer;

/**
 * The main plugin class to be used in the desktop.
 */
public class ServersPlugin extends AbstractUIPlugin {

	private static Logger logger = Logger.getLogger(ServersPlugin.class);
	
	// The shared instance.
	private static ServersPlugin plugin;

	private Job serverRefreshJob;
	
	//private final long STARTUP_DELAY = 10000; // 10 seconds delay for first run
	private final long STARTUP_DELAY = 1000; // 1 seconds delay for first run
    private final long JOB_INTERVAL = 15000; // Job should run every 15 seconds
	
	/**
	 * The constructor.
	 */
	public ServersPlugin() {
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		StopWatch sw = new StopWatch();
		sw.start();
		
		super.start(context);
		
		PafServerUtil.loadPafServers();
		WebServicesUtil.loadPafService();
		
		Set<PafServer> initialServerSet = PafServerUtil.getPafServers();
		if ( initialServerSet != null ) {
			
			for (PafServer pafServer : initialServerSet ) {
				
				ServerMonitor.getInstance().addObserver(ServerManager.getInstance());
				//ServerMonitor.getInstance().addServer(pafServer);
				//TTN-2527 Removed code above with this.
				ServerMonitor.getInstance().initServer(pafServer);
			}
		}
		
	    serverRefreshJob = new ServerRefreshJob();
        serverRefreshJob.schedule(STARTUP_DELAY);
        serverRefreshJob.addJobChangeListener(new JobChangeAdapter() {
            @Override
            public void done(IJobChangeEvent event) {
                super.done(event);
                serverRefreshJob.schedule(JOB_INTERVAL);
            }
        });
        
        sw.stop();
        logger.info(String.format("Took %s (ms) to load Server plugin.", sw.getTotalTimeMillis()));
        
	}
	

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		serverRefreshJob.cancel();
		
		/*
		Set<PafServer> pafServerSet = PafServerUtil.getPafServers();
		
		if ( pafServerSet != null && pafServerSet.size() > 0 ) {
			
			for (PafServer pafServer : pafServerSet ) {
				
				if ( WebServicesUtil.isServerRunning(pafServer.getCompleteWSDLService())) {
					
					MessageDialog.openInformation(ServersPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getShell(),
							"Warning", "Server '" + pafServer.getName() + "' is still running.");
					
				}				
				
			}
			
			
		}
		*/
		
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 */
	public static ServersPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path.
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(
				"com.pace.admin.servers", path);
	}
	@SuppressWarnings("deprecation")
	public ServerView getServerView() {
		
		IViewPart[] views = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViews();
		
		for (IViewPart view : views) {
			
			if ( view instanceof ServerView) {
				
				return (ServerView) view;
				
			}
			
		}

		return null;
	}
}

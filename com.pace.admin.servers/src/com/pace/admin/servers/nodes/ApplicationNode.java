/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.nodes;

import java.util.List;

import org.apache.log4j.Logger;

import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.WebServicesUtil;
import com.pace.base.ui.PafServer;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.ApplicationStateRequest;
import com.pace.server.client.ApplicationStateResponse;
import com.pace.server.client.PafServerAck;
import com.pace.server.client.PafService;
import com.pace.server.client.PafSoapException_Exception;

public class ApplicationNode extends TreeNode {
	private static Logger logger = Logger.getLogger(ApplicationNode.class);

	public ApplicationNode(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public ApplicationNode(String name, TreeNode parent, PafServer server) {
		super(name, parent, server);
	}

	public static ApplicationState getApplicationState(PafServer pafServer) {
		return ServerMonitor.getInstance().getApplicationState(pafServer);
	}
	
	public static ApplicationState getApplicationState(PafServerAck pafServerAck, String url) {
		if( pafServerAck != null ) {
			PafService service = WebServicesUtil.getPafService(url);
			if( service != null ) {
				ApplicationStateRequest appStateRequest = new ApplicationStateRequest();
				appStateRequest.setClientId(pafServerAck.getClientId());
				try {
					ApplicationStateResponse appStateResponse = service.getApplicationState(appStateRequest);
					if( appStateResponse != null ) {
						List<ApplicationState> listAppStates = appStateResponse.getAppStates();
						if( listAppStates != null && listAppStates.size() > 0 )
							return listAppStates.get(0);
					}
				} catch (PafSoapException_Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
			}
		}
		return null;
	}
	
}
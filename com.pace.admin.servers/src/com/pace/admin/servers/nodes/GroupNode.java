/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.nodes;

import com.pace.admin.global.constants.Constants;
import com.pace.base.ui.PafServer;

public class GroupNode extends TreeNode {

	public static final String BLANK_GROUP_NAME = Constants.NO_GROUP_NODE_NAME;
	private boolean isBlank;

	/**
	 * Initializes a blank group node
	 * @param parent
	 * @param server 
	 */
	public GroupNode(TreeNode parent, PafServer server) {
		this(BLANK_GROUP_NAME, parent, server, true);
	}

	/**
	 * Constructor.
	 * @param name
	 * @param parent
	 * @param server
	 */
	public GroupNode(String name, TreeNode parent, PafServer server) {
		this(name, parent, server, false);
	}

	private GroupNode(String name, TreeNode parent, PafServer server, boolean isBlank){
		super(name, parent, server);
		this.isBlank = isBlank;
	}
		
	public boolean isBlank() {
		return isBlank;
	}

	public void setBlank(boolean isBlank) {
		if(isBlank) {
			this.setName(BLANK_GROUP_NAME);
		}
		this.isBlank = isBlank;
	}
}

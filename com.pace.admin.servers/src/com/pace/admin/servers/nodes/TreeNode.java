/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.nodes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Image;

import com.pace.admin.global.interfaces.ITreeNode;
import com.pace.base.ui.PafServer;


public class TreeNode implements ITreeNode {

		private String name;

		private TreeNode parent;
		
		private PafServer server;
		
		protected List<TreeNode> children;

		public TreeNode(String name) {
			this.name = name;
			children = new ArrayList<TreeNode>();
		}
		
		public TreeNode(String name, TreeNode parent, PafServer server) {
			this(name);
			this.parent = parent;
			this.server = server;
		}

		/**
		 * Called by the framework to build the child nodes.  
		 * In general one should not call this directly.
		 * @param children
		 */
		public void createChildren(List<TreeNode> children) {
			
			clearChildren();
			
			if ( children != null ) {
				
				getChildren().addAll(children);
			
			}
			
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setParent(TreeNode parent) {
			this.parent = parent;
		}

		public TreeNode getParent() {
			return parent;
		}

		public String toString() {
			return getName();
		}
	
		public void addChild(TreeNode child) {
									
			getChildren().add(child);
			
			child.setParent(this);
		}

		public void removeChild(TreeNode child) {
			
			child.setParent(null);
			
			getChildren().remove(child);
			
		}
		
		public boolean hasChildren() {
			
			return (getChildren().size() > 0);
			
		}

		public List<TreeNode> getChildren() {
			
			if ( children == null ) {
				
				children = new ArrayList<TreeNode>();
			}
			
			return children;						
		}

		public Image getImage() {
			// TODO Auto-generated method stub
			return null;
		}
	
		public void clearChildren() {
			
			getChildren().clear();
			
		}

		/**
		 * @return the server
		 */
		public PafServer getServer() {
			return server;
		}

		/**
		 * @param server the server to set
		 */
		public void setServer(PafServer server) {
			this.server = server;
		}
	
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.servers.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.pace.admin.servers.nodes.ApplicationNode;
import com.pace.admin.servers.nodes.ServerFolderNode;
import com.pace.admin.servers.nodes.ServerNode;
import com.pace.admin.servers.nodes.TreeNode;
import com.pace.base.ui.PafServer;

/**
 * @author JMilliron
 *
 */
public class TreeNodeUtil {
	
	/**
	 * Find all application nodes for server node.
	 * 
	 * @param serverNode start node
	 * @return
	 */
	public static Set<ApplicationNode> findApplicationNode(ServerNode serverNode) {
		
		Set<ApplicationNode> appNodeSet = new HashSet<ApplicationNode>();
		
		if ( serverNode != null ) {
			
			if ( serverNode.hasChildren()) {
				
				List<TreeNode> childrenNodes = serverNode.getChildren();
				
				if ( childrenNodes != null ) {
					
					for (TreeNode childNode : childrenNodes) {
						
						if ( childNode instanceof ApplicationNode) {
							
							appNodeSet.add((ApplicationNode) childNode);
							
						}
						
					}
					
				}
				
			}
			
		}
		
		return appNodeSet;
		
	}
	
	/**
	 * Find all server nodes for server folder node.
	 * 
	 * @param serverFolderNode start node
	 * @return
	 */
	public static ServerNode findServerNode(List<TreeNode> groupNodes, PafServer server) {
		
		ServerNode serverNode = null;
		
		for(TreeNode groupNode : groupNodes){
		
			if ( groupNode != null && server != null ) {
				
				if ( groupNode.hasChildren()) {
					
					List<TreeNode> childrenNodes = groupNode.getChildren();
					
					if ( childrenNodes != null ) {
						
						for (TreeNode childNode : childrenNodes) {
							
							if ( childNode instanceof ServerNode && ((ServerNode) childNode).getServer().equals(server)) {
								
								serverNode = ((ServerNode) childNode);
								
								break;
								
							}
							
						}
						
					}
					
				}
				
			}
		}
		return serverNode;
		
	}
	
	/**
	 * Find all server nodes for server folder node.
	 * 
	 * @param serverFolderNode start node
	 * @return
	 */
	public static ServerNode findServerNode(ServerFolderNode serverFolderNode, PafServer server) {
		
		ServerNode serverNode = null;
		
		if ( serverFolderNode != null && server != null ) {
			
			if ( serverFolderNode.hasChildren()) {
				
				List<TreeNode> childrenNodes = serverFolderNode.getChildren();
				
				if ( childrenNodes != null ) {
					
					for (TreeNode childNode : childrenNodes) {
						
						if ( childNode instanceof ServerNode && ((ServerNode) childNode).getServer().equals(server)) {
							
							serverNode = ((ServerNode) childNode);
							
							break;
							
						}
						
					}
					
				}
				
			}
			
		}
		
		return serverNode;
		
	}

}

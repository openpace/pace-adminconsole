/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.servers.utils;

import java.io.File;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.springframework.util.StopWatch;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.constants.ErrorConstants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.server.ServerMonitor;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.global.util.PafServerUtil;
import com.pace.admin.global.util.PafSimpleDimTreeUtil;
import com.pace.admin.global.util.SystemUtil;
import com.pace.admin.servers.exceptions.DimensionNotFoundException;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.security.SecurityManager;
import com.pace.admin.servers.views.ServerView;
import com.pace.base.PafException;
import com.pace.base.mdb.PafSimpleAttributeTree;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PafXStream;
import com.pace.server.client.ApplicationState;
import com.pace.server.client.PafMdbProps;
import com.pace.server.client.PafSimpleDimMember;
import com.pace.server.client.PafSimpleDimTree;
import com.pace.server.client.PafSoapException_Exception;

/**
 * Utility to cache dimension tree via server/application name local. This
 * allows the admin console to work in a disconnected way
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class DimensionTreeUtility {

	// logger
	private static Logger logger = Logger.getLogger(DimensionTreeUtility.class);

	/**
	 * gets the simple dimension tree from cache for project, app name
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @return simple paf tree
	 * @throws Exception
	 */
	public static PafSimpleDimTree getDimensionTree(IProject iProject,
			String projectApplicationName, String dimensionName)
			throws Exception {
		
		StopWatch sw = new StopWatch();
		if(logger.isDebugEnabled()) sw.start();
		
		PafSimpleDimTree tree = getDimensionTree(iProject, projectApplicationName,
				dimensionName, false);
		
		if(logger.isDebugEnabled()) sw.stop();
		if(logger.isDebugEnabled()) logger.debug(String.format("getDimensionTree for dimension %s took %s ms", dimensionName, sw.getTotalTimeMillis()));
		
		return tree;
		
	}

	/**
	 * gets the simple dimension tree from cache for project, app name but
	 * forces a refresh from server
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @param forceRefreshFromServer
	 *            forces a refresh from the server
	 * @return simple paf tree
	 * @throws Exception
	 */
	public static PafSimpleDimTree getDimensionTree(IProject iProject,
			String projectApplicationName, String dimensionName,
			boolean forceRefreshFromServer) throws Exception {

		// try to get already cached dim tree
		PafSimpleDimTree simpleTree = getCachedDimensionTree(iProject,
				projectApplicationName, dimensionName);

		// if not already cached or user wants a forced refresh from server
		// call service to get dimension tree and cache it
		if (simpleTree == null || forceRefreshFromServer) {

			// get project server for project
			final PafServer projectServer = getPafServerForProject(iProject);
			
			if( projectServer == null ) {
				throw new Exception("No default server nor paf server found.");
			}
			String url = projectServer.getCompleteWSDLService();
			
			if( url != null ){
				ServerView.authWithLoginDialogIfNotAuthed(projectServer, false);
				
				if ( SecurityManager.isAuthenticated(url) ) {
	
					// check to see if server is running
					//PafService service = WebServicesUtil.getPafService(projectServer.getCompleteWSDLService());
			
						/*PafTreeRequest pafTreeRequest = new PafTreeRequest();
	
						PafServerAck pafServerAck = WebServicesUtil
								.getPafServerAck(projectServer
										.getCompleteWSDLService());
											
						pafTreeRequest.setClientId(pafServerAck.getClientId());
	
						pafTreeRequest.setTreeName(dimensionName);
						
						pafTreeRequest.setCompressResponse(true);
	
						// get simple tree from web service call
						PafTreeResponse pafTreeResponse = service
								.getDimensionTree(pafTreeRequest);
	
						if (pafTreeResponse != null) {
	
							simpleTree = pafTreeResponse.getPafSimpleDimTree();
							
							if ( simpleTree != null && simpleTree.isCompressed()) {
								
								simpleTree = TreeUncompressUtil.uncompressTree(simpleTree);
								
							}
	
						}*/
						
						//call service to get dimension tree
					try {
						
						simpleTree = SecurityManager.getPafSimpleDimTree(url,dimensionName);
						
						// if tree is returned, cache it
						if (simpleTree != null) {
	
							setCachedDimensionTree(iProject,
									projectApplicationName, dimensionName,
									simpleTree);
	
						}
						// uh-oh, exception was thrown
					} catch (RemoteException e) {
						String errorMessage = "Could not connect to server '"
							+ projectServer.getName() + "' located at '"
							+ projectServer.getCompleteWSDLService() + "'." + e.getMessage();
	
						// log error message
						logger.error(errorMessage);
		
						// throw new exception
						throw new ServerNotRunningException(errorMessage);
					} catch (PafException e) {
							// log error message
						logger.error("PafException occurred: " + e.getMessage());
	
						// re throw exception
						throw e;
	
					} catch (RuntimeException e) {
							// log error message
						String errorMessage = String.format(
								ErrorConstants.INVALID_DIMENSION,
								new Object[] { dimensionName });

						// log error
						logger.error(errorMessage);

						// throw new exception
						throw new DimensionNotFoundException(errorMessage);
	
					} catch (Exception e) {
							// log error message
						// if null pointer is thrown, get error message, logg it and
						// throw new dim not found exc
						logger.error("Couldn't get cached simple tree for dimension " + dimensionName + ". Login into server and cache dimensions.");
						throw e;
					}
				}
			} 
		}
		
		//if still not null, throw exception
		if( simpleTree == null ){
			throw new Exception("Couldn't get cached simple tree for dimension " + dimensionName + ". Login into server and cache dimensions.");
		}

		return simpleTree;
	}
	
	/**
	 * Finds the cached dimension tree
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for correct project
	 * @param dimensionName
	 *            Name of dimension to retrieve
	 * @return simple paf tree
	 * @throws Exception 
	 */
	private static PafSimpleDimTree getCachedDimensionTree(IProject iProject,
			String projectApplicationName, String dimensionName) throws Exception {

		StopWatch sw = new StopWatch();
		if(logger.isDebugEnabled()) sw.start();
		
		// get cahced dimension tree directory
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		PafSimpleDimTree cachedSimpleTree = null;

		// if directoy exist, continue
		if (cachedDimensionTreeDir.exists()) {

			// get cached dimension tree file name
			PafServer pafServer = getPafServerForProject(iProject);
			
			
			if(  pafServer != null ) {
				
				String cachedDimensionTreeFileName = getFullDimensionNameFileName(
						pafServer, projectApplicationName,
						dimensionName);
				
				// try to create a file reference
				File cachedDimensionTreeFile = new File(cachedDimensionTreeFileName);
				
				//TTN-1950
				if(! getCachedFileStatus(pafServer, projectApplicationName, dimensionName, cachedDimensionTreeFile)){
					return null;
				}
				
				// if file exist
				if (cachedDimensionTreeFile.exists()) {
	
					// try to get cached simple tree
					try {
						cachedSimpleTree = (PafSimpleDimTree) PafXStream
								.importObjectFromXml(cachedDimensionTreeFileName);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}
			}
		}
		
		if(logger.isDebugEnabled()) sw.stop();
		if(logger.isDebugEnabled()) logger.debug(String.format("getCachedDimensionTree for dimension %s took %s ms", dimensionName, sw.getTotalTimeMillis()));
		
		// return cached simple tree
		return cachedSimpleTree;
	}
	
	/**
	 * Gets the map (from the filesystem) that tracks the dimension and uniquie id
	 * @param pafServer
	 * @param projectApplicationName
	 * @param dimensionName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, String> getDimensionCacheMap(PafServer pafServer, 
			String projectApplicationName, String dimensionName) throws Exception {

		StopWatch sw = new StopWatch();
		if(logger.isDebugEnabled()) sw.start();
		
		// get cahced dimension tree directory
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		Map<String, String> map = null;

		// if directoy exist, continue
		if (cachedDimensionTreeDir.exists()) {

			if(  pafServer != null ) {
				
				String dimCacheMapFilePath = getDimensionSessionMapFileName(pafServer,
						projectApplicationName,
						dimensionName);
	
				// try to create a file reference
				File dimCacheMapFile = new File(dimCacheMapFilePath);
	
				// if file exist
				if (dimCacheMapFile.exists()) {
	
					// try to get cached simple tree
					try {
						map = (Map<String, String>) PafXStream
								.importObjectFromXml(dimCacheMapFilePath);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				} 
			}
		}
		
		if(logger.isDebugEnabled()) sw.stop();
		if(logger.isDebugEnabled()) logger.debug(String.format("getDimensionCacheMap for dimension %s took %s ms", dimensionName, sw.getTotalTimeMillis()));
		
		// return map
		return map;
	}
	
	/**
	 * Serializes the dimension cache map to disk.
	 * @param pafServer
	 * @param map
	 * @param projectApplicationName
	 * @param dimensionName
	 */
	private static void setDimensionCacheMap(PafServer pafServer, Map<String, String> map, 
			String projectApplicationName, String dimensionName) {
		
		StopWatch sw = new StopWatch();
		if(logger.isDebugEnabled()) sw.start();
		
		// get cahced dimension tree directory
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		// if directoy exist, continue
		if (cachedDimensionTreeDir.exists()) {

			if(  pafServer != null ) {
				
				String dimCacheMapFilePath = getDimensionSessionMapFileName(pafServer,
						projectApplicationName,
						dimensionName);
	
				// try to create a file reference
				File dimCacheMapFile = new File(dimCacheMapFilePath);
	
				// if file exist
				if (dimCacheMapFile.exists()) {
					dimCacheMapFile.delete();
				} 
				
				// try to get cached simple tree
				try {
					logger.debug(String.format("Writing updated '%s' cache map", dimCacheMapFilePath));
					PafXStream.exportObjectToXml(map, dimCacheMapFilePath);
					
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
		}
		
		if(logger.isDebugEnabled()) sw.stop();
		if(logger.isDebugEnabled()) logger.debug(String.format("setDimensionCacheMap for dimension %s took %s ms", dimensionName, sw.getTotalTimeMillis()));
	}

	/**
	 * Finds the cached attribute tree
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @return simple paf tree
	 */
	public static PafSimpleAttributeTree getCachedAttributeTree(IProject iProject,
			String projectApplicationName, String dimensionName) {

		// get cahced dimension tree directory
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		PafSimpleAttributeTree cachedSimpleTree = null;

		// if directoy exist, continue
		if (cachedDimensionTreeDir.exists()) {

			// get cached dimension tree file name
			String cachedDimensionTreeFileName = getFullDimensionNameFileName(
					getPafServerForProject(iProject), projectApplicationName,
					dimensionName);

			// try to create a file reference
			File cachedDimensionTreeFile = new File(cachedDimensionTreeFileName);

			// if file exist
			if (cachedDimensionTreeFile.exists()) {

				// try to get cached simple tree
				try {
					cachedSimpleTree = (PafSimpleAttributeTree) PafXStream
							.importObjectFromXml(cachedDimensionTreeFileName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		// return cached simple tree
		return cachedSimpleTree;
	}
	
	/**
	 * Caches the dimension tree
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @param treeToCache
	 *            Dimension tree to cache
	 */
	public static void setCachedDimensionTree(IProject iProject,
			String projectApplicationName, String dimensionName,
			PafSimpleDimTree treeToCache) {

		// get project server
		PafServer projectServer = getPafServerForProject(iProject);

		// cache dimension tree
		if( projectServer != null ) {
			setCachedDimensionTree(projectServer, projectApplicationName,
					dimensionName, treeToCache);
		}
	}

	/**
	 * Caches the dimension tree
	 * @param <setDimensionTreeStatus>
	 * 
	 * @param projectServer
	 *            Current project server
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @param treeToCache
	 *            Dimension tree to cache
	 */
	public static void setCachedDimensionTree(PafServer projectServer,
			String projectApplicationName, String dimensionName,
			PafSimpleDimTree treeToCache) {

		// get directory where dimenion caching will take place
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		// if dir does not exist, create
		if (!cachedDimensionTreeDir.exists()) {

			cachedDimensionTreeDir.mkdir();

		}

		// get cached dim server dir
		File cachedDimensionServerDir = new File(
				getServerCachedTreeDir(projectServer));

		// if doesn't exist, create
		if (!cachedDimensionServerDir.exists()) {

			cachedDimensionServerDir.mkdir();

		}

		// get cached dim server app dir
		File cachedDimensionServerApplicationDir = new File(
				getServerCachedTreeDir(projectServer) + File.separator
						+ projectApplicationName);

		// if doesn't exist, create
		if (!cachedDimensionServerApplicationDir.exists()) {
			cachedDimensionServerApplicationDir.mkdir();
		}

		
		addOrUpdateCacheMap(projectServer, projectApplicationName, dimensionName);
		
		// cache paf simple tree
		PafXStream.exportObjectToXml(treeToCache, getFullDimensionNameFileName(
				projectServer, projectApplicationName, dimensionName));

	}

	/**
	 * Caches an PafMdbProps object.
	 * 
	 * @param projectServer The current paf server.
	 * @param projectApplicationName The current application.
	 * @param mdbProps The PafMdbProps object to cache.
	 */
	public static void setMdbProps(PafServer projectServer,
			String projectApplicationName, PafMdbProps mdbProps) {

		// get directory where dimension caching will take place
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		// if dir does not exist, create
		if (!cachedDimensionTreeDir.exists()) {

			cachedDimensionTreeDir.mkdir();

		}

		// get cached dim server dir
		File cachedDimensionServerDir = new File(
				getServerCachedTreeDir(projectServer));

		// if doesn't exist, create
		if (!cachedDimensionServerDir.exists()) {

			cachedDimensionServerDir.mkdir();

		}

		// get cached dim server app dir
		File cachedDimensionServerApplicationDir = new File(
				getServerCachedTreeDir(projectServer) + File.separator
						+ projectApplicationName);

		// if doesn't exist, create
		if (!cachedDimensionServerApplicationDir.exists()) {
			cachedDimensionServerApplicationDir.mkdir();
		}

		// cache paf simple tree
		if(mdbProps != null)
		{
			
			addOrUpdateCacheMap(projectServer, projectApplicationName, PafMdbProps.class.getSimpleName());
			
			PafXStream.exportObjectToXml(mdbProps, getFullDimensionNameFileName(
				projectServer, projectApplicationName, PafMdbProps.class.getSimpleName()));
		}
	}
	
	/**
	 * 
	 * @param projectServer
	 * @param projectApplicationName
	 * @param dimensionName
	 */
	private static void addOrUpdateCacheMap(PafServer projectServer,
			String projectApplicationName, String dimensionName){
		
		Map<String, String> map = null;
		try {
			 map = getDimensionCacheMap(projectServer, projectApplicationName, dimensionName);
		} catch (Exception e) {
			//Don't care as we'll just create it below
		} finally {
			if(map == null){
				map = new HashMap<String, String>(15);
			}
		}
		
		//Get the current session, so the AppSessionId can be retrieved 
		ApplicationState state = ServerMonitor.getInstance().getApplicationState(projectServer);
		if(state == null){
			logger.error("Cannot find application state, cache map can't be written to disk");
		} else{
			//Add/update the new dimension/id
			map.put(dimensionName, state.getAppSessionId());
			//Write the map to disk.
			setDimensionCacheMap(projectServer, map, projectApplicationName, dimensionName);
		}
	}
	
	/**
	 * Gets an array of attribute dimension names from the cache.
	 * 
	 * @param projectServer The current paf server.
	 * @param projectApplicationName The current application.
	 * @return An array of attribute dimension names.
	 * @throws Exception
	 */
	public static String[] getAttributeDimensionMembers(PafServer projectServer,
			String projectApplicationName) throws Exception {
		
		// create new set
		String[] attributeMembers = null;
		
		PafMdbProps mdbProps = getMdbProps(projectServer, projectApplicationName); 
		
		//if not null and list contains at least 1 item
		if (mdbProps != null && mdbProps.getAttributeDims().size() > 0 ) {			
			
			attributeMembers = mdbProps.getAttributeDims().toArray(new String[0]);
			
		}
		
		return attributeMembers;
	}
	

	public static PafMdbProps getMdbProps(PafServer projectServer,
			String projectApplicationName)
			throws Exception {
		
		return getMdbProps(projectServer, projectApplicationName, false);
	}
	
	/**
	 * Gets the cached PafMdbProps object.
	 * 
	 * @param projectServer The current paf server.
	 * @param projectApplicationName The current application.
	 * @param forceRefreshFromServer  Forces a refresh from the server
	 * @return The cached PafMdbProps object.
	 * @throws Exception
	 */
	public static PafMdbProps getMdbProps(PafServer projectServer,
			String projectApplicationName, boolean forceRefreshFromServer) 
			throws Exception {
	
		if( projectServer == null ) {
			throw new Exception("No default project server nor paf server found.");
			
		}
		//try to get already cached dim tree
		PafMdbProps pafMdbProps = getCachedMdbProps(projectServer, projectApplicationName);

		// if not already cached or user wants a forced refresh from server
		if (pafMdbProps == null || forceRefreshFromServer) {

			String url = projectServer.getCompleteWSDLService();
			ServerView.authWithLoginDialogIfNotAuthed(projectServer, false);
			
			if ( SecurityManager.isAuthenticated(url) ) {
				try {
					
					/*PafMdbPropsResponse pafMdbPropsResponse = null;
					ServerSession serverSession = SecurityManager.getSession(url);
					
					//create a props request object
					PafMdbPropsRequest pafMdbPropsRequest = new PafMdbPropsRequest();
					pafMdbPropsRequest.setClientId(serverSession.getClientId());
					pafMdbPropsRequest.setSessionToken(serverSession.getSecurityToken());
					
					// get all paf simple trees on running server
					pafMdbPropsResponse = WebServicesUtil.getPafService(url).getMdbProps(pafMdbPropsRequest);
					
					if (pafMdbPropsResponse != null) {
						pafMdbProps = pafMdbPropsResponse.getMdbProps();
					}
*/
					
					pafMdbProps = SecurityManager.getPafMdbProps(url);
					
					// if tree is returned, cache it
					if (pafMdbProps != null) {
						setMdbProps(projectServer,
								projectApplicationName,
								pafMdbProps);
					}
					
					// uh-oh, exception was thrown
				
				} catch (RemoteException re) {
					
					// create error message
					String errorMessage = "Could not connect to server '"
							+ projectServer.getName() + "' located at '"
							+ projectServer.getCompleteWSDLService() + "'.";
					// log error message
					logger.error(errorMessage);
					// throw new exception
					throw new ServerNotRunningException(errorMessage);
					
				} catch (PafSoapException_Exception e) {
					
					// log error message
					logger.error("PafSoapException occurred: " + e.getMessage());
					// re throw exception
					throw e;
					
				} catch (Exception e) {
					
					// log error message
					logger.error("Exception occurred: " + e.getMessage());
					// re throw exception
					throw e;
					
				}
			}
		}
		if( pafMdbProps == null ) {
			throw new Exception("Couldn't get cached dimensions. Login into server and cache dimensions.");
		}
		return pafMdbProps;
	}
	
	/**
	 * Gets the cached PafMdbProps object.
	 * 
	 * @param projectServer The current paf server.
	 * @param projectApplicationName The current application.
	 * @return The cached PafMdbProps object.
	 */
	public static PafMdbProps getCachedMdbProps(PafServer projectServer,
			String projectApplicationName) {

		// get cahced dimension tree directory
		File cachedDimensionTreeDir = new File(getRootCachedTreeDir());

		PafMdbProps cachedMdbProps = null;

		if(!cachedDimensionTreeDir.exists()){
			cachedDimensionTreeDir.mkdirs();
		}
		
		// if directoy exist, continue
		//TTN-1481, fix error where Project is imported and no PafServer is set as default.
		if (cachedDimensionTreeDir.exists() && projectServer != null) {

			// get cached dimension tree file name
			String cachedDimensionTreeFileName = getFullDimensionNameFileName(
					projectServer, projectApplicationName,
					PafMdbProps.class.getSimpleName());

			// try to create a file reference
			File cachedDimensionTreeFile = new File(cachedDimensionTreeFileName);

			//TTN-1950
			if(! getCachedFileStatus(projectServer, projectApplicationName, PafMdbProps.class.getSimpleName(), cachedDimensionTreeFile)){
				return null;
			}
			
			// if file exist
			if (cachedDimensionTreeFile.exists()) {

				// try to get cached simple tree
				try {
					cachedMdbProps = (PafMdbProps) PafXStream
							.importObjectFromXml(cachedDimensionTreeFileName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		// return cached simple tree
		return cachedMdbProps;
	}
	
	
	/**
	 * Returns false if the file is outdated or does not exist.
	 * @param projectServer
	 * @param projectApplicationName
	 * @param dimensionName
	 * @param cacheFile
	 * @return
	 */
	private static boolean getCachedFileStatus(PafServer projectServer,
			String projectApplicationName, String dimensionName, File cacheFile ){
		
		//Get the map file from disk.  If it does not exist then the tree file must be created.
		Map<String, String> map = null;
		try {
			map = getDimensionCacheMap(projectServer, projectApplicationName, dimensionName);
		} catch (Exception e1) {
			//No map can be found, that's ok.  All files will be reloaded and the map will be created.
		}
		if(map == null || map.size() == 0){
			return false;
		} else {
			//Get the current app state
			ApplicationState state = ServerMonitor.getInstance().getApplicationState(projectServer);
			
			if(state != null && state.getAppSessionId() != null){
				
				boolean isValid = true;
				//If the valid map entry does not exist, this means:
				//1 the dimension was never cached
				//2 the session id has changed.
				if(!map.containsKey(dimensionName)){
					logger.info(String.format("'%s' dimension not found in dimension cache map", dimensionName));
					isValid = false;
				} else {
					if(!state.getAppSessionId().equals(map.get(dimensionName))){
						logger.info(String.format("'%s' dimension sessionId does not match current applicationSessionId, a new cache file will be created", dimensionName));
						isValid = false;
					}
				}
				if(!isValid){
					if (cacheFile.exists()) {
						logger.info(String.format("Purging stale file: %s", cacheFile));
						cacheFile.delete();
					} 
					return false;
				}
			
			} else {
				logger.warn(String.format("Cannot find application state, cached trees will be verified later.  Please check the connection to Server: %s, Application: %s", projectServer.getName(), projectApplicationName));
			}
			return true;
		}
	}
	
	/**
	 * Gets the dimension members as a unique set
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @return A set of unqiue members from the dimension tree
	 * @throws Exception
	 */
	public static Set<String> getDimensionMemberSet(IProject iProject,
			String projectApplicationName, String dimensionName)
			throws Exception {

		// create new set
		Set<String> memberSet = new HashSet<String>();

		// get simple tree from util
		PafSimpleDimTree simpleTree = getDimensionTree(iProject,
				projectApplicationName, dimensionName);

		// if not null
		if (simpleTree != null) {
		
			// loop through members and add to set
			for (PafSimpleDimMember member : simpleTree.getMemberObjects()) {

				memberSet.add(member.getKey());

			}

		}

		// return member set from dimension tree
		return memberSet;

	}
	
	/**
	 * Gets the full dimension name file name
	 * 
	 * @param projectServer
	 *            Current projects project server
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @return String The full dimension name for file name
	 */
	public static String getFullDimensionNameFileName(PafServer projectServer,
			String projectApplicationName, String dimensionName) {
		return getServerCachedTreeDir(projectServer) + File.separator
				+ projectApplicationName + File.separator + dimensionName + '.'
				+ Constants.XML_EXT;
	}
	
	/**
	 * Gets the name of the file that tracks the id for which the cache was built
	 * @param projectServer
	 * @param projectApplicationName
	 * @param dimensionName
	 * @return
	 */
	public static String getDimensionSessionMapFileName(PafServer projectServer,
			String projectApplicationName, String dimensionName){
		return getServerCachedTreeDir(projectServer) + File.separator
				+ projectApplicationName + File.separator +  ".cachemap";
	}

	/**
	 * Gets the full dimension name file name
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @return The full dimension name file name
	 */
	public static String getFullDimensionNameFileName(IProject iProject,
			String projectApplicationName, String dimensionName) {
		return getFullDimensionNameFileName(getPafServerForProject(iProject),
				projectApplicationName, dimensionName);
	}
	
	/**
	 * Gets the dimension members as a unique set
	 * 
	 * @param iProject
	 *            Current project selected
	 * @return The project server
	 */
	private static PafServer getPafServerForProject(IProject iProject) {

		PafServer projectServer = null;

		try {

			// try to get project server
			projectServer = PafProjectUtil.getProjectServer(iProject);

			// if project server is null
			if (projectServer == null) {

				// get default server
				projectServer = PafServerUtil.getDefaultServer();

			}

		} catch (PafServerNotFound e1) {
			
			logger.error( e1.getMessage() + " Project: " + iProject.getName() );
//			e1.printStackTrace();
		}

		// return project server
		return projectServer;

	}

	/**
	 * Gets the root directory for the cached info
	 * 
	 * @return The string directory for cached dim trees
	 */
	private static String getRootCachedTreeDir() {

		//return Constants.ADMIN_CONSOLE_PLATFORM_CONF_HOME + Constants.CACHED_DIM_TREE_DIR;
		return FilenameUtils.concat(SystemUtil.getAcProgramDataDirectory(), Constants.CACHED_DIM_TREE_DIR);
	}

	public static String getServerCachedTreeDir(PafServer projectServer) {
		return getRootCachedTreeDir() + File.separator
				+ projectServer.getName();
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}

	/**
	 * Converts a PafSimpleTree into a HashMap
	 * @param tree The PafSimpleTree to convert.
	 * @return A HashMap, containg the items from the PafSimpleTree.
	 */
	public static HashMap<String, PafSimpleDimMember> convertTreeIntoHashMap(PafSimpleDimTree tree) {

		return PafSimpleDimTreeUtil.convertTreeIntoHashMap(tree);
	}
	
	
	// This method will return the measure descendants based on the parent and the list fo measures .
	public static List<PafSimpleDimMember> getDescendantsOfParent(List<PafSimpleDimMember> members, String parent)
	{
		return PafSimpleDimTreeUtil.getDescendantsForParent(members, parent);
	}
	
	/**
	 * gets the simple dimension tree from cache for project, app name but
	 * forces a refresh from server
	 * 
	 * @param iProject
	 *            Current project selected
	 * @param projectApplicationName
	 *            Application name for currect project
	 * @param dimensionName
	 *            Name of dimension to retrive
	 * @param forceRefreshFromServer
	 *            forces a refresh from the server
	 * @return simple paf tree
	 * @throws Exception
	 */
	public static PafSimpleDimTree getFilteredDimensionTree(IProject iProject,
			String projectApplicationName, String dimensionName,
			List<String> filterSpec) throws Exception {


		PafSimpleDimTree  filteredTree = null;
		
		// get project server for project
		final PafServer projectServer = getPafServerForProject(iProject);
		
		if( projectServer == null ) {
			throw new Exception("No default server nor paf server found.");
		}
		String url = projectServer.getCompleteWSDLService();
		
		if( url != null ){
			ServerView.authWithLoginDialogIfNotAuthed(projectServer, false);
			
			if ( SecurityManager.isAuthenticated(url) ) {

				try {
					
					filteredTree = SecurityManager.getFilteredPafSimpleDimTree(url,dimensionName, filterSpec);
					
					// uh-oh, exception was thrown
				} catch (RemoteException e) {
					String errorMessage = "Could not connect to server '"
						+ projectServer.getName() + "' located at '"
						+ projectServer.getCompleteWSDLService() + "'." + e.getMessage();

					// log error message
					logger.error(errorMessage);
	
					// throw new exception
					throw new ServerNotRunningException(errorMessage);
				} catch (PafException e) {
						// log error message
					logger.error("PafException occurred: " + e.getMessage());

					// re throw exception
					throw e;

				} catch (RuntimeException e) {
						// log error message
					String errorMessage = String.format(
							ErrorConstants.INVALID_DIMENSION,
							new Object[] { dimensionName });

					// log error
					logger.error(errorMessage);

					// throw new exception
					throw new DimensionNotFoundException(errorMessage);

				} catch (Exception e) {
						// log error message
					// if null pointer is thrown, get error message, logg it and
					// throw new dim not found exc
					logger.error("Couldn't get cached simple tree for dimension " + dimensionName + ". Login into server and cache dimensions.");
					throw e;
				}
			}
			
		}
		
		//if still not null, throw exception
		if( filteredTree == null ){
			throw new Exception("Couldn't get cached simple tree for dimension " + dimensionName + ". Login into server and cache dimensions.");
		}

		return filteredTree;
	}
}

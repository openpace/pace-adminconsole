@rem ***************************************************************************
@rem Copyright (c) 2017-2019 Contributors to Open Pace and others.
@rem  
@rem The OPEN PACE product suite is free software: you can redistribute it and/or
@rem modify it under the terms of the GNU General Public License as published by
@rem the Free Software Foundation, either version 3 of the License, or (at your
@rem option) any later version.
@rem  
@rem This software is distributed in the hope that it will be useful, but WITHOUT
@rem ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
@rem or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
@rem License for more details.
@rem  
@rem You should have received a copy of the GNU General Public License along with
@rem this  software. If not, see <http://www.gnu.org/licenses/>.
@rem ***************************************************************************
@echo off
rem -------------------------------------------------------------------------
rem Admin Console Bootstrap Script for Win32
rem -------------------------------------------------------------------------


echo %JAVA_HOME%
echo
eclipse.exe -vm "%JAVA_HOME%\bin\javaw.exe"
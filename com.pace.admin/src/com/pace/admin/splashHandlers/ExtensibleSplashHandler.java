/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.admin.splashHandlers;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.splash.EclipseSplashHandler;

import com.pace.admin.AdminconsolePlugin;

/**
 * @since 3.3
 *
 */
@SuppressWarnings("restriction")
public class ExtensibleSplashHandler extends EclipseSplashHandler {
	Font font1, font2, font3;
	/**
	 * 
	 */
	public ExtensibleSplashHandler() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.splash.AbstractSplashHandler#init(org.eclipse.swt.widgets.Shell)
	 */
	public void init(Shell splash) {
		// Store the shell
		super.init(splash);
		
		getContent().addPaintListener (new PaintListener () {
			public void paintControl (PaintEvent e) {
				
				e.gc.setForeground(new Color(e.gc.getDevice(), new RGB(255, 255, 255))); 
				
				font1 = new Font(e.gc.getDevice(), e.gc.getDevice().getSystemFont().getFontData()[0].getName(), 8, SWT.NORMAL);
				e.gc.setFont(font1); 
				e.gc.drawText("Open Pace Administration", 135, 228, true);
				e.gc.drawText("Copyright 2017-" + AdminconsolePlugin.getDefault().getAdminConsoleReleaseYear()
						+ "  GNU GPLv3", 135, 240, true);
				//e.gc.drawText("Licensed Under GNU GPLv3", 135, 271, true);
				
				font2 = new Font(e.gc.getDevice(), e.gc.getDevice().getSystemFont().getFontData()[0].getName(), 5, SWT.NORMAL);
				e.gc.setFont(font2); 
				//e.gc.drawText("TM", 260, 232, true);
				
				font3 = new Font(e.gc.getDevice(), e.gc.getDevice().getSystemFont().getFontData()[0].getName(), 12, SWT.NORMAL);
				e.gc.setFont(font3); 
				
				String version = AdminconsolePlugin.getDefault().getAdminConsoleReleaseNumber();
				e.gc.drawText("Version " + version, 355, 235, true);
			}
		});
	    splash.layout(true, true);
	}

	
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.splash.AbstractSplashHandler#dispose()
	 */
	public void dispose() {
		super.dispose();
		if( font1 != null )
			font1.dispose();
		if( font2 != null )
			font2.dispose();
		if( font3 != null )
			font3.dispose();
	}
}

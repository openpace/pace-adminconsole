/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.PropertyResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.pace.base.ui.DefaultPrintSettings;

/**
 * The main plugin class to be used in the desktop.
 * @author jmilliron
 */
public class AdminconsolePlugin extends AbstractUIPlugin {
		
	private static final String LOG4J_PROPERTIES = "log4j.properties";

	private static final String PROPS = "props";

	private static Logger logger = Logger.getLogger(AdminconsolePlugin.class);

	//The shared instance.
	private static AdminconsolePlugin plugin;

	/**
	 * Returns the shared instance.
	 * @return AdminconsolePlugin
	 */
	public static AdminconsolePlugin getDefault() {
		return plugin;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path.
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin("com.pace.admin", path);
	}
	
	/**
	 * The constructor.
	 */
	public AdminconsolePlugin() {
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 * @param context
	 * @throws java.lang.Exception
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		configureLogging();
		DefaultPrintSettings.getInstance();
		
//		logger.info( this.getAdminConsoleProgramName() + " [" + this.getAdminConsoleReleaseNumber() + "] is starting.");		
						
	}

	private void configureLogging() throws IOException {

		String log4jProperty = LOG4J_PROPERTIES; 
		
		File currentDir = new File(".");
		
		boolean localLog4JfileFound = false;
		
		String varLog4jPath = null;
		
		if ( currentDir.exists() ) {
			
			File log4jPropertyFile = new File(currentDir.toString() + File.separator + log4jProperty);
			
			if ( log4jPropertyFile.exists() ) {
											
				varLog4jPath = log4jPropertyFile.getAbsoluteFile().toString();
				
				PropertyConfigurator.configure(varLog4jPath);
				
				localLog4JfileFound = true;
				
			}
			
		}
		
		if ( ! localLog4JfileFound  ) {
			
			URL varPluginURL = this.getBundle().getEntry("/");
			String varInstallPath = FileLocator.toFileURL(varPluginURL).getFile();
			varLog4jPath = varInstallPath + PROPS + File.separator + log4jProperty;
			PropertyConfigurator.configure(varLog4jPath);
			
		}
		
		logger = Logger.getLogger(this.getClass());
		
		logger.debug("log4j configuration file = " + varLog4jPath);

		
	}

	/**
	 * This method is called when the plug-in is stopped
	 * @param context
	 * @throws java.lang.Exception
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
//		logger.info( this.getAdminConsoleProgramName() + " [" + this.getAdminConsoleBuildNumber() + "] is stopping.");
		plugin = null;		
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
	
	protected final static String ABOUT_MAPPING = "about.mappings";

    protected PropertyResourceBundle aboutMappingProperties;

    
    public String getAdminConsoleProgramName() {
    	return getMyProperties().getString("0");
    }
    
    public String getAdminConsoleReleaseYear() {
    	return getMyProperties().getString("5");
    }
    
	public String getAdminConsoleReleaseNumber() {
       	return getMyProperties().getString("6");
	}

    private PropertyResourceBundle getMyProperties(){

	  if (aboutMappingProperties == null){

	    try {

	    	aboutMappingProperties = new PropertyResourceBundle(

	        FileLocator.openStream(this.getBundle(),

	          new Path(ABOUT_MAPPING),false));

	    } catch (IOException e) {

	      logger.error(e.getMessage());

	    }

	  }

	  return aboutMappingProperties;
	  
	 }
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;

import com.pace.admin.menu.views.InvalidSecurityMemberView;
import com.pace.admin.menu.wizards.ProjectNewWizard;


/**
 * Class_description_goes_here
 * @author jmilliron
 * @version	x.xx
 */
public class Perspective implements IPerspectiveFactory {
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}


	/**
	 * Used to create the intial perspective layout
	 * @param layout holds the layout for the intial layout
	 */
	public void createInitialLayout(IPageLayout layout) {
		
		String editorArea = layout.getEditorArea();
				
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		
		//layout.addStandaloneView("com.pace.paf.adminconsole.navtreeview.NavTreeView", false, 0, 0.38f, editorArea);
				
		IFolderLayout left = layout.createFolder("leftViews", IPageLayout.LEFT, 0.28f, editorArea);
		left.addView("com.pace.admin.menu.views.menuview");
		left.addView("com.pace.admin.projectnavigator.view");
		
		IFolderLayout bottom = layout.createFolder("bottomViews", IPageLayout.BOTTOM, 0.7f, editorArea);
		
		//for now, have problem view first so it will initalize, then the validators will work
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);
		bottom.addView(IPageLayout.ID_PROGRESS_VIEW);
		bottom.addPlaceholder(InvalidSecurityMemberView.ID);
		
		
		//initalize this view
		//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(IPageLayout.ID_PROBLEM_VIEW); 
				
		IFolderLayout topRight = layout.createFolder("messages", IPageLayout.RIGHT, 0.65f, editorArea);
		topRight.addView("com.pace.admin.servers.ServerView");
		topRight.addView(IPageLayout.ID_OUTLINE);		
		
		layout.addNewWizardShortcut(ProjectNewWizard.ID);
		//layout.addNewWizardShortcut(NewProjectFromServerWizard.ID);
		//layout.addNewWizardShortcut(NewProjectFromTemplateWizard.ID);
		
		//set line numbers visible
		String lineNumbers = AbstractDecoratedTextEditorPreferenceConstants.EDITOR_LINE_NUMBER_RULER;
		
		EditorsUI.getPreferenceStore().setValue(lineNumbers, true);
		
		
			
	}
}

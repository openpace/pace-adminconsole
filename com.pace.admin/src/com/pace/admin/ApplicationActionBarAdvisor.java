/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin;

import org.eclipse.core.runtime.IExtension;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.registry.ActionSetRegistry;
import org.eclipse.ui.internal.registry.IActionSetDescriptor;

import com.pace.admin.menu.actions.importexport.ImportAndDeployRulesetsAction;
import com.pace.admin.menu.actions.projects.DeleteProjectAction;
import com.pace.admin.menu.actions.projects.ToggleAutoConvertProjectAction;
import com.pace.admin.menu.actions.projects.ToggleExpandProjectTreeAction;

/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	 
	private IWorkbenchWindow window;

	private DeleteProjectAction deleteProjectAction = null;

	private IAction resetAction = null;
	
	private IAction openPrefsAction = null;
	
	private IAction aboutAction = null;
	
	private ToggleExpandProjectTreeAction toggleExpandProjectTreeAction = null;
	private ToggleAutoConvertProjectAction toggleAutoConvertProjectAction = null;
	
	public final static String ABOUT_MENU_TEXT = "About Open Pace Adminstration...";

	private static final String[] ACTIONS_2_WIPE = new String[] { 
//		"org.eclipse.search.searchActionSet", 
//		"org.eclipse.ui.edit.text.actionSet.presentation", 
//		"org.eclipse.ui.edit.text.actionSet.openExternalFile", 
//		"org.eclipse.ui.edit.text.actionSet.annotationNavigation", 
//		"org.eclipse.ui.edit.text.actionSet.navigation", 
//		"org.eclipse.ui.edit.text.actionSet.convertLineDelimitersTo", 
		"org.eclipse.update.ui.softwareUpdates",
		"org.eclipse.debug.ui.debugActionSet",
		"org.eclipse.debug.ui.launchActionSet",
		"org.eclipse.debug.ui.breakpointActionSet",
		"org.eclipse.debug.ui.profileActionSet"
		}; 


	/**
	 * Method_description_goes_here
	 * 
	 * @param configurer
	 */
	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
		window = null;
		
		//used to get rid of Covert lines to delimiters to in File menu
		ActionSetRegistry reg = WorkbenchPlugin.getDefault().getActionSetRegistry();
		IActionSetDescriptor[] actionSets = reg.getActionSets();
		
		for (String actionId : ACTIONS_2_WIPE) {
			removeActionFromActionSet(actionSets, actionId);
		}	
		 
	}
		
	private void removeActionFromActionSet(IActionSetDescriptor[] actionSets, String actionId) {
				
		for (int i = 0; i <actionSets.length; i++)
		{
		    if (!actionSets[i].getId().equals(actionId))
		        continue;
		        IExtension ext = actionSets[i].getConfigurationElement()
		            .getDeclaringExtension();
		        WorkbenchPlugin.getDefault().getActionSetRegistry().removeExtension(ext, new Object[] { actionSets[i] });
		}
		
	}

	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}

	protected void fillCoolBar(ICoolBarManager cbManager) {
		
		cbManager.add(new GroupMarker("group.file")); //$NON-NLS-1$
						
		//File Group
		IToolBarManager fileToolBar = new ToolBarManager(cbManager
				.getStyle());
		
		fileToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));
		
		fileToolBar.add(getAction(ActionFactory.NEW_WIZARD_DROP_DOWN.getId()));
		
		//fileToolBar.add(new GroupMarker(IWorkbenchActionConstants.OPEN_EXT));
		
		//fileToolBar.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
				
		fileToolBar.add(new GroupMarker(IWorkbenchActionConstants.SAVE_GROUP));
				
		fileToolBar.add(getAction(ActionFactory.SAVE.getId()));
				
		fileToolBar.add(getAction(ActionFactory.PRINT.getId()));

		fileToolBar.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		// Add to the cool bar manager
		cbManager.add(new ToolBarContributionItem(fileToolBar,
				IWorkbenchActionConstants.TOOLBAR_FILE));
		
		// End File Group
		
		//cbManager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

		cbManager.add(new GroupMarker(IWorkbenchActionConstants.GROUP_EDITOR));
	}

	protected void fillMenuBar(IMenuManager menubar) {
		menubar.add(createFileMenu());
		menubar.add(createEditMenu());
		menubar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menubar.add(createProjectMenu());
		menubar.add(createWindowMenu());
		menubar.add(createHelpMenu());
		
	}

	protected void makeActions(IWorkbenchWindow window) {

		this.window = window;

		deleteProjectAction = new DeleteProjectAction(window);
		resetAction = ActionFactory.RESET_PERSPECTIVE.create(window);
		openPrefsAction = ActionFactory.PREFERENCES.create(window);
		
		toggleExpandProjectTreeAction = new ToggleExpandProjectTreeAction();
		toggleAutoConvertProjectAction = new ToggleAutoConvertProjectAction(window);
		
		aboutAction = ActionFactory.ABOUT.create(window);
	    register(aboutAction);
		
		registerAsGlobal(deleteProjectAction);
		registerAsGlobal(resetAction);
		registerAsGlobal(openPrefsAction);
		registerAsGlobal(toggleExpandProjectTreeAction);
		registerAsGlobal(toggleAutoConvertProjectAction);
		registerAsGlobal(ActionFactory.NEW.create(window));
		registerAsGlobal(ActionFactory.SAVE.create(window));
		registerAsGlobal(ActionFactory.SAVE_AS.create(window));
		registerAsGlobal(ActionFactory.ABOUT.create(window));
		registerAsGlobal(ActionFactory.SAVE_ALL.create(window));
		registerAsGlobal(ActionFactory.PRINT.create(window));
		registerAsGlobal(ActionFactory.UNDO.create(window));
		registerAsGlobal(ActionFactory.REDO.create(window));
		registerAsGlobal(ActionFactory.CUT.create(window));
		registerAsGlobal(ActionFactory.COPY.create(window));
		registerAsGlobal(ActionFactory.PASTE.create(window));
		registerAsGlobal(ActionFactory.SELECT_ALL.create(window));
		registerAsGlobal(ActionFactory.FIND.create(window));
		registerAsGlobal(ActionFactory.CLOSE.create(window));
		registerAsGlobal(ActionFactory.CLOSE_ALL.create(window));
		registerAsGlobal(ActionFactory.CLOSE_ALL_SAVED.create(window));
		registerAsGlobal(ActionFactory.REVERT.create(window));
		registerAsGlobal(ActionFactory.QUIT.create(window));
		registerAsGlobal(ActionFactory.HELP_CONTENTS.create(window));
		registerAsGlobal(ActionFactory.DELETE.create(window));
		registerAsGlobal(ActionFactory.IMPORT.create(window));
		registerAsGlobal(ActionFactory.EXPORT.create(window));
		registerAsGlobal(ActionFactory.NEW_WIZARD_DROP_DOWN.create(window));

	}

	/**
	 * Creates and returns the File menu.
	 */
	private MenuManager createFileMenu() {
		
		MenuManager menu = new MenuManager(
				"&File", IWorkbenchActionConstants.M_FILE); //$NON-NLS-1$
		
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_START));
		
		menu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		
		MenuManager newMenu = new MenuManager("&New", "new"); //$NON-NLS-1$
						
		newMenu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		
		newMenu.add(ContributionItemFactory.NEW_WIZARD_SHORTLIST.create(window));
		
		menu.add(newMenu);
		
		menu.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
		
		menu.add(getAction(ActionFactory.CLOSE.getId()));
		menu.add(getAction(ActionFactory.CLOSE_ALL.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.CLOSE_EXT));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.SAVE.getId()));
		menu.add(getAction(ActionFactory.SAVE_AS.getId()));
		menu.add(getAction(ActionFactory.SAVE_ALL.getId()));
		menu.add(getAction(ActionFactory.REVERT.getId()));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.PRINT.getId()));
		menu.add(ContributionItemFactory.REOPEN_EDITORS
				.create(getActionBarConfigurer().getWindowConfigurer()
						.getWindow()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.MRU));
		menu.add(new Separator());
		
	
		menu.add(getAction(ActionFactory.IMPORT.getId()));
		
		//TTN-2165 Add new menu option for Import Rule Sets and Deploy
		ImportAndDeployRulesetsAction importAndDeployAction = new ImportAndDeployRulesetsAction("Import And Deploy Rulesets",window,null);
		
		menu.add(importAndDeployAction);
		
		menu.add(getAction(ActionFactory.EXPORT.getId()));
		
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.QUIT.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_END));
		return menu;
	}

	/**
	 * Creates and returns the Edit menu.
	 */
	private MenuManager createEditMenu() {
		MenuManager menu = new MenuManager(
				"&Edit", IWorkbenchActionConstants.M_EDIT); //$NON-NLS-1$
		menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_START));

		menu.add(getAction(ActionFactory.UNDO.getId()));
		menu.add(getAction(ActionFactory.REDO.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.UNDO_EXT));

		menu.add(getAction(ActionFactory.CUT.getId()));
		menu.add(getAction(ActionFactory.COPY.getId()));
		menu.add(getAction(ActionFactory.PASTE.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.CUT_EXT));

		menu.add(getAction(ActionFactory.SELECT_ALL.getId()));
		menu.add(new Separator());

		menu.add(getAction(ActionFactory.FIND.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));

		menu.add(new GroupMarker(IWorkbenchActionConstants.ADD_EXT));

		menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_END));
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		return menu;
	}

	/**
	 * Creates and returns the Edit menu.
	 */
	private MenuManager createHelpMenu() {
		MenuManager menu = new MenuManager(
				"&Help", IWorkbenchActionConstants.M_HELP); //$NON-NLS-1$
		menu.add(new GroupMarker(IWorkbenchActionConstants.HELP_START));
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(new GroupMarker(IWorkbenchActionConstants.HELP_END));
		menu.add(new Separator("about")); //$NON-NLS-1$
//		menu.add(getAction(ActionFactory.HELP_CONTENTS.getId()));		// Disabled for Open Pace Rebranding (TTN-2666)
//		menu.add(getAction(ActionFactory.ABOUT.getId()));
		aboutAction.setText(ABOUT_MENU_TEXT);
        menu.add(aboutAction);
		
		return menu;
	}

	private MenuManager createProjectMenu() {

		MenuManager projectMenu = new MenuManager("&Project", "project");

		projectMenu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		projectMenu.add(deleteProjectAction);
		projectMenu.add(toggleAutoConvertProjectAction);
		projectMenu.add(new Separator());
		projectMenu.add(toggleExpandProjectTreeAction);

		return projectMenu;
	}

	private MenuManager createWindowMenu() {

		MenuManager showViewMenuMgr = new MenuManager("Show View", "showView");
		MenuManager menu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW);

		showViewMenuMgr.add(ContributionItemFactory.VIEWS_SHORTLIST
				.create(window));

		menu.add(showViewMenuMgr);
		
		resetAction.setEnabled(true);
		menu.add(resetAction);
		
		//TODO: un-comment if need to get Window -> Preferences
		//openPrefsAction.setEnabled(true);
		//menu.add(openPrefsAction);
		
		return menu;
	}

	/**
	 * Registers the action as global action and registers it for disposal.
	 * 
	 * @param action
	 *            the action to register
	 */
	private void registerAsGlobal(IAction action) {
		getActionBarConfigurer().registerGlobalAction(action);
		register(action);
	}

	

 

}

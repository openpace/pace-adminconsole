/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IPlatformRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * This class controls all aspects of the application's execution
 * @author jmilliron
 */
public class Application implements IPlatformRunnable {

	private static final Logger logger = Logger.getLogger(Application.class);
	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IPlatformRunnable#run(java.lang.Object)
	 */
	
	/**
	 * Method_description_goes_here
	 * @param args
	 * @return
	 * @throws java.lang.Exception
	 */
	public Object run(Object args) throws Exception {
		Display display = PlatformUI.createDisplay();
		
		Location instanceLocation = Platform.getInstanceLocation();
        
		// TTN-2569 Add a lock to the workspace and prevent multiple instances.
        if (!instanceLocation.lock()) {
        	logger.warn("Trying to open multiple instances of the AC. One instance is already running with a lock applied.");
        	String errorMsg = "The Pace Project Workspace ["+instanceLocation.getURL().getPath()+"] is currently locked by another instance of the Pace Administration Console.";
            MessageDialog.openError(new Shell(display), "Pace Administration",
            errorMsg);
            return IPlatformRunnable.EXIT_OK;
        } 
		
		try {
			final int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) {
				return IPlatformRunnable.EXIT_RESTART;
			}
			return IPlatformRunnable.EXIT_OK;
		} finally {
			display.dispose();
		}
	}
	
	/**
	 * Automatically generated method: toString
	 * @return String
	 */
	public String toString () {
		return super.toString();
	}
}

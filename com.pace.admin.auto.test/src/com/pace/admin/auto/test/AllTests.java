/*******************************************************************************
 * Copyright (c) 2008 Syntax Consulting, Inc. All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package com.pace.admin.auto.test;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.rcpquickstart.bundletestcollector.BundleTestCollector;

public class AllTests extends BundleTestCollector {

	public static Test suite() {
		BundleTestCollector testCollector = new BundleTestCollector();
		
		TestSuite suite = new TestSuite("All Tests");

		/*
		 * assemble as many collections as you like based on bundle, package and
		 * classname filters
		 */
		testCollector.collectTests(suite, "com.pace", "com.pace.admin",
				"*Test*");

		return suite;

	}
}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your 
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.menu.dialogs;

import com.pace.admin.global.model.PaceMemberSelection;

import junit.framework.TestCase;

public class MemberSelectionTest extends TestCase {

	String existingUserSelDescLevel = "@IDESC(@USER_SEL(CLIMATE1), L0)";
	
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCreateMemberSelectionFromExistingMember() {
		
		PaceMemberSelection existingUserSelDescLevelMemberSelection = PaceMemberSelection.createMemberSelectionFromExistingMember(existingUserSelDescLevel);
		
		assertNotNull(existingUserSelDescLevelMemberSelection);
		assertEquals(existingUserSelDescLevelMemberSelection.getMemberWithSelection(), existingUserSelDescLevel);
		assertEquals(existingUserSelDescLevelMemberSelection.getMembers().length, 1);
		assertEquals(existingUserSelDescLevelMemberSelection.getMembers()[0], "CLIMATE1");
		assertEquals(existingUserSelDescLevelMemberSelection.isUserSelection(), true);
		
		
		
	}

}

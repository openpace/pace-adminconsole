To set up the build, do the following:

1. Import and configure the build project.

* Import the five projects into your Eclipse workspace.

* Open the com.rcpquickstart.helloworld.build-and-test project.

* In the build.properties file, set the "eclipseLocation" property to your Eclipse installation location.

* Modify the two "version" properties at the top of the build.properties file to match the versions of the plug-ins in your Eclipse installation. They are currently set to the correct versions for Eclipse 3.4.


2. Create the build target.

* Create a directory called c:\helloworld-build-target.

* Unzip both the RCP Runtime Binary and the RCP Delta Pack into that directory. Some of the files overlap and it's ok to overwrite them.

* Copy the org.junit plug-in from your Eclipse installation into the build target "plug-in" directory.

* Download the Eclipse SDK and place the archive (not extracted) into the build target directory.


IF USING ECLIPSE 3.3 or 3.4

* Download the appropriate Eclipse Testing Framework version and place it in the com.rcpquickstart.helloworld.build-and-test project.

* Modify the build.xml file to use this version of the ETF.


IF USING ECLIPSE 3.2

* Download the Eclipse Testing Framework version 3.2 and place it in the com.rcpquickstart.helloworld.build-and-test project.

* Modify the build.xml file to use this version of the ETF.

* Examine the java task in the build.xml file. Follow the instructions in comments to make the appropriate changes.


3. Run the build.

* In your Eclipse workspace, right click on the build.xml file in this project and choose Run As -> Ant Build from the context menu. 


4. Verify the build.

* When the build completes, navigate to c:\helloworld-build\I.HelloWorld and unzip the HelloWorld-win32.win32.x86.zip file to some place on your hard drive.

* Locate and execute the helloworld.exe file to verify that the application runs.

* Open c:\helloworld-build\test-results.xml to see the results of the JUnit test run.

* Open c:\helloworld-build\coverage-reports\coverage.html to see the coverage analysis produced by EMMA.

NOTE: The EMMA instrumentation task fails if you are running the ANT script from the IDE and have spaces in your workspace path. I think this is related to a defect with ANT 1.7.0. In any case, if you get an I/O exception in the instrumentation task, removing the spaces from your workspace path should fix this.

Regards,

Patrick Paulin
Eclipse RCP/OSGi Trainer and Consultant
Modular Mind

patrick@modumind.com
www.modumind.com

twitter.com/pjpaulin
linkedin.com/in/pjpaulin


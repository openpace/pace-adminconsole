/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.validation.jobs;

import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidatorJob;
import org.eclipse.wst.xml.core.internal.validation.core.Helper;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.model.managers.MemberTagModelManager;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.IResourceUtil;
import com.pace.admin.global.util.PafMdbPropsUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.validation.constants.PluginConstants;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.ui.PafServer;
import com.pace.server.client.PafMdbProps;

/**
 * Member Tag Dimension Validator Job.  Validates that all member tags have valid dimensions.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class MemberTagDimensionValidatorJob implements IValidatorJob {

	private static final Logger logger = Logger.getLogger(MemberTagDimensionValidatorJob.class);
	
	protected IReporter _reporter;

	public MemberTagDimensionValidatorJob() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#getSchedulingRule(org.eclipse.wst.validation.internal.provisional.core.IValidationContext)
	 */
	public ISchedulingRule getSchedulingRule(IValidationContext arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#validateInJob(org.eclipse.wst.validation.internal.provisional.core.IValidationContext, org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public IStatus validateInJob(IValidationContext helper, IReporter reporter)
			throws ValidationException {
		
		_reporter = reporter;
		
		//remove all messages from reporter
		_reporter.removeAllMessages(this);

		IProject project = null; 
		
		if ( helper instanceof Helper ) {
		
			project = ((Helper) helper).getProject();
			
			try {
				PafProjectUtil.addPafProjectMap(project);
			} catch (PafServerNotFound e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		
		if ( project != null) {
			//if URIs exist
			if (helper.getURIs() != null) {

				//get uris from helper
				String[] uris = helper.getURIs();
				
					// for each uri
				for (String uri : uris) {

					// get problem resource
					IResource resource = project.findMember(uri);
					// if instance of ifile, which should be because of extension
					// value
					if (resource instanceof IFile) {
	
						// cast to ifile
						IFile memberTagDefFile = (IFile) resource;
	
						if (  ! FileHelperUtil.isIFileDescendantOfIFolder(memberTagDefFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
						
							//get conf folder
							IFolder confFolder = project.getFolder(Constants.CONF_DIR);
							
							//if conf exists (has too since member tag def file does)
							if ( confFolder.exists()) {											
									
								//get model manager so we can access all the member tags
								MemberTagModelManager memberTagModelManger = new MemberTagModelManager(project);
								
								//if member tags exists
								if ( memberTagModelManger.size() > 0 ) {
									
									//try to get the project server
									PafServer projectServer = null;
									
									try {
										
										projectServer = PafProjectUtil.getProjectServer(project);
										
									} catch (PafServerNotFound psnf) {
			
										return new Status(
												IStatus.ERROR,
												"id of plugin",
												0,
												"Can not validate " + memberTagDefFile
														+ " because server '"
														+ projectServer.getName()
														+ "' was not found. Please fix "
														+ Constants.SERVERS_FILE + " file.",
												psnf);
									}
															
									//get the application name
									String applicationName = PafProjectUtil.getApplicationName(project);
									
									//ensure both project server and app name are NOT null
									if ( projectServer != null && applicationName != null ) {
																	
										try {
											
											//get the mdbprops.  Should be cached version.
											PafMdbProps pafMdbProps = DimensionTreeUtility.getMdbProps(projectServer, applicationName);
											
											//get all the dimensions from mdb props ( base + attributes )
											Set<String> allDimensionSet = PafMdbPropsUtil.getAllDimensions(pafMdbProps);
											
											//for each member tag def, verify all member tag dimensions are valid for project								
											for (MemberTagDef memberTagDef : memberTagModelManger.getMemberTags(false)) {										
												
												String[] memberTagDefDims = memberTagDef.getDims();
												
												//loop and verify each one
												for ( String memberTagDefDim : memberTagDefDims ) {
													
													//if the dim set doesn't contain the member tag def dimesnion, write an error.
													if (! allDimensionSet.contains(memberTagDefDim ) ) {
														
														logger.error("Member tag '" + memberTagDef.getName() + "' contains invalid dimension '" + memberTagDefDim + "'.");
														
		//												 create blank error message
														Message errorMessage = new Message();
		
														// get errored line number
														int errorLineNo = IResourceUtil.findLineNo(
																memberTagDefFile, memberTagDef.getName());
		
														// set values
														errorMessage.setLineNo(errorLineNo);
														errorMessage.setBundleName("plugin");
														errorMessage
																.setSeverity(IMessage.HIGH_SEVERITY);
														errorMessage
																.setId(PluginConstants.MEMBER_TAG_DIMENSION_ERROR);
														errorMessage
																.setParams(new String[] { memberTagDef.getName(), memberTagDefDim });
														errorMessage.setTargetObject(memberTagDefFile);
		
														// add message to reporter
														_reporter.addMessage(this, errorMessage);
														
													}
													
												}										
												
											}
											
										//catch when server is not running
										} catch (ServerNotRunningException snre) {														
		
												return new Status(IStatus.ERROR, "id of plugin", 0,
														"Can not validate " + memberTagDefFile
																+ " because server '"
																+ projectServer.getName()
																+ "' is not running.", snre);									
		
										//catch any other exception
										} catch (Exception exc) {
		
											return new Status(IStatus.ERROR, "id of plugin", 0, exc
													.getMessage(), exc);
		
										}
								
										
									}
								}
							}
						}
					}				
				}
				
			}
			
		}


		return Status.OK_STATUS;
		
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#cleanup(org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void cleanup(IReporter arg0) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#validate(org.eclipse.wst.validation.internal.provisional.core.IValidationContext, org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void validate(IValidationContext arg0, IReporter arg1)
			throws ValidationException {
		// TODO Auto-generated method stub

	}

}

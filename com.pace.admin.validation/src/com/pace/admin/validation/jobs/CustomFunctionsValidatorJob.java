/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.validation.jobs;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidatorJob;
import org.eclipse.wst.xml.core.internal.validation.core.Helper;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.IResourceUtil;
import com.pace.admin.validation.Activator;
import com.pace.admin.validation.constants.PluginConstants;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureFunctionFactory;
import com.pace.base.funcs.CustomFunctionDef;
import com.pace.base.rules.Formula;
import com.pace.base.rules.Rule;
import com.pace.base.rules.RuleGroup;
import com.pace.base.rules.RuleSet;
import com.sun.org.apache.xml.internal.resolver.helpers.Debug;

/**
 * @author jmilliron
 * 
 */
public class CustomFunctionsValidatorJob implements IValidatorJob {

	//logger for class
	private static final Logger logger = Logger
			.getLogger(RulesValidatorJob.class);

	//holds all the custom function names
	private Set<String> customFunctionsSet = null;

	//an array of all the custom functions
	private CustomFunctionDef[] customFunctions = null;

	//ref to custom function file
	private IFile customFunctionFile = null;

	//ref to ireporter. used to add error messages
	protected IReporter _reporter;

	public CustomFunctionsValidatorJob() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#getSchedulingRule(org.eclipse.wst.validation.internal.provisional.core.IValidationContext)
	 */
	public ISchedulingRule getSchedulingRule(IValidationContext arg0) {

		//no scheduling rule
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#validateInJob(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	@SuppressWarnings("unchecked")
	public IStatus validateInJob(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		//set the class reporter
		_reporter = reporter;

		try {
			
			IProject project = null; 
			
			if ( helper instanceof Helper ) {
			
				project = ((Helper) helper).getProject();
				
			}
			
			if ( project != null) {
			
				//if URIs exist
				if (helper.getURIs() != null) {
	
					//get uris from helper
					String[] uris = helper.getURIs();
					
						// for each uri
					for (String uri : uris) {
	
						// get problem resource
						IResource resource = project.findMember(uri);
	
						// if instance of ifile, which should be because of
						// extension value
						if (resource instanceof IFile) {
	
							// cast to ifile
							customFunctionFile = (IFile) resource;
																			
							IFolder confFolder = (IFolder) customFunctionFile.getParent();							

							if (resource.getName().equals(PafBaseConstants.FN_CustomFunctionMetaData) && ! FileHelperUtil.isIFileDescendantOfIFolder(customFunctionFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
								loadCustomFunctionsJar(confFolder);

								//try to get custom function
								customFunctions = (CustomFunctionDef[]) ACPafXStream.importObjectFromXml(customFunctionFile);
																
								//if custom function exist
								if (customFunctions != null) {

									//initilize cf set
									customFunctionsSet = new HashSet<String>();

									//for each cf, add to set and verify function is loaded via jar file
									for (CustomFunctionDef cfDef : customFunctions) {

										//upper case function name
										customFunctionsSet.add(cfDef
											.getFunctionName()
											.toUpperCase());

										verifyFunctionIsLoaded(cfDef);
									}
								}								
								
							} else {
	
								//open error message
								GUIUtil
										.openMessageWindow(
												Constants.DIALOG_ERROR_HEADING,
												"Invalid custom functions exist. Verify you have the proper jar files placed in the "
														+ Constants.LIB_DIR
														+ " directory of your project.", MessageDialog.ERROR);
	
							}
	
						}
					}
				}
			}
		} catch (Exception e) {

			//return error status
			return new Status(IStatus.ERROR, "id of plugin", 0, e.getMessage(),
					e);
		}

		return Status.OK_STATUS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#cleanup(org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void cleanup(IReporter arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#validate(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void validate(IValidationContext helper, IReporter reporter)
			throws ValidationException {
		
		validateInJob(helper, reporter);

	}

	//used to create a message
	private void createMessage(IFile inputFile, String errorNode, String Id,
			String[] params) {

		// create blank error message
		Message errorMessage = new Message();

		// get errored line number
		int errorLineNo = IResourceUtil.findLineNo(inputFile, errorNode);

		// set values
		errorMessage.setLineNo(errorLineNo);
		errorMessage.setBundleName("plugin");
		errorMessage.setSeverity(IMessage.HIGH_SEVERITY);
		errorMessage.setId(Id);
		errorMessage.setParams(params);
		errorMessage.setTargetObject(inputFile);

		// add message to reporter
		_reporter.addMessage(this, errorMessage);
	}
	
	private void verifyFunctionIsLoaded(CustomFunctionDef cfDef) {

		try {

			//try to load class
			Class.forName(cfDef.getClassName().trim(), false, Thread
					.currentThread().getContextClassLoader());

		} catch (ClassNotFoundException cnfe) {

			// create an error message
			createMessage(customFunctionFile, cfDef.getFunctionName(),
					PluginConstants.FUNCTION_ERROR, new String[] { cfDef
							.getFunctionName() });

		}

	}

	private void loadCustomFunctionsJar(IFolder jarIFolder)
			throws CoreException {

		logger.debug("Trying to load jar files in jar folder: " + jarIFolder);
		
		URLClassLoader classLoader = null;

		try {

			//if jar folder is not null and members are greter than 0
			if ( jarIFolder != null && jarIFolder.members().length > 0  ) {
				
				//create new url list
				List<URL> urlList = new ArrayList<URL>();
				
				//for each recource, add jar file
				for (IResource iResource : jarIFolder.members()) {
					
					//ensure jar file
					if (iResource.getFileExtension().equals(Constants.JAR_EXT)) {
						
						urlList.add(iResource.getLocationURI().toURL());
						
					}
					
				}
				
				//if 1 or more jar files exist, add to class loader
				if ( urlList.size() > 0 ) {

					//create new class loader on top of current class loader
					classLoader = new URLClassLoader(urlList.toArray(new URL[0]), Activator.class.getClassLoader());					
					
				}
								
			}
		
		} catch (MalformedURLException e) {

			logger.debug("Malformed URL exception");
			e.printStackTrace();

		}

		//if class loader is not null, set current threads class loader
		if ( classLoader != null ) 	{
			
			Thread.currentThread().setContextClassLoader(classLoader);
			
		}

	}
}

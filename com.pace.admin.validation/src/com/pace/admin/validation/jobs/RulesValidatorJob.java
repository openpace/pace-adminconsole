/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.validation.jobs;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidatorJob;
import org.eclipse.wst.xml.core.internal.validation.core.Helper;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.GUIUtil;
import com.pace.admin.global.util.IResourceUtil;
import com.pace.admin.validation.Activator;
import com.pace.admin.validation.constants.PluginConstants;
import com.pace.base.PafBaseConstants;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureFunctionFactory;
import com.pace.base.funcs.CustomFunctionDef;
import com.pace.base.rules.Formula;
import com.pace.base.rules.Rule;
import com.pace.base.rules.RuleGroup;
import com.pace.base.rules.RuleSet;

/**
 * @author jmilliron
 * 
 */
public class RulesValidatorJob implements IValidatorJob {

	//logger for class
	private static final Logger logger = Logger
			.getLogger(RulesValidatorJob.class);

	//holds all the valid measure names
	private Set<String> measureSet = null;

	//holds all the custom function names
	private Set<String> customFunctionsSet = null;

	//an array of all the custom functions
	private CustomFunctionDef[] customFunctions = null;

	//ref to rules file
	private IFile rulesFile = null;

	//ref to custom function file
	private IFile customFunctionFile = null;

	//ref to ireporter. used to add error messages
	protected IReporter _reporter;

	//true/false to see if all custom fucntions are valid
	private boolean allCustomFunctionsValid = false;

	public RulesValidatorJob() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#getSchedulingRule(org.eclipse.wst.validation.internal.provisional.core.IValidationContext)
	 */
	public ISchedulingRule getSchedulingRule(IValidationContext arg0) {

		//no scheduling rule
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#validateInJob(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	@SuppressWarnings("unchecked")
	public IStatus validateInJob(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		//set the class reporter
		_reporter = reporter;

		try {			
			
			IProject project = null; 
			
			if ( helper instanceof Helper ) {
			
				project = ((Helper) helper).getProject();
				
			}
			
			if ( project != null) {
			
				//if URIs exist
				if (helper.getURIs() != null) {
	
					//get uris from helper
					String[] uris = helper.getURIs();
					
						// for each uri
					for (String uri : uris) {
	
						// get problem resource
						IResource resource = project.findMember(uri);
	
						// if instance of ifile, which should be because of
						// extension value
						if (resource instanceof IFile) {
	
							// cast to ifile
							rulesFile = (IFile) resource;
																			
							IFolder confFolder = null;
							
							if ( rulesFile.getName().equals(PafBaseConstants.FN_RuleSetMetaData)&& ! FileHelperUtil.isIFileDescendantOfIFolder(rulesFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
								
								//get conf folder
								confFolder = (IFolder) rulesFile.getParent();
								
							} else if ( rulesFile.getParent().getName().startsWith(PafBaseConstants.DN_RuleSetsFldr) && ! FileHelperUtil.isIFileDescendantOfIFolder(rulesFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
								
								confFolder = (IFolder) rulesFile.getParent().getParent();
								
							} else {
								
								//continue to next resource
								continue;
							}
	
												
							//get all files from conf dir and loop through them
							for (IResource iResource : confFolder.members()) {
	
								//if the resource is an ifile, see if it matches measures or fucntions
								if (iResource instanceof IFile) {
	
									//if measures file
									if (iResource.getName().equals(
											PafBaseConstants.FN_MeasureMetaData)) {
	
										//get ifile ref to measures file
										IFile measureFile = (IFile) iResource;
	
										//get the measure definitions
										MeasureDef[] measureDefs = (MeasureDef[]) ACPafXStream
												.importObjectFromXml(measureFile);
	
										//if measure defs exist, populate the measures set
										if (measureDefs != null) {
	
											//initilize the measure set
											measureSet = new HashSet<String>();
	
											//TODO: ADD dimension name to valid list
											
											for (MeasureDef measureDef : measureDefs) {
	
												//add measure def name to set
												measureSet
														.add(measureDef.getName());
	
											}
	
										}
	
									//if function file
									} else if (iResource.getName().equals(PafBaseConstants.FN_CustomFunctionMetaData)) {
	
										//get ref to function file
										customFunctionFile = (IFile) iResource;
	
										//try to get custom function
										customFunctions = (CustomFunctionDef[]) ACPafXStream
												.importObjectFromXml(customFunctionFile);
																			
										//if custom function exist
										if (customFunctions != null) {
	
											//initilize cf set
											customFunctionsSet = new HashSet<String>();
	
											// set to true
											allCustomFunctionsValid = true;
	
											//for each cf, add to set and verify function is loaded via jar file
											for (CustomFunctionDef cfDef : customFunctions) {
	
												//upper case function name
												customFunctionsSet.add(cfDef
														.getFunctionName()
														.toUpperCase());
	
												boolean validFunction = verifyFunctionIsLoaded(cfDef);
	
												//if function is not valid, set flag
												if (!validFunction) {
	
													allCustomFunctionsValid = false;
													break;
												}
											}
										}
									}
	
								}
							}
	
							//if all custom functions are valid
							if (allCustomFunctionsValid) {
	
	//							get the measure function factory
								MeasureFunctionFactory measureFactor = new MeasureFunctionFactory(
										customFunctions, Thread.currentThread()
												.getContextClassLoader());
								
								
								if ( rulesFile.getName().equals(PafBaseConstants.FN_RuleSetMetaData)) {
								
	//								get rules List from rules file
									List<RuleSet> ruleList = (List<RuleSet>) ACPafXStream
											.importObjectFromXml(rulesFile);
	
									//if rules exist
									if (ruleList != null) {
										
										//for each rule set in rule list
										for (RuleSet ruleSet : ruleList) {
	
											validateRuleSet(rulesFile, ruleSet, measureFactor);
											
										}
	
									}
									
								} else if ( rulesFile.getParent().getName().startsWith(PafBaseConstants.DN_RuleSetsFldr)) {
	
									RuleSet ruleSet = (RuleSet) ACPafXStream.importObjectFromXml(rulesFile);
															
									validateRuleSet(rulesFile, ruleSet, measureFactor);
									
								} 							
								
							} else {
	
								//open error message
								GUIUtil
										.openMessageWindow(
												Constants.DIALOG_ERROR_HEADING,
												"Invalid custom functions exist. Verify you have the proper jar files placed in the "
														+ Constants.LIB_DIR
														+ " directory of your project.", MessageDialog.ERROR);
	
							}
	
						}
					}
				}
			}
		} catch (Exception e) {

			//return error status
			return new Status(IStatus.ERROR, "id of plugin", 0, e.getMessage(),
					e);
		}

		return Status.OK_STATUS;
	}

	private void validateRuleSet(IFile rulesFile, RuleSet ruleSet, MeasureFunctionFactory measureFactor) {
		
		//if rule set is not null and file is not in backup folder
		if (ruleSet != null && ! FileHelperUtil.isIFileDescendantOfIFolder(rulesFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
						
			//validate rule set name
			validateRuleSetName(rulesFile, ruleSet);
			
			//get measure's list
			String[] measureList = ruleSet.getMeasureList();
			
			//if measures exists, validate list
			if ( measureList != null && measureList.length > 0 ) {
				
				for (String measure : measureList) {
				
					validateMeasure(measure, null);
					
				}				
				
			}

			//get the rule groups
			RuleGroup[] ruleGroups = ruleSet
					.getRuleGroups();

			//if rule groups are not null
			if (ruleGroups != null) {

				//for each rule group get rules
				for (RuleGroup ruleGroup : ruleGroups) {

					//get rules from rule group
					Rule[] rules = ruleGroup
							.getRules();

					//if rules exist
					if (rules != null) {

						//for each rule, validate
						for (Rule rule : rules) {

							//get formula from rule
							Formula formula = rule
									.getFormula();

							logger.debug(formula);

							try {

								//parse formula
								formula
										.parse(measureFactor);

								//get expression terms from formula
								String[] expressionTerms = formula
										.getExpressionTerms();

								//for each expression term
								for (String expressionTerm : expressionTerms) {

									logger
											.debug("\tValidating Expression Term: "
													+ expressionTerm);

									//if custom function
									if (expressionTerm
											.startsWith("@")) {

										//validate custom function
										validateFunctionName(
												expressionTerm,
												formula
														.getExpression());

									//else default is measure
									} else {

										//validate measure
										validateMeasure(
												expressionTerm,
												formula
														.getExpression());

									}

								}

								//get result term
								String resultTerm = formula
										.getResultTerm();

								logger
										.debug("\tValidating Expression Result: "
												+ resultTerm
												+ "\n");

								//if result is function
								if (formula
										.isResultFunction()) {

									validateFunctionName(
											resultTerm,
											formula
													.getResultTerm());
								//result is not function
								} else {

									validateMeasure(
											resultTerm,
											formula
													.getResultTerm());

								}
								
							//catch any exception
							} catch (Exception exp) {

								//get erro message
								String errorMessage = exp
										.getMessage();

								//get indexes of error message
								int leftNdx = errorMessage
										.indexOf('[');
								int rightNdx = errorMessage
										.indexOf(']');

								//get error expression
								String errorExpression = errorMessage
										.substring(
												leftNdx + 1,
												rightNdx);

								logger
										.debug("Validating error message expression: "
												+ errorExpression);

								//validated function as result term
								if (formula
										.getResultTerm()
										.startsWith(
												"@")) {

									validateFunctionName(
											errorExpression,
											formula
													.getResultTerm());
								//validate function as expression
								} else {

									validateFunctionName(
											errorExpression,
											formula
													.getExpression());

								}

								//print stack trace and log
								exp
										.printStackTrace();
								logger
										.debug(exp
												.getMessage());

							}

							//get trigger measures from rule
							String[] triggerMeasures = rule
									.getTriggerMeasures();

							//if trigger measures exist
							if (triggerMeasures != null) {

								//for each trigger measure
								for (String triggerMeasure : triggerMeasures) {

									//validate trigger measure
									validateMeasure(
											triggerMeasure,
											null);

								}

							}

							//if base allocate measure exist, validate it
							if (rule
									.getBaseAllocateMeasure() != null && !rule.getBaseAllocateMeasure().trim().equals("")) {

								//validated base allocate measure
								validateMeasure(
										rule
												.getBaseAllocateMeasure(),
										null);

							}

						}

					}
				}

			}
		}
		
	}
	
	/**
	 * Validates a ruleset with the xml rule name.  For example, if rule set xml was 
	 * Planning.xml then the rulset name tag should be <name>Planning</name>
	 * 
	 * @param rulesFile Rules file to validate
	 * @param ruleSet	Rule set to validate
	 */
	private void validateRuleSetName(IFile rulesFile, RuleSet ruleSet) {

		//make sure both are not null
		if ( ruleSet != null && rulesFile != null ) {
			
			String rulesFileName = rulesFile.getName().substring(0, rulesFile.getName().indexOf(PafBaseConstants.XML_EXT));
			
			String ruleSetName = ruleSet.getName();
			
			//if names are not equal
			if ( ! rulesFileName.equalsIgnoreCase(ruleSetName)) {
				
				createMessage(rulesFile, ruleSet.getName(), PluginConstants.RULE_SET_NAME_ERROR, new String[] { ruleSetName, rulesFileName });
				
			}
			
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#cleanup(org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void cleanup(IReporter arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#validate(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void validate(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		validateInJob(helper, reporter);

	}

	//used to create a message
	private void createMessage(IFile inputFile, String errorNode, String Id,
			String[] params) {

		// create blank error message
		Message errorMessage = new Message();

		// get errored line number
		int errorLineNo = IResourceUtil.findLineNo(inputFile, errorNode);

		// set values
		errorMessage.setLineNo(errorLineNo);
		errorMessage.setBundleName("plugin");
		errorMessage.setSeverity(IMessage.HIGH_SEVERITY);
		errorMessage.setId(Id);
		errorMessage.setParams(params);
		errorMessage.setTargetObject(inputFile);

		// add message to reporter
		_reporter.addMessage(this, errorMessage);
	}

	//used to validated a measure
	private void validateMeasure(String measure, String fullExpression) {

		//if measure set is not null
		if (measureSet != null) {

			//if measure set does not contain measure
			if (! measureSet.contains(measure) ) {

				//if full expression is null, use measure
				if (fullExpression == null) {
					createMessage(rulesFile, measure,
							PluginConstants.MEASURE_ERROR,
							new String[] { measure });
					
				//else use full expression
				} else {
					createMessage(rulesFile, fullExpression,
							PluginConstants.MEASURE_ERROR,
							new String[] { measure });
				}

			}

		}

	}

	//validate function name
	private void validateFunctionName(String functionExpression,
			String fullExpression) {

		//if custom function set exist
		if (customFunctionsSet != null) {

			//get left par index
			int leftParNdx = functionExpression.indexOf('(');

			//get function name
			String functionName = functionExpression.substring(0, leftParNdx)
					.trim();

			// if the function name is not in the list of valid custom functions
			if (!customFunctionsSet.contains(functionName.toUpperCase())) {

				//create an error message
				createMessage(rulesFile, fullExpression,
						PluginConstants.FUNCTION_ERROR,
						new String[] { functionName });

			}

		}

	}
	
	private boolean verifyFunctionIsLoaded(CustomFunctionDef cfDef) {

		boolean validFunction = false;

		try {

			//try to load class
			Class.forName(cfDef.getClassName().trim(), false, Thread
					.currentThread().getContextClassLoader());

			// if passes loading, is valid
			validFunction = true;

		} catch (ClassNotFoundException cnfe) {

			// create an error message
			

		}

		//true if valid function
		return validFunction;

	}

}

/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.admin.validation.jobs;

import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidatorJob;
import org.eclipse.wst.xml.core.internal.validation.core.Helper;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.IResourceUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.exceptions.DimensionNotFoundException;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.validation.constants.PluginConstants;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.VersionDef;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PafXStream;
import com.pace.server.client.PafSimpleDimTree;

/**
 * @author jmilliron
 * 
 */
public class VersionsValidatorJob implements IValidatorJob {

	private static final Logger logger = Logger
			.getLogger(VersionsValidatorJob.class);

	protected IReporter _reporter;

	public VersionsValidatorJob() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#getSchedulingRule(org.eclipse.wst.validation.internal.provisional.core.IValidationContext)
	 */
	public ISchedulingRule getSchedulingRule(IValidationContext arg0) {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#validateInJob(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public IStatus validateInJob(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		_reporter = reporter;

		
		//remove all messages from reporter
		_reporter.removeAllMessages(this);

		IProject project = null; 
		
		if ( helper instanceof Helper ) {
		
			project = ((Helper) helper).getProject();
			
			try {
				PafProjectUtil.addPafProjectMap(project);
			} catch (PafServerNotFound e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if ( project != null) {
			//if URIs exist
			if (helper.getURIs() != null) {

				//get uris from helper
				String[] uris = helper.getURIs();
				
					// for each uri
				for (String uri : uris) {

					// get problem resource
					IResource resource = project.findMember(uri);
					// if instance of ifile, which should be because of extension
					// value
					if (resource instanceof IFile) {
	
						// cast to ifile
						IFile versionsFile = (IFile) resource;
	
						if (  ! FileHelperUtil.isIFileDescendantOfIFolder(versionsFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
						
							// get project and folder
							IFolder iFolder = project.getFolder(Constants.CONF_DIR);
		
							// get pafapps.xml ifile
							IFile pafAppFile = iFolder.getFile(PafBaseConstants.FN_ApplicationMetaData);
		
							String versionDim = DimensionUtil
									.getVersionDimensionName(project);
						
							Set<String> dimensionMemberSet = null;
		
							try {
		
								// get dimension members in a unique set
								dimensionMemberSet = DimensionTreeUtility
										.getDimensionMemberSet(project, PafProjectUtil.getApplicationName(project), versionDim);
		
							} catch (DimensionNotFoundException dnfe) {
		
								return new Status(IStatus.ERROR, "id of plugin", 0,
										"Can not validate " + versionsFile.getName()
												+ " because dimension '" + versionDim
												+ "' was not found on the server.",
										dnfe);
		
							} catch (ServerNotRunningException snre) {
		
								PafServer pafServer = null;
		
								try {
		
									pafServer = PafProjectUtil
											.getProjectServer(project);
		
									return new Status(IStatus.ERROR, "id of plugin", 0,
											"Can not validate " + versionsFile
													+ " because server '"
													+ pafServer.getName()
													+ "' is not running.", snre);
		
								} catch (PafServerNotFound epsnf) {
		
									return new Status(
											IStatus.ERROR,
											"id of plugin",
											0,
											"Can not validate " + versionsFile
													+ " because server '"
													+ pafServer.getName()
													+ "' was not found. Please fix "
													+ Constants.SERVERS_FILE + " file.",
											epsnf);
		
								}
		
							} catch (Exception exc) {
		
								return new Status(IStatus.ERROR, "id of plugin", 0, exc
										.getMessage(), exc);
		
							}
		
							PafSimpleDimTree cachedTree = null;
							
							// get cached tree
							try {
								cachedTree = cachedTree = (PafSimpleDimTree) PafXStream
										.importObjectFromXml(DimensionTreeUtility
												.getFullDimensionNameFileName(project, PafProjectUtil.getApplicationName(project), 
														versionDim));
							} catch (PafConfigFileNotFoundException e) {
								return new Status(IStatus.ERROR, "Paf config file not found", 0, e.getMessage(), e);
							}
		
							VersionDef[] versionDefs = null;
		
							// try to load versions defs. if blows up, means xml is not
							// well formatted
							try {
		
								versionDefs = (VersionDef[]) ACPafXStream
										.importObjectFromXml(versionsFile);
		
							} catch (Exception ee) {
		
								logger.warn(ee.getMessage());
							}
		
							// if versions defs exisis
							if (versionDefs != null) {
		
								// for each version, check to see if valid
								for (VersionDef versionDef : versionDefs) {
		
									// if the member does not exist in the dimension
									// member set, handle error
									if (!dimensionMemberSet.contains(versionDef
											.getName())) {
		
										// create blank error message
										Message errorMessage = new Message();
		
										// get errored line number
										int errorLineNo = IResourceUtil.findLineNo(
												versionsFile, versionDef.getName());
		
										// set values
										errorMessage.setLineNo(errorLineNo);
										errorMessage.setBundleName("plugin");
										errorMessage
												.setSeverity(IMessage.HIGH_SEVERITY);
										errorMessage
												.setId(PluginConstants.VERSION_ERROR);
										errorMessage
												.setParams(new String[] { versionDef
														.getName() });
										errorMessage.setTargetObject(versionsFile);
		
										// add message to reporter
										_reporter.addMessage(this, errorMessage);
										
										return new Status(IStatus.ERROR, "Paf config file not found", errorMessage.getText());
									}
		
								}
							}
						}
					}
				}
			}
		}

		return Status.OK_STATUS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#cleanup(org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void cleanup(IReporter arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#validate(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void validate(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		validateInJob(helper, reporter);

	}

}

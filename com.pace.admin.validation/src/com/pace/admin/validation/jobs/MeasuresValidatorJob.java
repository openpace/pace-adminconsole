/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.admin.validation.jobs;

import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidatorJob;
import org.eclipse.wst.xml.core.internal.validation.core.Helper;

import com.pace.admin.global.constants.Constants;
import com.pace.admin.global.exceptions.PafServerNotFound;
import com.pace.admin.global.util.ACPafXStream;
import com.pace.admin.global.util.DimensionUtil;
import com.pace.admin.global.util.FileHelperUtil;
import com.pace.admin.global.util.IResourceUtil;
import com.pace.admin.global.util.PafProjectUtil;
import com.pace.admin.servers.exceptions.DimensionNotFoundException;
import com.pace.admin.servers.exceptions.ServerNotRunningException;
import com.pace.admin.servers.utils.DimensionTreeUtility;
import com.pace.admin.validation.constants.PluginConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.app.MeasureDef;
import com.pace.base.ui.PafServer;
import com.pace.base.utility.PafXStream;
import com.pace.server.client.PafSimpleDimTree;

/**
 * @author jmilliron
 * 
 */
public class MeasuresValidatorJob implements IValidatorJob {

	//logger for error messages
	private static final Logger logger = Logger
			.getLogger(MeasuresValidatorJob.class);

	//reporter to infrom of errors
	protected IReporter _reporter;

	//nothing much here
	public MeasuresValidatorJob() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#getSchedulingRule(org.eclipse.wst.validation.internal.provisional.core.IValidationContext)
	 */
	public ISchedulingRule getSchedulingRule(IValidationContext arg0) {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidatorJob#validateInJob(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public IStatus validateInJob(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		//set reporter
		_reporter = reporter;

		//remove all messages from reporter
		_reporter.removeAllMessages(this);

		IProject project = null; 
		
		if ( helper instanceof Helper ) {
		
			project = ((Helper) helper).getProject();
			
			try {
				PafProjectUtil.addPafProjectMap(project);
			} catch (PafServerNotFound e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if ( project != null) {
		
			//if URIs exist
			if (helper.getURIs() != null) {

				//get uris from helper
				String[] uris = helper.getURIs();
				
					// for each uri
				for (String uri : uris) {

					// get problem resource
					IResource resource = project.findMember(uri);

					// if instance of ifile, which should be because of
					// extension value
					if (resource instanceof IFile) {
	
						// cast to ifile
						IFile measuresFile = (IFile) resource;
											
						if (  ! FileHelperUtil.isIFileDescendantOfIFolder(measuresFile, PluginConstants.IFOLDER_BACKUP_IGNORE)) {
							//get measure dim name from dimension utility
							String measureDim = DimensionUtil
									.getMeasureDimensionName(project);
		
							//holds a set of dimension members in a unique set
							Set<String> dimensionMemberSet = null;
							
							try {
		
								//PafServer pafServer = PafProjectUtil.getProjectServer(iProject);
								
								// get dimension members in a unique set
								dimensionMemberSet = DimensionTreeUtility
										.getDimensionMemberSet(project, PafProjectUtil.getApplicationName(project), measureDim);
							
							//is thrown in the dimension name is not found
							} catch (DimensionNotFoundException dnfe) {
		
								//flag as error by returning an IStatus of ERROR with error message
								return new Status(IStatus.ERROR, "id of plugin", 0,
										"Can not validate "
										+ measuresFile.getName()
										+ " because dimension '" + measureDim
										+ "' was not found on the server.", dnfe);
										
							//is thrown if the project server is not running
							} catch (ServerNotRunningException snre) {
		
								//instance of project server
								PafServer pafServer = null;
		
								try {
		
									//try to get project server
									pafServer = PafProjectUtil
											.getProjectServer(project);
		
									//flag as error by returning an IStatus of ERROR with error message
									return new Status(IStatus.ERROR, "id of plugin", 0,
											"Can not validate " + measuresFile
													+ " because server '"
													+ pafServer.getName()
													+ "' is not running.", snre);
									
								//if project server is not found
								} catch (PafServerNotFound epsnf) {
		
									//flag as error by returning an IStatus of ERROR with error message
									return new Status(IStatus.ERROR, "id of plugin", 0,
											"Can not validate " + measuresFile + " because server '" + pafServer.getName() 
											+ "' was not found. Please fix " 
											+ Constants.SERVERS_FILE 
											+ " file.", epsnf);
		
								}
		
							//catch any other exception and return ERROR IStatus
							} catch (Exception exc) {
		
								return new Status(IStatus.ERROR, "id of plugin", 0,
										exc.getMessage(), exc);
		
							}
		
							//made it this far, everything must be ok.  hold cached dim tree
							PafSimpleDimTree cachedTree = null;
		
							// try to get cached tree
							try {
								
								//importing the paf simple tree from cache or server
								cachedTree = (PafSimpleDimTree) PafXStream
										.importObjectFromXml(DimensionTreeUtility
												.getFullDimensionNameFileName(project, PafProjectUtil.getApplicationName(project), 
														measureDim));
							//file not found
							} catch (PafConfigFileNotFoundException e) {
		
								logger.error(e.getMessage());
		
							}
		
							//hold measure definitions
							MeasureDef[] measureDefs = null;
		
							// try to load measures defs. if blows up, means xml is
							// not well formatted
							try {
		
								measureDefs = (MeasureDef[]) ACPafXStream
										.importObjectFromXml(measuresFile);
		
							} catch (Exception ee) {
		
								logger.warn(ee.getMessage());
							}
		
							// if measures defs exisis
							if (measureDefs != null) {
		
								// for each measure, check to see if valid
								for (MeasureDef measureDef : measureDefs) {
		
									// if the member does not exist in the dimension
									// member set, handle error
									if (!dimensionMemberSet.contains(measureDef
											.getName())) {
		
										// create blank error message
										Message errorMessage = new Message();
		
										// get errored line number
										int errorLineNo = IResourceUtil.findLineNo(
												measuresFile, measureDef.getName());
		
										// set values
										errorMessage.setLineNo(errorLineNo);
										errorMessage.setBundleName("plugin");
										errorMessage
												.setSeverity(IMessage.HIGH_SEVERITY);
										errorMessage
												.setId(PluginConstants.MEASURE_ERROR);
										errorMessage
												.setParams(new String[] { measureDef
														.getName() });
										errorMessage.setTargetObject(measuresFile);
		
										// add message to reporter
										_reporter.addMessage(this, errorMessage);
									}
		
								}
							}
						}
					}
				}
			}
		}

		//if we make it this far, the status should be OK.  Have a nice day! JJM
		return Status.OK_STATUS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#cleanup(org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void cleanup(IReporter arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.wst.validation.internal.provisional.core.IValidator#validate(org.eclipse.wst.validation.internal.provisional.core.IValidationContext,
	 *      org.eclipse.wst.validation.internal.provisional.core.IReporter)
	 */
	public void validate(IValidationContext helper, IReporter reporter)
			throws ValidationException {

		validateInJob(helper, reporter);

	}

}
